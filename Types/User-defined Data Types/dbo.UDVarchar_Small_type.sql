CREATE TYPE [dbo].[UDVarchar_Small_type] FROM nvarchar (50) NOT NULL
GO
GRANT REFERENCES ON TYPE:: [dbo].[UDVarchar_Small_type] TO [public]
GO
