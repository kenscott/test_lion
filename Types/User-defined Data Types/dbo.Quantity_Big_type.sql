CREATE TYPE [dbo].[Quantity_Big_type] FROM decimal (28, 8) NOT NULL
GO
GRANT REFERENCES ON TYPE:: [dbo].[Quantity_Big_type] TO [public]
GO
