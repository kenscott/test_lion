CREATE TYPE [dbo].[UDDecimal_type] FROM decimal (38, 8) NOT NULL
GO
GRANT REFERENCES ON TYPE:: [dbo].[UDDecimal_type] TO [public]
GO
