CREATE TYPE [dbo].[Indicator_type] FROM binary (1) NOT NULL
GO
GRANT REFERENCES ON TYPE:: [dbo].[Indicator_type] TO [public]
GO
