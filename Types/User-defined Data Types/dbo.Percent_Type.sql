CREATE TYPE [dbo].[Percent_Type] FROM decimal (19, 8) NOT NULL
GO
GRANT REFERENCES ON TYPE:: [dbo].[Percent_Type] TO [public]
GO
