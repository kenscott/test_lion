CREATE TYPE [dbo].[Money_Normal_Type] FROM decimal (19, 8) NULL
GO
GRANT REFERENCES ON TYPE:: [dbo].[Money_Normal_Type] TO [public]
GO
