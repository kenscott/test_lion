CREATE TYPE [dbo].[Money_Big_type] FROM decimal (28, 8) NOT NULL
GO
GRANT REFERENCES ON TYPE:: [dbo].[Money_Big_type] TO [public]
GO
