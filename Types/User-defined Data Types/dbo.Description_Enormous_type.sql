CREATE TYPE [dbo].[Description_Enormous_type] FROM nvarchar (max) NOT NULL
GO
GRANT REFERENCES ON TYPE:: [dbo].[Description_Enormous_type] TO [public]
GO
