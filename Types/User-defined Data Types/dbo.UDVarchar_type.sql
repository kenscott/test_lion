CREATE TYPE [dbo].[UDVarchar_type] FROM nvarchar (250) NOT NULL
GO
GRANT REFERENCES ON TYPE:: [dbo].[UDVarchar_type] TO [public]
GO
