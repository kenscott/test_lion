CREATE TYPE [dbo].[Quantity_Normal_type] FROM decimal (19, 8) NOT NULL
GO
GRANT REFERENCES ON TYPE:: [dbo].[Quantity_Normal_type] TO [public]
GO
