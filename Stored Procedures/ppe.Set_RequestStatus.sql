SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [ppe].[Set_RequestStatus]
	@WebUserKey [INT],
	@Action [VARCHAR](10),
	@BatchKey [INT]
WITH EXECUTE AS CALLER
AS
BEGIN

	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

	DECLARE 
		@ApprovedStatusKey INT, 
		@ClosedStatusKey INT, 
		@HighestLevel INT, 
		@Margin DEC(10,4),
		@CountInBatch INT,
		@CountUpdated INT,
		@SPG VARCHAR(10),
		@ItemGroup3Key INT,
		@SPGDesc VARCHAR(100),
		@ItemKey INT

	SELECT @ApprovedStatusKey = StatusKey FROM ppe.RequestStatus WHERE StatusName = 'Approved'
	SELECT @ClosedStatusKey = StatusKey FROM ppe.RequestStatus WHERE StatusName = 'Closed'

	SELECT @CountInBatch = COUNT(*) , @ItemKey = MIN(ItemKey)
	FROM ppe.RequestMain r
	WHERE r.BatchKey = @BatchKey
	AND r.StatusKey NOT IN (@ClosedStatusKey,@ApprovedStatusKey)

	IF @CountInBatch = 1 AND @ItemKey <> 1
	BEGIN
		SELECT /*per LIO-1005 return NULL if request SPG and Item SPG match.  return item SPG otherwise*/
			@SPG = CASE WHEN dig3.ItemGroup3Key = dig32.ItemGroup3Key THEN NULL ELSE dig3.IG3Level1 END,
			@ItemGroup3Key = CASE WHEN dig3.ItemGroup3Key = dig32.ItemGroup3Key THEN NULL ELSE dig3.ItemGroup3Key END,
			@SPGDesc = CASE WHEN dig3.ItemGroup3Key = dig32.ItemGroup3Key THEN NULL ELSE dig3.IG3Level1UDVarchar1 END
		FROM ppe.RequestMain r
		JOIN dbo.DimItem di ON di.ItemKey = r.ItemKey
		JOIN dbo.DimItemGroup3 dig3 ON dig3.ItemGroup3Key = di.ItemGroup3Key
		JOIN dbo.DimItemGroup3 dig32 ON dig32.ItemGroup3Key = r.ItemGroup3Key
		WHERE r.BatchKey = @BatchKey
	END
	ELSE
		SELECT @SPG = NULL, @ItemGroup3Key=NULL, @SPGDesc=NULL

	SELECT @HighestLevel = MAX(ISNULL(wr.WorkflowLevel,0))
	FROM dbo.WebUserRole wur
	JOIN dbo.WebRole wr ON wr.WebRoleKey = wur.WebRoleKey
	WHERE wur.WebUserKey = @WebUserKey

	SELECT @Margin = CAST(ParamValue AS DEC(10,4)) FROM dbo.Param WHERE ParamName='WorkflowMargin'

	IF ISNULL(@WebUserKey,-1) NOT IN (SELECT WebUserKey FROM dbo.WebUser)
		SELECT -1 AS Result
	ELSE IF ISNULL(@Action,'') NOT IN ('ACCEPT','REJECT','CLOSE','APPROVE')
		SELECT -1 AS Result
	ELSE IF ISNULL(@BatchKey,-1) NOT IN (SELECT BatchKey FROM lion.Batch)
		SELECT -1 AS Result
	ELSE
	BEGIN



		INSERT INTO [ppe].[RequestLog]
			   ([WebUserKey]
			   ,[RequestKey]
			   ,[OldStatusKey]
			   ,[NewStatusKey]
			   ,[RequestType]
			   ,[RequestAction]
			   ,EmailSent
			   ,OldOwnerWebUserKey
			   ,NewOwnerWebUserKey
			   ,Notes)
		SELECT
			@WebUserKey,
			r.RequestKey,
			r.StatusKey,
			CASE 
				WHEN @Action = 'APPROVE' THEN @ApprovedStatusKey
				WHEN @Action = 'ACCEPT' THEN
					CASE 
						WHEN rs.StatusPosition = 0 THEN rs2.StatusKey /* per workflow rules set status to level of account owner.  See LEFT JOIN below */
						WHEN rs.StatusPosition = 1 THEN
							CASE 
								WHEN r.NewDeleteTerm = 1 THEN @ApprovedStatusKey
								WHEN ncd.Discount <= rsd.Discount -- per LIO-1084 AND ncd.Discount <= rd.Discount
									THEN @ApprovedStatusKey -- happy path
								WHEN @HighestLevel <= 1 -- when at branch and actor is branch or lower
									THEN (SELECT StatusKey FROM ppe.RequestStatus WHERE StatusPosition = rs.StatusPosition + 1 )
								WHEN @HighestLevel  = 2 -- when at branch and actor is network
									THEN (SELECT StatusKey FROM ppe.RequestStatus WHERE StatusPosition = rs.StatusPosition + 2 )
								WHEN @HighestLevel  >= 3 -- when at branch and actor is central
									THEN @ApprovedStatusKey
							END
						WHEN rs.StatusPosition = 2 THEN
							CASE 
								WHEN r.NewDeleteTerm = 1 THEN @ApprovedStatusKey
								WHEN ncd.Discount <= rsd.Discount OR /* OR vs. and LPF-1192 */ /*OR per LIO-1084*/ ncd.Discount <= rd.Discount
									THEN @ApprovedStatusKey -- happy path
								WHEN @HighestLevel <= 2 -- when at network and actor is network or lower
									THEN (SELECT StatusKey FROM ppe.RequestStatus WHERE StatusPosition = rs.StatusPosition + 1 )
								WHEN @HighestLevel  >= 3 -- when at network and actor is central
									THEN @ApprovedStatusKey
							END
						WHEN rs.StatusPosition = 3 THEN -- if accept at central, then approved
							@ApprovedStatusKey
						ELSE (SELECT StatusKey FROM ppe.RequestStatus 
								WHERE StatusPosition = rs.StatusPosition + 1 )
					END
				WHEN @Action = 'CLOSE' THEN @ClosedStatusKey
				WHEN @Action = 'REJECT' THEN
					CASE 
						WHEN rs.StatusPosition = 1 THEN 1 
						WHEN r.OwnerWebUserKey = wu0.WebUserKey THEN r.StatusKey --LPF-1326 cannot reject if request owner = account owner
						WHEN  p.LevelId = 'NetworkManager' AND pp.FullName = p.FullName THEN r.StatusKey  --LPF-4552, Don't allow level 2 managers to reject request they own
						ELSE (SELECT StatusKey FROM ppe.RequestStatus WHERE StatusPosition = rs.StatusPosition - 1 )					
					END 
				ELSE r.StatusKey
			END,
			'',
			@Action,
			CASE --EmailSent:  NULL for happy path, 0 for everything else
				WHEN @Action = 'APPROVE' THEN NULL -- per LPF-1679
				WHEN @Action = 'ACCEPT' THEN
					CASE 
						WHEN rs.StatusPosition = 0 THEN 0
						WHEN rs.StatusPosition = 1 THEN
							CASE 
								WHEN r.NewDeleteTerm = 1 THEN NULL
								WHEN ncd.Discount <= rsd.Discount -- per LIO-1084 AND ncd.Discount <= rd.Discount
									THEN NULL -- happy path
								WHEN @HighestLevel <= 1 -- when at branch and actor is branch or lower
									THEN 0
								WHEN @HighestLevel  = 2 -- when at branch and actor is network
									THEN 0
								WHEN @HighestLevel  >= 3 -- when at branch and actor is central
									THEN NULL -- per LPF-1679
							END
						WHEN rs.StatusPosition = 2 THEN
							CASE 
								WHEN r.NewDeleteTerm = 1 THEN NULL
								WHEN ncd.Discount <= rsd.Discount OR /* OR vs. and LPF-1192 */ /*OR per LIO-1084*/ ncd.Discount <= rd.Discount
									THEN NULL -- happy path
								WHEN @HighestLevel <= 2 -- when at network and actor is network or lower
									THEN 0
								WHEN @HighestLevel  >= 3 -- when at network and actor is central
									THEN NULL -- per LPF-1679
							END
						WHEN rs.StatusPosition = 3 THEN -- if accept at central, then approved
							NULL -- per LPF-1679
						ELSE 0
					END
				WHEN @Action = 'CLOSE' THEN NULL
				WHEN @Action = 'REJECT' THEN 0
				ELSE 0
			END,
			CASE 
				WHEN @Action = 'APPROVE' THEN r.OwnerWebUserKey
				WHEN @Action = 'ACCEPT' THEN r.OwnerWebUserKey -- on acceptance, current becomes prior 
				WHEN @Action = 'REJECT' THEN r.OwnerWebUserKey -- owner of account
				WHEN @Action = 'CLOSE' THEN r.OwnerWebUserKey -- owner of account
			END,
			CASE 
				WHEN @Action = 'APPROVE' THEN wu0.WebUserKey
				WHEN @Action = 'ACCEPT' THEN --wu2.WebUserKey -- managers webuserkey
					CASE
						WHEN r.NewDeleteTerm = 1 AND rs.StatusPosition <> 0 THEN wu0.WebUserKey -- owner of account
						WHEN rs.StatusPosition = 0 THEN wu0.WebUserKey
						WHEN rs.StatusPosition = 1 THEN
							CASE 
								WHEN ncd.Discount <= rsd.Discount -- per LIO-1084 AND ncd.Discount <= rd.Discount
									THEN wu0.WebUserKey  -- owner of account -- happy path
								WHEN @HighestLevel <= 1 -- when at branch and actor is branch or lower
									THEN wu1.WebUserKey -- managers webuserkey
								WHEN @HighestLevel  = 2 
									THEN  wu2.WebUserKey -- manager's manager webuserkey
								WHEN @HighestLevel  >= 3 -- when at branch and actor is central
									THEN wu0.WebUserKey -- owner of account
							END
						WHEN rs.StatusPosition = 2 AND wr0.WebRoleName = 'BranchManager' THEN
							CASE 
								WHEN ncd.Discount <= rsd.Discount OR /* OR vs. and LPF-1192 */ /*OR per LIO-1084*/ ncd.Discount <= rd.Discount
									THEN wu0.WebUserKey  -- owner of account -- happy path
								WHEN @HighestLevel <= 2 -- when at network and actor is network or lower
									THEN  wu2.WebUserKey -- manager's manager webuserkey
								WHEN @HighestLevel  >= 3 -- when at network and actor is central
									THEN wu0.WebUserKey -- owner of account
							END
						WHEN rs.StatusPosition = 2 AND wr0.WebRoleName = 'NetworkManager' THEN
							CASE 
								WHEN ncd.Discount <= rsd.Discount OR /* OR vs. and LPF-1192 */ /*OR per LIO-1084*/ ncd.Discount <= rd.Discount
									THEN wu0.WebUserKey  -- owner of account -- happy path
								WHEN @HighestLevel <= 1 -- when at branch and actor is branch or lower
									THEN wu1.WebUserKey -- managers webuserkey
								WHEN @HighestLevel <= 2 -- when at network and actor is network or lower
									THEN  wu1.WebUserKey -- manager's webuserkey
								WHEN @HighestLevel  >= 3 -- when at network and actor is central
									THEN wu0.WebUserKey -- owner of account
							END
						WHEN rs.StatusPosition = 3 THEN -- if accept at central, then approved
							wu0.WebUserKey -- owner of account
						ELSE wu0.WebUserKey -- managers webuserkey
					END
				WHEN @Action = 'REJECT' THEN
					CASE --LPF-4027
						WHEN rs.StatusPosition = 3 THEN -- send back down to user responsible for level 2 ownership
							wu1.WebUserKey
						ELSE 
							wu0.WebUserKey -- if at level 1 or 2 then set to the level 1 (base) owner
					END 
				WHEN @Action = 'CLOSE' THEN wu0.WebUserKey -- owner of account
			END,
			CASE
				WHEN r.ItemKey = 1 THEN
					ISNULL('New Discount: ' + CAST(CAST(NewCompoundDiscount*100 AS DEC(24,2)) AS VARCHAR(100)) + '%', 'New Disc: NULL' )
				ELSE
					ISNULL('New Discount: ' + CAST(CAST(NewExceptionDiscount*100  AS DEC(24,2)) AS VARCHAR(100)) + '% ', 'New Disc: NULL ' ) + 
					ISNULL('New Price: £' + CAST(CAST(NewExceptionFixedPrice AS DEC(24,3)) AS VARCHAR(100)), 'New Price: NULL')
			END AS Note
			--ISNULL('Disc: ' + CAST(CAST(NewCompoundDiscount  AS DEC(24,4)) AS VARCHAR(100)) + ' ','Disc: NULL ') +
			--	ISNULL('Price: ' + CAST(CAST(NewExceptionFixedPrice AS DEC(24,4)) AS VARCHAR(100)) + ' ','Price: NULL ') +
			--	ISNULL('Impact: ' +CAST(CAST(AcceptedImpact AS DEC(24,4)) AS VARCHAR(100)) + ' ','Impact: NULL ') +
			--	ISNULL('GPP: ' +CAST(CAST(( np.Price - r.CurrentCost ) / NULLIF(np.Price, 0) AS DEC(24,4)) AS VARCHAR(100)) ,'GPP: NULL')
		--FROM ppe.RequestMain r
		--JOIN dbo.DimItem di ON di.ItemKey = r.ItemKey
		--JOIN (SELECT di.ItemGroup3Key, MAX(di.ItemUDVarChar2 ) ProdBrandAtLLSPG FROM dbo.DimItem di GROUP BY di.ItemGroup3Key) pb ON pb.ItemGroup3Key = r.ItemGroup3Key --LPF-2301
		--JOIN dbo.DimBrand db ON pb.ProdBrandAtLLSPG = db.Brand --LPF-2301
		--LEFT JOIN dbo.DimAccountBrandManager dabm ON dabm.AccountKey = r.AccountKey AND dabm.BrandKey = db.BrandKey  --LPF-2301
		--JOIN ppe.RequestStatus rs ON rs.StatusKey = r.StatusKey
		--JOIN dbo.DimAccount da ON da.AccountKey = r.AccountKey
		--JOIN BridgeAccountManager bam1 ON bam1.SubsidiaryAccountManagerKey = ISNULL(dabm.AccountManagerKey , da.AccountManagerKey)  AND bam1.NumberOfLevels =1
		----JOIN BridgeAccountManager bam2 ON bam2.SubsidiaryAccountManagerKey = ISNULL(dabm.AccountManagerKey , da.AccountManagerKey)  AND bam2.NumberOfLevels =2
		--JOIN BridgeAccountManager bam2 ON bam2.SubsidiaryAccountManagerKey = bam1.SubsidiaryAccountManagerKey AND bam2.NumberOfLevels =2
		--LEFT JOIN WebUser wu0 ON wu0.AccountManagerKey = dabm.AccountManagerKey  --AccountBrandManager
		--INNER JOIN WebUser wu00 ON wu00.AccountManagerKey = da.AccountManagerKey
		--JOIN WebUser wu1 ON wu1.AccountManagerKey = bam1.ParentAccountManagerKey
		--JOIN WebUser wu2 ON wu2.AccountManagerKey = bam2.ParentAccountManagerKey
		--LEFT JOIN WebUserRole wur0 ON wur0.WebUserKey = wu0.WebUserKey  --AccountBrandManager
		--JOIN WebUserRole wur00 ON wur00.WebUserKey = wu00.WebUserKey
		--LEFT JOIN WebRole wr0 ON wr0.WebRoleKey = wur0.WebRoleKey AND wr0.WebRoleName IN ('BranchManager','NetworkManager','CentralPricing')  --AccountBrandManager
		--JOIN WebRole wr00 ON wr00.WebRoleKey = wur00.WebRoleKey AND wr00.WebRoleName IN ('BranchManager','NetworkManager','CentralPricing')
		--LEFT JOIN ppe.RequestStatus rs2 ON rs2.StatusPosition = wr00.WorkflowLevel
		FROM ppe.RequestMain r
		JOIN dbo.DimItem di ON di.ItemKey = r.ItemKey
		JOIN dbo.DimItemGroup3 dig3 ON dig3.ItemGroup3Key = r.ItemGroup3Key
		JOIN dbo.DimBrand db ON db.Brand = dig3.IG3Level5 --LPF-2301
		LEFT JOIN dbo.DimAccountBrandManager dabm ON dabm.AccountKey = r.AccountKey AND dabm.BrandKey = db.BrandKey  --LPF-2301
		JOIN ppe.RequestStatus rs ON rs.StatusKey = r.StatusKey
		JOIN dbo.DimAccount da ON da.AccountKey = r.AccountKey
		JOIN BridgeAccountManager bam1 ON bam1.SubsidiaryAccountManagerKey = ISNULL(dabm.AccountManagerKey , da.AccountManagerKey)  AND bam1.NumberOfLevels =1
		JOIN BridgeAccountManager bam2 ON bam2.SubsidiaryAccountManagerKey = bam1.SubsidiaryAccountManagerKey AND bam2.NumberOfLevels =2

		--JOIN WebUser wu4 ON wu4.WebUserKey = r.OwnerWebUserKey
		--JOIN BridgeAccountManager bam3 ON bam3.ParentAccountManagerKey = wu4.AccountManagerKey AND bam3.SubsidiaryAccountManagerKey = bam1.SubsidiaryAccountManagerKey -- request owner hiearchy
		--JOIN BridgeAccountManager bam4 ON bam4.SubsidiaryAccountManagerKey = bam1.SubsidiaryAccountManagerKey AND bam4.NumberOfLevels = bam3.NumberOfLevels - 1 
		--JOIN WebUser wu5 ON wu5.AccountManagerKey = bam4.ParentAccountManagerKey -- person below request owner in account owner (aka reject recipient)

		JOIN WebUser wu0 ON wu0.AccountManagerKey = bam1.SubsidiaryAccountManagerKey
		JOIN WebUserRole wur0 ON wur0.WebUserKey = wu0.WebUserKey
		JOIN WebRole wr0 ON wr0.WebRoleKey = wur0.WebRoleKey AND wr0.WebRoleName IN ('BranchManager','NetworkManager','CentralPricing')

		JOIN WebUser wu1 ON wu1.AccountManagerKey = bam1.ParentAccountManagerKey
		JOIN WebUser wu2 ON wu2.AccountManagerKey = bam2.ParentAccountManagerKey
		LEFT JOIN ppe.RequestStatus rs2 ON rs2.StatusPosition = wr0.WorkflowLevel
				
		CROSS APPLY ppe.fn_GetSimpleDiscount(r.ProposedExceptionFixedPrice, r.TradePrice, r.ProposedExceptionDiscount, r.ProposedCompoundDiscount) rsd
		CROSS APPLY ppe.fn_GetPrice(r.ProposedExceptionFixedPrice, r.TradePrice, r.ProposedExceptionDiscount, r.ProposedCompoundDiscount) rp
		CROSS APPLY ppe.fn_GetPrice(r.NewExceptionFixedPrice, r.TradePrice, r.NewExceptionDiscount, r.NewCompoundDiscount) np
		CROSS APPLY ppe.fn_GetSimpleDiscount(r.NewExceptionFixedPrice, r.TradePrice, r.NewExceptionDiscount, r.NewCompoundDiscount) ncd
		CROSS APPLY ppe.fn_GetPriceIndex(np.Price, r.RedPrice, r.AmberPrice, r.GreenPrice) npi
		CROSS APPLY ppe.fn_GetDiscount(r.AccountKey, r.ItemGroup3Key, r.ItemKey, r.RedPrice, r.TradePrice, 'R') rd

		LEFT JOIN dbo.ODSPerson p ON r.OwnerWebUserKey = p.ODSPersonKey         --LPF-4552, Don't allow level 2 managers to reject request they own
		LEFT JOIN dbo.DimAccountManager am ON am.AccountManagerKey = da.AccountManagerKey	-- LPF-4552, Don't allow level 2 managers to reject request they own
		LEFT JOIN dbo.DimPerson pp ON pp.DimPersonKey = am.DimPersonKey						-- LPF-4552, Don't allow level 2 managers to reject request they own

		WHERE BatchKey = @BatchKey
		AND r.StatusKey NOT IN (@ClosedStatusKey,@ApprovedStatusKey)
		AND (r.ItemGroup3Key = di.ItemGroup3Key OR r.ItemKey = 1 OR @Action = 'CLOSE' )



		UPDATE r SET StatusKey = 
			CASE 
				WHEN @Action = 'APPROVE' THEN @ApprovedStatusKey
				WHEN @Action = 'ACCEPT' THEN
					CASE 
						WHEN r.NewDeleteTerm = 1 AND rs.StatusPosition <> 0 THEN @ApprovedStatusKey
						WHEN rs.StatusPosition = 0 THEN rs2.StatusKey /* per workflow rules set status to level of account owner.  See LEFT JOIN below */
						WHEN rs.StatusPosition = 1 THEN
							CASE 
								WHEN ncd.Discount <= rsd.Discount -- per LIO-1084 AND ncd.Discount <= rd.Discount
									THEN @ApprovedStatusKey -- happy path
								WHEN @HighestLevel <= 1 -- when at branch and actor is branch or lower
									THEN (SELECT StatusKey FROM ppe.RequestStatus WHERE StatusPosition = rs.StatusPosition + 1 )
								WHEN @HighestLevel  = 2 -- when at branch and actor is network
									THEN (SELECT StatusKey FROM ppe.RequestStatus WHERE StatusPosition = rs.StatusPosition + 2 )
								WHEN @HighestLevel  >= 3 -- when at branch and actor is central
									THEN @ApprovedStatusKey
							END
						WHEN rs.StatusPosition = 2 THEN
							CASE 
								WHEN ncd.Discount <= rsd.Discount OR /* OR vs. and LPF-1192 */ /*OR per LIO-1084*/ ncd.Discount <= rd.Discount
									THEN @ApprovedStatusKey -- happy path
								WHEN @HighestLevel <= 2 -- when at network and actor is network or lower
									THEN (SELECT StatusKey FROM ppe.RequestStatus WHERE StatusPosition = rs.StatusPosition + 1 )
								WHEN @HighestLevel  >= 3 -- when at network and actor is central
									THEN @ApprovedStatusKey
							END
						WHEN rs.StatusPosition = 3 THEN -- if accept at central, then approved
							@ApprovedStatusKey
						ELSE (SELECT StatusKey FROM ppe.RequestStatus 
								WHERE StatusPosition = rs.StatusPosition + 1 )
					END
				WHEN @Action = 'CLOSE' THEN @ClosedStatusKey
				WHEN @Action = 'REJECT' THEN
					CASE 
						WHEN rs.StatusPosition = 1 THEN r.StatusKey
						WHEN r.OwnerWebUserKey = wu0.WebUserKey THEN r.StatusKey --LPF-1326 cannot reject if request owner = account owner
						WHEN  p.LevelId = 'NetworkManager' AND pp.FullName = p.FullName THEN r.StatusKey  --LPF-4552, Don't allow level 2 managers to reject request they own
						ELSE (SELECT StatusKey FROM ppe.RequestStatus WHERE StatusPosition = rs.StatusPosition - 1 )					
					END 
				ELSE r.StatusKey
			END,
			ModificationDate = GETDATE(),
			ModifiedByWebUserKey = @WebUserKey,
			NotificationEmailSentIndicator = 0,
			LastActionKey = CASE 
				WHEN rs.StatusPosition = 0 THEN (SELECT ActionKey FROM ppe.RequestAction WHERE ActionName = 'New Request')
				WHEN @Action = 'APPROVE' THEN (SELECT ActionKey FROM ppe.RequestAction WHERE ActionName = 'Approved')
				WHEN @Action = 'ACCEPT' THEN
					CASE
						WHEN r.NewDeleteTerm = 1 AND rs.StatusPosition <> 0 THEN (SELECT ActionKey FROM ppe.RequestAction WHERE ActionName = 'Approved')
						WHEN rs.StatusPosition = 0 THEN (SELECT ActionKey FROM ppe.RequestAction WHERE ActionName = 'Escalated') 
						WHEN rs.StatusPosition = 1 THEN
							CASE 
								WHEN ncd.Discount <= rsd.Discount -- per LIO-1084 AND ncd.Discount <= rd.Discount
									THEN  (SELECT ActionKey FROM ppe.RequestAction WHERE ActionName = 'Approved') -- happy path
								WHEN @HighestLevel <= 1 -- when at branch and actor is branch or lower
									THEN (SELECT ActionKey FROM ppe.RequestAction WHERE ActionName = 'Escalated') 
								WHEN @HighestLevel  = 2 -- when at branch and actor is network
									THEN (SELECT ActionKey FROM ppe.RequestAction WHERE ActionName = 'Escalated') 
								WHEN @HighestLevel  >= 3 -- when at branch and actor is central
									THEN (SELECT ActionKey FROM ppe.RequestAction WHERE ActionName = 'Approved') 
							END
						WHEN rs.StatusPosition = 2 THEN
							CASE 
								WHEN ncd.Discount <= rsd.Discount OR /* OR vs. and LPF-1192 */ /*OR per LIO-1084*/ ncd.Discount <= rd.Discount
									THEN  (SELECT ActionKey FROM ppe.RequestAction WHERE ActionName = 'Approved') -- happy path
								WHEN @HighestLevel <= 2 -- when at network and actor is network or lower
									THEN  (SELECT ActionKey FROM ppe.RequestAction WHERE ActionName = 'Escalated') 
								WHEN @HighestLevel  >= 3 -- when at network and actor is central
									THEN (SELECT ActionKey FROM ppe.RequestAction WHERE ActionName = 'Approved') 
							END
						WHEN rs.StatusPosition = 3 THEN -- if accept at central, then approved
							(SELECT ActionKey FROM ppe.RequestAction WHERE ActionName = 'Approved') 
						ELSE  (SELECT ActionKey FROM ppe.RequestAction WHERE ActionName = 'Escalated') 
					END
				WHEN @Action = 'CLOSE' THEN (SELECT ActionKey FROM ppe.RequestAction WHERE ActionName = 'Closed') 	
				WHEN @Action = 'REJECT' THEN
					CASE 
						WHEN rs.StatusPosition = 1 THEN r.LastActionKey
						ELSE (SELECT ActionKey FROM ppe.RequestAction WHERE ActionName = 'Rejected') 			
					END 
				ELSE r.LastActionKey
			END,
			PriorOwnerWebUserKey = CASE 
				WHEN @Action = 'APPROVE' THEN r.OwnerWebUserKey
				WHEN @Action = 'ACCEPT' THEN r.OwnerWebUserKey -- on acceptance, current becomes prior 
				WHEN @Action = 'REJECT' THEN r.OwnerWebUserKey -- owner of account
				WHEN @Action = 'CLOSE' THEN r.OwnerWebUserKey -- owner of account
			END,
			OwnerWebUserKey = CASE 
				WHEN @Action = 'APPROVE' THEN wu0.WebUserKey
				WHEN @Action = 'ACCEPT' THEN --wu2.WebUserKey -- managers webuserkey
					CASE
						WHEN r.NewDeleteTerm = 1 AND rs.StatusPosition <> 0 THEN wu0.WebUserKey -- owner of account
						WHEN rs.StatusPosition = 0 THEN wu0.WebUserKey
						WHEN rs.StatusPosition = 1 THEN
							CASE 
								WHEN ncd.Discount <= rsd.Discount -- per LIO-1084 AND ncd.Discount <= rd.Discount
									THEN wu0.WebUserKey  -- owner of account -- happy path
								WHEN @HighestLevel <= 1 -- when at branch and actor is branch or lower
									THEN wu1.WebUserKey -- managers webuserkey
								WHEN @HighestLevel  = 2 -- when at branch and actor is network
									THEN  wu2.WebUserKey -- manager's manager webuserkey
								WHEN @HighestLevel  >= 3 -- when at branch and actor is central
									THEN wu0.WebUserKey -- owner of account
							END
						WHEN rs.StatusPosition = 2 AND wr0.WebRoleName = 'BranchManager' THEN
							CASE 
								WHEN ncd.Discount <= rsd.Discount OR /* OR vs. and LPF-1192 */ /*OR per LIO-1084*/ ncd.Discount <= rd.Discount
									THEN wu0.WebUserKey  -- owner of account -- happy path
								WHEN @HighestLevel <= 2 -- when at network and actor is network or lower
									THEN wu2.WebUserKey -- manager's manager webuserkey
								WHEN @HighestLevel  >= 3 -- when at network and actor is central
									THEN wu0.WebUserKey -- owner of account
							END
						WHEN rs.StatusPosition = 2 AND wr0.WebRoleName = 'NetworkManager' THEN
							CASE 
								WHEN ncd.Discount <= rsd.Discount OR /* OR vs. and LPF-1192 */ /*OR per LIO-1084*/ ncd.Discount <= rd.Discount
									THEN wu0.WebUserKey  -- owner of account -- happy path
								WHEN @HighestLevel <= 1 -- when at branch and actor is branch or lower
									THEN wu1.WebUserKey -- managers webuserkey
								WHEN @HighestLevel <= 2 -- when at network and actor is network or lower
									THEN wu1.WebUserKey -- manager's webuserkey
								WHEN @HighestLevel  >= 3 -- when at network and actor is central
									THEN wu0.WebUserKey -- owner of account
							END
						WHEN rs.StatusPosition = 3 THEN -- if accept at central, then approved
							wu0.WebUserKey -- owner of account
						ELSE wu0.WebUserKey -- managers webuserkey
					END 
				WHEN @Action = 'REJECT' THEN 
					CASE --LPF-4027
						WHEN rs.StatusPosition = 3 THEN -- send back down to user responsible for level 2 ownership
							wu1.WebUserKey
						ELSE 
							wu0.WebUserKey -- if at level 1 or 2 then set to the level 1 (base) owner
					END 
				WHEN @Action = 'CLOSE' THEN wu0.WebUserKey -- owner of account
			END,
		NewSPGFromDate = 
			CASE /*LIO-683*/
				WHEN r.ItemKey = 1 AND ISNULL(NewSPGFromDate, '1-1-1900')  < DATEADD(d, 1, CAST(GETDATE() AS DATE)) THEN DATEADD(d, 1, CAST(GETDATE() AS DATE))
				ELSE NewSPGFromDate 
			END
			
		--FROM ppe.RequestMain r
		--JOIN dbo.DimItem di ON di.ItemKey = r.ItemKey
		--JOIN (SELECT di.ItemGroup3Key, MAX(di.ItemUDVarChar2 ) ProdBrandAtLLSPG FROM dbo.DimItem di GROUP BY di.ItemGroup3Key) pb ON pb.ItemGroup3Key = r.ItemGroup3Key --LPF-2301
		--JOIN dbo.DimBrand db ON pb.ProdBrandAtLLSPG = db.Brand --LPF-2301
		--LEFT JOIN dbo.DimAccountBrandManager dabm ON dabm.AccountKey = r.AccountKey AND dabm.BrandKey = db.BrandKey  --LPF-2301
		--JOIN ppe.RequestStatus rs ON rs.StatusKey = r.StatusKey
		--JOIN dbo.DimAccount da ON da.AccountKey = r.AccountKey
		--JOIN BridgeAccountManager bam1 ON bam1.SubsidiaryAccountManagerKey = ISNULL(dabm.AccountManagerKey , da.AccountManagerKey)  AND bam1.NumberOfLevels =1
		----JOIN BridgeAccountManager bam2 ON bam2.SubsidiaryAccountManagerKey = ISNULL(dabm.AccountManagerKey , da.AccountManagerKey)  AND bam2.NumberOfLevels =2
		--JOIN BridgeAccountManager bam2 ON bam2.SubsidiaryAccountManagerKey = bam1.SubsidiaryAccountManagerKey AND bam2.NumberOfLevels =2
		--LEFT JOIN WebUser wu0 ON wu0.AccountManagerKey = dabm.AccountManagerKey  --AccountBrandManager
		--INNER JOIN WebUser wu00 ON wu00.AccountManagerKey = da.AccountManagerKey
		--JOIN WebUser wu1 ON wu1.AccountManagerKey = bam1.ParentAccountManagerKey
		--JOIN WebUser wu2 ON wu2.AccountManagerKey = bam2.ParentAccountManagerKey
		--LEFT JOIN WebUserRole wur0 ON wur0.WebUserKey = wu0.WebUserKey  --AccountBrandManager
		--JOIN WebUserRole wur00 ON wur00.WebUserKey = wu00.WebUserKey
		--LEFT JOIN WebRole wr0 ON wr0.WebRoleKey = wur0.WebRoleKey AND wr0.WebRoleName IN ('BranchManager','NetworkManager','CentralPricing')  --AccountBrandManager
		--JOIN WebRole wr00 ON wr00.WebRoleKey = wur00.WebRoleKey AND wr00.WebRoleName IN ('BranchManager','NetworkManager','CentralPricing')
		--LEFT JOIN ppe.RequestStatus rs2 ON rs2.StatusPosition = wr00.WorkflowLevel

		FROM ppe.RequestMain r
		JOIN dbo.DimItem di ON di.ItemKey = r.ItemKey
		JOIN dbo.DimItemGroup3 dig3 ON dig3.ItemGroup3Key = r.ItemGroup3Key
		JOIN dbo.DimBrand db ON db.Brand = dig3.IG3Level5 --LPF-2301
		LEFT JOIN dbo.DimAccountBrandManager dabm ON dabm.AccountKey = r.AccountKey AND dabm.BrandKey = db.BrandKey  --LPF-2301
		JOIN ppe.RequestStatus rs ON rs.StatusKey = r.StatusKey
		JOIN dbo.DimAccount da ON da.AccountKey = r.AccountKey
		JOIN BridgeAccountManager bam1 ON bam1.SubsidiaryAccountManagerKey = ISNULL(dabm.AccountManagerKey , da.AccountManagerKey)  AND bam1.NumberOfLevels =1
		JOIN BridgeAccountManager bam2 ON bam2.SubsidiaryAccountManagerKey = bam1.SubsidiaryAccountManagerKey AND bam2.NumberOfLevels =2

		--JOIN WebUser wu4 ON wu4.WebUserKey = r.OwnerWebUserKey
		--JOIN BridgeAccountManager bam3 ON bam3.ParentAccountManagerKey = wu4.AccountManagerKey AND bam3.SubsidiaryAccountManagerKey = bam1.SubsidiaryAccountManagerKey -- request owner hiearchy
		--JOIN BridgeAccountManager bam4 ON bam4.SubsidiaryAccountManagerKey = bam1.SubsidiaryAccountManagerKey AND bam4.NumberOfLevels = CASE bam3.NumberOfLevels WHEN 0 THEN 0 ELSE bam3.NumberOfLevels - 1  END  --LPF-2621 if at bottom of tree, don't go any lower
		--JOIN WebUser wu5 ON wu5.AccountManagerKey = bam4.ParentAccountManagerKey -- person below request owner in account owner (aka reject recipient)

		JOIN WebUser wu0 ON wu0.AccountManagerKey = bam1.SubsidiaryAccountManagerKey
		JOIN WebUserRole wur0 ON wur0.WebUserKey = wu0.WebUserKey
		JOIN WebRole wr0 ON wr0.WebRoleKey = wur0.WebRoleKey AND wr0.WebRoleName IN ('BranchManager','NetworkManager','CentralPricing')

		JOIN WebUser wu1 ON wu1.AccountManagerKey = bam1.ParentAccountManagerKey
		JOIN WebUser wu2 ON wu2.AccountManagerKey = bam2.ParentAccountManagerKey
		LEFT JOIN ppe.RequestStatus rs2 ON rs2.StatusPosition = wr0.WorkflowLevel

		CROSS APPLY ppe.fn_GetSimpleDiscount(r.ProposedExceptionFixedPrice, r.TradePrice, r.ProposedExceptionDiscount, r.ProposedCompoundDiscount) rsd
		CROSS APPLY ppe.fn_GetPrice(r.ProposedExceptionFixedPrice, r.TradePrice, r.ProposedExceptionDiscount, r.ProposedCompoundDiscount) rp
		CROSS APPLY ppe.fn_GetPrice(r.NewExceptionFixedPrice, r.TradePrice, r.NewExceptionDiscount, r.NewCompoundDiscount) np
		CROSS APPLY ppe.fn_GetSimpleDiscount(r.NewExceptionFixedPrice, r.TradePrice, r.NewExceptionDiscount, r.NewCompoundDiscount) ncd
		CROSS APPLY ppe.fn_GetPriceIndex(np.Price, r.RedPrice, r.AmberPrice, r.GreenPrice) npi
		CROSS APPLY ppe.fn_GetDiscount(r.AccountKey, r.ItemGroup3Key, r.ItemKey, r.RedPrice, r.TradePrice, 'R') rd
		LEFT JOIN dbo.ODSPerson p ON r.OwnerWebUserKey = p.ODSPersonKey         --LPF-4552, Don't allow level 2 managers to reject request they own
		LEFT JOIN dbo.DimAccountManager am ON am.AccountManagerKey = da.AccountManagerKey	-- LPF-4552, Don't allow level 2 managers to reject request they own
		LEFT JOIN dbo.DimPerson pp ON pp.DimPersonKey = am.DimPersonKey						-- LPF-4552, Don't allow level 2 managers to reject request they own
		WHERE BatchKey = @BatchKey
		AND r.StatusKey NOT IN (@ClosedStatusKey,@ApprovedStatusKey)
		AND (r.ItemGroup3Key = di.ItemGroup3Key OR r.ItemKey = 1 OR @Action = 'CLOSE' )

		SELECT @CountUpdated = @@ROWCOUNT


		IF @Action = 'ACCEPT' OR @Action = 'APPROVE' --LPF-1775 move collision logic from RequestMain trigger to this procedure.  Requires disabling of RequestMain.tIII_RequestMain trigger.
		BEGIN

			UPDATE old SET StatusKey = @ClosedStatusKey
			FROM ppe.RequestMain old 
			INNER JOIN ppe.RequestStatus oldstatus ON old.StatusKey = oldstatus.StatusKey
			INNER JOIN ppe.RequestMain new 
				ON old.AccountKey = new.AccountKey 
				AND	old.ItemGroup3Key = new.ItemGroup3Key 
				AND	old.ItemKey = new.ItemKey
				AND old.BatchKey <> new.BatchKey
				AND new.BatchKey = @BatchKey
			INNER JOIN ppe.RequestStatus newstatus ON new.StatusKey = newstatus.StatusKey
			WHERE oldstatus.StatusRollup = 'Pending'
			AND newstatus.StatusRollup IN ('Pending','Approved')

		END -- end LPF-1775

		SELECT 
			NEWID() AS GUID,
			@CountInBatch AS CountInBatch,
			@CountUpdated AS CountUpdated,
			@SPG AS SPG,
			@ItemGroup3Key AS ItemGroup3Key,
			@SPGDesc AS SPGDescription
	END



END


GO
