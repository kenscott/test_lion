SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [sup].[GetSpCurrentMonth]
    @claimMonth INT,
    @vendorKey INT
AS

SET NOCOUNT ON;
SET ANSI_WARNINGS OFF ;

SELECT NEWID() AS GUID,
	   cj.ClaimMonth,
       COUNT(DISTINCT cj.CurrentContractID) AS contracts,
       COUNT(*) AS pendingClaimLines,
       SUM(   CASE cj.ClaimType
                  WHEN 'A' THEN
                      1
                  WHEN 'W' THEN
                      1
                  ELSE
                      0
              END
          ) AS adjClaimLines,
       SUM(cj.ClaimLineValue) AS pendingTotal
FROM cm.ClaimJournal cj
WHERE cj.VendorKey = @vendorKey
      AND ClaimMonth = @claimMonth
      AND cj.ClaimJournalStatus IN ( 'P', 'C' )
GROUP BY cj.ClaimMonth,
         cj.VendorKey;
GO
