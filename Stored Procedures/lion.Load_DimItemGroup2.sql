SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE PROCEDURE [lion].[Load_DimItemGroup2]
AS
SET NOCOUNT ON

EXEC LogDCPEvent 'ETL - Load_DimItemGroup2', 'B'

----This table will contain a row for every row in the dimension table that has been changed in the staging table (type 1 update will be done)
--CREATE TABLE #UpdateKeys (KeyID INTEGER)

DECLARE @RowsI Quantity_Normal_type, @RowsU Quantity_Normal_type, @RowsD Quantity_Normal_type
SET @RowsI = 0
SET @RowsU = 0
SET @RowsD = 0

EXEC DBA_IndexRebuild 'DimItemGroup2', 'Drop'


--TRUNCATE TABLE DimItemGroup2

--This is used to look up the date key in the DimDate table
DECLARE @TodayDayKey Key_Small_Type
SET @TodayDayKey = (SELECT DayKey FROM DimDay WHERE CONVERT(VARCHAR(10), SQLDate, 120) = CONVERT(VARCHAR(10), GETDATE(), 120))

IF NOT EXISTS (SELECT 1 FROM DimItemGroup2 WHERE IG2Level1 = 'Unknown')
BEGIN
	INSERT DimItemGroup2 (
		IG2Level1, 
		IG2Level2, 
		IG2Level3, 
		IG2Level4, 
		IG2Level5, 
		IG2Level1UDVarchar1, 
		CreationDayKey, 
		ModificationDayKey)
	VALUES (
		'Unknown',
		'Unknown',
		'Unknown',
		'Unknown',
		'Unknown',
		'Unknown',
		@TodayDayKey,
		@TodayDayKey)
	SET @RowsI = @RowsI + @@RowCount
END


INSERT DimItemGroup2 (
	IG2Level1,
	IG2Level2,
	IG2Level3,
	IG2Level4,
	IG2Level5, 
	IG2Level1UDVarchar1,
	CreationDayKey,
	ModificationDayKey)
SELECT DISTINCT
	p.ProdBrandAtLLSPG AS ProdBrandAtLLSPG,
	'Unknown',
	'Unknown',
	'Unknown',
	'Unknown',
	ProdBrandAtLLSPG AS ProdBrandAtLLSPGDescription,
	@TodayDayKey,
	@TodayDayKey
FROM lion_Staging.dbo.Product p
WHERE 
	p.ETLRowStatusCodeKey = 0 
	AND p.ProdBrandAtLLSPG NOT IN (SELECT IG2Level1 FROM DimItemGroup2)
ORDER BY p.ProdBrandAtLLSPG
SET @RowsI = @RowsI + @@RowCount

EXEC DBA_IndexRebuild 'DimItemGroup2', 'Create'

EXEC LogDCPEvent 'ETL - Load_DimItemGroup2', 'E', @RowsI, @RowsU, @RowsD

SELECT TOP 10 * FROM DimItemGroup2

GO
