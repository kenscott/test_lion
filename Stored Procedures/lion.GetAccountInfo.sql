SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE [lion].[GetAccountInfo]
    @Account Description_Small_type
AS 

SET NOCOUNT ON

select a.AccountNumber as [Account Number],
       a. AccountName as [Account Name],
	   a.Branch, 
	   a.BranchName as [Branch Name],
	   a.Area, 
	   a.AreaDescription as [Area Description], 
	   a.AccountManagerFullName as [Account Owner],
       a.AccountManagerEmail as [Account Owner Email],
       CASE WHEN (select count(*) from lion.vwAccount ia where ia.AccountNumberParent = a.AccountNumber) > 1 THEN 'Y' ELSE 'N' END AS [Has Children?], 
       DateOpened as [Date Opened],
       FixedPriceIndicator as [Fixed Price Indicator]
from lion.vwAccount a
where a.AccountNumber = @Account


GO
