SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE [ppe].[Get_SalesSummary] (@RequestKey INT) AS
BEGIN
	SET ANSI_WARNINGS OFF
	SET ARITHABORT OFF
	SET ARITHIGNORE ON
		
	--DECLARE @RequestKey INT
	--SELECT @RequestKey = 2169

	DECLARE @AccountKey INT, @ItemGroup3Key INT , @ItemKey INT

	SELECT 
		@AccountKey =AccountKey, 
		@ItemGroup3Key=ItemGroup3Key ,
		@ItemKey = ItemKey
	FROM ppe.RequestMain WHERE RequestKey = @RequestKey


	SELECT 
		NEWID() AS GUID,

		di.ItemClientUniqueIdentifier AS Product, 
		di.ItemDescription, 

		SUM(TotalActualPrice) AS TotalSales,

		ISNULL((SUM(TotalActualPrice) - SUM(TotalActualCost)) / SUM(TotalActualPrice),0) AS TotalGPP,

		SUM(CASE WHEN dilg2.InvoiceLineGroup2UDVarchar5 = '1 - Terms Price Used' THEN TotalActualPrice ELSE 0 END) AS TermsSales,

		ISNULL((SUM(CASE WHEN dilg2.InvoiceLineGroup2UDVarchar5 = '1 - Terms Price Used' THEN TotalActualPrice ELSE 0 END) - 
			SUM(CASE WHEN dilg2.InvoiceLineGroup2UDVarchar5 = '1 - Terms Price Used' THEN TotalActualCost ELSE 0 END)) /
				SUM(CASE WHEN dilg2.InvoiceLineGroup2UDVarchar5 = '1 - Terms Price Used' THEN TotalActualPrice ELSE 0 END),0) AS TermsGPP,

		SUM(CASE WHEN dilg2.InvoiceLineGroup2UDVarchar5 = '2 - Default Contract Used' THEN TotalActualPrice ELSE 0 END) AS MatrixSales,

		ISNULL((SUM(CASE WHEN dilg2.InvoiceLineGroup2UDVarchar5 = '2 - Default Contract Used' THEN TotalActualPrice ELSE 0 END) - 
			SUM(CASE WHEN dilg2.InvoiceLineGroup2UDVarchar5 = '2 - Default Contract Used' THEN TotalActualCost ELSE 0 END)) /
				SUM(CASE WHEN dilg2.InvoiceLineGroup2UDVarchar5 = '2 - Default Contract Used' THEN TotalActualPrice ELSE 0 END),0) AS MatrixGPP,

		SUM(CASE WHEN dilg2.InvoiceLineGroup2UDVarchar5 = '3 - Price Guidance Used' THEN TotalActualPrice ELSE 0 END) AS PGSales,

		ISNULL((SUM(CASE WHEN dilg2.InvoiceLineGroup2UDVarchar5 = '3 - Price Guidance Used' THEN TotalActualPrice ELSE 0 END) - 
			SUM(CASE WHEN dilg2.InvoiceLineGroup2UDVarchar5 = '3 - Price Guidance Used' THEN TotalActualCost ELSE 0 END)) /
				SUM(CASE WHEN dilg2.InvoiceLineGroup2UDVarchar5 = '3 - Price Guidance Used' THEN TotalActualPrice ELSE 0 END),0) AS PGGPP,

		SUM(CASE WHEN dilg2.InvoiceLineGroup2UDVarchar5 = '4 - Manually Priced' THEN TotalActualPrice ELSE 0 END) AS ManualSales,

		ISNULL((SUM(CASE WHEN dilg2.InvoiceLineGroup2UDVarchar5 = '4 - Manually Priced' THEN TotalActualPrice ELSE 0 END) - 
			SUM(CASE WHEN dilg2.InvoiceLineGroup2UDVarchar5 = '4 - Manually Priced' THEN TotalActualCost ELSE 0 END)) /
				SUM(CASE WHEN dilg2.InvoiceLineGroup2UDVarchar5 = '4 - Manually Priced' THEN TotalActualPrice ELSE 0 END),0) AS ManualGPP,

		MAX(dd.SQLDate) AS LastSaleDate,  -- use MAX per LPF-1179
		SUM(CASE WHEN fil.UDDecimal2 > 0 THEN fil.TotalActualPrice ELSE 0 END) AS TotalContractClaimSales , --LPF-2900
		SUM(CASE WHEN fil.UDDecimal2 = 0 THEN fil.TotalActualPrice ELSE 0 END) AS TotalNonContractClaimSales   --LPF-2900
	FROM dbo.FactInvoiceLine fil
	INNER JOIN dbo.DimItem di ON di.ItemKey = fil.ItemKey
	INNER JOIN dbo.DimInvoiceLineGroup2 dilg2 ON dilg2.InvoiceLineGroup2Key = fil.InvoiceLineGroup2Key
	INNER JOIN dbo.FactLastPriceAndCost flpc 
		ON flpc.AccountKey = fil.AccountKey
		AND flpc.InvoiceLineGroup1Key = fil.InvoiceLineGroup1Key
		AND flpc.ItemKey = fil.ItemKey
	INNER JOIN dbo.DimDay dd ON dd.DayKey = flpc.LastSaleDayKey
	WHERE fil.Last12MonthsIndicator = N'Y'
	AND fil.AccountKey = @AccountKey
	AND di.ItemGroup3Key = @ItemGroup3Key
	AND (fil.ItemKey = @ItemKey OR @ItemKey = 1)
	GROUP BY 
		di.ItemClientUniqueIdentifier, 
		--dd.SQLDate, per LPF-1179
		di.ItemDescription


END
GO
