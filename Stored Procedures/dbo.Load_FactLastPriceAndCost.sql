SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO






CREATE PROCEDURE [dbo].[Load_FactLastPriceAndCost]
AS

SET NOCOUNT ON
SET QUOTED_IDENTIFIER ON 
SET ANSI_NULLS ON 
SET ARITHABORT ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
SET CONCAT_NULL_YIELDS_NULL ON
SET NUMERIC_ROUNDABORT OFF

EXEC LogDCPEvent 'ETL - dbo.FactLastPriceAndCost -    #LastAISAccountSales', 'B'

DECLARE 
	@RowsI Quantity_Normal_type, 
	@RowsU Quantity_Normal_type, 
	@RowsD Quantity_Normal_type,
	@TodayDayKey Key_Small_Type,
	@DefaultInvoiceLineGroup2Key Key_Normal_Type,
	@UnknownJunkKey Key_Normal_type

	
SET @RowsI = 0
SET @RowsU = 0
SET @RowsD = 0

SET @UnknownJunkKey = (SELECT InvoiceLineJunkKey FROM dbo.DimInvoiceLineJunk WHERE InvoiceLineJunkUDVarchar1 = 'Unknown')
SET @DefaultInvoiceLineGroup2Key = (SELECT InvoiceLineGroup2Key FROM dbo.DimInvoiceLineGroup2 WHERE InvoiceLineGroup2UDVarchar1 = 'N')	-- http://enterbridgedc:8080/browse/BPB-356

EXEC DBA_IndexRebuild 'FactLastPriceAndCost', 'Drop'


--TRUNCATE TABLE dbo.FactLastPriceAndCost
DELETE FROM dbo.FactLastPriceAndCost
SET @RowsD = @RowsD + @@RowCount


SELECT 
	AccountKey, 
	SUM(TotalActualPrice) AS TotalAccountSales,
	COUNT(DISTINCT ItemKey) AS TotalNumberOfItemSold
INTO #LastAISAccountSales
FROM dbo.FactInvoiceLine
WHERE Last12MonthsIndicator = 'Y'
GROUP BY AccountKey

CREATE  UNIQUE CLUSTERED INDEX I_TAS_1 ON #LastAISAccountSales(AccountKey)

EXEC LogDCPEvent 'ETL - dbo.FactLastPriceAndCost -    #LastAISAccountSales', 'E', 0,0,0


EXEC LogDCPEvent 'ETL - dbo.FactLastPriceAndCost -    #AccountItemSales', 'B'

SELECT
	AccountItemTotals.AccountKey,
	AccountItemTotals.ItemKey,
	CASE TotalAccountSales
		WHEN 0.0 THEN 0.0
		ELSE TotalAccountItemSales/TotalAccountSales
	END AS 'PercentAccountItemSalesToAccountSales'
INTO #AccountItemSales
FROM 
	(SELECT 
		AccountKey, 
		ItemKey, 
		SUM(TotalActualPrice) AS TotalAccountItemSales
	FROM dbo.FactInvoiceLine
	WHERE Last12MonthsIndicator = 'Y'
	GROUP BY 
		AccountKey, 
		ItemKey
	) AccountItemTotals
JOIN #LastAISAccountSales AccountTotals ON 
	AccountItemTotals.AccountKey = AccountTotals.AccountKey

CREATE  UNIQUE CLUSTERED INDEX I_AIS_1 ON #AccountItemSales(AccountKey, ItemKey)

EXEC LogDCPEvent 'ETL - dbo.FactLastPriceAndCost -    #AccountItemSales', 'E', 0,0,0



EXEC LogDCPEvent 'ETL - dbo.FactLastPriceAndCost -    #LastAISTotals', 'B'

SELECT 
	AccountKey, 
	ItemKey, 
	InvoiceLineGroup1Key, 
	SUM(TotalActualPrice) AS TotalAISSales, 
	SUM(TotalActualCost) AS TotalAISCost, 
	SUM(TotalQuantity) AS TotalAISQuantity, 
	AVG(TotalQuantity) AS AverageQuantityPerOrder, 
	COUNT(*) AS Total12MonthInvoiceLines, 
	COUNT(DISTINCT ClientInvoiceID) AS Total12MonthInvoices
INTO #LastAISTotals
FROM dbo.FactInvoiceLine
WHERE Last12MonthsIndicator = 'Y'
GROUP BY 
	AccountKey, 
	ItemKey, 
	InvoiceLineGroup1Key

CREATE  CLUSTERED INDEX I_TAIS_1 ON #LastAISTotals (AccountKey, TotalAISSales DESC)

EXEC LogDCPEvent 'ETL - dbo.FactLastPriceAndCost -    #LastAISTotals', 'E', 0,0,0


EXEC LogDCPEvent 'ETL - dbo.FactLastPriceAndCost -    #AISTRiskRanking', 'B'

SELECT 
	AccountKey, 
	ItemKey, 
	InvoiceLineGroup1Key,
	(SELECT COUNT(*) FROM #LastAISTotals T2 WHERE T2.AccountKey = T.AccountKey AND T2.TotalAISSales >= T.TotalAISSales) AS 'RowRank',
	(SELECT COUNT(*) FROM #LastAISTotals T2 WHERE T2.AccountKey = T.AccountKey) AS AccountCount
INTO #AISTRiskRanking
FROM #LastAISTotals T

CREATE UNIQUE CLUSTERED INDEX I_TARR_1 ON #AISTRiskRanking (AccountKey, ItemKey, InvoiceLineGroup1Key)
EXEC LogDCPEvent 'ETL - dbo.FactLastPriceAndCost -    #AISTRiskRanking', 'E', 0,0,0


EXEC LogDCPEvent 'ETL - dbo.FactLastPriceAndCost -    #LikeItemCodeSales', 'B'

SELECT
	Account_LIC_ILG1Totals.AccountKey,
	Account_LIC_ILG1Totals.LikeItemCode,
	Account_LIC_ILG1Totals.InvoiceLineGroup1Key,
	CASE TotalAccountSales
		WHEN 0.0 THEN 0.0
		ELSE LikeItemCodeTotalSales/TotalAccountSales
	END AS 'PercentLikeItemCodeToTotalAccountSales',
	LikeItemCodeTotalInvoiceLines,
	LikeItemCodeTotalSales
INTO #LikeItemCodeSales
FROM 
	(
	SELECT 
		AccountKey, 
		LikeItemCode, 
		InvoiceLineGroup1Key, 
		SUM(TotalActualPrice) AS LikeItemCodeTotalSales, 
		COUNT(*) AS LikeItemCodeTotalInvoiceLines
	FROM dbo.FactInvoiceLine FIL
	JOIN dbo.DimItem DI ON 
		DI.ItemKEy = FIL.ItemKey
	WHERE Last12MonthsIndicator = 'Y'
	GROUP BY 
		AccountKey, 
		LikeItemCode, 
		InvoiceLineGroup1Key
	) Account_LIC_ILG1Totals
JOIN #LastAISAccountSales AccountTotals ON 
	Account_LIC_ILG1Totals.AccountKey = AccountTotals.AccountKey

CREATE CLUSTERED INDEX I_LICS_1 ON #LikeItemCodeSales(AccountKey,LikeItemCode,InvoiceLineGroup1Key)

EXEC LogDCPEvent 'ETL - dbo.FactLastPriceAndCost -    #LikeItemCodeSales', 'E'



EXEC LogDCPEvent 'ETL - dbo.FactLastPriceAndCost -    #LastAIS', 'B'


---Now, find the last invoiceline for each of these:
CREATE TABLE #LastAIS 
(
	RowID INT IDENTITY(1,1), 
	AccountKey INT, 
	ItemKey INT, 
	InvoiceLineGroup1Key INT, 
	LastItemPrice DECIMAL(19,8), 
	LastItemCost DECIMAL(19,8), 
	LastItemRebate DECIMAL(19,8), 
	LastQuantity DECIMAL(19,8), 
	LastItemDiscountPercent DECIMAL(19,8),
	LastInvoiceLineGroup2Key INT, 
	LastInvoiceLineGroup3Key INT, 
	LastInvoiceDateDayKey INT,
	ClientInvoiceID VARCHAR(50) NOT NULL, 
	ClientInvoiceLineID VARCHAR(50) NOT NULL,
	InvoiceLineJunkKey INT
)

INSERT INTO #LastAIS
(
	AccountKey, 
	ItemKey, 
	InvoiceLineGroup1Key, 
	LastItemPrice, 
	LastItemCost, 
	LastItemRebate, 
	LastQuantity, 
	LastItemDiscountPercent,
	LastInvoiceLineGroup2Key, 
	LastInvoiceLineGroup3Key, 
	LastInvoiceDateDayKey,
	ClientInvoiceID,
	ClientInvoiceLineID,
	InvoiceLineJunkKey
)
SELECT
	FIL.AccountKey, 
	FIL.ItemKey, 
	FIL.InvoiceLineGroup1Key,	
	ISNULL(FIL.TotalActualPrice/NULLIF(FIL.TotalQuantity, 0.0), 0.0) AS LastItemPrice,
	ISNULL(FIL.TotalActualCost/NULLIF(FIL.TotalQuantity, 0.0), 0.0) AS LastItemCost,
	ISNULL(FIL.TotalActualRebate/NULLIF(FIL.TotalQuantity, 0.0), 0.0) AS LastItemRebate,
	TotalQuantity AS LastQuantity,
	ISNULL(	(UnitListPrice - (FIL.TotalActualPrice/NULLIF(FIL.TotalQuantity, 0.0))) / NULLIF(UnitListPrice, 0.), 0.) AS LastItemDiscountPercent,
	InvoiceLineGroup2Key AS LastInvoiceLineGroup2Key,
	InvoiceLineGroup3Key AS LastInvoiceLineGroup3Key,
	InvoiceDateDayKey AS LastInvoiceDateDayKey,
	FIL.ClientInvoiceID,
	FIL.InvoiceLineUniqueIdentifier,
	FIL.InvoiceLineJunkKey
FROM dbo.FactInvoiceLine FIL 
WHERE Last12MonthsIndicator = 'Y'
-- WHERE 
-- 	TotalQuantity > 0.0 AND 
-- 	TotalActualPrice > 0.0 AND 
-- 	TotalActualCost > 0.0
ORDER BY 
	FIL.AccountKey, 
	FIL.ItemKey, 
	FIL.InvoiceLineGroup1Key, 
	FIL.InvoiceDateDayKey DESC, 
	FIL.TotalQuantity DESC, 
	FIL.TotalActualPrice DESC,
	FIL.ClientInvoiceID DESC,
	FIL.InvoiceLineUniqueIdentifier DESC	

--Get rid of a few thousand rows that fall outside a valid GP%
--DELETE FROM #LastAIS WHERE ((LastItemPrice-LastItemCost)/LastItemPrice) NOT BETWEEN -1.2 AND 0.90

CREATE UNIQUE CLUSTERED INDEX I_T_1 ON #LastAIS (RowID, AccountKey, ItemKey, InvoiceLineGroup1Key)
CREATE INDEX I_T_2 ON #LastAIS (AccountKey, ItemKey)

EXEC LogDCPEvent 'ETL - dbo.FactLastPriceAndCost -    #LastAIS', 'E',0,0,0


EXEC LogDCPEvent 'ETL - dbo.FactLastPriceAndCost - Insert', 'B'

--Insert for any rows that we found a last price/cost for.  If we did not find one, we'll insert zeros here instead.
INSERT INTO dbo.FactLastPriceAndCost
(
	FactInvoiceLine.AccountKey, 
	FactInvoiceLine.ItemKey, 
	FactInvoiceLine.InvoiceLineGroup1Key, 
	LastSaleDayKey, 
	LastItemPrice, 
	LastItemCost,
	LastItemRebate,
	LastQuantity,
	LastInvoiceLineGroup2Key,
	LastInvoiceLineGroup3Key,
	AverageQuantityPerOrder,
	PercentAccountItemSalesToAccountSales,
	LikeItemCodePercent,
	Total12MonthInvoices,
	Total12MonthInvoiceLines,
	Total12MonthSales,
	Total12MonthCost,
	Total12MonthQuantity,	
	AISToAccountSalesRowRanking,
	LICS.LikeItemCodeTotalInvoiceLines,
	LICS.LikeItemCodeTotalSales,
	LastClientInvoiceID,
	LastClientInvoiceLineID,
	LastInvoiceLineJunkKey,
	UDVarchar7,
	UDVarchar8,
	UDDecimal2,
	LastItemDiscountPercent
)
SELECT
	T.AccountKey,
	T.ItemKey, 
	T.InvoiceLineGroup1Key, 
	T.LastInvoiceDateDayKey, 
	T.LastItemPrice, 
	T.LastItemCost,
	T.LastItemRebate,
	T.LastQuantity,
	T.LastInvoiceLineGroup2Key,
	T.LastInvoiceLineGroup3Key,
	AIST.AverageQuantityPerOrder,
	AIS.PercentAccountItemSalesToAccountSales,
	LICS.PercentLikeItemCodeToTotalAccountSales AS 'LikeItemCodePercent',
	AIST.Total12MonthInvoices,
	AIST.Total12MonthInvoiceLines,
	AIST.TotalAISSales AS Total12MonthSales,
	AIST.TotalAISCost AS Total12MonthCost,
	AIST.TotalAISQuantity AS Total12MonthQuantity,
	CASE AISTRR.AccountCount 
		WHEN 0.0 THEN 0.0
		ELSE CAST(AISTRR.RowRank AS DECIMAL(19,8))/CAST(AISTRR.AccountCount AS DECIMAL(19,8)) 
	END AS 'AISToAccountSalesRowRanking',
	LICS.LikeItemCodeTotalInvoiceLines,
	LICS.LikeItemCodeTotalSales,
	ClientInvoiceID,
	ClientInvoiceLineID,
	ISNULL(InvoiceLineJunkKey, @UnknownJunkKey),
	'N' AS UDVarchar7,
	'N'AS UDVarchar8,		-- covered by a rebate; now set later in load_factrebate
	NULL AS UDDecimal2,		-- RebatedCommissionCost; now set later in load_factrebate
	T.LastItemDiscountPercent AS LastItemDiscountPercent
FROM #LastAIS T
JOIN (
	SELECT MIN(RowID) AS MinRowID
	FROM #LastAIS
	WHERE 	((LastItemPrice-LastItemCost)/LastItemPrice) BETWEEN -1.2 AND 0.90 AND 
	 	LastQuantity > 0.0 AND 
 		LastItemPrice > 0.0 AND 
	 	LastItemCost > 0.0
	GROUP BY AccountKey, ItemKey, InvoiceLineGroup1Key
	) LastID ON
	LastID.MinRowID = T.RowID
JOIN dbo.DimItem DI ON 
	DI.ItemKey = T.ItemKey
JOIN #LastAISTotals AIST ON 
	AIST.AccountKey = T.AccountKey AND 
	AIST.ItemKey = T.ItemKey AND 
	AIST.InvoiceLineGroup1Key = T.InvoiceLineGroup1Key
JOIN #AccountItemSales AIS ON 
	AIS.AccountKey = T.AccountKey AND 
	AIS.ItemKey = T.ItemKey
JOIN #AISTRiskRanking AISTRR ON 
	AISTRR.AccountKey = T.ACcountKey AND 
	AISTRR.ItemKey = T.ItemKey AND 
	AISTRR.InvoiceLineGroup1Key = T.InvoiceLineGroup1Key
LEFT JOIN #LikeItemCodeSales LICS ON 
	LICS.AccountKey = T.AccountKey AND 
	LICS.LikeItemCode = DI.LikeItemCode AND 
	LICS.InvoiceLineGroup1Key = T.InvoiceLineGroup1Key
--LEFT JOIN dbo.FactRebate
--	ON FactRebate.AccountKey = t.AccountKey
--	AND FactRebate.ItemKey = t.ItemKey
SET @RowsI = @RowsI + @@RowCount

EXEC LogDCPEvent 'ETL - dbo.FactLastPriceAndCost - Insert', 'E',  @RowsI, @RowsU, @RowsD

SET @RowsI = 0
SET @RowsU = 0
SET @RowsD = 0


SELECT TOP 10 * FROM dbo.FactLastPriceAndCost

EXEC LogDCPEvent 'ETL - dbo.FactLastPriceAndCost - Indexes Create', 'B'

EXEC DBA_IndexRebuild 'FactLastPriceAndCost', 'Create'

EXEC LogDCPEvent 'ETL - dbo.FactLastPriceAndCost - Indexes Create', 'E',0,0,0



-- EXEC LogDCPEvent 'ETL - dbo.FactLastPriceAndCost - Missing AIS Insert', 'B'
-- 
-- CREATE TABLE #LastAISMissing
-- (
-- 	RowID INT IDENTITY(1,1), 
-- 	AccountKey INT, 
-- 	ItemKey INT, 
-- 	InvoiceLineGroup1Key INT, 
-- 	LastInvoiceLineGroup2Key INT, 
-- 	LastInvoiceDateDayKey INT,
-- 	ClientInvoiceID VARCHAR(50) NOT NULL, 
-- 	ClientInvoiceLineID VARCHAR(50) NOT NULL
-- )
-- 
-- INSERT INTO #LastAISMissing
-- (
-- 	AccountKey, 
-- 	ItemKey, 
-- 	InvoiceLineGroup1Key, 
-- 	LastInvoiceLineGroup2Key, 
-- 	LastInvoiceDateDayKey,
-- 	ClientInvoiceID,
-- 	ClientInvoiceLineID
-- )
-- SELECT
-- 	FIL.AccountKey, 
-- 	FIL.ItemKey, 
-- 	FIL.InvoiceLineGroup1Key,	
-- 	FIL.InvoiceLineGroup2Key AS LastInvoiceLineGroup2Key,
-- 	FIL.InvoiceDateDayKey AS LastInvoiceDateDayKey,
-- 	FIL.ClientInvoiceID,
-- 	FIL.ClientInvoiceLineID
-- FROM dbo.FactInvoiceLine FIL 
-- LEFT JOIN FactLastPriceAndCost FLPC 
-- 	ON FIL.AccountKey = FLPC.AccountKey 
-- 	AND FIL.ItemKey = FLPC.ItemKey 
-- 	AND FIL.InvoiceLineGroup1Key = FLPC.InvoiceLineGroup1Key
-- WHERE 
-- 	FLPC.AccountKey IS NULL 	-- i.e. the ones without a last price and cost record (could have been because price, cost, & qty were zero perhaps of bad GP%
-- ORDER BY 
-- 	FIL.AccountKey, 
-- 	FIL.ItemKey, 
-- 	FIL.InvoiceLineGroup1Key, 
-- 	FIL.InvoiceDateDayKey DESC, 
-- 	FIL.ClientInvoiceID DESC,
-- 	FIL.ClientInvoiceLineID DESC	
-- 
-- EXEC LogDCPEvent 'ETL - dbo.FactLastPriceAndCost - Missing AIS Insert', 'E', 0, 0, 0
-- 
-- 
-- EXEC LogDCPEvent 'ETL - dbo.FactLastPriceAndCost - Missing AIS Insert - index create', 'B',
-- CREATE UNIQUE CLUSTERED INDEX I_T_2 ON #LastAISMissing (RowID, AccountKey, ItemKey, InvoiceLineGroup1Key)
-- 
-- EXEC LogDCPEvent 'ETL - dbo.FactLastPriceAndCost - Missing AIS Insert - index create', 'E',  0, 0, 0

EXEC LogDCPEvent 'ETL - dbo.FactLastPriceAndCost - Missing AIS Insert - fact table insert', 'B'
INSERT INTO FactLastPriceAndCost
(
	AccountKey, 
	ItemKey, 
	InvoiceLineGroup1Key, 
	LastSaleDayKey, 
	LastItemPrice, 
	LastItemCost, 
	LastQuantity, 
	LastItemRebate, 
	LastInvoiceLineGroup2Key,
	LastInvoiceLineGroup3Key, 
	AverageQuantityPerOrder, 
	PercentAccountItemSalesToAccountSales, 
	LikeItemCodePercent, 
	Total12MonthInvoices, 
	Total12MonthInvoiceLines,
	Total12MonthSales,
	Total12MonthCost,
	Total12MonthQuantity,
	LastClientInvoiceID,
	LastClientInvoiceLineID,
	LastInvoiceLineJunkKey,
	UDVarchar7,
	UDVarchar8,
	UDDecimal2,
	LastItemDiscountPercent
)
SELECT 
	T.AccountKey, 
	T.ItemKey, 
	T.InvoiceLineGroup1Key, 
	T.LastInvoiceDateDayKey, 
	0, 				-- LastItemPrice
	0, 				-- LastItemCost
	0, 				-- LastQuantity
	0, 				-- LastItemRebate
	LastInvoiceLineGroup2Key,	-- http://enterbridgedc:8080/browse/BPB-356 
	LastInvoiceLineGroup3Key,
	0, 				-- AverageQuantityPerOrder
	0, 				-- PercentAccountItemSalesToAccountSales
	0, 				-- LikeItemCodePercent
	0, 				-- Total12MonthInvoices
	0, 				-- Total12MonthInvoiceLines
	0,				-- Total12MonthSales,
	0,				-- Total12MonthCost,
	0,				-- Total12MonthQuantity,	
	ClientInvoiceID,
	ClientInvoiceLineID,
	@UnknownJunkKey,
	'N' AS UDVarchar7,
	'N' AS UDVarchar8,		-- covered by a rebate; now set later in load_factrebate
	NULL,					-- RebatedCommissionCost; now set later in load_factrebate
	0. AS LastItemDiscountPercent
FROM #LastAIS T
JOIN (
	-- get min RowId where we did not already insert
	SELECT MIN(RowID) AS MinRowID
	FROM #LastAIS L
	LEFT JOIN FactLastPriceAndCost FLPC 
		ON L.AccountKey = FLPC.AccountKey 
		AND L.ItemKey = FLPC.ItemKey 
		AND L.InvoiceLineGroup1Key = FLPC.InvoiceLineGroup1Key
	WHERE
		FLPC.AccountKey IS NULL
	GROUP BY L.AccountKey, L.ItemKey, L.InvoiceLineGroup1Key
	) LastID ON
	LastID.MinRowID = T.RowID
--LEFT JOIN dbo.FactRebate
--	ON FactRebate.AccountKey = t.AccountKey
--	AND FactRebate.ItemKey = t.ItemKey	
SET @RowsI = @RowsI + @@RowCount


UPDATE flpac
SET 
	UDDecimal1 = a.TotalAccountSales,
	UDDecimal3 = TotalNumberOfItemSold
FROM dbo.FactLastPriceAndCost flpac
INNER JOIN #LastAISAccountSales a
	ON a.AccountKey = flpac.AccountKey

UPDATE flpac
	SET UDVarchar9 = 
		CASE
			WHEN UDDecimal1 >= 25000.00000000 AND UDDecimal1 < 999999999.00000000 THEN 'A'
			WHEN UDDecimal1 >= 1000.00000000 AND UDDecimal1 < 25000.00000000 THEN 'B'
			WHEN UDDecimal1 >= -999999999.00000000 AND UDDecimal1 < 1000.00000000 THEN 'C'
			ELSE NULL
		END
FROM dbo.FactLastPriceAndCost flpac


--SELECT TOP 1000 * FROM FactLastPriceAndCost WHERE uddecimal1 IS NOT NULL
; WITH TheAverageQuantityPerInvoiceLineValuesByIS AS (
	SELECT DISTINCT 
		ItemKey,
		InvoiceLineGroup1Key,
		AverageQuantityPerOrder -- really average quanty per INVOICE LINE
	FROM dbo.FactLastPriceAndCost flpac
), AverageQuantityPerInvoiceLineByISTile AS 
(
	SELECT
		ItemKey, 
		InvoiceLineGroup1Key,
		AverageQuantityPerOrder, -- really average quanty per INVOICE LINE,
		NTILE(3) OVER (PARTITION BY ItemKey, InvoiceLineGroup1Key ORDER BY AverageQuantityPerOrder ) AS Tile
	FROM TheAverageQuantityPerInvoiceLineValuesByIS
)

UPDATE flpac
	SET 
		UDVarchar1 = 
			CASE
				WHEN AverageQuantityPerInvoiceLineByISTile.Tile = 1 THEN 'Low'
				WHEN AverageQuantityPerInvoiceLineByISTile.Tile = 2 THEN 'Med'
				WHEN AverageQuantityPerInvoiceLineByISTile.Tile = 3 THEN 'High'
				ELSE '???'
			END
FROM dbo.FactLastPriceAndCost flpac
INNER JOIN AverageQuantityPerInvoiceLineByISTile 
	ON AverageQuantityPerInvoiceLineByISTile.ItemKey = flpac.ItemKey
	AND AverageQuantityPerInvoiceLineByISTile.InvoiceLineGroup1Key = flpac.InvoiceLineGroup1Key
	AND AverageQuantityPerInvoiceLineByISTile.AverageQuantityPerOrder = flpac.AverageQuantityPerOrder

--SELECT TOP 10 UDVarchar1 FROM dbo.FactLastPriceAndCost WHERE  UDVarchar1 IS NOT NULL

--UPDATE dbo.FactLastPriceAndCost SET UDVarchar2 = NULL, UDVarchar3 = NULL

--UDDecimal1 = total account sales
; 
WITH TheTotalSalesCustomerValues AS (
	SELECT DISTINCT 
		UDDecimal1 AS TotalSalesCustomer
	FROM dbo.FactLastPriceAndCost
	WHERE UDDecimal1 IS NOT NULL
	AND UDDecimal1 <> 0.0
), TotalSalesCustomerTile AS (
	SELECT
		TotalSalesCustomer,
		NTILE(3) OVER (ORDER BY TotalSalesCustomer ) AS Tile
	FROM TheTotalSalesCustomerValues
)
UPDATE flpac
	SET 
		UDVarchar2 = 
			CASE
				WHEN TotalSalesCustomerTile.Tile = 1 THEN 'Low'
				WHEN TotalSalesCustomerTile.Tile = 2 THEN 'Med'
				WHEN TotalSalesCustomerTile.Tile = 3 THEN 'High'
				ELSE '???'
			END
FROM dbo.FactLastPriceAndCost flpac
INNER JOIN TotalSalesCustomerTile 
	ON TotalSalesCustomerTile.TotalSalesCustomer = flpac.UDDecimal1

--''New' Sales: if total item sales for Jan-Sep 2008 = 0 then flag the 2009 'New' Sales = Y. In other words if there are total item sales > 0 in 2008 then 'New' flag = N
;
WITH SalesSummary AS (
	SELECT
		AccountKey,
		ItemKey,
		InvoiceLineGroup1Key,
		SUM(CASE WHEN MonthSequenceNumber IN (1, 2, 3) THEN TotalActualPrice ELSE 0.0 END) AS RecentSales,
		SUM(CASE WHEN MonthSequenceNumber NOT IN (1, 2, 3) THEN TotalActualPrice ELSE 0.0 END) AS PreviousSales
	FROM dbo.FactInvoiceLine fil
	INNER JOIN DimMonth dm ON
		dm.MonthKey = fil.InvoiceDateMonthKey
	GROUP BY 
		AccountKey,
		ItemKey,
		InvoiceLineGroup1Key
)
UPDATE flpac
	SET UDVarchar3 = 
		ISNULL(
			CASE
				WHEN PreviousSales = 0.0 AND RecentSales > 0.0 THEN 'Y'
				ELSE 'N'
			END, 
			'N')
FROM dbo.FactLastPriceAndCost flpac
INNER JOIN SalesSummary s 
	ON s.AccountKey = flpac.AccountKey
	AND s.ItemKey = flpac.ItemKey
	AND s.InvoiceLineGroup1Key = flpac.InvoiceLineGroup1Key

--
--SELECT TOP 10 * FROM dbo.FactLastPriceAndCost WHERE UDVarchar3 IS NULL
--AND AverageQuantityPerOrder <> 0.0
--



-- UDVarchar4 frequency bucket
; 
WITH TheFrequencyValues AS (
	SELECT DISTINCT 
		ItemKey,
		InvoiceLineGroup1Key,
		Total12MonthInvoiceLines AS Frequency
	FROM dbo.FactLastPriceAndCost
	WHERE Total12MonthInvoiceLines IS NOT NULL
	AND Total12MonthInvoiceLines <> 0.0
), FrequencyTile AS (
	SELECT
		ItemKey, 
		InvoiceLineGroup1Key,
		Frequency,
		NTILE(3) OVER (PARTITION BY ItemKey, InvoiceLineGroup1Key ORDER BY Frequency ) AS Tile
	FROM TheFrequencyValues
)
UPDATE flpac
	SET 
		UDVarchar4 = 
			CASE
				WHEN FrequencyTile.Tile = 1 THEN 'Low'
				WHEN FrequencyTile.Tile = 2 THEN 'Med'
				WHEN FrequencyTile.Tile = 3 THEN 'High'
				ELSE '???'
			END
FROM dbo.FactLastPriceAndCost flpac
INNER JOIN FrequencyTile 
	ON FrequencyTile.ItemKey = flpac.ItemKey
	AND FrequencyTile.InvoiceLineGroup1Key = flpac.InvoiceLineGroup1Key
	AND FrequencyTile.Frequency = flpac.Total12MonthInvoiceLines


---- Strategic Item Indicator INDUSTRIAL
--/*
--UDDecimal1 = a.TotalAccountSales
--FROM dbo.FactLastPriceAndCost flpac
--INNER JOIN #LastAISAccountSales a
--	ON a.AccountKey = flpac.AccountKey
--*/


--SELECT
--	a.AccountKey,
--	VendorKey,
--	ProductLine,
--	ClassCode,
--	SubClassCode,
--	CASE
--		WHEN ISNULL(TotalSales/NULLIF(TotalAccountSales, 0.0), 0.0) > 0.10 THEN 'Y'
--		ELSE 'N'
--	END AS StrategicIndicator
--INTO #StrategicIndustrial
--FROM (
--	SELECT
--		AccountKey,
--		VendorKey,
--		ItemUDVarchar10 AS ProductLine,
--		ItemUDVarchar11 AS ClassCode,
--		ItemUDVarchar13 AS SubClassCode,
--		SUM(TotalActualPrice) AS TotalSales
--	FROM dbo.FactInvoiceLine fil
--	INNER JOIN dbo.DimItem di
--		ON di.ItemKey = fil.ItemKey
--	WHERE Last12MonthsIndicator = 'Y'
--	GROUP BY 
--		AccountKey,
--		VendorKey,
--		ItemUDVarchar10,
--		ItemUDVarchar11,
--		ItemUDVarchar13
--) a
--JOIN #LastAISAccountSales AccountTotals ON 
--	a.AccountKey = AccountTotals.AccountKey

--SELECT
--	a.AccountKey,
--	VendorKey,
--	ProductLine,
--	ClassCode,
--	SubClassCode,
--	VendorGrade,
--	CASE
--		WHEN ISNULL(TotalSales/NULLIF(TotalAccountSales, 0.0), 0.0) > 0.10 THEN 'Y'
--		ELSE 'N'
--	END AS StrategicIndicator
--INTO #StrategicPrint
--FROM (
--	SELECT
--		AccountKey,
--		VendorKey,
--		ItemUDVarchar10 AS ProductLine,
--		ItemUDVarchar11 AS ClassCode,
--		ItemUDVarchar13 AS SubClassCode,
--		ItemUDVarchar15 AS VendorGrade,
--		SUM(TotalActualPrice) AS TotalSales
--	FROM dbo.FactInvoiceLine fil
--	INNER JOIN dbo.DimItem di
--		ON di.ItemKey = fil.ItemKey
--	WHERE Last12MonthsIndicator = 'Y'
--	GROUP BY 
--		AccountKey,
--		VendorKey,
--		ItemUDVarchar10,
--		ItemUDVarchar11,
--		ItemUDVarchar13,
--		ItemUDVarchar15
--) a
--JOIN #LastAISAccountSales AccountTotals ON 
--	a.AccountKey = AccountTotals.AccountKey

--CREATE  UNIQUE CLUSTERED INDEX I_1 ON #StrategicIndustrial (AccountKey,VendorKey,ProductLine,ClassCode,SubClassCode)
--CREATE  UNIQUE CLUSTERED INDEX I_1 ON #StrategicPrint      (AccountKey,VendorKey,ProductLine,ClassCode,SubClassCode,VendorGrade)

--UPDATE flpac
--	SET 
--		UDVarchar5 = ISNULL(StrategicIndicator, 'N')
--FROM dbo.FactLastPriceAndCost flpac
--INNER JOIN dbo.DimItem di ON
--	di.ItemKey = flpac.ItemKey
--LEFT JOIN #StrategicIndustrial s
--	ON flpac.AccountKey = s.AccountKey
--	AND di.ItemVendorKey = s.VendorKey
--	AND di.ItemUDVarchar10 = s.ProductLine
--	AND di.ItemUDVarchar11 = s.ClassCode
--	AND di.ItemUDVarchar13 = s.SubClassCode

--UPDATE flpac
--	SET 
--		UDVarchar6 = ISNULL(StrategicIndicator, 'N')
--FROM dbo.FactLastPriceAndCost flpac
--INNER JOIN dbo.DimItem di ON
--	di.ItemKey = flpac.ItemKey
--LEFT JOIN #StrategicPrint s
--	ON flpac.AccountKey = s.AccountKey
--	AND di.ItemVendorKey = s.VendorKey
--	AND di.ItemUDVarchar10 = s.ProductLine
--	AND di.ItemUDVarchar11 = s.ClassCode
--	AND di.ItemUDVarchar13 = s.SubClassCode
--	AND di.ItemUDVarchar15 = s.VendorGrade



--UPDATE flpac
--	SET 
--		flpac.UDVarchar7 =	-- a CDI Indicator 
--			CASE
--				WHEN LEFT(di.ItemDescription, 1) = '@' THEN 'Y'
--				WHEN StockStatus IS NULL OR CDIFlag IS NULL THEN 'N'
--				WHEN StockStatus = 'CS' OR StockStatus = 'CA'
--				  OR CDIFlag = 'B' OR CDIFlag = 'C' OR CDIFlag = 'P' THEN 'Y'
--				ELSE
--					'N'
--			END,
--			flpac.AccountGroup3ItemKey = pag3i.AccountGroup3ItemKey
--FROM dbo.FactLastPriceAndCost flpac
--INNER JOIN dbo.DimAccount da 
--	ON da.AccountKey = flpac.AccountKey
--INNER JOIN dbo.DimItem di
--	ON di.ItemKey = flpac.ItemKey
--LEFT JOIN dbo.PlaybookAccountGroup3Item pag3i
--	ON pag3i.AccountGroup3Key = da.FirstAlternateAccountGroup3Key
--	AND pag3i.ItemKey = flpac.ItemKey

	

/***
UDVarchar8 ALLREADY SET
***/

DROP TABLE #LastAISAccountSales
DROP TABLE #AccountItemSales
DROP TABLE #LastAISTotals
DROP TABLE #AISTRiskRanking
DROP TABLE #LikeItemCodeSales
DROP TABLE #LastAIS
--DROP TABLE #StrategicIndustrial
--DROP TABLE #StrategicPrint



EXEC LogDCPEvent 'ETL - dbo.FactLastPriceAndCost - Missing AIS Insert - fact table insert', 'E',  @RowsI, @RowsU, @RowsD
--EXEC LogDCPEvent 'ETL - dbo.FactLastPriceAndCost - Missing AIS Insert', 'E',  @RowsI, @RowsU, @RowsD











GO
