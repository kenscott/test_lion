SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [cm].[GetActiveClaimMonths]

AS

SELECT DISTINCT ClaimMonth
FROM cm.ClaimJournal (nolock)
WHERE SupplierClaimKey IS NULL
	AND ClaimJournalStatus IN ( 'P', 'R' )  --Pending or Review
ORDER BY ClaimMonth 
GO
