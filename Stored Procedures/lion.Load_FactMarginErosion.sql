SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [lion].[Load_FactMarginErosion]
AS

SET NOCOUNT ON;

EXEC LogDCPEvent 'ETL - lion.Load_FactMarginErosion', 'B';


IF OBJECT_ID('tempdb.dbo.#AISDistinctGPP', 'U') IS NOT NULL
    DROP TABLE #AISDistinctGPP;
IF OBJECT_ID('tempdb.dbo.#CDFTable', 'U') IS NOT NULL
    DROP TABLE #CDFTable;
IF OBJECT_ID('tempdb.dbo.#SustainedRow', 'U') IS NOT NULL
    DROP TABLE #SustainedRow;
IF OBJECT_ID('tempdb.dbo.#SustainedData', 'U') IS NOT NULL
    DROP TABLE #SustainedData;
IF OBJECT_ID('tempdb.dbo.#LastAIS', 'U') IS NOT NULL
    DROP TABLE #LastAIS;
IF OBJECT_ID('tempdb.dbo.#TotalData', 'U') IS NOT NULL
    DROP TABLE #TotalData;
IF OBJECT_ID('tempdb.dbo.#AccountTotals', 'U') IS NOT NULL
    DROP TABLE #AccountTotals;
IF OBJECT_ID('tempdb.dbo.#AccountItemTotals', 'U') IS NOT NULL
    DROP TABLE #AccountItemTotals;
IF OBJECT_ID('tempdb.dbo.#ItemMedians', 'U') IS NOT NULL
    DROP TABLE #ItemMedians;


DECLARE @RowsI Quantity_Normal_type ,
    @RowsU Quantity_Normal_type ,
    @RowsD Quantity_Normal_type ,
    @UnknownDayKey dbo.Key_Small_Type ,
    @MinInvoiceDateDayKey INT ,
    @MaxInvoiceDateDayKey INT ,
    @AvgDistributorGPP dbo.Percent_Type ,

	-- ME --
    @SMEMinGPP dbo.Percent_Type ,
    @SMEMaxGPP dbo.Percent_Type ,
    @MaxDayThreshold INT ,
    @SalesPercentThreshold DECIMAL(19, 8) ,
    @TotalInvoiceLinesPercentThreshold DECIMAL(19, 8) ,
    @GPPFrequencyThreshold dbo.Percent_Type ,
	
	-- PLS --
    @PLSDaysWithNoSales dbo.Int_Type ,
    @PLSFrequency dbo.Int_Type ,
    @PLSMinGP DECIMAL(38, 8)

SET @RowsI = 0;
SET @RowsU = 0;
SET @RowsD = 0;
SET @UnknownDayKey = ( SELECT   DayKey
                       FROM     dbo.DimDay
                       WHERE    SQLDate IS NULL
                     );

SELECT @PLSDaysWithNoSales = ParamValue FROM dbo.Param WHERE ParamName = 'PLSDaysWithNoSales';
SELECT @PLSFrequency = ParamValue FROM dbo.Param WHERE ParamName = 'PLSFrequency';
SELECT @PLSMinGP = ParamValue FROM dbo.Param WHERE ParamName = 'PLSMinGP';

/* ME Parameters */;
/* the following are traditionally playbook parameters */;
SELECT @MaxDayThreshold = ParamValue FROM dbo.Param WHERE ParamName = 'MaxDayThreshold';
SELECT  @SMEMinGPP = ParamValue FROM dbo.Param WHERE ParamName = 'SMEMinGPP';
SELECT  @SMEMaxGPP = ParamValue FROM dbo.Param WHERE ParamName = 'SMEMaxGPP';

SELECT  @SalesPercentThreshold = ParamValue FROM dbo.Param WHERE ParamName = 'SalesPercentThreshold';
SELECT  @TotalInvoiceLinesPercentThreshold = ParamValue FROM dbo.Param WHERE ParamName = 'TotalInvoiceLinesPercentThreshold';
SELECT  @GPPFrequencyThreshold = ParamValue FROM dbo.Param WHERE ParamName = 'GPPFrequencyThreshold';


SET @MaxInvoiceDateDayKey = ( SELECT    MAX(InvoiceDateDayKey)
                              FROM      dbo.FactInvoiceLine
                            );
SET @MinInvoiceDateDayKey = @MaxInvoiceDateDayKey - @MaxDayThreshold;



EXEC LogDCPEvent 'ETL - lion.Load_FactMarginErosion: Determining ME - Step 1',
    'B';
SET @RowsI = 0;
SET @RowsU = 0;
SET @RowsD = 0;

/*** ME - Margin Erosion ***/;
CREATE TABLE #AISDistinctGPP
    (
      RowID INT IDENTITY(1, 1) ,
      AccountKey INT ,
      ItemKey INT ,
      InvoiceLineGroup1Key INT ,
      PriceDerivation VARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS ,
      GPP DEC(19, 8) ,
      TotalPrice DEC(19, 8) ,
      TotalInvoiceLines INT ,
      MaxInvoiceLineUniqueIdentifier INT
    )




INSERT  #AISDistinctGPP
        ( AccountKey ,
          ItemKey ,
          InvoiceLineGroup1Key ,
          PriceDerivation ,
          GPP ,
          TotalPrice ,
          TotalInvoiceLines ,
          MaxInvoiceLineUniqueIdentifier
        )
        SELECT  AccountKey ,
                ItemKey ,
                InvoiceLineGroup1Key ,
                UDVarchar1 ,
                ( TotalActualPrice - TotalActualCost ) / TotalActualPrice AS Expr1 ,
                SUM(TotalActualPrice) AS Expr2 ,
                COUNT(*) AS Expr3 ,
                MAX(InvoiceLineUniqueIdentifier) AS MaxInvoiceLineUniqueIdentifier
        FROM    FactInvoiceLine AS fil
        WHERE   ( InvoiceDateDayKey BETWEEN @MinInvoiceDateDayKey
                                    AND     @MaxInvoiceDateDayKey )
                AND ( TotalActualPrice > 0.0 )
                AND ( TotalActualCost > 0.0 )
                AND ( TotalQuantity > 0.0 )
                AND ( ( TotalActualPrice - TotalActualCost )
                      / TotalActualPrice BETWEEN @SMEMinGPP
                                         AND     @SMEMaxGPP )
        GROUP BY AccountKey ,
                ItemKey ,
                InvoiceLineGroup1Key ,
                UDVarchar1 ,
                ( TotalActualPrice - TotalActualCost ) / TotalActualPrice
        ORDER BY AccountKey ,
                ItemKey ,
                InvoiceLineGroup1Key ,
                UDVarchar1 ,
                Expr1 ,
                MaxInvoiceLineUniqueIdentifier;
SET @RowsI = @@RowCount;

CREATE UNIQUE CLUSTERED INDEX I_AISDistinctGPP_1 ON #AISDistinctGPP (AccountKey, ItemKey, InvoiceLineGroup1Key,PriceDerivation, RowID, GPP);

EXEC LogDCPEvent 'ETL - lion.Load_FactMarginErosion: Determining ME - Step 1',
    'E', @RowsI, @RowsU, @RowsD;


EXEC LogDCPEvent 'ETL - lion.Load_FactMarginErosion: Determining ME - Step 2',
    'B';
SET @RowsI = 0;
SET @RowsU = 0;
SET @RowsD = 0;

SELECT  T1.* ,
        ( SELECT    SUM(T2.TotalPrice)
          FROM      #AISDistinctGPP T2
          WHERE     T2.RowID >= T1.RowID
                    AND T1.AccountKey = T2.AccountKey
                    AND T1.ItemKey = T2.ItemKey
                    AND T1.InvoiceLineGroup1Key = T2.InvoiceLineGroup1Key
                    AND T1.PriceDerivation = T2.PriceDerivation
        ) / ( SELECT    SUM(T2.TotalPrice)
              FROM      #AISDistinctGPP T2
              WHERE     T1.AccountKey = T2.AccountKey
                        AND T1.ItemKey = T2.ItemKey
                        AND T1.InvoiceLineGroup1Key = T2.InvoiceLineGroup1Key
                        AND T1.PriceDerivation = T2.PriceDerivation
            ) AS TotalPriceCDF ,
        ( SELECT    CAST(SUM(T2.TotalInvoiceLines) AS DEC(19, 8))
          FROM      #AISDistinctGPP T2
          WHERE     T2.RowID >= T1.RowID
                    AND T1.AccountKey = T2.AccountKey
                    AND T1.ItemKey = T2.ItemKey
                    AND T1.InvoiceLineGroup1Key = T2.InvoiceLineGroup1Key
                    AND T1.PriceDerivation = T2.PriceDerivation
        ) / ( SELECT    CAST(SUM(T2.TotalInvoiceLines) AS DEC(19, 8))
              FROM      #AISDistinctGPP T2
              WHERE     T1.AccountKey = T2.AccountKey
                        AND T1.ItemKey = T2.ItemKey
                        AND T1.InvoiceLineGroup1Key = T2.InvoiceLineGroup1Key
                        AND T1.PriceDerivation = T2.PriceDerivation
            ) AS TotalInvoiceLinesCDF ,
        ( SELECT    CAST(SUM(T2.TotalInvoiceLines) AS DEC(19, 8))
          FROM      #AISDistinctGPP T2
          WHERE     T1.GPP = T2.GPP
                    AND T1.AccountKey = T2.AccountKey
                    AND T1.ItemKey = T2.ItemKey
                    AND T1.InvoiceLineGroup1Key = T2.InvoiceLineGroup1Key
                    AND T1.PriceDerivation = T2.PriceDerivation
        ) / ( SELECT    CAST(SUM(T2.TotalInvoiceLines) AS DEC(19, 8))
              FROM      #AISDistinctGPP T2
              WHERE     T1.AccountKey = T2.AccountKey
                        AND T1.ItemKey = T2.ItemKey
                        AND T1.InvoiceLineGroup1Key = T2.InvoiceLineGroup1Key
                        AND T1.PriceDerivation = T2.PriceDerivation
            ) AS GPPFrequency
INTO    #CDFTable
FROM    #AISDistinctGPP T1;
SET @RowsI = @RowsI + @@RowCount;

CREATE UNIQUE CLUSTERED INDEX I_CDFTable_1 ON #CDFTable (AccountKey, ItemKey, InvoiceLineGroup1Key, PriceDerivation, RowID, GPP);

EXEC LogDCPEvent 'ETL - lion.Load_FactMarginErosion: Determining ME - Step 2',
    'E', @RowsI, @RowsU, @RowsD;


EXEC LogDCPEvent 'ETL - lion.Load_FactMarginErosion: Determining ME - Step 3',
    'B';
SET @RowsI = 0;
SET @RowsU = 0;
SET @RowsD = 0;

SELECT  AccountKey ,
        ItemKey ,
        InvoiceLineGroup1Key ,
        PriceDerivation ,
        MAX(RowID) AS SustainedRowID
INTO    #SustainedRow
FROM    #CDFTable
WHERE   TotalPriceCDF > @SalesPercentThreshold
        AND TotalInvoiceLinesCDF > @TotalInvoiceLinesPercentThreshold
        AND GPPFrequency > @GPPFrequencyThreshold
GROUP BY AccountKey ,
        ItemKey ,
        InvoiceLineGroup1Key ,
        PriceDerivation;
SET @RowsI = @RowsI + @@RowCount;

CREATE UNIQUE CLUSTERED INDEX I_SustainedRow_1 ON #SustainedRow (AccountKey, ItemKey, InvoiceLineGroup1Key, PriceDerivation);

EXEC LogDCPEvent 'ETL - lion.Load_FactMarginErosion: Determining ME - Step 3',
    'E', @RowsI, @RowsU, @RowsD;


EXEC LogDCPEvent 'ETL - lion.Load_FactMarginErosion: Determining ME - Step 4',
    'B';
SET @RowsI = 0;
SET @RowsU = 0;
SET @RowsD = 0;

SELECT  T.AccountKey ,
        T.ItemKey ,
        T.InvoiceLineGroup1Key ,
        T.PriceDerivation ,
        T.GPP AS SustainedGPP ,
        ISNULL(fil.TotalActualPrice / NULLIF(fil.TotalQuantity, 0.0), 0.0) AS UnitPriceAtSustained ,
        ISNULL(fil.TotalActualCost / NULLIF(fil.TotalQuantity, 0.0), 0.0) AS UnitCostAtSustained
INTO    #SustainedData
FROM    #SustainedRow SR
        JOIN #AISDistinctGPP T ON SR.AccountKey = T.AccountKey
                                  AND SR.ItemKey = T.ItemKey
                                  AND SR.InvoiceLineGroup1Key = T.InvoiceLineGroup1Key
                                  AND SR.PriceDerivation = T.PriceDerivation
        JOIN dbo.FactInvoiceLine fil ON fil.InvoiceLineUniqueIdentifier = T.MaxInvoiceLineUniqueIdentifier  --************************************************************************************************
WHERE   SR.SustainedRowID = T.RowID;
SET @RowsI = @RowsI + @@RowCount;
EXEC LogDCPEvent 'ETL - lion.Load_FactMarginErosion: Determining ME - Step 4',
    'E', @RowsI, @RowsU, @RowsD;


EXEC LogDCPEvent 'ETL - lion.Load_FactMarginErosion: Determining Lasts',
    'B';
SET @RowsI = 0;
SET @RowsU = 0;
SET @RowsD = 0;

WITH    Lines
          AS ( SELECT   AccountKey ,
                        ItemKey ,
                        InvoiceLineGroup1Key ,
                        UDVarchar1 AS PriceDerivation ,
                        CASE WHEN TotalNetQuantity IS NOT NULL
                                  AND TotalNetPrice IS NOT NULL
                                  AND TotalNetCost IS NOT NULL
                             THEN TotalNetQuantity
                             ELSE TotalQuantity
                        END AS TotalQuantity ,
                        CASE WHEN TotalNetQuantity IS NOT NULL
                                  AND TotalNetPrice IS NOT NULL
                                  AND TotalNetCost IS NOT NULL
                             THEN ISNULL(TotalNetPrice
                                         / NULLIF(TotalNetQuantity, 0.0), 0.0)
                             ELSE ISNULL(TotalActualPrice
                                         / NULLIF(TotalQuantity, 0.0), 0.0)
                        END AS UnitPrice ,
                        CASE WHEN TotalNetQuantity IS NOT NULL
                                  AND TotalNetPrice IS NOT NULL
                                  AND TotalNetCost IS NOT NULL
                             THEN ISNULL(TotalNetCost
                                         / NULLIF(TotalNetQuantity, 0.0), 0.0)
                             ELSE ISNULL(TotalActualCost
                                         / NULLIF(TotalQuantity, 0.0), 0.0)
                        END AS UnitCost ,
                        CASE WHEN TotalNetQuantity IS NOT NULL
                                  AND TotalNetPrice IS NOT NULL
                                  AND TotalNetCost IS NOT NULL
                             THEN ISNULL(( UnitListPrice - ( TotalNetPrice
                                                             / NULLIF(TotalNetQuantity,
                                                              0.0) ) )
                                         / NULLIF(UnitListPrice, 0.), 0.)
                             ELSE ISNULL(( UnitListPrice - ( TotalActualPrice
                                                             / NULLIF(TotalQuantity,
                                                              0.0) ) )
                                         / NULLIF(UnitListPrice, 0.), 0.)
                        END AS DiscountPercent ,
                        UnitListPrice ,
                        ISNULL(UDDecimal3 / NULLIF(TotalQuantity, 0.0), 0.0) AS UnitRetailPrice ,
                        ISNULL(TotalActualRebate / NULLIF(TotalQuantity, 0.0),
                               0.0) AS UnitRebate ,
                        InvoiceLineGroup2Key ,
                        InvoiceLineGroup3Key ,
                        InvoiceDateDayKey ,
                        ClientInvoiceID ,
                        ClientInvoiceLineID ,
                        InvoiceLineUniqueIdentifier ,
                        LinkedInvoiceLineUniqueIdentifier ,
                        InvoiceLineJunkKey ,
                        Last12MonthsIndicator ,
                        NULL AS TotalNetSupplierCost ,
                        TotalActualPrice ,
                        TotalActualCost
               FROM     dbo.FactInvoiceLine
               WHERE    Last12MonthsIndicator = 'Y'
             ),
        LastData
          AS ( SELECT   AccountKey ,
                        ItemKey ,
                        InvoiceLineGroup1Key ,
                        PriceDerivation ,
                        TotalQuantity ,
                        TotalActualPrice ,
                        TotalActualCost ,
                        InvoiceDateDayKey ,
                        ROW_NUMBER() OVER ( PARTITION BY AccountKey, ItemKey,
                                            InvoiceLineGroup1Key,
                                            PriceDerivation ORDER BY CASE
                                                              WHEN ISNULL(( ( UnitPrice
                                                              - UnitCost )
                                                              / NULLIF(UnitPrice,
                                                              0.0) ), -9999) BETWEEN -1.2
                                                              AND
                                                              0.90
                                                              AND TotalQuantity > 0.0
                                                              AND UnitPrice > 0.0
                                                              AND UnitCost > 0.0
                                                              AND PriceDerivation <> N'SPO' -- PriceDerivation
                                                              AND PriceDerivation <> N'CCP' -- PriceDerivation
                                                              AND Last12MonthsIndicator = 'Y'
                                                              THEN 1 -- good to use as last
                                                              ELSE 2 -- bad to get used only if not "good"/last resort
                                                              END ASC, InvoiceDateDayKey DESC, InvoiceLineUniqueIdentifier DESC ) AS RowRank
               FROM     Lines
             )
    SELECT  AccountKey ,
            ItemKey ,
            InvoiceLineGroup1Key ,
            PriceDerivation ,
            TotalQuantity ,
            TotalActualPrice ,
            TotalActualCost ,
            InvoiceDateDayKey
    INTO    #LastAIS
    FROM    LastData ld
    WHERE   ld.RowRank = 1;
SET @RowsI = @RowsI + @@RowCount;

CREATE UNIQUE CLUSTERED INDEX I_LastAIS_1 ON #LastAIS (AccountKey, ItemKey, InvoiceLineGroup1Key, PriceDerivation );

EXEC LogDCPEvent 'ETL - lion.Load_FactMarginErosion: Determining Lasts',
    'E', @RowsI, @RowsU, @RowsD;


/***  ********************************************************************************************************************************************  ***/;
EXEC LogDCPEvent 'ETL - lion.Load_FactMarginErosion: Determining #TotalData',
    'B';
SET @RowsI = 0;
SET @RowsU = 0;
SET @RowsD = 0;

SELECT  AccountKey ,
        ItemKey ,
        InvoiceLineGroup1Key ,
        UDVarchar1 AS PriceDerivation ,
        SUM(TotalQuantity) AS Total12MonthQuantity ,
        SUM(TotalActualPrice) AS Total12MonthSales ,
        SUM(TotalActualCost) AS Total12MonthCost ,
        COUNT(*) AS Total12InvoiceLineCount
INTO    #TotalData
FROM    dbo.FactInvoiceLine fil
WHERE   Last12MonthsIndicator = 'Y'
GROUP BY AccountKey ,
        ItemKey ,
        InvoiceLineGroup1Key ,
        UDVarchar1;
SET @RowsI = @RowsI + @@RowCount;

CREATE UNIQUE CLUSTERED INDEX I_AISTotals_1 ON #TotalData (AccountKey, ItemKey, InvoiceLineGroup1Key ,PriceDerivation);
CREATE INDEX I_AISTotals_2 ON #TotalData (ItemKey);
UPDATE STATISTICS #TotalData;

EXEC LogDCPEvent 'ETL - lion.Load_FactMarginErosion: Determining #TotalData',
    'E', @RowsI, @RowsU, @RowsD;


EXEC LogDCPEvent 'ETL - lion.Load_FactMarginErosion: Determining #AccountTotals',
    'B';
SET @RowsI = 0;
SET @RowsU = 0;
SET @RowsD = 0;

SELECT  AccountKey ,
        SUM(TotalActualPrice) AS TotalAccountSales ,
        SUM(TotalActualPrice) - SUM(TotalActualCost) AS TotalAccountGP
INTO    #AccountTotals
FROM    dbo.FactInvoiceLine fil
WHERE   Last12MonthsIndicator = 'Y'
GROUP BY AccountKey;
SET @RowsI = @RowsI + @@RowCount;

CREATE UNIQUE CLUSTERED INDEX I_AccountTotals_1 ON #AccountTotals (AccountKey);

EXEC LogDCPEvent 'ETL - lion.Load_FactMarginErosion: Determining #AccountTotals',
    'E', @RowsI, @RowsU, @RowsD;


EXEC LogDCPEvent 'ETL - lion.Load_FactMarginErosion: Determining #AccountItemTotals',
    'B';
SET @RowsI = 0;
SET @RowsU = 0;
SET @RowsD = 0;

SELECT  AccountKey ,
        ItemKey ,
        SUM(TotalActualPrice) AS TotalAccountItemSales ,
        SUM(TotalActualPrice) - SUM(TotalActualCost) AS TotalAccountItemGP
INTO    #AccountItemTotals
FROM    dbo.FactInvoiceLine fil
WHERE   Last12MonthsIndicator = 'Y'
GROUP BY AccountKey ,
        ItemKey;
SET @RowsI = @RowsI + @@RowCount;

CREATE UNIQUE CLUSTERED INDEX I_AITotals_1 ON #AccountItemTotals (AccountKey, ItemKey);

EXEC LogDCPEvent 'ETL - lion.Load_FactMarginErosion: Determining #AccountItemTotals',
	'E', @RowsI, @RowsU, @RowsD;


EXEC LogDCPEvent 'ETL - lion.Load_FactMarginErosion: Determining #ItemMedians',
	'B';
SET @RowsI = 0;
SET @RowsU = 0;
SET @RowsD = 0;

WITH    ItemData
          AS ( SELECT   ItemKey ,
                        InvoiceLineGroup1Key ,
                        UDVarchar1 AS PriceDerivation ,
                        TotalActualPrice / NULLIF(TotalQuantity, 0.0) AS ItemUnitPrice ,
                        ( TotalActualPrice - TotalActualCost )
                        / NULLIF(TotalActualPrice, 0.0) AS GPP ,
                        ROW_NUMBER() OVER ( PARTITION BY ItemKey,
                                            InvoiceLineGroup1Key ORDER BY TotalActualPrice
                                            / NULLIF(TotalQuantity, 0.0) ) AS UnitPriceRowNum ,
                        ROW_NUMBER() OVER ( PARTITION BY ItemKey,
                                            InvoiceLineGroup1Key ORDER BY ( TotalActualPrice
                                                              - TotalActualCost )
                                            / NULLIF(TotalActualPrice, 0.0) ) AS GPPRowNum ,
                        COUNT(*) OVER ( PARTITION BY ItemKey,
                                        InvoiceLineGroup1Key ) AS ItemKeyCnt
               FROM     dbo.FactInvoiceLine fil
               WHERE    Last12MonthsIndicator = 'Y'
             ),
        ItemUnitPriceMed
          AS ( SELECT   ItemKey ,
                        InvoiceLineGroup1Key ,
                        PriceDerivation ,
                        AVG(ItemUnitPrice) AS MedianUnitPrice
               FROM     ItemData
               WHERE    UnitPriceRowNum IN ( ( ItemKeyCnt + 1 ) / 2,
                                             ( ItemKeyCnt + 2 ) / 2 )
               GROUP BY ItemKey ,
                        InvoiceLineGroup1Key ,
                        PriceDerivation
             ),
        ItemGPPMed
          AS ( SELECT   id.ItemKey ,
                        id.InvoiceLineGroup1Key ,
                        id.PriceDerivation ,
                        AVG(GPP) AS MedianItemGPP
               FROM     ItemData id
               WHERE    GPPRowNum IN ( ( ItemKeyCnt + 1 ) / 2,
                                       ( ItemKeyCnt + 2 ) / 2 )
               GROUP BY id.ItemKey ,
                        id.InvoiceLineGroup1Key ,
                        id.PriceDerivation
             )
    SELECT  pm.ItemKey ,
            pm.InvoiceLineGroup1Key ,
            pm.PriceDerivation ,
            pm.MedianUnitPrice ,
            gm.MedianItemGPP
    INTO    #ItemMedians
    FROM    ItemUnitPriceMed pm
            INNER JOIN ItemGPPMed gm ON gm.ItemKey = pm.ItemKey
                                        AND gm.InvoiceLineGroup1Key = pm.InvoiceLineGroup1Key
                                        AND gm.PriceDerivation = pm.PriceDerivation;
SET @RowsI = @RowsI + @@RowCount;
CREATE UNIQUE CLUSTERED INDEX I_ItemMedians_1 ON #ItemMedians (ItemKey, InvoiceLineGroup1Key, PriceDerivation);

EXEC LogDCPEvent 'ETL - lion.Load_FactMarginErosion: Determining #ItemMedians',
	'E', @RowsI, @RowsU, @RowsD;


EXEC LogDCPEvent 'ETL - lion.Load_FactMarginErosion: Insert FactMarginErosion',
	'B';
SET @RowsI = 0;
SET @RowsU = 0;
SET @RowsD = 0;

TRUNCATE TABLE dbo.FactMarginErosion;

INSERT  INTO dbo.FactMarginErosion
        ( 
		AccountKey ,
		ItemKey ,
		InvoiceLineGroup1Key ,
		PriceDerivation ,
		LastQuantity ,
		LastUnitPrice ,
		LastUnitCost ,
		LastSaleDayKey ,
		LastSaleDate ,
		Total12MonthQuantity ,
		Total12MonthSales ,
		Total12MonthCost ,
		Total12MonthInvoiceLineCount ,
		SustainedGPP ,
		UnitPriceAtSustained ,
		UnitCostAtSustained ,
		MEProposedPrice ,
		MEImpact ,
		PLSIndicator,
			
		TermPyramidCode,
		TermLLSPGCode,
		TermExceptionProduct,

		Usualdiscount1,
		Usualdiscount2,
		ExceptionDiscount,
		ExceptionFixedPrice,
		ExceptionFromDate,
		ExceptionToDate
        )
    SELECT  
		td.AccountKey ,
        td.ItemKey ,
        td.InvoiceLineGroup1Key ,
        td.PriceDerivation ,
        ISNULL(ll.TotalQuantity, 0.0) AS LastQuantity ,
        ISNULL(ll.TotalActualPrice / NULLIF(ll.TotalQuantity, 0.0),
                0.0) AS LastUnitPrice ,
        ISNULL(ll.TotalActualCost / NULLIF(ll.TotalQuantity, 0.0), 0.0) AS LastUnitCost ,
        ISNULL(ll.InvoiceDateDayKey, @UnknownDayKey) AS LastSaleDayKey ,
        ddLast.SQLDate AS LastSaleDate ,
        td.Total12MonthQuantity ,
        td.Total12MonthSales ,
        td.Total12MonthCost ,
        td.Total12InvoiceLineCount ,
        ISNULL(sd.SustainedGPP, 0.0) ,
        ISNULL(sd.UnitPriceAtSustained, 0.0) ,
        ISNULL(sd.UnitCostAtSustained, 0.0) ,
        ISNULL(( ll.TotalActualCost / NULLIF(ll.TotalQuantity, 0.0) )
			/ NULLIF(1.0 - sd.SustainedGPP, 0.0)-- LastCost / (1.0 - SustainedGPP)
		, 0.0) AS MEProposedPrice ,
        CASE WHEN ISNULL(( ( ll.TotalActualCost
                                / NULLIF(ll.TotalQuantity, 0.0) )
                            / NULLIF(1.0 - sd.SustainedGPP, 0.0)
                            - ( ll.TotalActualPrice
                                / NULLIF(ll.TotalQuantity, 0.0) ) )
                            * td.Total12MonthQuantity-- (Corrected Price - Last Price) * 12 month Qty
				, 0.0) BETWEEN 0.0
            AND     td.Total12MonthSales
                THEN -- it is within a tolerable range and thus "good"
                    ISNULL(( ( ll.TotalActualCost
                                / NULLIF(ll.TotalQuantity, 0.0) )
                            / NULLIF(1.0 - sd.SustainedGPP, 0.0)
                            - ( ll.TotalActualPrice
                                / NULLIF(ll.TotalQuantity, 0.0) ) )
                            * td.Total12MonthQuantity-- (Corrected Price - Last Price) * 12 month Qty
					, 0.0)
                ELSE 0.0
        END AS MEImpact ,
        CASE WHEN ( @MaxInvoiceDateDayKey - ll.InvoiceDateDayKey ) >= @PLSDaysWithNoSales				-- no sales > 180 days
                    AND td.Total12InvoiceLineCount >= @PLSFrequency										-- freq of 2
                    AND ( td.Total12MonthSales - td.Total12MonthCost ) >= @PLSMinGP						-- GP > 5.00 
                    THEN 'Y'
                ELSE 'N'
        END AS PLSIndicator,

		ISNULL(ct.PyramidCode, ct2.PyramidCode) AS TermPyramidCode,
		ISNULL(ct.SPGCode, ct2.SPGCode) AS TermLLSPGCode,
		ISNULL(ct.ExceptionProduct, ct2.ExceptionProduct) AS TermExceptionProduct,
		
		ISNULL(ct.Usualdiscount1, ct2.Usualdiscount1) AS Usualdiscount1,
		ISNULL(ct.Usualdiscount2, ct2.Usualdiscount2) AS Usualdiscount2,
		ISNULL(ct.ExceptionDiscount, ct2.ExceptionDiscount) AS ExceptionDiscount,
		ISNULL(ct.ExceptionFixedPrice, ct2.ExceptionFixedPrice) AS ExceptionFixedPrice,

		ISNULL(ct.ExceptionFromDate, ct2.ExceptionFromDate) AS ExceptionFromDate,
		ISNULL(ct.ExceptionToDate, ct2.ExceptionToDate) AS ExceptionToDate

    FROM    #TotalData td
    INNER JOIN #LastAIS ll --LastAIS
    ON ll.AccountKey = td.AccountKey
        AND ll.ItemKey = td.ItemKey
        AND ll.InvoiceLineGroup1Key = td.InvoiceLineGroup1Key
        AND ll.PriceDerivation = td.PriceDerivation
    LEFT JOIN #ItemMedians im ON im.ItemKey = td.ItemKey
                                    AND im.InvoiceLineGroup1Key = td.InvoiceLineGroup1Key
                                    AND im.PriceDerivation = td.PriceDerivation
    LEFT JOIN #AccountItemTotals ait ON ait.AccountKey = td.AccountKey
                                        AND ait.ItemKey = td.ItemKey
    LEFT JOIN #AccountTotals at ON at.AccountKey = td.AccountKey
    LEFT JOIN #SustainedData sd ON sd.AccountKey = td.AccountKey
                                    AND sd.PriceDerivation = td.PriceDerivation
                                    AND sd.ItemKey = td.ItemKey
                                    AND sd.InvoiceLineGroup1Key = td.InvoiceLineGroup1Key
    LEFT JOIN dbo.DimDay ddLast ON ISNULL(ll.InvoiceDateDayKey,
                                            @UnknownDayKey) = ddLast.DayKey
	INNER JOIN dbo.DimItem di
		ON di.ItemKey = td.ItemKey
	LEFT JOIN lion.CustomerTerms ct
		ON ct.AccountKey = td.AccountKey
		AND ct.ItemKey = di.ItemKey
		AND ct.ItemKey <> 1
	LEFT JOIN lion.CustomerTerms ct2
		ON ct2.AccountKey = td.AccountKey
		AND ct2.ItemGroup3Key = di.ItemGroup3Key
		AND ct2.ItemKey = 1
	WHERE ISNULL(sd.SustainedGPP, 0.0) > 0 AND 
		CASE WHEN ISNULL(( ( ll.TotalActualCost
                                    / NULLIF(ll.TotalQuantity, 0.0) )
                                / NULLIF(1.0 - sd.SustainedGPP, 0.0)
                                - ( ll.TotalActualPrice
                                    / NULLIF(ll.TotalQuantity, 0.0) ) )
                                * td.Total12MonthQuantity-- (Corrected Price - Last Price) * 12 month Qty
		, 0.0) BETWEEN 0.0
                AND     td.Total12MonthSales
                    THEN -- it is within a tolerable range and thus "good"
                        ISNULL(( ( ll.TotalActualCost
                                    / NULLIF(ll.TotalQuantity, 0.0) )
                                / NULLIF(1.0 - sd.SustainedGPP, 0.0)
                                - ( ll.TotalActualPrice
                                    / NULLIF(ll.TotalQuantity, 0.0) ) )
                                * td.Total12MonthQuantity-- (Corrected Price - Last Price) * 12 month Qty
			, 0.0)
                    ELSE 0.0
            END
				> 0;
SET @RowsI = @RowsI + @@RowCount;

EXEC LogDCPEvent 'ETL - lion.Load_FactMarginErosion: Insert FactMarginErosion',
	'E', @RowsI, @RowsU, @RowsD;




--/*;
IF OBJECT_ID('tempdb.dbo.#AISDistinctGPP', 'U') IS NOT NULL
    DROP TABLE #AISDistinctGPP;
IF OBJECT_ID('tempdb.dbo.#CDFTable', 'U') IS NOT NULL
    DROP TABLE #CDFTable;
IF OBJECT_ID('tempdb.dbo.#SustainedRow', 'U') IS NOT NULL
    DROP TABLE #SustainedRow;
IF OBJECT_ID('tempdb.dbo.#SustainedData', 'U') IS NOT NULL
    DROP TABLE #SustainedData;
IF OBJECT_ID('tempdb.dbo.#LastAIS', 'U') IS NOT NULL
    DROP TABLE #LastAIS;
IF OBJECT_ID('tempdb.dbo.#TotalData', 'U') IS NOT NULL
    DROP TABLE #TotalData;
IF OBJECT_ID('tempdb.dbo.#AccountTotals', 'U') IS NOT NULL
    DROP TABLE #AccountTotals;
IF OBJECT_ID('tempdb.dbo.#AccountItemTotals', 'U') IS NOT NULL
    DROP TABLE #AccountItemTotals;
IF OBJECT_ID('tempdb.dbo.#ItemMedians', 'U') IS NOT NULL
    DROP TABLE #ItemMedians;

;

EXEC LogDCPEvent 'ETL - lion.Load_FactMarginErosion', 'E', @RowsI, @RowsU, @RowsD;

GO
