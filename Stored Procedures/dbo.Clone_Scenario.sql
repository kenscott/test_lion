SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO










-- exec clone_Scenario 3, 'N'
CREATE      PROCEDURE [dbo].[Clone_Scenario] 
@ScenarioKey INT,
@Command CHAR(1) = 'N'
AS


CREATE TABLE #ScenarioData([ScenarioDataKey] [int] IDENTITY (1, 1) NOT NULL, TextData NVARCHAR(MAX))

SET NoCount ON

IF NOT EXISTS (SELECT * FROM ATKScenario WHERE ScenarioKey = @ScenarioKey)
BEGIN
	INSERT INTO #ScenarioData SELECT 'Scenario does not exist!'	
END

IF @Command = 'N'
BEGIN
	INSERT INTO #ScenarioData SELECT  'Set Xact_Abort On'
	INSERT INTO #ScenarioData SELECT  'Set NocOunt On'
	INSERT INTO #ScenarioData SELECT  'Begin Transaction'
END


DECLARE @SQL NVARCHAR(MAX)


DECLARE 
	@ApproachTypeKey NVARCHAR(MAX),
	@LimitDataPlaybookKey  NVARCHAR(MAX),
	@SegmentFactorPlaybookKey  NVARCHAR(MAX),
	@PricingRulePlaybookKey  NVARCHAR(MAX),
	@ProposalRecommendationPlaybookKey  NVARCHAR(MAX),
	@ScenarioCreationWebUserKey  NVARCHAR(MAX),
	@ScenarioName  NVARCHAR(MAX),
	@ActiveScenarioFlag  NVARCHAR(MAX),
	@MinDaysToRecomputeQuantityBandFromAutomaticRecompute  NVARCHAR(MAX),
	@MinDaysToRecomputeQuantityBandFromManualChange  NVARCHAR(MAX),
	@SuppressionTypeKey  NVARCHAR(MAX),
	@ProjectSpecificationDescription NVARCHAR(MAX),
	@SuppressionTypeDesc NVARCHAR(MAX),
	@UDVarchar1 Description_Small_type

DECLARE @ProjectSpecificationName NVARCHAR(800)


SELECT 
	@ScenarioKey = ScenarioKey, 
	@ApproachTypeKey = ApproachTypeKey, 
	@LimitDataPlaybookKey = LimitDataPlaybookKey, 
	@SegmentFactorPlaybookKey = SegmentFactorPlaybookKey, 
	@PricingRulePlaybookKey = PricingRulePlaybookKey, 
	@ProposalRecommendationPlaybookKey = ProposalRecommendationPlaybookKey, 
	@ScenarioCreationWebUserKey = ScenarioCreationWebUserKey, 
	@ScenarioName = REPLACE(ScenarioName, '''', ''''''), 
	@ActiveScenarioFlag = ActiveScenarioFlag, 
	@MinDaysToRecomputeQuantityBandFromAutomaticRecompute = MinDaysToRecomputeQuantityBandFromAutomaticRecompute, 
	@MinDaysToRecomputeQuantityBandFromManualChange = MinDaysToRecomputeQuantityBandFromManualChange, 
	@SuppressionTypeKey = SuppressionTypeKey,
	@UDVarchar1 = UDVarchar1 
FROM ATkScenario
WHERE ScenarioKey = @ScenarioKey

IF @Command = 'Y'
BEGIN
	INSERT INTO #ScenarioData SELECT  '------------------------------------------------------'
	INSERT INTO #ScenarioData SELECT  '--Your current ScenarioKey is: '+CAST(@ScenarioKey AS NVARCHAR(100))
	INSERT INTO #ScenarioData SELECT  '------------------------------------------------------'
	INSERT INTO #ScenarioData SELECT  '--LimitDataPlaybookKey:                                 '+ISNULL(@LimitDataPlaybookKey, 'NULL')
	IF @ApproachTypeKey IN (2, 3, 5, 7) 
		INSERT INTO #ScenarioData SELECT  '--SegmentFactorPlaybookKey:                             '+ISNULL(@SegmentFactorPlaybookKey, 'NULL')
	INSERT INTO #ScenarioData SELECT  '--PricingRulePlaybookKey:                               '+ISNULL(@PricingRulePlaybookKey, 'NULL')
	INSERT INTO #ScenarioData SELECT  '--ProposalRecommendationPlaybookKey:                    '+ISNULL(@ProposalRecommendationPlaybookKey, 'NULL')
	INSERT INTO #ScenarioData SELECT  '--ScenarioCreationWebUserKey:                           '+ISNULL(@ScenarioCreationWebUserKey, 'NULL')
	INSERT INTO #ScenarioData SELECT  '--ScenarioName:                                         '+@ScenarioName
	INSERT INTO #ScenarioData SELECT  '--ActiveScenarioFlag:                                   '+@ActiveScenarioFlag
	IF @ApproachTypeKey IN (2, 3, 5, 7) 
		INSERT INTO #ScenarioData SELECT  '--MinDaysToRecomputeQuantityBandFromAutomaticRecompute: '+@MinDaysToRecomputeQuantityBandFromAutomaticRecompute
	IF @ApproachTypeKey IN (2, 3, 5, 7) 
		INSERT INTO #ScenarioData SELECT  '--MinDaysToRecomputeQuantityBandFromManualChange:       '+@MinDaysToRecomputeQuantityBandFromManualChange
	IF @ApproachTypeKey IN (1, 2, 3, 5, 7, 8) 
	BEGIN
		INSERT INTO #ScenarioData SELECT  '--SuppressionTypeKey:                                   '+@SuppressionTypeKey
		SELECT @SuppressionTypeDesc = SuppressionTypeDesc FROM ATKSuppressionType WHERE SuppressionTypeKey = @SuppressionTypeKey
		INSERT INTO #ScenarioData SELECT  '--SuppressionTypeDesc:                                   '+@SuppressionTypeDesc
	END
	INSERT INTO #ScenarioData SELECT  '--UDVarchar1:                                           '+ISNULL(@UDVarchar1, 'NULL')
END
ELSE IF @Command = 'U'
BEGIN
	INSERT INTO #ScenarioData SELECT  '------------------------------------------------------'
	INSERT INTO #ScenarioData SELECT  '--Updating ATKScenario: '+CAST(@ScenarioKey AS NVARCHAR(100))
	INSERT INTO #ScenarioData SELECT  '------------------------------------------------------'
	INSERT INTO #ScenarioData SELECT  '	
	UPDATE ATKScenario SET
	LimitDataPlaybookKey='+ISNULL(@LimitDataPlaybookKey,'NULL')+',
	SegmentFactorPlaybookKey='+ISNULL(@SegmentFactorPlaybookKey,'NULL')+',
	PricingRulePlaybookKey='+ISNULL(@PricingRulePlaybookKey,'NULL')+',
	ProposalRecommendationPlaybookKey='+ISNULL(@ProposalRecommendationPlaybookKey,'NULL')+',
	ScenarioCreationWebUserKey='+ISNULL(@ScenarioCreationWebUserKey,'NULL')+',
	ScenarioName='''+ISNULL(@ScenarioName,'')+''',
	ActiveScenarioFlag='''+ISNULL(@ActiveScenarioFlag,'Y')+''',
	MinDaysToRecomputeQuantityBandFromAutomaticRecompute='+ISNULL(@MinDaysToRecomputeQuantityBandFromAutomaticRecompute,'0')+',
	MinDaysToRecomputeQuantityBandFromManualChange='+ISNULL(@MinDaysToRecomputeQuantityBandFromManualChange,'0')+',
	SuppressionTypeKey='+ISNULL(@SuppressionTypeKey,'NULL')+',
	UDVarchar1='''+ISNULL(@UDVarchar1, 'NULL')+'''
	Where ScenarioKey = '+CAST(@ScenarioKey AS NVARCHAR(100))
END


IF @LimitDataPlaybookKey IS NOT NULL 
	EXEC Clone_LimitDataPlaybook @LimitDataPlaybookKey, @Command, @ApproachTypeKey

IF @SegmentFactorPlaybookKey IS NOT NULL 
	EXEC Clone_SegmentationPlaybook @SegmentFactorPlaybookKey, @Command, @ApproachTypeKey

IF @PricingRulePlaybookKey IS NOT NULL 
	EXEC Clone_PricingRulePlaybook @PricingRulePlaybookKey, @Command, @ApproachTypeKey

IF @ProposalRecommendationPlaybookKey IS NOT NULL 
	EXEC Clone_ProposalRecommendationPlaybook @ProposalRecommendationPlaybookKey, @Command, @ApproachTypeKey


IF @Command = 'N'
BEGIN

	INSERT INTO #ScenarioData SELECT  '-------------------------------------'
	INSERT INTO #ScenarioData SELECT  '--Insert new ATKScenario Row'
	INSERT INTO #ScenarioData SELECT  '-------------------------------------'
	SET @SQL = '
	INSERT INTO ATKScenario(ApproachTypeKey, LimitDataPlaybookKey, SegmentFactorPlaybookKey, PricingRulePlaybookKey, ProposalRecommendationPlaybookKey, ScenarioCreationWebUserKey, ScenarioName, ActiveScenarioFlag, MinDaysToRecomputeQuantityBandFromAutomaticRecompute, MinDaysToRecomputeQuantityBandFromManualChange, SuppressionTypeKey, UDVarchar1, Version)
	Values ('''+@ApproachTypeKey+''',' + IIF(@LimitDataPlaybookKey IS NULL, 'NULL', '@NewLimitDataPlaybookKey') + ',' + IIF(@SegmentFactorPlaybookKey IS NULL, 'NULL', '@NewSegmentFactorPlaybookKey') +',' + IIF(@PricingRulePlaybookKey IS NULL, 'NULL', '@NewPricingRulePlaybookKey') + ',' + IIF(@ProposalRecommendationPlaybookKey IS NULL, 'NULL', '@NewProposalRecommendationPlaybookKey') +','+ISNULL(@ScenarioCreationWebUserKey,'NULL')+','''+ISNULL(@ScenarioName,'')+''','''+ISNULL(@ActiveScenarioFlag,'Y')+''','+ISNULL(@MinDaysToRecomputeQuantityBandFromAutomaticRecompute,'0')+','+ISNULL(@MinDaysToRecomputeQuantityBandFromManualChange,'0')+','+@SuppressionTypeKey+','''+ISNULL(@UDVarchar1, 'NULL')+''',0)'
	INSERT INTO #ScenarioData SELECT  @SQL
	INSERT INTO #ScenarioData SELECT  'Declare @NewScenarioKey int'
	INSERT INTO #ScenarioData SELECT  'Set @NewScenarioKey = @@Identity'
	INSERT INTO #ScenarioData SELECT  'Print ''Your new Scenario Key is: ''+Cast(@NewScenarioKey as nvarchar(10))'
	
	SELECT @ProjectSpecificationName = ProjectSpecificationName, @ProjectSpecificationDescription = ProjectSpecificationDescription
	FROM ATKProjectSpecificationScenario A
	JOIN ATKProjectSpecification B ON A.ProjectSpecificationKey = B.ProjectSpecificationKey
	WHERE ScenarioKey = @ScenarioKey

	INSERT INTO #ScenarioData SELECT  '-------------------------------------'
	INSERT INTO #ScenarioData SELECT  '--Insert new ATKProjectSpecification Row'
	INSERT INTO #ScenarioData SELECT  '-------------------------------------'
	SET @SQL = '
	INSERT INTO ATKProjectSpecification(ProjectSpecificationName, ProjectSpecificationDescription, ApproachTypeKey, Version)
	Values ('''+@ProjectSpecificationName+''','+''''+ISNULL(@ProjectSpecificationDescription,'')+''','+@ApproachTypeKey+',0)'
	INSERT INTO #ScenarioData SELECT  @SQL
	INSERT INTO #ScenarioData SELECT  'Declare @NewProjectSpecificationKey int'
	INSERT INTO #ScenarioData SELECT  'Set @NewProjectSpecificationKey = @@Identity'
	INSERT INTO #ScenarioData SELECT  'Print ''Your new Project Specification Key is: ''+Cast(@NewProjectSpecificationKey as nvarchar(10))'
	
	INSERT INTO #ScenarioData SELECT  '-------------------------------------'
	INSERT INTO #ScenarioData SELECT  '--Insert new ATKProjectSpecificationScenario Row'
	INSERT INTO #ScenarioData SELECT  '-------------------------------------'
	SET @SQL = '
	INSERT INTO ATKProjectSpecificationScenario(ProjectSpecificationKey, ScenarioKey, Version)
	VALUES(@NewProjectSpecificationKey, @NewScenarioKey, 0)'
	INSERT INTO #ScenarioData SELECT  @SQL
	INSERT INTO #ScenarioData SELECT  'Declare @NewProjectSpecificationScenarioKey int'
	INSERT INTO #ScenarioData SELECT  'Set @NewProjectSpecificationScenarioKey = @@Identity'
END

IF @Command = 'N'
BEGIN
	INSERT INTO #ScenarioData SELECT  'Commit Transaction'
END

SELECT TextData AS '--TextData--' FROM #ScenarioData ORDER BY ScenarioDataKey












GO
