SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE procedure [dbo].[DBA_TableSpace]
as
--Author: Tom Werz
--Date: 10/07/2005

declare @a varchar(5000)

Create table #Temp(name varchar(50), rows int, reserved varchar(50), data varchar(50),index_size varchar(50),unused varchar(50))


declare a cursor for
Select 'Insert into #Temp exec sp_spaceused ['+sysusers.name+'.'+sysobjects.name+']' 
from sysobjects, sysusers 
where xtype = 'U'
and sysobjects.uid = sysusers.uid


open a
fetch next from a into @a
while @@fetch_status = 0
begin
	Exec (@a)
	print @a
	fetch next from a into @a
end
close a
deallocate a


update #Temp set 
	reserved = replace (reserved,' KB', ''),
	data = replace (data,' KB', ''),
	index_size = replace (index_size,' KB', ''),
	unused = replace (unused,' KB', '')

Alter table #Temp alter column reserved int
Alter table #Temp alter column data int
Alter table #Temp alter column index_size int
Alter table #Temp alter column unused int

Select * from #Temp order by Reserved desc

Drop table #Temp


GO
