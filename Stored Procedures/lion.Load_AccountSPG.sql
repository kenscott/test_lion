SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
----/****** Object:  StoredProcedure [lion].[Load_AccountSPG]    Script Date: 02/12/2014 20:33:57 ******/
----DROP PROCEDURE [lion].[Load_AccountSPG]
----GO

----/****** Object:  StoredProcedure [lion].[Load_AccountSPG]    Script Date: 02/12/2014 20:33:57 ******/
----SET ANSI_NULLS ON
----GO

----SET QUOTED_IDENTIFIER ON
----GO










CREATE PROCEDURE [lion].[Load_AccountSPG]
AS

/*

EXEC lion.Load_AccountSPG

*/


SET ANSI_WARNINGS OFF
SET ARITHABORT OFF
SET ARITHIGNORE ON

SET NOCOUNT ON

EXEC LogDCPEvent 'ETL - Load_AccountSPG', 'B'


DECLARE 
	@RowsI Quantity_Normal_type, 
	@RowsU Quantity_Normal_type, 
	@RowsD Quantity_Normal_type

SET @RowsI = 0
SET @RowsU = 0
SET @RowsD = 0



TRUNCATE TABLE lion.AccountSPG


INSERT INTO lion.AccountSPG
(
	AccountKey,
	LLSPGCode,
	AccountId
)
SELECT DISTINCT
	ISNULL(AccountKey, 1),
	SPG,
	AccountId
FROM Lion_Staging.dbo.CustomerSPGs s
LEFT JOIN dbo.DimAccount da
	ON da.AccountNumber = s.AccountID
--WHERE
--	SPG <> 'ALL'
SET @RowsI = @RowsI + @@RowCount


--; WITH AllCodes AS (
--	SELECT DISTINCT
--		IG3Level1 AS LLSPGCode
--	FROM dbo.DimItemGroup3
--	WHERE
--		IG3Level1 <> ''
--)
--INSERT INTO lion.AccountSPG
--(
--	AccountKey,
--	LLSPGCode
--)
--SELECT
--	DISTINCT
--	AccountKey,
--	LLSPGCode
--FROM Lion_Staging.dbo.CustomerSPGs s
--INNER JOIN dbo.DimAccount da
--	ON da.AccountNumber = s.AccountID
--CROSS JOIN AllCodes
--WHERE
--	SPG = 'ALL'
--SET @RowsI = @RowsI + @@RowCount


SELECT TOP 10 * FROM lion.AccountSPG


EXEC LogDCPEvent 'ETL - Load_AccountSPG', 'E', @RowsI, @RowsU, @RowsD















GO
