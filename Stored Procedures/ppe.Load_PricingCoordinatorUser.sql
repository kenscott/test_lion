SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [ppe].[Load_PricingCoordinatorUser] (@PCWebUserKey INT, @Action VARCHAR(10)) AS 
BEGIN

	--DECLARE @PCWebUserKey INT
	--DECLARE @Action VARCHAR(10)
	--SELECT @Action = 'ADD' --'REMOVE'
	--SELECT @PCWebUserKey = 1234

	DECLARE @CentPriceWebUserKey INT

	SELECT @CentPriceWebUserKey = MIN(WebUserKey)
	FROM dbo.WebUser wu
	INNER JOIN dbo.ODSPerson odsp
		ON odsp.ODSPersonKey = wu.ODSPersonKey
	WHERE
		odsp.LevelId = 'CentralPricing'

	IF @Action = 'ADD'
	BEGIN

		INSERT INTO dbo.WebUserView (ViewerWebUserKey,ViewedWebUserKey)
		SELECT @PCWebUserKey, ViewedWebUserKey
		FROM WebUserView
		WHERE ViewerWebUserKey = @CentPriceWebUserKey
		EXCEPT (SELECT ViewerWebUserKey, ViewedWebUserKey FROM WebUserView)
		
	END
	ELSE IF @Action = 'REMOVE'
	BEGIN

		DELETE FROM dbo.WebUserView
		WHERE ViewerWebUserKey = @PCWebUserKey

		INSERT INTO dbo.WebUserView (ViewerWebUserKey,ViewedWebUserKey)
		VALUES (@PCWebUserKey,@PCWebUserKey)

		INSERT INTO dbo.WebUserView (ViewerWebUserKey,ViewedWebUserKey)
		SELECT DISTINCT
			wuamat.WebUserKey, 
			wu.WebUserKey
		FROM dbo.WebUserAccountManagerAccessType wuamat
		INNER JOIN dbo.WebUser wu
			ON wu.AccountManagerKey = wuamat.AccountManagerKey
		WHERE wuamat.WebUserKey = @PCWebUserKey
		EXCEPT (SELECT ViewerWebUserKey, ViewedWebUserKey FROM WebUserView)

	END

	SELECT 1 AS RESULT
END
GO
