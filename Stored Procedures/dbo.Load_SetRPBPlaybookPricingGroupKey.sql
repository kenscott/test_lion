SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO







CREATE PROCEDURE [dbo].[Load_SetRPBPlaybookPricingGroupKey]
	@TableName VARCHAR(255),
	@TargetColumn VARCHAR(255),
	@AlternateTargetColumn VARCHAR(255),	
	@WhereClause VARCHAR(MAX),
	@ScenarioKey Key_Normal_type,
	@PlaybookDataPointGroupKey Key_Normal_type,
	@ParamDebug CHAR(1) = 'N'
AS

/*
EXEC dbo.P2P_RPBRefreshLastData 'Y'

exec dcplog
*/

SET NOCOUNT ON


EXEC LogDCPEvent 'DCP --- dbo.Load_SetRPBPlaybookPricingGroupKey', 'B'

DECLARE
	@RowsI Quantity_Normal_type, 
	@RowsU Quantity_Normal_type, 
	@RowsD Quantity_Normal_type,
	@DefaultValueForMatch Description_Normal_type,
	@PricingRuleSequenceNumber INT,
	@PricingRuleAttributeSequenceNumber VARCHAR(25),
	@PricingRuleAttributeType VARCHAR(MAX),
	@PricingRuleAttributeSourceTable VARCHAR(MAX),
	@PricingRuleKey INT,
	@PricingRuleAttributeTypeOverride VARCHAR(1),
	@TempFieldList NVARCHAR(MAX),
	@TempFieldListAlternate NVARCHAR(MAX),
	@TempJoinList NVARCHAR(MAX),
	@SQLString NVARCHAR(MAX)

SET @RowsI = 0
SET @RowsU = 0
SET @RowsD = 0

--set  @ParamDebug = 'Y'

IF @ParamDebug = 'Y'
BEGIN 
	RAISERROR ('---', 0, 1) WITH NOWAIT
	RAISERROR ('---', 0, 1) WITH NOWAIT
	RAISERROR ('@TableName: %s', 0, 1, @TableName) WITH NOWAIT
	RAISERROR ('@WhereClause: %s', 0, 1, @WhereClause) WITH NOWAIT
	RAISERROR ('@ScenarioKey: %d', 0, 1, @ScenarioKey) WITH NOWAIT
	RAISERROR ('@PlaybookDataPointGroupKey: %d', 0, 1, @PlaybookDataPointGroupKey) WITH NOWAIT
END


SELECT 
	@WhereClause = 
		CASE
			WHEN ISNULL(LTRIM(RTRIM(@WhereClause)), '') = '' THEN '' 
			WHEN RTRIM(@WhereClause) NOT LIKE 'AND%' THEN ' AND ' + @WhereClause
			ELSE @WhereClause
		END


DECLARE PricingRuleCursor 
CURSOR 
FAST_FORWARD 
FOR
	SELECT DISTINCT
		PricingRuleKey,
		PricingRuleSequenceNumber
		--, *
	FROM dbo.ATKPricingRule pr (NOLOCK)
	INNER JOIN dbo.ATKScenario s (NOLOCK)
		ON s.PricingRulePlaybookKey = pr.PricingRulePlaybookKey
	WHERE ScenarioKey = @ScenarioKey
	ORDER BY
		pr.PricingRuleSequenceNumber

OPEN PricingRuleCursor

FETCH NEXT FROM PricingRuleCursor INTO @PricingRuleKey, @PricingRuleSequenceNumber

WHILE (@@FETCH_STATUS <> -1)
BEGIN

	IF @ParamDebug = 'Y'
	BEGIN 
		RAISERROR ('...', 0, 1) WITH NOWAIT
		RAISERROR ('...', 0, 1) WITH NOWAIT
		RAISERROR ('@PricingRuleSequenceNumber: %d', 0, 1, @PricingRuleSequenceNumber) WITH NOWAIT
	END

	SET @TempFieldList = ''
	SET @TempFieldListAlternate = ''
	SET @TempJoinList = 'JOIN DimItem ON DimItem.ItemKey = ' + @TableName + '.ItemKey ' -- need dim item to get product line
	SET @PricingRuleAttributeTypeOverride = 'N'

	DECLARE PricingRuleAttributeCursor 
	CURSOR 
	FAST_FORWARD 
	FOR
		SELECT DISTINCT
			PricingRuleAttributeSequenceNumber,
			PricingRuleAttributeType,
			PricingRuleAttributeSourceTable,
			NULLIF(DefaultValueForMatch, '')
			--,*
		FROM dbo.ATKPricingRuleAttribute pra (NOLOCK)
		INNER JOIN dbo.ATKPricingRuleAttributeType prat (NOLOCK)
			ON prat.PricingRuleAttributeTypeKey = pra.PricingRuleAttributeTypeKey
		WHERE 
			PricingRuleKey = @PricingRuleKey
		ORDER BY
			pra.PricingRuleAttributeSequenceNumber
		
	OPEN PricingRuleAttributeCursor

	FETCH NEXT FROM PricingRuleAttributeCursor INTO @PricingRuleAttributeSequenceNumber, @PricingRuleAttributeType, @PricingRuleAttributeSourceTable, @DefaultValueForMatch

	WHILE (@@FETCH_STATUS <> -1)
	BEGIN
		
		IF @ParamDebug = 'Y'
		BEGIN 
			RAISERROR ('@PricingRuleAttributeSequenceNumber: %s', 0, 1, @PricingRuleAttributeSequenceNumber) WITH NOWAIT
			RAISERROR ('@PricingRuleAttributeType: %s', 0, 1, @PricingRuleAttributeType) WITH NOWAIT
			RAISERROR ('@PricingRuleAttributeSourceTable: %s', 0, 1, @PricingRuleAttributeSourceTable) WITH NOWAIT
		END

		SET @PricingRuleAttributeTypeOverride = 'N'

		/***  current rebate override - yuck  ***/
		--IF @PricingRuleAttributeSourceTable = 'DimInvoiceLineGroup2' AND @PricingRuleAttributeType = 'InvoiceLineGroup2UDVarchar1'
		--BEGIN
		
		--	--SET @PricingRuleAttributeSourceTable = @TableName
		--	--SET @PricingRuleAttributeType = 'UDVarchar8'

		--	SET @PricingRuleAttributeTypeOverride = 'Y'
		--	SET @PricingRuleAttributeSourceTable = 'FactRebate'
		--	SET @PricingRuleAttributeType = 'CASE WHEN FactRebate.AccountKey IS NULL THEN ''N'' ELSE ''Y'' END'
			
		--END
		--ELSE IF @PricingRuleAttributeSourceTable = 'FactLastPriceAndCost' AND @PricingRuleAttributeType = 'UDVarchar7'
		--BEGIN
		
		--	/*** OVERRIDE: CDI ***/
			
		--	SET @PricingRuleAttributeTypeOverride = 'Y'
		--	SET @PricingRuleAttributeSourceTable = 'FactLastPriceAndCost'
		--	SET @PricingRuleAttributeType = '
		--				CASE
		--					WHEN LEFT(DimItem.ItemDescription, 1) = ''@'' THEN ''Y''
		--					WHEN pag3i.StockStatus IS NULL OR pag3i.CDIFlag IS NULL THEN ''N''
		--					WHEN pag3i.StockStatus = ''CS'' OR pag3i.StockStatus = ''CA''
		--					  OR pag3i.CDIFlag = ''B'' OR pag3i.CDIFlag = ''C'' OR pag3i.CDIFlag = ''P'' THEN ''Y''
		--					ELSE ' + 
		--						CASE 
		--							WHEN @DefaultValueForMatch IS NULL THEN '''N'''
		--							ELSE '''' + @DefaultValueForMatch + ''''
		--						END + ' 
		--				END	'

		--	IF @TempJoinList NOT LIKE '%DimAccount%'
		--	BEGIN
		--		SET @TempJoinList = @TempJoinList + 'JOIN DimAccount ON DimAccount.AccountKey = ' + @TableName + '.AccountKey ' 
		--	END

		--	IF @TempJoinList NOT LIKE '%DimItem%'
		--	BEGIN
		--		SET @TempJoinList = @TempJoinList + 'JOIN DimItem ON DimItem.ItemKey = ' + @TableName + '.ItemKey ' 
		--	END

		--	IF @TempJoinList NOT LIKE '%PlaybookAccountGroup3Item%'
		--	BEGIN
		--		SET @TempJoinList = @TempJoinList + '
		--			LEFT JOIN dbo.PlaybookAccountGroup3Item pag3i
		--				ON pag3i.AccountGroup3Key = DimAccount.FirstAlternateAccountGroup3Key
		--				AND pag3i.ItemKey = DimItem.ItemKey
		--		' 
		--	END			
			
		--END

		IF @PricingRuleAttributeSequenceNumber = '1'
		BEGIN
			
			IF @PricingRuleAttributeTypeOverride = 'Y'
			BEGIN
				SET @TempFieldList = @PricingRuleAttributeType
				SET @TempFieldListAlternate = @PricingRuleAttributeType
			END
			ELSE
			BEGIN
				SELECT @TempFieldList =
					CASE 
						WHEN @DefaultValueForMatch IS NOT NULL THEN 'COALESCE(CAST(' + @PricingRuleAttributeSourceTable + '.' + @PricingRuleAttributeType + ' as varchar(255)), ''' + @DefaultValueForMatch + ''', '''') '
						ELSE 'CAST(' + @PricingRuleAttributeSourceTable + '.' + @PricingRuleAttributeType + ' as varchar(255)) '
					END
			
				IF @PricingRuleAttributeSourceTable = 'DimAccount' AND @PricingRuleAttributeType = 'AccountUDVarChar7'			
					SET @TempFieldListAlternate = 'N'  -- for national accounts, alternatively get the "local" band - more yuck
				ELSE
					SELECT @TempFieldListAlternate =
						CASE 
							WHEN @DefaultValueForMatch IS NOT NULL THEN 'COALESCE(CAST(' + @PricingRuleAttributeSourceTable + '.' + @PricingRuleAttributeType + ' as varchar(255)), ''' + @DefaultValueForMatch + ''', '''') '
							ELSE 'CAST(' + @PricingRuleAttributeSourceTable + '.' + @PricingRuleAttributeType + ' as varchar(255)) '
						END
				END
		END
		ELSE
		BEGIN

			IF @PricingRuleAttributeTypeOverride = 'Y'
			BEGIN
				SET @TempFieldList =  @TempFieldList + ' + '','' + ' + @PricingRuleAttributeType
				SET @TempFieldListAlternate =  @TempFieldListAlternate + ' + '','' + ' + @PricingRuleAttributeType
			END
			ELSE
			BEGIN
				SELECT @TempFieldList = @TempFieldList + 
					CASE 
						WHEN @DefaultValueForMatch IS NOT NULL THEN ' + '','' + COALESCE(CAST(' + @PricingRuleAttributeSourceTable + '.' + @PricingRuleAttributeType + ' as varchar(255)), ''' + @DefaultValueForMatch + ''', '''') '
						ELSE ' + '','' +CAST(' + @PricingRuleAttributeSourceTable + '.' + @PricingRuleAttributeType + ' as varchar(255)) '
					END
			
				IF @PricingRuleAttributeSourceTable = 'DimAccount' AND @PricingRuleAttributeType = 'AccountUDVarChar7'			
					SET @TempFieldListAlternate = @TempFieldListAlternate + ' + '','' + ''N'' '  -- for national accounts, alternatively get the "local" band
				ELSE 
					SELECT @TempFieldListAlternate = @TempFieldListAlternate + 
						CASE 
							WHEN @DefaultValueForMatch IS NOT NULL THEN ' + '','' + COALESCE(CAST(' + @PricingRuleAttributeSourceTable + '.' + @PricingRuleAttributeType + ' as varchar(255)), ''' + @DefaultValueForMatch + ''', '''') '
							ELSE ' + '','' + CAST(' + @PricingRuleAttributeSourceTable + '.' + @PricingRuleAttributeType + ' as varchar(255)) '
						END
			END 
		END	

		IF @PricingRuleAttributeSourceTable = 'FactLastPriceAndCost' AND @TableName <> 'FactLastPriceAndCost' AND @TempJoinList NOT LIKE '%LEFT JOIN FactLastPriceAndCost ON ' + @TableName + '.AccountKey = FactLastPriceAndCost.AccountKey AND ' + @TableName + '.ItemKey = FactLastPriceAndCost.ItemKey AND ' + @TableName + '.InvoiceLineGroup1Key = FactLastPriceAndCost.InvoicelineGroup1Key%'
		BEGIN
			SET @TempJoinList = @TempJoinList + 'LEFT JOIN FactLastPriceAndCost ON ' + @TableName + '.AccountKey = FactLastPriceAndCost.AccountKey AND ' + @TableName + '.ItemKey = FactLastPriceAndCost.ItemKey AND ' + @TableName + '.InvoiceLineGroup1Key = FactLastPriceAndCost.InvoicelineGroup1Key ' 
		END

		IF @PricingRuleAttributeSourceTable = 'FactRebate' AND @TempJoinList NOT LIKE '%LEFT JOIN FactRebate ON FactRebate.AccountKey = ' + @TableName + '.AccountKey AND FactRebate.ItemKey = ' + @TableName + '.ItemKey%'
		BEGIN
			SET @TempJoinList = @TempJoinList + 'LEFT JOIN FactRebate ON FactRebate.AccountKey = ' + @TableName + '.AccountKey AND FactRebate.ItemKey = ' + @TableName + '.ItemKey ' 
		END
		
		IF @PricingRuleAttributeSourceTable = 'DimAccount' AND @TempJoinList NOT LIKE '%DimAccount%'
		BEGIN
			SET @TempJoinList = @TempJoinList + 'JOIN DimAccount ON DimAccount.AccountKey = ' + @TableName + '.AccountKey ' 
		END

		IF @PricingRuleAttributeSourceTable = 'DimItem' AND @TempJoinList NOT LIKE '%DimItem%'
		BEGIN
			SET @TempJoinList = @TempJoinList + 'JOIN DimItem ON DimItem.ItemKey = ' + @TableName + '.ItemKey ' 
		END

		IF @PricingRuleAttributeSourceTable = 'DimAccountGroup1' AND @TempJoinList NOT LIKE '%DimAccountGroup1%'
		BEGIN
			IF @TempJoinList NOT LIKE '%JOIN DimAccount ON DimAccount.AccountKey = ' + @TableName + '.AccountKey%'
				SET @TempJoinList = @TempJoinList + 'JOIN DimAccount ON DimAccount.AccountKey = ' + @TableName + '.AccountKey ' 
			IF @TempJoinList NOT LIKE '%JOIN DimAccountGroup1 ON DimAccountGroup1.AccountGroup1Key = DimAccount.AccountGroup1Key%'
				SET @TempJoinList = @TempJoinList + 'JOIN DimAccountGroup1 ON DimAccountGroup1.AccountGroup1Key = DimAccount.AccountGroup1Key ' 
		END

		IF @PricingRuleAttributeSourceTable = 'DimAccountGroup2' AND @TempJoinList NOT LIKE '%DimAccountGroup2%'
		BEGIN
			IF @TempJoinList NOT LIKE '%JOIN DimAccount ON DimAccount.AccountKey = ' + @TableName + '.AccountKey%'
				SET @TempJoinList = @TempJoinList + 'JOIN DimAccount ON DimAccount.AccountKey = ' + @TableName + '.AccountKey ' 
			IF @TempJoinList NOT LIKE '%JOIN DimAccountGroup2 ON DimAccountGroup2.AccountGroup2Key = DimAccount.AccountGroup2Key%'
				SET @TempJoinList = @TempJoinList + 'JOIN DimAccountGroup2 ON DimAccountGroup2.AccountGroup2Key = DimAccount.AccountGroup2Key ' 
		END

		IF @PricingRuleAttributeSourceTable = 'DimAccountGroup3' AND @TempJoinList NOT LIKE '%DimAccountGroup3%'
		BEGIN
			IF @TempJoinList NOT LIKE '%JOIN DimAccount ON DimAccount.AccountKey = ' + @TableName + '.AccountKey%'
				SET @TempJoinList = @TempJoinList + 'JOIN DimAccount ON DimAccount.AccountKey = ' + @TableName + '.AccountKey ' 
			IF @TempJoinList NOT LIKE '%JOIN DimAccountGroup3 ON DimAccountGroup3.AccountGroup3Key = DimAccount.AccountGroup2Key%'
				SET @TempJoinList = @TempJoinList + 'JOIN DimAccountGroup2 ON DimAccountGroup2.AccountGroup2Key = DimAccount.AccountGroup3Key ' 
		END

		IF @PricingRuleAttributeSourceTable = 'DimItemGroup1' AND @TempJoinList NOT LIKE '%DimItemGroup1%'
		BEGIN
			IF @TempJoinList NOT LIKE '%JOIN DimItem ON DimItem.ItemKey = ' + @TableName + '.ItemKey%'
				SET @TempJoinList = @TempJoinList + 'JOIN DimItem ON DimItem.ItemKey = ' + @TableName + '.ItemKey ' 
			IF @TempJoinList NOT LIKE '%JOIN DimItemGroup1 ON DimItemGroup1.ItemGroup1Key = DimItem.ItemGroup1Key%'
				SET @TempJoinList = @TempJoinList + 'JOIN DimItemGroup1 ON DimItemGroup1.ItemGroup1Key = DimItem.ItemGroup1Key ' 
		END

		IF @PricingRuleAttributeSourceTable = 'DimItemGroup2' AND @TempJoinList NOT LIKE '%DimItemGroup2%'
		BEGIN
			IF @TempJoinList NOT LIKE '%JOIN DimItem ON DimItem.ItemKey = ' + @TableName + '.ItemKey%'
				SET @TempJoinList = @TempJoinList + 'JOIN DimItem ON DimItem.ItemKey = ' + @TableName + '.ItemKey ' 
			IF @TempJoinList NOT LIKE '%JOIN DimItemGroup2 ON DimItemGroup2.ItemGroup2Key = DimItem.ItemGroup2Key%'
				SET @TempJoinList = @TempJoinList + 'JOIN DimItemGroup2 ON DimItemGroup2.ItemGroup2Key = DimItem.ItemGroup2Key ' 
		END

		IF @PricingRuleAttributeSourceTable = 'DimItemGroup3' AND @TempJoinList NOT LIKE '%DimItemGroup3%'
		BEGIN
			IF @TempJoinList NOT LIKE '%JOIN DimItem ON DimItem.ItemKey = ' + @TableName + '.ItemKey%'
				SET @TempJoinList = @TempJoinList + 'JOIN DimItem ON DimItem.ItemKey = ' + @TableName + '.ItemKey ' 
			IF @TempJoinList NOT LIKE '%JOIN DimItemGroup3 ON DimItemGroup3.ItemGroup3Key = DimItem.ItemGroup3Key%'
				SET @TempJoinList = @TempJoinList + 'JOIN DimItemGroup3 ON DimItemGroup3.ItemGroup3Key = DimItem.ItemGroup3Key ' 
		END
		
		IF @PricingRuleAttributeSourceTable = 'DimVendor' AND @TempJoinList NOT LIKE '%DimVendor%'
		BEGIN
			IF @TempJoinList NOT LIKE '%JOIN DimItem ON DimItem.ItemKey = ' + @TableName + '.ItemKey%'
				SET @TempJoinList = @TempJoinList + 'JOIN DimItem ON DimItem.ItemKey = ' + @TableName + '.ItemKey ' 
			IF @TempJoinList NOT LIKE '%JOIN DimVendor ON DimVendor.VendorKey = DimItem.ItemVendorKey%'
				SET @TempJoinList = @TempJoinList + 'JOIN DimVendor ON DimVendor.VendorKey = DimItem.ItemVendorKey ' 
		END		

		IF @PricingRuleAttributeSourceTable = 'DimInvoiceLineGroup1' AND @TempJoinList NOT LIKE '%DimInvoiceLineGroup1%'
		BEGIN
			SET @TempJoinList = @TempJoinList + 'JOIN DimInvoiceLineGroup1 ON DimInvoiceLineGroup1.InvoiceLineGroup1Key = ' + @TableName + '.InvoiceLineGroup1Key ' 
		END

		IF @PricingRuleAttributeSourceTable = 'DimInvoiceLineGroup2' AND @TempJoinList NOT LIKE '%DimInvoiceLineGroup2%'
		BEGIN
			SET @TempJoinList = @TempJoinList + 'JOIN DimInvoiceLineGroup2 ON DimInvoiceLineGroup2.InvoiceLineGroup2Key = ' + @TableName + '.LastInvoiceLineGroup2Key ' 
		END		
		
		IF @PricingRuleAttributeSourceTable = 'DimInvoiceLineGroup3' AND @TempJoinList NOT LIKE '%DimInvoiceLineGroup3%'
		BEGIN
			SET @TempJoinList = @TempJoinList + 'JOIN DimInvoiceLineGroup3 ON DimInvoiceLineGroup3.InvoiceLineGroup3Key = ' + @TableName + '.LastInvoiceLineGroup3Key ' 
		END		
		
		
		
		FETCH NEXT FROM PricingRuleAttributeCursor INTO @PricingRuleAttributeSequenceNumber, @PricingRuleAttributeType, @PricingRuleAttributeSourceTable, @DefaultValueForMatch
		
	END 

	CLOSE PricingRuleAttributeCursor
	DEALLOCATE PricingRuleAttributeCursor

	IF @ParamDebug = 'Y'
	BEGIN 
		RAISERROR ('@TempFieldList: %s', 0, 1, @TempFieldList) WITH NOWAIT
		RAISERROR ('@TempJoinList: %s', 0, 1, @TempJoinList) WITH NOWAIT
	END
	
			
	SET @SQLString = '
		; WITH AttiributeData AS (
			SELECT DISTINCT
				' + @TableName + '.AccountKey,
				' + @TableName + '.ItemKey,
				' + @TableName + '.InvoiceLineGroup1Key,
				' + @TempFieldList + ' AS MatchStrring
			FROM ' + @TableName + '
				' + @TempJoinList + '
			WHERE 1=1
				' + @WhereClause + '
				AND ' + @TableName + '.' + @TargetColumn + ' IS NULL
		)
		UPDATE t
		SET
			t.' + @TargetColumn + ' = ppg.PlaybookPricingGroupKey
		FROM 
			' + @TableName + ' t
		INNER JOIN AttiributeData ad
			ON ad.AccountKey = t.AccountKey
			AND ad.ItemKey = t.ItemKey
			AND ad.InvoiceLineGroup1Key = t.InvoiceLineGroup1Key
		INNER JOIN PlaybookPricingGroup ppg 
			ON PlaybookDataPointGroupKey = ' + CAST(@PlaybookDataPointGroupKey AS VARCHAR(25)) + '
			AND ppg.GroupValues = ad.MatchStrring '

	IF @ParamDebug = 'Y'
	BEGIN 
		RAISERROR ('@SQLString:...', 0, 1) WITH NOWAIT
		RAISERROR ('%s', 0, 1, @SQLString) WITH NOWAIT
	END
	
	EXECUTE sp_executesql @SQLString

	IF @AlternateTargetColumn IS NOT NULL AND @AlternateTargetColumn <> ''
	BEGIN
		SET @SQLString = '
			; WITH AttiributeData AS (
				SELECT DISTINCT
					' + @TableName + '.AccountKey,
					' + @TableName + '.ItemKey,
					' + @TableName + '.InvoiceLineGroup1Key,
					' + @TempFieldListAlternate + ' AS MatchStrring
				FROM ' + @TableName + '
					' + @TempJoinList + '
				WHERE 1=1
					' + @WhereClause + '				
					AND ' + @TableName + '.' + @AlternateTargetColumn + ' IS NULL
					--AND AccountUDVarChar7 = ''Y''
			)
			UPDATE t
			SET
				t.' + @AlternateTargetColumn + ' = ppg.PlaybookPricingGroupKey
			FROM 
				' + @TableName + ' t
			INNER JOIN AttiributeData ad
				ON ad.AccountKey = t.AccountKey
				AND ad.ItemKey = t.ItemKey
				AND ad.InvoiceLineGroup1Key = t.InvoiceLineGroup1Key
			INNER JOIN PlaybookPricingGroup ppg 
				ON PlaybookDataPointGroupKey = ' + CAST(@PlaybookDataPointGroupKey AS VARCHAR(25)) + '
				AND ppg.GroupValues = ad.MatchStrring '
			-- AlternateRPBPlaybookPricingGroupKey

		IF @ParamDebug = 'Y'
		BEGIN 
			RAISERROR ('@SQLString:...', 0, 1) WITH NOWAIT
			RAISERROR ('%s', 0, 1, @SQLString) WITH NOWAIT
		END
		
		
		
		EXECUTE sp_executesql @SQLString
	END
	
	
	FETCH NEXT FROM PricingRuleCursor INTO @PricingRuleKey, @PricingRuleSequenceNumber

END

CLOSE PricingRuleCursor
DEALLOCATE PricingRuleCursor

EXEC LogDCPEvent 'DCP --- dbo.Load_SetRPBPlaybookPricingGroupKey', 'E', @RowsI, @RowsU, @RowsD





GO
