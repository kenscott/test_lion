SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO










CREATE PROCEDURE [lion].[Load_AccountContractTerms]
AS

/*

EXEC lion.Load_AccountContractTerms

*/


SET ANSI_WARNINGS OFF
SET ARITHABORT OFF
SET ARITHIGNORE ON

SET NOCOUNT ON

EXEC LogDCPEvent 'ETL - Load_AccountContractTerms', 'B'


DECLARE 
	@RowsI Quantity_Normal_type, 
	@RowsU Quantity_Normal_type, 
	@RowsD Quantity_Normal_type

SET @RowsI = 0
SET @RowsU = 0
SET @RowsD = 0


TRUNCATE TABLE lion.AccountContractTerms

; WITH Data AS (
	SELECT DISTINCT
		ItemGroup3Key,
		ItemKey,
		ContractName,
		SPG,
		SPGDescription,
		SPGExpiryDate,
		UsualDiscount1,
		UsualDiscount2,
		ExceptionProduct,
		ExceptionProductDescription,
		ExceptionDiscount,
		ExceptionFixedPrice,
		FixedPricePer,
		ReleaseDate,
		ROW_NUMBER() OVER (
			PARTITION BY 
				ContractName,
				ItemGroup3Key,
				ItemKey
			ORDER BY 
				act.UsualDiscount1 DESC,
				act.UsualDiscount2 DESC,
				act.ExceptionDiscount DESC,
				act.ExceptionFixedPrice DESC
		) AS RowRank
	FROM Lion_Staging.dbo.AccountContractTerms act
	INNER JOIN DimItem di
		ON di.ItemNumber = CAST(CAST(act.PyramidCode AS INT) AS NVARCHAR(2)) + act.ExceptionProduct
)
INSERT INTO lion.AccountContractTerms
(
	ItemGroup3Key,
	ItemKey,
	ContractName,
	SPG,
	SPGDescription,
	SPGExpiryDate,
	UsualDiscount1,
	UsualDiscount2,
	ExceptionProduct,
	ExceptionProductDescription,
	ExceptionDiscount,
	ExceptionFixedPrice,
	FixedPricePer,
	ReleaseDate
)
SELECT DISTINCT
	ItemGroup3Key,
	ItemKey,
	ContractName,
	SPG,
	SPGDescription,
	SPGExpiryDate,
	UsualDiscount1,
	UsualDiscount2,
	ExceptionProduct,
	ExceptionProductDescription,
	ExceptionDiscount,
	ExceptionFixedPrice,
	FixedPricePer,
	ReleaseDate
FROM Data
WHERE Data.RowRank = 1
SET @RowsI = @RowsI + @@RowCount



; WITH Data AS (
	SELECT DISTINCT
		dig3.ItemGroup3Key AS ItemGroup3Key,
		1 AS ItemKey,
		ContractName,
		SPG,
		SPGDescription,
		SPGExpiryDate,
		UsualDiscount1,
		UsualDiscount2,
		ExceptionProduct,
		ExceptionProductDescription,
		ExceptionDiscount,
		ExceptionFixedPrice,
		FixedPricePer,
		ReleaseDate,
		ROW_NUMBER() OVER (
			PARTITION BY 
				ContractName,
				dig3.ItemGroup3Key
				--ItemKey				
			ORDER BY 
				act.UsualDiscount1 DESC,
				act.UsualDiscount2 DESC,
				act.ExceptionDiscount DESC,
				act.ExceptionFixedPrice DESC
		) AS RowRank
	FROM Lion_Staging.dbo.AccountContractTerms act
	INNER JOIN dbo.DimItemGroup3 dig3
		ON dig3.IG3Level1 = act.SPG
		AND dig3.IG3Level4 = CAST(CAST(act.PyramidCode AS INT) AS NVARCHAR(2)) 
	WHERE
		ISNULL(act.ExceptionProduct, N'') =  N''  -- only go the spg path if no item specified
)
INSERT INTO lion.AccountContractTerms
(
	ItemGroup3Key,
	ItemKey,
	ContractName,
	SPG,
	SPGDescription,
	SPGExpiryDate,
	UsualDiscount1,
	UsualDiscount2,
	ExceptionProduct,
	ExceptionProductDescription,
	ExceptionDiscount,
	ExceptionFixedPrice,
	FixedPricePer,
	ReleaseDate
)
SELECT DISTINCT
	ItemGroup3Key,
	ItemKey,
	ContractName,
	SPG,
	SPGDescription,
	SPGExpiryDate,
	UsualDiscount1,
	UsualDiscount2,
	ExceptionProduct,
	ExceptionProductDescription,
	ExceptionDiscount,
	ExceptionFixedPrice,
	FixedPricePer,
	ReleaseDate
FROM Data
WHERE Data.RowRank = 1
SET @RowsI = @RowsI + @@RowCount


SELECT TOP 10 * FROM lion.AccountContractTerms


EXEC LogDCPEvent 'ETL - Load_AccountContractTerms', 'E', @RowsI, @RowsU, @RowsD
















GO
