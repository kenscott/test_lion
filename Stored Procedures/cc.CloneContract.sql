SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [cc].[CloneContract](@ContractKey INT, @WebUserKey INT )
/*
Purpose: Based on the contractkey and webuserkey, creates a new contract.

JIRA: LPF-2874 Create stored procedure to clone an existing contract.
	
Application:  This is used primarily by the clone contract feature within the
			  Contract Claims (Manager) application.

Sample usage:  exec cc.CloneContract 675863, 1

Examples:

	select top 10 * from cc.SupplierContract order by contractkey desc
	select * from cc.suppliercontractstatus

	DECLARE @ContractKey INT
	SET @ContractKey = 546623
		
	select * from cc.SupplierContract where contractkey = @ContractKey
	select * from cc.suppliercontractaccount where contractkey = @ContractKey
	select * from cc.suppliercontractlocation where contractkey = @ContractKey
	select * from cc.suppliercontractitem where contractkey = @ContractKey
	select * from cc.suppliercontractitemgroupx where contractkey = @ContractKey
	 
	select * from webuser wu
		join webuserrole wur on wur.webuserkey = wu.webuserkey
		join webrole wr on wr.webrolekey = wur.webrolekey
	where username = 'james.dameron' 
	
	
	rollback tran

	dbcc opentran		
*/

AS 




-- For Testing...
--DECLARE @ContractKey INT
--DECLARE @WebUserKey INT 
--SET @ContractKey = 13346
--SET @WebUserKey = 1972

DECLARE @Originator NVARCHAR(250)
DECLARE @ContractOwner NVARCHAR(250)
DECLARE @ContractNumber NVARCHAR(250)
DECLARE @NewContractKey INT 
DECLARE @ErrorMessage NVARCHAR(250)
DECLARE @vendorKey INT
DECLARE @contractID VARCHAR(9)

SET NOCOUNT ON

SELECT @ContractOwner = ContractOwner FROM cc.SupplierContract sc WHERE sc.ContractKey = @ContractKey

SELECT @ErrorMessage = ISNULL(@ErrorMessage, '') + 'All Accounts Invalid For Supplier ' + dv.VendorNumber + '; '
	FROM cc.SupplierContract sc
	INNER JOIN dbo.DimVendor dv 
		ON dv.VendorKey = sc.VendorKey
	WHERE sc.ContractKey = @ContractKey
		AND dv.AllowAllAccounts = 'Never'
		AND sc.IncludeAllAccounts = 1

SELECT @ErrorMessage = ISNULL(@ErrorMessage, '') + 'All Locations Invalid For Supplier ' + dv.VendorNumber + '; '
	FROM cc.SupplierContract sc
	INNER JOIN dbo.DimVendor dv 
		ON dv.VendorKey = sc.VendorKey
	WHERE sc.ContractKey = @ContractKey
		AND dv.AllowAllLocations = 'Never'
		AND sc.IncludeAllLocations = 1

IF @ErrorMessage IS NULL
BEGIN

BEGIN TRAN
/* 
	Clone the contract into a temp table and reset values accordingly 
	select top 10 ContractNumber, * from cc.SupplierContract
*/
SELECT * INTO #TempContract FROM cc.SupplierContract WHERE ContractKey = @ContractKey
ALTER TABLE #TempContract DROP COLUMN ContractKey


-- Need to determine who the Originator is based ont he webuserkey passed in (LPF-3169)
IF @WebUserKey IN
	( SELECT WebUserKey
		FROM WebUserRole wur
		JOIN WebRole wr ON wr.WebRoleKey = wur.WebRoleKey
	    WHERE wr.WebRoleName = 'ContractCoordinator'   --Contract Coordinator
	)
	BEGIN
		SELECT @Originator = 'WUK'
	END
ELSE
	BEGIN
		SELECT @Originator = AG1Level1 
			FROM WebUser wu
			JOIN DimAccountManager dam on dam.AccountManagerKey = wu.AccountManagerKey
			JOIN DimAccountGroup1 dag1 on dag1.AccountGroup1Key = dam.PrimaryAccountGroup1Key
			WHERE wu.WebUserKey = @WebUserKey
	END

--LPF-4619 When Cloning a BA Contract, set Originator and Owner to WUK BEGIN
IF @Originator = 'BA'
	BEGIN
		SELECT @Originator = 'WUK'
		SELECT @ContractOwner = 'WUK'
	END
--LPF-4619 When Cloning a BA Contract, set Originator and Owner to WUK END


-- Get the next contract number based on the originator
-- SELECT @Originator = Originator FROM cc.SupplierContract WHERE ContractKey = @ContractKey
CREATE TABLE #TempContractNumber ([ContractNumberCounter] VARCHAR(10))
INSERT INTO #TempContractNumber EXEC cc.GetNextContractNumber @Originator; 
SELECT @ContractNumber = ContractNumberCounter FROM #TempContractNumber
DROP TABLE #TempContractNumber

-- Change the values that need to be changed or removed
UPDATE #TempContract 
	SET StatusKey = 2, 
		CreatedByWebUserKey = @WebUserKey, 
		ModifiedByWebUserKey = @WebUserKey,
		ContractCreationDT = GETDATE(),
		ContractSourceKey = 2,
		ContractNumber = @ContractNumber,
		Originator = @Originator,
		ContractOwner = @ContractOwner
		  
INSERT INTO cc.SupplierContract (
            [Originator]
           ,[ContractNumber]
           ,[ContractOwner]
           ,[VendorKey]
           ,[StartDate]
           ,[ExpirationDate]
           ,[JobSite]
           ,[PyramidCode]
           ,[ContractType1]
           ,[ContractLevel]
           ,[JobFlag]
           ,[ContractSourceKey]
           ,[CreatedByWebUserKey]
           ,[ModifiedByWebUserKey]
           ,[ContractCreationDT]
           ,[Developer]
           ,[PricingContractIndicator]
           ,[PricingContractID]
           ,[PurchaseType]
           ,[ContactName]
           ,[ContactEmail]
           ,[IncludeAllAccounts]
           ,[ExcludeCashAccounts]
           ,[StatusKey]
           ,[IncludeAllLocations] ) 
       SELECT 
		    [Originator]
           ,[ContractNumber]
           ,[ContractOwner]
           ,[VendorKey]
           ,[StartDate]
           ,[ExpirationDate]
           ,[JobSite]
           ,[PyramidCode]
           ,[ContractType1]
           ,[ContractLevel]
           ,[JobFlag]
           ,[ContractSourceKey]
           ,[CreatedByWebUserKey]
           ,[ModifiedByWebUserKey]
           ,[ContractCreationDT]
           ,[Developer]
           ,[PricingContractIndicator]
           ,[PricingContractID]
           ,[PurchaseType]
           ,[ContactName]
           ,[ContactEmail]
           ,[IncludeAllAccounts]
           ,[ExcludeCashAccounts]
           ,[StatusKey]
           ,[IncludeAllLocations] 
		FROM #TempContract

SET @NewContractKey = @@IDENTITY

SELECT @vendorKey = VendorKey FROM #TempContract
SELECT @contractID = contractID FROM #TempContract
--SELECT @NewContractKey AS NewContractKey

DROP TABLE #TempContract

/* 
	Clone the contract account 
	Select top 10 * from cc.SupplierContractAccount
*/
SELECT * INTO #TempContractAccount FROM cc.SupplierContractAccount WHERE ContractKey = @ContractKey
ALTER TABLE #TempContractAccount DROP COLUMN ContractAccountKey
UPDATE #TempContractAccount
	SET ContractKey = @NewContractKey,
		CreatedByWebUserKey = @WebUserKey,
		ModifiedByWebUserKey = @WebUserKey
		
INSERT INTO cc.SupplierContractAccount (
			[ContractKey]
           ,[AccountKey]
           ,[StartDate]
           ,[ExpirationDate]
           ,[CreatedByWebUserKey]
           ,[CreatedOnDT]
           ,[ModifiedByWebUserKey]
           ,[ModifiedOnDT]) SELECT * FROM #TempContractAccount
DROP TABLE #TempContractAccount

--SELECT * FROM cc.SupplierContractAccount Where Contractkey = @NewContractKey

/* 
	Clone the Contract item 
	Select top 10 * from cc.SupplierContractItem
*/
SELECT * INTO #TempContractItem FROM cc.SupplierContractItem WHERE ContractKey = @ContractKey
ALTER TABLE #TempContractItem DROP COLUMN ContractItemKey
UPDATE #TempContractItem
	SET ContractKey = @NewContractKey,
		CreatedByWebUserKey = @WebUserKey,
		ModifiedByWebUserKey = @WebUserKey
		
INSERT INTO cc.SupplierContractItem (
			[ContractKey]
           ,[ItemKey]
           ,[ClaimBackAmount]
           ,[ClaimBackPerc]
           ,[StartDT]
           ,[ExpiryDT]
           ,[ClaimCost]
           ,[ClaimType]
		   ,[LimitType]
           ,[LimitValue]
           ,[LimitCurrentValue]
           ,[CreatedByWebUserKey]
           ,[CreatedOnDT]
           ,[ModifiedByWebUserKey]
           ,[ModifiedOnDT]
           ,[GreenClaimBackPercent]
           ,[AmberClaimBackPercent]
           ,[RedClaimBackPercent]
           ,[GreenClaimBackAmount]
           ,[AmberClaimBackAmount]
           ,[RedClaimBackAmount]
           ,[PlaybookPricingGroupKey]
		   ,[LimitCurrentRatio]
		   ,[LimitPreviousRatio]
		   ,[CurrentMonthTally]
		   ,[CurrentMonthTallyOld]
		   ,[PreviousMonthTally]
           ) 
SELECT ContractKey,
       ItemKey,
       ClaimBackAmount,
       ClaimBackPerc,
       StartDT,
       ExpiryDT,
       ClaimCost,
       ClaimType,
       LimitType,
       LimitValue,
       LimitCurrentValue,
       CreatedByWebUserKey,
       CreatedOnDT,
       ModifiedByWebUserKey,
       ModifiedOnDT,
       GreenClaimBackPercent,
       AmberClaimBackPercent,
       RedClaimBackPercent,
       GreenClaimBackAmount,
       AmberClaimBackAmount,
       RedClaimBackAmount,
       PlaybookPricingGroupKey,
       LimitCurrentRatio,
       LimitPreviousRatio,
       CurrentMonthTally,
       CurrentMonthTallyOld,
       PreviousMonthTally
FROM #TempContractItem

DROP TABLE #TempContractItem

--SELECT * FROM cc.SupplierContractItem Where Contractkey = @NewContractKey
--select * from #TempContractItem

/* 
	Clone the ContractLocation 
	Select top 10 * from cc.SupplierContractLocation
*/
SELECT * INTO #TempContractLocation FROM cc.SupplierContractLocation WHERE ContractKey = @ContractKey
ALTER TABLE #TempContractLocation DROP COLUMN ContractLocationKey
UPDATE #TempContractLocation
	SET ContractKey = @NewContractKey,
		CreatedByWebUserKey = @WebUserKey,
		ModifiedByWebUserKey = @WebUserKey
		
INSERT INTO cc.SupplierContractLocation (
			[ContractKey]
           ,[ContractLocationLevelKey]
           ,[LocationKey]
           ,[StartDate]
           ,[ExpirationDate]
           ,[CreatedByWebUserKey]
           ,[CreatedOnDT]
           ,[ModifiedByWebUserKey]
           ,[ModifiedOnDT]
           ,[LocationName]
           ,[LocationDescription]) SELECT * FROM #TempContractLocation
DROP TABLE #TempContractLocation

--SELECT * FROM cc.SupplierContractLocation Where Contractkey = @NewContractKey

/* 
	Clone the ContractItemGroupX
	Select top 10 * from cc.SupplierContractItemGroupX
*/
SELECT * INTO #TempContractItemGroupX FROM cc.SupplierContractItemGroupX WHERE ContractKey = @ContractKey
ALTER TABLE #TempContractItemGroupX DROP COLUMN ContractItemGroupXKey
UPDATE #TempContractItemGroupX
	SET ContractKey = @NewContractKey,
		CreatedByWebUserKey = @WebUserKey,
		ModifiedByWebUserKey = @WebUserKey
		
INSERT INTO cc.SupplierContractItemGroupX (
			[ContractKey]
           ,[ItemGroupLevel]
           ,[ItemGroupXKey]
           ,[ClaimBackAmount]
           ,[ClaimBackPerc]
           ,[StartDT]
           ,[ExpiryDT]
           ,[ClaimCost]
           ,[ClaimType]
		   ,[LimitType]
           ,[LimitValue]
           ,[LimitCurrentValue]
           ,[CreatedByWebUserKey]
           ,[CreatedOnDT]
           ,[ModifiedByWebUserKey]
           ,[ModifiedOnDT]
           ,[GreenClaimBackPercent]
           ,[AmberClaimBackPercent]
           ,[RedClaimBackPercent]
		   ,[GreenClaimBackAmount]
		   ,[AmberClaimBackAmount]
		   ,[RedClaimBackAmount]
           ,[PlaybookPricingGroupKey]
		   ,[LimitCurrentRatio]
           ,[LimitPreviousRatio]
           ,[CurrentMonthTally]
           ,[CurrentMonthTallyOld]
           ,[PreviousMonthTally]
           ) 
		   SELECT [ContractKey],
           [ItemGroupLevel],
           [ItemGroupXKey],
           [ClaimBackAmount],
           [ClaimBackPerc],
           [StartDT],
           [ExpiryDT],
           [ClaimCost],
           [ClaimType],
           [LimitType],
           [LimitValue],
           [LimitCurrentValue],
           [CreatedByWebUserKey],
           [CreatedOnDT],
           [ModifiedByWebUserKey],
           [ModifiedOnDT],
           [GreenClaimBackPercent],
           [AmberClaimBackPercent],
           [RedClaimBackPercent],
           [GreenClaimBackAmount],
           [AmberClaimBackAmount],
           [RedClaimBackAmount],
           [PlaybookPricingGroupKey],
           [LimitCurrentRatio],
           [LimitPreviousRatio],
           [CurrentMonthTally],
           [CurrentMonthTallyOld],
           [PreviousMonthTally]
    FROM #TempContractItemGroupX
DROP TABLE #TempContractItemGroupX

--SELECT * FROM cc.SupplierContractItemGroupX Where Contractkey = @NewContractKey

/* 
	Insert a log record into the new 
	select top 10 * from cc.SupplierContractLog
*/
INSERT  INTO cc.SupplierContractLog
        ( WebUserKey ,
			 ContractKey ,
			 ContractID,
			 vendorKey,
			 Notes ,
			 ContractAction,
			 CreationDate
        )
VALUES  ( @WebUserKey ,
		  @NewContractKey, 
		  @contractID,
		  @vendorKey,
         'Contract was cloned from ' + CAST(@contractID AS VARCHAR(20)) ,        
          'Clone Contract',
		  GETDATE()
        )

COMMIT TRAN

END

SELECT @NewContractKey AS NewContractKey, @ErrorMessage as ErrorMessage










GO
