SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





CREATE PROCEDURE [dbo].[Update_ODSPerson]
	@FullName Description_Normal_Type, 
	@Email Description_Normal_Type,
	@ODSPersonKey Key_Normal_Type

AS

/*   IMPORTANT:
	For each client, override this procedure (i.e. bearcat.Update_ODSPerson) and call the 
	client specific Load_DimPerson (i.e. bearcat.Load_DimPerson)
*/

SET NOCOUNT OFF

--This updates the ODSPersons info
UPDATE ODSPerson SET 
	FullName = @FullName,
	Email = LTRIM(RTRIM(NULLIF(@Email, '')))
WHERE ODSPersonKey = @ODSPersonKey



--This updates the datamart with the new info
--EXEC <client>.Load_DimPerson
EXEC dbo.Load_DimPerson









GO
