SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO














CREATE                                            
PROCEDURE [dbo].[P2P_PROS_CreateProposal]
	@ProjectKey Key_Normal_type, 
	@PlaybookDataPointGroupKey Key_Normal_type, 
	@LimitDataPlaybookKey Key_Normal_type, 
	@PricingRulePlaybookKey Key_Normal_type, 
	@ProposalRecommendationPlaybookKey Key_Normal_type,
	@BeginStep INT,
	@EndStep INT
AS

SET NOCOUNT ON


EXEC LogDCPEvent 'DCP - P2P_PROS_CreateProposal: Delete', 'B', 0, 0, 0, @ProjectKey


DECLARE
	@RowsI Quantity_Normal_type, 
	@RowsU Quantity_Normal_type, 
	@RowsD Quantity_Normal_type, 
	@Pass INT,
	@ForecastType Description_Small_type,
	@ProposalRecommendationKey Key_Normal_type,
	@DeferredAcceptanceCodeKey Key_Normal_type,
	@WhereClause2 Description_Huge_Type, 
	@JoinClause2 Description_Huge_Type, 
	@AcceptRejectCode_PriceProposal Description_Big_type,
	@ProposedPriceOverride Description_Big_type, 
	@ReasonCode Description_Big_type,
	@EffectiveDate Description_Big_type,
	@SuppressionDate Description_Big_type,
	@DynamicUDVarchar1 Description_Small_type,
	@DynamicUDVarchar2 Description_Small_type,
	@PriceIncreaseCapPercent Percent_Type,
	@MsgStr NVARCHAR(2000),
	@ProposedPrice NVARCHAR(MAX),
	@SQLStmt NVARCHAR(MAX)	


SET @RowsI = 0
SET @RowsU = 0
SET @RowsD = 0


SELECT @ForecastType = ForecastType 								-- u
FROM ATKProposalRecommendationPlaybook
WHERE ProposalRecommendationPlaybookKey = @ProposalRecommendationPlaybookKey


SELECT @ProposalRecommendationKey = ProposalRecommendationKey 
FROM ATKProposalRecommendation
WHERE ProposalRecommendationPlaybookKey = @ProposalRecommendationPlaybookKey


SELECT @DeferredAcceptanceCodeKey = AcceptanceCodeKey 							-- u
FROM ATKAcceptanceCode 
WHERE AcceptanceCodeDesc = 'Deferred'


SELECT 
	@WhereClause2 = ISNULL(WhereClause2, ''),						-- u
	@JoinClause2= ISNULL(JoinClause2, ''),							-- u
	@AcceptRejectCode_PriceProposal = ISNULL(AcceptRejectCode_PriceProposal, '3'),		-- u
	@ProposedPriceOverride = ISNULL(ProposedPriceOverride, ''), 				-- u
	@ReasonCode = ISNULL(DynamicReasonCode, 'NULL'),					-- u
	@EffectiveDate = ISNULL(DynamicEffectiveDate, GETDATE()), 				-- u
	@SuppressionDate = ISNULL(DynamicSuppressionDate, GETDATE()), 				-- u
	@DynamicUDVarchar1 = ISNULL(DynamicUDVarchar1, 'NULL'), 				-- u
	@DynamicUDVarchar2 = ISNULL(DynamicUDVarchar2, 'NULL'), 				-- u
	@PriceIncreaseCapPercent = ISNULL(PriceIncreaseCapPercent, 9999999) 			-- u
FROM ATKProposalRecommendation
WHERE ProposalRecommendationKey = @ProposalRecommendationKey


IF @WhereClause2 <> '' AND @WhereClause2 NOT LIKE 'AND %'
	SET @WhereClause2 = 'AND ' + @WhereClause2



--If we're re-running a project, need to delete all the existing proposals for it first
IF @BeginStep <= 1 AND @EndStep >= 1
BEGIN
	--DELETE FROM FactProposalPrice WHERE ProjectKey = @ProjectKey
	--SET @RowsD = @RowsD + @@ROWCOUNT

	DELETE FROM ProposalPrice WHERE ProjectKey = @ProjectKey
	-- DELETE FROM ProposalPrice WHERE PlaybookDataPointGroupKey = @PlaybookDataPointGroupKey
	SET @RowsD = @RowsD + @@ROWCOUNT
	
	DELETE FROM PlaybookProjectFullScopeSummary WHERE ProjectKey = @ProjectKey
	SET @RowsD = @RowsD + @@ROWCOUNT
END

IF @BeginStep <= 2 AND @EndStep >= 2
BEGIN
	DELETE FROM PlaybookProjectAccountStatus WHERE ProjectKey = @ProjectKey
	SET @RowsD = @RowsD + @@RowCOunt
END

IF @BeginStep <= 3 AND @EndStep >= 3
BEGIN
	DELETE FROM PlaybookProjectAccountManagerStatus WHERE ProjectKey = @ProjectKey
	SET @RowsD = @RowsD + @@RowCOunt
END

--This always needs to be chunked, it gets created after ProposalPrice is populated
DELETE FROM PlaybookProjectFullScopeSummary WHERE ProjectKey = @ProjectKey
SET @RowsD = @RowsD + @@RowCount


EXEC LogDCPEvent 'DCP - P2P_PROS_CreateProposal: Delete', 'E', @RowsI, @RowsU, @RowsD 






IF @BeginStep <= 1 AND @EndStep >= 1
BEGIN


	DECLARE CurAutoSagePass CURSOR FOR
	SELECT 
		ProposalRecommendationPassSequenceNumber,
		LTRIM(RTRIM(ISNULL(WhereClause2, ''))),
		ISNULL(JoinClause2, ''),
		ISNULL(AcceptRejectCode_PriceProposal, '3'),
		ISNULL(ProposedPriceOverride, ''),
		ISNULL(DynamicReasonCode, 'NULL'),
		ISNULL(DynamicEffectiveDate, GETDATE()),
		ISNULL(DynamicSuppressionDate, GETDATE()),
		ISNULL(DynamicUDVarchar1, 'NULL'),
		ISNULL(DynamicUDVarchar2, 'NULL'),
		ISNULL(PriceIncreaseCapPercent, 9999999)
	FROM ATKProposalRecommendation
	WHERE ProposalRecommendationPlaybookKey = @ProposalRecommendationPlaybookKey
	ORDER BY ProposalRecommendationPassSequenceNumber

	OPEN CurAutoSagePass
	FETCH NEXT FROM CurAutoSagePass INTO @Pass, @WhereClause2, @JoinClause2, @AcceptRejectCode_PriceProposal, @ProposedPriceOverride, @ReasonCode, @EffectiveDate, @SuppressionDate, @DynamicUDVarchar1, @DynamicUDVarchar2, @PriceIncreaseCapPercent

	WHILE (@@FETCH_STATUS <> -1)
	BEGIN

		SET @MsgStr = 'DCP - P2P_PROS_CreateProposal: Insert ProposalPrice - AutoSage Pass: ' + CAST(@Pass AS NVARCHAR(22))
		EXEC LogDCPEvent @MsgStr, 'B', 0, 0, 0, @ProjectKey

		SET @RowsI = 0
		SET @RowsU = 0
		SET @RowsD = 0
	

		IF @ProposedPriceOverride = ''		
		BEGIN
			SET @ProposedPrice = '
				CASE
					WHEN FactPROSPrice.TargetPrice < FactLastPriceAndCost.LastItemPrice * (1.0 + '+CAST(@PriceIncreaseCapPercent AS NVARCHAR(22))+') THEN FactPROSPrice.TargetPrice
					ELSE FactLastPriceAndCost.LastItemPrice * (1.0 + '+CAST(@PriceIncreaseCapPercent AS NVARCHAR(22))+')
				END '
		END
		ELSE
		BEGIN
			SET @ProposedPrice = '
			'+@ProposedPriceOverride+'
			'
		END
	
		IF @WhereClause2 <> '' AND @WhereClause2 NOT LIKE 'AND %'
			SET @WhereClause2 = 'AND ' + @WhereClause2

	
		SET @SQLStmt = '
DECLARE 
	@RowsI Quantity_Normal_type
	
SET @RowsI = 0

INSERT INTO ProposalPrice(
	ProjectKey,
	PlaybookDataPointGroupKey,
	AccountKey,
	AccountManagerKey,
	ItemKey,
	InvoiceLineGroup1Key,
	PlaybookPriceProposalKey, 
	PlaybookMarginErosionProposalKey, 
	ProposedPrice,
	NewPrice, 
	AcceptanceCodeKey,
	UserAcceptanceCodeKey,
	DefaultAcceptanceCodeKey,
	TotalActual12MonthQuantity,
	TotalActual12MonthFrequency,
	TotalActual12MonthCost,
	TotalActual12MonthPrice,
	LastItemCost,
	LastItemPrice,
	Forecasted12MonthQuantity,
	TotalForecasted12MonthCost_LastCost,
	TotalForecasted12MonthPrice_LastPrice,
	TotalForecasted12MonthPrice_ProposedPrice,
	TotalForecasted12MonthPrice_NewPrice,
	TotalForecasted12MonthCost_RelevantCost,
	AcceptanceStatusDate,
	LastSaleDayKey,
	FactLastPriceAndCost.LastInvoiceLineGroup2Key,
	ReasonCodeKey,
	SuppressionDate,
	EffectiveDate,
	UDVarchar1,
	UDVarchar2,
	OptimalPrice
)
SELECT
	'+CAST(@ProjectKey AS NVARCHAR(22))+',
	'+CAST(@PlaybookDataPointGroupKey AS NVARCHAR(22))+',
	fpp.AccountKey,
	da.AccountManagerKey,
	fpp.ItemKey,
	fpp.InvoiceLineGroup1Key,
	NULL AS ''PlaybookPriceProposalKey'',
	NULL AS PlaybookMarginErosionProposalKey,
	'+@ProposedPrice+' AS ''ProposedPrice'',
	'+@ProposedPrice+' AS ''NewPrice'',
	'+@AcceptRejectCode_PriceProposal+',
	'+CAST(@DeferredAcceptanceCodeKey AS NVARCHAR(22))+',
	'+@AcceptRejectCode_PriceProposal+',
	filsum.TotalQuantity,
	filsum.Cnt,
	filsum.TotalActualCost,
	filsum.TotalActualPrice,
	flpc.LastItemCost,
	flpc.LastItemPrice,
	fsf.ForecastedMonthlyQty*12,
	flpc.LastItemCost * (fsf.ForecastedMonthlyQty*12),
	flpc.LastItemPrice * (fsf.ForecastedMonthlyQty*12),
	'+@ProposedPrice+' * (fsf.ForecastedMonthlyQty*12),
	'+@ProposedPrice+' * (fsf.ForecastedMonthlyQty*12),
	ISNULL(fcpc.CurrentItemCost,flpc.LastItemCost) * (fsf.ForecastedMonthlyQty*12),
	GETDATE(),
	flpc.LastSaleDayKey,
	flpc.LastInvoiceLineGroup2Key,
	'+@ReasonCode+',
	'''+@SuppressionDate+''',
	'''+@EffectiveDate+''',
	'''+@DynamicUDVarchar1+''',
	'''+@DynamicUDVarchar2+''',
	fpp.TargetPrice
FROM dbo.FactPROSPrice fpp
INNER JOIN dbo.DimAccount da ON 
	da.AccountKey = fpp.AccountKey
INNER JOIN dbo.FactSalesForecast fsf ON 
	fsf.AccountKey = fpp.AccountKey AND 
	fsf.ItemKey = fpp.ItemKey AND 
	fsf.InvoiceLineGroup1Key = fpp.InvoiceLineGroup1Key AND 
	ForecastType = '''+@ForecastType+'''
INNER JOIN dbo.FactLastPriceAndCost flpc ON 
	flpc.AccountKey = fpp.AccountKey AND 
	flpc.ItemKey = fpp.ItemKey AND 
	flpc.InvoiceLineGroup1Key = fpp.InvoiceLineGroup1Key
INNER JOIN (SELECT 
		AccountKey, 
		ItemKey, 
		InvoiceLineGroup1Key, 
		SUM(TotalQuantity) AS TotalQuantity,
		COUNT(*) AS Cnt,
		SUM(TotalActualCost) AS TotalActualCost,
		SUM(TotalActualPrice) AS TotalActualPrice
	FROM dbo.FactInvoiceLine
	WHERE Last12MonthsIndicator = ''Y''
	GROUP BY 
		AccountKey, 
		ItemKey, 
		InvoiceLineGroup1Key
	) filsum ON
	filsum.AccountKey = fpp.AccountKey AND 
	filsum.ItemKey = fpp.ItemKey AND 
	filsum.InvoiceLineGroup1Key = fpp.InvoiceLineGroup1Key
LEFT JOIN dbo.FactCurrentPriceAndCost fcpc ON 
	fcpc.AccountKey = fpp.AccountKey AND 
	fcpc.ItemKey = fpp.ItemKey AND 
	fcpc.InvoiceLineGroup1Key = fpp.InvoiceLineGroup1Key 
'+@JoinClause2+'
WHERE 
	NOT EXISTS (SELECT 1 FROM dbo.ProposalPrice p WHERE p.ProjectKey = '+CAST(@ProjectKey AS NVARCHAR(22))+' AND p.AccountKey = fpp.AccountKey AND p.ItemKey = fpp.ItemKey and p.InvoiceLineGroup1Key = fpp.InvoiceLineGroup1Key) AND 
	flpc.LastItemPrice IS NOT NULL AND 
	flpc.LastItemCost IS NOT NULL AND 
	'+@ProposedPrice+' > 0.0 AND 
	flpc.LastItemPrice <> 0.0 AND 
	flpc.LastItemCost <> 0.0 AND 
	flpc.LastItemPrice < '+@ProposedPrice+' 
	'+@WhereClause2+'
SET @RowsI = @RowsI + @@RowCount

EXEC LogDCPEvent '''+@MsgStr+''', ''E'', @RowsI, 0, 0
'
	
		SELECT 'INSERT INTO ProposalPrice Stmt (len: ' + CAST(LEN(@SQLStmt) AS NVARCHAR(22)) + '): '

		PRINT @SQLStmt
	
		EXEC (@SQLStmt)

		FETCH NEXT FROM CurAutoSagePass INTO @Pass, @WhereClause2, @JoinClause2, @AcceptRejectCode_PriceProposal, @ProposedPriceOverride, @ReasonCode, @EffectiveDate, @SuppressionDate, @DynamicUDVarchar1, @DynamicUDVarchar2, @PriceIncreaseCapPercent
	END
	
	CLOSE CurAutoSagePass
	DEALLOCATE CurAutoSagePass
		
END


























GO
