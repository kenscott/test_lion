SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO






CREATE PROCEDURE [dbo].[P2P_ME_PopulateProposal]
	@ProjectKey INT, 
	@PlaybookDataPointGroupKey INT, 
	@LimitDataPlaybookKey INT, 
	@PricingRulePlaybookKey INT, 
	@ProposalRecommendationPlaybookKey INT,
	@BeginStep INT,
	@EndStep INT
AS

DECLARE @LimitDataJoin NVARCHAR(MAX)
DECLARE @LimitDataWhere NVARCHAR(MAX)
DECLARE @LimitDataHaving NVARCHAR(MAX)
DECLARE @MaxDayThreshold INT
DECLARE @WhereClause1 Description_Big_Type, @WhereClause2 Description_Big_Type, @WhereClause3 Description_Big_Type
DECLARE @JoinClause1 Description_Big_Type, @JoinClause2 Description_Big_Type, @JoinClause3 Description_Big_Type
DECLARE @ProposalRecommendationKey INT
DECLARE @AcceptRejectCode_PriceProposal description_big_Type
DECLARE @AcceptRejectCode_CostProposal description_big_Type
DECLARE @SQLStmt NVARCHAR(MAX)
DECLARE @SQLStmt2 NVARCHAR(MAX)
DECLARE @RowsI Quantity_Normal_type, @RowsU Quantity_Normal_type, @RowsD Quantity_Normal_type, @PricingRuleWhereClause NVARCHAR(2000), @tmpValuesAssigned NVARCHAR(2000)
DECLARE @SMEMinGPP percent_type, @SMEMaxGPP percent_Type
DECLARE @ForecastType NVARCHAR(50)
DECLARE @PriceIncreaseCapPercent Percent_Type
DECLARE @ProposedPriceOverride description_big_Type
DECLARE @GPPCalculation description_big_Type
--declare @DynamicUDVarchar1 Description_Small_type

SET @RowsI = 0
SET @RowsU = 0
SET @RowsD = 0


SELECT @ForecastType = ForecastType 
FROM ATKProposalRecommendationPlaybook
WHERE ProposalRecommendationPlaybookKey =  @ProposalRecommendationPlaybookKey

--There must be only 1 pass!!!!!
SELECT @ProposalRecommendationKey = ProposalRecommendationKey 
FROM ATKProposalRecommendation
WHERE ProposalRecommendationPlaybookKey = @ProposalRecommendationPlaybookKey

DECLARE @DeferredCode INT
SELECT @DeferredCode = AcceptanceCodeKey FROM atkacceptancecode WHERE acceptanceCodeDesc = 'Deferred'

DECLARE @ReasonCode NVARCHAR(2000)
DECLARE @EffectiveDate NVARCHAR(2000)
DECLARE @SuppressionDate NVARCHAR(2000)

SELECT 
	@WhereClause1 = ISNULL(WhereClause1, ''),
	@JoinClause1 = ISNULL(JoinClause1, ''),
	@WhereClause2 = ISNULL(WhereClause2, ''),
	@JoinClause2= ISNULL(JoinClause2, ''),
	@WhereClause3 = ISNULL(WhereClause3, ''),
	@JoinClause3 = ISNULL(JoinClause3, ''),
	@AcceptRejectCode_PriceProposal = ISNULL(AcceptRejectCode_PriceProposal, '2'),
	@AcceptRejectCode_CostProposal = ISNULL(AcceptRejectCode_PriceProposal, '2'),
	@SMEMinGPP = ISNULL(SMEMinGPP, 0),
	@SMEMaxGPP = ISNULL(SMEMaxGPP, 1),
	@ProposedPriceOverride = ISNULL(ProposedPriceOverride, ''),		
	@ReasonCode = ISNULL(DynamicReasonCode, 'NULL'),
	@EffectiveDate = ISNULL(DynamicEffectiveDate, GETDATE()),
	@SuppressionDate = ISNULL(DynamicSuppressionDate, GETDATE()),
	@PriceIncreaseCapPercent = ISNULL(PriceIncreaseCapPercent,9999999)
	--@DynamicUDVarchar1 = ISNULL(DynamicUDVarchar1, '')
		
FROM ATKProposalRecommendation
WHERE ProposalRecommendationKey = @ProposalRecommendationKey


SELECT @MaxDayThreshold = ISNULL(MaxDayThreshold,0),
		@LimitDataJoin = ISNULL(JoinClause, ''),
		@LimitDataWhere = ISNULL(WhereClause, ''),
		@LimitDataHaving = ISNULL(HavingClause, '')
FROM ATKLimitDataPlaybook 
WHERE LimitDataPlaybookKey = @LimitDataPlaybookKey


IF @LimitDataWhere <> '' AND @LimitDataWhere NOT LIKE 'and %'
	SET @LimitDataWhere = 'and ' + @LimitDataWhere

IF @LimitDataHaving <> '' AND @LimitDataHaving NOT LIKE 'having %'
	SET @LimitDataHaving = 'having ' + @LimitDataHaving


IF @WhereClause1 <> '' AND @WhereClause1 NOT LIKE 'and %'
	SET @WhereClause1 = 'and ' + @WhereClause1
IF @WhereClause2 <> '' AND @WhereClause2 NOT LIKE 'and %'
	SET @WhereClause2 = 'and ' + @WhereClause2
IF @WhereClause3 <> '' AND @WhereClause3 NOT LIKE 'and %'
	SET @WhereClause3 = 'and ' + @WhereClause3

DECLARE @MinInvoiceDateDayKey INT, @MaxInvoiceDateDayKey INT

DECLARE @SalesPercentThreshold DECIMAL(19,8), @TotalInvoiceLinesPercentThreshold DECIMAL(19,8), @GPPFrequencyThreshold Percent_Type


SELECT
	@SalesPercentThreshold = ISNULL(TotalSalesCDFMinPercent,0), 
	@TotalInvoiceLinesPercentThreshold = ISNULL(TotalInvoiceLinesCDFMinPercent, 0),
	@GPPFrequencyThreshold = ISNULL(GPPFrequencyThreshold, 0)
FROM ATKPricingRulePlaybook
	WHERE PricingRulePlaybookKey = @PricingRulePlaybookKey

SET @MaxInvoiceDateDayKey = (SELECT MAX(InvoiceDateDayKey) FROM FactInvoiceLine)
SET @MinInvoiceDateDayKey = @MaxInvoiceDateDayKey - @MaxDayThreshold


DECLARE @ScenarioKey INT
SELECT @ScenarioKey = ScenarioKey
FROM PlaybookDataPointGroup A
JOIN ATKProjectSpecificationScenario B ON A.ProjectSpecificationScenarioKey = B.ProjectSpecificationScenarioKey
WHERE A.PlaybookDataPointGroupKey = @PlaybookDataPointGroupKey

DECLARE @SuppressionTypeKey Key_Small_Type
SELECT @SuppressionTypeKey = SuppressionTypeKey FROM ATKScenario WHERE ScenarioKey = @ScenarioKey

DECLARE @SuppressionJoinClause NVARCHAR(MAX)

--exec LogDCPEvent 'DCP --- ME_CreateMarginErosionProposal: Delete', 'B',0,0,0,@ProjectKey
--If we're re-running a project, need to delete all the existing proposals for it first
IF @BeginStep <= 2 AND @EndStep >= 2
BEGIN
	SET @RowsI = 0
	SET @RowsU = 0
	SET @RowsD = 0

	--EXEC LogDCPEvent 'DCP --- ME_CreateMarginErosionProposal: Delete FactProposalPrice', 'B',0,0,0,@ProjectKey
	--DELETE FROM FactProposalPrice WHERE ProjectKey = @ProjectKey
	--EXEC LogDCPEvent 'DCP --- ME_CreateMarginErosionProposal: Delete FactProposalPrice', 'E', @RowsI, @RowsU, @RowsD 
	
	EXEC LogDCPEvent 'DCP --- ME_CreateMarginErosionProposal: Delete ProposalPrice', 'B',0,0,0,@ProjectKey
	DELETE FROM ProposalPrice WHERE PlaybookDataPointGroupKey = @PlaybookDataPointGroupKey
	SET @RowsD = @RowsD + @@ROWCOUNT
	EXEC LogDCPEvent 'DCP --- ME_CreateMarginErosionProposal: Delete ProposalPrice', 'E', @RowsI, @RowsU, @RowsD 
END

IF @BeginStep <= 1 AND @EndStep >= 1
BEGIN
	SET @RowsI = 0
	SET @RowsU = 0
	SET @RowsD = 0

	EXEC LogDCPEvent 'DCP --- ME_CreateMarginErosionProposal: Delete PlaybookProjectFullScopeSummary', 'B',0,0,0,@ProjectKey
	DELETE FROM PlaybookProjectFullScopeSummary WHERE ProjectKey = @ProjectKey
	EXEC LogDCPEvent 'DCP --- ME_CreateMarginErosionProposal: Delete PlaybookProjectFullScopeSummary', 'E', @RowsI, @RowsU, @RowsD 	
	
	EXEC LogDCPEvent 'DCP --- ME_CreateMarginErosionProposal: Delete PlaybookMarginErosionProposal', 'B',0,0,0,@ProjectKey
	DELETE FROM PlaybookMarginErosionProposal WHERE PlaybookDataPointGroupKey = @PlaybookDataPointGroupKey
	SET @RowsD = @RowsD + @@ROWCOUNT
	EXEC LogDCPEvent 'DCP --- ME_CreateMarginErosionProposal: Delete PlaybookMarginErosionProposal', 'E', @RowsI, @RowsU, @RowsD 	
END

IF @BeginStep <= 3 AND @EndStep >= 3
BEGIN
	SET @RowsI = 0
	SET @RowsU = 0
	SET @RowsD = 0

	EXEC LogDCPEvent 'DCP --- ME_CreateMarginErosionProposal: Delete PlaybookProjectAccountStatus', 'B',0,0,0,@ProjectKey
	DELETE FROM PlaybookProjectAccountStatus WHERE ProjectKey = @ProjectKey
	SET @RowsD = @RowsD + @@ROWCOUNT
	EXEC LogDCPEvent 'DCP --- ME_CreateMarginErosionProposal: Delete PlaybookProjectAccountStatus', 'E', @RowsI, @RowsU, @RowsD 		
END

IF @BeginStep <= 4 AND @EndStep >= 4
BEGIN
	SET @RowsI = 0
	SET @RowsU = 0
	SET @RowsD = 0

	EXEC LogDCPEvent 'DCP --- ME_CreateMarginErosionProposal: Delete PlaybookProjectAccountStatus', 'B',0,0,0,@ProjectKey
	DELETE FROM PlaybookProjectAccountManagerStatus WHERE ProjectKey = @ProjectKey
	SET @RowsD = @RowsD + @@ROWCOUNT
	EXEC LogDCPEvent 'DCP --- ME_CreateMarginErosionProposal: Delete PlaybookProjectAccountStatus', 'E', @RowsI, @RowsU, @RowsD 			
END

SET @RowsI = 0
SET @RowsU = 0
SET @RowsD = 0

EXEC LogDCPEvent 'DCP --- ME_CreateMarginErosionProposal: Delete PlaybookProjectFullScopeSummary', 'B',0,0,0,@ProjectKey

--This always needs to be chunked, it gets created after ProposalPrice
DELETE FROM PlaybookProjectFullScopeSummary WHERE ProjectKey = @ProjectKey
SET @RowsD = @RowsD + @@RowCount

EXEC LogDCPEvent 'DCP --- ME_CreateMarginErosionProposal: Delete PlaybookProjectFullScopeSummary', 'E', @RowsI, @RowsU, @RowsD 			
	
--exec LogDCPEvent 'DCP --- ME_CreateMarginErosionProposal: Delete', 'E', @RowsI, @RowsU, @RowsD 

---------------------------------------
SET @RowsI = 0
SET @RowsU = 0
SET @RowsD = 0


IF @BeginStep <= 1 AND @EndStep >= 1
BEGIN

	EXEC LogDCPEvent 'DCP --- ME_CreateMarginErosionProposal: Insert PlaybookMarginErosionProposal', 'B',0,0,0,@ProjectKey

	SET @SQLStmt = '
	DECLARE @RowsI Quantity_Normal_type
	SET @RowsI = 0

	SELECT 
		FactInvoiceLine.AccountKey, 
		FactInvoiceLine.ItemKey, 
		FactInvoiceLine.InvoiceLineGroup1Key, 
		CASE SUM(TotalActualPrice)
			WHEN 0.0 THEN 0.0
			ELSE (SUM(TotalActualPrice)-SUM(TotalActualCost))/SUM(TotalActualPrice) 
		END AS AvgGPP
	INTO #AvgGPP
	FROM dbo.FactInvoiceLine
	'+@LimitDataJoin+'
	WHERE InvoiceDateDayKey BETWEEN ' + CAST(@MinInvoiceDateDayKey AS NVARCHAR(25)) + ' AND ' + CAST(@MaxInvoiceDateDayKey AS NVARCHAR(25)) + '
	AND TotalActualPrice > 0.0 
	AND TotalActualCost > 0.0
	AND TotalQuantity > 0.0
	AND TotalActualPrice <> TotalActualCost
	'+@LimitDataWhere+'		
	GROUP BY FactInvoiceLine.AccountKey, FactInvoiceLine.ItemKey, FactInvoiceLine.InvoiceLineGroup1Key
	'+@LimitDataHaving+'
	
	CREATE UNIQUE CLUSTERED INDEX I_AvgGPP_1 on #AvgGPP (AccountKey, ItemKey, InvoiceLineGroup1Key)
	
	
	CREATE TABLE #AISDistinctGPP
	(
		RowID INT IDENTITY(1,1),
		AccountKey INT,
		ItemKey INT,
		InvoiceLineGroup1Key INT,
		GPP DEC(19,8),
		TotalActualPrice DEC(19,8),
		TotalInvoiceLines INT,
		MaxClientInvoiceLineID nvarchar(50)
	)
	
	INSERT #AISDistinctGPP (AccountKey, ItemKey, InvoiceLineGroup1Key, GPP, TotalActualPrice, TotalInvoiceLines, MaxClientInvoiceLineID)
	SELECT
		FactInvoiceLine.AccountKey, 
		FactInvoiceLine.ItemKey, 
		FactInvoiceLine.InvoiceLineGroup1Key,
		(TotalActualPrice - TotalActualCost) / TotalActualPrice,
		SUM(TotalActualPrice),	
		COUNT(*),
		MAX(ClientInvoiceLineID)
	FROM dbo.FactInvoiceLine
	INNER JOIN #AvgGPP
		ON dbo.FactInvoiceLine.AccountKey = #AvgGPP.AccountKey
		AND dbo.FactInvoiceLine.ItemKey = #AvgGPP.ItemKey
		AND dbo.FactInvoiceLine.InvoiceLineGroup1Key = #AvgGPP.InvoiceLineGroup1Key
	WHERE InvoiceDateDayKey BETWEEN ' + CAST(@MinInvoiceDateDayKey AS NVARCHAR(25)) + ' AND ' + CAST(@MaxInvoiceDateDayKey AS NVARCHAR(25)) + '
		AND TotalActualPrice > 0.0 
		AND TotalActualCost > 0.0 
		AND TotalQuantity > 0.0
		AND ((TotalActualPrice - TotalActualCost) / TotalActualPrice) BETWEEN ' + CAST(@SMEMinGPP AS NVARCHAR(50))+' AND ' + CAST(@SMEMaxGPP AS NVARCHAR(50)) + '
	GROUP BY
		FactInvoiceLine.AccountKey, 
		FactInvoiceLine.ItemKey, 
		FactInvoiceLine.InvoiceLineGroup1Key,
		(TotalActualPrice - TotalActualCost) / TotalActualPrice
	ORDER BY
		FactInvoiceLine.AccountKey, 
		FactInvoiceLine.ItemKey, 
		FactInvoiceLine.InvoiceLineGroup1Key,
		(TotalActualPrice - TotalActualCost) / TotalActualPrice ASC,
		MAX(ClientInvoiceLineID)   -- a tiebreaker

	CREATE UNIQUE CLUSTERED INDEX I_AISDistinctGPP_1 ON #AISDistinctGPP (AccountKey, ItemKey, InvoiceLineGroup1Key, RowID, GPP)


	SELECT T1.*,
		(SELECT SUM(T2.TotalActualPrice) FROM #AISDistinctGPP T2 WHERE T2.RowID >= T1.RowID AND T1.AccountKey = T2.AccountKey AND T1.ItemKey = T2.ItemKey AND T1.InvoiceLineGroup1Key = T2.InvoiceLineGroup1Key)
		/
		(SELECT SUM(T2.TotalActualPrice) FROM #AISDistinctGPP T2 WHERE T1.AccountKey = T2.AccountKey AND T1.ItemKey = T2.ItemKey AND T1.InvoiceLineGroup1Key = T2.InvoiceLineGroup1Key)
	AS TotalActualPriceCDF,

		(SELECT CAST(SUM(T2.TotalInvoiceLines) AS DEC(19,8)) FROM #AISDistinctGPP T2 WHERE T2.RowID >= T1.RowID AND T1.AccountKey = T2.AccountKey AND T1.ItemKey = T2.ItemKey AND T1.InvoiceLineGroup1Key = T2.InvoiceLineGroup1Key)
		/
		(SELECT CAST(SUM(T2.TotalInvoiceLines) AS DEC(19,8)) FROM #AISDistinctGPP T2 WHERE T1.AccountKey = T2.AccountKey AND T1.ItemKey = T2.ItemKey AND T1.InvoiceLineGroup1Key = T2.InvoiceLineGroup1Key)
	AS TotalInvoiceLinesCDF,

		(SELECT CAST(SUM(T2.TotalInvoiceLines) AS DEC(19,8)) FROM #AISDistinctGPP T2 WHERE T1.GPP = T2.GPP AND T1.AccountKey = T2.AccountKey AND T1.ItemKey = T2.ItemKey AND T1.InvoiceLineGroup1Key = T2.InvoiceLineGroup1Key)
		/
		(SELECT CAST(SUM(T2.TotalInvoiceLines) AS DEC(19,8)) FROM #AISDistinctGPP T2 WHERE T1.AccountKey = T2.AccountKey AND T1.ItemKey = T2.ItemKey AND T1.InvoiceLineGroup1Key = T2.InvoiceLineGroup1Key)
	AS GPPFrequency
	INTO #CDFTable
	FROM #AISDistinctGPP T1

	CREATE UNIQUE CLUSTERED INDEX I_CDFTable_1 ON #CDFTable (AccountKey, ItemKey, InvoiceLineGroup1Key, RowID, GPP)

	
	SELECT AccountKey, ItemKey, InvoiceLineGroup1Key, MAX(RowID) AS SustainedRowID
	INTO #SustainedRow
	FROM #CDFTable
	WHERE TotalActualPriceCDF > '+CAST(@SalesPercentThreshold AS NVARCHAR(50))+'
		AND TotalInvoiceLinesCDF > '+ CAST(@TotalInvoiceLinesPercentThreshold AS NVARCHAR(50))+'
		AND GPPFrequency > ' + CAST(@GPPFrequencyThreshold AS NVARCHAR(50))+'
	GROUP BY AccountKey, ItemKey, InvoiceLineGroup1Key

	CREATE UNIQUE CLUSTERED INDEX I_SustainedRow_1 ON #SustainedRow (AccountKey, ItemKey, InvoiceLineGroup1Key)'
	
	
	SET @SQLStmt2 = '
	INSERT INTO dbo.PlaybookMarginErosionProposal
	(
		PlaybookDataPointGroupKey,
		AccountKey, 
		ItemKey, 
		InvoiceLineGroup1Key, 
		AverageGrossProfitPercent,
		SustainedGrossProfitPercent,
		ItemPrice,
		ItemCost
	)
	SELECT 
		'+CAST(@PlaybookDataPointGroupKey AS NVARCHAR(25))+' AS PlaybookDataPointGroupKey, 
		AvgGPP.AccountKey, 
		AvgGPP.ItemKey, 
		AvgGPP.InvoiceLineGroup1Key, 
		AvgGPP.AvgGPP, 
		SustainedGPP,
		SustainedGPPPrice,
		SustainedGPPCost
	FROM #AvgGPP AvgGPP
	'+@JoinClause1+'
	LEFT JOIN (
		SELECT
			T.AccountKey, 
			T.ItemKey, 
			T.InvoiceLineGroup1Key, 
			T.GPP AS SustainedGPP,
			ISNULL(fil.TotalActualPrice / NULLIF(fil.TotalQuantity, 0.0), 0.0) AS SustainedGPPPrice,
			ISNULL(fil.TotalActualCost  / NULLIF(fil.TotalQuantity, 0.0), 0.0) AS SustainedGPPCost
		FROM #SustainedRow SR 
		JOIN #AISDistinctGPP T
			ON SR.AccountKey = T.AccountKey 
			AND SR.ItemKey = T.ItemKey
			AND SR.InvoiceLineGroup1Key = T.InvoiceLineGroup1Key
		JOIN dbo.FactInvoiceLine fil
			ON fil.ClientInvoiceLineID = t.MaxClientInvoiceLineID
		WHERE SR.SustainedRowID = T.RowID
		) SustainedData 
		ON SustainedData.AccountKey = AvgGPP.AccountKey
		AND SustainedData.ItemKey = AvgGPP.ItemKey
		AND SustainedData.InvoiceLineGroup1Key = AvgGPP.InvoiceLineGroup1Key
	WHERE (AvgGPP.AvgGPP IS NOT NULL OR SustainedData.SustainedGPP IS NOT NULL)
	'+@WhereClause1+'
	SET @RowsI = @RowsI + @@RowCount
	
	EXEC LogDCPEvent ''DCP --- ME_CreateMarginErosionProposal: Insert PlaybookMarginErosionProposal'', ''E'', @RowsI, 0, 0 
	'

-- why NOT GET the cost AS well here AND perhaps put IN LAST cost; OR at least look at it AND hard code the desired filter here IN this INSERT statement
-- Cognos metadata looks ok TO reuse lastitemprice FROM pmep; need TO CHECK stored procedures:
-- EXEC dbo.DBA_Find_String 'PlaybookMarginErosionProposal'
-- 
-- (core thoughts... change "last" TO just ItemPrice, ItemCost, perhaps ADD SustainedFactInvoiceLineKey)
--SELECT TOP 10 * FROM PlaybookMarginErosionProposal WHERE LastItemPrice IS NOT NULL 
	
	--PRINT @SQLStmt
	--PRINT @SQLStmt2
	
	EXEC dbo.PrintNVarcharMax @SQLStmt
	EXEC dbo.PrintNVarcharMax @SQLStmt2
	
	RAISERROR (' --- ', 0, 1) WITH NOWAIT
	
	EXEC (@SQLStmt + @SQLStmt2)
END
--------------------------------------------------------------
SET @RowsI = 0
SET @RowsU = 0
SET @RowsD = 0


IF @BeginStep <= 2 AND @EndStep >= 2
BEGIN

	EXEC LogDCPEvent 'DCP --- ME_CreateMarginErosionProposal: Insert ProposalPrice', 'B',0,0,0,@ProjectKey

	IF @ProposedPriceOverride = ''		
	BEGIN
		
		--Set @GPPCalculation = '(FactLastPriceAndCost.LastItemCost / (1-SustainedGrossProfitPercent))'
		-- handle capping...
		SET @GPPCalculation = '(FactLastPriceAndCost.LastItemCost / (1.0-
			CASE
				WHEN ((FactLastPriceAndCost.LastItemPrice-FactLastPriceAndCost.LastItemCost)/FactLastPriceAndCost.LastItemPrice) + ' + CAST(@PriceIncreaseCapPercent AS NVARCHAR(500)) + ' <= SustainedGrossProfitPercent
					THEN ((FactLastPriceAndCost.LastItemPrice-FactLastPriceAndCost.LastItemCost)/FactLastPriceAndCost.LastItemPrice) + ' + CAST(@PriceIncreaseCapPercent AS NVARCHAR(500)) + '
				ELSE SustainedGrossProfitPercent
			END
			))'

	END
	ELSE
	BEGIN
		SET @GPPCalculation = '
			'+@ProposedPriceOverride+'
			'
	END


	--Now, if we're going to suppress proposals from being generated, add the dynamic SQL here:
	IF (SELECT SuppressionTypeCode 
		FROM ATKSuppressionType 
		WHERE SuppressionTypeKey = @SuppressionTypeKey) = 'Proposal'
	BEGIN
		SET @SuppressionJoinClause = 
		'LEFT JOIN (SELECT DISTINCT AccountKey, ItemKey, InvoiceLineGroup1Key 
				FROM dbo.ProposalPrice
				WHERE SuppressionDate >= GetDate()) SuppressionList
			ON SuppressionList.AccountKey = PlaybookMarginErosionProposal.AccountKey
				and SuppressionList.ItemKey = PlaybookMarginErosionProposal.ItemKey
				and SuppressionList.InvoiceLineGroup1Key = PlaybookMarginErosionProposal.InvoiceLineGroup1Key'
		SET @WhereClause2 = @WhereClause2 + '
			AND SuppressionList.AccountKey IS NULL
			'
	END
	ELSE
		SET @SuppressionJoinClause = ''


	SET @SQLStmt = '

	DECLARE @RowsI Quantity_Normal_type
	SET @RowsI = 0
	
	INSERT INTO ProposalPrice
	(
		ProjectKey,
		PlaybookDataPointGroupKey,
		AccountKey,
		AccountManagerKey,
		ItemKey,
		InvoiceLineGroup1Key,
		PlaybookPriceProposalKey, 
		PlaybookMarginErosionProposalKey, 
		ProposedPrice,
		NewPrice, 
		AcceptanceCodeKey,
		UserAcceptanceCodeKey,
		DefaultAcceptanceCodeKey,
		TotalActual12MonthQuantity,
		TotalActual12MonthFrequency,
		TotalActual12MonthCost,
		TotalActual12MonthPrice,
		LastItemCost,
		LastItemPrice,
		Forecasted12MonthQuantity,
		TotalForecasted12MonthCost_LastCost,
		TotalForecasted12MonthPrice_LastPrice,
		TotalForecasted12MonthPrice_ProposedPrice,
		TotalForecasted12MonthPrice_NewPrice,
		TotalForecasted12MonthCost_RelevantCost,
		AcceptanceStatusDate,
		LastSaleDayKey,
		FactLastPriceAndCost.LastInvoiceLineGroup2Key,
		ReasonCodeKey,
		SuppressionDate,
		EffectiveDate,
		UDDecimal1
	) '
	
	SET @SQLStmt2 = '
	SELECT
		'+CAST(@ProjectKey AS NVARCHAR(500))+',
		'+CAST(@PlaybookDataPointGroupKey AS NVARCHAR(500))+',
		PlaybookMarginErosionProposal.AccountKey,
		DimAccount.AccountManagerKey,
		PlaybookMarginErosionProposal.ItemKey,
		PlaybookMarginErosionProposal.InvoiceLineGroup1Key,
		NULL,
		PlaybookMarginErosionProposalKey,
		'+@GPPCalculation+',
		'+@GPPCalculation+',
		'+@AcceptRejectCode_PriceProposal+',
		'+CAST(@DeferredCode AS NVARCHAR(500))+',
		'+@AcceptRejectCode_PriceProposal+',
		FILSUM.TotalQuantity,
		Cnt,
		FILSUM.TotalActualCost,
		FILSUM.TotalActualPrice,
		FactLastPriceAndCost.LastItemCost,
		FactLastPriceAndCost.LastItemPrice,
		ForecastedMonthlyQty*12,
		FactLastPriceAndCost.LastItemCost * (ForecastedMonthlyQty*12),
		FactLastPriceAndCost.LastItemPrice * (ForecastedMonthlyQty*12),
		'+@GPPCalculation+' * (ForecastedMonthlyQty*12),
		'+@GPPCalculation+' * (ForecastedMonthlyQty*12),
		ISNULL(CurrentItemCost,FactLastPriceAndCost.LastItemCost) * (ForecastedMonthlyQty*12),
		GETDATE(),
		FactLastPriceAndCost.LastSaleDayKey,
		FactLastPriceAndCost.LastInvoiceLineGroup2Key,
		'+@ReasonCode+',
		'''+@SuppressionDate+''',
		'''+@EffectiveDate+''',
		FactLastPriceAndCost.PercentAccountItemSalesToAccountSales
	FROM PlaybookMarginErosionProposal
	JOIN DimAccount ON DimAccount.AccountKey = PlaybookMarginErosionProposal.AccountKey
	JOIN FactSalesForecast ON FactSalesForecast.AccountKey = PlaybookMarginErosionProposal.AccountKey AND FactSalesForecast.ItemKey = PlaybookMarginErosionProposal.ItemKey AND FactSalesForecast.InvoiceLineGroup1Key = PlaybookMarginErosionProposal.InvoiceLineGroup1Key AND ForecastType = '''+@ForecastType+'''
	JOIN FactLastPriceAndCost ON FactLastPriceAndCost.AccountKey = PlaybookMarginErosionProposal.AccountKey AND FactLastPriceAndCost.ItemKey = PlaybookMarginErosionProposal.ItemKey AND FactLastPriceAndCost.InvoiceLineGroup1Key = PlaybookMarginErosionProposal.InvoiceLineGroup1Key
	JOIN (
		SELECT 
			AccountKey, ItemKey, InvoiceLineGroup1Key, 
			SUM(TotalQuantity) TotalQuantity,
			COUNT(*) Cnt,
			SUM(TotalActualCost) TotalActualCost,
			SUM(TotalActualPrice) TotalActualPrice
		FROM dbo.FactInvoiceLine FIL 
		WHERE Last12MonthsIndicator = ''Y''
		GROUP BY AccountKey, ItemKey, InvoiceLineGroup1Key
		) FILSUM
		ON FILSUM.AccountKey = PlaybookMarginErosionProposal.AccountKey AND FILSUM.ItemKey = PlaybookMarginErosionProposal.ItemKey AND FILSUM.InvoiceLineGroup1Key = PlaybookMarginErosionProposal.InvoiceLineGroup1Key
	LEFT JOIN FactCurrentPriceAndCost ON FactCurrentPriceAndCost.AccountKey = PlaybookMarginErosionProposal.AccountKey AND FactCurrentPriceAndCost.ItemKey = PlaybookMarginErosionProposal.ItemKey AND FactCurrentPriceAndCost.InvoiceLineGroup1Key = PlaybookMarginErosionProposal.InvoiceLineGroup1Key 
	'+@JoinClause2+'
	' + @SuppressionJoinClause + '
	WHERE PlaybookMarginErosionProposal.PlaybookDataPointGroupKey = '+CAST(@PlaybookDataPointGroupKey AS NVARCHAR(100))+ ' 
	AND FactLastPriceAndCost.LastItemPrice IS NOT NULL 
	AND FactLastPriceAndCost.LastItemCost IS NOT NULL 
	AND PlaybookMarginErosionProposal.SustainedGrossProfitPercent > 0.0 
	AND FactLastPriceAndCost.LastItemPrice <> 0.0 
	AND FactLastPriceAndCost.LastItemCost <> 0.0 
	AND FactLastPriceAndCost.LastItemPrice < '+@GPPCalculation+' 
	'+@WhereClause2
	

	PRINT 'INSERT INTO ProposalPrice Stmt:'
	PRINT @SQLStmt
	PRINT @SQLStmt2
	PRINT ' 
	SET @RowsI = @RowsI + @@RowCount
	
	EXEC LogDCPEvent ''DCP --- ME_CreateMarginErosionProposal: Insert ProposalPrice'', ''E'', @RowsI, 0, 0 '

	RAISERROR (' --- ', 0, 1) WITH NOWAIT

	-- according to BOL, one can concatenate strings in the exec call even if the result would be > 8k: Although each [N] 'tsql_string' or @string_variable must be less than 8,000 bytes, the concatenation is performed logically in the SQL Server parser and never materializes in memory
	EXEC 
	(
		@SQLStmt
		+ @SQLStmt2
		+  ' 
	SET @RowsI = @RowsI + @@RowCount
	
	EXEC LogDCPEvent ''DCP --- ME_CreateMarginErosionProposal: Insert ProposalPrice'', ''E'', @RowsI, 0, 0 '	
	)

	
	

END
--------------------------------------------------
IF @BeginStep <= 3 AND @EndStep >= 3
BEGIN

	EXEC dbo.P2P_PopulatePlaybookProjectAccountStatus @ProjectKey, @PlaybookDataPointGroupKey
	
--	exec LogDCPEvent 'DCP --- ME_CreateMarginErosionProposal: Insert ProjectAcountStatus', 'B',0,0,0,@ProjectKey
--	set @RowsI = 0
--	set @RowsU = 0
--	set @RowsD = 0
--	
--	Insert PlaybookProjectAccountStatus
--	(ProjectKey, AccountKey, ProjectAccountStatus, ProjectAccountManagerStatus, AccountManagerKey)
--	Select distinct @ProjectKey, PlaybookMarginErosionProposal.AccountKey, 'Not Reviewed',  'Not Finalized', DimAccount.AccountManagerKey
--	from PlaybookMarginErosionProposal
--	join DimAccount on DimAccount.AccountKey = PlaybookMarginErosionProposal.AccountKey
--	where PlaybookDataPointGroupKey = @PlaybookDataPointGroupKey
--	Set @RowsI = @RowsI + @@RowCount
--	exec LogDCPEvent 'DCP --- ME_CreateMarginErosionProposal: Insert ProjectAcountStatus', 'E', @RowsI, @RowsU, @RowsD

END	
-----------------------------
IF @BeginStep <= 4 AND @EndStep >= 4
BEGIN


	EXEC dbo.P2P_PopulatePlaybookProjectAccountManagerStatus @ProjectKey, @PlaybookDataPointGroupKey
	
--	exec LogDCPEvent 'DCP --- ME_CreateMarginErosionProposal: Insert PlaybookProjectAccountManagerStatus ', 'B',0,0,0,@ProjectKey
--	set @RowsI = 0
--	set @RowsU = 0
--	set @RowsD = 0
--	
--	
--	Insert PlaybookProjectAccountManagerStatus 
--	(ProjectKey, AccountManagerKey, ProjectAccountManagerStatus)
--	Select @ProjectKey, ParentAccountManagerKey, 'Not Finalized' from
--	(Select distinct ParentACcountManagerKey from BridgeACcountManager where SubsidiaryACcountManagerKey in 
--	(
--		Select distinct AccountManagerKey 
--		from ProposalPrice
--		where ProjectKey = @ProjectKey
--	)
--	) A
--	Where (Select count(*) from ProposalPrice PP where ProjectKey = @ProjectKey and PP.AccountManagerKey in (Select SubsidiaryAccountManagerKey from BridgeAccountManager where ParentAccountManagerKey = A.ParentACcountManagerKey)) > 0--as TotalProposals

END


SET @RowsI = @RowsI + @@RowCount
EXEC LogDCPEvent 'DCP --- ME_CreateMarginErosionProposal: Insert PlaybookProjectAccountManagerStatus ', 'E', @RowsI, @RowsU, @RowsD













GO
