SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




--CreateScript_SegmentationPlaybook 1

CREATE      PROCEDURE [dbo].[Clone_SegmentationPlaybook]
@SegmentFactorPlaybookKey INT,
@Command CHAR(1) = 'N',
@ApproachTypeKey INT
AS

IF @Command = 'N'	--Gets printed whether or not this is ME
	INSERT INTO #ScenarioData SELECT  'Declare @NewSegmentFactorPlaybookKey int'

IF @ApproachTypeKey IN (2, 3, 5, 7) -- Ex. if it's not a Margin Erosion scenario, VCM, etc.
BEGIN
	DECLARE @SQL NVARCHAR(MAX)
	
	--For the segment customer table
	DECLARE
	@SegmentCustomerKey NVARCHAR(MAX), 
	@SegmentSequenceNumber NVARCHAR(MAX), 
	@SegmentLabel NVARCHAR(MAX), 
	@SegmentLowerBound NVARCHAR(MAX), 
	@SegmentUpperBound NVARCHAR(MAX), 
	@SegmentType NVARCHAR(MAX)
	
	
	DECLARE 
	@BlendedRankRule NVARCHAR(MAX), 
	@CoreNonCoreThreshold NVARCHAR(MAX), 
	@AccountItemSalesLowerLimit NVARCHAR(MAX), 
	@AccountItemFrequencyLowerLimit NVARCHAR(MAX), 
	@AccountItemQuantityLowerLimit NVARCHAR(MAX), 
	@ItemSalesLowerLimit NVARCHAR(MAX), 
	@ItemFrequencyLowerLimit NVARCHAR(MAX), 
	@ItemQuantityLowerLimit NVARCHAR(MAX), 
	@AccountItemSalesWeight NVARCHAR(MAX), 
	@AccountItemFrequencyWeight NVARCHAR(MAX), 
	@AccountItemQuantityWeight NVARCHAR(MAX), 
	@ItemSalesWeight NVARCHAR(MAX), 
	@ItemFrequencyWeight NVARCHAR(MAX), 
	@ItemQuantityWeight NVARCHAR(MAX), 
	@SpanAlpha NVARCHAR(MAX), 
	@SpanBeta NVARCHAR(MAX), 
	@B_ItemPercentage NVARCHAR(MAX)
	
	SELECT 
		@BlendedRankRule = ISNULL(BlendedRankRule,''),
		@CoreNonCoreThreshold = ISNULL(CoreNonCoreThreshold,''),
		@AccountItemSalesLowerLimit = ISNULL(AccountItemSalesLowerLimit,''),
		@AccountItemFrequencyLowerLimit = ISNULL(AccountItemFrequencyLowerLimit,''),
		@AccountItemQuantityLowerLimit = ISNULL(AccountItemQuantityLowerLimit,''),
		@ItemSalesLowerLimit = ISNULL(ItemSalesLowerLimit,''),
		@ItemFrequencyLowerLimit = ISNULL(ItemFrequencyLowerLimit,''),
		@ItemQuantityLowerLimit = ISNULL(ItemQuantityLowerLimit,''),
		@AccountItemSalesWeight = ISNULL(AccountItemSalesWeight,''),
		@AccountItemFrequencyWeight = ISNULL(AccountItemFrequencyWeight,''),
		@AccountItemQuantityWeight = ISNULL(AccountItemQuantityWeight,''),
		@ItemSalesWeight = ISNULL(ItemSalesWeight,''),
		@ItemFrequencyWeight = ISNULL(ItemFrequencyWeight,''),
		@ItemQuantityWeight = ISNULL(ItemQuantityWeight,''),
		@SpanAlpha = ISNULL(SpanAlpha,''),
		@SpanBeta = ISNULL(SpanBeta,''),
		@B_ItemPercentage = ISNULL(CAST(B_ItemPercentage AS NVARCHAR(MAX)),'')
	FROM ATKSegmentFactorPlaybook
	WHERE SegmentFactorPlaybookKey = @SegmentFactorPlaybookKey
	
	
	IF @Command = 'N'
	BEGIN
		INSERT INTO #ScenarioData SELECT  '-------------------------------------'
		INSERT INTO #ScenarioData SELECT  '--Insert new SegmentationPlaybook Row'
		INSERT INTO #ScenarioData SELECT  '-------------------------------------'

		
		
		SET @SQL = 
		'INSERT INTO ATKSegmentFactorPlaybook(BlendedRankRule, CoreNonCoreThreshold, AccountItemSalesLowerLimit, AccountItemFrequencyLowerLimit, AccountItemQuantityLowerLimit, ItemSalesLowerLimit, ItemFrequencyLowerLimit, ItemQuantityLowerLimit, AccountItemSalesWeight, AccountItemFrequencyWeight, AccountItemQuantityWeight, ItemSalesWeight, ItemFrequencyWeight, ItemQuantityWeight, SpanAlpha, SpanBeta, B_ItemPercentage, Version)
		Values ('''+@BlendedRankRule+''','''+@CoreNonCoreThreshold+''','''+@AccountItemSalesLowerLimit+''','''+@AccountItemFrequencyLowerLimit+''','''+@AccountItemQuantityLowerLimit+''','''+@ItemSalesLowerLimit+''','''+@ItemFrequencyLowerLimit+''','''+@ItemQuantityLowerLimit+''','''+@AccountItemSalesWeight+''','''+@AccountItemFrequencyWeight+''','''+@AccountItemQuantityWeight+''','''+@ItemSalesWeight+''','''+@ItemFrequencyWeight+''','''+@ItemQuantityWeight+''','''+@SpanAlpha+''','''+@SpanBeta+''','''+@B_ItemPercentage+''',0)'
		
		INSERT INTO #ScenarioData SELECT  @SQL
		
		INSERT INTO #ScenarioData SELECT  'Set @NewSegmentFactorPlaybookKey = @@Identity'
		INSERT INTO #ScenarioData SELECT  'Print ''Your new Segmentation Playbook Key is: ''+Cast(@NewSegmentFactorPlaybookKey as nvarchar(10))'
	END
	ELSE IF @Command = 'U'
	BEGIN
		INSERT INTO #ScenarioData SELECT  '------------------------------------------------------'
		INSERT INTO #ScenarioData SELECT  '--Updating ATKSegmentFactorPlaybook: '+CAST(@SegmentFactorPlaybookKey AS NVARCHAR(100))
		INSERT INTO #ScenarioData SELECT  '------------------------------------------------------'
		INSERT INTO #ScenarioData SELECT  '	
		UPDATE ATKSegmentFactorPlaybook SET
		BlendedRankRule='''+@BlendedRankRule+''',
		CoreNonCoreThreshold='''+@CoreNonCoreThreshold+''',
		AccountItemSalesLowerLimit='''+@AccountItemSalesLowerLimit+''',
		AccountItemFrequencyLowerLimit='''+@AccountItemFrequencyLowerLimit+''',
		AccountItemQuantityLowerLimit='''+@AccountItemQuantityLowerLimit+''',
		ItemSalesLowerLimit='''+@ItemSalesLowerLimit+''',
		ItemFrequencyLowerLimit='''+@ItemFrequencyLowerLimit+''',
		ItemQuantityLowerLimit='''+@ItemQuantityLowerLimit+''',
		AccountItemSalesWeight='''+@AccountItemSalesWeight+''',
		AccountItemFrequencyWeight='''+@AccountItemFrequencyWeight+''',
		AccountItemQuantityWeight='''+@AccountItemQuantityWeight+''',
		ItemSalesWeight='''+@ItemSalesWeight+''',
		ItemFrequencyWeight='''+@ItemFrequencyWeight+''',
		ItemQuantityWeight='''+@ItemQuantityWeight+''',
		SpanAlpha='''+@SpanAlpha+''',
		SpanBeta='''+@SpanBeta+''',
		B_ItemPercentage='''+@B_ItemPercentage+'''
		Where SegmentFactorPlaybookKey = '+CAST(@SegmentFactorPlaybookKey AS NVARCHAR(100))
	
	END
	ELSE
	BEGIN
		INSERT INTO #ScenarioData SELECT  '------------------------------------------------------------'
		INSERT INTO #ScenarioData SELECT  'Your current Segmentation Playbook Key is: '+CAST(@SegmentFactorPlaybookKey AS NVARCHAR(100))
		INSERT INTO #ScenarioData SELECT  '------------------------------------------------------------'
		INSERT INTO #ScenarioData SELECT  '  BlendedRankRule:                        '+ISNULL(@BlendedRankRule, 'NULL')
		INSERT INTO #ScenarioData SELECT  '  CoreNonCoreThreshold:                   '+ISNULL(@CoreNonCoreThreshold, 'NULL')
		INSERT INTO #ScenarioData SELECT  '  Weighted Account/Item Banding:          '
		INSERT INTO #ScenarioData SELECT  '    Lower Limits-->    Sales:  '+ISNULL(@AccountItemSalesLowerLimit, 'NULL') + '   Frequency:  '+ISNULL(@AccountItemFrequencyLowerLimit, 'NULL')+ '   Quantity:  '+ISNULL(@AccountItemQuantityLowerLimit, 'NULL')	
		INSERT INTO #ScenarioData SELECT  '    Weights------->    Sales:  '+ISNULL(@AccountItemSalesWeight, 'NULL')+ '   Frequency:  '+ISNULL(@AccountItemFrequencyWeight, 'NULL')+ '   Quantity:  '+ISNULL(@AccountItemQuantityWeight, 'NULL')
		INSERT INTO #ScenarioData SELECT  '  Weighted Item Banding:          '
		INSERT INTO #ScenarioData SELECT  '    Lower Limits-->    Sales:  '+ISNULL(@ItemSalesLowerLimit, 'NULL') + '   Frequency:  '+ISNULL(@ItemFrequencyLowerLimit, 'NULL')+ '   Quantity:  '+ISNULL(@ItemQuantityLowerLimit, 'NULL')	
		INSERT INTO #ScenarioData SELECT  '    Weights------->    Sales:  '+ISNULL(@ItemSalesWeight, 'NULL')+ '   Frequency:  '+ISNULL(@ItemFrequencyWeight, 'NULL')+ '   Quantity:  '+ISNULL(@ItemQuantityWeight, 'NULL')
	-- 	Print '  ItemSalesLowerLimit:                    '+IsNull(@ItemSalesLowerLimit, 'NULL')
	-- 	Print '  ItemFrequencyLowerLimit:                '+IsNull(@ItemFrequencyLowerLimit, 'NULL')
	-- 	Print '  ItemQuantityLowerLimit:                 '+IsNull(@ItemQuantityLowerLimit, 'NULL')
	-- 	Print '  ItemSalesWeight:                        '+IsNull(@ItemSalesWeight, 'NULL')
	-- 	Print '  ItemFrequencyWeight:                    '+IsNull(@ItemFrequencyWeight, 'NULL')
	-- 	Print '  ItemQuantityWeight:                     '+IsNull(@ItemQuantityWeight, 'NULL')
		INSERT INTO #ScenarioData SELECT  '  SpanAlpha (used for weighted banding):  '+ISNULL(@SpanAlpha, 'NULL')
		INSERT INTO #ScenarioData SELECT  '  SpanBeta (used for weighted banding):   '+ISNULL(@SpanBeta, 'NULL')
		INSERT INTO #ScenarioData SELECT  '  B_ItemPercentage:                       '+ISNULL(@B_ItemPercentage, 'NULL')
	--	INSERT INTO #ScenarioData SELECT  '------------------------------------------------------------'
	END
	
	--Now we need to loop through all the segment customer rules for this playbook and insert them
	DECLARE SegmentCustomers CURSOR FOR
	SELECT 
	SegmentCustomerKey,
	ISNULL(SegmentFactorPlaybookKey,''), 
	ISNULL(SegmentSequenceNumber,''), 
	ISNULL(SegmentLabel,''), 
	ISNULL(CONVERT(NVARCHAR(500), SegmentLowerBound), ''), 
	ISNULL(CONVERT(NVARCHAR(500), SegmentUpperBound), ''), 
	ISNULL(SegmentType,'')
	FROM ATKSegmentCustomer
	WHERE SegmentFactorPlaybookKey = @SegmentFactorPlaybookKey
	ORDER BY SegmentType, SegmentSequenceNumber
	
	OPEN SegmentCustomers
	
	FETCH NEXT FROM SegmentCustomers INTO @SegmentCustomerKey, @SegmentFactorPlaybookKey, @SegmentSequenceNumber, @SegmentLabel, @SegmentLowerBound, @SegmentUpperBound, @SegmentType
	
	WHILE @@Fetch_Status = 0
	BEGIN
	
		IF @Command = 'N'
		BEGIN
	
			INSERT INTO #ScenarioData SELECT  '     -------------------------------------'
			INSERT INTO #ScenarioData SELECT  '     --Insert new ATKSegmentCustomer Row'
			INSERT INTO #ScenarioData SELECT  '     -------------------------------------'
		
			SET @SQL = '
		     INSERT INTO  ATKSegmentCustomer (
		     SegmentFactorPlaybookKey , SegmentSequenceNumber , SegmentLabel , SegmentLowerBound , SegmentUpperBound , SegmentType ,  Version)
		     Values (@NewSegmentFactorPlaybookKey,'''+ @SegmentSequenceNumber+''','''+ @SegmentLabel+''','+ @SegmentLowerBound+','+ @SegmentUpperBound+','''+ @SegmentType + ''',0)'
		
			INSERT INTO #ScenarioData SELECT  @SQL
		END
		ELSE IF @Command = 'U'
		BEGIN
			INSERT INTO #ScenarioData SELECT  '------------------------------------------------------'
			INSERT INTO #ScenarioData SELECT  '--Updating ATKSegmentCustomer: '+CAST(@SegmentCustomerKey AS NVARCHAR(100))
			INSERT INTO #ScenarioData SELECT  '------------------------------------------------------'
			INSERT INTO #ScenarioData SELECT  '	
			UPDATE ATKSegmentCustomer SET
			SegmentSequenceNumber='''+ @SegmentSequenceNumber+''',
			SegmentLabel='''+ @SegmentLabel+''',
			SegmentLowerBound='+ @SegmentLowerBound+',
			SegmentUpperBound='+ @SegmentUpperBound+',
			SegmentType='''+ @SegmentType + '''
			Where SegmentCustomerKey = '+CAST(@SegmentCustomerKey AS NVARCHAR(100))
	
		END
	
		ELSE
		BEGIN
			INSERT INTO #ScenarioData SELECT  '      ------------------------------------------------------'
			INSERT INTO #ScenarioData SELECT  '      --ATKSegmentCustomerKey: '+CAST(@SegmentCustomerKey AS NVARCHAR(100))
			INSERT INTO #ScenarioData SELECT  '      ------------------------------------------------------'
			INSERT INTO #ScenarioData SELECT  '        SegmentType:            '+ISNULL(@SegmentType, 'NULL')
	--		INSERT INTO #ScenarioData SELECT  '---  SegmentSequenceNumber:  '+IsNull(@SegmentSequenceNumber
			INSERT INTO #ScenarioData SELECT  '        SegmentLabel:           '+ISNULL(@SegmentLabel, 'NULL')
			INSERT INTO #ScenarioData SELECT  '        >=  '+ISNULL(@SegmentLowerBound, 'NULL')+ '  <  '+ISNULL(@SegmentUpperBound, 'NULL')
	--		INSERT INTO #ScenarioData SELECT  '      ------------------------------------------------------'
		END
	
		FETCH NEXT FROM SegmentCustomers INTO @SegmentCustomerKey, @SegmentFactorPlaybookKey, @SegmentSequenceNumber, @SegmentLabel, @SegmentLowerBound, @SegmentUpperBound, @SegmentType
	END
	CLOSE SegmentCustomers
	DEALLOCATE SegmentCustomers
	
	IF @Command = 'N'
	BEGIN
		INSERT INTO #ScenarioData SELECT  '-------------------------------------'
		INSERT INTO #ScenarioData SELECT  '-------------------------------------'
	END
	ELSE
	BEGIN
		INSERT INTO #ScenarioData SELECT  '------------------------------------------------------------'
	END
END











GO
