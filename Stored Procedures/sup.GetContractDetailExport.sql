SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE procedure [sup].[GetContractDetailExport]
	@contractID varchar(9)

AS

SET NOCOUNT ON;
SET ANSI_WARNINGS OFF;

/*
--TESTING
DECLARE @contractID VARCHAR(100);
SET @contractID = 'HGH000984';
--END TESTING
*/

DECLARE @table1 TABLE
(
    Row_Seq INT,
    Supplier NVARCHAR(50),
    ContractID VARCHAR(50),
    ContractStartDate DATE,
    ContractExpiryDate DATE,
    WolseleyContractID VARCHAR(10),
    ContractType VARCHAR(20),
    PurchaseType VARCHAR(20),
    PricingContractIndicator VARCHAR(1),
    PricingContractID NVARCHAR(250),
    JobSite VARCHAR(50),
    Developer NVARCHAR(500),
    ContactName NVARCHAR(250),
    ContactEmail NVARCHAR(250)
);

INSERT INTO @table1
(
    Row_Seq,
    Supplier,
    ContractID,
    ContractStartDate,
    ContractExpiryDate,
    WolseleyContractID,
    ContractType,
    PurchaseType,
    PricingContractIndicator,
    PricingContractID,
    JobSite,
    Developer,
    ContactName,
    ContactEmail
)
SELECT ROW_NUMBER() OVER (ORDER BY v.VendorDescription, sc.ContractReference, ContractID ASC) AS Row_Seq,
       v.VendorDescription AS Supplier,
       sc.ContractReference AS ContractID,
       sc.StartDate AS ContractStartDate,
       sc.ExpirationDate AS ContractExpiryDate,
       sc.ContractID AS WolseleyContractID,
       CASE sc.ContractType1
           WHEN 'C' THEN
               'Claim Back'
           WHEN 'O' THEN
               'Off Invoice'
       END AS 'ContractType',
       CASE sc.PurchaseType
           WHEN 'S' THEN
               'Ex Stock'
           WHEN 'D' THEN
               'Direct'
           WHEN 'B' THEN
               'Both'
       END AS PurchaseType,
       sc.PricingContractIndicator,
       sc.PricingContractID,
       sc.JobSite,
       sc.Developer,
       sc.ContactName,
       sc.ContactEmail
FROM cc.SupplierContract sc
    INNER JOIN dbo.DimVendor v
        ON sc.VendorKey = v.VendorKey
WHERE ContractID = @contractID;


DECLARE @table2 TABLE
(
    Row_Seq INT,
    AccountName NVARCHAR(50),
    AccountStartDate DATE,
    AccountExpiryDate DATE
);

INSERT INTO @table2
(
    Row_Seq,
    AccountName,
    AccountStartDate,
    AccountExpiryDate
)
SELECT ROW_NUMBER() OVER (ORDER BY sca.ContractAccountKey ASC) AS Row_Seq,
       sca.AccountName,
       sca.StartDate AS AccountStartDate,
       sca.ExpirationDate AS AccountExpiryDate
FROM cc.SupplierContract sc
    INNER JOIN cc.vw_ContractAccount sca
        ON sc.ContractKey = sca.ContractKey
WHERE ContractID = @contractID;


DECLARE @table3 TABLE
(
    Row_Seq INT,
    Products NVARCHAR(50),
    WolseleyProductCode NVARCHAR(250),
    ProductDescription NVARCHAR(250),
    ProductDiscount DECIMAL(9, 4),
    ProductLimitType VARCHAR(1),
    ProductLimit DECIMAL(36, 4),
    ProductStartDate DATETIME,
    ProductExpiryDate DATETIME
);

INSERT INTO @table3
(
    Row_Seq,
    Products,
    WolseleyProductCode,
    ProductDescription,
    ProductDiscount,
    ProductLimitType,
    ProductLimit,
    ProductStartDate,
    ProductExpiryDate
)
SELECT ROW_NUMBER() OVER (ORDER BY ci.GUID ASC) AS Row_Seq,
       ci.ProductCode AS Products,
       ci.VendorStockNumber AS WolseleyProductCode,
       ci.ItemDescription AS ProductDescription,
       ci.ClaimBackPerc AS ProductDiscount,
       ci.LimitType AS ProductLimitType,
       ci.LimitValue AS ProductLimt,
       ci.StartDT AS ProductStartDate,
       ci.ExpiryDT AS ProductExpiryDate
FROM cc.SupplierContract sc
    INNER JOIN cc.vw_ContractDetailItem ci
        ON sc.ContractKey = ci.ContractKey
           AND ci.ProductCode <> ''
WHERE ContractID = @contractID;

DECLARE @table4 TABLE
(
    Row_Seq INT,
    ProductGroups NVARCHAR(50),
    ProductGroupsDescription NVARCHAR(250),
    productGroupDiscount DECIMAL(9, 4),
    productGroupLimitType VARCHAR(1),
    ProductGroupLimit DECIMAL(36, 4),
    ProductGroupStartDate DATETIME,
    ProductGroupExpiryDate DATETIME
);
INSERT INTO @table4
(
    Row_Seq,
    ProductGroups,
    ProductGroupsDescription,
    productGroupDiscount,
    productGroupLimitType,
    ProductGroupLimit,
    ProductGroupStartDate,
    ProductGroupExpiryDate
)
SELECT ROW_NUMBER() OVER (ORDER BY igx.ContractItemGroupXKey ASC) AS Row_Seq,
       IG1Level1 AS ProductGroups,
       IG1Level1UDVarchar1 AS ProductGroupsDescription,
       igx.ClaimBackPerc AS productGroupDiscount,
       igx.LimitType AS ProductGroupLimitType,
       igx.LimitValue AS ProductGroupLimit,
       igx.StartDT AS ProductGroupStartDate,
       igx.ExpiryDT AS ProductGroupExpiryDate
FROM cc.SupplierContract sc
    INNER JOIN cc.SupplierContractItemGroupX igx
        ON sc.ContractKey = igx.ContractKey
    JOIN dbo.DimItemGroup1 dig1
        ON dig1.ItemGroup1Key = igx.ItemGroupXKey
WHERE igx.ItemGroupLevel = 1
      AND sc.ContractID = @contractID;


INSERT INTO @table4
(
    Row_Seq,
    ProductGroups,
    ProductGroupsDescription,
    productGroupDiscount,
    productGroupLimitType,
    ProductGroupLimit,
    ProductGroupStartDate,
    ProductGroupExpiryDate
)
SELECT ROW_NUMBER() OVER (ORDER BY igx.ContractItemGroupXKey) +
       (
           SELECT COUNT(*)
           FROM cc.SupplierContract sc
               INNER JOIN cc.SupplierContractItemGroupX igx
                   ON sc.ContractKey = igx.ContractKey
               JOIN dbo.DimItemGroup1 dig1
                   ON dig1.ItemGroup1Key = igx.ItemGroupXKey
           WHERE igx.ItemGroupLevel = 1
                 AND sc.ContractID = @contractID
       ) AS Row_Seq,
       IG3Level1 AS ProductGroups,
       IG3Level1UDVarchar1 AS ProductGroupsDescription,
       igx.ClaimBackPerc AS productGroupDiscount,
       igx.LimitType AS ProductGroupLimitType,
       igx.LimitValue AS ProductGroupLimit,
       igx.StartDT AS ProductGroupStartDate,
       igx.ExpiryDT AS ProductGroupExpiryDate
FROM cc.SupplierContract sc
    INNER JOIN cc.SupplierContractItemGroupX igx
        ON sc.ContractKey = igx.ContractKey
    JOIN dbo.DimItemGroup3 dig3
        ON dig3.ItemGroup3Key = igx.ItemGroupXKey
WHERE igx.ItemGroupLevel = 3
      AND sc.ContractID = @contractID;


DECLARE @table5 TABLE
(
    Row_Seq INT,
    LocationName NVARCHAR(50),
    LocationDescription NVARCHAR(250),
    LocationStartDate DATE,
    LocationExpiryDate DATE
);

INSERT INTO @table5
(
    Row_Seq,
    LocationName,
    LocationDescription,
    LocationStartDate,
    LocationExpiryDate
)
SELECT ROW_NUMBER() OVER (ORDER BY sl.ContractLocationKey ASC) AS Row_Seq,
       sl.LocationName,
       sl.LocationDescription,
       sl.StartDate AS LocationStartDate,
       sl.ExpirationDate AS LocationExpiryDate
FROM cc.SupplierContract sc
    INNER JOIN cc.SupplierContractLocation sl
        ON sc.ContractKey = sl.ContractKey
WHERE sc.ContractID = @contractID;

DECLARE @mymax1 INT;
DECLARE @mymax2 INT;
DECLARE @mymax3 INT;
DECLARE @mymax4 INT;
DECLARE @mymax5 INT;
DECLARE @myMax INT;
SELECT @mymax1 = MAX(Row_Seq)
FROM @table1;
SELECT @mymax2 = MAX(Row_Seq)
FROM @table2;
SELECT @mymax3 = MAX(Row_Seq)
FROM @table3;
SELECT @mymax4 = MAX(Row_Seq)
FROM @table4;
SELECT @mymax5 = MAX(Row_Seq)
FROM @table5;

SELECT @myMax = MAX(v)
FROM
(
    VALUES
        (@mymax1),
        (@mymax2),
        (@mymax3),
        (@mymax4),
        (@mymax5)
) AS value (v);

/***********************************************************************************/
IF @myMax = @mymax3
    SELECT NEWID() AS GUID,
           CASE
               WHEN @myMax = @mymax3 THEN
                   t3.Row_Seq
               WHEN @myMax = @mymax4 THEN
                   t4.Row_Seq
               WHEN @myMax = @mymax2 THEN
                   t2.Row_Seq
               WHEN @myMax = @mymax5 THEN
                   t5.Row_Seq
               WHEN @myMax = @mymax1 THEN
                   t1.Row_Seq
           END AS Row_Seq,
           t1.Supplier,
           t1.ContractID,
           t1.ContractStartDate,
           t1.ContractExpiryDate,
           t1.WolseleyContractID,
           t1.ContractType,
           t1.PurchaseType,
           t1.PricingContractIndicator,
           t1.PricingContractID,
           t1.JobSite,
           t1.Developer,
           t1.ContactName,
           t1.ContactEmail,
           t2.Row_Seq AS AccountSeq,
           t2.AccountName,
           t2.AccountStartDate,
           t2.AccountExpiryDate,
           t3.Row_Seq AS ProdSeq,
           t3.Products,
           t3.WolseleyProductCode,
           t3.ProductDescription,
           t3.ProductDiscount,
           t3.ProductLimitType,
           t3.ProductLimit,
           t3.ProductStartDate,
           t3.ProductExpiryDate,
           t4.Row_Seq AS GroupSeq,
           t4.ProductGroups,
           t4.ProductGroupsDescription,
           t4.productGroupDiscount,
           t4.productGroupLimitType,
           t4.ProductGroupLimit,
           t4.ProductGroupStartDate,
           t4.ProductGroupExpiryDate,
           t5.Row_Seq AS LocationSeq,
           t5.LocationName,
           t5.LocationDescription,
           t5.LocationStartDate,
           t5.LocationExpiryDate
    FROM @table3 t3
        FULL OUTER JOIN @table2 t2
            ON t3.Row_Seq = t2.Row_Seq
        FULL OUTER JOIN @table1 t1
            ON t3.Row_Seq = t1.Row_Seq
        FULL OUTER JOIN @table4 t4
            ON t3.Row_Seq = t4.Row_Seq
        FULL OUTER JOIN @table5 t5
            ON t3.Row_Seq = t5.Row_Seq
    ORDER BY CASE
                 WHEN @myMax = @mymax3 THEN
                     t3.Row_Seq
                 WHEN @myMax = @mymax4 THEN
                     t4.Row_Seq
                 WHEN @myMax = @mymax2 THEN
                     t2.Row_Seq
                 WHEN @myMax = @mymax5 THEN
                     t5.Row_Seq
                 WHEN @myMax = @mymax1 THEN
                     t1.Row_Seq
             END;
ELSE IF @myMax = @mymax4
    SELECT NEWID() AS GUID,
           CASE
               WHEN @myMax = @mymax3 THEN
                   t3.Row_Seq
               WHEN @myMax = @mymax4 THEN
                   t4.Row_Seq
               WHEN @myMax = @mymax2 THEN
                   t2.Row_Seq
               WHEN @myMax = @mymax5 THEN
                   t5.Row_Seq
               WHEN @myMax = @mymax1 THEN
                   t1.Row_Seq
           END AS Row_Seq,
           t1.Supplier,
           t1.ContractID,
           t1.ContractStartDate,
           t1.ContractExpiryDate,
           t1.WolseleyContractID,
           t1.ContractType,
           t1.PurchaseType,
           t1.PricingContractIndicator,
           t1.PricingContractID,
           t1.JobSite,
           t1.Developer,
           t1.ContactName,
           t1.ContactEmail,
           t2.Row_Seq AS AccountSeq,
           t2.AccountName,
           t2.AccountStartDate,
           t2.AccountExpiryDate,
           t3.Row_Seq AS ProdSeq,
           t3.Products,
           t3.WolseleyProductCode,
           t3.ProductDescription,
           t3.ProductDiscount,
           t3.ProductLimitType,
           t3.ProductLimit,
           t3.ProductStartDate,
           t3.ProductExpiryDate,
           t4.Row_Seq AS GroupSeq,
           t4.ProductGroups,
           t4.ProductGroupsDescription,
           t4.productGroupDiscount,
           t4.productGroupLimitType,
           t4.ProductGroupLimit,
           t4.ProductGroupStartDate,
           t4.ProductGroupExpiryDate,
           t5.Row_Seq AS LocationSeq,
           t5.LocationName,
           t5.LocationDescription,
           t5.LocationStartDate,
           t5.LocationExpiryDate
    FROM @table4 t4
        FULL OUTER JOIN @table2 t2
            ON t4.Row_Seq = t2.Row_Seq
        FULL OUTER JOIN @table1 t1
            ON t4.Row_Seq = t1.Row_Seq
        FULL OUTER JOIN @table3 t3
            ON t4.Row_Seq = t3.Row_Seq
        FULL OUTER JOIN @table5 t5
            ON t4.Row_Seq = t5.Row_Seq
    ORDER BY CASE
                 WHEN @myMax = @mymax3 THEN
                     t3.Row_Seq
                 WHEN @myMax = @mymax4 THEN
                     t4.Row_Seq
                 WHEN @myMax = @mymax2 THEN
                     t2.Row_Seq
                 WHEN @myMax = @mymax5 THEN
                     t5.Row_Seq
                 WHEN @myMax = @mymax1 THEN
                     t1.Row_Seq
             END;

--should be 2
IF @myMax = @mymax2
    SELECT NEWID() AS GUID,
           CASE
               WHEN @myMax = @mymax3 THEN
                   t3.Row_Seq
               WHEN @myMax = @mymax4 THEN
                   t4.Row_Seq
               WHEN @myMax = @mymax2 THEN
                   t2.Row_Seq
               WHEN @myMax = @mymax5 THEN
                   t5.Row_Seq
               WHEN @myMax = @mymax1 THEN
                   t1.Row_Seq
           END AS Row_Seq,
           t1.Supplier,
           t1.ContractID,
           t1.ContractStartDate,
           t1.ContractExpiryDate,
           t1.WolseleyContractID,
           t1.ContractType,
           t1.PurchaseType,
           t1.PricingContractIndicator,
           t1.PricingContractID,
           t1.JobSite,
           t1.Developer,
           t1.ContactName,
           t1.ContactEmail,
           t2.Row_Seq AS AccountSeq,
           t2.AccountName,
           t2.AccountStartDate,
           t2.AccountExpiryDate,
           t3.Row_Seq AS ProdSeq,
           t3.Products,
           t3.WolseleyProductCode,
           t3.ProductDescription,
           t3.ProductDiscount,
           t3.ProductLimitType,
           t3.ProductLimit,
           t3.ProductStartDate,
           t3.ProductExpiryDate,
           t4.Row_Seq AS GroupSeq,
           t4.ProductGroups,
           t4.ProductGroupsDescription,
           t4.productGroupDiscount,
           t4.productGroupLimitType,
           t4.ProductGroupLimit,
           t4.ProductGroupStartDate,
           t4.ProductGroupExpiryDate,
           t5.Row_Seq AS LocationSeq,
           t5.LocationName,
           t5.LocationDescription,
           t5.LocationStartDate,
           t5.LocationExpiryDate
    FROM @table2 t2
        FULL OUTER JOIN @table4 t4
            ON t2.Row_Seq = t4.Row_Seq
        FULL OUTER JOIN @table1 t1
            ON t2.Row_Seq = t1.Row_Seq
        FULL OUTER JOIN @table3 t3
            ON t2.Row_Seq = t3.Row_Seq
        FULL OUTER JOIN @table5 t5
            ON t2.Row_Seq = t5.Row_Seq
    ORDER BY CASE
                 WHEN @myMax = @mymax3 THEN
                     t3.Row_Seq
                 WHEN @myMax = @mymax4 THEN
                     t4.Row_Seq
                 WHEN @myMax = @mymax2 THEN
                     t2.Row_Seq
                 WHEN @myMax = @mymax5 THEN
                     t5.Row_Seq
                 WHEN @myMax = @mymax1 THEN
                     t1.Row_Seq
             END;
--end of 2
ELSE IF @myMax = @mymax5
    SELECT NEWID() AS GUID,
           CASE
               WHEN @myMax = @mymax3 THEN
                   t3.Row_Seq
               WHEN @myMax = @mymax4 THEN
                   t4.Row_Seq
               WHEN @myMax = @mymax2 THEN
                   t2.Row_Seq
               WHEN @myMax = @mymax5 THEN
                   t5.Row_Seq
               WHEN @myMax = @mymax1 THEN
                   t1.Row_Seq
           END AS Row_Seq,
           t1.Supplier,
           t1.ContractID,
           t1.ContractStartDate,
           t1.ContractExpiryDate,
           t1.WolseleyContractID,
           t1.ContractType,
           t1.PurchaseType,
           t1.PricingContractIndicator,
           t1.PricingContractID,
           t1.JobSite,
           t1.Developer,
           t1.ContactName,
           t1.ContactEmail,
           t2.Row_Seq AS AccountSeq,
           t2.AccountName,
           t2.AccountStartDate,
           t2.AccountExpiryDate,
           t3.Row_Seq AS ProdSeq,
           t3.Products,
           t3.WolseleyProductCode,
           t3.ProductDescription,
           t3.ProductDiscount,
           t3.ProductLimitType,
           t3.ProductLimit,
           t3.ProductStartDate,
           t3.ProductExpiryDate,
           t4.Row_Seq AS GroupSeq,
           t4.ProductGroups,
           t4.ProductGroupsDescription,
           t4.productGroupDiscount,
           t4.productGroupLimitType,
           t4.ProductGroupLimit,
           t4.ProductGroupStartDate,
           t4.ProductGroupExpiryDate,
           t5.Row_Seq AS LocationSeq,
           t5.LocationName,
           t5.LocationDescription,
           t5.LocationStartDate,
           t5.LocationExpiryDate
    FROM @table5 t5
        FULL OUTER JOIN @table2 t2
            ON t5.Row_Seq = t2.Row_Seq
        FULL OUTER JOIN @table1 t1
            ON t5.Row_Seq = t1.Row_Seq
        FULL OUTER JOIN @table3 t3
            ON t5.Row_Seq = t3.Row_Seq
        FULL OUTER JOIN @table4 t4
            ON t5.Row_Seq = t4.Row_Seq
    ORDER BY CASE
                 WHEN @myMax = @mymax3 THEN
                     t3.Row_Seq
                 WHEN @myMax = @mymax4 THEN
                     t4.Row_Seq
                 WHEN @myMax = @mymax2 THEN
                     t2.Row_Seq
                 WHEN @myMax = @mymax5 THEN
                     t5.Row_Seq
                 WHEN @myMax = @mymax1 THEN
                     t1.Row_Seq
             END;
IF @myMax = @mymax1
    SELECT NEWID() AS GUID,
           CASE
               WHEN @myMax = @mymax3 THEN
                   t3.Row_Seq
               WHEN @myMax = @mymax4 THEN
                   t4.Row_Seq
               WHEN @myMax = @mymax2 THEN
                   t2.Row_Seq
               WHEN @myMax = @mymax5 THEN
                   t5.Row_Seq
               WHEN @myMax = @mymax1 THEN
                   t1.Row_Seq
           END AS Row_Seq,
           t1.Supplier,
           t1.ContractID,
           t1.ContractStartDate,
           t1.ContractExpiryDate,
           t1.WolseleyContractID,
           t1.ContractType,
           t1.PurchaseType,
           t1.PricingContractIndicator,
           t1.PricingContractID,
           t1.JobSite,
           t1.Developer,
           t1.ContactName,
           t1.ContactEmail,
           t2.Row_Seq AS AccountSeq,
           t2.AccountName,
           t2.AccountStartDate,
           t2.AccountExpiryDate,
           t3.Row_Seq AS ProdSeq,
           t3.Products,
           t3.WolseleyProductCode,
           t3.ProductDescription,
           t3.ProductDiscount,
           t3.ProductLimitType,
           t3.ProductLimit,
           t3.ProductStartDate,
           t3.ProductExpiryDate,
           t4.Row_Seq AS GroupSeq,
           t4.ProductGroups,
           t4.ProductGroupsDescription,
           t4.productGroupDiscount,
           t4.productGroupLimitType,
           t4.ProductGroupLimit,
           t4.ProductGroupStartDate,
           t4.ProductGroupExpiryDate,
           t5.Row_Seq AS LocationSeq,
           t5.LocationName,
           t5.LocationDescription,
           t5.LocationStartDate,
           t5.LocationExpiryDate
    FROM @table1 t1
        FULL OUTER JOIN @table2 t2
            ON t1.Row_Seq = t2.Row_Seq
        FULL OUTER JOIN @table5 t5
            ON t1.Row_Seq = t5.Row_Seq
        FULL OUTER JOIN @table3 t3
            ON t1.Row_Seq = t3.Row_Seq
        FULL OUTER JOIN @table4 t4
            ON t1.Row_Seq = t4.Row_Seq
    ORDER BY CASE
                 WHEN @myMax = @mymax3 THEN
                     t3.Row_Seq
                 WHEN @myMax = @mymax4 THEN
                     t4.Row_Seq
                 WHEN @myMax = @mymax2 THEN
                     t2.Row_Seq
                 WHEN @myMax = @mymax5 THEN
                     t5.Row_Seq
                 WHEN @myMax = @mymax1 THEN
                     t1.Row_Seq
             END;


/***********************************************************************************/
GO
