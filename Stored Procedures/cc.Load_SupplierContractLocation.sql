SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [cc].[Load_SupplierContractLocation] AS 
BEGIN

	SET NOCOUNT ON

	EXEC LogDCPEvent 'cc.Load_SupplierContractLocation', 'B'

	DECLARE @RowsI INT,  @RowsU INT,  @RowsD INT

	
-- insert new from ContractBranch
	INSERT INTO cc.SupplierContractLocation
	        ( ContractKey ,
	          ContractLocationLevelKey ,
	          LocationKey ,
			  LocationName,
			  LocationDescription,
			  CreatedOnDT
	        )
	SELECT DISTINCT 
		sc.ContractKey,
		1,
		dag1.AccountGroup1Key,
		dag1.AG1Level1,
		dag1.AG1Level1UDVarchar1,
		sc.ContractCreationDT
	FROM Lion_Staging.dbo.ContractBranch cb
	JOIN cc.SupplierContract sc 
		ON cb.OriginatorBranchOrRegion + cb.ContractNumber = sc.ContractID
	JOIN dbo.DimAccountGroup1 dag1
		ON dag1.AG1Level1 = cb.ContractBranches
	LEFT JOIN cc.SupplierContractLocation scl
		ON scl.ContractKey = sc.ContractKey
		AND scl.ContractLocationLevelKey = 1 -- 1=branch
		AND scl.LocationKey = dag1.AccountGroup1Key
	WHERE
		scl.ContractLocationKey IS NULL
		AND sc.ContractSourceKey = 1
		AND dag1.AG1Level1 NOT IN ('F14', 'F99', 'DA', 'TA', 'WA', 'ZA')		-- LPF-4244 Modify Load Contract to exclude loading non-branch branches - kfk
	
	SELECT @RowsI = @@ROWCOUNT

--Delete branch records missing in the staging table
	DELETE FROM cc.SupplierContractLocation
	WHERE ContractLocationKey IN 
	(SELECT scl.ContractLocationKey
	FROM cc.SupplierContractLocation scl
	JOIN cc.SupplierContract sc 
		ON sc.ContractKey = scl.ContractKey
	JOIN dbo.DimAccountGroup1 dag1
		ON dag1.AccountGroup1Key = scl.LocationKey
	LEFT JOIN Lion_Staging.dbo.ContractBranch cb
		ON cb.ContractBranches = dag1.AG1Level1
		AND cb.OriginatorBranchOrRegion + cb.ContractNumber = sc.ContractID
	WHERE sc.ContractSourceKey = 1
		AND scl.ContractLocationLevelKey = 1 -- 1=branch
		AND cb.ContractNumber IS NULL)

	SELECT @RowsD = @@ROWCOUNT


	EXEC LogDCPEvent 'cc.Load_SupplierContractLocation', 'E', @RowsI, 0, @RowsD

END
GO
