SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [ppe].[Create_Recommendations] 
(
@RecommendKey INT,
@ParamDebug [char](1) = 'N'
)
AS

/*

	EXEC ppe.Create_Recommendations2 6
	EXEC ppe.Create_RecommendationsBase 6


	declare @RecommendKey INT
	SELECT @RecommendKey =6

*/



SET NOCOUNT ON
SET ANSI_WARNINGS OFF 

DECLARE 
	@LowerTradeThreshold UDDecimal_type, 
	@TradeFactor UDDecimal_type, 
	@DefaultAction NVARCHAR(255),
	@WebUserKey INT,
	@IncSalesTeam BIT,
	@IncBranch BIT,
	@SkipDurationMonths INT, 
	@MaxSIChange UDDecimal_type = 0.00,
	@MaxChange UDDecimal_type = 0.02, 
	@TermsStartDate DATE,

	@LLSPGInstruction NVARCHAR(255), 
	@ItemInstruction NVARCHAR(255), 
	@MarketInstruction NVARCHAR(255), 
	@UnusedInstruction NVARCHAR(255), 
	@UnknownInstruction NVARCHAR(255), 
	@NonQualException NVARCHAR(255),
	@LowSalesInstruction NVARCHAR(255), 
	@RecentChange NVARCHAR(255), 
	@TBDInstruction NVARCHAR(255)

SELECT 
	@MaxSIChange = MaxSIChange, 
	@MaxChange = MaxChange,
	@WebUserKey = WebUserKey,
	@IncSalesTeam = ISNULL(IncludeSalesTeamManagers,1),
	@IncBranch = ISNULL(IncludeBranchManagers,1),
	@SkipDurationMonths = SkipDurationMonths,
	@TermsStartDate = TermsEffectiveDate
FROM 
	ppe.Recommend 
WHERE RecommendKey = @RecommendKey

SET @LowerTradeThreshold = -.01
SET @TradeFactor = 1 + @LowerTradeThreshold
SET @DefaultAction = 'Pending'

SET @LLSPGInstruction	 = '1 - LLSPG Disc %'
SET @ItemInstruction	 = '2 - Item Disc %'
SET @MarketInstruction	 = '3 - Market (No Terms)'
SET @UnusedInstruction	 = '4 - Unused Exception'
SET @NonQualException    = '5 - Non-Qual Exception'
SET @UnknownInstruction	 = '6 - Unknown'
SET @LowSalesInstruction = '7 - Low Sales'
SET @RecentChange        = '8 - Recent Change'
SET @TBDInstruction		 = 'TBD'


CREATE TABLE #UserAccount (AccountKey INT)
CREATE TABLE #UserAccountOwner (AccountKey INT)
CREATE TABLE #UserArea (AccountKey INT)
CREATE TABLE #UserBranch (AccountKey INT)
CREATE TABLE #UserNetwork (AccountKey INT)
CREATE TABLE #Account (AccountKey INT)

IF EXISTS (SELECT TOP 1 * FROM ppe.RecommendAccount WHERE  RecommendKey = @RecommendKey)
	INSERT INTO #UserAccount ( AccountKey )
	SELECT AccountKey
	FROM ppe.RecommendAccount 
	WHERE  RecommendKey = @RecommendKey
ELSE 
	INSERT INTO #UserAccount ( AccountKey )
	SELECT AccountKey
	FROM dbo.DimAccount

IF EXISTS (SELECT TOP 1 * FROM ppe.RecommendAccountOwner WHERE  RecommendKey = @RecommendKey)
	INSERT INTO #UserAccountOwner ( AccountKey )
	/* RecommendAccountOwner - Really should be RecommendAccountBrandOwner */
	SELECT dabm.AccountKey
	FROM dbo.DimAccountBrandManager dabm 
	JOIN dbo.DimBrand db ON db.BrandKey = dabm.BrandKey --jed
	JOIN DimAccountGroup1 dag1 ON dag1.AG1Level1UDVarchar6 = db.Brand --jed
	JOIN ppe.RecommendAccountOwner rao ON rao.AccountManagerKey = dabm.AccountManagerKey
	WHERE rao.RecommendKey = @RecommendKey		
	UNION
	/*RecommendAccountOwner use Account Owner if Brand Owner does not exist per LPF-2316 and Scott Mon 6/20/2016 12:35 PM email*/
	SELECT da.AccountKey
	FROM dbo.DimAccount da
	JOIN ppe.RecommendAccountOwner rao ON rao.AccountManagerKey = da.AccountManagerKey
	WHERE rao.RecommendKey = @RecommendKey
	AND NOT EXISTS (SELECT TOP 1 * FROM dbo.DimAccountBrandManager dabm WHERE dabm.AccountKey = da.AccountKey)
ELSE 
	INSERT INTO #UserAccountOwner ( AccountKey )
	SELECT AccountKey
	FROM dbo.DimAccount


IF EXISTS (SELECT TOP 1 * FROM ppe.RecommendArea WHERE  RecommendKey = @RecommendKey)
	INSERT INTO #UserArea ( AccountKey )
	SELECT AccountKey
	FROM dbo.DimAccount da 
	JOIN dbo.DimAccountGroup1 dag1 ON dag1.AccountGroup1Key = da.AccountGroup1Key
	JOIN ppe.RecommendArea rarea ON rarea.AG1Level3 = dag1.AG1Level3
	WHERE rarea.RecommendKey = @RecommendKey 
ELSE 
	INSERT INTO #UserArea ( AccountKey )
	SELECT AccountKey
	FROM dbo.DimAccount


IF EXISTS (SELECT TOP 1 * FROM ppe.RecommendBranch WHERE  RecommendKey = @RecommendKey)
	INSERT INTO #UserBranch ( AccountKey )
	SELECT da.AccountKey
	FROM dbo.DimAccount da 
	JOIN dbo.DimAccountGroup1 dag1 ON dag1.AccountGroup1Key = da.AccountGroup1Key
	JOIN ppe.RecommendBranch rb ON rb.AG1Level1 = dag1.AG1Level1
	WHERE rb.RecommendKey = @RecommendKey
ELSE 
	INSERT INTO #UserBranch ( AccountKey )
	SELECT AccountKey
	FROM dbo.DimAccount


IF EXISTS (SELECT TOP 1 * FROM ppe.RecommendNetwork WHERE  RecommendKey = @RecommendKey)
	INSERT INTO #UserNetwork ( AccountKey )
	SELECT da.AccountKey
	FROM dbo.DimAccount da 
	JOIN dbo.DimAccountGroup1 dag1 ON dag1.AccountGroup1Key = da.AccountGroup1Key
	JOIN ppe.RecommendNetwork rn ON rn.AG1Level2 = dag1.AG1Level2
	WHERE rn.RecommendKey = @RecommendKey
ELSE 
	INSERT INTO #UserNetwork ( AccountKey )
	SELECT AccountKey
	FROM dbo.DimAccount


INSERT INTO #Account(AccountKey)
SELECT DISTINCT ua.AccountKey
FROM #UserAccount ua 
JOIN #UserAccountOwner uao ON uao.AccountKey = ua.AccountKey
JOIN #UserArea uarea ON uarea.AccountKey = ua.AccountKey
JOIN #UserBranch ub ON ub.AccountKey = ua.AccountKey
JOIN #UserNetwork un ON un.AccountKey = ua.AccountKey
JOIN dbo.DimAccount da ON da.AccountKey = ua.AccountKey
JOIN dbo.DimAccountGroup2 dag2 ON dag2.AccountGroup2Key = da.AccountGroup2Key
WHERE ( ( da.AccountUDVarchar8 IN ('National','Sales Team') AND @IncSalesTeam = 1) --LPF-1625
						OR
		( da.AccountUDVarchar8 NOT IN ('National','Sales Team') AND @IncBranch = 1)
		)
AND ISNULL(dag2.AG2Level1,'') <> 'CS'
AND da.Inactive = 0 -- LPF-2935

IF @ParamDebug = 'Y'
BEGIN 
	SELECT '#UserAccount count: ', COUNT(*) FROM #UserAccount
	SELECT '#UserAccountOwner count: ', COUNT(*) FROM #UserAccountOwner
	SELECT '#UserArea count: ', COUNT(*) FROM #UserArea
	SELECT '#UserBranch count: ', COUNT(*) FROM #UserBranch
	SELECT '#UserNetwork count: ', COUNT(*) FROM #UserNetwork
END

DROP TABLE #UserAccount
DROP TABLE #UserAccountOwner
DROP TABLE #UserArea
DROP TABLE #UserBranch
DROP TABLE #UserNetwork

CREATE TABLE #UserSPG(ItemGroup3Key INT)
CREATE TABLE #UserBrand(ItemGroup3Key INT)	
CREATE TABLE #SPG(ItemGroup3Key INT)	

IF EXISTS (SELECT TOP 1 * FROM ppe.RecommendSPG WHERE  RecommendKey = @RecommendKey)
	INSERT INTO #UserSPG ( ItemGroup3Key )
	SELECT ItemGroup3Key
	FROM ppe.RecommendSPG WHERE  RecommendKey = @RecommendKey
ELSE 
	INSERT INTO #UserSPG ( ItemGroup3Key )
	SELECT ItemGroup3Key
	FROM dbo.DimItemGroup3

IF EXISTS (SELECT TOP 1 * FROM ppe.RecommendBrand WHERE  RecommendKey = @RecommendKey)
	INSERT INTO #UserBrand( ItemGroup3Key )
	SELECT ItemGroup3Key
	FROM ppe.RecommendBrand Wrb
	JOIN dbo.DimItemGroup3 dig3 ON dig3.IG3Level5 = Wrb.BranchPrimaryBrand
	WHERE RecommendKey = @RecommendKey
ELSE 
	INSERT INTO #UserBrand( ItemGroup3Key )
	SELECT ItemGroup3Key
	FROM dbo.DimItemGroup3

INSERT INTO #SPG(ItemGroup3Key)
SELECT spg.ItemGroup3Key
FROM #UserSPG spg
JOIN #UserBrand brand ON brand.ItemGroup3Key = spg.ItemGroup3Key

IF EXISTS (SELECT TOP 1 * FROM ppe.RecommendItem  WHERE  RecommendKey = @RecommendKey) -- performance improvement per LPF-3578
BEGIN
	DELETE
	FROM #SPG
	WHERE ItemGroup3Key NOT IN (
					SELECT DISTINCT ItemGroup3Key 
					FROM ppe.RecommendItem ri
					JOIN dbo.DimItem di ON di.ItemKey = ri.ItemKey
					WHERE  RecommendKey = @RecommendKey )
END


IF @ParamDebug = 'Y'
BEGIN 
	SELECT '#UserSPG count: ', COUNT(*) FROM #UserSPG
	SELECT '#UserBrand count: ', COUNT(*) FROM #UserBrand
	SELECT '#SPG count: ', COUNT(*) FROM #SPG
END


DROP TABLE #UserSPG
DROP TABLE #UserBrand

CREATE UNIQUE CLUSTERED INDEX i_temp ON #Account(AccountKey)

CREATE UNIQUE CLUSTERED INDEX i_temp ON #SPG(ItemGroup3Key)

IF NOT EXISTS (SELECT TOP 1 * FROM #Account)
BEGIN

	INSERT INTO #Account (AccountKey) /*per LPF-1415 if none add accounts for all SPGs*/
	SELECT DISTINCT f.AccountKey
	FROM FactLastPriceAndCost f
	JOIN dbo.DimItem di ON di.ItemKey = f.ItemKey
	JOIN #SPG s ON di.ItemGroup3Key = s.ItemGroup3Key

END

IF NOT EXISTS (SELECT TOP 1 * FROM #SPG)
BEGIN

	INSERT INTO #SPG (ItemGroup3Key) /*per LPF-1415 if none add SPGs for all accounts*/
	SELECT DISTINCT di.ItemGroup3Key
	FROM FactLastPriceAndCost f
	JOIN dbo.DimItem di ON di.ItemKey = f.ItemKey
	JOIN #Account a ON a.AccountKey = f.AccountKey
	UNION
	SELECT ct.ItemGroup3Key
	FROM  lion.CustomerTerms ct
	JOIN #Account a ON a.AccountKey = ct.AccountKey
	JOIN dbo.DimItemGroup3 dig3 ON dig3.ItemGroup3Key = ct.ItemGroup3Key
	WHERE dig3.IG3Level4 = '1'
	AND ct.ItemKey = 1

END


IF @ParamDebug = 'Y'
BEGIN 
	SELECT '#Account count: ', COUNT(*) FROM #Account
	SELECT '#SPG count: ', COUNT(*) FROM #SPG
END


-- Get ACCOUNT contract LLSPG Terms
SELECT
	act.ItemGroup3Key,
	act.ItemKey,
	act.ContractName,
	act.SPG AS SPGCode,
	MAX(1 - (1 - ISNULL(act.UsualDiscount1 / 100, 0)) * (1 - ISNULL(act.UsualDiscount2 / 100, 0))) AS Discount
INTO #DefaultTerms
FROM lion.AccountContractTerms act
INNER JOIN #SPG spg
	ON spg.ItemGroup3Key = act.ItemGroup3Key
WHERE SPGExpiryDate > GETDATE() 
GROUP BY
	act.ItemGroup3Key,
	act.ItemKey,
	act.ContractName,
	act.SPG


--PRINT 'step 2: ' + CONVERT(VARCHAR(50), GETDATE(),13)

-- Get existing LLSPG-level terms for given account
    SELECT DISTINCT
        ct.AccountKey ,
		ct.ItemGroup3Key,
        ct.PyramidCode ,
        ct.SPGCode ,
		dig3.IG3Level1UDVarchar1 AS LLSPGDescription ,
        OwningBrandCode ,
        ct.Usualdiscount1 / 100 AS SPGDisc1 ,
        ct.Usualdiscount2 / 100 AS SPGDisc2 ,
        1 - (1 - ISNULL(ct.Usualdiscount1 / 100, 0)) * (1 - ISNULL(ct.Usualdiscount2 / 100, 0)) AS Disc ,
        DeleteFlag ,
        LastChangeInitials,
        CONVERT(DATE, LEFT(ct.LastChangeDate, CHARINDEX(' ', ct.LastChangeDate))) AS LastChangeDate,
        LastChangeBranch
INTO    #LLSPGTerms
FROM    lion.CustomerTerms ct
INNER JOIN #Account a2 ON a2.AccountKey = ct.AccountKey
INNER JOIN #SPG s ON s.ItemGroup3Key = ct.ItemGroup3Key
INNER JOIN dbo.DimItemGroup3 dig3 ON dig3.ItemGroup3Key = ct.ItemGroup3Key
WHERE dig3.IG3Level4 = '1'
AND ct.ItemKey = 1 


IF @ParamDebug = 'Y'
	SELECT '#LLSPGTerms count: ', COUNT(*) FROM #LLSPGTerms


-- Get LLSPGs purchased by the customer
SELECT  DISTINCT
        f.AccountKey,
		i.ItemGroup3Key,
        i.ItemUDVarchar1 AS ItemPyramidCode,
        dig3.IG3Level1 AS LLSPGCode,
		dig3.IG3Level1UDVarchar1 LLSPGDescription,
        acct.BranchPrimaryBrand
INTO    #PrelimSoldLLSPG
FROM    dbo.FactLastPriceAndCost f
        INNER JOIN lion.vwAccount acct ON f.AccountKey = acct.AccountKey
		INNER JOIN #Account a2 ON a2.AccountKey = f.AccountKey
        INNER JOIN DimItem i ON f.ItemKey = i.ItemKey
		INNER JOIN #SPG s ON s.ItemGroup3Key = i.ItemGroup3Key
		INNER JOIN dbo.DimItemGroup3 dig3 ON dig3.ItemGroup3Key = i.ItemGroup3Key
        INNER JOIN lion.vwFactInvoiceLineGroup1 g1 ON f.InvoiceLineGroup1Key = g1.InvoiceLineGroup1Key
WHERE   g1.ShipmentType = 'S'
        AND g1.DeviatedIndicator = 'N'


IF @ParamDebug = 'Y'
	SELECT '#PrelimSoldLLSPG count: ', COUNT(*) FROM #PrelimSoldLLSPG


--PRINT 'step 3: ' + CONVERT(VARCHAR(50), GETDATE(),13)


-- Combine existing LLSPG-level terms with other LLSPGs purchased by the customer
SELECT  DISTINCT
        COALESCE(s.AccountKey, t.AccountKey) AS AccountKey ,
        COALESCE(s.ItemPyramidCode, CONVERT(INT, t.PyramidCode)) AS PyramidCode ,
        COALESCE(s.LLSPGCode, t.SPGCode) AS LLSPGCode ,
		COALESCE(s.LLSPGDescription, t.LLSPGDescription) AS LLSPGDescription ,
        -- for LPF-1695 replace with below COALESCE(t.OwningBrandCode, s.BranchPrimaryBrand) AS Brand ,
		t.OwningBrandCode,
        t.SPGDisc1,
        t.SPGDisc2 ,
        t.Disc,
        t.DeleteFlag,
        t.LastChangeInitials,
        t.LastChangeDate,
        t.LastChangeBranch ,
        CONVERT(NVARCHAR(255), '') AS PriceInstructionTypeLevel ,
		CONVERT(DECIMAL(38,8), t.Disc) AS RecommendedDiscount ,
        CASE WHEN ( s.AccountKey IS NULL) 
			    THEN IIF(ISNULL(t.[LastChangeDate], '1-JAN-2015') <= DATEADD(dd, -184, CAST(getdate() AS DATE)), 'Delete', 'Skip')
                ELSE @DefaultAction
        END AS BaseAction ,
		' ' AS KVI,
		COALESCE(s.ItemGroup3Key,t.ItemGroup3Key) AS ItemGroup3Key 
INTO    #PrelimLLSPG
FROM    #PrelimSoldLLSPG s
FULL OUTER JOIN #LLSPGTerms t 
	ON t.AccountKey = s.AccountKey
	AND CONVERT(INT, t.PyramidCode) = s.ItemPyramidCode
	AND t.SPGCode = s.LLSPGCode


IF @ParamDebug = 'Y'
	SELECT '#PrelimLLSPG count: ', COUNT(*) FROM #PrelimLLSPG


-- Get existing product exception terms
SELECT  CustomerTermsKey,
        ct.AccountKey ,
        ct.ItemKey ,
        PyramidCode,
        SPGCode,
        /*a.BranchPrimaryBrand AS removed per LPF-1733*/ OwningBrandCode ,
        Usualdiscount1 / 100 AS UsualDiscount1,
        Usualdiscount2 / 100 AS UsualDiscount2,
        DeleteFlag ,
        LastChangeInitials ,
        CONVERT(DATE, LEFT(ct.LastChangeDate, CHARINDEX(' ', ct.LastChangeDate))) AS LastChangeDate,
        LastChangeTime ,
        LastChangePLID ,
        LastChangeBranch ,
        ExceptionProduct ,
        ExceptionDiscount / 100 AS ExceptionDiscount,
        ExceptionFixedPrice ,
        ExceptionFromDate ,
        ExceptionToDate ,
        ExceptionPCF,
		di.ItemUDVarchar13 AS KVI,
		ct.ItemGroup3Key
INTO #ItemTerms
FROM    lion.CustomerTerms ct
--INNER JOIN lion.vwAccount a ON ct.AccountKey = a.AccountKey
--INNER JOIN lion.vwItem i ON i.ProductCode = ct.ExceptionProduct AND i.ItemPyramidCode = 1
INNER JOIN #Account a2 ON a2.AccountKey = ct.AccountKey
INNER JOIN dbo.DimItem di ON di.ItemKey = ct.ItemKey
INNER JOIN #SPG s ON s.ItemGroup3Key = ct.ItemGroup3Key
WHERE   ct.ExceptionProduct <> ''
AND CONVERT (INT, ct.PyramidCode) = 1
AND di.InActive = 0 --LPF-2935

CREATE CLUSTERED INDEX i_temp ON #ItemTerms(AccountKey, ItemKey)


IF @ParamDebug = 'Y'
BEGIN 
	SELECT '#ItemTerms count: ', COUNT(*) FROM #ItemTerms
	--SELECT '#SPG' AS label, * FROM #SPG
	--SELECT '#ItemTerms' AS label, * FROM #ItemTerms
END


SELECT v.* 
INTO #Discounts
FROM ppe.vw_AccountSPGDiscount v
JOIN #Account a ON a.AccountKey = v.AccountKey
JOIN #SPG s ON s.ItemGroup3Key = v.ItemGroup3Key

CREATE UNIQUE CLUSTERED INDEX i_temp ON #Discounts(ItemGroup3Key, AccountKey)


IF @ParamDebug = 'Y'
	SELECT '#Discounts count: ', COUNT(*) FROM #Discounts

--PRINT 'step 4: ' +CONVERT(VARCHAR(50), GETDATE(),13)


;WITH SPGTerms AS (
	SELECT ct.AccountKey, ct.SPGCode, MAX(CONVERT(DATE,ct.LastChangeDate)) AS LastChangeDate
	FROM lion.CustomerTerms ct
	JOIN #Account a ON a.AccountKey = ct.AccountKey
	WHERE ct.PyramidCode = '1'
	GROUP BY ct.AccountKey, ct.SPGCode
)

SELECT  f.AccountKey ,
	    di.ItemGroup3Key,
        f.ItemKey ,
        f.InvoiceLineGroup1Key , 
	-- Product Information
        i.LLSPGCode ,
        i.LLSPGCode + ' - ' + i.LLSPGDescription AS LLSPG ,
        i.ItemNumber AS ProductCode,
        i.ItemDescription AS ProductDescription,
        acct.BranchPrimaryBrand ,
		i.CurrentCost AS CurrentCost,
		CASE 
			WHEN (t.Price = 0 ) THEN IIF(ISNULL(s.LastChangeDate, '1-JAN-2015') <= DATEADD(dd, -184, CAST(GETDATE() AS DATE)), 'Delete', 'Skip')
			WHEN it.ItemKey IS NOT NULL THEN @DefaultAction 
			WHEN ( lil.PriceDerivation IN ('ADJ', 'DC', 'DP', 'SPO') AND f.UDVarchar1 <> 'Y') THEN 'Skip' -- f.UDVarchar1 = StrategicItem
			WHEN (i.KnownValueInd = 'Y' AND 
					NOT EXISTS (SELECT * 
								FROM lion.AccountSPG aspg 
								WHERE aspg.AccountKey = f.AccountKey AND 
										aspg.LLSPGCode IN (i.LLSPGCode, 'ALL')))
				THEN 'Skip'
			ELSE @DefaultAction 
		END AS BaseAction ,
        CASE WHEN ( t.Price = 0 ) THEN @UnknownInstruction
			    WHEN it.ItemKey IS NOT NULL OR f.UDVarchar1 = 'Y' THEN @ItemInstruction
			    WHEN ( lil.PriceDerivation IN ('DP', 'DC', 'ADJ', 'SPO', 'OCD', 'OCL') AND f.UDVarchar1 <> 'Y') THEN @NonQualException -- f.UDVarchar1 = StrategicItem
			    WHEN ( f.UDVarchar1 <> 'Y' AND dd.SQLDate < ISNULL(s.LastChangeDate, dd.SQLDate)) THEN @NonQualException -- f.UDVarchar1 = StrategicItem
				WHEN (i.KnownValueInd = 'Y' AND 
			                NOT EXISTS (SELECT * 
							            FROM lion.AccountSPG aspg 
										WHERE aspg.AccountKey = f.AccountKey AND 
										        aspg.LLSPGCode IN (i.LLSPGCode, 'ALL'))) OR
                    ( alip.Max - t.Price ) / NULLIF(t.Price, 0) >= @LowerTradeThreshold
                THEN @MarketInstruction
                --WHEN alip.Max <= r.Price
                --THEN IIF(f.UDVarchar1 = 'Y', @ItemInstruction, @TBDInstruction)
				ELSE @TBDInstruction
                --WHEN alip.Max <= a.Price
                --THEN @TBDInstruction
                --WHEN alip.Max <= g.Price THEN @MarketInstruction
                --ELSE @MarketInstruction
        END AS PriceInstructionTypeLevel,

        CASE WHEN it.ItemKey is not null and f.UDVarchar1='N'
			    THEN IIF(it.ExceptionFixedPrice is not null, 
				        it.ExceptionFixedPrice, 
				        IIF((t.Price * (1-it.ExceptionDiscount)) + (t.Price * @MaxChange) < g.Price, (t.Price * (1-it.ExceptionDiscount)) + (t.Price * @MaxChange), g.Price)
						)
				WHEN i.KnownValueInd = 'Y' and 
			                not exists (select 1 
							            from lion.AccountSPG aspg 
										where aspg.AccountKey = f.AccountKey and 
										        aspg.LLSPGCode IN (i.LLSPGCode, 'ALL'))
				THEN t.Price
				WHEN it.ItemKey is not null and f.UDVarchar1='Y' -- Strategic Item
				THEN IIF(it.ExceptionFixedPrice is not null, 
				        it.ExceptionFixedPrice, 
				        IIF((t.Price * (1-it.ExceptionDiscount)) + (t.Price * @MaxSIChange) < g.Price, (t.Price * (1-it.ExceptionDiscount)) + (t.Price * @MaxSIChange), g.Price)
						)
			    WHEN lil.PriceDerivation IN ('DP', 'DC', 'ADJ', 'SPO', 'OCD', 'OCL') and f.UDVarchar1 <> 'Y'
			    THEN alip.Max
				WHEN f.UDVarchar1='Y' AND alip.Max <= g.Price
					-- Move by 0.5% up to green
				THEN IIF(alip.Max + (t.Price * @MaxSIChange) < g.Price, alip.Max + (t.Price * @MaxSIChange), g.Price)
			    WHEN ( alip.Max - t.Price ) / NULLIF(t.Price, 0) >= @LowerTradeThreshold
					-- Apply Trade Price Matching Logic
                THEN IIF(alip.Max + (t.Price * @MaxChange) < t.Price, alip.Max + (t.Price * @MaxChange), t.Price)
                WHEN alip.Max <= g.Price
					-- Move by 2% up to green
				THEN IIF(alip.Max + (t.Price * @MaxChange) < g.Price, alip.Max + (t.Price * @MaxChange), g.Price)
					-- If already charging above green, use the last price
                ELSE alip.Max
		END AS RecommendPrice ,
        ROUND(CASE WHEN it.ItemKey is not null and f.UDVarchar1='N' 
					THEN IIF(it.ExceptionFixedPrice is not null, 
							NULL, 
							(1 - ( IIF((t.Price * (1-it.ExceptionDiscount)) + (t.Price * @MaxChange) < g.Price, (t.Price * (1-it.ExceptionDiscount)) + (t.Price * @MaxChange), g.Price) / NULLIF(t.Price, 0)))
							)
					WHEN i.KnownValueInd = 'Y' and 
			                not exists (select * 
							            from lion.AccountSPG aspg 
										where aspg.AccountKey = f.AccountKey and 
										        aspg.LLSPGCode IN (i.LLSPGCode, 'ALL'))
					THEN 0
				    WHEN it.ItemKey is not null and f.UDVarchar1='Y' -- Strategic Item
				    THEN IIF(it.ExceptionFixedPrice is not null, 
				            null, 
				            (1 - IIF((t.Price * (1-it.ExceptionDiscount)) + (t.Price * @MaxSIChange) < g.Price, (t.Price * (1-it.ExceptionDiscount)) + (t.Price * @MaxSIChange), g.Price) / NULLIF(t.Price, 0))
						    ) 
			        WHEN lil.PriceDerivation IN ('DP', 'DC', 'ADJ', 'SPO', 'OCD', 'OCL') and f.UDVarchar1 <> 'Y'
			        THEN 1-(alip.Max / NULLIF(t.Price, 0))
				    WHEN f.UDVarchar1='Y' AND alip.Max <= g.Price
				      	-- Move by 0.5% up to green
				    THEN (1 - ( IIF(alip.Max + (t.Price * @MaxSIChange) < g.Price, alip.Max + (t.Price * @MaxSIChange), g.Price) / NULLIF(t.Price, 0)))
				    WHEN ( alip.Max - t.Price ) / NULLIF(t.Price, 0) >= @LowerTradeThreshold
						-- Apply Trade Price Matching Logic
					THEN (1 - ( IIF(alip.Max + (t.Price * @MaxChange) < t.Price, alip.Max + (t.Price * @MaxChange), t.Price) / NULLIF(t.Price, 0)))
					WHEN alip.Max <= g.Price
						-- Move by 2% up to green
					THEN (1 - ( IIF(alip.Max + (t.Price * @MaxChange) < g.Price, alip.Max + (t.Price * @MaxChange), g.Price) / NULLIF(t.Price, 0)))
						-- If already charging above green, use the last price
					ELSE (1 - (alip.Max / NULLIF(t.Price, 0)))
				END
        , 4 ) AS RecommendedDiscount ,	-- D = 1 - (P / L)

	-- Accepted/Recommended Pricing
		-- AcceptedPrice
		-- AcceptedDiscount
		-- AcceptedImpact
		-- AcceptedPriceLevel
        CASE WHEN it.ItemKey IS NOT NULL
			    THEN 'Excpt'
			    WHEN i.KnownValueInd = 'Y' AND 
			        NOT EXISTS (SELECT * 
					            FROM lion.AccountSPG aspg 
								WHERE aspg.AccountKey = f.AccountKey AND 
								        aspg.LLSPGCode IN (i.LLSPGCode, 'ALL'))
				THEN 'Trade'
				WHEN lil.PriceDerivation IN ('DP', 'DC', 'ADJ', 'SPO', 'OCD', 'OCL')
			    THEN 'Last'
			    WHEN ( alip.Max - t.Price ) / NULLIF(t.Price, 0) >= @LowerTradeThreshold
                THEN 'Trade'
                WHEN alip.Max <= r.Price
                THEN 'Red'
                WHEN alip.Max <= a.Price
                THEN 'Amber'
                WHEN alip.Max <= g.Price 
				THEN 'Green'
                ELSE 'Last'
        END AS RecommendPriceLogic,
		((CASE WHEN it.ItemKey is not null and f.UDVarchar1='N' 
				THEN IIF(it.ExceptionFixedPrice is not null, 
							NULL, 
							(1 - ( IIF((t.Price * (1-it.ExceptionDiscount)) + (t.Price * @MaxChange) < g.Price, (t.Price * (1-it.ExceptionDiscount)) + (t.Price * @MaxChange), g.Price) / NULLIF(t.Price, 0)))
						)
				WHEN i.KnownValueInd = 'Y' and 
			            not exists (select * 
							        from lion.AccountSPG aspg 
									where aspg.AccountKey = f.AccountKey and 
										    aspg.LLSPGCode IN (i.LLSPGCode, 'ALL'))
				THEN 0
				WHEN it.ItemKey is not null and f.UDVarchar1='Y' -- Strategic Item
				THEN IIF(it.ExceptionFixedPrice is not null, 
				            it.ExceptionFixedPrice, 
				            IIF((t.Price * (1-it.ExceptionDiscount)) + (t.Price * @MaxSIChange) < g.Price, (t.Price * (1-it.ExceptionDiscount)) + (t.Price * @MaxSIChange), g.Price)
						)
			    WHEN lil.PriceDerivation IN ('DP', 'DC', 'ADJ', 'SPO', 'OCD', 'OCL')
			    THEN alip.Max
				WHEN f.UDVarchar1='Y' AND alip.Max <= g.Price
				    -- Move by 0.5% up to green
				THEN IIF(alip.Max + (t.Price * @MaxSIChange) < g.Price, alip.Max + (t.Price * @MaxSIChange), g.Price)
				WHEN ( alip.Max - t.Price ) / NULLIF(t.Price, 0) >= @LowerTradeThreshold
					-- Apply Trade Price Matching Logic
				THEN (1 - ( IIF(alip.Max + (t.Price * @MaxChange) < t.Price, alip.Max + (t.Price * @MaxChange), t.Price) / NULLIF(t.Price, 0)))
				WHEN alip.Max <= g.Price
					-- Move by 2% up to green
				THEN (1 - ( IIF(alip.Max + (t.Price * @MaxChange) < g.Price, alip.Max + (t.Price * @MaxChange), g.Price) / NULLIF(t.Price, 0)))
					-- If already charging above green, use the last price
				ELSE (1 - (alip.Max / NULLIF(t.Price, 0)))
			END) - i.CurrentCost - (f.LastItemPrice - f.LastItemCost)
		) * f.Total12MonthQuantity AS RecommendImpact ,
        f.UDVarchar1 AS StrategicItem ,
	-- History
		f.LastItemPrice AS LastItemPrice ,
		f.LastItemCost AS LastItemCost,
        alip.Max AS AdjustedLastItemPrice,
        CASE WHEN alip.Max < a.Price THEN 'Red'
                WHEN alip.Max < g.Price THEN 'Amber'
                WHEN alip.Max >= g.Price THEN 'Green'
                ELSE 'Unknown'
        END AS LastPriceLevelGAR ,
        f.LastItemDiscountPercent AS LastItemDiscount ,
		(f.LastItemPrice - f.LastItemCost) / NULLIF(f.LastItemPrice, 0.0) AS LastItemGPP,
        l.GPP AS AdjustedLastItemGPP ,
        f.Total12MonthSales AS TotalSales ,
		f.Total12MonthQuantity * t.Price AS TotalSalesAtTrade,
        f.Total12MonthQuantity AS TotalQuantity,

	-- Price Bands
        r.Price AS RedPrice,
        a.Price AS AmberPrice ,
		g.Price AS GreenPrice_TradeAdj ,
        --t.Price AS TradePrice ,
		i.CurrBranchTradePrice AS TradePrice,
		t.Price AS CalcTradePrice,

		(1 - (r.Price / NULLIF(t.Price, 0))) AS RedDiscount,
		(1 - (a.Price / NULLIF(t.Price, 0))) AS AmberDiscount,
		(1 - (g.Price / NULLIF(t.Price, 0))) AS GreenDiscount,

		pb.StretchGPP AS GreenGPP,
		pb.TargetGPP AS AmberGPP,
		pb.FloorGPP AS RedGPP,
		tgpp.GPP AS TradeGPP,
        g1.DeviatedIndicator AS ContractClaims,
        f.UDVarchar10 AS PriceApproach,
        UDVarchar4 AS SalesSize,
            
		--pba.PricingRuleSequenceNumber AS PBLevel ,
        --pba.PlaybookPricingGroupKey ,

		pb.PricingRuleSequenceNumber AS PBLevel,
        pb.PlaybookPricingGroupKey ,

		lil.PriceDerivation ,
		dd.SQLDate AS LastSaleDate ,
		s.LastChangeDate,
		1 - (NULLIF(alip.[Max],0) / NULLIF(i.CurrBranchTradePrice, 0)) AS AdjustedLPCDiscount
INTO    #Prelim
FROM    dbo.FactLastPriceAndCost f
		INNER JOIN dbo.DimItem di ON di.ItemKey = f.ItemKey
		INNER JOIN #Account a2 ON a2.AccountKey = f.AccountKey
	    INNER JOIN lion.vwFactInvoiceLine lil ON f.LastInvoiceLineUniqueIdentifier = lil.InvoiceLineUniqueIdentifier
		INNER JOIN dbo.DimDay dd ON lil.InvoiceDateDayKey = dd.DayKey 
        INNER JOIN dbo.FactRecommendedPriceBand pb ON f.AccountKey = pb.AccountKey
                                                            AND f.ItemKey = pb.ItemKey
                                                            AND f.InvoiceLineGroup1Key = pb.InvoiceLineGroup1Key
        INNER JOIN lion.vwAccount acct ON f.AccountKey = acct.AccountKey
        INNER JOIN lion.vwItem i ON f.ItemKey = i.ItemKey
		INNER JOIN #SPG spg ON spg.ItemGroup3Key = di.ItemGroup3Key
        INNER JOIN lion.vwFactInvoiceLineGroup1 g1 ON f.InvoiceLineGroup1Key = g1.InvoiceLineGroup1Key
            
		LEFT OUTER JOIN lion.RPBPriceBandAttributes pba ON pb.PlaybookPricingGroupKey = pba.PlaybookPricingGroupKey
		LEFT OUTER JOIN SPGTerms s ON f.AccountKey = s.AccountKey AND i.LLSPGCode = s.SPGCode
		LEFT OUTER JOIN #ItemTerms it ON f.AccountKey = it.AccountKey AND f.ItemKey = it.ItemKey
        LEFT OUTER JOIN #Discounts d ON d.AccountKey = f.AccountKey AND d.ItemGroup3Key = lil.ItemGroup3Key
		--CROSS APPLY lion.[fn_GetRPBPriceBandAttributes(pb.CoveringBandGroupValues, CoveringBandGroupingColumns) pba
			
		CROSS APPLY lion.fn_GetGPP(f.LastItemPrice, f.LastItemCost) l
        CROSS APPLY lion.fn_GetPriceEx(i.CurrentCost, pb.FloorGPP, @TradeFactor, i.CurrBranchTradePrice) r
        CROSS APPLY lion.fn_GetPriceEx(i.CurrentCost, pb.TargetGPP, @TradeFactor, i.CurrBranchTradePrice) a
        CROSS APPLY lion.fn_GetPriceEx(i.CurrentCost, pb.StretchGPP, @TradeFactor, i.CurrBranchTradePrice) g
		CROSS APPLY lion.fn_GetPriceEx(i.CurrentCost, l.GPP, @TradeFactor, i.CurrBranchTradePrice) lip  -- Last item's price at same margin as last item
		CROSS APPLY lion.fn_Max(lip.Price, f.LastItemPrice) alip
        CROSS APPLY lion.fn_GetPrice( i.CurrBranchTradePrice, 0) t
        CROSS APPLY lion.fn_GetGPP(t.Price, i.CurrentCost) tgpp
		--CROSS APPLY lion.fn_GetDiscPrice(t.Price, f.LastItemDiscountPercent) lip  -- Last item's price at same discount w/today's trade price
		CROSS APPLY lion.fn_GetGPP(f.LastItemPrice,i.CurrentCost) lgpp
        --CROSS APPLY lion.fn_GetImpact(r.Price - i.CurrentCost, f.LastItemPrice - f.LastItemCost, f.Total12MonthQuantity) ri
        --CROSS APPLY lion.fn_GetImpact(a.Price - i.CurrentCost, f.LastItemPrice - f.LastItemCost, f.Total12MonthQuantity) ai
        --CROSS APPLY lion.fn_GetImpact(g.Price - i.CurrentCost, f.LastItemPrice - f.LastItemCost, f.Total12MonthQuantity) gi
        --CROSS APPLY lion.fn_GetImpact(t.Price - i.CurrentCost, f.LastItemPrice - f.LastItemCost, f.Total12MonthQuantity) ti
        --CROSS APPLY lion.fn_GetRisk(  t.Price - i.CurrentCost, f.LastItemPrice - f.LastItemCost, f.Total12MonthQuantity) tr
WHERE   f.Total12MonthQuantity > 0
	    AND f.LastItemPrice > 0
		AND i.CurrBranchTradePrice > 0
	    AND lil.PriceDerivation NOT IN ('DP', 'DC', 'ADJ', 'SPO', 'OCD', 'OCL')
 	    AND i.ItemPyramidCode = 1
        AND g1.ShipmentType = 'S'
        AND g1.DeviatedIndicator = 'N'
		AND di.InActive = 0 --LPF-2935


IF @ParamDebug = 'Y'
	SELECT '#Prelim count: ', count(*) from #Prelim


SELECT  p.AccountKey,
		p.LLSPG 
		,
        p.LLSPGCode	,
		SUM(TotalSalesAtTrade * RecommendedDiscount) /  -- Original Discount Amount
			NULLIF(SUM(TotalSalesAtTrade), 0) AS RecommendedDiscount
INTO    #LLSPG
FROM    #Prelim p
WHERE   PriceInstructionTypeLevel = @TBDInstruction AND
	    PriceDerivation NOT IN ('DP', 'DC', 'ADJ', 'SPO', 'OCD', 'OCL') AND
		StrategicItem = 'N' AND
		AdjustedLastItemGPP >= 0 AND
		p.LastSaleDate >= ISNULL(p.LastChangeDate, p.LastSaleDate)
GROUP BY p.AccountKey,p.LLSPG, p.LLSPGCode
ORDER BY 1


IF @ParamDebug = 'Y'
	SELECT '#LLSPG count: ', count(*) from #LLSPG

--PRINT 'step 5: ' + CONVERT(VARCHAR(50), GETDATE(),13)


UPDATE p
    SET p.PriceInstructionTypeLevel = @NonQualException,
	    p.BaseAction = 'Skip'
FROM #Prelim p LEFT OUTER JOIN
	    #ItemTerms it ON p.AccountKey = it.AccountKey AND p.ItemKey = it.ItemKey
WHERE it.ItemKey IS NULL AND
	    StrategicItem = 'N' AND
	    ((PriceInstructionTypeLevel = @TBDInstruction AND
	    PriceDerivation IN ('DP', 'DC', 'ADJ', 'SPO', 'OCD', 'OCL')
		) OR
		p.LastSaleDate < ISNULL(p.LastChangeDate, p.LastSaleDate)
		)

SELECT  l.AccountKey,
		l.LLSPG ,
        l.LLSPGCode ,
        CASE WHEN ROUND(RecommendedDiscount, 1) = 1 THEN RecommendedDiscount ELSE ROUND(RecommendedDiscount / .0025, 0) * .0025 END AS NewDiscount
INTO    #Updates
FROM    #LLSPG l


IF @ParamDebug = 'Y'
	SELECT '#Updates count: ', count(*) from #Updates


UPDATE  p
SET     PriceInstructionTypeLevel = @LLSPGInstruction ,
        RecommendedDiscount = u.NewDiscount ,
        RecommendPrice = TradePrice * ( 1 - u.NewDiscount ) ,
        RecommendImpact = (((TradePrice * ( 1 - u.NewDiscount )) - CurrentCost) - ( LastItemPrice - LastItemCost)) * p.TotalQuantity
FROM    #Prelim p
        INNER JOIN #Updates u ON p.LLSPGCode = u.LLSPGCode AND u.AccountKey = p.AccountKey
WHERE  p.PriceInstructionTypeLevel = @TBDInstruction AND
        (p.StrategicItem = 'N' OR
		NOT EXISTS (SELECT * 
			            FROM #ItemTerms it 
						WHERE it.AccountKey = p.AccountKey AND
							it.ItemKey = p.ItemKey) 
		OR
		(p.StrategicItem = 'Y' AND ABS(p.RecommendedDiscount - u.NewDiscount) <= .01)
		)

UPDATE  pl
SET     PriceInstructionTypeLevel = @LLSPGInstruction ,
        RecommendedDiscount = u.NewDiscount
FROM    #PrelimLLSPG pl
INNER JOIN #Updates u 
	ON pl.LLSPGCode = u.LLSPGCode 
	AND u.AccountKey = pl.AccountKey

--   select '#PrelimLLSPG', * from #PrelimLLSPG

UPDATE  p
SET     PriceInstructionTypeLevel = @ItemInstruction ,
        RecommendedDiscount = p.LastItemDiscount ,
        RecommendPrice = TradePrice * ( 1 - p.LastItemDiscount ) ,
        RecommendImpact = (((TradePrice * ( 1 - p.LastItemDiscount )) - CurrentCost) - ( LastItemPrice - LastItemCost)) * p.TotalQuantity,
		RecommendPriceLogic = 'Last',
		BaseAction = 'Skip'
FROM    #Prelim p
WHERE  p.PriceInstructionTypeLevel = @TBDInstruction AND 
		NOT EXISTS (SELECT * 
			            FROM #ItemTerms it 
					WHERE it.AccountKey = p.AccountKey AND
							it.ItemKey = p.ItemKey) AND
		NOT EXISTS (SELECT * 
			            FROM #LLSPGTerms lt 
					WHERE lt.AccountKey = p.AccountKey AND
							lt.SPGCode = p.LLSPGCode) AND
		NOT EXISTS (SELECT * 
			            FROM #PrelimLLSPG pl 
					WHERE pl.AccountKey = p.AccountKey AND
							pl.LLSPGCode = p.LLSPGCode AND
							pl.SPGDisc1 IS NOT NULL)

UPDATE  p /* pasted from LPF-1415 */
    SET  
		BaseAction = 'Rollup',
        PriceInstructionTypeLevel = @LLSPGInstruction ,
        RecommendedDiscount = u.NewDiscount ,
        RecommendPrice = TradePrice * ( 1 - u.NewDiscount ) ,
        RecommendImpact = (((TradePrice * ( 1 - u.NewDiscount )) - CurrentCost) - ( LastItemPrice - LastItemCost)) * p.TotalQuantity
    FROM  #Prelim p
        INNER JOIN #Updates u ON p.AccountKey = u.AccountKey AND p.LLSPGCode = u.LLSPGCode
    WHERE  p.PriceInstructionTypeLevel = @TBDInstruction AND
        (p.StrategicItem = 'N' OR
		NOT EXISTS (SELECT * 
			            FROM #ItemTerms it 
						WHERE it.AccountKey = p.AccountKey AND
							it.ItemKey = p.ItemKey) 
		OR
		(p.StrategicItem = 'Y' AND ABS(p.RecommendedDiscount - u.NewDiscount) <= .01)
		)


UPDATE  #Prelim
SET     PriceInstructionTypeLevel = @ItemInstruction
WHERE   PriceInstructionTypeLevel = @TBDInstruction

UPDATE pl
	SET pl.KVI = k.MaxKVI
	FROM #PrelimLLSPG pl INNER JOIN 
		( SELECT p.AccountKey, p.LLSPGCode, p.PyramidCode, MAX(i.KnownValueInd) AS MaxKVI
			FROM #PrelimLLSPG p
				INNER JOIN lion.vwItem i ON p.LLSPGCode = i.LLSPGCode
				AND p.PyramidCode = i.ItemPyramidCode
		GROUP BY p.AccountKey, p.LLSPGCode, p.PyramidCode ) k 
	ON pl.LLSPGCode = k.LLSPGCode 
	AND pl.PyramidCode = k.PyramidCode
	AND k.AccountKey = pl.AccountKey


--PRINT 'step 6: ' + CONVERT(VARCHAR(50), GETDATE(),13)




--SELECT '#DefaultTerms', * FROM #DefaultTerms
--SELECT '#SPG' AS label, * FROM #SPG
--SELECT '#ItemTerms' AS label, * FROM #ItemTerms
--SELECT '#PrelimSoldLLSPG', * FROM #PrelimSoldLLSPG
--select '#PrelimLLSPG', * from #PrelimLLSPG
--SELECT '#Prelim', *  from #Prelim
--SELECT '#LLSPG', * FROM #LLSPG
--SELECT '#Updates', * FROM #Updates
--SELECT '#Discounts', * FROM #Discounts

--SELECT '#LLSPGDefaultTerms', * FROM #LLSPGDefaultTerms

----------------------------------------------------------------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------------------------------------------------------------
SELECT  
		'Detail' AS RowType ,
        COALESCE(p.AccountKey, c.AccountKey) AS AccountKey,
        COALESCE(p.ItemKey, c.ItemKey) AS ItemKey,
        InvoiceLineGroup1Key ,
        a.AccountNumber AS AccountNumber ,
        a.AccountName AS AccountName,
        a.Area AS Area ,
        i.LLSPGCode AS LLSPG ,
        i.LLSPGDescription AS LLSPGDescription ,
        SUBSTRING(i.ItemNumber, 2, 100) AS ProductCode ,
        i.ItemDescription AS ProductDescription ,
		p.CurrentCost ,
        i.KnownValueInd AS KVI ,
        NULL AS NDPFlag ,
        d.Discount AS DefaultDiscount ,
        a.BranchPrimaryBrand  ,
        c.UsualDiscount1 AS UsualDiscount1 ,
        c.UsualDiscount2 AS UsualDiscount2 ,
        c.ExceptionDiscount AS ExceptionDiscount ,
        1 - (1 - ISNULL(c.UsualDiscount1, 0)) * (1 - ISNULL(c.UsualDiscount2, 0)) AS Disc ,
        RecommendedDiscount AS RecommendedDiscount ,
        COALESCE(PriceInstructionTypeLevel, @UnusedInstruction) AS PriceInstructionTypeLevel  ,
		@DefaultAction AS Action,
		COALESCE(p.BaseAction, IIF(ISNULL(c.LastChangeDate, '1-JAN-2015') <= DATEADD(dd, -184, CAST(GETDATE() AS DATE)), 'Delete', 'Skip')) AS BaseAction ,
        c.LastChangeDate AS LastChangeDate ,
        c.ExceptionFixedPrice AS FixedPrice ,
        TotalSales AS ItemSales ,
        SUM(TotalSales) OVER ( PARTITION BY LLSPG, p.AccountKey ) AS LLSPGSales ,
        SUM(TotalSales) OVER ( PARTITION BY p.AccountKey ) AS TotalAcctSales ,
        RecommendPrice ,
        RecommendPriceLogic ,
        RecommendImpact ,
        StrategicItem ,
        LastItemPrice ,
		LastItemCost,
        AdjustedLastItemPrice,
        LastPriceLevelGAR ,
        LastItemDiscount,
        LastItemGPP ,
		AdjustedLastItemGPP ,
        TotalQuantity ,
        GreenPrice_TradeAdj ,

	    RedDiscount,
        AmberDiscount,
        GreenDiscount,

        AmberPrice,
        RedPrice ,
        TradePrice ,
		GreenGPP,
		AmberGPP,
		RedGPP,
		TradeGPP,
        ContractClaims,
        SalesSize,
        PriceApproach,
        PBLevel ,
        CustomerTermsKey,
        a.AccountNumber AS AccountID,
        PyramidCode ,
        i.LLSPGCode AS SPGCode,
        OwningBrandCode ,
        DeleteFlag ,
        LastChangeInitials ,
        LastChangeTime ,
        LastChangePLID ,
        LastChangeBranch ,
        ExceptionProduct ,
        ExceptionFixedPrice ,
        ExceptionFromDate ,
        ExceptionToDate ,
        ExceptionPCF ,
        PlaybookPricingGroupKey ,
		PriceDerivation ,
		LastSaleDate ,
		--AdjustedLastItemPrice AS AdjustedLastItemPrice ,
		CalcTradePrice AS CalcTradePrice ,
		GreenPrice_TradeAdj AS GreenPrice ,
		a.AccountManagerFullName AS AccountOwner,
		a.Branch,
		a.FixedPriceIndicator,
		ISNULL(1.0 - (i.CurrentCost / NULLIF(RecommendPrice, 0) ), 0) AS RecommendGPP,
		p.AdjustedLPCDiscount,
		COALESCE(p.ItemGroup3Key,c.ItemGroup3Key) AS ItemGroup3Key,
		i.ProdBrandAtLLSPG,
		ISNULL(dp.FullName,a.AccountManagerFullName) AS AccountBrandOwnerFullname,
		a.AccountOwnersBrand,
		TotalSalesAtTrade,
		CAST('' AS NVARCHAR(50)) AS CriterionDescription,
		RecommendedDiscount AS InitialRecommendedDiscount,
		CAST(N'Change' AS NVARCHAR(50)) AS ActionType

INTO    #AllResults

FROM    #Prelim p
        FULL OUTER JOIN #ItemTerms c ON c.AccountKey = p.AccountKey AND c.ItemKey = p.ItemKey
        INNER JOIN lion.vwAccount a ON COALESCE(p.AccountKey, c.AccountKey) = a.AccountKey
        INNER JOIN lion.vwItem i ON COALESCE(p.ItemKey, c.ItemKey) = i.ItemKey

		/* pound prod except def trms is the old way*/
		--	LEFT OUTER JOIN #ProductExceptionDefaultTerms d ON i.ProductCode = d.Product
		--                                                  AND i.ItemPyramidCode = 1

		LEFT OUTER JOIN #DefaultTerms d ON d.SPGCode = i.LLSPGCode
				AND d.ItemKey = i.ItemKey
				AND d.ContractName = a.ContractName
                AND i.ItemPyramidCode = 1

		LEFT JOIN dbo.DimBrand db ON db.Brand = i.ProdBrandAtLLSPG
		LEFT JOIN DimAccountBrandManager dabm ON dabm.BrandKey = db.BrandKey AND dabm.AccountKey = a.AccountKey
		LEFT JOIN dbo.DimAccountManager dam ON dam.AccountManagerKey = dabm.AccountManagerKey
		LEFT JOIN dbo.DimPerson dp ON dp.DimPersonKey = dam.DimPersonKey
		--INNER JOIN dbo.DimBrand db ON db.Brand = i.ProdBrandAtLLSPG 
		--INNER JOIN DimAccountBrandManager dabm ON dabm.BrandKey = db.BrandKey AND dabm.AccountKey = a.AccountKey
		--INNER JOIN dbo.DimAccountManager dam ON dam.AccountManagerKey = dabm.AccountManagerKey
		--INNER JOIN dbo.DimPerson dp ON dp.DimPersonKey = dam.DimPersonKey
UNION
SELECT  DISTINCT
        'LLSPG' AS RowType ,
        pl.AccountKey ,
        NULL AS ItemKey ,
        NULL AS InvoiceLineGroup1Key ,
        AccountNumber ,
        AccountName ,
        Area ,
        pl.LLSPGCode AS LLSPG ,
        LLSPGDescription AS LLSPGDescription ,
        NULL AS ProductCode ,
        NULL AS ProductDescription ,
		NULL AS CurrentCost ,
        KVI ,
        CASE WHEN k.LLSPGCode IS NOT NULL THEN 'Y' ELSE NULL END AS NDPFlag ,
        d.Discount AS DefaultDiscount ,
        a.BranchPrimaryBrand ,
        SPGDisc1 ,
        SPGDisc2 ,
        NULL AS ExceptionDiscount ,
        Disc ,
        RecommendedDiscount AS RecommendedDiscount ,
        PriceInstructionTypeLevel ,
		@DefaultAction AS Action,
		BaseAction,
        LastChangeDate AS LastChangeDate ,
        NULL AS FixedPrice ,
        NULL AS ItemSales ,
        NULL AS LLSPGSales ,
        NULL AS TotalSales ,
        NULL AS RecommendPrice ,
        NULL AS RecommendPriceLogic ,
        NULL AS RecommendImpact ,
        NULL AS StrategicItem ,
        NULL AS LastItemPrice ,
        NULL AS LastItemCost ,
		NULL AS AdjustedLastItemPrice ,
        NULL AS LastPriceLevel_GAR ,
        NULL AS LastItemDiscount ,
        NULL AS LastItemGPP ,
        NULL AS AdjustedLastItemGPP ,
        NULL AS TotalQuantity ,
        NULL AS GreenPrice_TradeAdj ,

	    RAGdisc.RedDiscount,
        RAGdisc.AmberDiscount,
        RAGdisc.GreenDiscount,

        NULL AS AmberPrice ,
        NULL AS RedPrice ,
        NULL AS TradePrice ,
		NULL AS GreenGPP ,
		NULL AS AmberGPP ,
		NULL AS RedGPP ,
		NULL AS TradeGPP ,
        NULL AS ContractClaims ,
        NULL AS SalesSize ,
        NULL AS PriceApproach ,
        NULL AS PBLevel ,
        NULL AS CustomerTermsKey ,
        NULL AS AccountID ,
        NULL AS PyramidCode ,
        NULL AS SPGCode ,
        pl.OwningBrandCode ,
        NULL AS DeleteFlag ,
        LastChangeInitials ,
        NULL AS LastChangetime ,
        NULL AS LastChangePLID ,
        LastChangeBranch ,
        NULL AS ExceptionProduct ,
        NULL AS ExceptionFixedprice ,
        NULL AS ExceptionFromdate ,
        NULL AS ExceptionToDate ,
        NULL AS ExceptionPCF ,
        NULL AS PlaybookPricingGroupKey ,
		NULL AS PriceDerivation ,
		NULL AS LastSaleDate ,
		--NULL AS AdjustedLastItemPrice ,
		NULL AS CalcTradePrice ,
		NULL AS GreenPrice,
		a.AccountManagerFullName AS AccountOwner,
		a.Branch,
		a.FixedPriceIndicator,
		NULL AS RecommendGPP,
		NULL AS AdjustedLPCDiscount,
		pl.ItemGroup3Key,
		pb.ProdBrandAtLLSPG,
		ISNULL(dp.FullName,a.AccountManagerFullName) AS AccountBrandOwnerFullname,
		a.AccountOwnersBrand,
		NULL AS TotalSalesAtTrade,
		CAST('' AS NVARCHAR(50)) AS CriterionDescription,
		RecommendedDiscount AS InitialRecommendedDiscount,
		CAST(N'Change' AS NVARCHAR(50)) AS ActionType

FROM    #PrelimLLSPG pl
        INNER JOIN lion.vwAccount a 
			ON pl.AccountKey = a.AccountKey

		LEFT OUTER JOIN #Discounts RAGdisc 
			ON RAGdisc.AccountKey = pl.AccountKey 
			AND RAGdisc.ItemGroup3Key = pl.ItemGroup3Key

		--LEFT OUTER JOIN #LLSPGDefaultTerms d 
		--	ON pl.LLSPGCode = d.SPGCode

		-- pount def terms is the new way
		LEFT OUTER JOIN #DefaultTerms d ON d.SPGCode = pl.LLSPGCode
				AND d.ItemKey = 1
				AND d.ContractName = a.ContractName

		LEFT OUTER JOIN lion.AccountSPG k ON k.AccountKey = pl.AccountKey AND (k.LLSPGCode = pl.LLSPGCode OR k.LLSPGCode = 'ALL')
		LEFT JOIN (SELECT di.ItemGroup3Key, MAX(di.ItemUDVarchar2 ) ProdBrandAtLLSPG FROM dbo.DimItem di GROUP BY di.ItemGroup3Key) pb ON pb.ItemGroup3Key = pl.ItemGroup3Key

		LEFT JOIN dbo.DimBrand db ON db.Brand = pb.ProdBrandAtLLSPG
		LEFT JOIN DimAccountBrandManager dabm ON dabm.BrandKey = db.BrandKey AND dabm.AccountKey = a.AccountKey
		LEFT JOIN dbo.DimAccountManager dam ON dam.AccountManagerKey = dabm.AccountManagerKey
		LEFT JOIN dbo.DimPerson dp ON dp.DimPersonKey = dam.DimPersonKey
		--INNER JOIN dbo.DimBrand db ON db.Brand = pb.ProdBrandAtLLSPG
		--INNER JOIN DimAccountBrandManager dabm ON dabm.BrandKey = db.BrandKey AND dabm.AccountKey = a.AccountKey
		--INNER JOIN dbo.DimAccountManager dam ON dam.AccountManagerKey = dabm.AccountManagerKey
		--INNER JOIN dbo.DimPerson dp ON dp.DimPersonKey = dam.DimPersonKey


IF @ParamDebug = 'Y'
BEGIN 
	SELECT '#AllResults count: ', COUNT(*) from #AllResults
	SELECT '#AllResults' AS Label, * FROM #AllResults
END


/*** LPF-3296 Provide "CCOR" like capabilities within Create Recommendations ***/
--DELETE FROM #AllResults WHERE ProdBrandAtLLSPG NOT IN ('', 'PLUMB', 'PARTS','DRAIN','PIPE','CLIMT')

--select '#AllResults count#2 count: ', count(*) from #AllResults

--PRINT 'step 7: ' + CONVERT(VARCHAR(50), GETDATE(),13)


UPDATE a
	SET a.PriceInstructionTypeLevel = @ItemInstruction,
	    a.BaseAction = 'Accept' ,
		a.RecommendPrice =  CASE WHEN ( a.AdjustedLastItemPrice - a.TradePrice ) / NULLIF(a.TradePrice, 0) >= @LowerTradeThreshold
										-- Apply Trade Price Matching Logic
									THEN IIF(a.AdjustedLastItemPrice + (a.TradePrice * @MaxChange) < a.TradePrice, a.AdjustedLastItemPrice + (a.TradePrice * @MaxChange), a.TradePrice)
									WHEN a.AdjustedLastItemPrice <= a.GreenPrice
										-- Move by 2% up to green
									THEN IIF(a.AdjustedLastItemPrice + (a.TradePrice * @MaxChange) < a.GreenPrice, a.AdjustedLastItemPrice + (a.TradePrice * @MaxChange), a.GreenPrice)
										-- If already charging above green, use the last price
									ELSE a.AdjustedLastItemPrice
							END,
		a.RecommendedDiscount = CASE WHEN ( a.AdjustedLastItemPrice - a.TradePrice ) / NULLIF(a.TradePrice, 0) >= @LowerTradeThreshold
										-- Apply Trade Price Matching Logic
									THEN (IIF(a.AdjustedLastItemPrice + (a.TradePrice * @MaxChange) < a.TradePrice, a.AdjustedLastItemPrice + (a.TradePrice * @MaxChange), a.TradePrice) / NULLIF(a.TradePrice, 0)) - 1
									WHEN a.AdjustedLastItemPrice <= a.GreenPrice
										-- Move by 2% up to green
									THEN (IIF(a.AdjustedLastItemPrice + (a.TradePrice * @MaxChange) < a.GreenPrice, a.AdjustedLastItemPrice + (a.TradePrice * @MaxChange), a.GreenPrice) / NULLIF(a.TradePrice, 0)) - 1
										-- If already charging above green, use the last price
									ELSE (a.AdjustedLastItemPrice / NULLIF(a.TradePrice, 0)) - 1
								END

	FROM #AllResults a LEFT OUTER JOIN
	    #ItemTerms it ON a.AccountKey = it.AccountKey AND a.ItemKey = it.ItemKey
WHERE it.ItemKey IS NULL AND
	    (((a.UsualDiscount1 = 0 OR a.PriceInstructionTypeLevel = @NonQualException) AND
	 	a.ExceptionDiscount IS NOT NULL AND
		a.BaseAction <> 'Delete'
		) OR 
		(a.ExceptionDiscount IS NOT NULL AND 
		a.PriceInstructionTypeLevel = @LLSPGInstruction AND
        a.BaseAction = 'Skip'
		)
		)




UPDATE #AllResults
	SET InitialRecommendedDiscount = RecommendedDiscount


;
WITH a AS (
	SELECT 
		AccountKey,
		LLSPG,
		SUM(ItemSales) AS LLSPGSales,
		MAX(LastSaleDate) AS LastSaleDate
	FROM
		#AllResults
	WHERE 
		LLSPGSales IS NOT NULL
		AND RowType = 'Detail'
	GROUP BY
		AccountKey,
		LLSPG	
)
UPDATE ar
SET
	ar.LLSPGSales = a.LLSPGSales,
	ar.LastSaleDate = ISNULL(ar.LastSaleDate, a.LastSaleDate)
FROM #AllResults ar
INNER JOIN a
	ON a.AccountKey = ar.AccountKey
	AND a.LLSPG = ar.LLSPG


/*
select '#AllResults', *
from #AllResults
*/




DECLARE
	@SequenceNumber INTEGER,
	@StrategicIndicatorSelection VARCHAR(1),
	@CriterionDate DATETIME,
	@CriterionPercent Percent_Type,
	@CriterionMonth SMALLINT,
	@CriterionAmount DECIMAL(19,8),
	@ActionPercent Percent_Type,
	@CapPercent Percent_Type,
	@RecommendCriterionKey Key_Normal_type,
	@CriterionDescription Description_Small_type,
	@InputType Description_Small_type,
	@ActionType Description_Small_type,
	@CapType Description_Small_type


DECLARE RecommendCriteriaCursor 
CURSOR 
FAST_FORWARD 
FOR
	SELECT 
		rc.SequenceNumber,
		rc.StrategicIndicatorSelection,
		rc.CriterionDate,
		rc.CriterionPercent,
		rc.CriterionMonth,
		rc.CriterionAmount,
		rc.ActionPercent,
		rc.CapPercent,
		Criterion.RecommendCriterionKey,
		Criterion.Description AS CriterionDescription,
		Criterion.InputType,
		at.ActionType,
		ct.CapType
	FROM 
		ppe.RecommendCriteria rc
	INNER JOIN ppe.RecommendCriterion Criterion
		ON Criterion.RecommendCriterionKey = rc.RecommendCriterionKey
	INNER JOIN ppe.RecommendCriteriaActionType at
		ON at.RecommendCriteriaActionTypeKey = rc.RecommendCriteriaActionTypeKey
	INNER JOIN ppe.RecommendCriteriaCapType ct
		ON ct.RecommendCriteriaCapTypeKey = rc.RecommendCriteriaCapTypeKey
	WHERE
		rc.RecommendKey = @RecommendKey
	ORDER BY
		rc.SequenceNumber

OPEN RecommendCriteriaCursor

FETCH NEXT FROM RecommendCriteriaCursor 
INTO 
	@SequenceNumber,
	@StrategicIndicatorSelection,
	@CriterionDate,
	@CriterionPercent,
	@CriterionMonth,
	@CriterionAmount,
	@ActionPercent,
	@CapPercent,
	@RecommendCriterionKey,
	@CriterionDescription,
	@InputType,
	@ActionType,
	@CapType


WHILE (@@FETCH_STATUS = 0)
BEGIN

	IF @ParamDebug = 'Y'
	BEGIN 
		SELECT 'SequenceNumber              ', @SequenceNumber
		SELECT 'StrategicIndicatorSelection ', @StrategicIndicatorSelection
		SELECT 'CriterionDate				', @CriterionDate
		SELECT 'CriterionPercent			', @CriterionPercent
		SELECT 'CriterionMonth				', @CriterionMonth
		SELECT 'CriterionAmount				', @CriterionAmount
		SELECT 'ActionPercent				', @ActionPercent
		SELECT 'CapPercent					', @CapPercent
		SELECT 'RecommendCriterionKey		', @RecommendCriterionKey
		SELECT 'CriterionDescription		', @CriterionDescription
		SELECT 'InputType					', @InputType
		SELECT 'ActionType					', @ActionType
		SELECT 'CapType						', @CapType
		SELECT '----------------------------', @SequenceNumber
	END
	

	IF (@CriterionDescription = 'Terms Last Changed Before' OR @RecommendCriterionKey = 5)
	BEGIN

		UPDATE a
		SET
			CriterionDescription = @CriterionDescription,
			a.Action = 
				CASE 
					WHEN @ActionType = 'Delete' THEN 'Delete'
					WHEN a.BaseAction <> @DefaultAction THEN a.BaseAction  -- if not pending, then a special skip or delete rule has been applied
					ELSE 'Accept'
				END,
			a.RecommendedDiscount = nd.RecommendedDiscount,
			RecommendPrice = TradePrice * ( 1 - nd.RecommendedDiscount ) ,
			RecommendImpact = (((TradePrice * ( 1 - nd.RecommendedDiscount )) - CurrentCost) - ( LastItemPrice - LastItemCost)) * TotalQuantity,
			RecommendGPP = 1.0 - (CurrentCost / NULLIF((TradePrice * ( 1 - nd.RecommendedDiscount )), 0) ) ,
			ActionType = @ActionType
		FROM #AllResults a
		OUTER APPLY ppe.fn_GetNewDiscount (
			@ActionType,
			@ActionPercent,
			@CapType,
			@CapPercent,
			--a.RecommendedDiscount,
			a.Disc, 		-- https://enterbridge.atlassian.net/browse/LPF-5270 
			a.RedDiscount,
			a.AmberDiscount,
			a.GreenDiscount) nd
		WHERE
			a.Action = 'Pending'
			AND (a.StrategicItem = @StrategicIndicatorSelection OR @StrategicIndicatorSelection = 'B' OR a.StrategicItem IS NULL)
			AND a.LastChangeDate <= @CriterionDate
			AND (RowType = 'LLSPG'
				OR
				(RowType = 'Detail' AND (ExceptionFixedPrice IS NOT NULL OR ExceptionDiscount IS NOT NULL)	
				)
			)

	END

	IF (@CriterionDescription = 'Below Red Term' OR @RecommendCriterionKey = 1)
	BEGIN

		UPDATE a
		SET
			CriterionDescription = @CriterionDescription,
			a.Action = 
				CASE 
					WHEN @ActionType = 'Delete' THEN 'Delete'
					WHEN a.BaseAction <> @DefaultAction THEN a.BaseAction  -- if not pending, then a special skip or delete rule has been applied
					ELSE 'Accept'
				END,
			RecommendedDiscount = nd.RecommendedDiscount,
			RecommendPrice = TradePrice * ( 1 - nd.RecommendedDiscount ) ,
			RecommendImpact = (((TradePrice * ( 1 - nd.RecommendedDiscount )) - CurrentCost) - ( LastItemPrice - LastItemCost)) * TotalQuantity,
			RecommendGPP = 1.0 - (CurrentCost / NULLIF(TradePrice * ( 1 - nd.RecommendedDiscount ), 0) ),
			ActionType = @ActionType		
		FROM #AllResults a
		OUTER APPLY ppe.fn_GetNewDiscount (
			@ActionType,
			@ActionPercent,
			@CapType,
			@CapPercent,
			a.RecommendedDiscount,
			a.RedDiscount,
			a.AmberDiscount,
			a.GreenDiscount) nd
		WHERE
			a.Action = 'Pending'
			AND (a.StrategicItem = @StrategicIndicatorSelection OR @StrategicIndicatorSelection = 'B')
			AND a.RecommendedDiscount > a.RedDiscount
			AND a.RecommendedDiscount <> 0.

	END

	IF (@CriterionDescription = 'Red-Amber Term' OR @RecommendCriterionKey = 2)
	BEGIN

		UPDATE a
		SET
			CriterionDescription = @CriterionDescription,
			a.Action = 
				CASE 
					WHEN @ActionType = 'Delete' THEN 'Delete'
					WHEN a.BaseAction <> @DefaultAction THEN a.BaseAction  -- if not pending, then a special skip or delete rule has been applied
					ELSE 'Accept'
				END,
			RecommendedDiscount = nd.RecommendedDiscount,
			RecommendPrice = TradePrice * ( 1 - nd.RecommendedDiscount ) ,
			RecommendImpact = (((TradePrice * ( 1 - nd.RecommendedDiscount )) - CurrentCost) - ( LastItemPrice - LastItemCost)) * TotalQuantity,
			RecommendGPP = 1.0 - (CurrentCost / NULLIF(TradePrice * ( 1 - nd.RecommendedDiscount ), 0) ),
			ActionType = @ActionType
		FROM #AllResults a
		OUTER APPLY ppe.fn_GetNewDiscount (
			@ActionType,
			@ActionPercent,
			@CapType,
			@CapPercent,
			a.RecommendedDiscount,
			a.RedDiscount,
			a.AmberDiscount,
			a.GreenDiscount) nd
		WHERE
			a.Action = 'Pending'
			AND (a.StrategicItem = @StrategicIndicatorSelection OR @StrategicIndicatorSelection = 'B')
			AND a.RecommendedDiscount BETWEEN a.AmberDiscount AND a.RedDiscount
			AND a.RecommendedDiscount <> 0.

	END

	IF (@CriterionDescription = 'Amber-Green Term' OR @RecommendCriterionKey = 3)
	BEGIN

		UPDATE a
		SET
			CriterionDescription = @CriterionDescription,
			a.Action = 
				CASE 
					WHEN @ActionType = 'Delete' THEN 'Delete'
					WHEN a.BaseAction <> @DefaultAction THEN a.BaseAction  -- if not pending, then a special skip or delete rule has been applied
					ELSE 'Accept'
				END,
			RecommendedDiscount = nd.RecommendedDiscount,
			RecommendPrice = TradePrice * ( 1 - nd.RecommendedDiscount ) ,
			RecommendImpact = (((TradePrice * ( 1 - nd.RecommendedDiscount )) - CurrentCost) - ( LastItemPrice - LastItemCost)) * TotalQuantity,
			RecommendGPP = 1.0 - (CurrentCost / NULLIF(TradePrice * ( 1 - nd.RecommendedDiscount ), 0) ),
			ActionType = @ActionType
		FROM #AllResults a
		OUTER APPLY ppe.fn_GetNewDiscount (
			@ActionType,
			@ActionPercent,
			@CapType,
			@CapPercent,
			a.RecommendedDiscount,
			a.RedDiscount,
			a.AmberDiscount,
			a.GreenDiscount) nd
		WHERE
			a.Action = 'Pending'
			AND (a.StrategicItem = @StrategicIndicatorSelection OR @StrategicIndicatorSelection = 'B')
			AND a.RecommendedDiscount BETWEEN a.GreenDiscount AND a.AmberDiscount

	END

	IF (@CriterionDescription = 'Above Green Term' OR @RecommendCriterionKey = 4)
	BEGIN

		UPDATE a
		SET
			CriterionDescription = @CriterionDescription,
			a.Action = 
				CASE 
					WHEN @ActionType = 'Delete' THEN 'Delete'
					WHEN a.BaseAction <> @DefaultAction THEN a.BaseAction  -- if not pending, then a special skip or delete rule has been applied
					ELSE 'Accept'
				END,
			RecommendedDiscount = nd.RecommendedDiscount,
			RecommendPrice = TradePrice * ( 1 - nd.RecommendedDiscount ) ,
			RecommendImpact = (((TradePrice * ( 1 - nd.RecommendedDiscount )) - CurrentCost) - ( LastItemPrice - LastItemCost)) * TotalQuantity,
			RecommendGPP = 1.0 - (CurrentCost / NULLIF(TradePrice * ( 1 - nd.RecommendedDiscount ), 0) ),
			ActionType = @ActionType
		FROM #AllResults a
		OUTER APPLY ppe.fn_GetNewDiscount (
			@ActionType,
			@ActionPercent,
			@CapType,
			@CapPercent,
			a.RecommendedDiscount,
			a.RedDiscount,
			a.AmberDiscount,
			a.GreenDiscount) nd
		WHERE
			a.Action = 'Pending'
			AND (a.StrategicItem = @StrategicIndicatorSelection OR @StrategicIndicatorSelection = 'B')
			AND a.RecommendedDiscount < a.GreenDiscount
	END
    
	IF (@CriterionDescription = 'Terms With GP Less Than X' OR @RecommendCriterionKey = 6)
	BEGIN

		UPDATE a
		SET
			CriterionDescription = @CriterionDescription,
			a.Action = 
				CASE 
					WHEN @ActionType = 'Delete' THEN 'Delete'
					WHEN a.BaseAction <> @DefaultAction THEN a.BaseAction  -- if not pending, then a special skip or delete rule has been applied
					ELSE 'Accept'
				END,
			RecommendedDiscount = nd.RecommendedDiscount,
			RecommendPrice = TradePrice * ( 1 - nd.RecommendedDiscount ) ,
			RecommendImpact = (((TradePrice * ( 1 - nd.RecommendedDiscount )) - CurrentCost) - ( LastItemPrice - LastItemCost)) * TotalQuantity,
			RecommendGPP = 1.0 - (CurrentCost / NULLIF(TradePrice * ( 1 - nd.RecommendedDiscount ), 0) ),
			ActionType = @ActionType
		FROM #AllResults a
		OUTER APPLY ppe.fn_GetNewDiscount (
			@ActionType,
			@ActionPercent,
			@CapType,
			@CapPercent,
			a.RecommendedDiscount,
			a.RedDiscount,
			a.AmberDiscount,
			a.GreenDiscount) nd
		WHERE
			a.Action = 'Pending'
			AND (a.StrategicItem = @StrategicIndicatorSelection OR @StrategicIndicatorSelection = 'B')
			AND a.RecommendGPP < (@CriterionPercent)

			AND RowType = 'Detail'					-- https://enterbridge.atlassian.net/browse/LPF-5271 
			AND (ExceptionFixedPrice IS NOT NULL		-- https://enterbridge.atlassian.net/browse/LPF-5271
             OR ExceptionDiscount IS NOT NULL)		-- https://enterbridge.atlassian.net/browse/LPF-5271

	END

	IF (@CriterionDescription = '0% Terms' OR @RecommendCriterionKey = 7)
	BEGIN

		UPDATE a
		SET
			CriterionDescription = @CriterionDescription,
			a.Action = 
				CASE 
					WHEN @ActionType = 'Delete' THEN 'Delete'
					WHEN a.BaseAction <> @DefaultAction THEN a.BaseAction  -- if not pending, then a special skip or delete rule has been applied
					ELSE 'Accept'
				END,
			RecommendedDiscount = nd.RecommendedDiscount,
			RecommendPrice = TradePrice * ( 1 - nd.RecommendedDiscount ) ,
			RecommendImpact = (((TradePrice * ( 1 - nd.RecommendedDiscount )) - CurrentCost) - ( LastItemPrice - LastItemCost)) * TotalQuantity,
			RecommendGPP = 1.0 - (CurrentCost / NULLIF(TradePrice * ( 1 - nd.RecommendedDiscount ), 0) ),
			ActionType = @ActionType
		FROM #AllResults a
		OUTER APPLY ppe.fn_GetNewDiscount (
			@ActionType,
			@ActionPercent,
			@CapType,
			@CapPercent,
			a.RecommendedDiscount,
			a.RedDiscount,
			a.AmberDiscount,
			a.GreenDiscount) nd
		WHERE
			a.Action = 'Pending'
			AND (a.StrategicItem = @StrategicIndicatorSelection OR @StrategicIndicatorSelection = 'B' OR a.StrategicItem IS NULL)
			--AND a.RecommendGPP < 0.0
			
			/* LPF-5272 PF-618 Criteria | 0% Terms Not Working as Expected - could probably just filter out Disc <> 0 */
			AND (
				RowType = 'LLSPG' AND Disc IS NOT NULL
				OR
				RowType = 'Detail' AND UsualDiscount1 IS NOT NULL
			)
			AND a.RecommendedDiscount = 0.0

	END

	IF (@CriterionDescription = 'Sales Less Than X Pounds in Last X Months' OR @RecommendCriterionKey = 8)
	BEGIN

		UPDATE a
		SET
			CriterionDescription = @CriterionDescription,
			a.Action = 
				CASE 
					WHEN @ActionType = 'Delete' THEN 'Delete'
					WHEN a.BaseAction <> @DefaultAction THEN a.BaseAction  -- if not pending, then a special skip or delete rule has been applied
					ELSE 'Accept'
				END,
			RecommendedDiscount = nd.RecommendedDiscount,
			RecommendPrice = TradePrice * ( 1 - nd.RecommendedDiscount ) ,
			RecommendImpact = (((TradePrice * ( 1 - nd.RecommendedDiscount )) - CurrentCost) - ( LastItemPrice - LastItemCost)) * TotalQuantity,
			RecommendGPP = 1.0 - (CurrentCost / NULLIF((TradePrice * ( 1 - nd.RecommendedDiscount )), 0) ),
			ActionType = @ActionType
		FROM #AllResults a
		OUTER APPLY ppe.fn_GetNewDiscount (
			@ActionType,
			@ActionPercent,
			@CapType,
			@CapPercent,
			a.RecommendedDiscount,
			a.RedDiscount,
			a.AmberDiscount,
			a.GreenDiscount) nd
		WHERE
			a.Action = 'Pending'
			--AND (a.StrategicItem = @StrategicIndicatorSelection OR @StrategicIndicatorSelection = 'B')	-- https://enterbridge.atlassian.net/browse/LPF-5273
			
			--AND a.ItemSales < @CriterionAmount													-- https://enterbridge.atlassian.net/browse/LPF-5273
			AND LLSPGSales < @CriterionAmount														-- https://enterbridge.atlassian.net/browse/LPF-5273
			AND RowType = 'LLSPG'																	-- https://enterbridge.atlassian.net/browse/LPF-5273

			AND DATEDIFF(MONTH, a.LastSaleDate, GETDATE()) <= @CriterionMonth
			--AND DATEDIFF(MONTH, a.LastChangeDate, GETDATE()) <= @CriterionMonth
			
	END

	IF (@CriterionDescription = 'Fixed Prices Expiring' OR @RecommendCriterionKey = 9)
	BEGIN

		--Fixed Prices Expiring – This criterion will isolate pending recommendations that have fixed prices expiring between todays date and the date indicated by the user (it should include the selected date)
		--	- Fixed prices are only allowed for product exceptions, not SPG terms
		--	- These product exceptions should have an expiration date

		UPDATE a
		SET
			CriterionDescription = @CriterionDescription,
			a.Action = 
				CASE 
					WHEN @ActionType = 'Delete' THEN 'Delete'
					WHEN a.BaseAction <> @DefaultAction THEN a.BaseAction  -- if not pending, then a special skip or delete rule has been applied
					ELSE 'Accept'
				END,
			RecommendedDiscount = nd.RecommendedDiscount,
			RecommendPrice = TradePrice * ( 1 - nd.RecommendedDiscount ) ,
			RecommendImpact = (((TradePrice * ( 1 - nd.RecommendedDiscount )) - CurrentCost) - ( LastItemPrice - LastItemCost)) * TotalQuantity,
			RecommendGPP = 1.0 - (CurrentCost / NULLIF(TradePrice * ( 1 - nd.RecommendedDiscount ), 0) ),
			ActionType = @ActionType
		FROM #AllResults a
		OUTER APPLY ppe.fn_GetNewDiscount (
			@ActionType,
			@ActionPercent,
			@CapType,
			@CapPercent,
			a.RecommendedDiscount,
			a.RedDiscount,
			a.AmberDiscount,
			a.GreenDiscount) nd
		WHERE
			a.Action = 'Pending'
			AND (a.StrategicItem = @StrategicIndicatorSelection OR @StrategicIndicatorSelection = 'B')
			AND RowType = 'Detail'
			AND ExceptionFixedPrice IS NOT NULL
            AND ExceptionFixedPrice > 0
			AND ISDATE(ExceptionToDate) = 1
			AND ExceptionToDate BETWEEN GETDATE() AND CAST(@CriterionDate AS DATETIME)
	END


	FETCH NEXT FROM RecommendCriteriaCursor 
	INTO
		@SequenceNumber,
		@StrategicIndicatorSelection,
		@CriterionDate,
		@CriterionPercent,
		@CriterionMonth,
		@CriterionAmount,
		@ActionPercent,
		@CapPercent,
		@RecommendCriterionKey,
		@CriterionDescription,
		@InputType,
		@ActionType,
		@CapType
END


CLOSE RecommendCriteriaCursor
DEALLOCATE RecommendCriteriaCursor




--select '#AllResults 1', InitialRecommendedDiscount, RecommendedDiscount, *
--from #AllResults


--/* LPF-5069 PF-594 7142L78: llspg row's "recommended" discount needs to be recomputed based on the new product level recommended discount values */
;
WITH spg AS (
	SELECT
		AccountKey,
		LLSPG,
		SUM(TotalSalesAtTrade * RecommendedDiscount) / NULLIF(SUM(TotalSalesAtTrade), 0) AS RecommendedDiscount
	FROM #AllResults a
	WHERE
		a.RowType <> 'LLSPG'
		AND a.Action = 'Include'
		AND LLSPG IN (SELECT DISTINCT LLSPG FROM #AllResults a2 WHERE a2.CriterionDescription <> '')
	GROUP BY
		AccountKey,
		a.LLSPG
)
UPDATE a
SET a.RecommendedDiscount = spg.RecommendedDiscount
FROM #AllResults a
INNER JOIN spg
	ON spg.LLSPG = a.LLSPG
	AND spg.AccountKey = a.AccountKey
WHERE
	A.RowType = 'LLSPG'
	AND A.CriterionDescription NOT IN ('Terms Last Changed Before', 'Sales Less Than X Pounds in Last X Months')			-- https://enterbridge.atlassian.net/browse/LPF-5270 , https://enterbridge.atlassian.net/browse/LPF-5273





/*
kk note: next update the remaining PENDING: Existing terms to SKIP; New terms to ACCEPT (then run through existing skip and delete rules)
*/
UPDATE a
SET 
	a.Action = 'Skip'
FROM #AllResults a
WHERE 
	a.Action = 'Pending'
	AND a.UsualDiscount1 IS NOT NULL

UPDATE a
SET 
	a.Action = 'Accept'
FROM #AllResults a
WHERE 
	a.Action = 'Pending'
	AND a.UsualDiscount1 IS NULL


/*********************************************************************************************************************/
/*********************************************************************************************************************/
/*********************************************************************************************************************/
/*********************************************************************************************************************/




SELECT f.AccountKey, i.LLSPGCode AS LLSPG, SUM(ISNULL(f.TotalNetPrice,f.TotalActualPrice)) AS Sales, SUM(ISNULL(f.TotalNetQuantity, f.TotalQuantity)) AS Quantity, COUNT(DISTINCT f.ItemKey) AS ItemsSold
INTO #SixMonthSales
FROM lion.vwFactInvoiceLine f 
--INNER JOIN dbo.DimDay dd ON f.InvoiceDateDayKey = dd.DayKey 
INNER JOIN lion.vwItem i ON f.ItemKey = i.ItemKey
INNER JOIN #SPG spg ON spg.ItemGroup3Key = f.ItemGroup3Key
INNER JOIN #Account a ON a.AccountKey = f.AccountKey
--WHERE dd.SQLDate >= DATEADD(MM, -6, CAST(GETDATE() AS DATE))  removed because below is faster DB per LPF-3578
WHERE f.InvoiceDateDayKey >= (SELECT DayKey - 184 FROM dbo.DimDay WHERE SQLDate = CAST(GETDATE() AS DATE))
GROUP BY f.AccountKey,i.LLSPGCode


--PRINT 'step 8: ' + CONVERT(VARCHAR(50), GETDATE(),13)

       
-- If all rows for an LLSPG have a recommended discount that equals the default discount, do not create new term (Skip)
; 
WITH DefDiscLLSPG AS (
SELECT ad.AccountKey, al.LLSPG
FROM #AllResults ad 
INNER JOIN #AllResults al 
	ON ad.LLSPG = al.LLSPG 
	AND ad.AccountKey = al.AccountKey
	AND al.RowType = 'LLSPG' 
	AND ad.RowType = 'Detail'
WHERE al.DefaultDiscount <> 0 
	AND ad.Action <> 'Skip'
GROUP BY ad.AccountKey, al.LLSPG
HAVING COUNT(*) = SUM(CASE WHEN ad.RecommendedDiscount = -al.DefaultDiscount THEN 1 ELSE 0 END)
)
UPDATE a
SET a.Action = 'Skip'
FROM #AllResults a 
INNER JOIN DefDiscLLSPG s 
	ON a.LLSPG = s.LLSPG 
	AND s.AccountKey = a.AccountKey
AND a.Action <> 'Delete'


-- If all detail rows for an LLSPG have a market recommendation and the SPG has no terms, do not create new term (Skip)
; 
WITH MarketTerms AS (
SELECT a.AccountKey, a.LLSPG
FROM #AllResults a
WHERE a.RowType <> 'LLSPG'
GROUP BY a.AccountKey, a.LLSPG
HAVING COUNT(*) = SUM(CASE WHEN a.PriceInstructionTypeLevel = @MarketInstruction THEN 1 ELSE 0 END)
)
UPDATE a
SET a.Action = 'Skip'
FROM #AllResults a 
INNER JOIN MarketTerms s 
	ON a.LLSPG = s.LLSPG 
	AND s.AccountKey = a.AccountKey
WHERE a.Action <> 'Delete'

       
-- If all detail rows for an LLSPG are non-qualifying exceptions and the SPG has no terms, do not create new term (Skip)
;
WITH NoQuals AS (
SELECT a.AccountKey, a.LLSPG
FROM #AllResults a
WHERE a.RowType <> 'LLSPG'
GROUP BY a.AccountKey, a.LLSPG
HAVING COUNT(*) = SUM(CASE WHEN a.PriceInstructionTypeLevel = @NonQualException THEN 1 ELSE 0 END)
)
UPDATE a
SET a.Action = 'Skip'
FROM #AllResults a 
INNER JOIN NoQuals s 
	ON a.LLSPG = s.LLSPG 
	AND s.AccountKey = a.AccountKey
WHERE a.RowType = 'LLSPG' 
	AND a.UsualDiscount1 IS NULL
	AND a.Action <> 'Delete'

       
-- If no terms exist at the SPG level, do not create a new terms recommendation – leave with no terms unless:
--		Sales value is > £150 in last 6 months AND the quantity is greater than 5
--		OR, there are 5 or more items sold in the last 6 months
--		Then recommend a new term
;
WITH LowSales AS
( SELECT a.AccountKey, a.LLSPG
FROM #AllResults a LEFT OUTER JOIN
	    #SixMonthSales s ON a.LLSPG = s.LLSPG AND a.AccountKey = s.AccountKey
WHERE a.RowType = 'LLSPG' 
	AND a.UsualDiscount1 IS NULL 
	AND a.Action <> 'Skip' 
	AND NOT ((ISNULL(s.Sales, 0) > 150 AND ISNULL(s.Quantity, 0) > 5) OR ISNULL(s.ItemsSold, 0) >= 5)
)
UPDATE a
SET a.Action = 'Skip',
    a.PriceInstructionTypeLevel = @LowSalesInstruction
FROM #AllResults a 
INNER JOIN LowSales s 
	ON a.LLSPG = s.LLSPG 
	AND s.AccountKey = a.AccountKey
	AND a.Action <> 'Delete'


-- Remove an SPG term older than 6 months if:
--	No sales
--	Only one item sold in the last 6 months with quantity of 1
--	OR sales <£30 in the last 6 months
;
WITH DeleteCandidates AS
(
	SELECT 
		a.AccountKey, 
		a.LLSPG
	FROM #AllResults a 
	LEFT OUTER JOIN #SixMonthSales s 
		ON a.LLSPG = s.LLSPG 
		AND a.AccountKey = s.AccountKey
	WHERE a.RowType = 'LLSPG' 
		AND a.UsualDiscount1 IS NOT NULL 
		AND ISNULL(a.LastChangeDate, '1-JAN-2015') <= DATEADD(MM, -6, CAST(GETDATE() AS DATE)) 
		AND (ISNULL(s.Sales, 0) < 30 OR (ISNULL(s.Quantity, 0) <= 1 AND ISNULL(s.ItemsSold, 0) <= 1))
)
UPDATE a
SET a.Action = CASE WHEN (a.RowType = 'LLSPG' OR 
                            a.ExceptionDiscount IS NOT NULL OR 
							a.ExceptionFixedPrice IS NOT NULL
							) AND 
							ISNULL(a.LastChangeDate, '1-JAN-2015') <= DATEADD(MM, -6, CAST(GETDATE() AS DATE))
                        THEN 'Delete' 
						ELSE 'Skip' END,
    a.PriceInstructionTypeLevel = @LowSalesInstruction
FROM #AllResults a 
INNER JOIN DeleteCandidates d 
	ON a.LLSPG = d.LLSPG 
	AND d.AccountKey = a.AccountKey
	AND a.Action <> 'Delete'


-- Leave existing terms unchanged (Skip) if they have changed within the last three months
;
WITH RecentlyChanged AS
( 
	SELECT 
		a.AccountKey, 
		a.LLSPG
	FROM #AllResults a
	WHERE a.RowType = 'LLSPG' 
	AND CAST(ISNULL(a.LastChangeDate, '1-JAN-2015') AS DATE) > DATEADD(MONTH, -1.0 * @SkipDurationMonths, CAST(GETDATE() AS DATE))
)
UPDATE a
SET a.Action = 'Skip',
    a.PriceInstructionTypeLevel = @RecentChange
FROM #AllResults a 
INNER JOIN RecentlyChanged r 
	ON a.LLSPG = r.LLSPG 
	AND r.AccountKey = a.AccountKey
WHERE 
	a.UsualDiscount1 IS NOT NULL
	AND a.Action <> 'Delete'


--	PRINT 'step 9: ' + CONVERT(VARCHAR(50), GETDATE(),13)


-- If all detail rows are being skipped and LLSPG does not have an existing term, do not create a term (Skip)
;
WITH SkippedLLSPGs AS (
	SELECT 
		a.AccountKey, 
		a.LLSPG
	FROM #AllResults a
	WHERE a.RowType <> 'LLSPG'
	GROUP BY a.AccountKey, a.LLSPG
	HAVING COUNT(*) = SUM(CASE WHEN a.Action = 'Skip' THEN 1 ELSE 0 END)
)
UPDATE a
SET a.Action = 'Skip'
FROM #AllResults a 
INNER JOIN SkippedLLSPGs s 
	ON a.LLSPG = s.LLSPG 
	AND s.AccountKey = a.AccountKey
WHERE 
	a.RowType = 'LLSPG' 
	AND a.UsualDiscount1 IS NULL
	AND a.Action <> 'Delete'


/* LPF-1441 */
-- Leave existing terms unchanged (Skip) if they have changed within the last three months
;
WITH RecentlyChanged AS
( 
	SELECT 
		a.AccountKey, 
		a.LLSPG
	FROM #AllResults a
	WHERE a.RowType = 'LLSPG' 
		AND CAST(ISNULL(a.LastChangeDate, '1-JAN-2015') AS DATE) > DATEADD(MONTH, -1.0 * @SkipDurationMonths , CAST(GETDATE() AS DATE))
)
UPDATE a
SET a.Action = 'Skip',
	a.PriceInstructionTypeLevel = @RecentChange
FROM #AllResults a 
INNER JOIN RecentlyChanged r 
	ON a.AccountKey = r.AccountKey
	AND a.LLSPG = r.LLSPG
WHERE 
	a.UsualDiscount1 IS NOT NULL
	AND a.Action <> 'Delete'


/* LPF-1441 */
-- If all detail rows are being skipped and account does not have an existing term, do not create a term (Skip)
-- New: https://enterbridge.atlassian.net/browse/LPF-5149 : "If all detail-level lines recommended action is set to SKIP, set SPG line recommended action to SKIP."
;
WITH SkippedLLSPGs AS (
	SELECT 
		a.AccountKey,
		LLSPG
	FROM #AllResults a
	WHERE a.RowType <> 'LLSPG'
	GROUP BY 
		a.AccountKey,
		LLSPG
	HAVING COUNT(*) = SUM(CASE WHEN a.Action = 'Skip' THEN 1 ELSE 0 END)
)
UPDATE a
	SET a.Action = 'Skip'
	FROM #AllResults a 
	INNER JOIN SkippedLLSPGs s 
		ON a.AccountKey = s.AccountKey
		AND a.LLSPG = s.LLSPG
	WHERE a.RowType = 'LLSPG' 
		-- LPF-5149 AND a.UsualDiscount1 IS NULL
		AND a.Action <> 'Delete'


-- If LPF-1712 skip all fixed price exception records
UPDATE a
	SET a.Action = 'Skip'
	FROM #AllResults a 
	WHERE 
		a.RowType = 'Detail' 
		AND a.FixedPrice IS NOT NULL
		AND a.Action <> 'Delete'


-- from LPF-2053
-- Delete terms older than 6 months with no sales
-- Skip terms with no sales if they have been in place for less than six months or there is no last change date
;
WITH AccountSales AS (
	SELECT 
		AccountKey,
		ItemGroup3Key,
		SUM(ISNULL(r.ItemSales, 0)) AS AccountSales
	FROM #AllResults r
	GROUP BY 
		AccountKey,
		ItemGroup3Key
)
UPDATE r
SET 
	r.Action = 
		CASE 
			WHEN ISNULL(r.LastChangeDate, GETDATE()) >= DATEADD(MM, -6, GETDATE()) THEN 'Skip' 
			ELSE 'Delete' 
		END
	FROM    #AllResults r 
	INNER JOIN AccountSales s 
		ON r.AccountKey = s.AccountKey 
		AND r.ItemGroup3Key = s.ItemGroup3Key
	WHERE
		ISNULL(s.AccountSales, 0) = 0 
		OR r.RowType = 'LLSPG'																		--??? todo is this correct with that OR there with no parentheses 
		AND ISNULL(r.RecommendedDiscount, 0) = 0 
		AND	(SELECT COUNT(*) 
				FROM #AllResults ir 
				WHERE ir.AccountKey = r.AccountKey 
					AND	ir.LLSPG = r.LLSPG 
					AND	ir.RowType = 'Detail' 
					AND	ir.PriceInstructionTypeLevel IN (@ItemInstruction,  @LLSPGInstruction, @LowSalesInstruction)) = 0
		AND r.Action <> 'Delete'


/*LPF-2107 & LPF-3649 */
UPDATE r SET r.Action = 'Skip'
FROM #AllResults r
WHERE 
	r.Action = 'Delete' 
	AND r.ProdBrandAtLLSPG IN ('PIPE', 'CLIMT')


/*LPF-2557 */
UPDATE r SET r.Action = 'Include'
FROM #AllResults r
WHERE 
	r.PriceInstructionTypeLevel  =  @LLSPGInstruction
	AND r.ItemKey IS NOT NULL
	AND r.Action <> 'Delete'


/* LPF-2229 */
UPDATE r SET r.Action = 'Skip'
FROM #AllResults r
JOIN lion.vwAccountLLSPG v
	ON r.AccountKey = v.AccountKey
	AND r.ItemGroup3Key = v.ItemGroup3Key
WHERE 
	v.ContractClaimsPercent > 0.80
	AND r.Action <> 'Delete'


/*LPF-2896*/
UPDATE r SET r.Action = 'Skip'
FROM #AllResults r 
WHERE 
	Action = 'Accept' AND RowType = 'LLSPG' 
	AND CAST(RecommendedDiscount AS DEC(38,4)) = CAST(Disc AS DEC(38,4))
	AND r.Action <> 'Delete'

UPDATE r SET r.Action = 'Skip'
FROM #AllResults r  
WHERE Action = 'Accept' 
	AND RowType = 'Detail' 
	AND CAST(RecommendedDiscount AS DEC(38,4)) = CAST(ExceptionDiscount AS DEC(38,4))
	AND r.Action <> 'Delete'
/*end of LPF-2896*/


----LPF-1845 we can't do the below because it won't match the spreadsheet
--UPDATE a
--   SET a.Action = 'Skip'
--  FROM #AllResults a 
-- WHERE a.RowType = 'Detail' and
--	   a.PriceInstructionTypeLevel = @LLSPGInstruction



-- LPF-5165 PF-614 0% terms Delete
UPDATE a
SET a.Action = 'Skip'
FROM #AllResults a
WHERE a.CriterionDescription = '0% Terms'
AND a.RowType = 'LLSPG'
AND (
	EXISTS (
		SELECT 1 
		FROM #AllResults a1 
		WHERE 
			a1.LLSPG = a.LLSPG 
			AND a1.RowType <> 'LLSPG' 
			AND (
				a1.Action IN ('Accept') 
				OR (
					(ExceptionFixedPrice IS NOT NULL OR ExceptionDiscount IS NOT NULL) -- Product exception https://enterbridge.atlassian.net/browse/LPF-5272
					AND a1.Action IN ('Skip') 
				)
			)
		)
	)
AND a.Action = 'Delete'




/** Now that all the actions have been set, recompute the llspg recommended discount ignoring non-includes - LPF-5163 PF-612 Logic issues with skip/include **/
--/* LPF-5069 PF-594 7142L78: llspg row's "recommended" discount needs to be recomputed based on the new product level recommended discount values */
;
WITH spg AS (
	SELECT
		AccountKey,
		LLSPG,
		SUM(TotalSalesAtTrade * RecommendedDiscount) / NULLIF(SUM(TotalSalesAtTrade), 0) AS RecommendedDiscount
	FROM #AllResults a
	WHERE
		a.RowType <> 'LLSPG'
		AND a.Action = 'Include'
		--AND LLSPG IN (SELECT DISTINCT LLSPG FROM #AllResults a2 WHERE a2.CriterionDescription <> '')
	GROUP BY
		AccountKey,
		a.LLSPG
)
UPDATE a
SET a.RecommendedDiscount = spg.RecommendedDiscount
FROM #AllResults a
INNER JOIN spg
	ON spg.LLSPG = a.LLSPG
	AND spg.AccountKey = a.AccountKey
WHERE
	A.RowType = 'LLSPG'
	AND A.CriterionDescription NOT IN ('Terms Last Changed Before', 'Sales Less Than X Pounds in Last X Months')			-- https://enterbridge.atlassian.net/browse/LPF-5270 , https://enterbridge.atlassian.net/browse/LPF-5273


/*********************************************************************************************************************/






/*** LPF-3296 Provide "CCOR" like capabilities within Create Recommendations ***/
	--SELECT DISTINCT r.AccountKey, r.LLSPG
	--INTO #NonPP
	--FROM #AllResults r
	--WHERE r.ProdBrandAtLLSPG IS NOT NULL 
	--AND r.ProdBrandAtLLSPG NOT IN ('', 'PLUMB', 'PARTS','DRAIN','PIPE','CLIMT') --Scott's notes??? yes
	--AND	r.LLSPG NOT IN (
	--	'DA37',
	--	'DA40',
	--	'DA55',
	--	'DA60',
	--	'DC31',
	--	'DC53',
	--	'DC54',
	--	'DC55',
	--	'DG01',
	--	'DG05',
	--	'DP92',
	--	'DW11',
	--	'DW52',
	--	'DW59',
	--	'EV13'
	--	)


--PRINT 'step 10: ' + CONVERT(VARCHAR(50), GETDATE(),13)


--Check for Account Brand Owners included in filter
IF EXISTS (SELECT TOP 1 * FROM ppe.RecommendAccountOwner WHERE RecommendKey = @RecommendKey)	
BEGIN
	DELETE FROM #AllResults
	WHERE AccountBrandOwnerFullname NOT IN
		(
		SELECT dp.FullName
		FROM ppe.RecommendAccountOwner rao
		INNER JOIN DimAccountManager dam
		ON dam.AccountManagerKey = rao.AccountManagerKey
		INNER JOIN DimPerson dp
		ON dp.DimPersonKey = dam.DimPersonKey
		WHERE rao.RecommendKey = @RecommendKey
		)
END
	
--LPF-3340
IF EXISTS (SELECT TOP 1 * FROM ppe.RecommendItem WHERE RecommendKey = @RecommendKey)	
BEGIN
	DELETE r
	FROM #AllResults r
	LEFT JOIN  ppe.RecommendItem ri ON r.ItemKey = ri.ItemKey AND RecommendKey = @RecommendKey
	WHERE ri.RecommendItemKey IS NULL 

	DELETE r
	FROM #AllResults r
	WHERE ExceptionDiscount IS NULL 
END
-- end LPF-3340


DELETE FROM ppe.RecommendOutput WHERE RecommendKey = @RecommendKey

INSERT INTO ppe.RecommendOutput
        (RecommendKey
        ,LLSPGImpact
        ,RowType
        ,RecID
        ,AccountKey
        ,ItemKey
        ,InvoiceLineGroup1Key
        ,AccountNumber
        ,AccountName
        ,Area
        ,LLSPG
        ,LLSPGDescription
        ,ProductCode
        ,ProductDescription
        ,CurrentCost
        ,ProdKVI
        ,TermsNDPOverride
        ,DefaultDiscount
        ,Brand
        ,SPGDisc1
        ,SPGDisc2
        ,ExceptionDiscount
        ,SPGCompoundedDiscount
        ,RecommendedDiscount
        ,PriceInstructionTypeLevel
        ,Action
        ,LastChangeDate
        ,FixedPrice
        ,ProductSales
        ,LLSPGSales
        ,TotalAccountSales
        ,RecommendedPrice
        ,RecommendedPriceLogic
        ,RecommendedImpact
        ,StrategicItem
        ,AdjustedLastItemPrice
        ,LastItemPrice
        ,LastPriceLevelGAR
        ,LastItemDiscount
        ,LastItemGPP
        ,AdjustedLastItemGPP
        ,TotalQuantity
        ,GreenPriceTradeAdj
        ,AmberPrice
        ,RedPrice
        ,TradePrice
        ,GreenGPP
        ,AmberGPP
        ,RedGPP
        ,TradeGPP
        ,ContractClaims
        ,SalesSize
        ,PriceApproach
        ,PBLevel
        ,CustomerTermsKey
        ,AccountID
        ,PyramidCode
        ,SPGCode
        ,OwningBrandCode
        ,DeleteFlag
        ,LastChangeInitials
        ,LastTermsChangeDate
        ,LastChangeTime
        ,LastChangePLID
        ,LastChangeBranch
        ,ExceptionProduct
        ,ExceptionFixedPrice
        ,FixedPriceStartDate
        ,FixedPriceExpiryDate
        ,ExceptionPCF
        ,PlaybookPricingGroupKey
        ,LastPriceDerivation
        ,LastSaleDate
        ,ExportFlag
		,GreenDiscount
		,AmberDiscount
		,RedDiscount
		,Branch
		,AccountOwner
		,FixedPriceIndicator
		,RecommendedGPP
		,AdjustedLPCDiscount
		,ExceptionFromDate
		,ExceptionToDate
		,KVIOverride
		,RecommendedDiscount2
		,AccountBrandOwner
		,ProdBrandAtLLSPG
		,AccountOwnersBrand
		,SPGFromDate
		,RecommendCriterionDescription
		,TotalSalesAtTrade
		,SPGLastDiscount
		,InitialRecommendedDiscount
		)
SELECT  @RecommendKey,
		SUM(IIF(r.Action='Skip',0,r.RecommendImpact)) OVER (PARTITION BY r.LLSPG, r.AccountKey) AS LLSPGImpact,
		r.RowType,
		CAST(NEWID() AS NVARCHAR(255)) AS RecID,
		r.AccountKey,
		r.ItemKey,
		r.InvoiceLineGroup1Key,
		r.AccountNumber,
		r.AccountName,
		r.Area,
		r.LLSPG,
		r.LLSPGDescription,
		r.ProductCode,
		r.ProductDescription,
		r.CurrentCost,
		r.KVI AS ProdKVI,
		r.NDPFlag AS TermsNDPOverride,
		r.DefaultDiscount AS DefaultDiscount,
		r.ProdBrandAtLLSPG,
		l.SPGDisc1,
		l.SPGDisc2,
		r.ExceptionDiscount AS ExceptionDiscount,
		l.Disc AS SPGCompoundedDiscount,
		r.RecommendedDiscount,
		--CASE RowType  -- per LPF-2009 replaces the below, see last line for SPGDisc2 recommendation
		--	WHEN 'LLSPG' THEN l.SPGDisc1
		--	ELSE r.RecommendedDiscount
		--END,
		--CASE --LPF-1491  01-21-2016 comments esp #4 also LPF-1620
		--	WHEN r.Action = 'Skip' THEN NULL
		--	--WHEN r.KVI = 'Y' AND NOT EXISTS (SELECT * 
		--	--			                FROM lion.AccountSPG aspg 
		--	--						   WHERE aspg.AccountKey = r.AccountKey AND 
		--	--						         aspg.LLSPGCode IN (r.LLSPG, 'ALL')) THEN 0
		--	WHEN RowType = 'LLSPG' AND 
		--		EXISTS (select COUNT(*), SUM(CASE KVI WHEN '' THEN 0 ELSE 1 END) -- LPF-1841
		--				from #ItemTerms 
		--				where itemgroup3key= r.ItemGroup3Key
		--				group by itemgroup3key having COUNT(*) = SUM(CASE KVI WHEN '' THEN 0 ELSE 1 END) and COUNT(*) > 0) THEN 0

		--	WHEN r.Action <> 'Delete' AND r.RecommendedDiscount < r.DefaultDiscount THEN r.DefaultDiscount
		--	WHEN r.LLSPGSales = 0 and a detail row LPF-5273 AND DATEDIFF(d,r.LastChangeDate,GETDATE()) > 90 THEN r.Disc + @MaxChange
		--	ELSE r.RecommendedDiscount --per LPF-2009 recommend discount applied to SPGDisc2, see last line
		--END AS RecommendedDiscount,
		r.PriceInstructionTypeLevel,
		r.Action,
		r.LastChangeDate,
		r.FixedPrice,
		r.ItemSales AS ProductSales,
        --SUM(r.ItemSales) OVER ( PARTITION BY r.LLSPG, r.AccountKey ) AS LLSPGSales ,
		LLSPGSales,
        SUM(r.ItemSales) OVER ( PARTITION BY r.AccountKey ) AS TotalAccountSales ,
		r.RecommendPrice,
		r.RecommendPriceLogic,
		CASE 
			WHEN r.RowType = 'Detail' AND r.Action <> 'Delete' THEN
							--(r.RecommendPrice - r.CurrentCost - (r.LastItemPrice - r.LastItemCost) ) * r.TotalQuantity
					CASE
						WHEN RecommendPriceLogic = 'Trade' THEN 0
						WHEN  r.Action IN ( 'Skip', 'Include')
							THEN (r.TradePrice * r.TotalQuantity) * ((1.0-a.SPGLastDiscount) /*r.AdjustedLPCDiscount*/ - SPGRecomDisc )
						ELSE 
							(r.TradePrice * r.TotalQuantity) * (r.AdjustedLPCDiscount - r.RecommendedDiscount )

					END 	

			WHEN  r.RowType = 'Detail' AND /* LPF-1949 SPG impact=0 when delete */ r.Action = 'Delete' THEN 0

			WHEN  r.RowType <> 'Detail' AND /*LPF-2021 SPG impact=0 when delete/skip */ r.Action IN ('Delete','Skip') THEN 0
				
			ELSE SUM( 
					CASE
						WHEN RecommendPriceLogic = 'Trade' THEN 0
						WHEN r.Action IN ('Delete','Skip','Accept') THEN 0
						--WHEN PriceInstructionTypeLevel IN (@NonQualException,@LowSalesInstruction) AND r.Action = 'Skip'
						--	THEN (r.TradePrice * r.TotalQuantity) * (r.AdjustedLPCDiscount - SPGRecomDisc )
						ELSE 
							(r.TradePrice * r.TotalQuantity) * ((1.0-a.SPGLastDiscount) - SPGRecomDisc)			--r.RecommendedDiscount )		-- kfk 4/18/2017 LPF-4888 SPG Impact Error in ECR

					END
					) OVER (PARTITION BY r.LLSPG, r.AccountKey) 

		END AS RecommendedImpact,
		r.StrategicItem,
		r.AdjustedLastItemPrice,
		r.LastItemPrice,
		r.LastPriceLevelGAR,
		r.LastItemDiscount,
		r.LastItemGPP,
		r.AdjustedLastItemGPP,
		r.TotalQuantity,
		r.GreenPrice_TradeAdj,
		r.AmberPrice,
		r.RedPrice,
		r.TradePrice,
		r.GreenGPP,
		r.AmberGPP,
		r.RedGPP,
		r.TradeGPP,
		r.ContractClaims,
		r.SalesSize,
		r.PriceApproach,
		r.PBLevel,
		r.CustomerTermsKey,
		r.AccountID,
		r.PyramidCode,
		r.SPGCode,
		r.OwningBrandCode,
		r.DeleteFlag,
		r.LastChangeInitials,
		CONVERT(DATE, r.LastChangeDate) AS LastTermsChangeDate,
		r.LastChangeTime,
		r.LastChangePLID,
		r.LastChangeBranch,
		r.ExceptionProduct,
		r.ExceptionFixedPrice AS ExceptionFixedPrice,
		r.ExceptionFromDate AS FixedPriceStartDate,
		r.ExceptionToDate AS FixedPriceExpiryDate,
		r.ExceptionPCF,
		r.PlaybookPricingGroupKey,
		r.PriceDerivation AS LastPriceDerivation,
		r.LastSaleDate AS LastSaleDate,
		0,
		--CASE WHEN r.ItemKey IS NULL THEN d.GreenDiscount ELSE ( 1 - ( r.GreenPrice_TradeAdj / NULLIF(r.TradePrice,0) ) ) END AS GreenDiscount,
		--CASE WHEN r.ItemKey IS NULL THEN d.AmberDiscount ELSE ( 1 - ( r.AmberPrice / NULLIF(r.TradePrice,0) ) ) END AS AmberDiscount,
		--CASE WHEN r.ItemKey IS NULL THEN d.RedDiscount   ELSE ( 1 - ( r.RedPrice / NULLIF(r.TradePrice,0) ) ) END AS RedDiscount,

		r.GreenDiscount,
		r.AmberDiscount,
		r.RedDiscount,

		r.Branch,
		r.AccountOwner,
		r.FixedPriceIndicator,
		r.RecommendGPP,
		r.AdjustedLPCDiscount,
		r.ExceptionFromDate,
		r.ExceptionToDate,
		CASE --LPF-1703
			WHEN  EXISTS (SELECT * 
						FROM lion.AccountSPG aspg 
						WHERE aspg.AccountKey = r.AccountKey AND 
								aspg.LLSPGCode IN (r.LLSPG, 'ALL')) THEN 'Y'
			ELSE 'N'
		END,

		CASE RowType -- per LPF-2009
			WHEN 'LLSPG' THEN (( --below is Disc2Recommended = (CompoundRecommended - Disc1Recommended) / (1 - Disc1Recommended)
							CASE --LPF-1491  01-21-2016 comments esp #4 also LPF-1620
								WHEN r.Action = 'Skip' THEN NULL
								--WHEN r.KVI = 'Y' AND NOT EXISTS (SELECT * 
								--			                FROM lion.AccountSPG aspg 
								--						   WHERE aspg.AccountKey = r.AccountKey AND 
								--						         aspg.LLSPGCode IN (r.LLSPG, 'ALL')) THEN 0
								WHEN RowType = 'LLSPG' AND 
									EXISTS (SELECT COUNT(*), SUM(CASE KVI WHEN '' THEN 0 ELSE 1 END) -- LPF-1841
											FROM #ItemTerms 
											WHERE ItemGroup3Key= r.ItemGroup3Key
											GROUP BY ItemGroup3Key HAVING COUNT(*) = SUM(CASE KVI WHEN '' THEN 0 ELSE 1 END) AND COUNT(*) > 0) THEN 0

								WHEN r.Action <> 'Delete' AND r.RecommendedDiscount < r.DefaultDiscount THEN r.DefaultDiscount
								
								/* the line below appears to have been never true since r.LLSPGSales was only computed for detail rows.  before https://enterbridge.atlassian.net/browse/LPF-5273 that is */
								--WHEN r.LLSPGSales = 0 AND DATEDIFF(d,r.LastChangeDate,GETDATE()) > 90 THEN r.Disc + @MaxChange

								ELSE r.RecommendedDiscount --per LPF-2009 recommend discount applied to SPGDisc2, see last line
							END
			) - l.SPGDisc1) / NULLIF(1 - NULLIF(l.SPGDisc1,0), 0)
			ELSE NULL
		END AS RecommendedDiscount2,
		r.AccountBrandOwnerFullname,
		r.ProdBrandAtLLSPG,
		r.AccountOwnersBrand,
		@TermsStartDate, --LPF-3305
		CriterionDescription,
		TotalSalesAtTrade,
		(1.0-a.SPGLastDiscount),
		InitialRecommendedDiscount
FROM    #AllResults r 
LEFT OUTER JOIN (SELECT AccountKey, LLSPG, RecommendedDiscount AS SPGRecomDisc FROM #AllResults WHERE RowType  <> 'Detail') r2 
	ON r.AccountKey = r2.AccountKey
	AND r.LLSPG = r2.LLSPG
LEFT OUTER JOIN #LLSPGTerms l ON r.LLSPG = l.SPGCode AND l.AccountKey = r.AccountKey
--LEFT OUTER JOIN	#Discounts d ON d.AccountKey = r.AccountKey AND d.ItemGroup3Key = l.ItemGroup3Key
LEFT OUTER JOIN ppe.ExcludeItem ei ON ei.ItemKey = r.ItemKey
LEFT OUTER JOIN ppe.ExcludeSPG es ON es.ItemGroup3Key = r.ItemGroup3Key
LEFT OUTER JOIN ppe.ExcludeAccount ea ON ea.AccountKey = r.AccountKey
--LEFT OUTER JOIN #NonPP npp ON npp.LLSPG = r.LLSPG AND npp.AccountKey = r.AccountKey 
OUTER APPLY ppe.fn_GetDiscountSPG (r.AccountKey,r.ItemGroup3Key) a
WHERE ei.ExcludeItemKey IS NULL
AND es.ExcludeSPGKey IS NULL
AND ea.ExcludeAccountKey IS NULL
--AND npp.LLSPG IS NULL
ORDER BY 
	1 DESC,  -- LLSPGImpact
	LLSPG, 
	RowType DESC, 
	PriceInstructionTypeLevel, 
	ProductCode

SELECT @@ROWCOUNT AS Result 



--DROP TABLE #LLSPGDefaultTerms
--DROP TABLE #ProductExceptionDefaultTerms
DROP TABLE #LLSPG
DROP TABLE #LLSPGTerms
DROP TABLE #Prelim
DROP TABLE #PrelimSoldLLSPG
DROP TABLE #PrelimLLSPG
DROP TABLE #ItemTerms
DROP TABLE #Updates
--DROP TABLE #NonPP
DROP TABLE #AllResults
DROP TABLE #SixMonthSales
DROP TABLE #DefaultTerms



GO
