SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [cm].[BulkEditClaimLineReview](@WebUserKey INT, @ClaimJournalKey INT, @RejectReasonKey INT = NULL, @Comment VARCHAR(4000) = NULL)

AS 

SET NOCOUNT ON

DECLARE @ContractBatchCount INT
DECLARE @UpdatedClaimLineCount INT
DECLARE @ErrorMessage VARCHAR(250)
DECLARE @CommentLineCount INT
DECLARE @MultipleClaimLines TABLE (ClientInvoiceLineUniqueID KEY_NORMAL_TYPE, BatchKey INT)
DECLARE @ClientInvoiceLineUniqueID INT


-------- Check for errors -------------------
IF @Comment IS NULL
BEGIN
	SET @ErrorMessage = 'ERROR: Must include Comment'
END

IF @RejectReasonKey IS NULL
BEGIN
	SET @ErrorMessage = 'ERROR: Reject Reason Key missing'
END
---------------------------------------------

IF @ErrorMessage IS NULL
BEGIN


SET @UpdatedClaimLineCount = 0

SET @ClientInvoiceLineUniqueID = (SELECT ClientInvoiceLineUniqueID FROM cm.ClaimJournal WHERE ClaimJournalKey = @ClaimJournalKey)

--Get multiple claim lines (claim lines with the same transaction id)
	INSERT INTO @MultipleClaimLines (ClientInvoiceLineUniqueID, BatchKey)
	SELECT ClientInvoiceLineUniqueID, BatchKey
	FROM cm.ClaimJournal
	WHERE ClientInvoiceLineUniqueID IN (
	SELECT DISTINCT ClientInvoiceLineUniqueID 
	FROM cm.ClaimJournal
	WHERE ClaimType = 'C'   --do not group Adjustment or Manual claims
	GROUP BY ClientInvoiceLineUniqueID
	HAVING COUNT(*) > 1 )


	BEGIN TRAN

	DECLARE @UpdatedStatusRowData TABLE (
		ClaimJournalKey KEY_NORMAL_TYPE,
		NewClaimJournalStatus varchar(1),
		OldClaimJournalStatus varchar(1),
		NewADVRecommendation varchar(1),
		OldADVRecommendation varchar(1)
	)

--Set selected claim line to Pending
	UPDATE cj
	SET ClaimJournalStatus = 'P',
		ADVRecommendation = 'A',
		ADVBatchKey = NULL,
		--Comment = @Comment,
		RejectionReasonKey = NULL,
		ModificationDate = GETDATE(),
		ModifiedByWebUserKey = @WebUserKey,
		ADVRecommendationDate = GETDATE(),
		ADVRecommendationWebUserKey = @WebUserKey
	OUTPUT 
			INSERTED.ClaimJournalKey,
			INSERTED.ClaimJournalStatus,
			DELETED.ClaimJournalStatus,
			INSERTED.ADVRecommendation,
			DELETED.ADVRecommendation
			INTO @UpdatedStatusRowData
	FROM cm.ClaimJournal cj
	WHERE cj.ClaimJournalKey = @ClaimJournalKey

	SET @UpdatedClaimLineCount = @@ROWCOUNT

	INSERT INTO cm.ClaimJournalLog (WebUserKey, ClaimJournalKey, Notes, CreationDate, ClaimAction )
		SELECT @WebUserKey, 
			    ClaimJournalKey, 
			    'Status changed from ' + OldClaimJournalStatus + ' to ' + NewClaimJournalStatus, 
			    GETDATE(),
			   'Accept Claim Line'
			FROM @UpdatedStatusRowData

	COMMIT TRAN

--Set all other associated multiple claim lines to a status of deleted
	BEGIN TRAN

	DECLARE @UpdatedStatusRowDataX TABLE (
		ClaimJournalKey KEY_NORMAL_TYPE,
		NewClaimJournalStatus varchar(1),
		OldClaimJournalStatus varchar(1),
		NewADVRecommendation varchar(1),
		OldADVRecommendation varchar(1)
	)

	UPDATE cm.ClaimJournal
	SET ClaimJournalStatus = 'D',
		ADVRecommendation = 'D',
		ADVBatchKey = NULL,
		--Comment = @Comment,
		RejectionReasonKey = @RejectReasonKey,
		ModificationDate = GETDATE(),
		ModifiedByWebUserKey = @WebUserKey,
		ADVRecommendationDate = GETDATE(),
		ADVRecommendationWebUserKey = @WebUserKey
	OUTPUT 
			INSERTED.ClaimJournalKey,
			INSERTED.ClaimJournalStatus,
			DELETED.ClaimJournalStatus,
			INSERTED.ADVRecommendation,
			DELETED.ADVRecommendation
			INTO @UpdatedStatusRowDataX
	FROM cm.ClaimJournal cj
	INNER JOIN @MultipleClaimLines m
		ON m.ClientInvoiceLineUniqueID = cj.ClientInvoiceLineUniqueID
	WHERE m.ClientInvoiceLineUniqueID = @ClientInvoiceLineUniqueID
			AND cj.ClaimJournalStatus <> 'D'
			AND cj.ClaimJournalKey <> @ClaimJournalKey

	SET @UpdatedClaimLineCount = @UpdatedClaimLineCount + @@ROWCOUNT

	INSERT INTO cm.ClaimJournalLog (WebUserKey, ClaimJournalKey, Notes, CreationDate, ClaimAction )
		SELECT @WebUserKey, 
			    ClaimJournalKey, 
			    'Status changed from ' + OldClaimJournalStatus + ' to ' + NewClaimJournalStatus +' Rejection Reason = ' + (SELECT RejectionReasonValue FROM cm.RejectionReason WHERE RejectionReasonKey = @RejectReasonKey) , 
			    GETDATE(),
			   'Reject Claim Line'
		FROM @UpdatedStatusRowDataX

	COMMIT TRAN


--If the supplier claims status is R (ready for claim) and an associated supplier claim line has changed status, the supplier claim should change to P (pending)
DECLARE @SupplierClaimUpdateRowData TABLE (
		SupplierClaimKey KEY_NORMAL_TYPE,
		ClaimStatus varchar(1))

	UPDATE sc
	SET ClaimStatus = 'P'
	OUTPUT INSERTED.SupplierClaimKey,
			INSERTED.ClaimStatus
		INTO @SupplierClaimUpdateRowData
	FROM cm.SupplierClaim sc
	INNER JOIN cm.ClaimJournal cj
		ON cj.VendorKey = sc.VendorKey
		AND cj.ClaimMonth = sc.ClaimMonth
	WHERE cj.ClaimJournalKey = @ClaimJournalKey
		AND sc.ClaimStatus = 'R'

--Log Supplier Claim status change
INSERT INTO cm.ClaimLog
	(
	[WebUserKey],
	[SupplierClaimKey],
	[Notes],
	[CreationDate],
	[ClaimAction]
	)
SELECT 
	1,
	d.SupplierClaimKey,
	'From: Ready to Claim  To: ' + s.SupplierClaimStatusDescription,
	GETDATE(),
	'Supplier Claim System Status Change'
FROM @SupplierClaimUpdateRowData d
INNER JOIN cm.SupplierClaimStatus s
	ON s.SupplierClaimStatus = d.ClaimStatus


IF @Comment IS NOT NULL
BEGIN
	UPDATE cj
	SET Comment = @Comment
	FROM cm.ClaimJournal cj
	LEFT JOIN @MultipleClaimLines m
		ON m.ClientInvoiceLineUniqueID = cj.ClientInvoiceLineUniqueID
	WHERE m.ClientInvoiceLineUniqueID = @ClientInvoiceLineUniqueID
			

	SET @CommentLineCount = @@ROWCOUNT

	--Log Comment update
	INSERT INTO cm.ClaimJournalLog (WebUserKey, ClaimJournalKey, Notes, CreationDate, ClaimAction)
		SELECT @WebUserKey, 
			    ClaimJournalKey, 
			    Comment, 
			    GETDATE(),
			   'Amend Comment'
		FROM cm.ClaimJournal
		WHERE ClientInvoiceLineUniqueID = @ClientInvoiceLineUniqueID

END 

END

SELECT @ContractBatchCount AS BatchContractCount, 
	   @UpdatedClaimLineCount AS UpdatedClaimLineCount,
	   @ErrorMessage AS ErrorMessage
GO
