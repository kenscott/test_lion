SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO













CREATE PROCEDURE [lion].[MarketingReport]
@startdate AS DATE , @enddate AS DATE,

   
	@RegionFilter Description_Small_type = '',
	@AreaFilter Description_Small_type = '',
	@NetworkFilter Description_Small_type = '',
	@BranchFilter Description_Small_type = ''

AS
SELECT  item.LLSPGCode ,
        item.ProductCode ,
        item.ItemDescription ,
	   
        CASE WHEN Retailer <> 'Wolseley' THEN Retailer WHEN f.PriceGuidanceIndicator = 'Y' THEN 'Non Terms' ELSE 'Terms' END AS [Price Source], --price source/terms indicator

		CASE WHEN Retailer <> 'Wolseley' THEN NULL ELSE SUM (ISNULL(f.TotalNetQuantity,f.TotalQuantity)) END AS TotalQty,
		CASE WHEN Retailer IS NULL AND f.Score = 1 THEN SUM (ISNULL(f.TotalNetQuantity,f.TotalQuantity)) ELSE NULL END AS QtyPriceIndex1,
		CASE WHEN Retailer IS NULL AND f.Score = 2 THEN SUM (ISNULL(f.TotalNetQuantity,f.TotalQuantity)) ELSE NULL END AS QtyPriceIndex2,
		CASE WHEN Retailer IS NULL AND f.Score = 3 THEN SUM (ISNULL(f.TotalNetQuantity,f.TotalQuantity)) ELSE NULL END AS QtyPriceIndex3,
		CASE WHEN Retailer IS NULL AND f.Score = 4 THEN SUM (ISNULL(f.TotalNetQuantity,f.TotalQuantity)) ELSE NULL END AS QtyPriceIndex4,
		CASE WHEN Retailer IS NULL AND f.Score = 5 THEN SUM (ISNULL(f.TotalNetQuantity,f.TotalQuantity)) ELSE NULL END AS QtyPriceIndex5,

	    CASE WHEN Retailer <> 'Wolseley' THEN NULL ELSE (SUM(ISNULL(f.TotalNetPrice, f.TotalActualPrice))/ NULLIF(SUM(ISNULL(f.TotalNetQuantity,f.TotalQuantity)),0)) END AS [Avg Price] ,
		CASE WHEN Retailer <> 'Wolseley' THEN NULL ELSE SUM(fil.CcProfit)/NULLIF(SUM(ISNULL(f.TotalNetQuantity,f.TotalQuantity)),0) END AS AvgClaims,
		CASE WHEN Retailer <> 'Wolseley' THEN AVG(wc.Price) ELSE SUM(f.UnitListPrice*ISNULL(f.TotalNetQuantity,f.TotalQuantity))/NULLIF(SUM(ISNULL(f.TotalNetQuantity,f.TotalQuantity)),0)  END AS TradePrice,
		CASE WHEN Retailer <> 'Wolseley' THEN NULL ELSE SUM(f.GreenPrice*ISNULL(f.TotalNetQuantity,f.TotalQuantity))/NULLIF(SUM(ISNULL(f.TotalNetQuantity,f.TotalQuantity)),0) END AS AvgGreenPrice,  
		CASE WHEN Retailer <> 'Wolseley' THEN NULL ELSE SUM(f.AmberPrice*ISNULL(f.TotalNetQuantity,f.TotalQuantity))/NULLIF(SUM(ISNULL(f.TotalNetQuantity,f.TotalQuantity)),0) END AS AvgAmberPrice, 
		CASE WHEN Retailer <> 'Wolseley' THEN NULL ELSE SUM(f.RedPrice*ISNULL(f.TotalNetQuantity,f.TotalQuantity))/NULLIF(SUM(ISNULL(f.TotalNetQuantity,f.TotalQuantity)),0) END AS AvgRedPrice, 

		CASE WHEN Retailer <> 'Wolseley' THEN AVG(item.CurrentCost) ELSE SUM(ISNULL(f.TotalNetCost, f.TotalActualCost))/NULLIF(SUM(ISNULL(f.TotalNetQuantity,f.TotalQuantity)),0) END AS [Avg Cost] ,

		CASE WHEN Retailer <> 'Wolseley' THEN NULL ELSE 
	    1-(AVG(ISNULL(f.TotalNetPrice, f.TotalActualPrice)/NULLIF (ISNULL(f.TotalNetQuantity,f.TotalQuantity),0)/NULLIF(f.UnitListPrice,0))) END AS [Avg Disc%],

		CASE WHEN Retailer <> 'Wolseley' THEN 1- (AVG (item.CurrentCost/NULLIF(wc.Price, 0))) ELSE 1-(SUM(ISNULL(f.TotalNetCost, f.TotalActualCost))/NULLIF(SUM(ISNULL(f.TotalNetPrice, f.TotalActualPrice)),0))END AS [Avg GP%],
			--CASE WHEN Retailer IS NULL AND Score = 1 THEN 1-(SUM(ISNULL(f.TotalNetCost, f.TotalActualCost))/NULLIF(SUM(ISNULL(f.TotalNetPrice, f.TotalActualPrice)),0)) ELSE NULL END AS [GP%PriceIndex1],
			--CASE WHEN Retailer IS NULL AND Score = 2 THEN 1-(SUM(ISNULL(f.TotalNetCost, f.TotalActualCost))/NULLIF(SUM(ISNULL(f.TotalNetPrice, f.TotalActualPrice)),0)) ELSE NULL END AS [GP%PriceIndex2],
			--CASE WHEN Retailer IS NULL AND Score = 3 THEN 1-(SUM(ISNULL(f.TotalNetCost, f.TotalActualCost))/NULLIF(SUM(ISNULL(f.TotalNetPrice, f.TotalActualPrice)),0)) ELSE NULL END AS [GP%PriceIndex3],
			--CASE WHEN Retailer IS NULL AND Score = 4 THEN 1-(SUM(ISNULL(f.TotalNetCost, f.TotalActualCost))/NULLIF(SUM(ISNULL(f.TotalNetPrice, f.TotalActualPrice)),0)) ELSE NULL END AS [GP%PriceIndex4],
			--CASE WHEN Retailer IS NULL AND Score = 5 THEN 1-(SUM(ISNULL(f.TotalNetCost, f.TotalActualCost))/NULLIF(SUM(ISNULL(f.TotalNetPrice, f.TotalActualPrice)),0)) ELSE NULL END AS [GP%PriceIndex5],

		CASE WHEN Retailer <> 'Wolseley' THEN NULL ELSE SUM(ISNULL(f.TotalNetPrice, f.TotalActualPrice)) END AS TotalNetSales,
		CASE WHEN Retailer IS NULL AND  Score = 1 THEN SUM(ISNULL(f.TotalNetPrice, f.TotalActualPrice)) ELSE NULL END AS NetSalesPriceIndex1,
		CASE WHEN Retailer IS NULL AND  Score = 2 THEN SUM(ISNULL(f.TotalNetPrice, f.TotalActualPrice)) ELSE NULL END AS NetSalesPriceIndex2,
		CASE WHEN Retailer IS NULL AND  Score = 3 THEN SUM(ISNULL(f.TotalNetPrice, f.TotalActualPrice)) ELSE NULL END AS NetSalesPriceIndex3,
		CASE WHEN Retailer IS NULL AND  Score = 4 THEN SUM(ISNULL(f.TotalNetPrice, f.TotalActualPrice)) ELSE NULL END AS NetSalesPriceIndex4,
		CASE WHEN Retailer IS NULL AND  Score = 5 THEN SUM(ISNULL(f.TotalNetPrice, f.TotalActualPrice)) ELSE NULL END AS NetSalesPriceIndex5,

		CASE WHEN Retailer <> 'Wolseley' THEN NULL ELSE SUM(ISNULL(f.TotalNetCost, f.TotalActualCost)) END AS TotalNetCost,
		CASE WHEN Retailer IS NULL AND  Score = 1 THEN SUM(ISNULL(f.TotalNetCost, f.TotalActualCost)) ELSE NULL END AS NetCostPriceIndex1,
		CASE WHEN Retailer IS NULL AND  Score = 2 THEN SUM(ISNULL(f.TotalNetCost, f.TotalActualCost)) ELSE NULL END AS NetCostPriceIndex2,
		CASE WHEN Retailer IS NULL AND  Score = 3 THEN SUM(ISNULL(f.TotalNetCost, f.TotalActualCost)) ELSE NULL END AS NetCostPriceIndex3,
		CASE WHEN Retailer IS NULL AND  Score = 4 THEN SUM(ISNULL(f.TotalNetCost, f.TotalActualCost)) ELSE NULL END AS NetCostPriceIndex4,
		CASE WHEN Retailer IS NULL AND  Score = 5 THEN SUM(ISNULL(f.TotalNetCost, f.TotalActualCost)) ELSE NULL END AS NetCostPriceIndex5,

		CASE WHEN Retailer <> 'Wolseley' THEN NULL ELSE SUM(fil.CcProfit) END AS NetClaims,
		
		CASE WHEN Retailer <> 'Wolseley' THEN NULL ELSE SUM (f.Score*ISNULL(f.TotalNetPrice, f.TotalActualPrice))/NULLIF(SUM(ISNULL(f.TotalNetPrice, f.TotalActualPrice)),0) END AS PriceIndex,



		CASE WHEN Retailer <> 'Wolseley' THEN NULL ELSE SUM(fi.Impact) END AS [Red Impact],
		CASE WHEN Retailer <> 'Wolseley' THEN NULL ELSE SUM(fti.Impact) END AS [Red-to-Amber Impact],
		CASE WHEN Retailer <> 'Wolseley' THEN NULL ELSE SUM(tsi.Impact) END AS [Amber-to-Green Impact],
		CASE WHEN Retailer <> 'Wolseley' THEN NULL ELSE SUM(tr.Risk) END AS GreenRisk,

		CASE WHEN Retailer <> 'Wolseley' THEN '' ELSE ilg1.deviatedindicator END AS ContractClaims,
		CASE WHEN Retailer <> 'Wolseley' THEN '' WHEN ilg1.deviatedindicator = 'N' THEN flpac.BaseStrategicItem ELSE 'ALL' END AS StrategicItem ,
		CASE WHEN Retailer <> 'Wolseley' THEN '' WHEN ilg1.deviatedindicator = 'N' THEN flpac.PriceApproach ELSE 'ALL' END AS PriceApproach


/*

f.AccountKey, f.AlternateAccountKey, f.ItemKey, f.VendorKey, f.AccountManagerKey, f.InvoiceDateDayKey, f.InvoiceDateMonthKey, f.InvoiceLineGroup1Key, f.InvoiceLineGroup2Key, f.InvoiceLineGroup3Key, 
                         f.InvoiceLineJunkKey, f.AccountGroup1Key, f.AccountGroup2Key, f.AccountGroup3Key, f.ItemGroup1Key, f.ItemGroup2Key, f.ItemGroup3Key, f.InvoiceLineUniqueIdentifier, f.LinkedInvoiceLineUniqueIdentifier, 
                         f.ClientInvoiceID, f.ClientInvoiceLineID,
						  f.TotalQuantity, f.TotalActualPrice, f.TotalActualCost, f.UnitListPrice, f.TotalNetQuantity, f.TotalNetPrice, f.TotalNetCost, f.PriceDerivation, 
						  fm.GPP AS FloorGPP, tm.GPP AS TargetGPP, sm.GPP AS StretchGPP,
						   f.Score,
						    f.RPBPlaybookPricingGroupKey,
							 f.TotalActualPrice - f.TotalActualCost AS GP,
							  (flpac.LastItemPrice - flpac.LastItemCost) / NULLIF (flpac.LastItemPrice, 0) AS LastItemGPP,
							   fi.Impact AS FloorImpact, ti.Impact AS TargetImpact, si.Impact AS StretchImpact, 
							   fti.Impact AS [Floor to Target Impact], tsi.Impact AS [Target to Stretch Impact], r.Risk AS [Stretch Risk], 
                         f.TotalActualPrice * fm.GPP AS [GTM$-Floor], f.TotalActualPrice * tm.GPP AS [GTM$-Target], f.TotalActualPrice * sm.GPP AS [GTM$-Stretch],
						  flpac.UDVarchar4 AS BaseSales603010Bucket, 
                         flpac.UDVarchar1 AS BaseStrategicItem, 
						 fil.UDVarchar1 AS LastPriceDerivation,
						  tr.Risk AS [Trade Risk], 
						  CASE WHEN (f.TotalActualPrice - f.TotalActualCost) < 0 THEN 'Negative' ELSE 'Positive' END AS MarginType,
                          CASE WHEN item.ItemPyramidCode <> 1 OR
                         acct.AccountName = 'Unknown' OR
                         item.ItemNumber = 'Unknown' OR
                         item.ItemObsolete = 'Y' OR
                         item.SpecialInd = 'Y' OR
                         ilg1.ShipmentType = 'D' OR
                         f.TotalQuantity = 0 OR
                         f.TotalActualPrice = 0 OR
                         f.PriceDerivation IN ('SPO', 'CCP') OR
                         acct.BranchPrimaryBrand = 'BURDN' THEN 'Y' ELSE 'N' END AS ExclusionFlag,
						  f.TotalActualPrice * f.Score AS ScoredSales,
						   f.GreenPrice * f.TotalQuantity AS [Sales @ Green], f.AmberPrice * f.TotalQuantity AS [Sales @ Amber], f.RedPrice * f.TotalQuantity AS [Sales @ Red],
						    f.UnitListPrice * f.TotalQuantity AS [Sales @ Trade],
							 ISNULL(vfil.ClaimNo, '') AS ClaimNumber, 
                         j.InvoiceLineJunkUDVarchar2 + '/' + REPLACE(j.InvoiceLineJunkUDVarchar3, '.00000000', '') AS ChargeNote,
						  ISNULL(j.InvoiceLineJunkUDVarchar16, '') AS CTSRelCode,
						   CASE WHEN ISNULL(f.TerminalEmulator, '') LIKE '(%/F)' THEN 'Flynet' ELSE 'Other' END AS TerminalEmulator, 
						   f.PriceGuidanceIndicator, 
						   CASE WHEN ps.BranchnchType = 'Pilot' THEN 'Pre-Pilot Terms' ELSE 'Terms' END END END AS [Price Source Group],
                          CASE WHEN ISNULL(f.TerminalEmulator, '') NOT LIKE '(%/F)' THEN CASE WHEN f.TerminalEmulator LIKE '(%/W%)' OR
                         f.TerminalEmulator LIKE '(%/PT%)' OR
                         f.TerminalEmulator IN ('()', '(Stairs)', '(PC)') Type = 'Pilot' AND dd.SQLDate >= ps.LiveDate THEN CASE WHEN ISNULL(f.TerminalEmulator, 
                         '') NOT LIKE '(%/F)' THEN 'Non Price Guidance' WHEN f.PriceGuidanceIndicator = 'Y' THEN 'Non Terms' ELSE 'Terms' END ELSE CASE WHEN f.PriceDerivation IN ('RP', 'LP', 'TP', 'DDC', 'MP', 'ADJ') 
                         THEN CASE WHEN ps.BranchType = 'Pilot' THEN 'Pre-Pilot Non Terms' ELSE 'Non Terms' END ELSE CASE WHEN ps.Bra
                         THEN 'PowerTerm/wIntegrate' WHEN f.TerminalEmulator = 'Q' THEN 'Quotes' WHEN f.TerminalEmulator = 'E' THEN 'EDI Orders' ELSE 'Other' END WHEN f.PriceGuidanceIndicator = 'Y' THEN 'Non Terms' ELSE 'Terms'
                          END AS [Price Source], CASE WHEN ct.AccountKey IS NOT NULL THEN 1 ELSE 0 END AS TermsIndicator,
						   vfil.TakenBy
*/
FROM    lion.vwFactInvoiceLinePricebandScore f
		--INNER JOIN lion.RPBPricebandAttributes pba ON f.RPBPlaybookPricingGroupKey = pba.PlaybookPricingGroupKey
        INNER JOIN dbo.DimDay dd ON f.InvoiceDateDayKey = dd.DayKey
        --INNER JOIN lion.vwFactInvoiceLine vfil ON f.InvoiceLineUniqueIdentifier = vfil.InvoiceLineUniqueIdentifier
        INNER JOIN lion.vwItem item ON f.ItemKey = item.ItemKey
                                       AND f.VendorKey = item.VendorKey
		INNER JOIN lion.MarketingReportItem  cs ON cs.ItemKey = item.ItemKey
        INNER JOIN lion.vwAccount acct ON f.AccountKey = acct.AccountKey
        INNER JOIN lion.vwFactInvoiceLineGroup1 ilg1 ON f.InvoiceLineGroup1Key = ilg1.InvoiceLineGroup1Key
        --INNER JOIN dbo.DimInvoiceLineJunk j ON f.InvoiceLineJunkKey = j.InvoiceLineJunkKey
        --LEFT OUTER JOIN ( SELECT    AccountKey ,
        --                            ItemKey ,
        --                            InvoiceLineGroup1Key
        --                  FROM      lion.vwFactInvoiceLinePricebandScore(NOLOCK)
        --                  GROUP BY  AccountKey ,
        --                            ItemKey ,
        --                            InvoiceLineGroup1Key
        --                  HAVING    SUM(TotalActualPrice) = 0
        --                            AND SUM(TotalActualCost) <> 0
        --                ) ex ON f.AccountKey = ex.AccountKey
        --                        AND f.ItemKey = ex.ItemKey
        --                        AND f.InvoiceLineGroup1Key = ex.InvoiceLineGroup1Key
        INNER JOIN lion.vwFactLastPriceAndCost (NOLOCK) flpac ON f.AccountKey = flpac.AccountKey
                                                             AND f.ItemKey = flpac.ItemKey
                                                             AND f.InvoiceLineGroup1Key = flpac.InvoiceLineGroup1Key
        --LEFT OUTER JOIN lion.vwLastTrans lt ON f.AccountKey = lt.AccountKey
        --                                       AND f.ItemKey = lt.ItemKey
        --                                       AND f.InvoiceLineGroup1Key = lt.InvoiceLineGroup1Key
        INNER JOIN lion.vwFactInvoiceLine (NOLOCK) fil ON fil.InvoiceLineUniqueIdentifier = flpac.[AlternateLastInvoiceLineUniqueIdentifier]
		LEFT OUTER JOIN lion.[CompetitorPricing] wc ON wc.[ItemKey] = item.ItemKey
        --INNER JOIN lion.vwAccountGroup1 ag1 ON ag1.AccountGroup1Key = f.AccountGroup1Key
        --LEFT OUTER JOIN lion.PilotSchedule ps ON ps.Branch = ag1.Branch
        LEFT OUTER JOIN lion.vwTermsExistence ct ON ct.AccountKey = f.AccountKey
                                                    AND ct.ItemGroup3Key = f.ItemGroup3Key
        CROSS APPLY lion.fn_GetCappedGPP(f.TotalActualCost
                                         / NULLIF(f.TotalQuantity, 0),
                                         f.FloorGPP, .99, f.UnitListPrice) fm
        CROSS APPLY lion.fn_GetCappedGPP(f.TotalActualCost
                                         / NULLIF(f.TotalQuantity, 0),
                                         f.TargetGPP, .99, f.UnitListPrice) tm
        CROSS APPLY lion.fn_GetCappedGPP(f.TotalActualCost
                                         / NULLIF(f.TotalQuantity, 0),
                                         f.StretchGPP, .99, f.UnitListPrice) sm
        CROSS 
                         APPLY lion.fn_GetImpact(( ( ( item.CurrentCost
                                                       - ISNULL(fil.CCProfit / NULLIF(fil.TotalQuantity,0),0))
                                                     / NULLIF(( 1 - fm.GPP ),
                                                              0) )
                                                   - ( item.CurrentCost
                                                       - ISNULL(fil.CCProfit / NULLIF(fil.TotalQuantity,0),0))),
                                                 ( fil.TotalActualPrice - fil.TotalActualCost) / NULLIF(fil.TotalQuantity,0),
                                                 f.[TotalQuantity]) fi
        --CROSS 
        --                 APPLY lion.fn_GetImpact(( ( ( item.CurrentBranchCost
        --                                               - item.Padding
        --                                               - item.CurrNotSettVal
        --                                               - ISNULL(lt.LastCCProfit,
        --                                                      0) )
        --                                             / NULLIF(( 1 - tm.GPP ),
        --                                                      0) )
        --                                           - ( item.CurrentBranchCost
        --                                               - item.Padding
        --                                               - item.CurrNotSettVal
        --                                               - ISNULL(lt.LastCCProfit,
        --                                                      0) ) ),
        --                                         ( lt.LastItemPrice
        --                                           - lt.LastItemCost ),
        --                                         f.[TotalQuantity]) ti
        --CROSS 
        --                 APPLY lion.fn_GetImpact(( ( ( item.CurrentBranchCost
        --                                               - item.Padding
        --                                               - item.CurrNotSettVal
        --                                               - ISNULL(lt.LastCCProfit,
        --                                                      0) )
        --                                             / NULLIF(( 1 - sm.GPP ),
        --                                                      0) )
        --                                           - ( item.CurrentBranchCost
        --                                               - item.Padding
        --                                               - item.CurrNotSettVal
        --                                               - ISNULL(lt.LastCCProfit,
        --                                                      0) ) ),
        --                                         ( lt.LastItemPrice
        --                                           - lt.LastItemCost ),
        --                                         f.[TotalQuantity]) si
        CROSS 
                         APPLY lion.fn_GetPartialImpact(( ( ( item.CurrentCost
																- ISNULL(fil.CCProfit / NULLIF(fil.TotalQuantity,0),0))
                                                            / NULLIF(( 1
                                                              - fm.GPP ), 0) )
                                                          - ( item.CurrentCost
                                                              - ISNULL(fil.CCProfit / NULLIF(fil.TotalQuantity,0),0))),
                                                        ( ( ( item.CurrentCost
                                                              - ISNULL(fil.CCProfit / NULLIF(fil.TotalQuantity,0),0))
                                                            / NULLIF(( 1
                                                              - tm.GPP ), 0) )
                                                          - ( item.CurrentCost
                                                              - ISNULL(fil.CCProfit / NULLIF(fil.TotalQuantity,0),0) ) ),
                                                        ( fil.TotalActualPrice - fil.TotalActualCost) / NULLIF(fil.TotalQuantity,0),
                                                        f.[TotalQuantity]) AS fti
        CROSS 
                         APPLY lion.fn_GetPartialImpact(( ( ( item.CurrentCost
                                                              - ISNULL(fil.CCProfit / NULLIF(fil.TotalQuantity,0),0) )
                                                            / NULLIF(( 1
                                                              - tm.GPP ), 0) )
                                                          - ( item.CurrentCost
                                                              - ISNULL(fil.CCProfit / NULLIF(fil.TotalQuantity,0),0))),
                                                        ( ( ( item.CurrentCost
                                                              - ISNULL(fil.CCProfit / NULLIF(fil.TotalQuantity,0),0))
                                                            / NULLIF(( 1
                                                              - sm.GPP ), 0) )
                                                          - ( item.CurrentCost
                                                              - ISNULL(fil.CCProfit / NULLIF(fil.TotalQuantity,0),0))),
                                                        ( fil.TotalActualPrice - fil.TotalActualCost) / NULLIF(fil.TotalQuantity,0),
                                                        f.[TotalQuantity]) AS tsi
        CROSS 
                         APPLY lion.fn_GetRisk(( ( ( item.CurrentCost
                                                     - ISNULL(fil.CCProfit / NULLIF(fil.TotalQuantity,0),0) )
                                                   / NULLIF(( 1 - sm.GPP ), 0) )
                                                 - ( item.CurrentCost
                                                     - ISNULL(fil.CCProfit / NULLIF(fil.TotalQuantity,0),0))),
                                               ( fil.TotalActualPrice - fil.TotalActualCost) / NULLIF(fil.TotalQuantity,0),
                                               f.[TotalQuantity]) r
        CROSS APPLY lion.fn_GetRisk(f.[UnitListPrice],
                                    f.TotalActualPrice
                                    / NULLIF(f.TotalQuantity, 0),
                                    f.TotalQuantity) tr
WHERE  
-- ex.AccountKey IS NULL
        --AND  -- keep this line for some exclusions
		(dd.SQLDate BETWEEN @startdate AND @enddate) AND 
	        ((@RegionFilter <> '' AND acct.Region = @RegionFilter) OR (@RegionFilter = '')) AND
	        ((@AreaFilter <> '' AND acct.Area = @AreaFilter) OR (@AreaFilter = '')) AND
	        ((@NetworkFilter <> '' AND acct.Network = @NetworkFilter) OR (@NetworkFilter = '')) AND
	        ((@BranchFilter <> '' AND acct.Branch = @BranchFilter) OR (@BranchFilter = ''))
	        


GROUP BY item.LLSPGCode ,
        item.ProductCode ,
        item.ItemDescription,
		retailer,	
		score,
		 CASE WHEN Retailer <> 'Wolseley' THEN Retailer WHEN f.PriceGuidanceIndicator = 'Y' THEN 'Non Terms' ELSE 'Terms' END,

  		CASE WHEN Retailer <> 'Wolseley' THEN '' ELSE ilg1.deviatedindicator END ,
		CASE WHEN Retailer <> 'Wolseley' THEN '' WHEN ilg1.deviatedindicator = 'N' THEN flpac.BaseStrategicItem ELSE 'ALL' END ,
		CASE WHEN Retailer <> 'Wolseley' THEN '' WHEN ilg1.deviatedindicator = 'N' THEN flpac.PriceApproach ELSE 'ALL' END 
  
       
	

		--	ilg1.deviatedindicator AS ContractClaims,
		--CASE WHEN ilg1.deviatedindicator = 'N' THEN flpac.BaseStrategicItem ELSE 'ALL' END AS StrategicItem ,
		--CASE WHEN ilg1.deviatedindicator = 'N' THEN pba.PriceApproach ELSE 'ALL' END AS PriceApproach

--ORDER BY Retailer,
--      CASE
--	  WHEN Retailer IS NULL THEN 1 
--	  ELSE 2
	  

	  --END 
	  












GO
