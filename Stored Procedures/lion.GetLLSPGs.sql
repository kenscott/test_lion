SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE [lion].[GetLLSPGs] @PyramidCode UDVarchar_type = '1'
AS

select i.LLSPGCode as [LLSPG Code], i.LLSPGDescription as [LLSPG Description], 
		(select count(*)
		   from lion.vwItem ii1
		  where ii1.LLSPGCode = i.LLSPGCode and ii1.ItemPyramidCode = @PyramidCode) as [Number of Products],
		count(distinct i.ItemKey) as [Number of Products Sold], 
        (select count(distinct accountkey)
		   from dbo.FactLastPriceAndCost f inner join
		        lion.vwItem ii on f.itemkey = ii.itemkey
		  where ii.LLSPGCode = i.LLSPGCode and f.Total12MonthSales > 0) as [Accounts with Sales],
		(select count(distinct accountkey)
		   from CustomerTerms ct 
		  where ct.SPGCode = i.LLSPGCode) as [Accounts with Terms],
		sum(FloorImpact) AS [Red Impact]
from lion.vwFactInvoiceLinePricebandScore f inner join lion.vwItem i on f.ItemKey = i.ItemKey 
where i.ItemPyramidCode = @PyramidCode
group by i.LLSPGCode, i.LLSPGDescription


GO
