SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- Stored Procedure

CREATE PROCEDURE [cm].[Load_StagingSupplierClaim]

AS

SET NOCOUNT ON


DECLARE
	@SQLString NVARCHAR(4000) = '',
	@ODSPrefix NVARCHAR(4000) = ''


SELECT 
	@ODSPrefix = ParamValue
FROM 
	dbo.[Param]
WHERE 
	ParamName = 'ClaimsManagerODSPrefix'

TRUNCATE TABLE cm.StagingSupplierClaim 
SELECT @SQLString = 'DELETE FROM ' + @ODSPrefix + '..DW_LOAD_ADV.ADV_SUPPLIER_DEFERRED_DATES'
EXECUTE sp_executesql @SQLString

;WITH ActiveClaim AS
(SELECT sc.VendorKey,
	sc.ClaimMonth,
	ISNULL(MAX(ModificationDate), (MAX(CreationDate))) AS MaxDate
FROM cm.SupplierClaim sc
GROUP BY sc.VendorKey,
	sc.ClaimMonth)

INSERT INTO cm.StagingSupplierClaim 
	([SupplierID]
	,[ClaimMonth]
	,[DeferredDate]
	)
SELECT dv.VendorNumber, 
	sc.ClaimMonth, 
	sc.DeferredDate
FROM cm.SupplierClaim sc
INNER JOIN DimVendor dv
	ON dv.VendorKey = sc.VendorKey
INNER JOIN ActiveClaim ac
	ON ac.VendorKey = sc.VendorKey
	AND ac.ClaimMonth = sc.ClaimMonth
	AND (ac.MaxDate = sc.ModificationDate OR ac.MaxDate = sc.CreationDate)

SELECT @SQLString = '
INSERT INTO ' + @ODSPrefix + '..DW_LOAD_ADV.ADV_SUPPLIER_DEFERRED_DATES
	(RUNDATE
	,SUPPLIER_CODE
	,PROCESSING_MONTH
	,DEFERRED_DATE)
SELECT
	GETDATE(),
	[SupplierID],
	[ClaimMonth],
	[DeferredDate]
FROM cm.StagingSupplierClaim
'
EXECUTE sp_executesql @SQLString

SELECT GETDATE()


GO
