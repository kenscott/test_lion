SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE  PROCEDURE [cc].[Load_SupplierContractItemGroupX] AS 
BEGIN

	SET NOCOUNT ON

	EXEC LogDCPEvent 'cc.Load_SupplierContractItemGroupX', 'B'

	DELETE cm --LPF-2522
	FROM Lion_Staging.dbo.ContractMPG cm
	LEFT JOIN (SELECT Region, OriginatorBranchOrRegion, ContractNumber, MpgsTotallyIncluded, MIN(RowIndex) MinRowIndex
				FROM Lion_Staging.dbo.ContractMPG
				GROUP BY Region, OriginatorBranchOrRegion, ContractNumber, MpgsTotallyIncluded) d ON d.MinRowIndex = cm.RowIndex
	WHERE d.MinRowIndex IS NULL

	DELETE cs --LPF-2522
	FROM Lion_Staging.dbo.ContractSPG cs
	LEFT JOIN (SELECT Region, OriginatorBranchOrRegion, ContractNumber, SPGSTotallyIncluded, MIN(RowIndex) MinRowIndex
				FROM Lion_Staging.dbo.ContractSPG
				GROUP BY Region, OriginatorBranchOrRegion, ContractNumber, SPGSTotallyIncluded) d ON d.MinRowIndex = cs.RowIndex
	WHERE d.MinRowIndex IS NULL

	DECLARE @RowsI INT,  @RowsU INT,  @RowsD INT

-- load new into SupplierContractItemGroupX
	INSERT INTO cc.SupplierContractItemGroupX
	        ( ContractKey ,
	          ItemGroupLevel ,
	          ItemGroupXKey ,
	          ClaimBackAmount ,
	          ClaimBackPerc ,
			  StartDT,
			  ExpiryDT,
			  ClaimType,
			  CreatedOnDT
	        )

	SELECT 
		sc.ContractKey,
		3,
		dig3.ItemGroup3Key,
		CASE 
			WHEN MAX(CAST(cs.SPGDiscountVal AS MONEY) / 100.0) <> 0 THEN MAX(CAST(cs.SPGDiscountVal AS MONEY) / 100.0) 
			ELSE 0.00 
		END ,
		CASE 
			WHEN MAX(CAST(cs.SPGDiscountVal AS MONEY)) = 0 AND MAX(CAST(cs.SPGDiscountPct AS MONEY)) = 0 THEN NULL 
			WHEN MAX(CAST(cs.SPGDiscountPct AS MONEY) / 100.0) <> 0 THEN MAX(CAST(cs.SPGDiscountPct AS MONEY) / 100.0) 
			ELSE 0.0000 
		END ,
		NULL, -- per LPF-2018 for now
		NULL, -- per LPF-2018 for now
		CASE
			WHEN (MAX(CAST(cs.SPGDiscountVal AS MONEY)) > 0 OR (MAX(CAST(cs.SPGDiscountVal AS MONEY)) = 0 and MAX(CAST(cs.SPGDiscountPct AS MONEY)) is null)) THEN 'A'
			WHEN (MAX(CAST(cs.SPGDiscountPct AS MONEY)) > 0 OR (MAX(CAST(cs.SPGDiscountPct AS MONEY)) = 0 and MAX(CAST(cs.SPGDiscountVal AS MONEY)) is null)) THEN 'P'
			ELSE 'A'
		END,
		sc.ContractCreationDT
	FROM Lion_Staging.dbo.ContractSPG cs
	JOIN cc.SupplierContract sc 
		ON cs.Region + cs.OriginatorBranchOrRegion + cs.ContractNumber = sc.FullContractNumber
	JOIN dbo.DimItemGroup3 dig3
		ON dig3.IG3Level1 = cs.SPGSTotallyIncluded
		AND sc.PyramidCode = dig3.IG3Level4
	LEFT JOIN cc.SupplierContractItemGroupX igx
		ON igx.ContractKey = sc.ContractKey
		AND igx.ItemGroupLevel = 3 -- spg = itemgroup3
		AND igx.ItemGroupXKey = dig3.ItemGroup3Key
	WHERE
		igx.ContractItemGroupXKey IS NULL
		AND sc.ContractSourceKey = 1
	GROUP BY sc.ContractKey,
		dig3.ItemGroup3Key, 
		sc.ContractCreationDT
	--HAVING MAX(CAST(cs.SPGDiscountVal AS MONEY)) > 0.0   ---LPF-3377
	--OR MAX(CAST(cs.SPGDiscountPct AS MONEY)) > 0.0

	SELECT @RowsI = @@ROWCOUNT 

--Check for Updates
	SELECT 
		sc.ContractKey,
		igx.ContractItemGroupXKey,
		3 AS ItemGroupLevel,
		dig3.ItemGroup3Key,
		CASE 
			WHEN MAX(CAST(cs.SPGDiscountVal AS MONEY) / 100.0) <> 0 THEN MAX(CAST(cs.SPGDiscountVal AS MONEY) / 100.0) 
			ELSE 0.00 
		END AS ClaimBackAmount,
		CASE 
			WHEN MAX(CAST(cs.SPGDiscountVal AS MONEY)) = 0 AND MAX(CAST(cs.SPGDiscountPct AS MONEY)) = 0 THEN NULL 
			WHEN MAX(CAST(cs.SPGDiscountPct AS MONEY) / 100.0) <> 0 THEN MAX(CAST(cs.SPGDiscountPct AS MONEY) / 100.0) 
			ELSE 0.0000 
		END AS ClaimBackPerc,
		CASE
			WHEN (MAX(CAST(cs.SPGDiscountVal AS MONEY)) > 0 OR (MAX(CAST(cs.SPGDiscountVal AS MONEY)) = 0 and MAX(CAST(cs.SPGDiscountPct AS MONEY)) is null)) THEN 'A'
			WHEN (MAX(CAST(cs.SPGDiscountPct AS MONEY)) > 0 OR (MAX(CAST(cs.SPGDiscountPct AS MONEY)) = 0 and MAX(CAST(cs.SPGDiscountVal AS MONEY)) is null)) THEN 'P'
			ELSE 'A'
		END AS ClaimType,
		sc.ContractCreationDT
	INTO #ContractItemGroupUpdates
	FROM Lion_Staging.dbo.ContractSPG cs
	JOIN cc.SupplierContract sc 
		ON cs.Region + cs.OriginatorBranchOrRegion + cs.ContractNumber = sc.FullContractNumber
	JOIN dbo.DimItemGroup3 dig3
		ON dig3.IG3Level1 = cs.SPGSTotallyIncluded
		AND sc.PyramidCode = dig3.IG3Level4
	JOIN cc.SupplierContractItemGroupX igx
		ON igx.ContractKey = sc.ContractKey
		AND igx.ItemGroupLevel = 3 -- spg = itemgroup3
		AND igx.ItemGroupXKey = dig3.ItemGroup3Key
	WHERE sc.ContractSourceKey = 1
	GROUP BY sc.ContractKey,
		igx.ContractItemGroupXKey,
		dig3.ItemGroup3Key, 
		sc.ContractCreationDT

UPDATE cc.SupplierContractItemGroupX
	SET	ClaimBackAmount = u.ClaimBackAmount,
		ClaimBackPerc = u.ClaimBackPerc,
		ClaimType = u.ClaimType,
		ModifiedOnDT = GETDATE() 
	FROM #ContractItemGroupUpdates u
	JOIN cc.SupplierContract sc
		ON sc.ContractKey = u.ContractKey 
	JOIN cc.SupplierContractItemGroupX sci
		ON u.ContractItemGroupXKey = sci.ContractItemGroupXKey
	WHERE sc.ContractSourceKey = 1
		AND u.ItemGroupLevel = sci.ItemGroupLevel
		AND (sci.ClaimBackAmount <> u.ClaimBackAmount
			OR sci.ClaimBackPerc <> u.ClaimBackPerc)	

	SELECT @RowsU = @@ROWCOUNT

--Delete spg records missing in the staging table
	DELETE FROM cc.SupplierContractItemGroupX
	WHERE ContractItemGroupXKey IN
	(SELECT sci.ContractItemGroupXKey
	FROM cc.SupplierContractItemGroupX sci
	JOIN cc.SupplierContract sc 
		ON sc.ContractKey = sci.ContractKey
	JOIN dbo.DimItemGroup3 dig3
		ON dig3.ItemGroup3Key = sci.ItemGroupXKey
	LEFT JOIN Lion_Staging.dbo.ContractSPG cs
		ON cs.Region + cs.OriginatorBranchOrRegion + cs.ContractNumber = sc.FullContractNumber 
		AND cs.SPGSTotallyIncluded = dig3.IG3Level1
	WHERE sc.ContractSourceKey = 1
		AND sci.ItemGroupLevel = 3 -- spg = itemgroup3
		AND cs.ContractNumber IS NULL)
		 
	SELECT @RowsD = @@ROWCOUNT

-- load new SupplierContractItemGroupX-level3 records
	INSERT INTO cc.SupplierContractItemGroupX
	        ( ContractKey ,
	          ItemGroupLevel ,
	          ItemGroupXKey ,
	          ClaimBackAmount ,
	          ClaimBackPerc ,
			  StartDT,
			  ExpiryDT,
			  ClaimType,
			  CreatedOnDT
	        )

	SELECT 
		sc.ContractKey,
		1,
		dig1.ItemGroup1Key,
		CASE 
			WHEN MAX(CAST(mpg.MPGDiscountVal AS MONEY) / 100.0) <> 0 THEN MAX(CAST(mpg.MPGDiscountVal AS MONEY) / 100.0) 
			ELSE 0.00 
		END ,
		CASE 
			WHEN MAX(CAST(mpg.MPGDiscountVal AS MONEY)) = 0 AND MAX(CAST(mpg.MPGDiscountPct AS MONEY)) = 0 THEN NULL 
			WHEN MAX(CAST(mpg.MPGDiscountPct AS MONEY) / 100.0) <> 0 THEN MAX(CAST(mpg.MPGDiscountPct AS MONEY) / 100.0) 
			ELSE 0.0000 
		END ,
		NULL, -- per LPF-2018 for now
		NULL, -- per LPF-2018 for now
		CASE
			WHEN (MAX(CAST(mpg.MPGDiscountVal AS MONEY)) > 0 OR (MAX(CAST(mpg.MPGDiscountVal AS MONEY)) = 0 and MAX(CAST(mpg.MPGDiscountPct AS MONEY)) is null)) THEN 'A'
			WHEN (MAX(CAST(mpg.MPGDiscountPct AS MONEY)) > 0 OR (MAX(CAST(mpg.MPGDiscountPct AS MONEY)) = 0 and MAX(CAST(mpg.MPGDiscountVal AS MONEY)) is null)) THEN 'P'
			ELSE 'A'
		END,
		sc.ContractCreationDT
	FROM Lion_Staging.dbo.ContractMPG mpg
	JOIN cc.SupplierContract sc 
		ON mpg.Region + mpg.OriginatorBranchOrRegion + mpg.ContractNumber = sc.FullContractNumber
	JOIN dbo.DimItemGroup1 dig1
		ON dig1.IG1Level1 = mpg.MpgsTotallyIncluded
		AND sc.PyramidCode = dig1.IG1Level2
	LEFT JOIN cc.SupplierContractItemGroupX igx
		ON igx.ContractKey = sc.ContractKey
		AND igx.ItemGroupLevel = 1 -- mpg = itemgroup1
		AND igx.ItemGroupXKey = dig1.ItemGroup1Key
	WHERE
		igx.ContractItemGroupXKey IS NULL
		AND sc.ContractSourceKey = 1
	GROUP BY sc.ContractKey,
		dig1.ItemGroup1Key, 
		sc.ContractCreationDT
	--HAVING MAX(CAST(mpg.MPGDiscountVal AS MONEY)) > 0.0   ---LPF-3377
	--OR MAX(CAST(mpg.MPGDiscountPct AS MONEY)) > 0.0
	
	SELECT @RowsI = @@ROWCOUNT + @RowsI

--Check for Updates
SELECT 
		sc.ContractKey,
		igx.ContractItemGroupXKey,
		1 AS ItemGroupLevel,
		dig1.ItemGroup1Key,
		CASE 
			WHEN MAX(CAST(mpg.MPGDiscountVal AS MONEY) / 100.0) <> 0 THEN MAX(CAST(mpg.MPGDiscountVal AS MONEY) / 100.0) 
			ELSE 0.00 
		END AS ClaimBackAmount,
		CASE 
			WHEN MAX(CAST(mpg.MPGDiscountVal AS MONEY)) = 0 AND MAX(CAST(mpg.MPGDiscountPct AS MONEY)) = 0 THEN NULL 
			WHEN MAX(CAST(mpg.MPGDiscountPct AS MONEY) / 100.0) <> 0 THEN MAX(CAST(mpg.MPGDiscountPct AS MONEY) / 100.0) 
			ELSE 0.0000 
		END AS ClaimBackPerc,
		CASE
			WHEN (MAX(CAST(mpg.MPGDiscountVal AS MONEY)) > 0 OR (MAX(CAST(mpg.MPGDiscountVal AS MONEY)) = 0 and MAX(CAST(mpg.MPGDiscountPct AS MONEY)) is null)) THEN 'A'
			WHEN (MAX(CAST(mpg.MPGDiscountPct AS MONEY)) > 0 OR (MAX(CAST(mpg.MPGDiscountPct AS MONEY)) = 0 and MAX(CAST(mpg.MPGDiscountVal AS MONEY)) is null)) THEN 'P'
			ELSE 'A'
		END AS ClaimType,
		sc.ContractCreationDT
	INTO #ContractItemGroup1Updates
	FROM Lion_Staging.dbo.ContractMPG mpg
	JOIN cc.SupplierContract sc 
		ON mpg.Region + mpg.OriginatorBranchOrRegion + mpg.ContractNumber = sc.FullContractNumber
	JOIN dbo.DimItemGroup1 dig1
		ON dig1.IG1Level1 = mpg.MpgsTotallyIncluded
		AND sc.PyramidCode = dig1.IG1Level2
	JOIN cc.SupplierContractItemGroupX igx
		ON igx.ContractKey = sc.ContractKey
		AND igx.ItemGroupLevel = 1 -- mpg = itemgroup1
		AND igx.ItemGroupXKey = dig1.ItemGroup1Key
	WHERE sc.ContractSourceKey = 1
	GROUP BY sc.ContractKey,
		igx.ContractItemGroupXKey,
		dig1.ItemGroup1Key, 
		sc.ContractCreationDT

UPDATE cc.SupplierContractItemGroupX
	SET	ClaimBackAmount = u.ClaimBackAmount,
		ClaimBackPerc = u.ClaimBackPerc,
		ClaimType = u.ClaimType,
		ModifiedOnDT = GETDATE() 
	FROM #ContractItemGroup1Updates u
	JOIN cc.SupplierContract sc
		ON sc.ContractKey = u.ContractKey 
	JOIN cc.SupplierContractItemGroupX sci
		ON u.ContractItemGroupXKey = sci.ContractItemGroupXKey
	WHERE sc.ContractSourceKey = 1
		AND u.ItemGroupLevel = sci.ItemGroupLevel
		AND (sci.ClaimBackAmount <> u.ClaimBackAmount
			OR sci.ClaimBackPerc <> u.ClaimBackPerc)	

	SELECT @RowsU = @RowsU + @@ROWCOUNT

--Delete mpg records missing in the staging table
	DELETE FROM cc.SupplierContractItemGroupX
	WHERE ContractItemGroupXKey IN
	(SELECT sci.ContractItemGroupXKey
	FROM cc.SupplierContractItemGroupX sci
	JOIN cc.SupplierContract sc 
		ON sc.ContractKey = sci.ContractKey
	JOIN dbo.DimItemGroup1 dig1
		ON dig1.ItemGroup1Key = sci.ItemGroupXKey
	LEFT JOIN Lion_Staging.dbo.ContractMPG mpg
		ON mpg.Region + mpg.OriginatorBranchOrRegion + mpg.ContractNumber = sc.FullContractNumber 
		AND mpg.MpgsTotallyIncluded = dig1.IG1Level1
	WHERE sc.ContractSourceKey = 1
		AND sci.ItemGroupLevel = 1 -- mpg = itemgroup1
		AND mpg.ContractNumber IS NULL)
		 
	SELECT @RowsD = @RowsD + @@ROWCOUNT


	EXEC LogDCPEvent 'cc.Load_SupplierContractItemGroupX', 'E', @RowsI, @RowsU, @RowsD

END


GO
