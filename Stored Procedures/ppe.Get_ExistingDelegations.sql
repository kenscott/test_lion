SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [ppe].[Get_ExistingDelegations] (@WebUserKey INT)
AS 
BEGIN

	IF EXISTS (SELECT * FROM dbo.WebRole wr
				JOIN dbo.WebUserRole wur ON wur.WebRoleKey = wr.WebRoleKey
				WHERE wr.WebRoleDescription IN ('Administrator','Price Flow Admin','Pricing Coordinator')
				AND wur.WebUserKey = @WebUserKey)
	BEGIN
		SELECT DISTINCT wud.*
		FROM WebUserDelegate wud
		WHERE DelegateWebUserKey <> DelegatorWebUserKey
		--AND GETDATE() BETWEEN wud.StartDate AND ISNULL(wud.EndDate,'12-31-9999')
		AND GETDATE() < ISNULL(wud.EndDate,'12-31-9999')
	END
	ELSE
    BEGIN
		SELECT DISTINCT wud.* --wu1.WebUserKey AS DelegateWebUserKey, wu2.WebUserKey DelegatorWebUserKey /* if I am a delegatee */
		FROM WebUserDelegate wud
		JOIN dbo.WebUser wu1 ON wu1.WebUserKey = wud.DelegateWebUserKey
		JOIN dbo.WebUser wu2 ON wu2.WebUserKey = wud.DelegatorWebUserKey
		WHERE DelegateWebUserKey <> DelegatorWebUserKey
		--AND GETDATE() BETWEEN wud.StartDate AND ISNULL(wud.EndDate,'12-31-9999')
		AND GETDATE() < ISNULL(wud.EndDate,'12-31-9999')
		AND wud.DelegateWebUserKey = @WebUserKey
		UNION
		SELECT DISTINCT wud.* --wu1.WebUserKey AS DelegateWebUserKey, wu2.WebUserKey DelegatorWebUserKey /* if I am a Delegator */
		FROM WebUserDelegate wud
		JOIN dbo.WebUser wu1 ON wu1.WebUserKey = wud.DelegateWebUserKey
		JOIN dbo.WebUser wu2 ON wu2.WebUserKey = wud.DelegatorWebUserKey
		WHERE DelegateWebUserKey <> DelegatorWebUserKey
		--AND GETDATE() BETWEEN wud.StartDate AND ISNULL(wud.EndDate,'12-31-9999')
		AND GETDATE() < ISNULL(wud.EndDate,'12-31-9999')
		AND wud.DelegatorWebUserKey = @WebUserKey
		UNION			
		SELECT wud.* --wu2.WebUserKey  AS DelegateWebUserKey , wu1.WebUserKey DelegatorWebUserKey /*my downline who are delegates */
		FROM BridgeAccountManager bam
		JOIN dbo.WebUser wu1 ON wu1.AccountManagerKey = bam.ParentAccountManagerKey
		JOIN dbo.WebUser wu2 ON wu2.AccountManagerKey = bam.SubsidiaryAccountManagerKey
		JOIN WebUserDelegate wud ON wud.DelegateWebUserKey = wu2.WebUserKey
		WHERE wud.DelegateWebUserKey <> wud.DelegatorWebUserKey
		--AND GETDATE() BETWEEN wud.StartDate AND ISNULL(wud.EndDate,'12-31-9999')
		AND GETDATE() < ISNULL(wud.EndDate,'12-31-9999')
		AND wu1.WebUserKey = @WebUserKey
		UNION			
		SELECT wud.* -- wu1.WebUserKey  AS DelegateWebUserKey , wu2.WebUserKey DelegatorWebUserKey /*my downline who are delegators */
		FROM BridgeAccountManager bam
		JOIN dbo.WebUser wu1 ON wu1.AccountManagerKey = bam.ParentAccountManagerKey
		JOIN dbo.WebUser wu2 ON wu2.AccountManagerKey = bam.SubsidiaryAccountManagerKey
		JOIN WebUserDelegate wud ON wud.DelegatorWebUserKey = wu2.WebUserKey
		WHERE wud.DelegateWebUserKey <> wud.DelegatorWebUserKey
		--AND GETDATE() BETWEEN wud.StartDate AND ISNULL(wud.EndDate,'12-31-9999')
		AND GETDATE() < ISNULL(wud.EndDate,'12-31-9999')
		AND wu1.WebUserKey = @WebUserKey
	END

END
GO
