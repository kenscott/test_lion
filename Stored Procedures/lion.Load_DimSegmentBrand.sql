SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [lion].[Load_DimSegmentBrand]
WITH EXECUTE AS CALLER
AS
EXEC LogDCPEvent 'ETL - DimSegmentBrand', 'B'

--EXEC dba_indexrebuild 'DimSegmentBrand', 'Drop'


--This table will contain a row for every row in the dimension table that has been changed in the staging table (type 1 update will be done)
CREATE TABLE #UpdateKeys (KeyID INTEGER)

DECLARE @RowsI Quantity_Normal_type, @RowsU Quantity_Normal_type, @RowsD Quantity_Normal_type
SET @RowsI = 0
SET @RowsU = 0
SET @RowsD = 0

--This is used to look up the Day key in the DimDay table
DECLARE @TodayDayKey Key_Small_type
SET @TodayDayKey = (SELECT DayKey FROM DimDay WHERE CONVERT(VARCHAR(10), SQLDate, 120) = CONVERT(VARCHAR(10), GETDATE(), 120))

--TRUNCATE TABLE DimSegmentBrand

IF NOT EXISTS (SELECT 1 FROM DimSegmentBrand WHERE SegmentBrand = 'Unknown' AND Segment = 'Unknown')
BEGIN
	INSERT DimSegmentBrand	(
		SegmentBrand,
		Segment)
	VALUES (
		'Unknown', 
		'Unknown')
	SET @RowsI = @RowsI + @@RowCount
END


INSERT DimSegmentBrand (
	SegmentBrand,
	Segment)
SELECT DISTINCT 
	ISNULL(SegmentBrand, N''),
	ISNULL(Segment, N'')
FROM Lion_Staging.dbo.CustomerBrandSegment s
WHERE NOT EXISTS (SELECT 1 FROM DimSegmentBrand x WHERE x.SegmentBrand = ISNULL(s.SegmentBrand, N'') AND x.Segment = ISNULL(s.Segment, N''))	--INSERTs all new dimension rows
SET @RowsI = @RowsI + @@RowCount


SELECT TOP 10 * FROM DimSegmentBrand

--EXEC dba_indexrebuild 'DimSegmentBrand', 'Create'

EXEC LogDCPEvent 'ETL - DimSegmentBrand', 'E', @RowsI, @RowsU, @RowsD




GO
