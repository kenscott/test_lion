SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE [cc].[BulkEditContract]
    (
      @WebUserKey INT ,
      @BatchKey INT ,
      @ExpirationDate DATE ,
      @AccountKeys VARCHAR(MAX)
    )
/*
Purpose: Allows bulk editing of expiration dates and addition of accounts.  The ExpirationDate and AccountKey parameters
		 are optional, either or both can be specified.

JIRA: LPF-2997 - Create a stored procedure that does a bulk insert of accounts and update contract expiration date for a contract
	
Application:  This is used in conjunction with the bulk editing feature within the application.

Outputs: BatchContractCount - the number of contracts found with the batchkey
		 UpdatedExpirationDateContractCount - the number of contracts updated with the expiration date
		 UpdatedAccountContractCount - the number of contracts updated with an account (this can be misleading I suppose,
									   since multiple accounts can be provided.


Examples:

	select * from cc.SupplierContractstatus
	select * from cc.SupplierContract where statuskey = 2
	select top 100 * from dimaccountgroup1
	select top 100 * from dimaccount
	
	select * from dimaccount where accountkey in (2,3,4)

	select * from lion.Batch
	insert into lion.Batch (sourcekey) values (6)
	
	update cc.SupplierContract set batchkey = 29036 where contractkey in (166792, 166793)

	select startdate, expirationdate, * from cc.SupplierContract where batchkey = 29036

	exec cc.bulkeditcontract 1965, 29036, '2016-06-01', null
	exec cc.bulkeditcontract 1, 205179, '2016-06-01', null
		
*/

/*
LPF-4601 CC-1576 Contract Manager - Contract View | Bulk edit does not updated Last Amended column. 
When bulk editing contracts, the Amend Date does not update. See attached video.
by Ken Scott 07/31/2017
*/

/*
LPF-4602 Update cc.BulkEditContract to set product level start dates as needed
When updating items in a contract due to expiration date changes, if the item's start date is after the new expiration date, set the start date to the new expiration date.
by Ken Scott 08/01/2017
*/


AS 

--testing
--DECLARE @WebUserKey INT, @BatchKey INT, @ExpirationDate DATE, @AccountKeys VARCHAR(Max)
--SET @WebUserKey= 1
--SET @BatchKey=200267
--SET @ExpirationDate = N'07/08/2017'
--SET @AccountKeys =  NULL

--EXEC cc.BulkEditContract @WebUserKey, @BatchKey, @ExpirationDate, @AccountKeys
 
    SET NOCOUNT ON

    DECLARE @UpdatedExpirationDateContractCount INT
    DECLARE @UpdatedAccountContractCount INT
    DECLARE @ContractBatchCount INT
    DECLARE @AddedContractAccountCount INT
    DECLARE @modificationDate DATETIME
    DECLARE @createDate DATETIME
    DECLARE @vendorKey INT
    DECLARE @ContractID VARCHAR(9)


    SET @UpdatedExpirationDateContractCount = 0
    SET @UpdatedAccountContractCount = 0
    SET @AddedContractAccountCount = 0
    SET @modificationDate = GETDATE()
    
	SELECT @ContractBatchCount = COUNT(*) FROM cc.SupplierContract WHERE BatchKey = @BatchKey


--Get list of contract branches for this WebUserKey
CREATE TABLE #ContractBranchList (SortOrder INT, PrimaryAccountGroup1Key INT, Branch VARCHAR(50), BranchName VARCHAR(250), Converted BIT)
INSERT INTO #ContractBranchList EXEC [cc].[Get_WebUserContractBranches] @WebUserKey;

   
    IF ( @ExpirationDate IS NOT NULL )
        BEGIN
            BEGIN TRAN

            DECLARE @UpdatedExpirationRowData TABLE
                (
                  ContractKey KEY_NORMAL_TYPE ,
                  StatusKey INT ,
                  UpdateStatusKey INT ,
                  OldExpirationDate DATE ,
                  NewExpirationDate DATE
                )

            UPDATE  sc
            SET     ExpirationDate = @ExpirationDate ,
                    StartDate = CASE WHEN StartDate > @ExpirationDate
                                     THEN @ExpirationDate
                                     ELSE StartDate
                                END ,  --LPF-4602
                    ModifiedByWebUserKey = @WebUserKey ,
                    ContractModificationDate = @modificationDate , --LPF-4601
                    ContractSourceKey = 3
            OUTPUT  INSERTED.ContractKey ,
                    INSERTED.StatusKey ,
                    --INSERTED.UpdateStatusKey ,
					DELETED.statusKey,
                    DELETED.ExpirationDate ,
                    INSERTED.ExpirationDate
                    INTO @UpdatedExpirationRowData
            FROM    cc.SupplierContract sc
                    JOIN DimAccountGroup1 dag1 
						ON dag1.AG1Level1 = sc.ContractOwner
					JOIN #ContractBranchList cbl
						ON sc.ContractOwner COLLATE DATABASE_DEFAULT = cbl.Branch COLLATE DATABASE_DEFAULT
            WHERE   BatchKey = @BatchKey
                    AND ( dag1.Converted = 1
                          OR sc.ContractOwner = 'WUK'
                        )
                    AND sc.StatusKey NOT IN ( 3, 5 )
			
           
		    SET @UpdatedExpirationDateContractCount = @@ROWCOUNT
			
            SELECT  @vendorKey = sc.vendorKey
            FROM    cc.SupplierContract sc
                    INNER JOIN @UpdatedExpirationRowData u ON sc.ContractKey = u.ContractKey
            SELECT  @ContractID = sc.ContractID
            FROM    cc.SupplierContract sc
                    INNER JOIN @UpdatedExpirationRowData u ON sc.ContractKey = u.ContractKey
  
            INSERT  INTO cc.SupplierContractLog
                    ( WebUserKey ,
                      ContractKey ,
                      contractID ,
                      ContractAction ,
                      Notes ,
                      vendorKey ,
                      CreationDate
			        )
                    SELECT distinct @WebUserKey ,
                            u.ContractKey ,
                            @ContractID ,
                            'Bulk Edit Expiry Date ' ,
                            '<' + Convert(VARCHAR(50),u.NewExpirationDate,103) + '>' ,
                            @vendorKey ,
                            @modificationDate
                    FROM    @UpdatedExpirationRowData u
                           



	
	--Check Expiration Dates in SupplierContractAccount, update if later than the new @ExpirationDate
         
		    DECLARE @UpdatedContractAccountRowData TABLE
                (
                  ContractKey INT ,
                  AccountKey INT ,
				  OldExpirationDate DATE ,
                  NewExpirationDate DATE ,
                  OldStartDate DATE ,
                  NewStartDate DATE
                )
	        UPDATE  cc.SupplierContractAccount
            SET     ExpirationDate = @ExpirationDate ,
                    ModifiedOnDT = @modificationDate ,
                    StartDate = ( CASE WHEN StartDate > @ExpirationDate
                                       THEN @ExpirationDate
                                       ELSE StartDate
                                  END )
            OUTPUT  Inserted.ContractKey ,
                    Inserted.AccountKey ,
                    Deleted.ExpirationDate ,
                    Inserted.ExpirationDate ,
                    Deleted.StartDate ,
                    Inserted.StartDate
                    INTO @UpdatedContractAccountRowData
            WHERE   ContractAccountKey IN (
                    SELECT  sca.ContractAccountKey
                    FROM    cc.SupplierContract sc
                            INNER JOIN cc.SupplierContractAccount sca ON sca.ContractKey = sc.ContractKey
                            INNER JOIN DimAccountGroup1 dag1 ON dag1.AG1Level1 = sc.ContractOwner
                    WHERE   sc.BatchKey = @BatchKey
                            AND ( dag1.Converted = 1
                                  OR sc.ContractOwner = 'WUK'
                                )
                            AND sc.StatusKey NOT IN ( 3, 5 )
                            AND sca.ExpirationDate > @ExpirationDate )

            INSERT  INTO cc.SupplierContractLog
                    ( WebUserKey ,
                      ContractKey ,
                      contractID ,
                      ContractAction ,
                      Notes ,
                      vendorKey ,
                      CreationDate
			        )
                    SELECT  @WebUserKey ,
                            u.ContractKey ,
                            sc.ContractID ,
                            'Bulk Edit Account Expiry Date' ,
                            'AccountCode:<' + CAST(u.AccountKey AS VARCHAR(30)) 
                            + '> NewExpiryDate:<'
                            + Convert(VARCHAR(50),u.NewExpirationDate,103) + '>' ,
                            sc.VendorKey ,
                            @modificationDate
                    FROM    @UpdatedContractAccountRowData u
                         --   INNER JOIN Lion.vwAccount va ON u.AccountKey = va.AccountKey
                            INNER JOIN cc.SupplierContract sc ON u.ContractKey = sc.ContractKey


	--Check Expiration Dates in SupplierContractLocation, update if later than the new @ExpirationDate
             DECLARE @UpdatedContractLocationRowDataLater TABLE
                (
                  ContractKey INT ,
                  ContractLocationKey INT ,
				  OldExpirationDate DATE ,
                  NewExpirationDate DATE ,
                  OldStartDate DATE ,
                  NewStartDate DATE
                )
			
			UPDATE  cc.SupplierContractLocation
            SET     ExpirationDate = @ExpirationDate ,
                    ModifiedOnDT = @modificationDate ,
                    StartDate = ( CASE WHEN StartDate > @ExpirationDate
                                       THEN @ExpirationDate
                                       ELSE StartDate
                                  END ),
			 ModifiedByWebUserKey = @WebUserKey
         	 OUTPUT Inserted.ContractKey ,
                    Inserted.ContractLocationKey ,
                    Deleted.ExpirationDate ,
                    Inserted.ExpirationDate ,
                    Deleted.StartDate ,
                    Inserted.StartDate
                    INTO @UpdatedContractLocationRowDataLater
            WHERE   ContractLocationKey IN (
                    SELECT  scl.ContractLocationKey
                    FROM    cc.SupplierContract sc
                            INNER JOIN cc.SupplierContractLocation scl ON scl.ContractKey = sc.ContractKey
                            INNER JOIN DimAccountGroup1 dag1 ON dag1.AG1Level1 = sc.ContractOwner
                    WHERE   sc.BatchKey = @BatchKey
                            AND ( dag1.Converted = 1
                                  OR sc.ContractOwner = 'WUK'
                                )
                            AND sc.StatusKey NOT IN ( 3, 5 )
                            AND scl.ExpirationDate > @ExpirationDate )

			INSERT  INTO cc.SupplierContractLog
                    ( WebUserKey ,
                      ContractKey ,
                      contractID ,
                      ContractAction ,
                      Notes ,
                      vendorKey ,
                      CreationDate
			        )
                    SELECT  @WebUserKey ,
                            u.ContractKey ,
                            sc.ContractID ,
                            'Bulk Edit Location Expiry Date' ,
                            'LocationKey:<' + CAST(u.ContractLocationKey AS VARCHAR(50))
                            + '> NewExpiryDate:<'
                            + Convert(VARCHAR(50),u.NewExpirationDate,103) + '>' ,
                            sc.VendorKey ,
                            @modificationDate
                    FROM    @UpdatedContractLocationRowDataLater u
                         --   INNER JOIN Lion.vwAccount va ON u.AccountKey = va.AccountKey
                            INNER JOIN cc.SupplierContract sc ON u.ContractKey = sc.ContractKey



	--Check Expiration Dates in SupplierContractItem, update if later than the new @ExpirationDate
		DECLARE @UpdatedContractSupplierContractItem TABLE
                (
                  ContractKey INT ,
                  ContractItemKey INT ,
				  OldExpirationDate DATE ,
                  NewExpirationDate DATE ,
                  OldStartDate DATE ,
                  NewStartDate DATE
                )
           
		    UPDATE  cc.SupplierContractItem
            SET     ExpiryDT = @ExpirationDate ,
                    ModifiedOnDT = @modificationDate ,
                    StartDT = ( CASE WHEN StartDT > @ExpirationDate
                                     THEN @ExpirationDate
                                     ELSE StartDT
                                END )

            OUTPUT  Inserted.ContractKey ,
                    Inserted.ContractItemKey ,
                    Deleted.ExpiryDt ,
                    Inserted.ExpiryDt ,
                    Deleted.StartDt ,
                    Inserted.StartDt
                    INTO @UpdatedContractSupplierContractItem

		    WHERE   ContractItemKey IN (
                    SELECT  sci.ContractItemKey
                    FROM    cc.SupplierContract sc
                            INNER JOIN cc.SupplierContractItem sci ON sci.ContractKey = sc.ContractKey
                            INNER JOIN DimAccountGroup1 dag1 ON dag1.AG1Level1 = sc.ContractOwner
                    WHERE   sc.BatchKey = @BatchKey
                            AND ( dag1.Converted = 1
                                  OR sc.ContractOwner = 'WUK'
                                )
                            AND sc.StatusKey NOT IN ( 3, 5 )
                            AND sci.ExpiryDT > @ExpirationDate )


			INSERT  INTO cc.SupplierContractLog
                    ( WebUserKey ,
                      ContractKey ,
                      contractID ,
                      ContractAction ,
                      Notes ,
                      vendorKey ,
                      CreationDate
			        )
                    SELECT  @WebUserKey ,
                            u.ContractKey ,
                            sc.ContractID ,
                            'Bulk Edit Item Expiry Date' ,
                            'ContractItemKey:<' + CAST(u.ContractItemKey AS VARCHAR(50))
                            + '> NewExpiryDate:<'
                           + Convert(VARCHAR(50),u.NewExpirationDate,103) + '>' ,
                            sc.VendorKey ,
                            @modificationDate
                    FROM    @UpdatedContractSupplierContractItem u
                         --   INNER JOIN Lion.vwAccount va ON u.AccountKey = va.AccountKey
                            INNER JOIN cc.SupplierContract sc ON u.ContractKey = sc.ContractKey



	--Check Expiration Dates in SupplierContractItemGroupX, update if later than the new @ExpirationDate
         	DECLARE @UpdatedContractSupplierContractItemGroupX TABLE
                (
                  ContractKey INT ,
                  ContractItemGroupXKey INT ,
				  OldExpirationDate DATE ,
                  NewExpirationDate DATE ,
                  OldStartDate DATE ,
                  NewStartDate DATE
                )
		 
		 
		 
		    UPDATE  cc.SupplierContractItemGroupX
            SET     ExpiryDT = @ExpirationDate ,
                    ModifiedOnDT = @modificationDate ,
                    StartDT = ( CASE WHEN StartDT > @ExpirationDate
                                     THEN @ExpirationDate
                                     ELSE StartDT
                                END )
           
		    OUTPUT  Inserted.ContractKey ,
                    Inserted.ContractItemGroupXKey ,
                    Deleted.ExpiryDt ,
                    Inserted.ExpiryDt ,
                    Deleted.StartDt ,
                    Inserted.StartDt
                    INTO @UpdatedContractSupplierContractItemGroupX

		    WHERE   ContractItemGroupXKey IN (
                    SELECT  scix.ContractItemGroupXKey
                    FROM    cc.SupplierContract sc
                            INNER JOIN cc.SupplierContractItemGroupX scix ON scix.ContractKey = sc.ContractKey
                            INNER JOIN DimAccountGroup1 dag1 ON dag1.AG1Level1 = sc.ContractOwner
                    WHERE   sc.BatchKey = @BatchKey
                            AND ( dag1.Converted = 1
                                  OR sc.ContractOwner = 'WUK'
                                )
                            AND sc.StatusKey NOT IN ( 3, 5 )
                            AND scix.ExpiryDT > @ExpirationDate )

				INSERT  INTO cc.SupplierContractLog
                    ( WebUserKey ,
                      ContractKey ,
                      contractID ,
                      ContractAction ,
                      Notes ,
                      vendorKey ,
                      CreationDate
			        )
                    SELECT  @WebUserKey ,
                            u.ContractKey ,
                            sc.ContractID ,
                            'Bulk Edit Item Expiry Date' ,
                            'ContractItemGroupXKey:<' + CAST(u.ContractItemGroupXKey AS VARCHAR(50))
                            + '> NewExpiryDate:<'
                            + Convert(VARCHAR(50),u.NewExpirationDate,103) + '>' ,
                            sc.VendorKey ,
                            @modificationDate
                    FROM    @UpdatedContractSupplierContractItemGroupX u
                         --   INNER JOIN Lion.vwAccount va ON u.AccountKey = va.AccountKey
                            INNER JOIN cc.SupplierContract sc ON u.ContractKey = sc.ContractKey

            COMMIT TRAN
        END

    IF ( @AccountKeys IS NOT NULL )
        BEGIN
            BEGIN TRAN

	/* Existing contract account combinations */
            SELECT DISTINCT
                    sc.ContractKey ,
                    ca.AccountKey
            INTO    #ExistingContractAccounts
            FROM    cc.SupplierContract sc
                    JOIN DimAccountGroup1 dag1 ON dag1.AG1Level1 = sc.ContractOwner
					JOIN #ContractBranchList cbl ON sc.ContractOwner COLLATE DATABASE_DEFAULT = cbl.Branch COLLATE DATABASE_DEFAULT
                    LEFT OUTER JOIN cc.SupplierContractAccount ca ON ca.ContractKey = sc.ContractKey
            WHERE   BatchKey = @BatchKey
                    AND ( dag1.Converted = 1
                          OR sc.ContractOwner = 'WUK'
                        )
                    AND  -- ask scott if this is right
                    sc.StatusKey NOT IN ( 3, 5 )
                    AND ( sc.IncludeAllAccounts IS NULL
                          OR sc.IncludeAllAccounts = 0
                        )

	--SELECT * FROM #ExistingContractAccounts
			
	/* Possible contract account combinations */
            SELECT DISTINCT
                    ContractKey ,
                    Value AS AccountKey
            INTO    #NewContractAccounts
            FROM    #ExistingContractAccounts ,
                    dbo.fn_ListToTable(@AccountKeys, ',') 

	--SELECT * FROM #NewContractAccounts

            DECLARE @UpdatedContractAccounts TABLE
                (
                  ContractKey INT ,
                  AccountKey INT,
				  CreatedByWebUserKey INT,
				  CreatedOnDT DATETIME
                )

	/* find contracts that need insert */
            INSERT  INTO cc.SupplierContractAccount
                    ( ContractKey ,
                      AccountKey ,
                      CreatedByWebUserKey ,
                      CreatedOnDT ,
                      ModifiedByWebUserKey ,
                      ModifiedOnDT
                    )
            OUTPUT  INSERTED.ContractKey,
                    INSERTED.AccountKey,
					INSERTED.CreatedByWebUserKey,
					INSERTED.CreatedOnDT
                    INTO @UpdatedContractAccounts
             SELECT DISTINCT
                            new.ContractKey ,
                            new.AccountKey ,
                            @webuserkey ,
                            @modificationDate ,
                            @webuserkey ,
                            @modificationDate
                    FROM    #NewContractAccounts new
                            LEFT OUTER JOIN #ExistingContractAccounts ex ON new.ContractKey = ex.ContractKey
                                                              AND new.AccountKey = ex.AccountKey
                    WHERE   ex.AccountKey IS NULL

	/* Set ContractSourceKey = 3  */
            UPDATE  sc
            SET     ContractSourceKey = 3,
					ModifiedByWebUserKey = @WebUserKey ,
                    ContractModificationDate = @modificationDate
			FROM    cc.SupplierContract sc
                    INNER JOIN @UpdatedContractAccounts u ON u.ContractKey = sc.ContractKey


            SET @AddedContractAccountCount = @@ROWCOUNT
            SELECT  @UpdatedAccountContractCount = COUNT(DISTINCT ( ContractKey ))
            FROM    @UpdatedContractAccounts

--5987 Bulk Edit Logging - Incorrect logging of contract level bulk edit of Add Account.
			INSERT  INTO cc.SupplierContractLog
                    ( WebUserKey ,
                      ContractKey ,
                      contractID ,
                      ContractAction ,
                      Notes ,
                      vendorKey ,
                      CreationDate
			        )
			SELECT DISTINCT @WebUserKey ,
                            rowdata.ContractKey ,
                            sc.ContractID ,
                            'Bulk Edit Add Account' ,
                            'New AccountID:<' + CAST(da.AccountNumber AS VARCHAR(50)) + '>' + 'AccountDescription:<' + da.AccountName + '>',
                            sc.VendorKey ,
                            @modificationDate
			FROM @UpdatedContractAccounts rowdata
			JOIN cc.SupplierContract sc ON
				sc.ContractKey = rowdata.ContractKey
			JOIN cc.SupplierContractAccount ca ON
				ca.ContractKey = sc.ContractKey
			JOIN DimAccount da ON
				da.AccountKey = rowdata.AccountKey

            DROP TABLE #ExistingContractAccounts
            DROP TABLE #NewContractAccounts

            COMMIT TRAN
        END

	DROP TABLE #ContractBranchList

    IF @UpdatedExpirationDateContractCount = 0
        AND @UpdatedAccountContractCount = 0
        BEGIN 
            SELECT  NULL AS ContractKey ,
                    NULL AS StatusKey ,
                    @ContractBatchCount AS BatchContractCount ,
                    @UpdatedExpirationDateContractCount AS UpdatedExpirationDateContractCount ,
                    @UpdatedAccountContractCount UpdatedAccountContractCount
        END

    ELSE
        SELECT  ContractKey ,
                StatusKey ,
                @ContractBatchCount AS BatchContractCount ,
                @UpdatedExpirationDateContractCount AS UpdatedExpirationDateContractCount ,
                @UpdatedAccountContractCount UpdatedAccountContractCount
        FROM    @UpdatedExpirationRowData
        UNION
        SELECT  u.ContractKey ,
                sc.StatusKey ,
                @ContractBatchCount AS BatchContractCount ,
                @UpdatedExpirationDateContractCount AS UpdatedExpirationDateContractCount ,
                @UpdatedAccountContractCount UpdatedAccountContractCount
        FROM    @UpdatedContractAccounts u
                INNER JOIN cc.SupplierContract sc ON sc.ContractKey = u.ContractKey
GO
