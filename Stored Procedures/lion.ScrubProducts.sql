SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [lion].[ScrubProducts] 
AS
BEGIN
	SET NOCOUNT ON
	

	EXEC LogDCPEvent N'ETL - ScrubProducts: HireInd = Y', N'B'
	UPDATE s
	SET
		s.ETLRowStatusCodeKey = 6
	FROM
		Lion_Staging.dbo.Product s
	WHERE
		HireInd = N'Y'
	EXEC LogDCPEvent N'ETL - ScrubProducts: HireInd = Y', N'E', 0, @@ROWCOUNT, 0

	EXEC LogDCPEvent N'ETL - ScrubProducts: LLSPGCode is HZ98', N'B'
	UPDATE s
	SET
		s.ETLRowStatusCodeKey = 7
	FROM
		Lion_Staging.dbo.Product s
	WHERE
		LLSPGCode = N'HZ98'
	EXEC LogDCPEvent N'ETL - ScrubProducts: LLSPGCode is HZ98', N'E', 0, @@ROWCOUNT, 0

	EXEC LogDCPEvent N'ETL - ScrubProducts: GSPGCode is Z', N'B'
	UPDATE s
	SET
		s.ETLRowStatusCodeKey = 8
	FROM
		Lion_Staging.dbo.Product s
	WHERE
		GSPGCode = N'Z'
	EXEC LogDCPEvent N'ETL - ScrubProducts: GSPGCode is Z', N'E', 0, @@ROWCOUNT, 0


END
GO
