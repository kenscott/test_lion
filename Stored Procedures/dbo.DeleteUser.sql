SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[DeleteUser] (@ODSPersonKey Key_Normal_type)
AS

/*

BEGIN TRAN

exec dbo.DeleteUser 1800


ROLLBACK TRAN
*/

SET NOCOUNT ON


DECLARE
	@WebUserKey Key_Normal_type,
	@DimPersonKey Key_Normal_type,
	@AccountManagerKey Key_Normal_type

SET @WebUserKey = NULL
SET @DimPersonKey = NULL
SET @AccountManagerKey = NULL



SELECT
	@WebUserKey = wu.WebUserKey,
	@AccountManagerKey = wu.AccountManagerKey
FROM dbo.WebUser wu
WHERE wu.ODSPersonKey = @ODSPersonKey


SELECT
	@DimPersonKey = DimPersonKey
FROM dbo.DimPerson
WHERE ODSPersonKey = @ODSPersonKey


IF @AccountManagerKey IS NULL AND @DimPersonKey IS NOT NULL
BEGIN
	SELECT
		@AccountManagerKey = AccountManagerKey
	FROM dbo.DimAccountManager
	WHERE DimPersonKey = @DimPersonKey
END


--SELECT '@WebUserKey', @WebUserKey
--SELECT '@DimPersonKey', @DimPersonKey
--SELECT '@AccountManagerKey', @AccountManagerKey

DECLARE @MsgStr VARCHAR(MAX)
SELECT @MsgStr = 'Deleteing User - ODSPersonKey: ' + ISNULL(CAST(@ODSPersonKey AS VARCHAR(25)), 'NIL')
	+ ', WebUserKey: ' + ISNULL(CAST(@WebUserKey AS VARCHAR(25)) , 'NIL')
	+ ', DimPersonKey: ' + ISNULL(CAST(@DimPersonKey AS VARCHAR(25)) , 'NIL')
	+ ', AccountManagerKey: ' + ISNULL(CAST(@AccountManagerKey AS VARCHAR(25)) , 'NIL')

EXEC LogDCPEvent @MsgStr, 'B', 0, 0, 0


IF @WebUserKey IS NOT NULL
BEGIN

	DELETE FROM dbo.WebUserRole WHERE WebUserKey = @WebUserKey
	DELETE FROM dbo.WebUserPermission WHERE WebUserKey = @WebUserKey
	DELETE FROM dbo.WebUserView WHERE ViewerWebUserKey = @WebUserKey OR ViewedWebUserKey  = @WebUserKey
	DELETE FROM dbo.WebUserDelegate WHERE DelegatorWebUserKey = @WebUserKey OR DelegateWebUserKey = @WebUserKey
	DELETE FROM dbo.WebUserAccountManager WHERE WebUserKey = @WebUserKey
	DELETE FROM dbo.WebUserAccountManagerAccessType WHERE WebUserKey = @WebUserKey

	DELETE FROM dbo.AuditLog WHERE WebUserKey = @WebUserKey

END


IF @AccountManagerKey IS NOT NULL AND @AccountManagerKey <> 1
BEGIN
 
	DELETE FROM dbo.WebUserAccountManager WHERE AccountManagerKey = @AccountManagerKey
	DELETE FROM dbo.WebUserAccountManagerAccessType WHERE AccountManagerKey = @AccountManagerKey
	DELETE FROM dbo.BridgeAccountManager WHERE ParentAccountManagerKey = @AccountManagerKey OR SubsidiaryAccountManagerKey = @AccountManagerKey

END 


IF @ODSPersonKey IS NOT NULL 
BEGIN

	DELETE FROM dbo.ODSPerson WHERE ODSPersonKey = @ODSPersonKey
	DELETE FROM dbo.DimPerson WHERE ODSPersonKey = @ODSPersonKey

END 


IF @WebUserKey IS NOT NULL
BEGIN

	DELETE FROM dbo.WebUserRole WHERE WebUserKey = @WebUserKey

	DELETE FROM dbo.WebUserPermission WHERE WebUserKey = @WebUserKey
	DELETE FROM dbo.WebUserView WHERE ViewerWebUserKey = @WebUserKey OR ViewedWebUserKey = @WebUserKey
	DELETE FROM dbo.WebUserDelegate WHERE DelegatorWebUserKey = @WebUserKey OR DelegateWebUserKey = @WebUserKey

	--DELETE FROM dbo.WebUserAccountManager WHERE WebUserKey = @WebUserKey OR (AccountManagerKey = @AccountManagerKey AND @AccountManagerKey IS NOT NULL)
	--DELETE FROM dbo.WebUserAccountManagerAccessType WHERE WebUserKey = @WebUserKey OR (AccountManagerKey = @AccountManagerKey AND @AccountManagerKey IS NOT NULL)

	DELETE FROM dbo.WebUserSetting WHERE WebUserKey = @WebUserKey
	DELETE FROM dbo.WebUserPasswordHistory WHERE WebUserKey = @WebUserKey
	--DELETE FROM WebUserFilter WHERE WebUserKey = @WebUserKey
	DELETE FROM dbo.WebUser WHERE WebUserKey = @WebUserKey

END


IF @AccountManagerKey IS NOT NULL AND @AccountManagerKey <> 1
BEGIN 
	DELETE FROM dbo.DimAccountManager WHERE AccountManagerKey = @AccountManagerKey
END

EXEC LogDCPEvent @MsgStr, 'E', 0, 0, 0
GO
