SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE
PROCEDURE [lion].[GetRPBPriceEnvelopeBulkXML]
	@ParamParameterList NVARCHAR(MAX)

WITH EXECUTE AS CALLER
AS

/*


DECLARE @ParamString NVARCHAR(4000)

SET @ParamString = N'
<ROOT>
	<Parameters>
		<BranchCode>TB</BranchCode>
		<AccountId>7000B12</AccountId>
		<PyramidNumber>1</PyramidNumber>
		<ProductCode>100195</ProductCode>
		<PriceDerivationCode>ADJ</PriceDerivationCode>
		<ShipmentType>S</ShipmentType>
		<DeviatedIndicator>N</DeviatedIndicator>
		<ItemCost>787.351</ItemCost>
	</Parameters>
	<Parameters>
		<BranchCode>TB</BranchCode>
		<AccountId>7000B12</AccountId>
		<PyramidNumber>1</PyramidNumber>
		<ProductCode>100196</ProductCode>
		<PriceDerivationCode>ADJ</PriceDerivationCode>
		<ShipmentType></ShipmentType>
		<DeviatedIndicator></DeviatedIndicator>
		<ItemCost></ItemCost>
	</Parameters>
	<Parameters>
		<BranchCode>TB</BranchCode>
		<AccountId>7000B12</AccountId>
		<PyramidNumber>1</PyramidNumber>
		<ProductCode>100197</ProductCode>
		<PriceDerivationCode>ADJ</PriceDerivationCode>
		<ShipmentType>S</ShipmentType>
		<DeviatedIndicator>N</DeviatedIndicator>
		<ItemCost>10.007</ItemCost>
	</Parameters>
</ROOT>
'

EXEC lion.GetRPBPriceEnvelopeBulkXML @ParamString


*/

SET NOCOUNT ON;


DECLARE
	@ScenarioKey Key_Normal_type,
	@PlaybookDataPointGroupKey Key_Normal_type,
	@ProjectKey Key_Normal_type,
	@AttributeBasedXMLList XML;


/*** Table for holding parameters and results ***/
CREATE TABLE #PGTable (
	RecordId INT IDENTITY,
	ParamBranchCode NVARCHAR(21) COLLATE SQL_Latin1_General_CP1_CI_AS,
	ParamAccountId NVARCHAR(21) COLLATE SQL_Latin1_General_CP1_CI_AS,
	ParamPyramidNumber NVARCHAR(1) COLLATE SQL_Latin1_General_CP1_CI_AS,
	ParamProductCode NVARCHAR(100) COLLATE SQL_Latin1_General_CP1_CI_AS,
	ParamPriceDerivationCode NVARCHAR(8) COLLATE SQL_Latin1_General_CP1_CI_AS,
	ParamShipmentType NVARCHAR(1) COLLATE SQL_Latin1_General_CP1_CI_AS,
	ParamDeviatedIndicator NVARCHAR(1) COLLATE SQL_Latin1_General_CP1_CI_AS,
	ParamItemCost NVARCHAR(100) COLLATE SQL_Latin1_General_CP1_CI_AS,

	ItemCostDecimal DEC(19,3) NULL,
	AccountKey INT NULL,
	ItemKey INT NULL,
	InvoiceLineGroup1Key INT NULL,

	ShipmentType NVARCHAR(1) COLLATE SQL_Latin1_General_CP1_CI_AS,
	DeviatedIndicator NVARCHAR(1) COLLATE SQL_Latin1_General_CP1_CI_AS,
	PriceDerivationCode NVARCHAR(8) COLLATE SQL_Latin1_General_CP1_CI_AS,

	ParamItemCostConverted DEC(38,3) NULL,
	PricingConvFactor DEC(19,3) NULL,
	SellingConvFactor DEC(19,3) NULL,

	PlaybookPricingGroupKey INT NULL,

	FloorPercentile DEC(19,8) NULL,
	TargetPercentile DEC(19,8) NULL,
	StretchPercentile DEC(19,8) NULL,
	Offset DEC(19,8) NULL,
	
	FloorGPP DEC(19,8) NULL,
	TargetGPP DEC(19,8) NULL,
	StretchGPP DEC(19,8) NULL,

	FloorPrice DEC(19,8) NULL,
	TargetPrice DEC(19,8) NULL,
	StretchPrice DEC(19,8) NULL,

	LastMarginPrice DECIMAL(19,3) NULL,
	
	LastItemPrice DEC(19,3) NULL,
	LastItemCost DEC(19,3) NULL,
	LastSalesDate DATE NULL,
		
	RedPrice DEC(19,3) NULL,
	AmberPrice DEC(19,3) NULL,
	GreenPrice DEC(19,3) NULL,
	
	RedGPP DEC(19,2) NULL,
	AmberGPP DEC(19,2) NULL,
	GreenGPP DEC(19,2) NULL,

	TradePrice DEC(19,3) NULL,

	ProductBrand NVARCHAR(250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,

	Branch NVARCHAR(250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	Region NVARCHAR(250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	Area NVARCHAR(250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	Network NVARCHAR(250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,

	LLSPGCode NVARCHAR(250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	ItemPyramidCode NVARCHAR(250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	
	ItemNumber NVARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,--- in xml params?

	Sales603010Bucket NVARCHAR(250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	CustomerType NVARCHAR(250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	PriceApproach NVARCHAR(250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	LastPriceApproach NVARCHAR(250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	InvoiceLineGroup2Key INT NULL
);



/*** Shred the XML to the the paramaters ***/

SELECT @AttributeBasedXMLList = CAST(@ParamParameterList AS XML);

INSERT INTO #PGTable
(
    ParamBranchCode,
    ParamAccountId,
    ParamPyramidNumber,
    ParamProductCode,
    ParamPriceDerivationCode,
    ParamShipmentType,
    ParamDeviatedIndicator,
    ParamItemCost
)
SELECT 
	 ParamBranchCode = t.c.value('BranchCode[1]','NVARCHAR(21)')
	,ParamAccountID=t.c.value('AccountId[1]','NVARCHAR(21)')
	,ParamPyramidNumber=t.c.value('PyramidNumber[1]','NVARCHAR(1)')
	,ParamProductCode=t.c.value('ProductCode[1]','NVARCHAR(100)')
	,ParamPriceDerivationCode=t.c.value('PriceDerivationCode[1]','NVARCHAR(8)')
	,ParamShipmentType=t.c.value('ShipmentType[1]','NVARCHAR(1)')
	,ParamDeviatedIndicator=t.c.value('DeviatedIndicator[1]','NVARCHAR(1)')
	,ParamItemCost=t.c.value('ItemCost[1]','NVARCHAR(100)')
FROM @AttributeBasedXMLList.nodes('/ROOT/Parameters') t(c);


/*** Get All Needed Attributes ***/



/* Account related attributes */
UPDATE pt
SET
	AccountKey = da.AccountKey,
	CustomerType = ISNULL(AG2Level1, N''),
	-- set default, will pull the real value from last sales data if available
	Sales603010Bucket = 
		CASE
			WHEN ISNULL(AG2Level1, N'') = N'CS' THEN N'Small/Cash'
			WHEN ISNULL(AccountUDVarChar8, N'') = N'National' THEN N'National'
			WHEN ISNULL(AccountUDVarChar8, N'') = N'Sales Team' THEN N'Sales Team'
			ELSE N'Small/Cash'
		END,
	-- Iteration 2 default...
	PriceApproach = 
		CASE
			WHEN ISNULL(AG2Level1, N'') = N'CS' THEN N'Cash'
			WHEN ISNULL(AccountUDVarChar8, N'') = N'National' THEN N'National'
			WHEN ISNULL(AccountUDVarChar8, N'') = N'Sales Team' THEN N'LargeSalesTeam'
			ELSE N'SmallMedium'
		END
FROM #PGTable pt
LEFT JOIN dbo.DimAccount da WITH (NOLOCK)
	ON da.AccountNumber = pt.ParamAccountId
LEFT JOIN dbo.DimAccountGroup1 dag1 WITH (NOLOCK)
	ON dag1.AccountGroup1Key = da.AccountGroup1Key
LEFT JOIN dbo.DimAccountGroup2 dag2 WITH (NOLOCK)
	ON dag2.AccountGroup2Key = da.AccountGroup2Key;


/* Branch related attributes */
UPDATE pt
SET
	Branch  = AG1Level1,
	Area    = AG1Level3,
	Region  = AG1Level4,
	Network = AG1Level2
FROM #PGTable pt
LEFT JOIN dbo.DimAccountGroup1 dag1 (NOLOCK)
	ON 	LTRIM(RTRIM(pt.ParamBranchCode)) = AG1Level1;


/* Item/Product related attributes */
UPDATE pt
SET
	ItemKey = di.ItemKey,
	ItemNumber = di.ItemNumber,
	TradePrice	= ItemUDDecimal1,
	LLSPGCode = IG3Level1,
	ItemPyramidCode = IG3Level4,
	ProductBrand = ItemUDVarChar2,
	ParamItemCostConverted = CAST(NULLIF(pt.ParamItemCost, N'') AS DEC(19,3)) * (ISNULL(uc.SellingConvFactor, 1)/ISNULL(uc.CostingConvFactor, 1)),
	PricingConvFactor = ISNULL(uc.PricingConvFact, 1),
	SellingConvFactor = ISNULL(uc.SellingConvFactor, 1),
	ItemCostDecimal = CAST(NULLIF(pt.ParamItemCost, N'') AS DEC(19,3))
FROM #PGTable pt
LEFT JOIN dbo.DimItem di WITH (NOLOCK)
	ON di.ItemNumber = pt.ParamPyramidNumber + pt.ParamProductCode
LEFT JOIN dbo.DimItemGroup3 dig3 WITH (NOLOCK)
	ON dig3.ItemGroup3Key = di.ItemGroup3Key
LEFT JOIN dbo.ItemUOMConversion uc WITH (NOLOCK)
	ON uc.ItemKey = di.ItemKey;


/* Shipment type and Deviated indicator attributes */
UPDATE pt
SET
	InvoiceLineGroup1Key = dilg1.InvoiceLineGroup1Key,	
	ShipmentType = dilg1.InvoiceLineGroup1UDVarchar1,
	DeviatedIndicator = dilg1.InvoiceLineGroup1UDVarchar2
FROM #PGTable pt
LEFT JOIN dbo.DimInvoiceLineGroup1 dilg1 WITH (NOLOCK)
	ON dilg1.InvoiceLineGroup1UDVarchar1 = ISNULL(pt.ParamShipmentType, N'S')
	AND dilg1.InvoiceLineGroup1UDVarchar2 = ISNULL(pt.ParamDeviatedIndicator, N'N');


/* Last/AISD related attributes */
UPDATE pt
SET 
	PlaybookPricingGroupKey = RPBPlaybookPricingGroupKey,
	LastItemPrice = flpac.LastItemPrice,
	LastItemCost = flpac.LastItemCost,
	LastSalesDate = dd.SQLDate,
	Sales603010Bucket = UDVarchar4,
	LastPriceApproach = UDVarchar10
FROM #PGTable pt
INNER JOIN dbo.FactLastPriceAndCost flpac WITH (NOLOCK)
	ON 	flpac.AccountKey = pt.AccountKey
	AND flpac.ItemKey = pt.ItemKey
	AND flpac.InvoiceLineGroup1Key = pt.InvoiceLineGroup1Key
INNER JOIN dbo.DimDay dd WITH (NOLOCK)
	ON dd.DayKey = flpac.LastSaleDayKey;


/* Price Approach attribute */
UPDATE pt
SET PriceApproach = COALESCE(pt.LastPriceApproach, pt.PriceApproach, N'SmallMedium') -- vs. Cash, SmallMedium, LargeSalesTeam, National
FROM #PGTable pt;


/* Price Derivation Code (PDC) attribute */
UPDATE pt
SET 
	InvoiceLineGroup2Key = dilg2.InvoiceLineGroup2Key,
	PriceDerivationCode = dilg2.InvoiceLineGroup2UDVarchar1
FROM #PGTable pt
LEFT JOIN dbo.DimInvoiceLineGroup2 dilg2 WITH (NOLOCK)
	ON dilg2.InvoiceLineGroup2UDVarchar1 = pt.ParamPriceDerivationCode;


/*** Check for Manual Price Band (MPB) ***/
UPDATE pt
SET
	FloorGPP   = MPBFloorGPP,
	TargetGPP  = MPBTargetGPP,
	StretchGPP = MPBStretchGPP
FROM #PGTable pt
OUTER APPLY lion.fn_GetManualPriceBand (
		pt.ItemPyramidCode,
		pt.LLSPGCode,
		pt.ItemNumber,
		pt.PriceApproach,
		pt.DeviatedIndicator,
		pt.Region,
		pt.Area,
		pt.Network,
		pt.Branch
	);



/*** Get playbook keys ***/
EXEC dbo.GetRPBScenarioKeys 
	@ScenarioKey OUTPUT, 
	@PlaybookDataPointGroupKey OUTPUT, 
	@ProjectKey OUTPUT;


/*** Find and set the PlaybookPricingGroupKey values (for any rows that did not get a key from FactLastPriceAndCost ***/
EXEC lion.Load_SetRPBPlaybookPricingGroupKey 
	N'#PGTable', 
	N'PlaybookPricingGroupKey', 
	NULL, 
	NULL, 
	@ScenarioKey, 
	@PlaybookDataPointGroupKey, 
	N'N';


/* FTS GPP Values */
UPDATE pt
SET 
	pt.FloorGPP   = a.FloorGPP,
	pt.TargetGPP  = a.TargetGPP,	
	pt.StretchGPP = a.StretchGPP,
	pt.Offset     = p.Offset
FROM #PGTable pt
OUTER APPLY lion.fn_GetPercentile (
		ProductBrand,			-- product brand
		Region,					-- Region,
		Area,					-- Area,
		Network,				-- Network
		Branch,					-- Branch,
		Sales603010Bucket,		-- Sales603010Bucket
		ItemPyramidCode,		-- ItemPyramidCode
		LLSPGCode				-- LLSPGCode
	) p
OUTER APPLY lion.fn_GetRPBPercentagesForPG (
		PlaybookPricingGroupKey,
		NULL, -- Total12MonthQuantity,  -- lion does not currently have quantity bands so null works here
		p.FloorPercentile,
		p.TargetPercentile,
		p.StretchPercentile,
		p.Offset
	) a
WHERE
	PlaybookPricingGroupKey IS NOT NULL;


/* RAG GPP Values */
UPDATE pt
SET 
	pt.RedGPP   = FloorGPP   * 100.,
	pt.AmberGPP = TargetGPP  * 100.,	
	pt.GreenGPP = StretchGPP * 100.
FROM #PGTable pt;


/* RAG Price Values */
UPDATE pt
SET 
	RedPrice   = m.RedPrice,
	AmberPrice = m.AmberPrice,
	GreenPrice = m.GreenPrice
FROM #PGTable pt
OUTER APPLY lion.fn_GetTradePriceMatchedRAGPrices (
		TradePrice,
		ParamItemCostConverted,
		FloorGPP,
		TargetGPP,
		StretchGPP,
		1,	--@PricingConvFactor,
		1	--@SellingConvFactor
	) m
WHERE
	ISNULL(pt.ParamItemCostConverted, 0.) > 0. AND pt.FloorGPP IS NOT NULL;


/* recalc gpp; truncate to 4 digits */
UPDATE pt
SET 
	RedGPP   = ROUND(((RedPrice   - ParamItemCostConverted) / RedPrice)   , 4, 1) * 100.,
	AmberGPP = ROUND(((AmberPrice - ParamItemCostConverted) / AmberPrice) , 4, 1) * 100.,
	GreenGPP = ROUND(((GreenPrice - ParamItemCostConverted) / GreenPrice) , 4, 1) * 100.
FROM #PGTable pt
WHERE
	ISNULL(pt.ParamItemCostConverted, 0.) > 0.;



/* LastMarginPrice */
UPDATE pt
SET 
	LastMarginPrice = 
		CAST(ROUND(
				CASE
					WHEN 
						ISNULL(ItemCostDecimal, 0.) > 0.
						AND ISNULL(LastItemPrice, 0.) > 0.
						THEN ParamItemCostConverted / NULLIF((1.0 -  ((LastItemPrice-LastItemCost)/LastItemPrice)), 0.)
					ELSE NULL
				END,
				3) AS DECIMAL(19,3))
FROM #PGTable pt
WHERE
	ISNULL(pt.ParamItemCostConverted, 0.) > 0.;


/* "Lasts" */
UPDATE pt
SET 
	LastMarginPrice = 
						CASE 
							WHEN LastMarginPrice IS NOT NULL AND LastMarginPrice <= RedPrice THEN NULL 
							ELSE LastMarginPrice 
						END,
	LastSalesDate   = 
						CASE 
							WHEN LastMarginPrice IS NOT NULL AND LastMarginPrice <= RedPrice THEN NULL 
							ELSE 
			 					CASE 
									WHEN CustomerType = N'CS' OR DATEDIFF(MONTH, LastSalesDate, GETDATE()) > 3 THEN NULL
									WHEN NOT (ISNULL(ItemCostDecimal, 0.) > 0. AND ISNULL(LastItemPrice, 0.) > 0.) THEN NULL
								ELSE LastSalesDate 
							END
					END
FROM #PGTable pt
WHERE
	ISNULL(pt.ParamItemCostConverted, 0.) > 0.;


/* Now, re-state the prices in the "unit" of the passed in cost */
UPDATE pt
SET 
		RedPrice   = RedPrice   * (PricingConvFactor / SellingConvFactor),
		AmberPrice = AmberPrice * (PricingConvFactor / SellingConvFactor),
		GreenPrice = GreenPrice * (PricingConvFactor / SellingConvFactor),
		LastMarginPrice = LastMarginPrice * (PricingConvFactor / SellingConvFactor)
FROM #PGTable pt
WHERE
	ISNULL(pt.ParamItemCostConverted, 0.) > 0.;




/*** Return the results ***/
SELECT 
	CASE
		-- 2 is supposed to be for pyrmaid code but we decided not to check this outside of item number / product code
		WHEN Branch IS NULL THEN 3											-- branch code
		WHEN ItemKey IS NULL THEN 4											-- item/product
		WHEN AccountKey IS NULL THEN 5										-- account/customer
		WHEN ParamItemCost IS NOT NULL AND ItemCostDecimal <= 0. THEN 6			-- bad cost supplied
		WHEN InvoiceLineGroup2Key IS NULL THEN 7							-- bad price derivation code supplied
		WHEN PlaybookPricingGroupKey IS NULL AND TargetGPP IS NULL THEN 1
		ELSE 0
	END AS ReturnCode,
	ParamProductCode AS ProductCode,
	ParamPyramidNumber AS PyramidCode,
	LastMarginPrice,
	LastSalesDate,
	RedGPP,
	AmberGPP,
	GreenGPP,
	RedPrice,
	AmberPrice,
	GreenPrice
FROM 
	#PGTable
ORDER BY 
	RecordId;




DROP TABLE #PGTable


/*
Below is the old, reliable but slow way:

SET NOCOUNT ON

DECLARE
	@ParamRecId INT,
	@ParamBranchCode NVARCHAR(21),
	@ParamAccountId NVARCHAR(21),
	@ParamPyramidNumber NVARCHAR(1),
	@ParamProductCode NVARCHAR(100),
	@ParamPriceDerivationCode NVARCHAR(8),
	@ParamShipmentType NVARCHAR(1) = 'S',
	@ParamDeviatedIndicator NVARCHAR(1) = 'N',
	@ParamItemCost NVARCHAR(100)

DECLARE @AttributeBasedXMLList XML

SELECT @AttributeBasedXMLList = CAST(@ParamParameterList AS XML)

DECLARE @ParameterTable TABLE (
	Recid INT IDENTITY,
	BranchCode NVARCHAR(21),
	AccountId NVARCHAR(21),
	PyramidNumber NVARCHAR(1),
	ProductCode NVARCHAR(100),
	PriceDerivationCode NVARCHAR(8),
	ShipmentType NVARCHAR(1),
	DeviatedIndicator NVARCHAR(1),
	ItemCost NVARCHAR(100)
)

INSERT INTO @ParameterTable
(
    BranchCode,
    AccountId,
    PyramidNumber,
    ProductCode,
    PriceDerivationCode,
    ShipmentType,
    DeviatedIndicator,
    ItemCost
)
SELECT 
	BranchCode = t.c.value('BranchCode[1]','NVARCHAR(21)')
	,AccountID=t.c.value('AccountId[1]','NVARCHAR(21)')
	,PyramidNumber=t.c.value('PyramidNumber[1]','NVARCHAR(1)')
	,ProductCode=t.c.value('ProductCode[1]','NVARCHAR(100)')
	,PriceDerivationCode=t.c.value('PriceDerivationCode[1]','NVARCHAR(8)')
	,ShipmentType=t.c.value('ShipmentType[1]','NVARCHAR(1)')
	,DeviatedIndicator=t.c.value('DeviatedIndicator[1]','NVARCHAR(1)')
	,ItemCost=t.c.value('ItemCost[1]','NVARCHAR(100)')
FROM @AttributeBasedXMLList.nodes('/ROOT/Parameters') t(c)


DECLARE @FinalResults TABLE (
	Recid INT,
	BranchCode NVARCHAR(21),
	AccountId NVARCHAR(21),
	PyramidNumber NVARCHAR(1),
	ProductCode NVARCHAR(100),
	PriceDerivationCode NVARCHAR(8),
	ShipmentType NVARCHAR(1),
	DeviatedIndicator NVARCHAR(1),
	ItemCost NVARCHAR(100),

	PlaybookPricingGroupKey INT,
	ReturnCode INT,
	GUID UNIQUEIDENTIFIER,
	LastMarginPrice DECIMAL(19,3),
	LastSaleDate DATE,
	CustomerRebate NVARCHAR(250),
	RedGPP DEC(19,2),
	AmberGPP DEC(19,2),
	GreenGPP DEC(19,2),
	RedPrice DEC(19,3),
	AmberPrice DEC(19,3),
	GreenPrice DEC(19,3)
)


DECLARE @EnvelopeResults TABLE (
	PlaybookPricingGroupKey INT,
	ReturnCode INT,
	GUID UNIQUEIDENTIFIER,
	LastMarginPrice DECIMAL(19,3),
	LastSaleDate DATE,
	CustomerRebate NVARCHAR(250),
	RedGPP DEC(19,2),
	AmberGPP DEC(19,2),
	GreenGPP DEC(19,2),
	RedPrice DEC(19,3),
	AmberPrice DEC(19,3),
	GreenPrice DEC(19,3)
)


DECLARE ParamList 
CURSOR 
FAST_FORWARD 
FOR

	SELECT
		Recid,
		BranchCode,
		AccountId,
		PyramidNumber,
		ProductCode,
		PriceDerivationCode,
		ShipmentType,
		DeviatedIndicator,
		NULLIF(ItemCost, '')
	FROM
		@ParameterTable

OPEN ParamList

FETCH NEXT FROM ParamList INTO 
	@ParamRecId,
	@ParamBranchCode,
	@ParamAccountId,
	@ParamPyramidNumber,
	@ParamProductCode,
	@ParamPriceDerivationCode,
	@ParamShipmentType,
	@ParamDeviatedIndicator,
	@ParamItemCost

WHILE (@@FETCH_STATUS <> -1)
BEGIN

	INSERT INTO @EnvelopeResults
	EXEC lion.GetRPBPriceEnvelope
		@ParamBranchCode,
		@ParamAccountId,
		@ParamPyramidNumber,
		@ParamProductCode,
		@ParamPriceDerivationCode,
		@ParamShipmentType,
		@ParamDeviatedIndicator,
		@ParamItemCost

	INSERT INTO @FinalResults (
		Recid,
		BranchCode,
		AccountId,
		PyramidNumber,
		ProductCode,
		PriceDerivationCode,
		ShipmentType,
		DeviatedIndicator,
		ItemCost,

		PlaybookPricingGroupKey,
		ReturnCode,
		GUID,
		LastMarginPrice,
		LastSaleDate,
		CustomerRebate,
		RedGPP,
		AmberGPP,
		GreenGPP,
		RedPrice,
		AmberPrice,
		GreenPrice
		)
	SELECT
		@ParamRecId,
		@ParamBranchCode,
		@ParamAccountId,
		@ParamPyramidNumber,
		@ParamProductCode,
		@ParamPriceDerivationCode,
		@ParamShipmentType,
		@ParamDeviatedIndicator,
		@ParamItemCost,

		PlaybookPricingGroupKey,
		ReturnCode,
		GUID,
		LastMarginPrice,
		LastSaleDate,
		CustomerRebate,
		RedGPP,
		AmberGPP,
		GreenGPP,
		RedPrice,
		AmberPrice,
		GreenPrice
	FROM @EnvelopeResults

	DELETE FROM @EnvelopeResults

	FETCH NEXT FROM ParamList INTO 
		@ParamRecId,
		@ParamBranchCode,
		@ParamAccountId,
		@ParamPyramidNumber,
		@ParamProductCode,
		@ParamPriceDerivationCode,
		@ParamShipmentType,
		@ParamDeviatedIndicator,
		@ParamItemCost

END

CLOSE ParamList
DEALLOCATE ParamList

SELECT 
	ReturnCode,
	ProductCode,
	PyramidNumber AS PyramidCode,
	LastMarginPrice,
	LastSaleDate,
	RedGPP,
	AmberGPP,
	GreenGPP,
	RedPrice,
	AmberPrice,
	GreenPrice
FROM @FinalResults
ORDER BY Recid

*/





GO
