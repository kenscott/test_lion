SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE [lion].[GetPriceRules]
AS

SELECT DISTINCT
	p.*
FROM dbo.PlaybookPricingGroup (NOLOCK) p
INNER JOIN dbo.FactLastPriceAndCost (NOLOCK) l
	ON p.PlaybookPricingGroupKey = l.RPBPlaybookPricingGroupKey
ORDER BY 1


GO
