SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO










--RBP_CalculateAccountItemBands 136, 1


CREATE           
PROCEDURE [dbo].[RBP_CalculateAccountItemBands]
(
@PlaybookDataPointGroupKey INT,
@SegmentFactorPlaybookKey INT

)

AS
SET QUOTED_IDENTIFIER ON 
SET ANSI_NULLS ON 
SET ARITHABORT ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
SET CONCAT_NULL_YIELDS_NULL ON
SET NUMERIC_ROUNDABORT OFF

DECLARE @RowsI Quantity_Normal_type, @RowsU Quantity_Normal_type, @RowsD Quantity_Normal_type

DECLARE @ProjectKey key_normal_type
SELECT @ProjectKey = ProjectKey FROM PlaybookDataPointGroup WHERE PlaybookDataPointGroupKey = @PlaybookDataPointGroupKey


EXEC LogDCPEvent 'DCP --- RBP_CalculateAccountItemBands', 'B',0,0,0,@ProjectKey
SET @RowsI = 0
SET @RowsU = 0
SET @RowsD = 0


DECLARE
	@LowerBoundSales Money_Normal_type,
	@LowerBoundFrequencyuency Quantity_Normal_Type,
	@LowerBoundQuantity Quantity_Normal_type,
	@WeightSales Quantity_Normal_Type,
	@WeightFrequency Quantity_Normal_Type,
	@WeightQuantity Quantity_Normal_Type

DELETE FROM PlaybookDataPointGroupAccountItemBands WHERE PlaybookDataPointGroupKey = @PlaybookDataPointGroupKey
SET @RowsD = @RowsD + @@RowCOunt

DECLARE @SpanAlpha INT,
   @SpanBeta INT

SELECT @SpanAlpha = SpanAlpha, @SpanBeta = SpanBeta,
	@LowerBoundSales = AccountItemSalesLowerLimit,
	@LowerBoundFrequencyuency = AccountItemFrequencyLowerLimit,
	@LowerBoundQuantity = AccountItemQuantityLowerLimit,
	@WeightSales = AccountItemSalesWeight,
	@WeightFrequency = AccountItemFrequencyWeight,
	@WeightQuantity = AccountItemQuantityWeight

FROM ATKSegmentFactorPlaybook WHERE SegmentFactorPlaybookKey = @SegmentFactorPlaybookKey


-- 
-- SELECT  
-- 	@LowerBoundSales = AccountItemSalesLowerLimit,
-- 	@LowerBoundFrequencyuency = AccountItemFrequencyLowerLimit,
-- 	@LowerBoundQuantity = AccountItemQuantityLowerLimit,
-- 	@WeightSales = AccountItemSalesWeight,
-- 	@WeightFrequency = AccountItemFrequencyWeight,
-- 	@WeightQuantity = AccountItemQuantityWeight
-- FROM ATKSegmentFactorPlaybook
-- WHERE ScenarioKey = @ScenarioKey


--ALTER  Account Summary--

CREATE TABLE #tmpAccountItem (
AccountKey INT,
ItemKey INT,
TotalQuantity DECIMAL(19,8),
TotalActualPrice DECIMAL(19,8),
TotalActualCost DECIMAL(19,8),
InvoiceCount DECIMAL(19,8)
)

INSERT INTO #tmpAccountItem
SELECT
	FIL.AccountKey,
	FIL.ItemKey,
	SUM(TotalQuantity),
	SUM(TotalActualPrice),
	SUM(TotalActualCost),
	COUNT(*) AS 'InvoiceCount'
FROM dbo.FactInvoiceLine FIL
WHERE Last12MonthsIndicator = 'Y'
GROUP BY FIL.AccountKey,
	FIL.ItemKey

CREATE CLUSTERED INDEX I_TAI_1 ON #TmpAccountItem(AccountKey, ItemKey)

CREATE TABLE #tmpAccountSummary  (
AccountKey INT, 
itemKey INT,
TQuantity DECIMAL(19,8),  
TFrequency DECIMAL(19,8),
TSales DECIMAL(19,8)
)

INSERT INTO #tmpAccountSummary
SELECT 	tai.AccountKey, 
	tai.itemKey,
	tai.TotalQuantity AS TQuantity, 
	tai.InvoiceCount AS TFrequency, 
	tai.TotalActualPrice AS TSales
FROM #tmpAccountItem tai
WHERE tai.TotalQuantity > @LowerBoundQuantity 
	AND tai.TotalActualPrice > @LowerBoundSales
	AND tai.InvoiceCount > @LowerBoundFrequencyuency 


CREATE UNIQUE CLUSTERED INDEX I_sdaaatdf_1 ON #tmpAccountSummary(AccountKey, ItemKey)



--MarketSummary -- 
CREATE TABLE #tmpallaccountssummary (
accountKey DECIMAL(19,8),
AcctMinQuantity DECIMAL(19,8), 
AcctAvgQuantity DECIMAL(19,8), 
AcctMaxQuantity DECIMAL(19,8), 
AcctStDevQuantity DECIMAL(19,8),
AcctMinFrequency DECIMAL(19,8), 
AcctAvgFrequency DECIMAL(19,8),
AcctMaxFrequency DECIMAL(19,8), 
AcctStDevFrequency DECIMAL(19,8),
AcctMinSales DECIMAL(19,8), 
AcctAvgSales DECIMAL(19,8),
AcctMaxSales DECIMAL(19,8), 
AcctStDevSales DECIMAL(19,8)
)

INSERT INTO #tmpallaccountssummary
SELECT 	tas.accountKey,
	MIN(tas.tQuantity) AS 'AcctMinQuantity', 
	AVG(tas.tQuantity) AS 'AcctAvgQuantity', 
	MAX(tas.tQuantity) AS 'AcctMaxQuantity', 
	ISNULL(STDEV(tas.tQuantity),0) AS 'AcctStDevQuantity',
	MIN(tas.tFrequency) AS 'AcctMinFrequency', 
	AVG(tas.tFrequency) AS 'AcctAvgFrequency',
	MAX(tas.tFrequency) AS 'AcctMaxFrequency', 
	ISNULL(STDEV(tas.tFrequency),0) AS 'AcctStDevFrequency',
	MIN(tas.tsales) AS 'AcctMinSales', 
	AVG(tas.tsales) AS 'AcctAvgSales',
	MAX(tas.tsales) AS 'AcctMaxSales', 
	ISNULL(STDEV(tas.tsales),0) AS 'AcctStDevSales'
FROM #tmpAccountSummary tas
GROUP BY tas.accountKey

CREATE UNIQUE CLUSTERED INDEX I_sdtdf_1 ON #tmpallaccountssummary(AccountKey)

---------------------------------------------------------------------------------------
CREATE TABLE #tmpallaccountsummarywlimits  (
accountKey DECIMAL(19,8),
AcctMinQuantity DECIMAL(19,8), 
AcctAvgQuantity DECIMAL(19,8), 

AcctMaxQuantity DECIMAL(19,8), 
AcctStDevQuantity DECIMAL(19,8),
AcctMinFrequency DECIMAL(19,8), 

AcctAvgFrequency DECIMAL(19,8),
AcctMaxFrequency DECIMAL(19,8), 
AcctStDevFrequency DECIMAL(19,8),
AcctMinSales DECIMAL(19,8), 
AcctAvgSales DECIMAL(19,8),
AcctMaxSales DECIMAL(19,8), 
AcctStDevSales DECIMAL(19,8),
AcctULQuantity DECIMAL(19,8),
AcctULFrequency DECIMAL(19,8), 
AcctULSales DECIMAL(19,8)
)

INSERT INTO #tmpallaccountsummarywlimits
SELECT 
	accountKey,
	AcctMinQuantity, 
	AcctAvgQuantity, 
	AcctMaxQuantity, 
	AcctStDevQuantity,
	AcctMinFrequency, 
	AcctAvgFrequency,
	AcctMaxFrequency, 
	AcctStDevFrequency,
	AcctMinSales, 
	AcctAvgSales,
	AcctMaxSales, 
	AcctStDevSales,
	(AcctAvgQuantity) + (AcctStDevQuantity)*@SpanAlpha AS AcctULQuantity,
	(AcctAvgFrequency) + (AcctStDevFrequency)*@SpanAlpha AS AcctULFrequency, 
	(AcctAvgSales) + (AcctStDevSales)*@SpanAlpha AS AcctULSales
FROM #tmpallaccountssummary

CREATE UNIQUE CLUSTERED INDEX I_sdtsdsddfgsdf_1 ON #tmpallaccountsummarywlimits(AccountKey)

---------------------------------------------------------------------------------------
CREATE TABLE #tmpaccountsummary2  (
itemKey INT,
accountKey INT, 
TQuantity DECIMAL(19,8), 
TFrequency DECIMAL(19,8), 
TSales DECIMAL(19,8), 
AcctulQuantity DECIMAL(19,8),
AcctulFrequency DECIMAL(19,8),
Acctulsales DECIMAL(19,8)
)

INSERT INTO #tmpaccountsummary2
SELECT 	tai.itemKey,
	tai.accountKey, 
	SUM(tai.TotalQuantity) AS TQuantity, 
	SUM(tai.InvoiceCount) AS TFrequency, 
	SUM(tai.TotalActualPrice) AS TSales, 
	taasl.AcctulQuantity,
	taasl.AcctulFrequency,
	taasl.Acctulsales
FROM #tmpAccountItem tai 
JOIN #tmpallaccountsummarywlimits taasl ON taasl.accountKey = tai.accountKey
GROUP BY tai.itemKey,tai.accountKey,taasl.AcctulQuantity,taasl.AcctulFrequency,taasl.Acctulsales
HAVING 	(SUM(tai.TotalQuantity) > @LowerBoundQuantity AND (SUM(tai.TotalQuantity) < taasl.AcctulQuantity)
	AND SUM(tai.TotalActualPrice) > @LowerBoundSales AND (SUM(tai.TotalActualPrice) < taasl.Acctulsales)
	AND SUM(tai.InvoiceCount) > @LowerBoundFrequencyuency AND (SUM(tai.InvoiceCount) < taasl.AcctulFrequency))

CREATE UNIQUE CLUSTERED INDEX I_sdtsdfgsdf_1 ON #tmpaccountsummary2(AccountKey, ItemKey)

---------------------------------------------------------------------------
--------------------------------------------------------------------------
CREATE TABLE #tmpallaccountsummary2  (
accountKey DECIMAL(19,8),
AcctMinQuantity DECIMAL(19,8), 
AcctAvgQuantity DECIMAL(19,8),
AcctMaxQuantity DECIMAL(19,8), 
AcctStDevQuantity DECIMAL(19,8),
AcctMinFrequency DECIMAL(19,8), 
AcctAvgFrequency DECIMAL(19,8),
AcctMaxFrequency DECIMAL(19,8), 
AcctStDevFrequency DECIMAL(19,8),
AcctMinSales DECIMAL(19,8), 
AcctAvgSales DECIMAL(19,8),
AcctMaxSales DECIMAL(19,8), 
AcctStDevSales DECIMAL(19,8)
)


INSERT INTO #tmpallaccountsummary2
SELECT 	tas2.accountKey,
	MIN(tas2.tQuantity) AS 'AcctMinQuantity', 
	AVG(tas2.tQuantity) AS 'AcctAvgQuantity',
	MAX(tas2.tQuantity) AS 'AcctMaxQuantity', 
	ISNULL(STDEV(tas2.tQuantity),0) AS 'AcctStDevQuantity',
	MIN(tas2.tFrequency) AS 'AcctMinFrequency', 
	AVG(tas2.tFrequency) AS 'AcctAvgFrequency',
	MAX(tas2.tFrequency) AS 'AcctMaxFrequency', 
	ISNULL (STDEV(tas2.tFrequency),0) AS 'AcctStDevFrequency',
	MIN(tas2.tsales) AS 'AcctMinSales', 
	AVG(tas2.tsales) AS 'AcctAvgSales',
	MAX(tas2.tsales) AS 'AcctMaxSales', 
	ISNULL (STDEV(tas2.tsales),0) AS 'AcctStDevSales'
FROM #tmpaccountsummary2 tas2
GROUP BY tas2.accountKey

CREATE UNIQUE CLUSTERED INDEX I_sdfdsf_1 ON #tmpallaccountsummary2(AccountKey)
-----------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
CREATE TABLE #tmpallaccountsummarywlimits2 (
accountKey DECIMAL(19,8),
AcctMinQuantity DECIMAL(19,8), 
AcctAvgQuantity DECIMAL(19,8),
AcctMaxQuantity DECIMAL(19,8), 
AcctStDevQuantity DECIMAL(19,8),
AcctMinFrequency DECIMAL(19,8), 
AcctAvgFrequency DECIMAL(19,8),
AcctMaxFrequency DECIMAL(19,8), 
AcctStDevFrequency DECIMAL(19,8),
AcctMinSales DECIMAL(19,8), 
AcctAvgSales DECIMAL(19,8),
AcctMaxSales DECIMAL(19,8), 
AcctStDevSales DECIMAL(19,8),
AcctULQuantity DECIMAL(19,8),
AcctULFrequency DECIMAL(19,8), 
AcctULSales DECIMAL(19,8)
)

INSERT INTO #tmpallaccountsummarywlimits2
SELECT accountKey,
AcctMinQuantity, 
AcctAvgQuantity,
AcctMaxQuantity, 
AcctStDevQuantity,
AcctMinFrequency, 

AcctAvgFrequency,
AcctMaxFrequency, 
AcctStDevFrequency,
AcctMinSales, 
AcctAvgSales,
AcctMaxSales, 
AcctStDevSales, 
	(AcctAvgQuantity) + (AcctStDevQuantity)*@SpanBeta AS AcctULQuantity,
	(AcctAvgFrequency) + (AcctStDevFrequency)*@SpanBeta AS AcctULFrequency, 
	(AcctAvgSales) + (AcctStDevSales)*@SpanBeta AS AcctULSales
FROM #tmpallaccountsummary2

CREATE UNIQUE CLUSTERED INDEX I_sdsf_1 ON #tmpallaccountsummarywlimits2(AccountKey)
---------------------------------------------------------------------------------------
CREATE TABLE #tmpaccountsummary3 (
itemKey INT, 
accountKey INT, 
TQuantity DECIMAL(19,8), 
TFrequency DECIMAL(19,8), 
TSales DECIMAL(19,8), 
AcctulQuantity DECIMAL(19,8),
AcctulFrequency DECIMAL(19,8),
Acctulsales DECIMAL(19,8)
)

INSERT INTO #tmpaccountsummary3
SELECT 	tai.itemKey, 
	tai.accountKey, 
	SUM(tai.TotalQuantity) AS TQuantity, 
	SUM(tai.InvoiceCount) AS TFrequency, 
	SUM(tai.TotalActualPrice) AS TSales, 
	taasl2.AcctulQuantity,
	taasl2.AcctulFrequency,
	taasl2.Acctulsales
--into #tmpaccountsummary3
FROM #tmpAccountItem tai JOIN #tmpallaccountsummarywlimits2 taasl2 ON taasl2.accountKey = tai.accountKey
GROUP BY tai.itemKey,tai.accountKey,taasl2.AcctulQuantity,taasl2.AcctulFrequency,taasl2.Acctulsales
HAVING 	(SUM(tai.TotalQuantity) > @LowerBoundQuantity AND (SUM(tai.TotalQuantity) < taasl2.AcctulQuantity)
	AND SUM(tai.TotalActualPrice) > @LowerBoundSales AND (SUM(tai.TotalActualPrice) < taasl2.Acctulsales)
	AND SUM(tai.InvoiceCount) > @LowerBoundFrequencyuency AND (SUM(tai.InvoiceCount) < taasl2.AcctulFrequency))

CREATE UNIQUE CLUSTERED INDEX I_T3_1 ON #tmpaccountsummary3 (AccountKey, ItemKey)

---------------------------------------------------------------------------
--------------------------------------------------------------------------
CREATE TABLE #tmpallaccountsummary3  (
accountKey DECIMAL(19,8),
AcctMinQuantity DECIMAL(19,8), 
AcctAvgQuantity DECIMAL(19,8),
AcctMaxQuantity DECIMAL(19,8), 
AcctStDevQuantity DECIMAL(19,8),
AcctMinFrequency DECIMAL(19,8), 
AcctAvgFrequency DECIMAL(19,8),
AcctMaxFrequency DECIMAL(19,8), 
AcctStDevFrequency DECIMAL(19,8),
AcctMinSales DECIMAL(19,8), 
AcctAvgSales DECIMAL(19,8),
AcctMaxSales DECIMAL(19,8), 
AcctStDevSales DECIMAL(19,8)
)

INSERT INTO #tmpallaccountsummary3
SELECT 	tas3.accountKey,
	MIN(tas3.tQuantity) AS 'AcctMinQuantity', 
	AVG(tas3.tQuantity) AS 'AcctAvgQuantity',
	MAX(tas3.tQuantity) AS 'AcctMaxQuantity', 
	ISNULL(STDEV(tas3.tQuantity),0) AS 'AcctStDevQuantity',
	MIN(tas3.tFrequency) AS 'AcctMinFrequency', 
	AVG(tas3.tFrequency) AS 'AcctAvgFrequency',
	MAX(tas3.tFrequency) AS 'AcctMaxFrequency', 
	ISNULL(STDEV(tas3.tFrequency),0) AS 'AcctStDevFrequency',
	MIN(tas3.tsales) AS 'AcctMinSales', 
	AVG(tas3.tsales) AS 'AcctAvgSales',
	MAX(tas3.tsales) AS 'AcctMaxSales', 
	ISNULL(STDEV(tas3.tsales),0) AS 'AcctStDevSales'
FROM #tmpaccountsummary3 tas3
GROUP BY tas3.accountKey

CREATE UNIQUE CLUSTERED INDEX I_TsdfdfQ_1 ON #tmpallaccountsummary3(AccountKey)

------------------------------------------------------------------------------------

CREATE TABLE #tmpQuartiles  (
accountKey INT,
AcctQuantityQuartile DECIMAL(19,8),
AcctFrequencyQuartile DECIMAL(19,8),
AcctSalesQuartile DECIMAL(19,8),
AcctmaxQuantity DECIMAL(19,8),
AcctmaxFrequency DECIMAL(19,8),
Acctmaxsales DECIMAL(19,8))

INSERT INTO #tmpQuartiles
SELECT 	accountKey,
	((AcctAvgQuantity + 6*AcctStDevQuantity)/4) AS 'AcctQuantityQuartile',

	((AcctAvgFrequency + 6*AcctStDevFrequency)/4) AS 'AcctFrequencyQuartile',
	((AcctAvgSales + 6*AcctStDevSales)/4) AS 'AcctSalesQuartile',
	AcctmaxQuantity,
	AcctmaxFrequency,
	Acctmaxsales
FROM #tmpallaccountsummary3

CREATE UNIQUE CLUSTERED INDEX I_TQ_1 ON #tmpQuartiles(AccountKey)

-------------------------------------------------------------------------------------
CREATE TABLE #tmpSummaryList  (
itemKey INT,
accountKey INT, 
TQuantity DECIMAL(19,8), 
TFrequency DECIMAL(19,8), 
TSales DECIMAL(19,8)
)

INSERT INTO #tmpSummaryList
SELECT 	tai.itemKey,
	tai.accountKey, 

	SUM(tai.TotalQuantity) AS TQuantity, 
	SUM(tai.InvoiceCount) AS TFrequency, 
	SUM(tai.TotalActualPrice) AS TSales
--into #tmpSummaryList
FROM #tmpAccountItem tai 

GROUP BY tai.itemKey, tai.accountKey

CREATE UNIQUE CLUSTERED INDEX I_sdfsdfxahggasx_1 ON #tmpSummaryList(AccountKey, ItemKey)

--------------------------------------------------------------------------------------
CREATE TABLE #tmpaccountitemscore  (
itemKey INT,
accountKey INT,
tQuantity DECIMAL(19,8),
tFrequency DECIMAL(19,8), 
tsales DECIMAL(19,8), 
QuantityScore DECIMAL(19,8),
FrequencyScore DECIMAL(19,8),
SalesScore DECIMAL(19,8),
QuantityWeight DECIMAL(19,8),
FrequencyWeight DECIMAL(19,8),
SalesWeight DECIMAL(19,8)
)


INSERT INTO #tmpaccountitemscore
SELECT 	tsl.itemKey,
	tsl.accountKey,
	tsl.tQuantity,
	tsl.tFrequency, 
	tsl.tsales, 
	'QuantityScore' =
	   CASE  
		WHEN tsl.TQuantity <= @LowerBoundQuantity  THEN 1 
		WHEN tsl.TQuantity > @LowerBoundQuantity AND tsl.TQuantity <= tq.AcctQuantityquartile THEN 2
		WHEN tsl.TQuantity > tq.AcctQuantityquartile AND tsl.TQuantity <= (tq.AcctQuantityquartile*2) THEN 3
		WHEN tsl.TQuantity > (2*tq.AcctQuantityquartile) AND tsl.TQuantity <= (tq.AcctQuantityquartile*3) THEN 4
		WHEN tsl.TQuantity > (3*tq.AcctQuantityquartile) AND tsl.TQuantity <= (tq.AcctmaxQuantity) THEN 5
		ELSE 6
	   END,

	'FrequencyScore' =
	   CASE  
		WHEN tsl.TFrequency <= @LowerBoundFrequencyuency  THEN 1 
		WHEN tsl.TFrequency > @LowerBoundFrequencyuency AND tsl.TFrequency <= tq.AcctFrequencyquartile THEN 2
		WHEN tsl.TFrequency > tq.AcctFrequencyquartile AND tsl.TFrequency <= (tq.AcctFrequencyquartile*2) THEN 3
		WHEN tsl.TFrequency > (2*tq.AcctFrequencyquartile) AND tsl.TFrequency <= (tq.AcctFrequencyquartile*3) THEN 4
		WHEN tsl.TFrequency > (3*tq.AcctFrequencyquartile) AND tsl.TFrequency <= (tq.AcctmaxFrequency) THEN 5
		ELSE 6
	   END,

	'SalesScore' =
	   CASE  
		WHEN tsl.TSales <= @LowerBoundSales  THEN 1 
		WHEN tsl.TSales > @LowerBoundSales AND tsl.TSales <= tq.Acctsalesquartile THEN 2
		WHEN tsl.TSales > tq.Acctsalesquartile AND tsl.TSales <= (tq.Acctsalesquartile*2) THEN 3
		WHEN tsl.TSales > (2*tq.Acctsalesquartile) AND tsl.TSales <= (tq.Acctsalesquartile*3) THEN 4
		WHEN tsl.TSales > (3*tq.Acctsalesquartile) AND tsl.TSales <= (tq.Acctmaxsales) THEN 5
		ELSE 6
	   END,

	'QuantityWeight' = @WeightQuantity,
	'FrequencyWeight' = @WeightFrequency,
	'SalesWeight' = @WeightSales
FROM #tmpSummaryList tsl LEFT JOIN #tmpQuartiles tq ON tsl.accountKey = tq.accountKey

CREATE UNIQUE CLUSTERED INDEX I_sdfsdfxaasx_1 ON #tmpaccountitemscore(AccountKey, ItemKey)

------------------------------------------------------------------------------------------
CREATE TABLE #tmpaccountitemscore2  (
itemKey INT, 
accountKey INT,
AggItemScore DECIMAL(19,8),
ConsItemScore DECIMAL(19,8),
tsales DECIMAL(19,8),
tFrequency DECIMAL(19,8),
tQuantity DECIMAL(19,8)
)

INSERT INTO #tmpaccountitemscore2
SELECT 	itemKey, accountKey,
	((QuantityScore*QuantityWeight) + (FrequencyScore*FrequencyWeight)+ (SalesScore*SalesWeight)) AS 'AggItemScore',
	
	'ConsItemScore'=
		CASE
			WHEN QuantityScore =6 THEN 6
			WHEN FrequencyScore =6 THEN 6
			WHEN SalesScore =6 THEN 6
			ELSE ((QuantityScore*QuantityWeight) + (FrequencyScore*FrequencyWeight)+ (SalesScore*SalesWeight))
		END,
	tsales,
	tFrequency,
	tQuantity
FROM #tmpaccountitemscore

CREATE UNIQUE CLUSTERED INDEX I_sdfsdfxx_1 ON #tmpaccountitemscore2(AccountKey, ItemKey)

-------------------------------------------------------------------------------------------


INSERT INTO PlaybookDataPointGroupAccountItemBands (PlaybookDataPointGroupKey, AccountKey, ItemKey, AccountItemBand)
SELECT @PlaybookDataPointGroupKey, AccountKey, ItemKey, 
--	aggItemScore,
	'AccountItemBand'=
		CASE
			WHEN Aggitemscore > 5.0 THEN 'A'
			WHEN Aggitemscore > 4.0 AND Aggitemscore <= 5.0 THEN 'B'
			WHEN Aggitemscore > 3.0 AND Aggitemscore <= 4.0 THEN 'C'
			WHEN Aggitemscore > 2.0 AND Aggitemscore <= 3.0 THEN 'D'
			WHEN Aggitemscore > 1.0 AND Aggitemscore <= 2.0 THEN 'E'
			WHEN Aggitemscore > 0.0 AND Aggitemscore <= 1.0 THEN 'F'
		END
--	tsales,
--	tFrequency,
--	tQuantity
FROM #tmpaccountitemscore2 A
SET @RowsI = @RowsI + @@RowCount

DECLARE @USql NVARCHAR(MAX)
SET @USql = 'Update Statistics PlaybookDataPointGroupAccountItemBands'
EXEC (@USql)

EXEC LogDCPEvent 'DCP --- RBP_CalculateAccountItemBands', 'E', @RowsI, @RowsU, @RowsD

























GO
