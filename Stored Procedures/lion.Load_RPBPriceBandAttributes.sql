SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [lion].[Load_RPBPriceBandAttributes]
AS

/*
EXEC lion.Load_RPBPriceBandAttributes
*/

SET NOCOUNT ON

DECLARE 
	@RowsI INT, 
	@RowsU INT, 
	@RowsD INT

SET @RowsI = 0
SET @RowsU = 0
SET @RowsD = 0


EXEC LogDCPEvent 'ETL - lion.Load_RPBPriceBandAttributes', 'B'



DECLARE
	@ScenarioKey Key_Normal_type,
	@PlaybookDataPointGroupKey Key_Normal_type,
	@ProjectKey Key_Normal_type

EXEC dbo.GetRPBScenarioKeys @ScenarioKey OUTPUT, @PlaybookDataPointGroupKey OUTPUT, @ProjectKey OUTPUT


IF @PlaybookDataPointGroupKey IS NOT NULL
BEGIN

	EXEC DBA_IndexRebuild 'RPBPriceBandAttributes', 'Drop', null, 'lion'

	TRUNCATE TABLE lion.RPBPriceBandAttributes
	
	INSERT lion.RPBPriceBandAttributes (
		PlaybookPricingGroupKey,
		PricingRuleSequenceNumber,
		PricingRuleKey,
		PlaybookRegion,
		CustomerSpecialism,
		Sales603010Bucket,
		StrategicItem,
		--CustomerSize,
		PriceApproach,
		Item,
		LLSPG,
		--HLSPG,
		--GSPG,
		
		ContractClaimsIndicator,
		ShipmentType,
		PlaybookProductBrand,
		RegionExceptionIndicator
	)
	SELECT DISTINCT
		ppg.PlaybookPricingGroupKey,
		PricingRuleSequenceNumber,
		apr.PricingRuleKey,
		ISNULL(PlaybookRegion, N'ALL') AS PlaybookRegion,
		ISNULL(CustomerSpecialism, N'ALL') AS CustomerSpecialism,
		ISNULL(Sales603010Bucket, N'ALL') AS Sales603010Bucket,
		ISNULL(StrategicItem, N'ALL') AS StrategicItem,
		--ISNULL(CustomerSize, N'ALL') AS CustomerSize,
		ISNULL(PriceApproach, N'ALL') AS PriceApproach,
		ISNULL(Item, N'ALL') AS Item,
		ISNULL(LLSPG, N'ALL') AS LLSPG,
		--ISNULL(HLSPG, N'ALL') AS HLSPG,
		--ISNULL(GSPG, N'ALL') AS GSPG,
		
		ISNULL(ContractClaimsIndicator, N'ALL') AS ContractClaimsIndicator,
		ISNULL(ShipmentType, N'ALL') AS ShipmentType,
		ISNULL(PlaybookProductBrand, N'ALL') AS PlaybookProductBrand,
		ISNULL(RegionExceptionIndicator, N'ALL') AS RegionExceptionIndicator
	FROM PlaybookPricingGroup ppg (NOLOCK)
	INNER JOIN dbo.ATKPricingRule apr (NOLOCK)
		ON ppg.PricingRuleKey = apr.PricingRuleKey
	CROSS APPLY [lion].[fn_GetRPBPriceBandAttributes](GroupValues, GroupingColumns) v
	WHERE
		PlaybookDataPointGroupKey = @PlaybookDataPointGroupKey

	SET @RowsI = @RowsI + @@RowCount

	EXEC DBA_IndexRebuild 'RPBPriceBandAttributes', 'Create', null, 'lion'
	
END



EXEC LogDCPEvent 'ETL - lion.Load_RPBPriceBandAttributes', 'E', @RowsI, @RowsU, @RowsD


















GO
