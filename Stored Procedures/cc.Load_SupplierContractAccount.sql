SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE  PROCEDURE [cc].[Load_SupplierContractAccount] AS 
BEGIN

	SET NOCOUNT ON

	EXEC LogDCPEvent 'cc.Load_SupplierContractAccount', 'B'

	DECLARE @RowsI INT,
			@RowsD INT

-- Insert new into SupplierContractAccount from ContractCustomer
	INSERT INTO cc.SupplierContractAccount(
		ContractKey ,
		AccountKey ,
		CreatedOnDT
	)
	SELECT 
		sc.ContractKey,
		da.AccountKey,
		sc.ContractCreationDT
	FROM Lion_Staging.dbo.ContractCustomer cc
	JOIN cc.SupplierContract sc 
		ON cc.Region + cc.OriginatorBranchOrRegion + cc.ContractNumber = sc.FullContractNumber
	JOIN dbo.DimAccount da
		ON da.AccountNumber = cc.ContractCustomer
	LEFT JOIN cc.SupplierContractAccount sca
		ON sca.AccountKey = da.AccountKey
		AND sca.ContractKey = sc.ContractKey
	WHERE sca.ContractAccountKey IS NULL
	AND cc.ContractCustomer <> 'ALL'
	AND sc.ContractSourceKey = 1

	SELECT @RowsI = @@ROWCOUNT

-- Delete records missing in the staging table
	DELETE FROM cc.SupplierContractAccount
	WHERE ContractAccountKey IN 
	(SELECT sca.ContractAccountKey
	FROM cc.SupplierContractAccount sca
	JOIN cc.SupplierContract sc 
		ON sca.ContractKey = sc.ContractKey
	JOIN dbo.DimAccount da
		ON da.AccountKey = sca.AccountKey
	LEFT JOIN Lion_Staging.dbo.ContractCustomer cc
		ON cc.Region + cc.OriginatorBranchOrRegion + cc.ContractNumber = sc.FullContractNumber
		AND cc.ContractCustomer = da.AccountNumber
	WHERE sc.ContractSourceKey = 1
		AND cc.ContractNumber IS NULL)

	SELECT @RowsD = @@ROWCOUNT


	UPDATE sc SET IncludeAllAccounts = 0
	FROM Lion_Staging.dbo.ContractCustomer cc
	JOIN cc.SupplierContract sc 
		ON cc.Region + cc.OriginatorBranchOrRegion + cc.ContractNumber = sc.FullContractNumber
	WHERE cc.ContractCustomer <> 'ALL'
	AND (sc.IncludeAllAccounts IS NULL OR sc.IncludeAllAccounts = 1)
	AND sc.ContractSourceKey = 1

	--If no matching Customer records then set IncludeAllAccounts = 0 (in case of deletes)
	UPDATE sc SET IncludeAllAccounts = 0
	FROM cc.SupplierContract sc 
	LEFT JOIN cc.SupplierContractAccount sca
		ON sca.ContractKey = sc.ContractKey
	LEFT JOIN Lion_Staging.dbo.ContractCustomer cc
		ON cc.Region + cc.OriginatorBranchOrRegion + cc.ContractNumber = sc.FullContractNumber
		AND cc.ContractCustomer = 'ALL'
	WHERE sc.IncludeAllAccounts = 1
		AND sca.ContractAccountKey IS NULL
		AND cc.ContractNumber IS NULL
		AND sc.ContractSourceKey = 1

	UPDATE sc SET IncludeAllAccounts = 1
	FROM Lion_Staging.dbo.ContractCustomer cc
	JOIN cc.SupplierContract sc 
		ON cc.Region + cc.OriginatorBranchOrRegion + cc.ContractNumber = sc.FullContractNumber
	WHERE cc.ContractCustomer = 'ALL'
		AND (sc.IncludeAllAccounts IS NULL OR sc.IncludeAllAccounts = 0)
		AND sc.ContractSourceKey = 1

	--If a contract has the IncludeAllAccounts flag set to 1 (yes) then delete any existing accounts from the contract
	DELETE FROM cc.SupplierContractAccount
	WHERE ContractAccountKey IN 
	(
	SELECT sca.ContractAccountKey
	FROM cc.SupplierContract sc
	INNER JOIN cc.SupplierContractAccount sca
		ON sca.ContractKey = sc.ContractKey
	WHERE sc.IncludeAllAccounts = 1
		AND sc.ContractSourceKey = 1
	)

	SELECT @RowsD = @RowsD + @@ROWCOUNT

	EXEC LogDCPEvent 'cc.Load_SupplierContractAccount', 'E', @RowsI, 0, @RowsD

END
GO
