SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




CREATE PROCEDURE [dbo].[SetPasswordValidityPeriod]
	@ParamPasswordValidityPeriod INTEGER
AS

/*

EXEC dbo.SetPasswordValidityPeriod 90


SELECT
	NEWID() AS GUID,
	*
FROM [Param]
WHERE ParamName = 'WebUserPasswordValidityPeriod'

EXEC dbo.GetPasswordValidityPeriod 


*/

SET NOCOUNT ON


UPDATE dbo.[Param]
SET
	ParamValue = CAST(@ParamPasswordValidityPeriod AS NVARCHAR(4000))
WHERE
	ParamName = 'WebUserPasswordValidityPeriod'
IF @@ROWCOUNT = 0 	-- row count must be checked immediately after the UPDATE
BEGIN
	INSERT INTO dbo.[Param] (
		ParamName,
		ParamValue
	) VALUES ( 
		'WebUserPasswordValidityPeriod',
		CAST(@ParamPasswordValidityPeriod AS NVARCHAR(4000))
	) 
END

SELECT
	NEWID() AS GUID,
	*
FROM [Param]
WHERE ParamName = 'WebUserPasswordValidityPeriod'



GO
