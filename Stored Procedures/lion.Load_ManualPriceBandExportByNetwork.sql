SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO






CREATE PROCEDURE [lion].[Load_ManualPriceBandExportByNetwork]  (
	@LLSPGCode UDVarchar_type
)
AS

/*

Only include rows where 1) there's data, or 2) a single summary row for every product. 

EXEC lion.Load_ManualPriceBandExportByNetwork N'AJ01'


EXEC lion.Load_ManualPriceBandExport N'AJ01'

SELECT * FROM lion.ATKManualPriceBandExport
ORDER BY 2,3,4,5,6,7,8,9,10

*/

SET NOCOUNT ON

DECLARE
	@RowsI INT,
	@RowsU INT,
	@RowsD INT


SET @RowsI = 0
SET @RowsU = 0
SET @RowsD = 0


TRUNCATE TABLE lion.ATKManualPriceBandExport



/*** Do the LLSPG Code Rollup ***/

/** Get Item "all" summary row **/
; WITH ItemSalesRollup AS (
	SELECT
		IG3Level4 AS ItemPyramidCode,
		IG3Level1 AS LLSPGCode,
		ItemUDVarChar20 AS ProductCode,

		SUM(TotalActualPrice) AS TotalSales,
		SUM(TotalActualCost) AS TotalCost,
		SUM(TotalQuantity) AS TotalQuantity,
		SUM(TotalQuantity * UnitListPrice) AS TotalTradePrice,
		--SUM(TotalActualPrice * il.FloorGPP) AS GTMFloor,
		--SUM(TotalActualPrice * il.TargetGPP) AS GTMTarget,
		--SUM(TotalActualPrice * il.StretchGPP) AS GTMStretch,
		SUM(TotalActualPrice * il.FloorGPP)/NULLIF(SUM(TotalActualPrice), 0) AS CurrentFloorGPP,
		SUM(TotalActualPrice * il.TargetGPP)/NULLIF(SUM(TotalActualPrice), 0) AS CurrentTargetGPP,
		SUM(TotalActualPrice * il.StretchGPP)/NULLIF(SUM(TotalActualPrice), 0) AS CurrentStretchGPP
	FROM FactInvoiceLinePricebandScore il
	--INNER JOIN dbo.DimAccountGroup1 dag1
	--	ON dag1.AccountGroup1Key = il.AccountGroup1Key
	INNER JOIN dbo.DimItem di
		ON di.ItemKey = il.ItemKey
	INNER JOIN dbo.DimItemGroup3 dig3
		ON dig3.ItemGroup3Key = di.ItemGroup3Key
	--INNER JOIN dbo.DimInvoiceLineGroup1 dilg1
	--	ON dilg1.InvoiceLineGroup1Key = il.InvoiceLineGroup1Key
	INNER JOIN dbo.FactLastPriceAndCost flpac
		ON flpac.AccountKey = il.AccountKey
		AND flpac.ItemKey = il.ItemKey
		AND flpac.InvoiceLineGroup1Key = il.InvoiceLineGroup1Key
	WHERE 
		IG3Level4 = N'1'
		--AND IG3Level1 = N'AJ01'	--@LLSPGCode
		AND IG3Level1 = @LLSPGCode
	GROUP BY
		IG3Level4,
		IG3Level1,
		ItemUDVarChar20
)
INSERT lion.ATKManualPriceBandExport (
	ItemPyramidCode,
	LLSPGCode,
	ProductCode,
	PriceApproach,
	ContractClaimsIndicator,
	Region,
	Area,
	Network,
	Branch,
	
	CurrentFloorGPP,
	CurrentTargetGPP,
	CurrentStretchGPP,

	CurrentFloorDiscount,
	CurrentTargetDiscount,
	CurrentStretchDiscount,

	CurrentFloorPrice, 
	CurrentTargetPrice, 
	CurrentStretchPrice, 

	ProductCost, 
	TradePrice
)
SELECT
	IG3Level4 AS ItemPyramidCode,
	IG3Level1 AS LLSPGCode,
	ItemUDVarChar20 AS ProductCode,
	N'ALL' AS PriceApproach,
	N'ALL' AS ContractClaimsIndicator,
	N'ALL' AS Region,
	N'ALL' AS Area,
	N'ALL' AS Network,
	N'ALL' AS Branch,			--LPF-5620 	Update stored procedure to write Branches field to table, and any additional logic changes	

	CurrentFloorGPP,
	CurrentTargetGPP,
	CurrentStretchGPP,
		
	1 - (rp.Price / NULLIF(TotalTradePrice/NULLIF(TotalQuantity, 0), 0)) AS CurrentFloorDiscount,
	1 - (ap.Price / NULLIF(TotalTradePrice/NULLIF(TotalQuantity, 0), 0)) AS CurrentTargetDiscount,
	1 - (gp.Price / NULLIF(TotalTradePrice/NULLIF(TotalQuantity, 0), 0)) AS CurrentGreeDiscount,

	rp.Price AS CurrentFloorPrice,
	ap.Price AS CurrentTargetPrice,
	gp.Price AS CurrentStretchPrice,

	(di.ItemUDDecimal3 - di.ItemUDDecimal4 - di.ItemUDDecimal5) AS ProductCost,
	ItemUDDecimal1 AS TradePrice
FROM dbo.DimItem di
INNER JOIN dbo.DimItemGroup3 dig3
	ON dig3.ItemGroup3Key = di.ItemGroup3Key
LEFT JOIN ItemSalesRollup r
	ON 
		IG3Level4 = r.ItemPyramidCode
		AND IG3Level1 = r.LLSPGCode
		AND ItemUDVarChar20 = r.ProductCode
OUTER APPLY lion.fn_GetPriceWeighted(TotalCost, TotalQuantity, CurrentFloorGPP) rp
OUTER APPLY lion.fn_GetPriceWeighted(TotalCost, TotalQuantity, CurrentTargetGPP) ap
OUTER APPLY lion.fn_GetPriceWeighted(TotalCost, TotalQuantity, CurrentStretchGPP) gp
WHERE 
	IG3Level4 = N'1'
	--AND IG3Level1 = N'AJ01'	--@LLSPGCode
	AND IG3Level1 = @LLSPGCode
SELECT @RowsI = @RowsI + @@ROWCOUNT


/** Get attribute summary rows **/
; WITH LLSPGSalesRollup AS (
	SELECT
		IG3Level4 AS ItemPyramidCode,
		IG3Level1 AS LLSPGCode,
		ItemUDVarChar20 AS ProductCode,
		UDVarchar10 AS PriceApproach,
		InvoiceLineGroup1UDVarchar2 AS ContractClaimsIndicator,
		AG1Level4 AS Region,
		AG1Level3 AS Area,
		AG1Level2 AS Network,
		AG1Level1 AS Branch,				--LPF-5620 	Update stored procedure to write Branches field to table, and any additional logic changes

		SUM(TotalActualPrice) AS TotalSales,
		SUM(TotalActualCost) AS TotalCost,
		SUM(TotalQuantity) AS TotalQuantity,
		SUM(TotalQuantity * UnitListPrice) AS TotalTradePrice,
		--SUM(TotalActualPrice * il.FloorGPP) AS GTMFloor,
		--SUM(TotalActualPrice * il.TargetGPP) AS GTMTarget,
		--SUM(TotalActualPrice * il.StretchGPP) AS GTMStretch,
		SUM(TotalActualPrice * il.FloorGPP)/NULLIF(SUM(TotalActualPrice), 0) AS CurrentFloorGPP,
		SUM(TotalActualPrice * il.TargetGPP)/NULLIF(SUM(TotalActualPrice), 0) AS CurrentTargetGPP,
		SUM(TotalActualPrice * il.StretchGPP)/NULLIF(SUM(TotalActualPrice), 0) AS CurrentStretchGPP
	FROM FactInvoiceLinePricebandScore il
	INNER JOIN dbo.DimAccountGroup1 dag1
		ON dag1.AccountGroup1Key = il.AccountGroup1Key
	INNER JOIN dbo.DimItem di
		ON di.ItemKey = il.ItemKey
	INNER JOIN dbo.DimItemGroup3 dig3
		ON dig3.ItemGroup3Key = di.ItemGroup3Key
	INNER JOIN dbo.DimInvoiceLineGroup1 dilg1
		ON dilg1.InvoiceLineGroup1Key = il.InvoiceLineGroup1Key
	INNER JOIN dbo.FactLastPriceAndCost flpac
		ON flpac.AccountKey = il.AccountKey
		AND flpac.ItemKey = il.ItemKey
		AND flpac.InvoiceLineGroup1Key = il.InvoiceLineGroup1Key
	WHERE 
		IG3Level4 = N'1'
		--AND IG3Level1 = N'AJ01'	--@LLSPGCode
		AND IG3Level1 = @LLSPGCode
	GROUP BY
		IG3Level4,
		IG3Level1,
		ItemUDVarChar20,
		UDVarchar10,
		InvoiceLineGroup1UDVarchar2,
		AG1Level4,
		AG1Level3,
		AG1Level2,
		AG1Level1			--LPF-5620 	Update stored procedure to write Branches field to table, and any additional logic changes
)
INSERT lion.ATKManualPriceBandExport (
	ItemPyramidCode,
	LLSPGCode,
	ProductCode,
	PriceApproach,
	ContractClaimsIndicator,
	Region,
	Area,
	Network,
	Branch,						--LPF-5620 	Update stored procedure to write Branches field to table, and any additional logic changes
	
	CurrentFloorGPP,
	CurrentTargetGPP,
	CurrentStretchGPP,

	CurrentFloorDiscount,
	CurrentTargetDiscount,
	CurrentStretchDiscount,

	CurrentFloorPrice, 
	CurrentTargetPrice, 
	CurrentStretchPrice, 

	ProductCost, 
	TradePrice
)
SELECT
	ItemPyramidCode,
	LLSPGCode,
	ProductCode,
	PriceApproach,
	ContractClaimsIndicator,
	Region,
	Area,
	Network,
	Branch,						--LPF-5620 	Update stored procedure to write Branches field to table, and any additional logic changes

	CurrentFloorGPP,
	CurrentTargetGPP,
	CurrentStretchGPP,
	
	1 - (rp.Price / NULLIF(TotalTradePrice/NULLIF(TotalQuantity, 0), 0)) AS CurrentFloorDiscount,
	1 - (ap.Price / NULLIF(TotalTradePrice/NULLIF(TotalQuantity, 0), 0)) AS CurrentTargetDiscount,
	1 - (gp.Price / NULLIF(TotalTradePrice/NULLIF(TotalQuantity, 0), 0)) AS CurrentGreeDiscount,

	rp.Price AS CurrentFloorPrice,
	ap.Price AS CurrentTargetPrice,
	gp.Price AS CurrentStretchPrice,
		
	(di.ItemUDDecimal3 - di.ItemUDDecimal4 - di.ItemUDDecimal5) AS ProductCost,
	ItemUDDecimal1 AS TradePrice
FROM 
	LLSPGSalesRollup r
INNER JOIN dbo.DimItem di
	ON di.ItemNumber = r.ItemPyramidCode + r.ProductCode
OUTER APPLY lion.fn_GetPriceWeighted(TotalCost, TotalQuantity, CurrentFloorGPP) rp
OUTER APPLY lion.fn_GetPriceWeighted(TotalCost, TotalQuantity, CurrentTargetGPP) ap
OUTER APPLY lion.fn_GetPriceWeighted(TotalCost, TotalQuantity, CurrentStretchGPP) gp
SELECT @RowsI = @RowsI + @@ROWCOUNT























---- create all our llspg code summary data
--INSERT lion.ATKManualPriceBandExport (
--	ItemPyramidCode,
--	LLSPGCode,
--	ProductCode,
--	PriceApproach,
--	ContractClaimsIndicator,
--	Region,
--	Area,
--	Network,
	
--	CurrentFloorGPP,
--	CurrentTargetGPP,
--	CurrentStretchGPP,

--	CurrentFloorDiscount,
--	CurrentTargetDiscount,
--	CurrentStretchDiscount,

--	CurrentFloorPrice, 
--	CurrentTargetPrice, 
--	CurrentStretchPrice, 

--	ProductCost, 
--	TradePrice
--)

--SELECT 
--	dc.ItemPyramidCode,
--	dc.LLSPGCode,
--	N'ALL' AS ProductCode,
--	dc.PriceApproach,
--	dc.ContractClaimsIndicator,
--	dc.Region,
--	dc.Area,
--	dc.Network,

--	CurrentFloorGPP,
--	CurrentTargetGPP,
--	CurrentStretchGPP,
	
--	CurrentFloorDiscount,
--	CurrentTargetDiscount,
--	CurrentGreeDiscount,

--	CurrentFloorPrice,
--	CurrentTargetPrice,
--	CurrentStretchPrice,

--	NULL AS CurrProductCost,
--	NULL AS CurrTradePrice

--FROM #LLSPGDataCombo dc
--LEFT 
--JOIN #LLSPGSalesRollupData rd ON
--	rd.ItemPyramidCode = dc.ItemPyramidCode
--	AND rd.LLSPGCode = dc.LLSPGCode
--	AND rd.PriceApproach = dc.PriceApproach
--	AND rd.ContractClaimsIndicator = dc.ContractClaimsIndicator
--	AND rd.Region = dc.Region
--	AND rd.Area = dc.Area
--	AND rd.Network = dc.Network
----OUTER APPLY lion.fn_GetManualPriceBand (
----	dc.ItemPyramidCode,
----	dc.LLSPGCode,
----	N'ALL',
----	dc.PriceApproach,
----	dc.ContractClaimsIndicator,
----	dc.Region,
----  dc.Area,
----	dc.Network
----	) mpb
--SELECT @RowsI = @RowsI + @@ROWCOUNT

























------------------------zzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzz----------------------zzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzz
------------------------zzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzz----------------------zzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzz
------------------------zzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzz----------------------zzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzz





--/*** Do the Item/Product Code Rollup ***/

---- get all the various base combinations
--; WITH PriceApproachData AS (
--	SELECT DISTINCT 
--		UDVarchar10 AS PriceApproach
--	FROM dbo.FactLastPriceAndCost
--), ContractClaimsIndicatorData AS (
--	SELECT DISTINCT 
--		InvoiceLineGroup1UDVarchar2 AS ContractClaimsIndicator
--	FROM dbo.DimInvoiceLineGroup1
--	WHERE InvoiceLineGroup1UDVarchar2 <> N'Unknown'
--), GeographicData AS (
--	SELECT DISTINCT
--		AG1Level4 AS Region,
--		AG1Level3 AS Area,
--		AG1Level2 AS Network
--	FROM dbo.DimAccount da
--	INNER JOIN dbo.DimAccountGroup1 dag1
--		ON dag1.AccountGroup1Key = da.AccountGroup1Key
--	WHERE AG1Level2 <> N'Unknown'
--	--AND AG1Level2 <> N''
--), DataCombo AS (
--	SELECT
--		N'1' AS ItemPyramidCode,
--		IG3Level1 AS LLSPGCode,
--		ItemUDVarChar20 AS ProductCode,
--		--@LLSPGCode AS LLSPGCode
--		PriceApproachData.*,
--		ContractClaimsIndicatorData.*,
--		GeographicData.*
--	FROM dbo.DimItem di
--	INNER JOIN dbo.DimItemGroup3 dig3
--		ON dig3.ItemGroup3Key = di.ItemGroup3Key
--	CROSS JOIN PriceApproachData
--	CROSS JOIN ContractClaimsIndicatorData
--	CROSS JOIN GeographicData
--	WHERE
--		IG3Level4 = N'1'
--		--AND IG3Level1 = N'AJ01'
--		AND IG3Level1 = @LLSPGCode
		
--)
--SELECT *
--INTO #ItemDataCombo
--FROM DataCombo

----select * from #ItemDataCombo
----order by 1,2,3,4,5, 6, 7


---- get the the actual sales data
--; WITH ItemSalesRollup AS (
--	SELECT
--		IG3Level4 AS ItemPyramidCode,
--		IG3Level1 AS LLSPGCode,
--		ItemUDVarChar20 AS ProductCode,
--		UDVarchar10 AS PriceApproach,
--		InvoiceLineGroup1UDVarchar2 AS ContractClaimsIndicator,
--		AG1Level4 AS Region,
--		AG1Level3 AS Area,
--		AG1Level2 AS Network,

--		SUM(TotalActualPrice) AS TotalSales,
--		SUM(TotalActualCost) AS TotalCost,
--		SUM(TotalQuantity) AS TotalQuantity,
--		SUM(TotalQuantity * UnitListPrice) AS TotalTradePrice,
--		--SUM(TotalActualPrice * il.FloorGPP) AS GTMFloor,
--		--SUM(TotalActualPrice * il.TargetGPP) AS GTMTarget,
--		--SUM(TotalActualPrice * il.StretchGPP) AS GTMStretch,
--		SUM(TotalActualPrice * il.FloorGPP)/NULLIF(SUM(TotalActualPrice), 0) AS CurrentFloorGPP,
--		SUM(TotalActualPrice * il.TargetGPP)/NULLIF(SUM(TotalActualPrice), 0) AS CurrentTargetGPP,
--		SUM(TotalActualPrice * il.StretchGPP)/NULLIF(SUM(TotalActualPrice), 0) AS CurrentStretchGPP
--	FROM FactInvoiceLinePricebandScore il
--	INNER JOIN dbo.DimAccountGroup1 dag1
--		ON dag1.AccountGroup1Key = il.AccountGroup1Key
--	INNER JOIN dbo.DimItem di
--		ON di.ItemKey = il.ItemKey
--	INNER JOIN dbo.DimItemGroup3 dig3
--		ON dig3.ItemGroup3Key = di.ItemGroup3Key
--	INNER JOIN dbo.DimInvoiceLineGroup1 dilg1
--		ON dilg1.InvoiceLineGroup1Key = il.InvoiceLineGroup1Key
--	INNER JOIN dbo.FactLastPriceAndCost flpac
--		ON flpac.AccountKey = il.AccountKey
--		AND flpac.ItemKey = il.ItemKey
--		AND flpac.InvoiceLineGroup1Key = il.InvoiceLineGroup1Key
--	WHERE 
--		IG3Level4 = N'1'
--		--AND IG3Level1 = N'AJ01'	--@LLSPGCode
--		AND IG3Level1 = @LLSPGCode
--	GROUP BY
--		IG3Level4,
--		IG3Level1,
--		ItemUDVarChar20,
--		UDVarchar10,
--		InvoiceLineGroup1UDVarchar2,
--		AG1Level4,
--		AG1Level3,
--		AG1Level2
--)
--SELECT
--	ItemPyramidCode,
--	LLSPGCode,
--	ProductCode,
--	PriceApproach,
--	ContractClaimsIndicator,
--	Region,
--	Area,
--	Network,

--	CurrentFloorGPP,
--	CurrentTargetGPP,
--	CurrentStretchGPP,
	
--	1 - (rp.Price / (TotalTradePrice/TotalQuantity)) AS CurrentFloorDiscount,
--	1 - (ap.Price / (TotalTradePrice/TotalQuantity)) AS CurrentTargetDiscount,
--	1 - (gp.Price / (TotalTradePrice/TotalQuantity)) AS CurrentGreeDiscount,

--	rp.Price AS CurrentFloorPrice,
--	ap.Price AS CurrentTargetPrice,
--	gp.Price AS CurrentStretchPrice

--INTO #ItemSalesRollupData
--FROM 
--	ItemSalesRollup
--OUTER APPLY lion.fn_GetPriceWeighted(TotalCost, TotalQuantity, CurrentFloorGPP) rp
--OUTER APPLY lion.fn_GetPriceWeighted(TotalCost, TotalQuantity, CurrentTargetGPP) ap
--OUTER APPLY lion.fn_GetPriceWeighted(TotalCost, TotalQuantity, CurrentStretchGPP) gp

---- create all the item specific row data
--INSERT lion.ATKManualPriceBandExport (
--	ItemPyramidCode,
--	LLSPGCode,
--	ProductCode,
--	PriceApproach,
--	ContractClaimsIndicator,
--	Region,
--	Area,
--	Network,
	
--	CurrentFloorGPP,
--	CurrentTargetGPP,
--	CurrentStretchGPP,

--	CurrentFloorDiscount,
--	CurrentTargetDiscount,
--	CurrentStretchDiscount,

--	CurrentFloorPrice, 
--	CurrentTargetPrice, 
--	CurrentStretchPrice, 

--	ProductCost, 
--	TradePrice
--)
--SELECT 
--	dc.ItemPyramidCode,
--	dc.LLSPGCode,
--	dc.ProductCode,
--	dc.PriceApproach,
--	dc.ContractClaimsIndicator,
--	dc.Region,
--	dc.Area,
--	dc.Network,

--	CurrentFloorGPP,
--	CurrentTargetGPP,
--	CurrentStretchGPP,
	
--	CurrentFloorDiscount,
--	CurrentTargetDiscount,
--	CurrentGreeDiscount,

--	CurrentFloorPrice,
--	CurrentTargetPrice,
--	CurrentStretchPrice,

--	NULL AS CurrProductCost,
--	NULL AS CurrTradePrice
--FROM #ItemDataCombo dc
--LEFT 
--JOIN #ItemSalesRollupData rd ON
--	rd.ItemPyramidCode = dc.ItemPyramidCode
--	AND rd.LLSPGCode = dc.LLSPGCode
--	AND rd.ProductCode = dc.ProductCode
--	AND rd.PriceApproach = dc.PriceApproach
--	AND rd.ContractClaimsIndicator = dc.ContractClaimsIndicator
--	AND rd.Region = dc.Region
--	AND rd.Area = dc.Area
--	AND rd.Network = dc.Network
----OUTER APPLY lion.fn_GetManualPriceBand (
----	dc.ItemPyramidCode,
----	dc.LLSPGCode,
----	N'ALL',
----	dc.PriceApproach,
----	dc.ContractClaimsIndicator,
----	dc.Region,
----  dc.Area,
----	dc.Network
----	) mpb
--SELECT @RowsI = @RowsI + @@ROWCOUNT



----/*** return the combined sets of data ***/
----SELECT
----NEWID() AS GUID,
----*
----FROM #LLSPGRollup
----UNION
----SELECT
----NEWID() AS GUID,
----*
----FROM #ItemRollup
----ORDER BY 2,3,4,5, 6, 7, 8





--DROP TABLE #LLSPGDataCombo
--DROP TABLE #LLSPGSalesRollupData
----DROP TABLE #LLSPGRollup

--DROP TABLE #ItemDataCombo
--DROP TABLE #ItemSalesRollupData
----DROP TABLE #ItemRollup


SELECT @RowsI AS NumberOfRows
GO
