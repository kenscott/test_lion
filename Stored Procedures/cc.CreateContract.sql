SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO






CREATE PROCEDURE [cc].[CreateContract](@WebUserKey INT, @BatchKey INT )
/*
Purpose: Based on the webuserkey and batchkey creates a new contract for each
         entry in the SupplierContractImport table.

JIRA: LPF-2441 A user can import a new contract
	
Application:  At the time of writing, this is being used exclusively by the
			  import contract process in the application.

Sample usage:  exec cc.CreateContract 1965, 1

Examples:
		
*/

AS 

-- For Testing...
--DECLARE @BatchKey INT
--DECLARE @WebUserKey INT 
--SET @BatchKey = xxx
--SET @WebUserKey = 1965

--DECLARE @Originator NVARCHAR(250)
--DECLARE @ContractNumber NVARCHAR(250)
--DECLARE @NewContractKey INT 

SET NOCOUNT ON

BEGIN TRAN


/* 
	Insert a log record into the new 
	select top 10 * from cc.SupplierContractLog
*/
--INSERT INTO cc.SupplierContractLog (WebUserKey, ContractKey, NewStatusKey, Notes, ContractAction )
--	VALUES(@WebUserKey, @ContractKey, 2, 'Contract was cloned from ' + CAST(@ContractKey AS VARCHAR(15)), 'Clone Contract' )

COMMIT TRAN

--SELECT @NewContractKey AS NewContractKey










GO
