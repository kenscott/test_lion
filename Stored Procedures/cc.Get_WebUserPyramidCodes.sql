SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [cc].[Get_WebUserPyramidCodes](@WebUserKey INT)
AS

/*
--LPF-5720 
I need a stored procedure such that, given a webuserkey, it returns all pyramid codes that the referenced user can see. 
That is, if the user is a branch or network manager, it would return the pyramid codes of their owned branch and any owned 
branches of managers under them. For contract coordinators and contract viewers, it would return all valid pyramid codes.

LPF-6032, Merged Code | Branch Manager of a pyramid 2 location can view pyramid code 1 contracts.  
Tester.Two is an owner of a pyramid 2 branch, however, it can see pyramid code 1 contracts.
This does not seem to be an issue for Tester.Four which owns a pyramid 1 branch.
*/

/*		Testing
DECLARE @WebUserKey INT
SET @WebUserKey = 1229
 --networkManager
SET @WebUserKey = 1972
 --ContractCoordinator
SET @WebUserKey = 1228
 --branchmanager
*/

/*
daniel.lee = 1354 (networkManager)
adrian.ward = 1243
amanda.lowe = 1262
*/


SET NOCOUNT ON

--Show all pyramidcodes for a webuser that is contract coordinators and contract viewers
IF @WebUserKey IN ( SELECT DISTINCT
                            WebUserKey
                    FROM    WebUserRole wur
                            JOIN WebRole wr ON wr.WebRoleKey = wur.WebRoleKey
                    WHERE   wr.WebRoleName = 'ContractCoordinator'
                            OR wr.WebRoleName = 'ContractViewer' )
    BEGIN	
        SELECT DISTINCT
                BranchPyramidCode
        FROM    lion.vwAccount
        WHERE   Inactive = 'false'
    END

--show ALL pyramidcodes FOR a webuser that networkManager
IF @WebUserKey IN ( SELECT  WebUserKey
                    FROM    WebUserRole wur
                            JOIN WebRole wr ON wr.WebRoleKey = wur.WebRoleKey
                    WHERE   wr.WebRoleName = 'NetworkManager' )
    BEGIN
        SELECT DISTINCT
                BranchPyramidCode
        FROM    lion.vwAccount
        WHERE   Inactive = 'false'
                AND AccountManagerKey IN (
                SELECT DISTINCT
                        damSub.AccountManagerKey
                FROM    BridgeAccountManager AS bam
                        INNER JOIN DimAccountManager AS damParent ON damParent.AccountManagerKey = bam.ParentAccountManagerKey
                        INNER JOIN WebUser AS wuParent ON wuParent.AccountManagerKey = damParent.AccountManagerKey
                        INNER JOIN DimAccountManager AS damSub ON damSub.AccountManagerKey = bam.SubsidiaryAccountManagerKey
                        INNER JOIN DimAccountGroup1 AS dag1 ON dag1.AG1Level1PersonKey = damSub.DimPersonKey
                WHERE   ( dag1.Converted = 1 )
                        AND ( wuParent.WebUserKey = @WebUserKey ) )
        UNION
        SELECT DISTINCT
                BranchPyramidCode
        FROM    lion.vwAccount
        WHERE   Inactive = 'false'
                AND AccountManagerKey IN (
                SELECT DISTINCT
                        damParent.AccountManagerKey AS Expr1
                FROM    DimAccountManager AS damParent
                        INNER JOIN WebUser AS wuParent ON wuParent.AccountManagerKey = damParent.AccountManagerKey
                WHERE   ( wuParent.WebUserKey = @WebUserKey ) )


    END

--show ALL pyramidcodes FOR a webuser that are Branch Managers
IF @WebUserKey IN ( SELECT  WebUserKey
                    FROM    WebUserRole wur
                            JOIN WebRole wr ON wr.WebRoleKey = wur.WebRoleKey
                    WHERE   wr.WebRoleName = 'BranchManager' )
    BEGIN
        SELECT DISTINCT
                BranchPyramidCode
        FROM    lion.vwAccount
        WHERE   Inactive = 'false'
                AND AccountManagerKey IN (
                SELECT DISTINCT
                        --wu.WebUserKey 
						wu.AccountManagerKey    --LPF-6032
                FROM    WebUserDelegate AS wud
                        INNER JOIN WebUser AS wu ON wu.WebUserKey = wud.DelegatorWebUserKey
                        INNER JOIN DimPerson AS dp ON dp.ODSPersonKey = wu.ODSPersonKey
                        INNER JOIN DimAccountGroup1 AS dag1 ON dag1.AG1Level1PersonKey = dp.DimPersonKey
                                                              AND dag1.Converted = 1
                WHERE   ( wud.DelegateWebUserKey = @WebUserKey ) )
    END








GO
