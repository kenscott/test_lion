SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE   PROCEDURE [cc].[Load_SupplierContractNote] AS 
/*
   LPF-5391, CC-1786 IM364753 - Contract Manager - Activities Tab | Issue with contract notes.
   There is an issue when a user saves a note on a contract. The note looks to have been replaced with the oldest note on the contract after the overnight process.
   This would be an update to the cc.Load_SupplierContractNote, only update notes if the contract source key is 1. - Melanie 
   Added line 64 "AND sc.ContractSourceKey = 1" to filter update, ken scott 07.31.2017
*/

BEGIN

	SET NOCOUNT ON

	EXEC LogDCPEvent 'cc.Load_SupplierContractNote', 'B'

	DECLARE @RowsI INT
	DECLARE @RowsU INT

-- Insert Contract Notes
	;WITH Con AS	(SELECT COUNT(*) OVER (PARTITION BY OriginatorBranchOrRegion+ContractNumber) ConCount, * FROM Lion_Staging.dbo.Contract)
	
	INSERT INTO cc.SupplierContractNote
			([WebUserKey]
			,[ContractKey]
            ,[Notes]
            ,[NoteType]
            ,[CreationDate])
	SELECT
			1,
			sc.ContractKey, 
			c.Narrative1 + ' ' + ISNULL(c.Narrative2, '') + ' ' + ISNULL(c.Narrative3, '') + ' ' + ISNULL(c.Narrative4, ''), 
			1, 
			ISNULL(sc.ContractCreationDT, GETDATE())
	FROM
		Con c
		INNER JOIN cc.SupplierContract sc
			ON sc.ContractID = c.OriginatorBranchOrRegion + c.ContractNumber
		LEFT JOIN cc.SupplierContractNote scn
			ON scn.ContractKey = sc.ContractKey
	WHERE scn.ContractKey IS NULL
	AND c.Narrative1 IS NOT NULL
	AND c.Narrative1 + ' ' + ISNULL(c.Narrative2, '') + ' ' + ISNULL(c.Narrative3, '') + ' ' + ISNULL(c.Narrative4, '') <> ''
	AND (c.ConCount = 1 OR c.Region = c.ContractOwner)

	SELECT @RowsI = @@ROWCOUNT

-- Check for Updates
	UPDATE cc.SupplierContractNote
	SET Notes = c.Narrative1 + ' ' + ISNULL(c.Narrative2, '') + ' ' + ISNULL(c.Narrative3, '') + ' ' + ISNULL(c.Narrative4, '')
	FROM
		Lion_Staging.dbo.[Contract] c
		INNER JOIN cc.SupplierContract sc
			ON sc.ContractID = c.OriginatorBranchOrRegion + c.ContractNumber
		INNER JOIN cc.SupplierContractNote scn
			ON scn.ContractKey = sc.ContractKey
	WHERE scn.Notes <> c.Narrative1 + ' ' + ISNULL(c.Narrative2, '') + ' ' + ISNULL(c.Narrative3, '') + ' ' + ISNULL(c.Narrative4, '')
	AND c.Narrative1 + ' ' + ISNULL(c.Narrative2, '') + ' ' + ISNULL(c.Narrative3, '') + ' ' + ISNULL(c.Narrative4, '') <> ''
	AND sc.ContractSourceKey = 1

	SELECT @RowsU = @@ROWCOUNT

	EXEC LogDCPEvent 'cc.Load_SupplierContractNote', 'E', @RowsI, @RowsU, 0

END
GO
