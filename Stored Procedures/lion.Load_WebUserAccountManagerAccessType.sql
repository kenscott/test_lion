SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [lion].[Load_WebUserAccountManagerAccessType]
WITH EXECUTE AS CALLER
AS
SET NOCOUNT ON

EXEC LogDCPEvent 'ETL - lion.Load_WebUserAccountManagerAccessType', 'B'

DECLARE 
	@RowsI Int_type,
	@RowsU Int_type,
	@RowsD Int_type,
	@WR1 INT,
	@WR2 INT,
	@WU1 INT

SET @RowsI = 0
SET @RowsU = 0
SET @RowsD = 0



/* set tables for hierarchy */

-- fully rebuild this table based on what's in UserAssoc
TRUNCATE TABLE WebUserAccountManager

INSERT WebUserAccountManager (WebUserKey, AccountManagerKey, AccessType, Version)
SELECT DISTINCT WebUserKey, AccountManagerKey, 'Modify', 0
FROM WebUser
WHERE AccountManagerKey IS NOT NULL



SELECT @WR1 = WebRoleKey FROM dbo.WebRole WHERE WebRoleDescription = 'Administrator'
SELECT @WR2 = WebRoleKey FROM dbo.WebRole WHERE WebRoleDescription = 'P2PUser'

SELECT @WU1 = (SELECT MIN(WebUserKey) FROM dbo.WebUser wu INNER JOIN dbo.ODSPerson o ON o.ODSPersonKey = wu.ODSPersonKey WHERE o.FullName = 'Coordinator' OR o.ExtID = '0')


IF NOT EXISTS (SELECT * FROM dbo.WebUserRole WHERE WebUserKey = @WU1)
BEGIN
	INSERT dbo.WebUserRole (WebUserKey, WebRoleKey) VALUES (@WU1, @WR1)
	INSERT dbo.WebUserRole (WebUserKey, WebRoleKey) VALUES (@WU1, @WR2)
END

/* made use of a temp table with an index for performance */
/* went from an hour to 45 seconds or less!!! */
-- DECLARE @a table (WebUserKey INT, SubsidiaryAccountManagerKey INT, AccessType VARCHAR(50))
CREATE TABLE #A (
	WebUserKey INT, 
	SubsidiaryAccountManagerKey INT,
	AccessType VARCHAR(50)
)


--Gets all trees for this webuser
INSERT INTO #A (WebUserKey, SubsidiaryAccountManagerKey, AccessType)
SELECT DISTINCT 
	WebUserKey, 
	SubsidiaryAccountManagerKey, 
	AccessType
FROM dbo.WebUserAccountManager u
INNER JOIN dbo.BridgeAccountManager am ON 
	u.AccountManagerKey = am.ParentAccountManagerKey

CREATE UNIQUE CLUSTERED INDEX [I_A_1] ON #A([WebUserKey], [SubsidiaryAccountManagerKey], [AccessType]) ON [PRIMARY]


TRUNCATE TABLE dbo.WebUserAccountManagerAccessType


--Shows the winning AccessType for each Subsidiary for each webuser (Each Subsidiary can have only one accesstype)
INSERT INTO dbo.WebUserAccountManagerAccessType (WebUserKey, AccountManagerKey, AccessType)
SELECT DISTINCT 
	WebUserKey, 
	SubsidiaryAccountManagerKey,
	CASE
		WHEN a1.AccessType = 'Modify' THEN 'Modify'
		WHEN
			a1.AccessType = 'View' AND 
			EXISTS (
				SELECT 1 
				FROM #A a2 
				WHERE 
					a1.WebUserKey = a2.WebUSerKey AND 
					a1.SubsidiaryAccountManagerKey = a2.SubsidiaryAccountManagerKey AND 
					a2.AccessType = 'Modify'
			)
		THEN 'Modify'
		ELSE 'View'
	END AS 'AccessType'
FROM #A a1
ORDER BY 
	WebUserKey, 
	SubsidiaryAccountManagerKey, 
	AccessType
SET @RowsI = @RowsI + @@RowCount


DROP TABLE #A



TRUNCATE TABLE dbo.WebUserView
INSERT dbo.WebUserView(ViewerWebUserKey, ViewedWebUserKey)
SELECT DISTINCT
	wuamat.WebUserKey, 
	wu.WebUserKey
FROM dbo.WebUserAccountManagerAccessType wuamat
INNER JOIN dbo.WebUser wu
	ON wu.AccountManagerKey = wuamat.AccountManagerKey
UNION
SELECT DISTINCT
	WebUserKey, 
	WebUserKey
FROM dbo.WebUser

/*per LPF-1260 and approved by KK */
INSERT INTO dbo.WebUserView (ViewerWebUserKey, ViewedWebUserKey)
SELECT DISTINCT wur.WebUserKey, wuv.ViewedWebUserKey
FROM dbo.WebUserRole wur
JOIN dbo.WebRole wr ON wr.WebRoleKey = wur.WebRoleKey,
dbo.WebUserView wuv
WHERE wr.WebRoleName='PricingCoordinator'
AND wuv.ViewerWebUserKey = (SELECT MIN(WebUserKey) AS WebUserKey
							FROM dbo.WebUser wu
							INNER JOIN dbo.ODSPerson odsp
								ON odsp.ODSPersonKey = wu.ODSPersonKey
							WHERE
								odsp.LevelId = 'CentralPricing')
EXCEPT (SELECT ViewerWebUserKey, ViewedWebUserKey FROM dbo.WebUserView)
/*end of LPF-1260*/





EXEC LogDCPEvent 'ETL - lion.Load_WebUserAccountManagerAccessType', 'E', @RowsI, @RowsU, @RowsD



GO
