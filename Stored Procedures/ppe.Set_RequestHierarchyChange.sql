SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE PROCEDURE [ppe].[Set_RequestHierarchyChange]
AS /* 

LIO-1045 Modify ETL to "clean up" existing requests for Account Owner changes
LIO-1058 Modify ETL to "clean up" existing requests for Hierarchy changes
LPF-3677 Change ETL to remove branch manager, network manager roles


BEGIN TRAN

EXEC ppe.Set_RequestHierarchyChange
EXEC DCPLOG

ROLLBACK TRAN

*/


    SET NOCOUNT ON


    EXEC LogDCPEvent 'ETL - ppe.Set_RequestHierarchyChange', 'B'


    DECLARE @RowsI INT ,
        @RowsU INT ,
        @RowsD INT ,
        @Level1PendingStatusKey INT ,
        @Level2PendingStatusKey INT ,
        @Level3PendingStatusKey INT ,
        @DBWebUserKey Key_Normal_type ,
        @RMWebUserKey Key_Normal_type

    SET @RowsI = 0
    SET @RowsU = 0
    SET @RowsD = 0

    SELECT  @Level1PendingStatusKey = ( SELECT  StatusKey
                                        FROM    ppe.RequestStatus
                                        WHERE   StatusName = 'Level 1 Pending'
                                      )
    SELECT  @Level2PendingStatusKey = ( SELECT  StatusKey
                                        FROM    ppe.RequestStatus
                                        WHERE   StatusName = 'Level 2 Pending'
                                      )
    SELECT  @Level3PendingStatusKey = ( SELECT  StatusKey
                                        FROM    ppe.RequestStatus
                                        WHERE   StatusName = 'Level 3 Pending'
                                      )
    SELECT  @DBWebUserKey = ISNULL(( SELECT WebUserKey
                                     FROM   dbo.WebUser
                                     WHERE  UserName = 'David.Boothman'
                                   ), 1)
    SELECT  @RMWebUserKey = ISNULL(( SELECT WebUserKey
                                     FROM   dbo.WebUser
                                     WHERE  UserName = 'Rehan.Mogul'
                                   ), 1)



    DECLARE @UpdatedRowData TABLE
        (
          RequestKey INT NOT NULL ,
          OldStatusKey INT NOT NULL ,
          NewStatusKey INT NOT NULL ,
          TypeKey INT NOT NULL ,
          OldOwnerWebUserKey INT NOT NULL ,
          NewOwnerWebUserKey INT NOT NULL
        )


    BEGIN TRAN


/*** Level 3 hierarchy check ***/
    UPDATE  rm
    SET     rm.PriorOwnerWebUserKey = rm.OwnerWebUserKey ,
            rm.OwnerWebUserKey = @DBWebUserKey
    OUTPUT  INSERTED.RequestKey ,
            DELETED.StatusKey ,
            INSERTED.StatusKey ,
            INSERTED.TypeKey ,
            DELETED.OwnerWebUserKey ,
            INSERTED.OwnerWebUserKey
            INTO @UpdatedRowData
    FROM    ppe.RequestMain rm
            JOIN ppe.RequestStatus rs ON rs.StatusKey = rm.StatusKey
            INNER JOIN dbo.WebUser wuRequestOwner ON wuRequestOwner.WebUserKey = rm.OwnerWebUserKey
            INNER JOIN dbo.ODSPerson odspRequestOwner ON odspRequestOwner.ODSPersonKey = wuRequestOwner.ODSPersonKey
    WHERE   rs.StatusName = 'Level 3 Pending'
            AND ISNULL(odspRequestOwner.LevelId, '') <> 'CentralPricing'
            AND odspRequestOwner.ExtId NOT IN ( 'Chris.Cottington',
												'David.Boothman',
                                                'Rehan.Mogul' )
    SET @RowsU = @RowsU + @@RowCount


/*** Level 2 hierarchy check ***/
    UPDATE  rm
    SET     rm.PriorOwnerWebUserKey = rm.OwnerWebUserKey ,
            rm.OwnerWebUserKey = ISNULL(wuBM.WebUserKey,
                                        wuAccountOwner.WebUserKey) ,
            rm.StatusKey = CASE WHEN odspNewOwner.LevelId = 'BranchManager'
                                THEN @Level1PendingStatusKey
                                WHEN odspNewOwner.LevelId = 'NetworkManager'
                                THEN @Level2PendingStatusKey
                                WHEN odspNewOwner.LevelId = 'CentralPricing'
                                THEN @Level3PendingStatusKey
                                WHEN ISNULL(wuBM.WebUserKey,
                                            wuAccountOwner.WebUserKey) = @RMWebUserKey
                                THEN @Level3PendingStatusKey	-- assign Rehan's request to him at level 3 - https://enterbridge.atlassian.net/browse/LP2P-775 
                                ELSE rm.StatusKey
                           END
    OUTPUT  INSERTED.RequestKey ,
            DELETED.StatusKey ,
            INSERTED.StatusKey ,
            INSERTED.TypeKey ,
            DELETED.OwnerWebUserKey ,
            INSERTED.OwnerWebUserKey
            INTO @UpdatedRowData
    FROM    ppe.RequestMain rm
            INNER JOIN ppe.RequestStatus rs ON rs.StatusKey = rm.StatusKey
            INNER JOIN dbo.DimAccount da ON da.AccountKey = rm.AccountKey
            INNER JOIN dbo.WebUser wuAccountOwner ON wuAccountOwner.AccountManagerKey = da.AccountManagerKey
            INNER JOIN dbo.DimItemGroup3 dig3 ON dig3.ItemGroup3Key = rm.ItemGroup3Key
            INNER JOIN dbo.DimBrand db ON db.BrandKey = dig3.BrandKey
            LEFT JOIN dbo.DimAccountBrandManager dabm ON dabm.AccountKey = rm.AccountKey
                                                         AND dabm.BrandKey = db.BrandKey
            LEFT JOIN dbo.WebUser wuBM ON wuBM.AccountManagerKey = dabm.AccountManagerKey
            INNER JOIN dbo.ODSPerson odspNewOwner ON odspNewOwner.ODSPersonKey = ISNULL(wuBM.ODSPersonKey,
                                                              wuAccountOwner.ODSPersonKey)
            INNER JOIN dbo.WebUser wuRequestOwner ON wuRequestOwner.WebUserKey = rm.OwnerWebUserKey
    WHERE   rs.StatusName IN ( 'Level 2 Pending' )
            AND wuRequestOwner.AccountManagerKey NOT IN (			-- is the current owner visible in the account owner chain
            SELECT  ParentAccountManagerKey						-- get list of accountmanager that can see this account
            FROM    dbo.BridgeAccountManager bam
            WHERE   bam.SubsidiaryAccountManagerKey = da.AccountManagerKey )
            AND wuRequestOwner.AccountManagerKey NOT IN (			-- is the current owner visible in the brand owner chain
            SELECT  ParentAccountManagerKey						-- get list of accountmanagers that can see this brand manager
            FROM    dbo.BridgeAccountManager bam
            WHERE   bam.SubsidiaryAccountManagerKey = ISNULL(wuBM.AccountManagerKey,
                                                             da.AccountManagerKey) )
    SET @RowsU = @RowsU + @@RowCount


/*** Level 1 hierarchy check ***/
    UPDATE  rm
    SET     rm.PriorOwnerWebUserKey = rm.OwnerWebUserKey ,
            rm.OwnerWebUserKey = ISNULL(wuBM.WebUserKey,
                                        wuAccountOwner.WebUserKey) ,
            rm.StatusKey = CASE WHEN ISNULL(wuBM.WebUserKey,
                                            wuAccountOwner.WebUserKey) = @RMWebUserKey
                                THEN @Level3PendingStatusKey	-- assign Rehan's request to him at level 3 - https://enterbridge.atlassian.net/browse/LP2P-775 
                                WHEN ISNULL(wuBM.WebUserKey,
                                            wuAccountOwner.WebUserKey) = @DBWebUserKey
                                THEN @Level3PendingStatusKey	-- should proably do the same as above for Chris C.
                                ELSE rm.StatusKey
                           END
    OUTPUT  INSERTED.RequestKey ,
            DELETED.StatusKey ,
            INSERTED.StatusKey ,
            INSERTED.TypeKey ,
            DELETED.OwnerWebUserKey ,
            INSERTED.OwnerWebUserKey
            INTO @UpdatedRowData
    FROM    ppe.RequestMain rm
            INNER JOIN ppe.RequestStatus rs ON rs.StatusKey = rm.StatusKey
            INNER JOIN dbo.DimAccount da ON da.AccountKey = rm.AccountKey
            INNER JOIN dbo.WebUser wuAccountOwner ON wuAccountOwner.AccountManagerKey = da.AccountManagerKey
            INNER JOIN dbo.DimItemGroup3 dig3 ON dig3.ItemGroup3Key = rm.ItemGroup3Key
            INNER JOIN dbo.DimBrand db ON db.BrandKey = dig3.BrandKey
            LEFT JOIN dbo.DimAccountBrandManager dabm ON dabm.AccountKey = rm.AccountKey
                                                         AND dabm.BrandKey = db.BrandKey
            LEFT JOIN dbo.WebUser wuBM ON wuBM.AccountManagerKey = dabm.AccountManagerKey
            INNER JOIN dbo.WebUser wuRequestOwner ON wuRequestOwner.WebUserKey = rm.OwnerWebUserKey
    WHERE   rs.StatusName IN ( 'Level 1 Pending' )
            AND rm.OwnerWebUserKey <> ISNULL(wuBM.WebUserKey,
                                             wuAccountOwner.WebUserKey)
    SET @RowsU = @RowsU + @@RowCount


--- insert into the request log noting the changes that were just made
    INSERT  INTO ppe.RequestLog
            ( WebUserKey ,
              RequestKey ,
              OldStatusKey ,
              NewStatusKey ,
              RequestType ,
              RequestAction ,
              EmailSent ,
              OldOwnerWebUserKey ,
              NewOwnerWebUserKey ,
              Notes
            )
            SELECT  1 ,
                    RequestKey ,
                    OldStatusKey ,
                    NewStatusKey ,
                    TypeKey ,
                    'REASSIGN' AS RequestAction ,
                    0 AS EmailSent ,
                    OldOwnerWebUserKey ,
                    NewOwnerWebUserKey ,
                    'Reassignment due to an owner change' AS Notes
            FROM    @UpdatedRowData
    SET @RowsI = @RowsI + @@RowCount





--SELECT TOP (@RowsI) * FROM ppe.RequestLog ORDER BY 1 DESC






    DELETE  FROM @UpdatedRowData

    UPDATE  rm
    SET     rm.StatusKey = CASE WHEN odsp.LevelId = 'BranchManager'
                                THEN @Level1PendingStatusKey
                                WHEN odsp.LevelId = 'NetworkManager'
                                THEN @Level2PendingStatusKey
                                WHEN odsp.LevelId = 'CentralPricing'
                                THEN @Level3PendingStatusKey
                                ELSE rm.StatusKey
                           END
    OUTPUT  INSERTED.RequestKey ,
            DELETED.StatusKey ,
            INSERTED.StatusKey ,
            INSERTED.TypeKey ,
            DELETED.OwnerWebUserKey ,
            INSERTED.OwnerWebUserKey
            INTO @UpdatedRowData
    FROM    ppe.RequestMain rm
            JOIN ppe.RequestStatus rs ON rs.StatusKey = rm.StatusKey
            INNER JOIN dbo.WebUser wuRequestOwner ON wuRequestOwner.WebUserKey = rm.OwnerWebUserKey
            INNER JOIN dbo.DimItemGroup3 dig3 ON dig3.ItemGroup3Key = rm.ItemGroup3Key
            INNER JOIN dbo.DimBrand db ON db.BrandKey = dig3.BrandKey
            INNER JOIN dbo.DimAccountBrandManager dabm ON dabm.AccountKey = rm.AccountKey
                                                          AND dabm.BrandKey = db.BrandKey
            INNER JOIN dbo.WebUser wuBM ON wuBM.AccountManagerKey = dabm.AccountManagerKey
            INNER JOIN dbo.ODSPerson odsp ON odsp.ODSPersonKey = wuBM.ODSPersonKey
    WHERE   rs.StatusName IN ( 'Level 1 Pending', 'Level 2 Pending',
                               'Level 3 Pending' )
	--AND wuAccountManager.WebUserKey = rm.OwnerWebUserKey
            AND wuRequestOwner.AccountManagerKey = dabm.AccountManagerKey  -- is it currently assigned the brand manager
            AND rm.StatusKey <> CASE WHEN odsp.LevelId = 'BranchManager'
                                     THEN @Level1PendingStatusKey
                                     WHEN odsp.LevelId = 'NetworkManager'
                                     THEN @Level2PendingStatusKey
                                     WHEN odsp.LevelId = 'CentralPricing'
                                     THEN @Level3PendingStatusKey
                                     ELSE rm.StatusKey
                                END
    SET @RowsU = @RowsU + @@RowCount

--/*
--Scott says that he does not want a request log entry for these status updates

    INSERT  INTO ppe.RequestLog
            ( WebUserKey ,
              RequestKey ,
              OldStatusKey ,
              NewStatusKey ,
              RequestType ,
              RequestAction ,
              EmailSent ,
              OldOwnerWebUserKey ,
              NewOwnerWebUserKey ,
              Notes
            )
            SELECT  1 ,
                    RequestKey ,
                    OldStatusKey ,
                    NewStatusKey ,
                    TypeKey ,
                    'REASSIGN' AS RequestAction ,
                    0 AS EmailSent ,
                    OldOwnerWebUserKey ,
                    NewOwnerWebUserKey ,
                    'Status update due to a hierarchy change' AS Notes
            FROM    @UpdatedRowData
    SET @RowsI = @RowsI + @@RowCount




--
/*** level 1 requests assigned to a level 3 manager get reassigned back to the level 1 manager ***/
/*
Ken Verbage
If request is pending level 1 AND request owner is Level3, revert to the level1 owner that that account should be.
*/

    UPDATE  rm
    SET     rm.OwnerWebUserKey = ISNULL(wuBM.ODSPersonKey,
                                        ( SELECT    ODSPersonKey
                                          FROM      ODSPerson
                                          WHERE     ( ODSPersonKey = da.AccountManagerKey )
                                        ))
    OUTPUT  INSERTED.RequestKey ,
            DELETED.StatusKey ,
            INSERTED.StatusKey ,
            INSERTED.TypeKey ,
            DELETED.OwnerWebUserKey ,
            INSERTED.OwnerWebUserKey
            INTO @UpdatedRowData
    FROM    ppe.RequestMain AS rm
            INNER JOIN ppe.RequestStatus AS rs ON rs.StatusKey = rm.StatusKey
            INNER JOIN WebUser AS wu ON wu.WebUserKey = rm.OwnerWebUserKey
            INNER JOIN ODSPerson AS odspRequestOwner ON odspRequestOwner.ODSPersonKey = wu.ODSPersonKey
            INNER JOIN DimAccount AS da ON da.AccountKey = rm.AccountKey
            INNER JOIN WebUser AS wuAccountOwner ON wuAccountOwner.AccountManagerKey = da.AccountManagerKey
            INNER JOIN DimItemGroup3 AS dig3 ON dig3.ItemGroup3Key = rm.ItemGroup3Key
            INNER JOIN DimBrand AS db ON db.BrandKey = dig3.BrandKey
            LEFT OUTER JOIN DimAccountBrandManager AS dabm ON dabm.AccountKey = rm.AccountKey
                                                              AND dabm.BrandKey = db.BrandKey
            LEFT OUTER JOIN WebUser AS wuBM ON wuBM.AccountManagerKey = dabm.AccountManagerKey
            INNER JOIN WebUser AS wuRequestOwner ON wuRequestOwner.WebUserKey = rm.OwnerWebUserKey
    WHERE   odspRequestOwner.LevelId = 'CentralPricing'
            AND rs.StatusName = 'Level 1 Pending'





    INSERT  INTO ppe.RequestLog
            ( WebUserKey ,
              RequestKey ,
              OldStatusKey ,
              NewStatusKey ,
              RequestType ,
              RequestAction ,
              EmailSent ,
              OldOwnerWebUserKey ,
              NewOwnerWebUserKey ,
              Notes
            )
            SELECT  1 ,
                    RequestKey ,
                    OldStatusKey ,
                    NewStatusKey ,
                    TypeKey ,
                    'REASSIGN' AS RequestAction ,
                    0 AS EmailSent ,
                    OldOwnerWebUserKey ,
                    NewOwnerWebUserKey ,
                    'Status update due to level 1 requests assigned to a level 3 manager' AS Notes
            FROM    @UpdatedRowData
    SET @RowsI = @RowsI + @@RowCount

    DELETE  FROM @UpdatedRowData


    SELECT TOP ( @RowsI )
            *
    FROM    ppe.RequestLog
    ORDER BY RequestKey DESC

--*/


    COMMIT TRAN


    EXEC LogDCPEvent 'ETL - ppe.Set_RequestHierarchyChange', 'E', @RowsI,
        @RowsU, @RowsD





GO
