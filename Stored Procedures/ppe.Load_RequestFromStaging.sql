SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [ppe].[Load_RequestFromStaging] (@WebUserKey INT, @BatchKey INT) 
AS
BEGIN
  
	SET ANSI_WARNINGS OFF
	SET ARITHABORT OFF
	SET ARITHIGNORE ON

	--DECLARE @WebUserKey INT, @BatchKey INT
	--SELECT @WebUserKey =1, @BatchKey=2

	/*LPF-1831 remove any Pound symbols from data */
	UPDATE ppe.RequestStaging SET NewExceptionFixedPrice = REPLACE(NewExceptionFixedPrice,CHAR(163),'' ) WHERE BatchKey=@BatchKey

	UPDATE ppe.RequestStaging SET ProductCode = NULL WHERE BatchKey = @BatchKey AND ProductCode=''
	UPDATE ppe.RequestStaging SET NewSPGDiscount1 = NULL WHERE BatchKey = @BatchKey AND NewSPGDiscount1=''
	UPDATE ppe.RequestStaging SET NewSPGDiscount2 = NULL WHERE BatchKey = @BatchKey AND NewSPGDiscount2=''
	UPDATE ppe.RequestStaging SET NewExceptionDiscount = NULL WHERE BatchKey = @BatchKey AND NewExceptionDiscount=''
	UPDATE ppe.RequestStaging SET NewExceptionFixedPrice = NULL WHERE BatchKey = @BatchKey AND NewExceptionFixedPrice=''
	UPDATE ppe.RequestStaging SET NewSPGFromDate = NULL WHERE BatchKey = @BatchKey AND NewSPGFromDate=''
	UPDATE ppe.RequestStaging SET NewExceptionStartDate = NULL WHERE BatchKey = @BatchKey AND NewExceptionStartDate=''
	UPDATE ppe.RequestStaging SET NewExceptionExpiryDate = NULL WHERE BatchKey = @BatchKey AND NewExceptionExpiryDate=''
	UPDATE ppe.RequestStaging SET DeleteTerm = NULL WHERE BatchKey = @BatchKey AND DeleteTerm=''

	UPDATE s SET ErrorMessage = ISNULL(ErrorMessage, '') + 'Invalid AccountNumber; '
	FROM ppe.RequestStaging  s
	LEFT JOIN dbo.DimAccount da ON da.AccountNumber = s.AccountNumber
	WHERE BatchKey=@BatchKey
	AND da.AccountKey IS NULL

	UPDATE s SET ErrorMessage = ISNULL(ErrorMessage, '') + 'Invalid LLSPG; '
	FROM ppe.RequestStaging  s
	LEFT JOIN dbo.DimItemGroup3 dig3 ON dig3.IG3Level1 = s.SPGCode AND dig3.IG3Level4 = '1'
	WHERE BatchKey=@BatchKey
	AND (dig3.ItemGroup3Key IS NULL OR s.SPGCode ='XX99')

	UPDATE s SET ErrorMessage = ISNULL(ErrorMessage, '') + 'Invalid ProductCode; '
	FROM ppe.RequestStaging  s
	LEFT JOIN dbo.DimItem di ON di.ItemNumber = '1' + s.ProductCode
	WHERE BatchKey=@BatchKey
	AND ProductCode IS NOT NULL
	AND di.ItemKey IS NULL

	UPDATE s SET ErrorMessage = ISNULL(ErrorMessage, '') + 'Invalid NewSPGDiscount1; '
	FROM ppe.RequestStaging  s
	WHERE BatchKey=@BatchKey
	AND CASE
			WHEN ISNUMERIC(ISNULL(s.NewSPGDiscount1,'0')) = 0 THEN 0
			WHEN CAST(s.NewSPGDiscount1 AS FLOAT) NOT BETWEEN -1.0 AND 1.0  THEN 0
			ELSE 1
		END = 0

	UPDATE s SET ErrorMessage = ISNULL(ErrorMessage, '') + 'Invalid NewSPGDiscount2; '
	FROM ppe.RequestStaging  s
	WHERE BatchKey=@BatchKey
	AND CASE
			WHEN ISNUMERIC(ISNULL(s.NewSPGDiscount2,'0')) = 0 THEN 0
			WHEN CAST(s.NewSPGDiscount2 AS FLOAT) NOT BETWEEN -1.0 AND 1.0  THEN 0
			ELSE 1
		END = 0

	UPDATE s SET ErrorMessage = ISNULL(ErrorMessage, '') + 'Invalid NewExceptionDiscount; '
	FROM ppe.RequestStaging  s
	WHERE BatchKey=@BatchKey
	AND CASE
			WHEN ISNUMERIC(ISNULL(s.NewExceptionDiscount,'0')) = 0 THEN 0
			WHEN CAST(s.NewExceptionDiscount AS FLOAT) NOT BETWEEN -1.0 AND 1.0  THEN 0
			ELSE 1
		END = 0

	UPDATE s SET ErrorMessage = ISNULL(ErrorMessage, '') + 'Invalid NewExceptionFixedPrice; '
	FROM ppe.RequestStaging  s
	WHERE BatchKey=@BatchKey
	AND ISNUMERIC(ISNULL(s.NewExceptionFixedPrice,'0')) = 0

	UPDATE s SET ErrorMessage = ISNULL(ErrorMessage, '') + 'Invalid SPG Expiration Date; '
	FROM ppe.RequestStaging  s
	WHERE BatchKey=@BatchKey
	AND (ISDATE(ISNULL(s.NewSPGFromDate,'01/01/1900')) = 0 
			OR CAST(s.NewSPGFromDate AS DATE) < GETDATE())

	UPDATE s SET ErrorMessage = ISNULL(ErrorMessage, '') + 'Invalid NewExceptionStartDate; '
	FROM ppe.RequestStaging  s
	WHERE BatchKey=@BatchKey
	AND (ISDATE(ISNULL(s.NewExceptionStartDate,'01/01/1900')) = 0
			/*OR CAST(s.NewExceptionStartDate AS DATE) < GETDATE()*/  )

	UPDATE s SET ErrorMessage = ISNULL(ErrorMessage, '') + 'Invalid NewExceptionExpiryDate; '
	FROM ppe.RequestStaging  s
	WHERE BatchKey=@BatchKey
	AND (ISDATE(ISNULL(s.NewExceptionExpiryDate,'01/01/1900')) = 0
			/*OR CAST(s.NewExceptionExpiryDate AS DATE) < GETDATE()*/  )

	-- below supersceded by LPF-2178
	--UPDATE s SET ErrorMessage = ISNULL(ErrorMessage, '') + 'Discount and FixedPrice present; '
	--FROM ppe.RequestStaging  s
	--WHERE BatchKey=@BatchKey
	--AND s.NewExceptionDiscount IS NOT NULL
	--AND s.NewExceptionFixedPrice IS NOT NULL

	UPDATE s SET ErrorMessage = ISNULL(ErrorMessage, '') + 'Product Discount and FixedPrice missing; '
	FROM ppe.RequestStaging  s
	WHERE BatchKey=@BatchKey
	AND s.ProductCode IS NOT NULL 
	--AND ISNULL(s.DeleteTerm,'') <> 'Y' removed per LPF-2177
	AND s.NewExceptionDiscount IS NULL
	AND s.NewExceptionFixedPrice IS NULL

	----LPF-2777 
	--UPDATE s SET ErrorMessage = ISNULL(ErrorMessage, '') + 'SPG Discount missing; '
	--FROM ppe.RequestStaging  s
	--WHERE BatchKey= @BatchKey
	--AND s.ProductCode IS NULL 
	--AND s.NewSPGDiscount1 IS NULL 
	--AND s.NewSPGDiscount2 IS NULL

	-- removed per LPF-2031 / LPF-2042
	--UPDATE s SET ErrorMessage = ISNULL(ErrorMessage, '') + 'Cannot Replace Existing Request; '
	--FROM ppe.RequestStaging s
	--JOIN dbo.DimAccount da ON da.AccountNumber = s.AccountNumber
	--JOIN dbo.DimItemGroup3 dig3 ON dig3.IG3Level1 = s.SPGCode AND dig3.IG3Level4 = '1'
	--JOIN ppe.RequestMain r
	--	ON r.AccountKey = da.AccountKey
	--	AND r.ItemGroup3Key = dig3.ItemGroup3Key
	--	AND ISNULL(r.ProposedSPGDiscount1,-0.01) = CAST(CAST(ISNULL(s.NewSPGDiscount1,'-0.01') AS FLOAT) AS DEC(38,5))
	--	AND ISNULL(r.ProposedSPGDiscount2,-0.01) = CAST(CAST(ISNULL(s.NewSPGDiscount2,'-0.01') AS FLOAT) AS DEC(38,5))
	--	AND r.StatusKey IN (SELECT StatusKey FROM ppe.RequestStatus WHERE StatusRollup IN ('New','Pending'))
	--WHERE s.BatchKey=@BatchKey
	--AND s.ProductCode IS NULL	
	--AND s.ErrorMessage IS NULL

	--UPDATE s SET ErrorMessage = ISNULL(ErrorMessage, '') + 'Cannot Replace Existing Request; '
	--FROM ppe.RequestStaging s
	--JOIN dbo.DimAccount da ON da.AccountNumber = s.AccountNumber
	--JOIN dbo.DimItem di ON di.ItemNumber = '1' + s.ProductCode
	--JOIN ppe.RequestMain r
	--	ON r.AccountKey = da.AccountKey
	--	AND r.ItemKey = di.ItemKey
	--	AND ISNULL(r.ProposedExceptionDiscount, -0.01) = CAST(CAST(ISNULL(s.NewExceptionDiscount,'-0.01') AS FLOAT) AS DEC(38,5))
	--	AND ISNULL(r.ProposedExceptionFixedPrice, -0.01) = CAST(CAST(ISNULL(s.NewExceptionFixedPrice,'-0.01') AS FLOAT) AS DEC(38,5))
	--	AND ISNULL(r.ProposedExceptionStartDate, CAST('1-1-1900' AS DATE)) = CAST(ISNULL(s.NewExceptionStartDate,'1-1-1900') AS DATE )
	--	AND ISNULL(r.ProposedExceptionExpiryDate, CAST('1-1-1900' AS DATE)) = CAST(ISNULL(s.NewExceptionExpiryDate,'1-1-1900') AS DATE )
	--	AND r.StatusKey IN (SELECT StatusKey FROM ppe.RequestStatus WHERE StatusRollup IN ('New','Pending'))
	--WHERE s.BatchKey = @BatchKey
	--AND s.ProductCode IS NOT NULL
	--AND s.ErrorMessage IS NULL

	/*LPF-1666 example #1 duplicate SPGs no product terms */
	UPDATE s SET ErrorMessage = ISNULL(ErrorMessage, '') + 'Duplicated SPGs without product exceptions; '
	FROM ppe.RequestStaging s
	JOIN (SELECT SPGCode, AccountNumber
			FROM ppe.RequestStaging
			WHERE BatchKey=@BatchKey
			AND ProductCode IS NULL
			GROUP BY SPGCode, AccountNumber
			HAVING COUNT(*) > 1) d ON d.SPGCode = s.SPGCode AND d.AccountNumber = s.AccountNumber
	WHERE BatchKey=@BatchKey
	AND ProductCode IS NULL

	/*LPF-1666 example #2 duplicate SPGs with and without product terms */
	UPDATE s SET ErrorMessage = ISNULL(ErrorMessage, '') + 'Duplicated SPGs with and without exceptions; '
	FROM ppe.RequestStaging s
	JOIN (SELECT SPGCode, AccountNumber
			FROM ppe.RequestStaging
			WHERE BatchKey = @BatchKey
			GROUP BY SPGCode, AccountNumber
			HAVING COUNT(*) > 1
			AND COUNT(DISTINCT ISNULL(ProductCode,'')) <> COUNT(*) ) d 
		ON d.AccountNumber = s.AccountNumber 
		AND d.SPGCode = s.SPGCode
	WHERE s.BatchKey = @BatchKey

	/*LPF-1783 invalid status */
	UPDATE s SET ErrorMessage = ISNULL(ErrorMessage, '') + 'Invalid status; '
	FROM ppe.RequestStaging s
	LEFT JOIN ppe.RequestStatus rs
		on s.StatusRollup = rs.StatusRollup
	WHERE s.BatchKey = @BatchKey
	AND s.StatusRollup IS NOT NULL -- if has a value in the column
	AND rs.StatusRollup IS NULL -- but doesn't match an actual status

	/*LPF-1791 cannot load excluded items/SPGs */
	UPDATE s SET ErrorMessage = ISNULL(ErrorMessage, '') + 'Excluded Item; '
	FROM ppe.RequestStaging s
	JOIN dbo.DimItem di ON di.ItemUDVarchar20 = s.ProductCode
	JOIN ppe.ExcludeItem i ON i.ItemKey = di.ItemKey
	JOIN lion.Batch rb ON rb.BatchKey = s.BatchKey -- LPF-3104 excluded items permitted if 'revert'
	WHERE s.BatchKey = @BatchKey
	AND ISNULL(rb.SourceKey,-1) <> 5 -- LPF-3104 excluded items permitted if 'revert'

	UPDATE s SET ErrorMessage = ISNULL(ErrorMessage, '') + 'Excluded SPG; '
	FROM ppe.RequestStaging s
	JOIN dbo.DimItemGroup3 dig3 ON dig3.IG3Level1 = s.SPGCode
	JOIN ppe.ExcludeSPG i ON i.ItemGroup3Key = dig3.ItemGroup3Key
	JOIN lion.Batch rb ON rb.BatchKey = s.BatchKey -- LPF-3104 excluded items permitted if 'revert'
	WHERE s.BatchKey = @BatchKey
	AND ISNULL(rb.SourceKey,-1) <> 5 --  LPF-3104 excluded items permitted if 'revert'

	/*end LPF-1791*/

	/*LPF-2742*/
	UPDATE s SET ErrorMessage = ISNULL(ErrorMessage, '') + 'Excluded Account; '
	FROM ppe.RequestStaging s
	JOIN dbo.DimAccount da ON da.AccountNumber = s.AccountNumber
	JOIN ppe.ExcludeAccount ea ON ea.AccountKey = da.AccountKey
	WHERE s.BatchKey = @BatchKey

	/* round all to 5 decimals per Scott */
	UPDATE s SET  
		NewSPGDiscount1 = CAST(CAST(CAST(s.NewSPGDiscount1 AS FLOAT) AS DEC(38,5)) AS VARCHAR(50)),
		NewSPGDiscount2 = CAST(CAST(CAST(s.NewSPGDiscount2 AS FLOAT) AS DEC(38,5)) AS VARCHAR(50)),
		NewExceptionDiscount = CAST(CAST(CAST(s.NewExceptionDiscount AS FLOAT) AS DEC(38,5)) AS VARCHAR(50)),
		NewExceptionFixedPrice = CAST(CAST(CAST(s.NewExceptionFixedPrice AS FLOAT) AS DEC(38,5)) AS VARCHAR(50))
	FROM ppe.RequestStaging  s
	WHERE BatchKey=@BatchKey
	AND ErrorMessage IS NULL

	IF (SELECT COUNT(*) 
		FROM ppe.RequestStaging  s
		WHERE BatchKey=@BatchKey AND ErrorMessage IS NOT NULL) = 0
	BEGIN


		INSERT INTO ppe.RequestMain
				( AccountKey ,
				ItemGroup3Key ,
				ItemKey ,
				InvoiceLineGroup1Key ,
				TypeKey ,
				StatusKey ,
				CreatedByWebUserKey ,
				ModifiedByWebUserKey ,
				DefaultDiscount ,
				CurrentSPGDiscount1 ,
				CurrentSPGDiscount2 ,
				ProposedSPGDiscount1 ,
				ProposedSPGDiscount2 ,
				NewSPGDiscount1 ,
				NewSPGDiscount2 ,
				NewSPGFromDate ,
				ProposedExceptionDiscount ,
				ProposedExceptionFixedPrice ,
				ProposedExceptionStartDate ,
				ProposedExceptionExpiryDate ,
				OwningBrandCode ,
				LastChangeInitials ,
				LastChangeDateTime ,
				LastChangeBranch ,
				StrategicItemFlag ,
				FixedPriceFlag ,
				KVIFlag ,
				KVIOverrideFlag ,
				LastSaleDate ,
				LastPriceDerivation ,
				LastItemDiscount ,
				LastItemPrice ,
				LastItemGPP ,
				AdjustedLastItemPrice ,
				AdjustedLastItemGPP ,
				RecommendedPrice ,
				RecommendedPriceLogic ,
				GreenPrice ,
				AmberPrice ,
				RedPrice ,
				TradePrice ,
				GreenGPP ,
				AmberGPP ,
				RedGPP ,
				TradeGPP ,
				LastPriceLevel ,
				SalesSize ,
				PriceApproach ,
				PriceBandLevel ,
				CurrentCost ,
				Total12MonthSales ,
				Total12MonthQty ,
				RecommendedImpact ,
				AcceptedImpact ,
				Version ,
				CurrentExceptionDiscount ,
				CurrentExceptionFixedPrice ,
				CurrentExceptionStartDate ,
				CurrentExceptionExpiryDate ,
				NewExceptionDiscount ,
				NewExceptionFixedPrice ,
				NewExceptionStartDate ,
				NewExceptionExpiryDate ,
				BatchKey ,
				ModificationDate ,
				CreationDate ,
				NotificationEmailSentIndicator ,
				LastActionKey ,
				NewDeleteTerm ,
				ProposedDeleteTerm ,
				OwnerWebUserKey ,
				PriorOwnerWebUserKey ,
				NewTermType ,
				RealityResponseCode ,
				ReviewType ,
				LastDiscount,
				SourceKey
				)
		SELECT da.accountkey , -- AccountKey - Key_Normal_type
				di.ItemGroup3Key  , -- ItemGroup3Key - Key_Normal_type
				di.ItemKey , -- ItemKey - Key_Normal_type
				4 , -- InvoiceLineGroup1Key - Key_Normal_type
				1 , -- TypeKey - smallint
				ISNULL(rs.StatusKey, 1) , -- StatusKey - smallint
				@WebUserKey , -- CreatedByWebUserKey - Key_Normal_type
				@WebUserKey , -- ModifiedByWebUserKey - Key_Normal_type
					/*skipped*/ NULL , -- DefaultDiscount - decimal
				ctSPG.UsualDiscount1 / 100.0, -- CurrentSPGDiscount1 - decimal
				ctSPG.UsualDiscount2 / 100.0 , -- CurrentSPGDiscount2 - decimal
				NULL , -- ProposedSPGDiscount1 - decimal
				NULL , -- ProposedSPGDiscount2 - decimal
				NULL , -- NewSPGDiscount1 - decimal
				NULL , -- NewSPGDiscount2 - decimal
				s.NewSPGFromDate , -- NewSPGFromDate - date			-- LPF-6366: Create Recommendations | Effective Date not being applied to products
				--NULL , -- NewSPGFromDate - date
				s.NewExceptionDiscount , -- ProposedExceptionDiscount - decimal
				s.NewExceptionFixedPrice , -- ProposedExceptionFixedPrice - money
				CAST(s.NewExceptionStartDate  AS DATE), -- ProposedExceptionStartDate - date
				CAST(s.NewExceptionExpiryDate  AS DATE), -- ProposedExceptionExpiryDate - date
				ISNULL(ctSPG.OwningBrandCode,'') , -- OwningBrandCode - Description_Small_type
				ctSPG.LastChangeInitials , -- LastChangeInitials - varchar(50)
				CAST(ctSPG.LastChangeDate AS DATE), -- LastChangeDateTime - date
				ISNULL(ctSPG.LastChangeBranch,'') , -- LastChangeBranch - UDVarchar_type
				CASE WHEN f.StratgicItemFlag ='Y' THEN 1 ELSE 0 END, -- StrategicItemFlag - bit
				CASE WHEN da.AccountUDVarchar12 ='Y' THEN 1 ELSE 0 END , -- FixedPriceFlag - bit
				CASE WHEN di.ItemUDVarchar13  ='Y' THEN 1 ELSE 0 END, -- KVIFlag - bit
				CASE WHEN aspg.AccountKey IS NOT NULL THEN 1 ELSE 0 END  , -- KVIOverrideFlag - bit
				f.LastSaleDayKey , -- LastSaleDate - date
				f.LastPriceDerivationCode , -- LastPriceDerivation - varchar(10)
				f.LastItemDiscount , -- LastItemDiscount - decimal
				f.LastItemPrice , -- LastItemPrice - money
				f.LastItemGPP , -- LastItemGPP - decimal
				f.LastItemPrice , -- AdjustedLastItemPrice - money
				f.LastItemGPP  , -- AdjustedLastItemGPP - decimal
				ISNULL(s.NewExceptionFixedPrice, di.ItemUDDecimal1 * (1.0 - s.NewExceptionDiscount) ) , -- RecommendedPrice - money
				'Last' , -- RecommendedPriceLogic - varchar(10)
				NULL , -- GreenPrice - money
				NULL , -- AmberPrice - money
				NULL , -- RedPrice - money
				di.ItemUDDecimal1 , -- TradePrice - money
				NULL , -- GreenGPP - decimal
				NULL , -- AmberGPP - decimal
				NULL , -- RedGPP - decimal
				(di.ItemUDDecimal1 - di.currentcost) / NULLIF(di.ItemUDDecimal1,0)  , -- TradeGPP - decimal
				'' , -- LastPriceLevel - varchar(10)
				f.SalesSize , -- SalesSize - varchar(10)
				f.PriceApproach , -- PriceApproach - varchar(10)
				NULL , -- PriceBandLevel - varchar(10)
				di.CurrentCost , -- CurrentCost - money
				f.SalesAtInvoice , -- Total12MonthSales - money
				f.Qty , -- Total12MonthQty - int
				--RecommendedImpact replaced by LPF-1606 f.SalesAtTrade * (1.0-(1.0 - ISNULL(s.NewSPGDiscount1, 0)) * (1.0 - ISNULL(s.NewSPGDiscount2, 0))) , -- RecommendedImpact - money
				CASE 
					WHEN s.DeleteTerm   ='Y' THEN f.SalesAtTrade * ( ALPCD - s.NewSPGDiscount1 )
					ELSE SalesAtTrade * ( ALPCD - s.NewExceptionDiscount )
				END ,  -- RecommendedImpact - money
				CASE 
					WHEN s.DeleteTerm   ='Y' THEN f.SalesAtTrade * ( ALPCD - s.NewSPGDiscount1 )
					ELSE SalesAtTrade * ( ALPCD - s.NewExceptionDiscount )
				END ,  -- AcceptedImpact - money				
				1 , -- Version - int
				ctSPG.ExceptionDiscount / 100.0 , -- CurrentExceptionDiscount - decimal
				ctSPG.ExceptionFixedPrice , -- CurrentExceptionFixedPrice - money
				CAST(ctSPG.ExceptionFromDate   AS DATE), -- CurrentExceptionStartDate - date
				CAST(ctSPG.ExceptionToDate   AS DATE), -- CurrentExceptionExpiryDate - date
				s.NewExceptionDiscount , -- NewExceptionDiscount - decimal
				s.NewExceptionFixedPrice , -- NewExceptionFixedPrice - money
				CASE --per LPF-1345
					WHEN CAST(GETDATE() AS DATE) > CAST(s.NewExceptionStartDate  AS DATE) THEN CAST(GETDATE() AS DATE)
					ELSE CAST(s.NewExceptionStartDate  AS DATE)
				END, -- NewExceptionStartDate - date
				CASE  --per LPF-1345
					WHEN DATEADD(d,30,CAST(GETDATE() AS DATE) ) > CAST(s.NewExceptionExpiryDate  AS DATE) THEN DATEADD(d,30,CAST(GETDATE() AS DATE) )
					ELSE CAST(s.NewExceptionExpiryDate  AS DATE)
				END, -- NewExceptionExpiryDate - date
				s.BatchKey , -- BatchKey - int
				GETDATE() , -- ModificationDate - datetime
				GETDATE() , -- CreationDate - datetime
				0 , -- NotificationEmailSentIndicator - bit
				1 , -- LastActionKey - smallint
				CASE WHEN s.DeleteTerm   ='Y' THEN 1 ELSE 0 END, -- NewDeleteTerm - bit
				CASE WHEN s.DeleteTerm   ='Y' THEN 1 ELSE 0 END, -- ProposedDeleteTerm - bit
				CASE s.StatusRollup WHEN 'New' THEN @WebUserKey ELSE ISNULL(wu2.WebUserKey,wu.WebUserKey) END /*wu.WebUserKey*/ , -- OwnerWebUserKey - int
				NULL , -- PriorOwnerWebUserKey - int
				CASE WHEN s.NewExceptionDiscount IS NOT NULL THEN 'TD' ELSE 'FP' END , -- NewTermType - varchar(2)
				NULL , -- RealityResponseCode - varchar(100)
				3 , -- ReviewType - int
				f.LastItemDiscount,  -- LastDiscount - decimal
				rb.SourceKey
		FROM ppe.RequestStaging s
		JOIN dbo.DimAccount da ON da.AccountNumber = s.AccountNumber
		JOIN dbo.WebUser wu ON wu.AccountManagerKey = da.AccountManagerKey
		JOIN dbo.DimItem di ON di.ItemNumber = '1' + s.ProductCode
		JOIN dbo.DimBrand db ON di.ItemUDVarChar2 = db.Brand
		LEFT JOIN dbo.DimAccountBrandManager dabm ON dabm.AccountKey = da.AccountKey AND dabm.BrandKey = db.BrandKey
		LEFT JOIN dbo.WebUser wu2 ON wu2.AccountManagerKey = dabm.AccountManagerKey
		JOIN lion.Batch rb ON rb.BatchKey = s.BatchKey
		LEFT JOIN  (SELECT WebUserKey,WorkflowLevel FROM WebUserRole wur JOIN WebRole wr ON wr.WebRoleKey = wur.WebRoleKey 
					WHERE WebRoleName IN ('BranchManager','NetworkManager','CentralPricing') ) wr0
			ON wr0.WebUserKey = wu.WebUserKey
		LEFT JOIN ppe.RequestStatus rs 
			ON rs.StatusRollup = s.StatusRollup 
			AND rs.StatusPosition = CASE s.StatusRollup WHEN 'Pending' THEN wr0.WorkflowLevel ELSE rs.StatusPosition END 
		LEFT JOIN lion.CustomerTerms ctSPG
			ON ctSPG.AccountKey = da.AccountKey
			AND ctSPG.ItemGroup3Key = di.ItemGroup3Key
			AND ctSPG.ItemKey = di.ItemKey
		LEFT JOIN ppe.vw_AccountItemSPGImpact f 
			ON f.AccountKey = da.AccountKey 
			AND f.ItemKey = di.ItemKey
		LEFT JOIN lion.AccountSPG aspg 
		ON aspg.AccountKey = da.AccountKey
		AND (aspg.LLSPGCode = s.SPGCode OR aspg.LLSPGCode='ALL')
		WHERE s.BatchKey = @BatchKey
		AND s.ProductCode IS NOT NULL
		AND s.ErrorMessage IS NULL

		--SELECT --per LPF-1847
		--	da.accountkey ,
		--	dig3.ItemGroup3Key  ,
		--	(ISNULL(s.SPGSales,0)-ISNULL(s.SPGSkippedSales,0)) * ( (1.0-a.SPGLastDiscount) - s.NewSPGDiscount1 ) SPGImpact
		--INTO #SPGImpact3
		--FROM ppe.RequestStaging s
		--JOIN dbo.DimAccount da ON da.AccountNumber = s.AccountNumber
		--JOIN dbo.DimItemGroup3 dig3 ON dig3.ig3level1 = s.SPGCode AND IG3Level4 = '1'
		--CROSS APPLY [ppe].[fn_GetDiscountSPG] (da.AccountKey,dig3.ItemGroup3Key) a
		--WHERE BatchKey =  @BatchKey
		--AND ProductCode IS NULL


		INSERT INTO ppe.RequestMain
				( AccountKey ,
				ItemGroup3Key ,
				ItemKey ,
				InvoiceLineGroup1Key ,
				TypeKey ,
				StatusKey ,
				CreatedByWebUserKey ,
				ModifiedByWebUserKey ,
				DefaultDiscount ,
				CurrentSPGDiscount1 ,
				CurrentSPGDiscount2 ,
				ProposedSPGDiscount1 ,
				ProposedSPGDiscount2 ,
				NewSPGDiscount1 ,
				NewSPGDiscount2 ,
				NewSPGFromDate ,
				ProposedExceptionDiscount ,
				ProposedExceptionFixedPrice ,
				ProposedExceptionStartDate ,
				ProposedExceptionExpiryDate ,
				OwningBrandCode ,
				LastChangeInitials ,
				LastChangeDateTime ,
				LastChangeBranch ,
				StrategicItemFlag ,
				FixedPriceFlag ,
				KVIFlag ,
				KVIOverrideFlag ,
				LastSaleDate ,
				LastPriceDerivation ,
				LastItemDiscount ,
				LastItemPrice ,
				LastItemGPP ,
				AdjustedLastItemPrice ,
				AdjustedLastItemGPP ,
				RecommendedPrice ,
				RecommendedPriceLogic ,
				GreenPrice ,
				AmberPrice ,
				RedPrice ,
				TradePrice ,
				GreenGPP ,
				AmberGPP ,
				RedGPP ,
				TradeGPP ,
				LastPriceLevel ,
				SalesSize ,
				PriceApproach ,
				PriceBandLevel ,
				CurrentCost ,
				Total12MonthSales ,
				Total12MonthQty ,
				RecommendedImpact ,
				AcceptedImpact ,
				Version ,
				CurrentExceptionDiscount ,
				CurrentExceptionFixedPrice ,
				CurrentExceptionStartDate ,
				CurrentExceptionExpiryDate ,
				NewExceptionDiscount ,
				NewExceptionFixedPrice ,
				NewExceptionStartDate ,
				NewExceptionExpiryDate ,
				BatchKey ,
				ModificationDate ,
				CreationDate ,
				NotificationEmailSentIndicator ,
				LastActionKey ,
				NewDeleteTerm ,
				ProposedDeleteTerm ,
				OwnerWebUserKey ,
				PriorOwnerWebUserKey ,
				NewTermType ,
				RealityResponseCode ,
				ReviewType ,
				LastDiscount,
				SourceKey,
				SPGSkippedSales
				)
		SELECT DISTINCT da.accountkey , -- AccountKey - Key_Normal_type
				dig3.ItemGroup3Key  , -- ItemGroup3Key - Key_Normal_type
				1 , -- ItemKey - Key_Normal_type
				4 , -- InvoiceLineGroup1Key - Key_Normal_type
				1 , -- TypeKey - smallint
				ISNULL(rs.StatusKey, 1) , -- StatusKey - smallint
				@WebUserKey , -- CreatedByWebUserKey - Key_Normal_type
				@WebUserKey , -- ModifiedByWebUserKey - Key_Normal_type
					/*skipped*/ NULL , -- DefaultDiscount - decimal
				ctSPG.UsualDiscount1 / 100.0 , -- CurrentSPGDiscount1 - decimal
				ctSPG.UsualDiscount2 / 100.0 , -- CurrentSPGDiscount2 - decimal
				ISNULL(s.NewSPGDiscount1,0) , -- ProposedSPGDiscount1 - decimal
				s.NewSPGDiscount2 , -- ProposedSPGDiscount2 - decimal
				ISNULL(s.NewSPGDiscount1,0) , -- NewSPGDiscount1 - decimal
				s.NewSPGDiscount2 , -- NewSPGDiscount2 - decimal
				s.NewSPGFromDate , -- NewSPGFromDate - date
				NULL , -- ProposedExceptionDiscount - decimal
				NULL , -- ProposedExceptionFixedPrice - money
				NULL , -- ProposedExceptionStartDate - date
				NULL , -- ProposedExceptionExpiryDate - date
				ISNULL(ctSPG.OwningBrandCode,'') , -- OwningBrandCode - Description_Small_type
				ctSPG.LastChangeInitials , -- LastChangeInitials - varchar(50)
				ctSPG.LastChangeDate , -- LastChangeDateTime - date
				ISNULL(ctSPG.LastChangeBranch,'') , -- LastChangeBranch - UDVarchar_type
				NULL , -- StrategicItemFlag - bit
				NULL , -- FixedPriceFlag - bit
				NULL , -- KVIFlag - bit
				NULL , -- KVIOverrideFlag - bit
				NULL , -- LastSaleDate - date
				NULL , -- LastPriceDerivation - varchar(10)
				NULL , -- LastItemDiscount - decimal
				NULL , -- LastItemPrice - money
				NULL , -- LastItemGPP - decimal
				NULL , -- AdjustedLastItemPrice - money
				NULL , -- AdjustedLastItemGPP - decimal
				(f.SalesAtTrade / f.SalesAtInvoice) * (1.0 - (1.0-(1.0 - ISNULL(s.NewSPGDiscount1, 0)) * (1.0 - ISNULL(s.NewSPGDiscount2, 0)))) , -- RecommendedPrice - money
				'Last' , -- RecommendedPriceLogic - varchar(10)
				NULL , -- GreenPrice - money
				NULL , -- AmberPrice - money
				NULL , -- RedPrice - money
				NULL , -- TradePrice - money
				NULL , -- GreenGPP - decimal
				NULL , -- AmberGPP - decimal
				NULL , -- RedGPP - decimal
				NULL , -- TradeGPP - decimal
				NULL , -- LastPriceLevel - varchar(10)
				NULL , -- SalesSize - varchar(10)
				NULL , -- PriceApproach - varchar(10)
				NULL , -- PriceBandLevel - varchar(10)
				NULL , -- CurrentCost - money
				f.SalesAtInvoice , -- Total12MonthSales - money
				f.Qty , -- Total12MonthQty - int
				CASE 
					WHEN s.DeleteTerm ='Y' THEN 0 
					ELSE ISNULL((ISNULL(s.SPGSales,f.SalesAtTradeNoTerms)-ISNULL(s.SPGSkippedSales,0)) * ( (1.0-a.SPGLastDiscount) - a2.Discount ) ,0) 
				END , -- zero per LPF-1949/LPF-1998 RecommendedImpact - money
				CASE 
					WHEN s.DeleteTerm ='Y' THEN 0 
					ELSE ISNULL((ISNULL(s.SPGSales,f.SalesAtTradeNoTerms)-ISNULL(s.SPGSkippedSales,0)) * ( (1.0-a.SPGLastDiscount) - a2.Discount ) ,0) 
				END  , -- zero per LPF-1949/LPF-1998 AcceptedImpact - money
				1 , -- Version - int
				NULL , -- CurrentExceptionDiscount - decimal
				NULL , -- CurrentExceptionFixedPrice - money
				NULL , -- CurrentExceptionStartDate - date
				NULL , -- CurrentExceptionExpiryDate - date
				NULL , -- NewExceptionDiscount - decimal
				NULL , -- NewExceptionFixedPrice - money
				NULL, -- NewExceptionStartDate - date
				NULL , -- NewExceptionExpiryDate - date
				s.BatchKey , -- BatchKey - int
				GETDATE() , -- ModificationDate - datetime
				GETDATE() , -- CreationDate - datetime
				0 , -- NotificationEmailSentIndicator - bit
				1 , -- LastActionKey - smallint
				CASE WHEN s.DeleteTerm   ='Y' THEN 1 ELSE 0 END , -- NewDeleteTerm - bit
				CASE WHEN s.DeleteTerm   ='Y' THEN 1 ELSE 0 END , -- ProposedDeleteTerm - bit
				CASE s.StatusRollup WHEN 'New' THEN @WebUserKey ELSE ISNULL(wu2.WebUserKey,wu.WebUserKey) END /*wu.WebUserKey*/ , -- OwnerWebUserKey - int
				NULL , -- PriorOwnerWebUserKey - int
				NULL , -- NewTermType - varchar(2)
				NULL , -- RealityResponseCode - varchar(100)
				3 , -- ReviewType - int
				NULL,  -- LastDiscount - decimal
				rb.SourceKey,
				s.SPGSkippedSales

		FROM ppe.RequestStaging s
		JOIN dbo.DimAccount da ON da.AccountNumber = s.AccountNumber
		JOIN dbo.WebUser wu ON wu.AccountManagerKey = da.AccountManagerKey
		JOIN dbo.DimItemGroup3 dig3 ON dig3.IG3Level1 = s.SPGCode AND dig3.IG3Level4 = '1'
		JOIN lion.Batch rb ON rb.BatchKey = s.BatchKey
		OUTER APPLY [ppe].[fn_GetDiscountSPG] (da.AccountKey,dig3.ItemGroup3Key) a
		OUTER APPLY [ppe].[fn_GetCompoundDiscount](s.NewSPGDiscount1, s.NewSPGDiscount2) a2
		LEFT JOIN (SELECT di.ItemGroup3Key, MAX(di.ItemUDVarChar2 ) ProdBrandAtLLSPG FROM dbo.DimItem di GROUP BY di.ItemGroup3Key) pb 
			ON pb.ItemGroup3Key = dig3.ItemGroup3Key
		LEFT JOIN dbo.DimBrand db ON pb.ProdBrandAtLLSPG = db.Brand
		LEFT JOIN dbo.DimAccountBrandManager dabm ON dabm.AccountKey = da.AccountKey AND dabm.BrandKey = db.BrandKey
		LEFT JOIN dbo.WebUser wu2 ON wu2.AccountManagerKey = dabm.AccountManagerKey
		LEFT JOIN  (SELECT WebUserKey,WorkflowLevel FROM WebUserRole wur JOIN WebRole wr ON wr.WebRoleKey = wur.WebRoleKey 
					WHERE WebRoleName IN ('BranchManager','NetworkManager','CentralPricing') ) wr0
			ON wr0.WebUserKey = wu.WebUserKey
		LEFT JOIN ppe.RequestStatus rs 
			ON rs.StatusRollup = s.StatusRollup 
			AND rs.StatusPosition = CASE s.StatusRollup WHEN 'Pending' THEN wr0.WorkflowLevel ELSE rs.StatusPosition END 
		LEFT JOIN lion.CustomerTerms ctSPG
			ON ctSPG.AccountKey = da.AccountKey
			AND ctSPG.ItemGroup3Key = dig3.ItemGroup3Key
			AND ctSPG.ItemKey = 1
		LEFT JOIN (SELECT 
						flpc.AccountKey, 
						di2.ItemGroup3Key, 
						SUM(flpc.Total12MonthSales) SalesAtInvoice, 
						SUM(flpc.Total12MonthQuantity) Qty,
						SUM(flpc.Total12MonthQuantity * di2.ItemUDDecimal1) SalesAtTrade,
						SUM(CASE WHEN ct.CustomerTermsKey IS NOT NULL OR r.RequestKey IS NOT NULL /*LPF-2670*/ 
								THEN 0 
								ELSE flpc.Total12MonthQuantity * di2.ItemUDDecimal1 
							END ) SalesAtTradeNoTerms,
						MAX(flpc.UDVarchar1) AS StratgicItemFlag,
						MAX(dd.SQLDate) AS LastSaleDayKey,
						MAX(fil.UDVarchar1) AS LastPriceDerivationCode,
						MAX((fil.UnitListPrice - flpc.LastItemPrice) / NULLIF(fil.UnitListPrice,0)) AS LastItemDiscount,
						MAX(flpc.LastItemPrice) AS LastItemPrice,
						MAX((flpc.LastItemPrice - flpc.LastItemPrice) / NULLIF(flpc.LastItemPrice,0)) AS LastItemGPP,
						MAX(flpc.UDVarchar4) AS SalesSize,
						MAX(flpc.UDVarchar10) AS PriceApproach
					FROM FactLastPriceAndCost flpc
					JOIN dbo.DimItem di2 ON di2.ItemKey = flpc.ItemKey
					JOIN dbo.FactInvoiceLine fil ON flpc.LastInvoiceLineUniqueIdentifier = fil.InvoiceLineUniqueIdentifier
					JOIN dbo.DimDay dd ON dd.DayKey = flpc.LastSaleDayKey
					JOIN lion.vwFactInvoiceLineGroup1 g1 ON flpc.InvoiceLineGroup1Key = g1.InvoiceLineGroup1Key
					LEFT JOIN lion.CustomerTerms ct
						ON ct.AccountKey = flpc.AccountKey
						AND ct.ItemKey = flpc.ItemKey
					LEFT JOIN ppe.RequestMain r --LPF-2670
						ON r.AccountKey = flpc.AccountKey
						AND r.ItemKey = flpc.ItemKey
						AND r.ItemGroup3Key = di2.ItemGroup3Key
						AND r.BatchKey = @BatchKey
					WHERE flpc.InvoiceLineGroup1Key = 4
					AND fil.UDVarchar1 NOT IN ('DC', 'OCD', 'OCL','DP') 
					AND flpc.Total12MonthQuantity > 0
					AND flpc.LastItemPrice > 0
					AND di2.ItemUDDecimal1 > 0
					AND di2.ItemUDVarchar1 = '1'
					AND g1.ShipmentType = 'S'
					AND g1.DeviatedIndicator = 'N'
					GROUP BY 
						flpc.AccountKey, 
						di2.ItemGroup3Key) f ON f.AccountKey = da.AccountKey AND f.ItemGroup3Key = dig3.ItemGroup3Key
		--LEFT JOIN #SPGImpact3 si3
		--	ON si3.ItemGroup3Key = dig3.ItemGroup3Key
		--	AND si3.AccountKey = da.AccountKey
		WHERE s.BatchKey = @BatchKey
		AND (s.ProductCode IS NULL  OR (/*per LPF-1372 and LPF-1417 */ ctSPG.CustomerTermsKey IS NULL 
													AND s.ProductCode IS NOT NULL 
													AND s.NewSPGDiscount1 IS NOT NULL) )
		AND s.ErrorMessage IS NULL



	END -- if no errors

	--LPF-1775 move collision logic from RequestMain trigger to this procedure.  Requires disabling of RequestMain.tIII_RequestMain trigger.		
	UPDATE old SET StatusKey = (SELECT StatusKey FROM ppe.RequestStatus WHERE StatusName = 'Closed')
	FROM ppe.RequestMain old 
	INNER JOIN ppe.RequestStatus oldstatus ON old.StatusKey = oldstatus.StatusKey
	INNER JOIN ppe.RequestMain new 
		ON old.AccountKey = new.AccountKey 
		AND	old.ItemGroup3Key = new.ItemGroup3Key 
		AND	old.ItemKey = new.ItemKey
		AND old.BatchKey <> new.BatchKey
		AND new.BatchKey = @BatchKey
	INNER JOIN ppe.RequestStatus newstatus ON new.StatusKey = newstatus.StatusKey
	WHERE oldstatus.StatusRollup = 'Pending'
	AND newstatus.StatusRollup IN ('Pending','Approved')
	-- end LPF-1775


	/*per LPF-1557 */

	--SELECT *
	UPDATE r2 SET NewDeleteTerm = 1
	FROM ppe.RequestMain r
	JOIN ppe.RequestMain r2
		ON r2.AccountKey = r.AccountKey
		AND r2.ItemGroup3Key = r.ItemGroup3Key
	JOIN ppe.RequestStatus rs ON rs.StatusKey = r2.StatusKey
	WHERE r.ItemKey = 1
	AND r.NewDeleteTerm = 1
	AND r2.ItemKey <> 1
	AND r2.NewDeleteTerm = 0
	AND rs.StatusRollup IN ('New','Pending')
	AND r.BatchKey = @BatchKey


	/*end LPF-1557 */

	/*per LPF-1698*/
		INSERT INTO ppe.RequestLog
				( WebUserKey ,
				  RequestKey ,
				  OldStatusKey ,
				  NewStatusKey ,
				  RequestType ,
				  LogTime ,
				  RequestAction ,
				  EmailSent ,
				  OldOwnerWebUserKey ,
				  NewOwnerWebUserKey ,
				  Notes
				)
		SELECT   @WebUserkey, --LPF-4028 -- OwnerWebUserKey , -- WebUserKey - int
				  RequestKey , -- RequestKey - int
				  0 , -- OldStatusKey - smallint
				  StatusKey , -- NewStatusKey - smallint
				  '1' , -- RequestType - varchar(50)
				  GETDATE() , -- LogTime - datetime
				  'CREATED' , -- RequestAction - varchar(50)
				  CASE StatusKey WHEN 1 /*1=new status*/ THEN NULL /*no email*/ ELSE 0 /*send email*/ END , -- EmailSent - bit per LPF-3644 if new, no email.
				  OwnerWebUserKey , -- OldOwnerWebUserKey - int
				  OwnerWebUserKey , -- NewOwnerWebUserKey - int
				  NULL  -- Notes - varchar(100)
		FROM ppe.RequestMain 
		WHERE BatchKey = @BatchKey

	/*end LPF-1698*/


	SELECT 
		RequestStagingKey,
		ErrorMessage,
		AccountNumber,
		SPGCode,
		ProductCode,
		RowIndex
	FROM ppe.RequestStaging
	WHERE BatchKey = @BatchKey
	AND ErrorMessage IS NOT NULL
    
END

GO
