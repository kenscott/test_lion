SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [cm].[GetPossibleContracts] (@ClientInvoiceLineUniqueID INT)

AS

--EXEC cm.GetPossibleContracts 626387840  --example with duplicates

SELECT  
		cj.SupplierRef AS SupplierReferenceNo,
		cj.OriginalContractID AS AppliedContractId,
		scp.PricingContractID AS AppliedPricingContractId,
		cj.CurrentContractID AS ContractId,
		sc.PricingContractID,
		sc.ContractOwner,
		sc.ContractType1 AS ContractType,
		sc.JobFlag,
		sc.JobSite,
		cj.ClaimJournalKey,
		cj.ClaimType,
		cj.TradingMargin / NULLIF(fil.NettPrice, 0) AS AfterMarginPerc,
		cj.TradingMargin AS AfterMarginAmt,
		cj.ARPResult,
		cj.ARPReason,
		cj.ClaimJournalStatus AS [Status],
		odsp.FullName AS StatusChangeUserName,
		cj.ADVRecommendationDate StatusChangeDate,
		rr.RejectionReasonValue,
		LEFT(cj.Comment, 10) AS ShortComment,
		cj.Comment,
		cj.ClaimLineValue AS ClaimValue
		

FROM cm.ClaimJournal cj
INNER JOIN cc.SupplierContract sc  --ARP
	ON sc.ContractID = cj.CurrentContractID
INNER JOIN dbo.DimVendor dv
	ON sc.VendorKey = dv.VendorKey
LEFT JOIN cc.SupplierContract scp  --POS
	ON scp.ContractID = cj.OriginalContractID
LEFT JOIN dbo.WebUser wu
	ON wu.WebUserKey = cj.ADVRecommendationWebUserKey
LEFT JOIN dbo.ODSPerson odsp
	ON odsp.ODSPersonKey = wu.ODSPersonKey
LEFT JOIN cm.RejectionReason rr
	ON rr.RejectionReasonKey = cj.RejectionReasonKey
LEFT JOIN cm.FactInvoiceLine fil
	on fil.ClientInvoiceLineUniqueID = cj.ClientInvoiceLineUniqueID

WHERE cj.ClientInvoiceLineUniqueID = @ClientInvoiceLineUniqueID
	AND cj.ClaimType = 'C'  --do not include manual claims or adjustments
	AND cj.ClaimJournalStatus <> 'D'  --do not include claims with a Deleted status 
GO
