SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE [lion].[Load_RBP_AISDReview]
AS

SET NOCOUNT ON


EXEC LogDCPEvent 'RPB - Building RBP_AISDReview', 'B'

DECLARE @RowsI INT

SET @RowsI = 0

EXEC DBA_IndexRebuild 'RBP_AISDReview', 'Drop', NULL, 'lion'


--drop table lion.RBP_AISReview
--select top 0 * into lion.RBP_AISDReview from lion.vwBandedAISDReview
-- select top 1 * from lion.vwBandedAISDReview
-- select top 10 FloorPlaybookPricingGroupPriceBandKey, TargetPlaybookPricingGroupPriceBandKey, StretchPlaybookPricingGroupPriceBandKey, * from lion.vwBandedAISDReview where RPBPlaybookPricingGroupKey is not null

TRUNCATE TABLE lion.RBP_AISDReview

INSERT lion.RBP_AISDReview (
	[AccountKey]
	,[ItemKey]
	,[InvoiceLineGroup1Key]
	,[CustomerBranch]
	,[CustomerNetwork]
	,[CustomerArea]
	,[CustomerRegion]
	,[CustomerBranchName]
	,[CustomerNetworkDescription]
	,[CustomerAreaDescription]
	,[CustomerRegionDescription]
	,[CustomerPlaybookRegion]
	,[CustomerPlaybookRegionDescription]
	,[CustomerPlaybookArea]
	,[CustomerPlaybookAreaDescription]
	,[CustomerBranchPlaybookBrandIndicator]
	,[CustomerBranchBrandGroup]
	,[CustomerBranchPGActiveIndicator]
	,[CustomerBranchPrimaryBrand]
	,[CustomerBranchTradingStatus]
	,[CustomerBranchType]
	,[CustomerBranchAddressLine1]
	,[CustomerBranchAddressLine2]
	,[CustomerBranchAddressLine3]
	,[CustomerBranchAddressLine4]
	,[CustomerBranchAddressLine5]
	,[CustomerBranchPostcode]
	,[CustomerBranchEmail]
	,[CustomerBranchTelephone]
	,[CustomerBranchPostCodeArea]
	,[CustomerBranchPostCodeDistrict]
	,[CustomerBranchPyramidCode]
	,[LastSaleBranch]
	,[LastSaleNetwork]
	,[LastSaleArea]
	,[LastSaleRegion]
	,[LastSaleBranchName]
	,[LastSaleNetworkDescription]
	,[LastSaleAreaDescription]
	,[LastSaleRegionDescription]
	,[LastSalePlaybookRegion]
	,[LastSalePlaybookRegionDescription]
	,[LastSalePlaybookArea]
	,[LastSalePlaybookAreaDescription]
	,[LastSaleBranchPrimaryBrand]
	,[LastSaleBranchTradingStatus]
	,[LastSaleBranchType]
	,[LastSaleBranchAddressLine1]
	,[LastSaleBranchAddressLine2]
	,[LastSaleBranchAddressLine3]
	,[LastSaleBranchAddressLine4]
	,[LastSaleBranchAddressLine5]
	,[LastSaleBranchPostcode]
	,[LastSaleBranchEmail]
	,[LastSaleBranchTelephone]
	,[LastSaleBranchPostCodeArea]
	,[LastSaleBranchPostCodeDistrict]
	,[LastSaleBranchPyramidCode]
	,[CustomerType]
	,[CustomerTypeDescription]
	,[TradingStatusCategory]
	,[TradingStatusCategoryDescription]
	,[AccountManagerCode]
	,[AccountManagerFullName]
	,[AccountManagerEmail]
	,[AccountClientUniqueIdentifier]
	,[AccountNumber]
	,[AccountName]
	,[AccountNumberParent]
	,[AccountAddress1]
	,[AccountAddress2]
	,[AccountAddress3]
	,[AccountAddress4]
	,[AccountPostCode]
	,[CreditLimit]
	,[PaymentTermName]
	,[IntercompanyAccount]
	,[OverLimitStatus]
	,[OverdueStatus]
	,[TermsParent]
	,[PricingCategory]
	,[AccountOwnerType]
	,[AccountOwnerBucket]
	,[CustomerSegment]
	,[CustomerRebate]
	,[DateOpened]
	,[DateClosed]
	,[VendorKey]
	,[VendorDescription]
	,[VendorNumber]
	,[ItemNumber]
	,[ItemDescription]
	,[VendorStockNumber]
	,[ItemPyramidCode]
	,[ProdBrandAtLLSPG]
	,[MPGCode]
	,[MPGDescription]
	,[SuperCategory]
	,[Category]
	,[SubCategory]
	,[ItemObsolete]
	,[HireInd]
	,[SpecialInd]
	,[ProductLifecycle]
	,[OwnBrandInd]
	,[KnownValueInd]
	,[NDPInd]
	,[MultiSourcedProductInd]
	,[VariablePackInd]
	,[ZonedInd]
	,[UnitCode]
	,[GenericProductID]
	,[ItemDateForDeletion]
	,[LLSPGCode]
	,[HLSPGCode]
	,[GSPGCode]
	,[LLSPGDescription]
	,[HLSPGDescription]
	,[GSPGDescription]
	,[ShipmentType]
	,[ContractClaimsIndicator]
	,[StrategicItem]
	,[SubLLSPG]
	,[NewItemForAccountIndicator]
	,[Sales603010Bucket]
	,[TotalSalesAccountBucket]
	,[FrequencyBucket]
	,[CustBaseBucket]
	,[FreqPurchBucket]
	,[TopSellerBucket]
	,[PriceApproach]
	,[TotalAccountSales]
	,[TotalNumberOfItemsSoldByAccount]
	,[TotalAccountGP]
	,[TotalNumberOfSuperCategoriesSold]
	,[ExcludeForSegmentationIndicator]
	,[PercentAccountItemSalesToAccountSales]
	,[AverageQuantityPerInvoiceLine]
	,[LastInvoiceDate]
	,[LastQuantity]
	,[LastItemPrice]
	,[LastItemCost]
	,[LastItemDiscountPercent]
	,[LastItemGP]
	,[LastItemGPP]
	,[Total12MonthInvoiceLineCount]
	,[Total12MonthQuantity]
	,[Total12MonthSales]
	,[Total12MonthCost]
	,[Total12MonthGP]
	,[Total12MonthGPP]
	,[LastPriceDerivationBucket]
	,[LastCustomerContractIndicator]
	,[LastBranchCode]
	,[LastTransNo]
	,[LastDelNo]
	,[LastLineNumber]
	,[LastNetQty]
	,[LastNetLinePrice]
	,[LastNetApTradingMarginPadRelease]
	,[LastDeliveryDate]
	,[LastSledgerNo]
	,[LastSledgerDate]
	,[LastYMTH]
	,[LastSupplyType]
	,[LastDocInd]
	,[LastCreditReason]
	,[LastKnownValueItem]
	,[LastCtsRelCode]
	,[LastCustCntNo]
	,[LastMthdOfDespatch]
	,[LastTransType]
	,[LastSalesOrigin]
	,[LastTakenBy]
	,[LastMethodOfPayment]
	,[LastHeaderID]
	,[LastCrdOrigInvNo]
	,[LastCrdOrigInvDate]
	,[LastOriginalOrdNo]
	,[LastCarriagePack]
	,[LastDelAddPhone]
	,[LastGpAllocBranchCode]
	,[LastSalesOriginDesc]
	,[LastPriceOrderType]
	,[LastTransactionDate]
	,[LastOrderDate]
	,[LastTradingPrice]
	,[LastTradeGPP]
	,[LastTotalDiscount]
	,[LastPriceDerivation]
	,[LastVariablePackInd]
	,[LastClaimNo]
	,[LastApTradingMarginPadRelease]
	,[LastCCProfit]
	,[LastRetailPrice]
	,[RPBPlaybookPricingGroupKey]
	,[CoveringBandGroupValues]
	,[CoveringBandPricingRuleKey]
	,[CoveringBandPlaybookPricingGroupQuantityBandKey]
	,[CoveringBandLowerBandQuantity]
	,[CoveringBandUpperBandQuantity]
	,[CoveringBandRulingMemberCount]
	,[CoveringBandBandSequenceNumber]
	,[CoveringBandPricingRuleSequenceNumber]
	,[FloorPlaybookPricingGroupPriceBandKey]
	,[FloorPlaybookPricingGroupKey]
	,[FloorLowerBandQuantity]
	,[FloorUpperBandQuantity]
	,[FloorMemberRank]
	,[FloorRulingMemberCount]
	,[FloorBandSequenceNumber]
	,[FloorPlaybookPricingGroupQuantityBandKey]
	,[FloorPricingRuleSequenceNumber]
	,[TargetPlaybookPricingGroupPriceBandKey]
	,[TargetPlaybookPricingGroupKey]
	,[TargetLowerBandQuantity]
	,[TargetUpperBandQuantity]
	,[TargetMemberRank]
	,[TargetRulingMemberCount]
	,[TargetBandSequenceNumber]
	,[TargetPlaybookPricingGroupQuantityBandKey]
	,[TargetPricingRuleSequenceNumber]
	,[StretchPlaybookPricingGroupPriceBandKey]
	,[StretchPlaybookPricingGroupKey]
	,[StretchLowerBandQuantity]
	,[StretchUpperBandQuantity]
	,[StretchMemberRank]
	,[StretchRulingMemberCount]
	,[StretchBandSequenceNumber]
	,[StretchPlaybookPricingGroupQuantityBandKey]
	,[StretchPricingRuleSequenceNumber]
	,[FloorPct]
	,[TargetPct]
	,[StretchPct]
	,[FloorGPP]
	,[TargetGPP]
	,[StretchGPP]
	,[EffectiveFloorDiscountPercent]
	,[EffectiveTargetDiscountPercent]
	,[EffectiveStretchDiscountPercent]

	)
SELECT
	[AccountKey]
	,[ItemKey]
	,[InvoiceLineGroup1Key]
	,[CustomerBranch]
	,[CustomerNetwork]
	,[CustomerArea]
	,[CustomerRegion]
	,[CustomerBranchName]
	,[CustomerNetworkDescription]
	,[CustomerAreaDescription]
	,[CustomerRegionDescription]
	,[CustomerPlaybookRegion]
	,[CustomerPlaybookRegionDescription]
	,[CustomerPlaybookArea]
	,[CustomerPlaybookAreaDescription]
	,[CustomerBranchPlaybookBrandIndicator]
	,[CustomerBranchBrandGroup]
	,[CustomerBranchPGActiveIndicator]
	,[CustomerBranchPrimaryBrand]
	,[CustomerBranchTradingStatus]
	,[CustomerBranchType]
	,[CustomerBranchAddressLine1]
	,[CustomerBranchAddressLine2]
	,[CustomerBranchAddressLine3]
	,[CustomerBranchAddressLine4]
	,[CustomerBranchAddressLine5]
	,[CustomerBranchPostcode]
	,[CustomerBranchEmail]
	,[CustomerBranchTelephone]
	,[CustomerBranchPostCodeArea]
	,[CustomerBranchPostCodeDistrict]
	,[CustomerBranchPyramidCode]
	,[LastSaleBranch]
	,[LastSaleNetwork]
	,[LastSaleArea]
	,[LastSaleRegion]
	,[LastSaleBranchName]
	,[LastSaleNetworkDescription]
	,[LastSaleAreaDescription]
	,[LastSaleRegionDescription]
	,[LastSalePlaybookRegion]
	,[LastSalePlaybookRegionDescription]
	,[LastSalePlaybookArea]
	,[LastSalePlaybookAreaDescription]
	,[LastSaleBranchPrimaryBrand]
	,[LastSaleBranchTradingStatus]
	,[LastSaleBranchType]
	,[LastSaleBranchAddressLine1]
	,[LastSaleBranchAddressLine2]
	,[LastSaleBranchAddressLine3]
	,[LastSaleBranchAddressLine4]
	,[LastSaleBranchAddressLine5]
	,[LastSaleBranchPostcode]
	,[LastSaleBranchEmail]
	,[LastSaleBranchTelephone]
	,[LastSaleBranchPostCodeArea]
	,[LastSaleBranchPostCodeDistrict]
	,[LastSaleBranchPyramidCode]
	,[CustomerType]
	,[CustomerTypeDescription]
	,[TradingStatusCategory]
	,[TradingStatusCategoryDescription]
	,[AccountManagerCode]
	,[AccountManagerFullName]
	,[AccountManagerEmail]
	,[AccountClientUniqueIdentifier]
	,[AccountNumber]
	,[AccountName]
	,[AccountNumberParent]
	,[AccountAddress1]
	,[AccountAddress2]
	,[AccountAddress3]
	,[AccountAddress4]
	,[AccountPostCode]
	,[CreditLimit]
	,[PaymentTermName]
	,[IntercompanyAccount]
	,[OverLimitStatus]
	,[OverdueStatus]
	,[TermsParent]
	,[PricingCategory]
	,[AccountOwnerType]
	,[AccountOwnerBucket]
	,[CustomerSegment]
	,[CustomerRebate]
	,[DateOpened]
	,[DateClosed]
	,[VendorKey]
	,[VendorDescription]
	,[VendorNumber]
	,[ItemNumber]
	,[ItemDescription]
	,[VendorStockNumber]
	,[ItemPyramidCode]
	,[ProdBrandAtLLSPG]
	,[MPGCode]
	,[MPGDescription]
	,[SuperCategory]
	,[Category]
	,[SubCategory]
	,[ItemObsolete]
	,[HireInd]
	,[SpecialInd]
	,[ProductLifecycle]
	,[OwnBrandInd]
	,[KnownValueInd]
	,[NDPInd]
	,[MultiSourcedProductInd]
	,[VariablePackInd]
	,[ZonedInd]
	,[UnitCode]
	,[GenericProductID]
	,[ItemDateForDeletion]
	,[LLSPGCode]
	,[HLSPGCode]
	,[GSPGCode]
	,[LLSPGDescription]
	,[HLSPGDescription]
	,[GSPGDescription]
	,[ShipmentType]
	,[ContractClaimsIndicator]
	,[StrategicItem]
	,[SubLLSPG]
	,[NewItemForAccountIndicator]
	,[Sales603010Bucket]
	,[TotalSalesAccountBucket]
	,[FrequencyBucket]
	,[CustBaseBucket]
	,[FreqPurchBucket]
	,[TopSellerBucket]
	,[PriceApproach]
	,[TotalAccountSales]
	,[TotalNumberOfItemsSoldByAccount]
	,[TotalAccountGP]
	,[TotalNumberOfSuperCategoriesSold]
	,[ExcludeForSegmentationIndicator]
	,[PercentAccountItemSalesToAccountSales]
	,[AverageQuantityPerInvoiceLine]
	,[LastInvoiceDate]
	,[LastQuantity]
	,[LastItemPrice]
	,[LastItemCost]
	,[LastItemDiscountPercent]
	,[LastItemGP]
	,[LastItemGPP]
	,[Total12MonthInvoiceLineCount]
	,[Total12MonthQuantity]
	,[Total12MonthSales]
	,[Total12MonthCost]
	,[Total12MonthGP]
	,[Total12MonthGPP]
	,[LastPriceDerivationBucket]
	,[LastCustomerContractIndicator]
	,[LastBranchCode]
	,[LastTransNo]
	,[LastDelNo]
	,[LastLineNumber]
	,[LastNetQty]
	,[LastNetLinePrice]
	,[LastNetApTradingMarginPadRelease]
	,[LastDeliveryDate]
	,[LastSledgerNo]
	,[LastSledgerDate]
	,[LastYMTH]
	,[LastSupplyType]
	,[LastDocInd]
	,[LastCreditReason]
	,[LastKnownValueItem]
	,[LastCtsRelCode]
	,[LastCustCntNo]
	,[LastMthdOfDespatch]
	,[LastTransType]
	,[LastSalesOrigin]
	,[LastTakenBy]
	,[LastMethodOfPayment]
	,[LastHeaderID]
	,[LastCrdOrigInvNo]
	,[LastCrdOrigInvDate]
	,[LastOriginalOrdNo]
	,[LastCarriagePack]
	,[LastDelAddPhone]
	,[LastGpAllocBranchCode]
	,[LastSalesOriginDesc]
	,[LastPriceOrderType]
	,[LastTransactionDate]
	,[LastOrderDate]
	,[LastTradingPrice]
	,[LastTradeGPP]
	,[LastTotalDiscount]
	,[LastPriceDerivation]
	,[LastVariablePackInd]
	,[LastClaimNo]
	,[LastApTradingMarginPadRelease]
	,[LastCCProfit]
	,[LastRetailPrice]
	,[RPBPlaybookPricingGroupKey]
	,[CoveringBandGroupValues]
	,[CoveringBandPricingRuleKey]
	,[CoveringBandPlaybookPricingGroupQuantityBandKey]
	,[CoveringBandLowerBandQuantity]
	,[CoveringBandUpperBandQuantity]
	,[CoveringBandRulingMemberCount]
	,[CoveringBandBandSequenceNumber]
	,[CoveringBandPricingRuleSequenceNumber]
	,[FloorPlaybookPricingGroupPriceBandKey]
	,[FloorPlaybookPricingGroupKey]
	,[FloorLowerBandQuantity]
	,[FloorUpperBandQuantity]
	,[FloorMemberRank]
	,[FloorRulingMemberCount]
	,[FloorBandSequenceNumber]
	,[FloorPlaybookPricingGroupQuantityBandKey]
	,[FloorPricingRuleSequenceNumber]
	,[TargetPlaybookPricingGroupPriceBandKey]
	,[TargetPlaybookPricingGroupKey]
	,[TargetLowerBandQuantity]
	,[TargetUpperBandQuantity]
	,[TargetMemberRank]
	,[TargetRulingMemberCount]
	,[TargetBandSequenceNumber]
	,[TargetPlaybookPricingGroupQuantityBandKey]
	,[TargetPricingRuleSequenceNumber]
	,[StretchPlaybookPricingGroupPriceBandKey]
	,[StretchPlaybookPricingGroupKey]
	,[StretchLowerBandQuantity]
	,[StretchUpperBandQuantity]
	,[StretchMemberRank]
	,[StretchRulingMemberCount]
	,[StretchBandSequenceNumber]
	,[StretchPlaybookPricingGroupQuantityBandKey]
	,[StretchPricingRuleSequenceNumber]
	,[FloorPercentile]
	,[TargetPercentile]
	,[StretchPercentile]
	,[FloorGPP]
	,[TargetGPP]
	,[StretchGPP]
	,[EffectiveFloorDiscountPercent]
	,[EffectiveTargetDiscountPercent]
	,[EffectiveStretchDiscountPercent]
FROM lion.vwBandedAISDReview
SELECT @RowsI = @RowsI + @@ROWCOUNT

EXEC DBA_IndexRebuild 'RBP_AISDReview', 'Create', NULL, 'lion'

EXEC LogDCPEvent 'RPB - Building RBP_AISDReview', 'E', 0, @RowsI, 0
GO
