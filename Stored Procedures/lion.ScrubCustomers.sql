SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [lion].[ScrubCustomers] 
AS
BEGIN
	SET NOCOUNT ON
	
	EXEC LogDCPEvent N'ETL - ScrubCustomers: CustomerType', N'B'
	UPDATE s
	SET
		s.ETLRowStatusCodeKey = 4
	FROM
		Lion_Staging.dbo.Customer s
	WHERE
		CustomerType IN (N'BD', N'BM', N'IN', N'LC')
		OR LTRIM(RTRIM(ISNULL(CustomerType, N''))) = N''
	EXEC LogDCPEvent N'ETL - ScrubCustomers: CustomerType', N'E', 0, @@ROWCOUNT, 0

	EXEC LogDCPEvent N'ETL - ScrubCustomers: IntercompanyAccount Indicator', N'B'
	UPDATE s
	SET
		s.ETLRowStatusCodeKey = 5
	FROM
		Lion_Staging.dbo.Customer s
	WHERE
		IntercompanyAccount = N'Y'
	EXEC LogDCPEvent N'ETL - ScrubCustomers: IntercompanyAccount Indicator', N'E', 0, @@ROWCOUNT, 0

	EXEC LogDCPEvent N'ETL - ScrubCustomers: IntercompanyAccount', N'B'
	UPDATE s
	SET
		s.ETLRowStatusCodeKey = 5
	FROM
		Lion_Staging.dbo.Customer s
	WHERE
		AccountID IN (N'7955F43', N'7618Q25', N'7818H68')
	EXEC LogDCPEvent N'ETL - ScrubCustomers: IntercompanyAccount', N'E', 0, @@ROWCOUNT, 0



 

END
GO
