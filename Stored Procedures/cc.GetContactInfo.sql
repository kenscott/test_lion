SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [cc].[GetContactInfo]
		(@ContractOwner varchar(10),
		@WebUserKey int)

--SET @ContractOwner = 'WUK'
--SET @WebUserKey = 1228


AS 

SET NOCOUNT ON

SELECT 

CASE
		WHEN @ContractOwner = 'WUK' --Advanous Contract 
		THEN (SELECT dp.Email FROM WebUser wu INNER JOIN DimPerson dp ON dp.ODSPersonKey = wu.ODSPersonKey WHERE wu.WebUserKey = @WebUserKey)
		WHEN dag1.AccountGroup1Key IS NOT NULL AND dag1.AG1Level1UDVarchar15 <> ''
		THEN dag1.AG1Level1UDVarchar15                          --General Branch Email Address
		WHEN dag1.AccountGroup1Key IS NOT NULL AND dag1.AG1Level1UDVarchar24 <> ''
		THEN dag1.AG1Level1UDVarchar24							--Branch Manager Email Address
		ELSE 'contract.claims@wolseley.co.uk'
END AS ContactEmail,
CASE
		WHEN @ContractOwner = 'WUK' --Advanous Contract 
		THEN (SELECT dp.FullName FROM WebUser wu Inner Join DimPerson dp ON dp.ODSPersonKey = wu.ODSPersonKey WHERE wu.WebUserKey = @WebUserKey)
		WHEN dag1.AccountGroup1Key IS NOT NULL AND dag1.AG1Level1UDVarchar1 <> ''
		THEN dag1.AG1Level1UDVarchar1                          --General Branch Description
		WHEN dag1.AccountGroup1Key IS NOT NULL AND dag1.AG1Level1UDVarchar23 <> ''
		THEN dag1.AG1Level1UDVarchar23							--Branch Manager Name
		ELSE 'Contract Claims'
END AS ContactName

FROM dbo.DimAccountGroup1 dag1
WHERE dag1.AG1Level1 = @ContractOwner	



GO
