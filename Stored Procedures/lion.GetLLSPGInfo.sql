SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE PROCEDURE [lion].[GetLLSPGInfo]
    @LLSPGCode Description_Small_type
AS 

SET NOCOUNT ON

select i.LLSPGCode, i.LLSPGDescription, count(*) as [Number of Products], 
        (select count(distinct accountkey)
		   from dbo.FactLastPriceAndCost f inner join
		        lion.vwItem ii on f.itemkey = ii.itemkey
		  where ii.LLSPGCode = i.LLSPGCode and f.Total12MonthSales > 0) as [Accounts with Sales],
		(select count(distinct accountkey)
		   from CustomerTerms ct 
		  where ct.SPGCode = i.LLSPGCode) as [Accounts with Terms]
from lion.vwItem i
where i.LLSPGCode = @LLSPGCode and i.ItemPyramidCode = 1
group by i.LLSPGCode, i.LLSPGDescription
GO
