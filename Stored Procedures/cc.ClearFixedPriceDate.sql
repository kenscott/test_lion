SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [cc].[ClearFixedPriceDate]
		@VendorKey INT,
		@WebUserKey INT

AS

DECLARE @SupplierContractUpdateRowData TABLE (
		ContractKey int,
		OldFixedPriceDate date)

UPDATE sc
SET FixedPriceDate = NULL 
OUTPUT INSERTED.ContractKey,
		DELETED.FixedPriceDate
		INTO @SupplierContractUpdateRowData
FROM cc.SupplierContract sc
WHERE sc.VendorKey = @VendorKey 
	AND FixedPriceDate IS NOT NULL

--Create log record
INSERT INTO [cc].[SupplierContractLog]
           ([WebUserKey]
           ,[ContractKey]
           ,[Notes]
           ,[CreationDate]
           ,[ContractAction])
SELECT
		@WebUserKey,
		ContractKey,
		'Fixed Price Date changed from ' + CAST(ISNULL(OldFixedPriceDate, ' ') AS VARCHAR(50)) + ' to <blank>',
		GETDATE(),
		'Supplier Preference changed'
FROM @SupplierContractUpdateRowData
GO
