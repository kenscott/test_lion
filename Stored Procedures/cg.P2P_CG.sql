SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE 
PROCEDURE [cg].[P2P_CG]
	@ProjectKey INT, 
	@ScenarioKey INT,
	@PlaybookDataPointGroupKey INT,
	@LimitDataPlaybookKey INT,
	@SegmentFactorPlaybookKey INT, 
	@PricingRulePlaybookKey INT, 
	@ProposalRecommendationPlaybookKey INT,
	@BeginStep INT,
	@EndStep INT
AS

SET QUOTED_IDENTIFIER ON 
SET ANSI_NULLS ON 
SET ARITHABORT ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
SET CONCAT_NULL_YIELDS_NULL ON
SET NUMERIC_ROUNDABORT OFF
SET NOCOUNT ON
SET XACT_ABORT ON



IF @BeginStep <= 1 AND @EndStep >= 1
BEGIN
	EXEC cg.PopulatePlaybookDataPoint @PlaybookDataPointGroupKey
END


IF @BeginStep <= 2 AND @EndStep >= 2
BEGIN
	EXEC cg.SetPriceGroup @PlaybookDataPointGroupKey, @PricingRulePlaybookKey, @ScenarioKey
END


IF @BeginStep <= 3 AND @EndStep >= 3
BEGIN
	EXEC cg.CalcualtePlaybookPricingGroupPriceBand @ProjectKey, @PlaybookDataPointGroupKey
END


GO
