SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[DBA_IndexRebuild]
	@TableName [NVARCHAR](500),
	@Command [NVARCHAR](50),
	@Test [NVARCHAR](1) = NULL,
	@SchemaName [NVARCHAR](256) = 'dbo'
WITH EXECUTE AS CALLER
AS
SET NOCOUNT ON
DECLARE 
	@IndexName NVARCHAR(500), 
	@IndexDesc NVARCHAR(500), 
	@Cols NVARCHAR(500), 
	@IncludedColumns NVARCHAR(MAX),
	@FilterDefinition NVARCHAR(MAX),
	@ClusteredIndicator CHAR(1), 
	@UniqueIndicator CHAR(1), 
	@PI INT, 
	@FileGroupName NVARCHAR(500), 
	@PKIndicator CHAR(1), 
	@cmd NVARCHAR(MAX), 
	@DBAID INT, 
	@UniqueText NVARCHAR(50)
	
DECLARE 
	@ChildTable NVARCHAR(500), 
	@ChildColumn NVARCHAR(500), 
	@PrimaryTable NVARCHAR(500), 
	@PrimaryColumn NVARCHAR(500), 
	@KeyNo INT, 
	@AllChildCols NVARCHAR(500), 
	@AllPrimaryCols NVARCHAR(500), 
	@ClusteredText NVARCHAR(50)

DECLARE 
	@FK NVARCHAR(500)

DECLARE
	@ChildSchemaName NVARCHAR(256),
	@PrimarySchemaName NVARCHAR(256)

DECLARE
	@StartTime DATETIME,
	@EndTime DATETIME,
	@MsgStr NVARCHAR(4000)
	
SELECT @StartTime = GETDATE()

IF NOT EXISTS (SELECT * FROM sysobjects WHERE xtype = 'u' AND name = @TableName AND SCHEMA_NAME(sysObjects.uid) = @SchemaName)
BEGIN
	PRINT 'Please spefict a valid tablename as the first paramater'
	RETURN
END
IF @Command NOT IN ('Drop', 'Create', 'CreateIndexesOnly', 'Drop_NoClustered')
BEGIN
	PRINT 'Please specify Drop or Create as the second paramater'
	RETURN
END


IF NOT EXISTS (SELECT * FROM sysobjects WHERE name = 'DBA_IndexRebuild_Log')
BEGIN
--drop table DBA_IndexRebuild_Log
	CREATE TABLE DBA_IndexRebuild_Log (TableName NVARCHAR(500), Command NVARCHAR(50), Statement NVARCHAR(500), BeginTime DATETIME, EndTime DATETIME, DBAID INT IDENTITY(1,1) )
END

IF NOT EXISTS (SELECT * FROM sysobjects WHERE name = 'DBA_Index_Rebuild')
BEGIN
-- drop table DBA_Index_Rebuild
	CREATE TABLE [dbo].[DBA_Index_Rebuild](
		[SchemaName] [NVARCHAR](128) NOT NULL,
		[TableName] [NVARCHAR](128) NOT NULL,
		[IndexName] [NVARCHAR](128) NOT NULL,
		[Cols] [NVARCHAR](500) NULL,
		[FileGroupName] [NVARCHAR](500) NULL,
		[UniqueIndicator] [CHAR](1) NULL,
		[PKIndicator] [CHAR](1) NULL,
		[ClusteredIndicator] [CHAR](1) NULL,
	 CONSTRAINT [PK_DBA_Index_Rebuild_1] PRIMARY KEY CLUSTERED 
	(
		[SchemaName] ASC,
		[TableName] ASC,
		[IndexName] ASC
	)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
	) ON [PRIMARY]
	ALTER TABLE [dbo].[DBA_Index_Rebuild] ADD  CONSTRAINT [DF_DBA_Index_Rebuild_SchemaName]  DEFAULT ('dbo') FOR [SchemaName]

END

IF NOT EXISTS (SELECT * FROM sysobjects WHERE name = 'DBA_FK_Rebuild')
BEGIN
-- drop table DBA_FK_Rebuild
	CREATE TABLE [dbo].[DBA_FK_Rebuild](
		[FKName] [NVARCHAR](500) NOT NULL,
		[ChildSchemaName] [NVARCHAR](256) NOT NULL,
		[ChildTableName] [NVARCHAR](500) NOT NULL,
		[PrimarySchemaName] [NVARCHAR](256) NOT NULL,
		[PrimaryTableName] [NVARCHAR](500) NOT NULL,
		[ChildCols] [NVARCHAR](500) NOT NULL,
		[PrimaryCols] [NVARCHAR](500) NOT NULL,
	 CONSTRAINT [PK_DBA_FK_Rebuild] PRIMARY KEY CLUSTERED 
	(
		[FKName] ASC,
		[ChildSchemaName] ASC
	)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
	) ON [PRIMARY]
END



IF @Command IN ('Drop', 'Drop_NoClustered')
BEGIN

		--This table holds PK and Indexes to be dropped
		CREATE TABLE #TempIndexRebuild (IndexName NVARCHAR(500), IndexDesc NVARCHAR(500), Cols NVARCHAR(500))
		INSERT INTO #TempIndexRebuild
		EXEC ('sp_helpindex ''[' + @SchemaName + '].['+@TableName+']''')
		UPDATE #TempIndexRebuild SET Cols = dbo.fn_WrapColumnsWithBrackets(Cols)

		--This table holds PK and Indexes to be dropped
		CREATE TABLE #TempIndexRebuild1 (IndexName NVARCHAR(500), IndexDesc NVARCHAR(500), Cols NVARCHAR(500), IncludedColumns NVARCHAR(MAX), FilterDefinition NVARCHAR(MAX))
		INSERT INTO #TempIndexRebuild1 
		SELECT IndexName , IndexDesc, Cols, dbo.fn_GetIncludedColumnsForIndex(@SchemaName, @TableName, IndexName) AS IncludedColumns, dbo.fn_GetFilterdefinitionForIndex(@SchemaName, @TableName, IndexName)
		FROM #TempIndexRebuild

		DROP TABLE #TempIndexRebuild
		

		--This table holds FK constraints to be dropped
		CREATE TABLE #FKs (
			FKName NVARCHAR(500), 
			ChildSchemaName NVARCHAR(500),
			ChildTable NVARCHAR(500), 
			ChildColumn NVARCHAR(500),
			PrimarySchemaName NVARCHAR(500), 
			PrimaryTable NVARCHAR(500), 
			PrimaryColumn NVARCHAR(500), 
			KeyNo INT)
		
		INSERT #FKs (FKName, ChildSchemaName, ChildTable, ChildColumn, PrimarySchemaName, PrimaryTable, PrimaryColumn, KeyNo)
		SELECT 
			SysObjects.Name AS 'FKName', ChildTable.SchemaName AS ChildSchema, ChildTable.tablename AS 'ChildTable', ChildTable.columnname AS 'ChildColumn', 
			PrimaryTable.SchemaName AS PrimarySchemaName, PrimaryTable.tablename AS 'PrimaryTable', PrimaryTable.columnname AS 'PrimaryColumn', KeyNo
		FROM sysforeignkeys 
		JOIN (SELECT SysObjects.Name tablename, 
					SysObjects.id, 
					SCHEMA_NAME(SysObjects.uid) AS SchemaName,
					sysColumns.name columnname, 
					sysColumns.colid  
				FROM SysObjects 
				JOIN sysColumns ON 
					SysObjects.id = sysColumns.id 
				WHERE SysObjects.xtype = 'U'
				) PrimaryTable ON sysforeignkeys.rkeyID = PrimaryTable.ID AND sysforeignkeys.rkey = PrimaryTable.colid
		JOIN (SELECT SysObjects.Name tablename, 
					SysObjects.id, 
					SCHEMA_NAME(SysObjects.uid) AS SchemaName,
					sysColumns.name columnname, 
					sysColumns.colid  
				FROM SysObjects 
				JOIN sysColumns ON SysObjects.id = sysColumns.id 
				WHERE SysObjects.xtype = 'U'
				) ChildTable ON sysforeignkeys.fkeyID = ChildTable.ID AND sysforeignkeys.fkey = ChildTable.colid
		JOIN SysObjects ON SysObjects.ID = sysforeignkeys.constid
		WHERE (ChildTable.tablename = @TableName AND ChildTable.SchemaName = @SchemaName)
			OR (PrimaryTable.tablename = @TableName AND PrimaryTable.SchemaName = @SchemaName)


		------------------------------------------------------------------------------------------------------
		--Now drop all the FKs that reference this tables PK
		------------------------------------------------------------------------------------------------------
--select * from #FKs
		DECLARE FKs CURSOR FOR
		SELECT DISTINCT FKName, ChildSchemaName, ChildTable FROM #FKs
		OPEN FKs
		FETCH NEXT FROM FKs INTO @FK, @ChildSchemaName, @ChildTable
		WHILE @@fetch_status = 0
		BEGIN
			SET @cmd = 'alter table ['+ @ChildSchemaName + '].['+@ChildTable+'] drop constraint ['+@FK+']'
			IF @Test IS NOT NULL
			BEGIN
				PRINT @cmd
			END

			IF @Test IS NULL
			BEGIN
				INSERT DBA_IndexRebuild_Log (TableName,Command,Statement,BeginTime,EndTime) --- 
				VALUES ('['+ @ChildSchemaName + '].['+@ChildTable+']', @Command, @cmd, GETDATE(), NULL)

				SET @DBAID = @@identity

				EXEC (@cmd)

				UPDATE DBA_IndexRebuild_Log SET EndTime = GETDATE() WHERE DBAID = @DBAID
		
				SELECT @AllChildCols = '', @AllPrimaryCols = ''
				DECLARE Cols CURSOR FOR
				SELECT ChildSchemaName, ChildTable, ChildColumn, PrimarySchemaName, PrimaryTable, PrimaryColumn
				FROM #FKs
				WHERE FKName = @FK
				AND ChildSchemaName = @ChildSchemaName
				
				ORDER BY KeyNo
				
				OPEN Cols
				FETCH NEXT FROM Cols INTO @ChildSchemaName, @ChildTable, @ChildColumn, @PrimarySchemaName, @PrimaryTable, @PrimaryColumn
				WHILE @@fetch_status = 0
				BEGIN
			--Alter table IndexTest add constraint FK_IndexTest_1 Foreign Key (b, c) references IndexTest2(b, c)
			--Alter table IndexTest add constraint FK_IndexTest_2 Foreign Key (c, b) references IndexTest2(b, c)
					SET @AllChildCols = @AllChildCols + '['+@ChildColumn + '],'
					SET @AllPrimaryCols = @AllPrimaryCols + '['+@PrimaryColumn + '],'
			
					FETCH NEXT FROM Cols INTO @ChildSchemaName, @ChildTable, @ChildColumn, @PrimarySchemaName, @PrimaryTable, @PrimaryColumn
				END
				CLOSE Cols
				DEALLOCATE Cols
			
				SET @AllChildCols = LEFT(@AllChildCols, LEN(@AllChildCols)-1) --to remove the last comma
				SET @AllPrimaryCols = LEFT(@AllPrimaryCols, LEN(@AllPrimaryCols)-1) --to remove the last comma
	
				IF NOT EXISTS (SELECT * FROM DBA_FK_Rebuild WHERE [FKName] = @FK AND ChildSchemaName = @ChildSchemaName)
					INSERT DBA_FK_Rebuild (FKName, ChildSchemaName, ChildTableName, PrimarySchemaName, PrimaryTableName, ChildCols, PrimaryCols)
					VALUES (@FK, @ChildSchemaName, @ChildTable, @PrimarySchemaName, @PrimaryTable, @AllChildCols, @AllPrimaryCols)
			END
			FETCH NEXT FROM FKs INTO @FK, @ChildSchemaName, @ChildTable
		END
		CLOSE FKs
		DEALLOCATE FKs
		--select * From #FKs
		DROP TABLE #FKs




		------------------------------------------------------------------------------------------------------
		--Drop all the nonclustered indexes first
		------------------------------------------------------------------------------------------------------

		DECLARE a CURSOR FOR
		SELECT IndexName, IndexDesc, Cols, IncludedColumns, FilterDefinition FROM #TempIndexRebuild1 WHERE IndexDesc NOT LIKE '%primary key%' AND IndexDesc LIKE '%nonclustered%'
		OPEN a
		FETCH NEXT FROM a INTO @IndexName, @IndexDesc, @Cols, @IncludedColumns, @FilterDefinition
		WHILE @@fetch_status = 0
		BEGIN
			IF @Test IS NOT NULL PRINT 'Drop index ['+ @SchemaName + '].['+@TableName+'].['+@IndexName+']'
			SET @cmd = 'Drop index ['+ @SchemaName + '].['+@TableName+'].['+@IndexName+']'

			IF @Test IS NULL
			BEGIN
				INSERT DBA_IndexRebuild_Log (TableName,Command,Statement,BeginTime,EndTime)
				VALUES ('['+ @SchemaName + '].['+@TableName+']', @Command, @cmd, GETDATE(), NULL)

				SET @DBAID = @@identity

				EXEC (@cmd)

				UPDATE DBA_IndexRebuild_Log SET EndTime = GETDATE() WHERE DBAID = @DBAID

				IF @IndexDesc LIKE '%nonclustered%'
					SET @ClusteredIndicator = 'N'
				ELSE
					SET @ClusteredIndicator = 'Y'
			
				IF @IndexDesc LIKE '%unique%'
					SET @UniqueIndicator = 'Y'
				ELSE
					SET @UniqueIndicator = 'N'
			
				SET @PKIndicator = 'N'
			
				SET @PI = PATINDEX('%located on %', @IndexDesc)+LEN('located on ')
				SET @FileGroupName = SUBSTRING (@IndexDesc, @PI+1, LEN(@IndexDesc)-@PI )
		
				UPDATE DBA_Index_Rebuild
					SET 
					SchemaName = @SchemaName,
					TableName = @TableName,
					IndexName = @IndexName,
					FileGroupName = @FileGroupName,
					UniqueIndicator = @UniqueIndicator,
					ClusteredIndicator = @ClusteredIndicator, 
					Cols = @Cols, 
					PKIndicator = @PKIndicator,
					IncludedColumns = @IncludedColumns,
					FilterDefinition = @FilterDefinition
				WHERE SchemaName = @SchemaName
					AND TableName = @TableName
					AND IndexName = @IndexName
				IF @@ROWCOUNT = 0 	-- row count must be checked immediately after the UPDATE
				BEGIN
					INSERT DBA_Index_Rebuild (SchemaName, TableName,IndexName,FileGroupName,UniqueIndicator,ClusteredIndicator, Cols, PKIndicator, IncludedColumns, FilterDefinition)
					VALUES (@SchemaName, @TableName, @IndexName, @FileGroupName, @UniqueIndicator, @ClusteredIndicator, @Cols, @PKIndicator, @IncludedColumns, @FilterDefinition)
				END
			END

			FETCH NEXT FROM a INTO @IndexName, @IndexDesc, @Cols, @IncludedColumns, @FilterDefinition
		END--End the Index drop
		CLOSE a
		DEALLOCATE a

		------------------------------------------------------------------------------------------------------
		--Drop all the clustered indexes
		------------------------------------------------------------------------------------------------------
		IF @Command <> 'Drop_NoClustered'
		BEGIN
			DECLARE a CURSOR FOR
			SELECT IndexName, IndexDesc, Cols, IncludedColumns, FilterDefinition FROM #TempIndexRebuild1 WHERE IndexDesc NOT LIKE '%primary key%' AND IndexDesc NOT LIKE '%nonclustered%'
			OPEN a
			FETCH NEXT FROM a INTO @IndexName, @IndexDesc, @Cols, @IncludedColumns, @FilterDefinition
			WHILE @@fetch_status = 0
			BEGIN
				IF @Test IS NOT NULL PRINT 'Drop index ['+@SchemaName + '].['+@TableName+'].['+@IndexName+']'
				SET @cmd = 'Drop index ['+@SchemaName + '].['+@TableName+'].['+@IndexName+']'
	
				IF @Test IS NULL
				BEGIN
					INSERT DBA_IndexRebuild_Log (TableName,Command,Statement,BeginTime,EndTime)
					VALUES ('['+@SchemaName + '].['+@TableName+']', @Command, @cmd, GETDATE(), NULL)
	
					SET @DBAID = @@identity
	
					EXEC (@cmd)
	
					UPDATE DBA_IndexRebuild_Log SET EndTime = GETDATE() WHERE DBAID = @DBAID
	
					IF @IndexDesc LIKE '%nonclustered%'
						SET @ClusteredIndicator = 'N'
					ELSE
						SET @ClusteredIndicator = 'Y'
				
					IF @IndexDesc LIKE '%unique%'
						SET @UniqueIndicator = 'Y'
					ELSE
						SET @UniqueIndicator = 'N'
				
					SET @PKIndicator = 'N'
				
					SET @PI = PATINDEX('%located on %', @IndexDesc)+LEN('located on ')
					SET @FileGroupName = SUBSTRING (@IndexDesc, @PI+1, LEN(@IndexDesc)-@PI )


					UPDATE DBA_Index_Rebuild
						SET 
						SchemaName = @SchemaName,
						TableName = @TableName,
						IndexName = @IndexName,
						FileGroupName = @FileGroupName,
						UniqueIndicator = @UniqueIndicator,
						ClusteredIndicator = @ClusteredIndicator, 
						Cols = @Cols, 
						PKIndicator = @PKIndicator,
						IncludedColumns = @IncludedColumns,
						FilterDefinition = @FilterDefinition
					WHERE SchemaName = @SchemaName
						AND TableName = @TableName
						AND IndexName = @IndexName
					IF @@ROWCOUNT = 0 	-- row count must be checked immediately after the UPDATE
					BEGIN
						INSERT DBA_Index_Rebuild (SchemaName, TableName,IndexName,FileGroupName,UniqueIndicator,ClusteredIndicator, Cols, PKIndicator, IncludedColumns, FilterDefinition)
						VALUES (@SchemaName, @TableName, @IndexName, @FileGroupName, @UniqueIndicator, @ClusteredIndicator, @Cols, @PKIndicator, @IncludedColumns, @FilterDefinition)
					END

				END
	
				FETCH NEXT FROM a INTO @IndexName, @IndexDesc, @Cols, @IncludedColumns, @FilterDefinition
			END--End the Index drop
			CLOSE a
			DEALLOCATE a
		END



		------------------------------------------------------------------------------------------------------
		--Drop the PK Constraint
		------------------------------------------------------------------------------------------------------
		IF EXISTS (SELECT 1 FROM #TempIndexRebuild1 WHERE IndexDesc LIKE '%primary key%')
					AND @Command <> 'Drop_NoClustered'
		BEGIN
			SELECT @IndexName = IndexName, @IndexDesc = IndexDesc, @Cols = Cols, @IncludedColumns = IncludedColumns, @FilterDefinition = FilterDefinition FROM #TempIndexRebuild1 WHERE IndexDesc LIKE '%primary key%'
			IF @IndexName IS NOT NULL
			BEGIN

				
				
			IF @Test IS NOT NULL PRINT 'ALTER TABLE ['+ @SchemaName + '].['+@TableName+'] DROP CONSTRAINT ['+@IndexName+']'
				SET @cmd = 'ALTER TABLE ['+ @SchemaName + '].['+@TableName+'] DROP CONSTRAINT ['+@IndexName+']'

				IF @Test IS NULL
				BEGIN
					INSERT DBA_IndexRebuild_Log (TableName,Command,Statement,BeginTime,EndTime)
					VALUES ('['+ @SchemaName + '].['+@TableName+']', @Command, @cmd, GETDATE(), NULL)
					SET @DBAID = @@identity

 					EXEC (@cmd)
					UPDATE DBA_IndexRebuild_Log SET EndTime = GETDATE() WHERE DBAID = @DBAID

			
					IF @IndexDesc LIKE '%nonclustered%'
						SET @ClusteredIndicator = 'N'
					ELSE
						SET @ClusteredIndicator = 'Y'
				
					IF @IndexDesc LIKE '%unique%'
						SET @UniqueIndicator = 'Y'
					ELSE
						SET @UniqueIndicator = 'N'
				
					SET @PI = PATINDEX('%located on %', @IndexDesc)+LEN('located on ')
					SET @FileGroupName = SUBSTRING (@IndexDesc, @PI+1, LEN(@IndexDesc)-@PI )
				
					SET @PKIndicator = 'Y'
	
					UPDATE DBA_Index_Rebuild
						SET 
						SchemaName = @SchemaName,
						TableName = @TableName,
						IndexName = @IndexName,
						FileGroupName = @FileGroupName,
						UniqueIndicator = @UniqueIndicator,
						ClusteredIndicator = @ClusteredIndicator, 
						Cols = @Cols, 
						PKIndicator = @PKIndicator,
						IncludedColumns = @IncludedColumns,
						FilterDefinition = @FilterDefinition
					WHERE SchemaName = @SchemaName
						AND TableName = @TableName
						AND IndexName = @IndexName
					IF @@ROWCOUNT = 0 	-- row count must be checked immediately after the UPDATE
					BEGIN
						INSERT DBA_Index_Rebuild (SchemaName, TableName,IndexName,FileGroupName,UniqueIndicator,ClusteredIndicator, Cols, PKIndicator, IncludedColumns, FilterDefinition)
						VALUES (@SchemaName, @TableName, @IndexName, @FileGroupName, @UniqueIndicator, @ClusteredIndicator, @Cols, @PKIndicator, @IncludedColumns, @FilterDefinition)
					END

				END
			END
		END--End the PK drop
		DROP TABLE #TempIndexRebuild1
END --End the drop command


IF @Command IN ('Create', 'CreateIndexesOnly')
BEGIN
		------------------------------------------------------------------------------------------------------
		--If a clustered index exists which is not the PK, create it first
		------------------------------------------------------------------------------------------------------
		IF EXISTS (SELECT 1 FROM DBA_Index_Rebuild
							WHERE ClusteredIndicator = 'Y' 
							AND PKIndicator = 'N'
							AND TableName = @TableName
							AND SchemaName = @SchemaName
							)
		BEGIN
			DECLARE ClusteredIndexCreate CURSOR FOR
			SELECT IndexName, Cols, FileGroupName, 
			CASE ClusteredIndicator 
				WHEN 'Y' THEN 'Clustered'
				ELSE 'NonClustered'
			END,
			CASE UniqueIndicator 
				WHEN 'Y' THEN 'Unique'
				ELSE ''
			END,
			IncludedColumns,
			FilterDefinition
			FROM DBA_Index_Rebuild
			WHERE PKIndicator = 'N' 
				AND TableName = @TableName
				AND SchemaName = @SchemaName
				AND ClusteredIndicator = 'Y'
			ORDER BY ClusteredIndicator DESC	--So the clustered index gets created first
			OPEN ClusteredIndexCreate
			FETCH NEXT FROM ClusteredIndexCreate INTO @IndexName, @Cols, @FileGroupName, @ClusteredText, @UniqueText, @IncludedColumns, @FilterDefinition
			WHILE @@fetch_status = 0
			BEGIN
				SET @Cols = REPLACE(@Cols,'(-)]', '] desc')	--replace the minus signs with desc for descending indexes

				IF @IncludedColumns IS NOT NULL
				BEGIN
				
					SET @cmd = 'IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N''[' + @SchemaName + '].[' + @TableName + ']'') AND name = N''' + @IndexName+ ''')  ' +
						'Create '+@UniqueText+' '+@ClusteredText+' index ['+@IndexName+
								'] on ['+ @SchemaName + '].['+@TableName+
								']('+@Cols+')'+
								' INCLUDE (' + @IncludedColumns + ') ' + 
								CASE WHEN @FilterDefinition IS NOT NULL THEN ' WHERE ' + @FilterDefinition ELSE ' ' END +
								'ON ['+@FileGroupName+']'
				END
				ELSE
				BEGIN
				
					SET @cmd = 'IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N''[' + @SchemaName + '].[' + @TableName + ']'') AND name = N''' + @IndexName+ ''')  ' +
						'Create '+@UniqueText+' '+@ClusteredText+' index ['+@IndexName+
						'] on ['+ @SchemaName + '].['+@TableName+
						']('+@Cols+')'+
						CASE WHEN @FilterDefinition IS NOT NULL THEN ' WHERE ' + @FilterDefinition ELSE ' ' END +
						'on ['+@FileGroupName+']'
					
				END
				
	
					IF @Test IS NOT NULL
					BEGIN
						PRINT @cmd
					END
		
					IF @Test IS NULL
					BEGIN
						INSERT DBA_IndexRebuild_Log (TableName,Command,Statement,BeginTime,EndTime)
						VALUES ('['+@SchemaName+'].['+@TableName+']', @Command, @cmd, GETDATE(), NULL)
	
						SET @DBAID = @@identity
	
						EXEC (@cmd)
	
						UPDATE DBA_IndexRebuild_Log SET EndTime = GETDATE() WHERE DBAID = @DBAID
	
						DELETE FROM DBA_Index_Rebuild WHERE TableName = @TableName AND IndexName = @IndexName AND SchemaName = @SchemaName	--Delete anything we just rebuilt
					END
	
					FETCH NEXT FROM ClusteredIndexCreate INTO @IndexName, @Cols, @FileGroupName, @ClusteredText, @UniqueText, @IncludedColumns, @FilterDefinition
			
			END--End index create
			CLOSE ClusteredIndexCreate
			DEALLOCATE ClusteredIndexCreate
		END

		------------------------------------------------------------------------------------------------------
		--Create PK Index, where it's clustered or not:
		------------------------------------------------------------------------------------------------------
		IF EXISTS (SELECT 1 FROM DBA_Index_Rebuild
							WHERE PKIndicator = 'Y' AND TableName = @TableName AND SchemaName = @SchemaName)
		BEGIN
				IF (SELECT ClusteredIndicator FROM DBA_Index_Rebuild WHERE PKIndicator = 'Y' AND TableName = @TableName AND SchemaName = @SchemaName) = 'Y'
				BEGIN
					SET @ClusteredText = 'CLUSTERED'
				END
				ELSE
				BEGIN
					SET @ClusteredText = 'NONCLUSTERED'				
				END
				
				SELECT 
					@cmd = 'IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N''[' + SchemaName + '].[' + TableName + ']'') AND name = N''' + IndexName+ ''')  ' +
						'Alter table ['+SchemaName+'].['+TableName+'] add constraint ['+IndexName+'] PRIMARY KEY '+ @ClusteredText + ' ' + '(' + Cols + ')'	
				
				FROM DBA_Index_Rebuild
				WHERE PKIndicator = 'Y' AND TableName = @TableName AND SchemaName = @SchemaName
				IF @Test IS NOT NULL
				BEGIN
					PRINT @cmd
				END

				IF @Test IS NULL
				BEGIN
					INSERT DBA_IndexRebuild_Log (TableName,Command,Statement,BeginTime,EndTime)
					VALUES ('['+@SchemaName+'].['+@TableName+']', @Command, @cmd, GETDATE(), NULL)
					SET @DBAID = @@identity

					EXEC (@cmd)

					UPDATE DBA_IndexRebuild_Log SET EndTime = GETDATE() WHERE DBAID = @DBAID
	
					DELETE FROM DBA_Index_Rebuild WHERE TableName = @TableName AND PKIndicator = 'Y' AND SchemaName = @SchemaName	--Delete anything we just rebuilt
				END
		END --End create PK index

		------------------------------------------------------------------------------------------------------
		--Create all nonclustered indexes:
		------------------------------------------------------------------------------------------------------
		DECLARE NonClusteredIndexCreate CURSOR FOR
		SELECT IndexName, Cols, FileGroupName, 
		CASE ClusteredIndicator 
			WHEN 'Y' THEN 'Clustered'
			ELSE 'NonClustered'
		END,
		CASE UniqueIndicator 
			WHEN 'Y' THEN 'Unique'
			ELSE ''
		END,
		IncludedColumns,
		FilterDefinition
		FROM DBA_Index_Rebuild
		WHERE ClusteredIndicator = 'N'
			AND PKIndicator = 'N' 
			AND TableName = @TableName
			AND SchemaName = @SchemaName
		ORDER BY ClusteredIndicator DESC	--So the clustered index gets created first
		OPEN NonClusteredIndexCreate
		FETCH NEXT FROM NonClusteredIndexCreate INTO @IndexName, @Cols, @FileGroupName, @ClusteredText, @UniqueText, @IncludedColumns, @FilterDefinition
		WHILE @@fetch_status = 0
		BEGIN
				SET @Cols = REPLACE(@Cols,'(-)]', '] desc')	--replace the minus signs with desc for descending indexes

				IF @IncludedColumns IS NOT NULL
				BEGIN
				
					SET @cmd = 'IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N''[' + @SchemaName + '].[' + @TableName + ']'') AND name = N''' + @IndexName+ ''')  ' +
						'Create '+@UniqueText+' '+@ClusteredText+' index ['+@IndexName+
								'] on ['+ @SchemaName + '].['+@TableName+
								']('+@Cols+')'+
								' INCLUDE (' + @IncludedColumns + ') ' + 
								CASE WHEN @FilterDefinition IS NOT NULL THEN ' WHERE ' + @FilterDefinition ELSE ' ' END +
								'ON ['+@FileGroupName+']'
				END
				ELSE
				BEGIN
					SET @cmd = 'IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N''[' + @SchemaName + '].[' + @TableName + ']'') AND name = N''' + @IndexName+ ''')  ' +
						'Create '+@UniqueText+' '+@ClusteredText+' index ['+@IndexName+
							'] on ['+ @SchemaName + '].['+@TableName+
							']('+@Cols+')'+
							CASE WHEN @FilterDefinition IS NOT NULL THEN ' WHERE ' + @FilterDefinition ELSE ' ' END + 
							'on ['+@FileGroupName+']'
				END

				IF @Test IS NOT NULL
				BEGIN
					PRINT @cmd
				END
	
				IF @Test IS NULL
				BEGIN
					INSERT DBA_IndexRebuild_Log (TableName,Command,Statement,BeginTime,EndTime)
					VALUES ('['+@SchemaName+'].['+@TableName +']', @Command, @cmd, GETDATE(), NULL)

					SET @DBAID = @@identity

					EXEC (@cmd)

					UPDATE DBA_IndexRebuild_Log SET EndTime = GETDATE() WHERE DBAID = @DBAID

					DELETE FROM DBA_Index_Rebuild WHERE TableName = @TableName AND IndexName = @IndexName AND SchemaName = @SchemaName	--Delete anything we just rebuilt
				END

				FETCH NEXT FROM NonClusteredIndexCreate INTO @IndexName, @Cols, @FileGroupName, @ClusteredText, @UniqueText, @IncludedColumns, @FilterDefinition
		
		END--End index create
		CLOSE NonClusteredIndexCreate
		DEALLOCATE NonClusteredIndexCreate

		IF @Command <> 'CreateIndexesOnly'
		BEGIN

			DECLARE FKInsert CURSOR FOR
			SELECT FKName,ChildSchemaName,ChildTableName,PrimarySchemaName,PrimaryTableName,ChildCols,PrimaryCols
			FROM DBA_FK_Rebuild WHERE ChildTableName = @TableName AND ChildSchemaName = @SchemaName--or PrimaryTableName = @TableName
			OPEN FKInsert
			FETCH NEXT FROM FKInsert INTO @FK, @ChildSchemaName, @ChildTable,@PrimarySchemaName,@PrimaryTable,@AllChildCols,@AllPrimaryCols
			WHILE @@fetch_status = 0	
			BEGIN
			
				SET @cmd = 'IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N''[' + @SchemaName + '].[' + @FK + ']'') AND parent_object_id = OBJECT_ID(N''[' + @SchemaName + '].[' + @ChildTable + ']'')) ' + 
								'Alter table ['+@ChildSchemaName+'].['+@ChildTable+'] add constraint ['+@FK+'] Foreign Key ('+ @AllChildCols+') references ['+@PrimarySchemaName+'].['+@PrimaryTable+']('+@AllPrimaryCols+')'
				IF @Test IS NOT NULL
				BEGIN
					PRINT @cmd
				END
	
				IF @Test IS NULL
				BEGIN	
					INSERT DBA_IndexRebuild_Log (TableName,Command,Statement,BeginTime,EndTime)
					VALUES ('['+@ChildSchemaName+'].['+@ChildTable+']', @Command, @cmd, GETDATE(), NULL)
					SET @DBAID = @@identity
	
					EXEC (@cmd)
	
					UPDATE DBA_IndexRebuild_Log SET EndTime = GETDATE() WHERE DBAID = @DBAID
	
					DELETE FROM DBA_FK_Rebuild WHERE FKName = @FK AND ChildTableName = @TableName AND ChildSchemaName = @SchemaName
				END
				FETCH NEXT FROM FKInsert INTO  @FK, @ChildSchemaName, @ChildTable,@PrimarySchemaName,@PrimaryTable,@AllChildCols,@AllPrimaryCols
			END
			CLOSE FKInsert
			DEALLOCATE FKInsert
		END
		
END--End the create command


SELECT @EndTime = GETDATE()
SELECT @MsgStr = '[' + CONVERT(NVARCHAR(25), @EndTime, 121) + '] ' + 'DBA_IndexRebuild performed a '+ @Command + ' on [' + @SchemaName + '].[' + @TableName + '] in ' +  + CAST(DATEDIFF(s, @StartTime, @EndTime) AS NVARCHAR(25)) + ' seconds' 
RAISERROR (@MsgStr, 0, 1) WITH NOWAIT



















GO
