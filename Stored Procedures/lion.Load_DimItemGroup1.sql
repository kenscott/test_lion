SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [lion].[Load_DimItemGroup1]
WITH EXECUTE AS CALLER
AS
SET NOCOUNT ON

EXEC LogDCPEvent 'ETL - Load_DimItemGroup1', 'B'

--This table will contain a row for every row in the dimension table that has been changed in the staging table (type 1 update will be done)
CREATE TABLE #UpdateKeys (KeyID INTEGER)

DECLARE @RowsI Quantity_Normal_type, @RowsU Quantity_Normal_type, @RowsD Quantity_Normal_type
SET @RowsI = 0
SET @RowsU = 0
SET @RowsD = 0

EXEC DBA_IndexRebuild 'DimItemGroup1', 'Drop'


--TRUNCATE TABLE DimItemGroup1

--This is used to look up the date key in the DimDate table
DECLARE @TodayDayKey Key_Small_Type
SET @TodayDayKey = (SELECT DayKey FROM DimDay WHERE CONVERT(VARCHAR(10), SQLDate, 120) = CONVERT(VARCHAR(10), GETDATE(), 120))

IF NOT EXISTS (SELECT 1 FROM DimItemGroup1 WHERE IG1Level1 = 'Unknown')
BEGIN
	INSERT DimItemGroup1 (
		IG1Level1, 
		IG1Level2, 
		IG1Level3, 
		IG1Level4, 
		IG1Level5, 
		IG1Level1UDVarchar1, 
		CreationDayKey, 
		ModificationDayKey)
	VALUES (
		'Unknown',
		'Unknown',
		'Unknown',
		'Unknown',
		'Unknown',
		'Unknown',
		@TodayDayKey,
		@TodayDayKey)
	SET @RowsI = @RowsI + @@RowCount
END

; WITH Staging AS (
	SELECT DISTINCT
		p.PYRCode,
		p.MPGCode,
		p.MPGDescription,
		ROW_NUMBER() OVER (PARTITION BY p.PYRCode, p.MPGCode ORDER BY p.MPGDescription ASC) AS RowRank
	FROM
		Lion_Staging.dbo.Product p
	WHERE 
		ETLRowStatusCodeKey = 0
)
INSERT DimItemGroup1 (
	IG1Level1,
	IG1Level2,
	IG1Level3,
	IG1Level4,
	IG1Level5, 
	IG1Level1UDVarchar1,
	CreationDayKey,
	ModificationDayKey)
SELECT DISTINCT
	MPGCode,
	PYRCode,
	'Unknown',
	'Unknown',
	'Unknown',
	MPGDescription AS ProductTypeDescription,
	@TodayDayKey,
	@TodayDayKey
FROM Staging s
WHERE 
	s.RowRank = 1
	AND NOT EXISTS (SELECT 1 FROM dbo.DimItemGroup1 dig1 WHERE dig1.IG1Level1 = s.MPGCode AND dig1.IG1Level2 = s.PYRCode)
ORDER BY
	PYRCode
SET @RowsI = @RowsI + @@RowCount

EXEC DBA_IndexRebuild 'DimItemGroup1', 'Create'

EXEC LogDCPEvent 'ETL - Load_DimItemGroup1', 'E', @RowsI, @RowsU, @RowsD

SELECT TOP 10 * FROM DimItemGroup1

GO
