SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE  PROCEDURE [lion].[Load_DimAccountBrandSegment]
WITH EXECUTE AS CALLER
AS


EXEC LogDCPEvent 'ETL - DimAccountBrandSegment', 'B'

EXEC dba_indexrebuild 'DimAccountBrandSegment', 'Drop'

TRUNCATE TABLE DBO.DimAccountBrandSegment


DECLARE @RowsI INT, @RowsU INT, @RowsD INT
SET @RowsI = 0
SET @RowsU = 0
SET @RowsD = 0

DECLARE @TodayDayKey Key_Small_type
SET @TodayDayKey = (SELECT DayKey FROM DimDay WHERE CONVERT(VARCHAR(10), SQLDate, 120) = CONVERT(VARCHAR(10), GETDATE(), 120))



INSERT dbo.DimAccountBrandSegment
(
	AccountKey,
	BrandKey,
	Segment,
	CreationDayKey,
	ModificationDayKey
)
SELECT DISTINCT 
	AccountKey,
	db.BrandKey,
	ISNULL(Segment, N''),
	@TodayDayKey,
	@TodayDayKey
FROM Lion_Staging.dbo.CustomerBrandSegment cbs
INNER JOIN dbo.DimAccount da 
	ON da.AccountClientUniqueIdentifier = cbs.AccountId
INNER JOIN dbo.DimBrand db 
	ON db.SegmentBrand = cbs.SegmentBrand
WHERE NOT EXISTS (SELECT 1 FROM DimAccountBrandSegment b where b.AccountKey = da.AccountKey and b.BrandKey = db.BrandKey)
AND ISNULL(Segment, N'') <> N''
SET @RowsI = @RowsI + @@RowCount


SELECT TOP 10 * FROM DimAccountBrandSegment

EXEC dba_indexrebuild 'DimAccountBrandSegment', 'Create'

EXEC LogDCPEvent 'ETL - DimAccountBrandSegment', 'E', @RowsI, @RowsU, @RowsD






GO
