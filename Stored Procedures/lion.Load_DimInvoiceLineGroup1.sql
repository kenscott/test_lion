SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE PROCEDURE [lion].[Load_DimInvoiceLineGroup1] 
AS

EXEC LogDCPEvent 'ETL - Load_DimInvoiceLineGroup1', 'B'

--This table will contain a row for every row in the dimension table that has been changed in the staging table (type 1 update will be done)
CREATE TABLE #UpdateKeys (KeyID INTEGER)

DECLARE @RowsI Quantity_Normal_type, @RowsU Quantity_Normal_type, @RowsD Quantity_Normal_type
SET @RowsI = 0
SET @RowsU = 0
SET @RowsD = 0

DECLARE @TodayDayKey Key_Small_Type

SET @TodayDayKey = (SELECT DayKey FROM DimDay WHERE CONVERT(VARCHAR(10), SQLDate, 120) = CONVERT(VARCHAR(10), GETDATE(), 120))

--This finds every combination of ShipmentType, ConType, and LineType, assigning a Key value to each combination.  
--There are a limited # of combinations so this allows all three to be stored AS one key value in the InvoiceLine table (Saving space and processing time)

--EXEC DBA_IndexRebuild 'DimInvoiceLineGroup1', 'Drop'

--Truncate table DimInvoiceLineGroup1



IF NOT EXISTS (SELECT 1 FROM DimInvoiceLineGroup1 WHERE InvoiceLineGroup1UDVarchar1 = 'Unknown')
BEGIN
	EXEC DBA_IndexRebuild 'DimInvoiceLineGroup1', 'Drop'

	TRUNCATE TABLE DimInvoiceLineGroup1

	SET IDENTITY_INSERT [dbo].[DimInvoiceLineGroup1] ON
	
	INSERT DimInvoiceLineGroup1 (InvoiceLineGroup1Key, InvoiceLineGroup1UDVarchar1, InvoiceLineGroup1UDVarchar2, InvoiceLineGroup1UDVarchar3, InvoiceLineGroup1UDVarchar4, InvoiceLineGroup1UDVarchar5, ModificationDayKey, CreationDayKey)
	SELECT InvoiceLineGroup1Key, InvoiceLineGroup1UDVarchar1, InvoiceLineGroup1UDVarchar2, InvoiceLineGroup1UDVarchar3, InvoiceLineGroup1UDVarchar4, InvoiceLineGroup1UDVarchar5, @TodayDayKey, @TodayDayKey
	FROM (
		SELECT '1' AS InvoiceLineGroup1Key, 'Unknown' AS InvoiceLineGroup1UDVarchar1, 'Unknown' AS InvoiceLineGroup1UDVarchar2, 'Unknown' AS InvoiceLineGroup1UDVarchar3, 'Unknown' AS InvoiceLineGroup1UDVarchar4, 'Unknown' AS InvoiceLineGroup1UDVarchar5
		union select '2', 'C', 'N', 'None', 'None', 'None'
		union select '3', 'D', 'N', 'None', 'None', 'None'
		union select '4', 'S', 'N', 'None', 'None', 'None'
		union select '5', 'C', 'Y', 'None', 'None', 'None'
		union select '6', 'D', 'Y', 'None', 'None', 'None'
		union select '7', 'S', 'Y', 'None', 'None', 'None') a
	SET @RowsI = @RowsI + @@RowCount

	SET IDENTITY_INSERT [dbo].[DimInvoiceLineGroup1] OFF
	
	EXEC DBA_IndexRebuild 'DimInvoiceLineGroup1', 'Create'

END

--IF NOT EXISTS (SELECT 1 FROM DimInvoiceLineGroup1 WHERE InvoiceLineGroup1UDVarchar1 = 'Unknown')
--BEGIN
--	--INSERT DimInvoiceLineGroup1 (
--	--	InvoiceLineGroup1UDVarchar1,
--	--	InvoiceLineGroup1UDVarchar2,
--	--	InvoiceLineGroup1UDVarchar3,
--	--	InvoiceLineGroup1UDVarchar4,
--	--	InvoiceLineGroup1UDVarchar5,
--	--	CreationDayKey,
--	--	ModificationDayKey)
--	--VALUES (
--	--	'Unknown', 
--	--	'Unknown',
--	--	'Unknown',
--	--	'Unknown',
--	--	'Unknown',
--	--	@TodayDayKey,
--	--	@TodayDayKey
--	--)
--	--SET @RowsI = @RowsI + @@RowCount
	
--	SET IDENTITY_INSERT [dbo].[DimInvoiceLineGroup1] ON
--	INSERT INTO [dbo].[DimInvoiceLineGroup1] ([InvoiceLineGroup1Key], [InvoiceLineGroup1UDVarchar1], [InvoiceLineGroup1UDVarchar2], [InvoiceLineGroup1UDVarchar3], [InvoiceLineGroup1UDVarchar4], [InvoiceLineGroup1UDVarchar5], [CreationDayKey], [ModificationDayKey]) VALUES (1, 'Unknown', 'Unknown', 'Unknown', 'Unknown', 'Unknown', @TodayDayKey, @TodayDayKey)
--	SET @RowsI = @RowsI + @@RowCount
--	INSERT INTO [dbo].[DimInvoiceLineGroup1] ([InvoiceLineGroup1Key], [InvoiceLineGroup1UDVarchar1], [InvoiceLineGroup1UDVarchar2], [InvoiceLineGroup1UDVarchar3], [InvoiceLineGroup1UDVarchar4], [InvoiceLineGroup1UDVarchar5], [CreationDayKey], [ModificationDayKey]) VALUES (2, 'N', 'Indirect', 'None', 'None', 'None', @TodayDayKey, @TodayDayKey)
--	SET @RowsI = @RowsI + @@RowCount
--	INSERT INTO [dbo].[DimInvoiceLineGroup1] ([InvoiceLineGroup1Key], [InvoiceLineGroup1UDVarchar1], [InvoiceLineGroup1UDVarchar2], [InvoiceLineGroup1UDVarchar3], [InvoiceLineGroup1UDVarchar4], [InvoiceLineGroup1UDVarchar5], [CreationDayKey], [ModificationDayKey]) VALUES (3, 'D', 'Direct', 'None', 'None', 'None', @TodayDayKey, @TodayDayKey)
--	SET @RowsI = @RowsI + @@RowCount
--	INSERT INTO [dbo].[DimInvoiceLineGroup1] ([InvoiceLineGroup1Key], [InvoiceLineGroup1UDVarchar1], [InvoiceLineGroup1UDVarchar2], [InvoiceLineGroup1UDVarchar3], [InvoiceLineGroup1UDVarchar4], [InvoiceLineGroup1UDVarchar5], [CreationDayKey], [ModificationDayKey]) VALUES (4, 'S', 'Stock', 'None', 'None', 'None', @TodayDayKey, @TodayDayKey)
--	SET @RowsI = @RowsI + @@RowCount
--	SET IDENTITY_INSERT [dbo].[DimInvoiceLineGroup1] OFF
	
--END

--EXEC DBA_IndexRebuild 'DimInvoiceLineGroup1', 'Create'

SELECT TOP 10 * FROM DimInvoiceLineGroup1


EXEC LogDCPEvent 'ETL - Load_DimInvoiceLineGroup1', 'E', @RowsI, @RowsU, @RowsD




GO
