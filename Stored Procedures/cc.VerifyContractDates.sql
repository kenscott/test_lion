SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE [cc].[VerifyContractDates](@WebUserKey INT, @ContractKey INT, @DEBUG BIT=0 )
/*
Purpose: Makes sure that any start and end dates entered for a specific account, item, item group or location conform to the actual 
	     contract start and end dates (on the header itself).  The following table shows the actions taken:
	     
	     Condition						Start Date					Expiration Date
		 before contract start			set to contract start		set to contract start
		 equal contract start			do nothing					do nothing
		 after contract start			do nothing					do nothing
		 before contract expiration		do nothing					do nothing
		 equal contract expiration		do nothing					do nothing
		 after contract expiration		set to contract expiration	set to contract expiration

		 NOTE: The above assumes all start dates are before expiration dates!  In fact, if the contract start date is after the
		 contract expiration date, the procedure will return an error (-1).

JIRA: LPF-3539 - Create a stored procedure that check and correct contract related associations start and expiry dates (accounts, locations, products, spgs, mpgs)
	
Application: The application calls this procedure after saving a contract to make sure all dates are ok on the contract.

Outputs: -1 error
		  0 no rows updated
		  >0 number of rows updated

Examples:
		exec cc.verifycontractdates 1965, 675744, 1
		
		select * from cc.suppliercontractstatus
		
		update cc.suppliercontractaccount set startdate='2015-01-01', expirationdate='2015-12-31' where contractaccountkey = 2708932
		update cc.suppliercontractitem set startdt='2017-12-31', expirydt='2018-02-01' where contractitemkey = 10400312
		
		select * from cc.suppliercontractitem where contractkey = 675744
		
		select top 100 contractkey, startdate, expirationdate, * from cc.suppliercontract where statuskey = 2
*/

AS 

SET NOCOUNT ON

DECLARE @ContractStartDate DATE
DECLARE @ContractExpirationDate DATE

DECLARE @UpdatedAccountCount INT
DECLARE @UpdatedItemCount INT
DECLARE @UpdatedItemGroupXCount INT
DECLARE @UpdatedLocationCount INT

/*
	Get the contract start dates
*/
SELECT @ContractStartDate=sc.StartDate, @ContractExpirationDate=sc.ExpirationDate FROM SupplierContract sc WHERE ContractKey = @ContractKey

IF(@ContractStartDate IS NULL OR @ContractExpirationDate IS NULL OR @ContractStartDate > @ContractExpirationDate)
BEGIN
	SELECT -1 AS Result
END

ELSE

BEGIN

	IF( @DEBUG = 1 )
	BEGIN
		SELECT @ContractStartDate AS ContractStart, @ContractExpirationDate as ContractExpiration
	END

	IF( @DEBUG = 1 )
	BEGIN
		SELECT * FROM SupplierContractAccount WHERE ContractKey = @ContractKey
	END

	/* Update the start and end dates in SupplierContractAccount */
	UPDATE SupplierContractAccount SET
		StartDate = 
			CASE
				WHEN StartDate IS NOT NULL AND StartDate < @ContractStartDate THEN @ContractStartDate
				WHEN StartDate IS NOT NULL AND StartDate > @ContractExpirationDate THEN @ContractExpirationDate
				ELSE StartDate 
			END,
		ExpirationDate = 
			CASE
				WHEN ExpirationDate IS NOT NULL AND ExpirationDate < @ContractStartDate THEN @ContractStartDate
				WHEN ExpirationDate IS NOT NULL AND ExpirationDate > @ContractExpirationDate THEN @ContractExpirationDate
				ELSE ExpirationDate
			END,
		ModifiedByWebUserKey = @WebUserKey,
		ModifiedOnDT = GETDATE()
	WHERE ContractKey = @ContractKey AND 
		(( StartDate IS NOT NULL AND StartDate < @ContractStartDate ) OR
		 ( StartDate IS NOT NULL AND StartDate > @ContractExpirationDate ) OR
		 ( ExpirationDate IS NOT NULL AND ExpirationDate < @ContractStartDate ) OR
		 ( ExpirationDate IS NOT NULL AND ExpirationDate > @ContractExpirationDate ))

	SELECT @UpdatedAccountCount = @@ROWCOUNT

	IF( @DEBUG = 1 )
	BEGIN
		SELECT * FROM SupplierContractAccount WHERE ContractKey = @ContractKey
	END

	/* Update the start and end dates in SupplierContractLocation */
	UPDATE SupplierContractLocation SET
		StartDate = 
			CASE
				WHEN StartDate IS NOT NULL AND StartDate < @ContractStartDate THEN @ContractStartDate
				WHEN StartDate IS NOT NULL AND StartDate > @ContractExpirationDate THEN @ContractExpirationDate
				ELSE StartDate 
			END,
		ExpirationDate = 
			CASE
				WHEN ExpirationDate IS NOT NULL AND ExpirationDate < @ContractStartDate THEN @ContractStartDate
				WHEN ExpirationDate IS NOT NULL AND ExpirationDate > @ContractExpirationDate THEN @ContractExpirationDate
				ELSE ExpirationDate
			END,
		ModifiedByWebUserKey = @WebUserKey,
		ModifiedOnDT = GETDATE()
	WHERE ContractKey = @ContractKey AND 
		(( StartDate IS NOT NULL AND StartDate < @ContractStartDate ) OR
		 ( StartDate IS NOT NULL AND StartDate > @ContractExpirationDate ) OR
		 ( ExpirationDate IS NOT NULL AND ExpirationDate < @ContractStartDate ) OR
		 ( ExpirationDate IS NOT NULL AND ExpirationDate > @ContractExpirationDate ))

	SELECT @UpdatedLocationCount = @@ROWCOUNT

	/* Update the start and end dates in SupplierContractItem */
	IF( @DEBUG = 1 )
	BEGIN
		SELECT * FROM SupplierContractItem WHERE ContractKey = @ContractKey
	END

	UPDATE SupplierContractItem SET
		StartDT = 
			CASE
				WHEN StartDT IS NOT NULL AND CAST(StartDT as DATE) < @ContractStartDate THEN @ContractStartDate
				WHEN StartDT IS NOT NULL AND CAST(StartDT as DATE) > @ContractExpirationDate THEN @ContractExpirationDate
				ELSE StartDT 
			END,
		ExpiryDT = 
			CASE
				WHEN ExpiryDT IS NOT NULL AND CAST(ExpiryDT as DATE) < @ContractStartDate THEN @ContractStartDate
				WHEN ExpiryDT IS NOT NULL AND CAST(ExpiryDT as DATE) > @ContractExpirationDate THEN @ContractExpirationDate
				ELSE ExpiryDT
			END,
		ModifiedByWebUserKey = @WebUserKey,
		ModifiedOnDT = GETDATE()
	WHERE ContractKey = @ContractKey AND
		  ( ( StartDT IS NOT NULL AND CAST(StartDT as DATE) < @ContractStartDate ) OR
		    ( StartDT IS NOT NULL AND CAST(StartDT as DATE) > @ContractExpirationDate ) OR 
		    ( ExpiryDT IS NOT NULL AND CAST(ExpiryDT as DATE) < @ContractStartDate ) OR
		    ( ExpiryDT IS NOT NULL AND CAST(ExpiryDT as DATE) > @ContractExpirationDate ) )
	
	SELECT @UpdatedItemCount = @@ROWCOUNT
	
	IF( @DEBUG = 1 )
	BEGIN
		SELECT * FROM SupplierContractItem WHERE ContractKey = @ContractKey
	END
	
	/* Update the start and expiry dates in SupplierContractItemGroupX */
	UPDATE SupplierContractItemGroupX SET
		StartDT = 
			CASE
				WHEN StartDT IS NOT NULL AND CAST(StartDT as DATE) < @ContractStartDate THEN @ContractStartDate
				WHEN StartDT IS NOT NULL AND CAST(StartDT as DATE) > @ContractExpirationDate THEN @ContractExpirationDate
				ELSE StartDT 
			END,
		ExpiryDT = 
			CASE
				WHEN ExpiryDT IS NOT NULL AND CAST(ExpiryDT as DATE) < @ContractStartDate THEN @ContractStartDate
				WHEN ExpiryDT IS NOT NULL AND CAST(ExpiryDT as DATE) > @ContractExpirationDate THEN @ContractExpirationDate
				ELSE ExpiryDT
			END,
		ModifiedByWebUserKey = @WebUserKey,
		ModifiedOnDT = GETDATE()
	WHERE ContractKey = @ContractKey AND
		  ( ( StartDT IS NOT NULL AND CAST(StartDT as DATE) < @ContractStartDate ) OR
		    ( StartDT IS NOT NULL AND CAST(StartDT as DATE) > @ContractExpirationDate ) OR 
		    ( ExpiryDT IS NOT NULL AND CAST(ExpiryDT as DATE) < @ContractStartDate ) OR
		    ( ExpiryDT IS NOT NULL AND CAST(ExpiryDT as DATE) > @ContractExpirationDate ) )
		  
    SELECT @UpdatedItemGroupXCount = @@ROWCOUNT

	SELECT @UpdatedAccountCount + @UpdatedItemCount + @UpdatedItemGroupXCount + @UpdatedLocationCount AS RESULT

END
GO
