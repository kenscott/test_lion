SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[DBA_FindString] (
	@chvStringToFind NVARCHAR(256), /* String to find */
	@chrObjectType CHAR(2) = NULL, /* Only look for objects of this type */
	@intNbCharToExtract INT = 50 /* Number of characters to extract before and after the string to find */
)

/*

Source: http://www.sqlservercentral.com/scripts/String+Match/70371/ 
Author: Laurent Michaud, 2010/06/21

EXEC dbo.DBA_FindString 'accountkey'

*/

AS 

SET NOCOUNT ON


SELECT
	DISTINCT
	TypeDescription,
	SchemaName,
	Name,
	'...' + SUBSTRING(t.ObjectDefinition,
	CHARINDEX(@chvStringToFind,
	t.ObjectDefinition)
	- @intNbCharToExtract,
	LEN(@chvStringToFind)
	+ ( @intNbCharToExtract * 2 )) + '...' AS Extract,
	CreationDate,
	ModificationDate	
FROM (
	SELECT DISTINCT
		o.name AS Name,
		SCHEMA_NAME (o.schema_id) AS SchemaName,
		o.type_desc AS TypeDescription,
		o.create_date AS CreationDate,
		o.modify_date AS ModificationDate,
		ISNULL(OBJECT_DEFINITION(object_id), c.name) AS ObjectDefinition
	FROM sys.objects o WITH (NOLOCK)
	LEFT OUTER JOIN syscolumns c
		ON c.id = o.object_id
	WHERE  
		--( 
		--	( o.type IN ( 'AF', 'FN', 'IF', 'P', 'TF', 'TT', 'U', 'V', 'X' )
		--	AND @chrObjectType IS NULL
		--	)
		--	OR o.type = @chrObjectType
		--)
		(o.type = @chrObjectType OR @chrObjectType IS NULL)
		AND (OBJECT_DEFINITION(o.object_id) LIKE '%' + @chvStringToFind + '%'
			OR
			c.name LIKE '%' + @chvStringToFind + '%'
			)
	) AS t
ORDER BY 
	1,
	2,
	3,
	4,
	5,
	6

GO
