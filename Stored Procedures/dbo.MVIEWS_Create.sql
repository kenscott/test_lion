SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO













CREATE                               PROCEDURE [dbo].[MVIEWS_Create]
AS
--MVIEWS_Drop
EXEC LogDCPEvent 'ETL - dbo.MVIEWS_Create', 'B'

--DECLARE @Cmd NVARCHAR(4000)

--SET @cmd = '
--CREATE VIEW dbo.FactInvoiceLine_Summary1
--WITH SCHEMABINDING
--AS
--SELECT 
----InvoiceLineKey, 
--AccountKey, 
--ItemKey, 
--VendorKey, 
--AccountManagerKey,
----ManagedByUserKey,
----InvoiceDateDayKey, 
--InvoiceDateMonthKey, 
----ClientUniqueIdentifier, 
----InvoiceLineJunkKey, 
--InvoiceLineGroup1Key, 
--InvoiceLineGroup2Key, 
--InvoiceLineGroup3Key, 
--AccountGroup1Key,
--AccountGroup2Key,
--AccountGroup3Key,
--ItemGroup1Key,
--ItemGroup2Key,
--ItemGroup3Key,
--Last12MonthsIndicator,
--Sum(TotalQuantity) TotalQuantity, 
--Sum(TotalActualPrice) TotalActualPrice, 
--Sum(TotalActualCost) TotalActualCost, 
--Sum(TotalActualRebate) TotalActualRebate,
--Count_Big(*) CountBig
--FROM dbo.FactInvoiceLine
--Group By
----InvoiceLineKey, 
--AccountKey, 
--ItemKey, 
--VendorKey, 
--AccountManagerKey,
----ManagedByUserKey,
----InvoiceDateDayKey, 
--InvoiceDateMonthKey, 
----ClientUniqueIdentifier, 
----InvoiceLineJunkKey, 
--InvoiceLineGroup1Key, 
--InvoiceLineGroup2Key, 
--InvoiceLineGroup3Key,
--AccountGroup1Key,
--AccountGroup2Key,
--AccountGroup3Key,
--ItemGroup1Key,
--ItemGroup2Key,
--ItemGroup3Key,
--Last12MonthsIndicator'

--EXEC sp_executesql @cmd

--CREATE UNIQUE CLUSTERED INDEX UI_FactInvoiceLine_Summary1 ON FactInvoiceLine_Summary1 
--(
----InvoiceLineKey, 
--AccountKey, 
--ItemKey,
--VendorKey,
--AccountManagerKey,
----ManagedByUserKey,
----InvoiceDateDayKey, 
--InvoiceDateMonthKey,
----ClientUniqueIdentifier, 
----InvoiceLineJunkKey, 
--InvoiceLineGroup1Key,
--InvoiceLineGroup2Key, 
--InvoiceLineGroup3Key,
--AccountGroup1Key,
--AccountGroup2Key,
--AccountGroup3Key,
--ItemGroup1Key,
--ItemGroup2Key,
--ItemGroup3Key,
--Last12MonthsIndicator
--) ON [SECONDARY]




--CREATE INDEX I_FactInvoiceLine_Summary1_1 ON FactInvoiceLine_Summary1(AccountKey) ON [SECONDARY]
--CREATE INDEX I_FactInvoiceLine_Summary1_2 ON FactInvoiceLine_Summary1(ItemKey) ON [SECONDARY]
--CREATE INDEX I_FactInvoiceLine_Summary1_13 ON FactInvoiceLine_Summary1(VendorKey) ON [SECONDARY]
--CREATE INDEX I_FactInvoiceLine_Summary1_14 ON FactInvoiceLine_Summary1(AccountManagerKey) ON [SECONDARY]
----Create Index I_FactInvoiceLine_Summary1_15 on FactInvoiceLine_Summary1(ManagedByUserKey) ON [SECONDARY]
--CREATE INDEX I_FactInvoiceLine_Summary1_3 ON FactInvoiceLine_Summary1(InvoiceDateMonthKey) ON [SECONDARY]
--CREATE INDEX I_FactInvoiceLine_Summary1_4 ON FactInvoiceLine_Summary1(InvoiceLineGroup1Key) ON [SECONDARY]
--CREATE INDEX I_FactInvoiceLine_Summary1_5 ON FactInvoiceLine_Summary1(InvoiceLineGroup2Key) ON [SECONDARY]
--CREATE INDEX I_FactInvoiceLine_Summary1_6 ON FactInvoiceLine_Summary1(InvoiceLineGroup3Key) ON [SECONDARY]
--CREATE INDEX I_FactInvoiceLine_Summary1_7 ON FactInvoiceLine_Summary1(AccountGroup1Key) ON [SECONDARY]
--CREATE INDEX I_FactInvoiceLine_Summary1_8 ON FactInvoiceLine_Summary1(AccountGroup2Key) ON [SECONDARY]
--CREATE INDEX I_FactInvoiceLine_Summary1_9 ON FactInvoiceLine_Summary1(AccountGroup3Key) ON [SECONDARY]
--CREATE INDEX I_FactInvoiceLine_Summary1_10 ON FactInvoiceLine_Summary1(ItemGroup1Key) ON [SECONDARY]
--CREATE INDEX I_FactInvoiceLine_Summary1_11 ON FactInvoiceLine_Summary1(ItemGroup2Key) ON [SECONDARY]
--CREATE INDEX I_FactInvoiceLine_Summary1_12 ON FactInvoiceLine_Summary1(ItemGroup3Key) ON [SECONDARY]

--CREATE INDEX I_FactInvoiceLine_Summary1_20 ON FactInvoiceLine_Summary1(TotalQuantity) ON [SECONDARY]
--CREATE INDEX I_FactInvoiceLine_Summary1_21 ON FactInvoiceLine_Summary1(TotalActualPrice) ON [SECONDARY]
--CREATE INDEX I_FactInvoiceLine_Summary1_22 ON FactInvoiceLine_Summary1(TotalActualCost) ON [SECONDARY]
--CREATE INDEX I_FactInvoiceLine_Summary1_23 ON FactInvoiceLine_Summary1(TotalActualRebate) ON [SECONDARY]


--SELECT COUNT(*) FactInvoiceLine_Summary1 FROM FactInvoiceLine_Summary1
-------------------------------------------------------------------------------------------------------------

--SET @cmd = '
--CREATE VIEW dbo.FactInvoiceLine_Summary2
--WITH SCHEMABINDING
--AS
--SELECT 
--AccountKey, 
--ItemKey, 
--ItemGroup1Key,
--Last12MonthsIndicator,
--Sum(TotalQuantity) TotalQuantity, 
--Sum(TotalActualPrice) TotalActualPrice, 
--Sum(TotalActualCost) TotalActualCost, 
--Count_Big(*) CountBig
--FROM dbo.FactInvoiceLine
--Group By
--AccountKey, 
--ItemKey, 
--ItemGroup1Key,
--Last12MonthsIndicator'

--EXEC sp_executesql @cmd

--CREATE UNIQUE CLUSTERED INDEX UI_FactInvoiceLine_Summary2 ON FactInvoiceLine_Summary2
--(
--AccountKey, 
--ItemKey,
--ItemGroup1Key,
--Last12MonthsIndicator) ON [SECONDARY]


--CREATE INDEX I_FactInvoiceLine_Summary2_2 ON FactInvoiceLine_Summary2(ItemKey) ON [SECONDARY]
--CREATE INDEX I_FactInvoiceLine_Summary2_10 ON FactInvoiceLine_Summary2(ItemGroup1Key) ON [SECONDARY]

--CREATE INDEX I_FactInvoiceLine_Summary2_20 ON FactInvoiceLine_Summary2(TotalQuantity) ON [SECONDARY]
--CREATE INDEX I_FactInvoiceLine_Summary2_21 ON FactInvoiceLine_Summary2(TotalActualPrice) ON [SECONDARY]
--CREATE INDEX I_FactInvoiceLine_Summary2_22 ON FactInvoiceLine_Summary2(TotalActualCost) ON [SECONDARY]

--SELECT COUNT(*) FactInvoiceLine_Summary2 FROM FactInvoiceLine_Summary2
-------------------------------------------------------------------------------------------------------------

--SET @cmd = '
--CREATE VIEW dbo.FactInvoiceLine_Summary3
--WITH SCHEMABINDING
--AS
--SELECT 
--AccountKey, 
--ItemKey, 
--VendorKey, 
--AccountManagerKey,
----ManagedByUserKey,
--InvoiceLineGroup1Key, 
--InvoiceLineGroup2Key, 
--InvoiceLineGroup3Key, 
--AccountGroup1Key,
--AccountGroup2Key,
--AccountGroup3Key,
--ItemGroup1Key,
--ItemGroup2Key,
--ItemGroup3Key,
--Last12MonthsIndicator,
--Sum(TotalQuantity) TotalQuantity, 
--Sum(TotalActualPrice) TotalActualPrice, 
--Sum(TotalActualCost) TotalActualCost, 
--Sum(TotalActualRebate) TotalActualRebate,
--Count_Big(*) CountBig
--FROM dbo.FactInvoiceLine
--Group By
--AccountKey, 
--ItemKey, 
--VendorKey, 
--AccountManagerKey,
----ManagedByUserKey,
--InvoiceLineGroup1Key, 
--InvoiceLineGroup2Key, 
--InvoiceLineGroup3Key,
--AccountGroup1Key,
--AccountGroup2Key,
--AccountGroup3Key,
--ItemGroup1Key,
--ItemGroup2Key,
--ItemGroup3Key,
--Last12MonthsIndicator'

--EXEC sp_executesql @cmd

--CREATE UNIQUE CLUSTERED INDEX UI_FactInvoiceLine_Summary3 ON FactInvoiceLine_Summary3
--(
--AccountKey, 
--ItemKey, 
--VendorKey, 
--AccountManagerKey,
----ManagedByUserKey,
--InvoiceLineGroup1Key, 
--InvoiceLineGroup2Key, 
--InvoiceLineGroup3Key,
--AccountGroup1Key,
--AccountGroup2Key,
--AccountGroup3Key,
--ItemGroup1Key,
--ItemGroup2Key,
--ItemGroup3Key,
--Last12MonthsIndicator) ON [SECONDARY]


--CREATE INDEX I_FactInvoiceLine_Summary3_1 ON FactInvoiceLine_Summary3(AccountKey) ON [SECONDARY]
--CREATE INDEX I_FactInvoiceLine_Summary3_2 ON FactInvoiceLine_Summary3(ItemKey) ON [SECONDARY]
--CREATE INDEX I_FactInvoiceLine_Summary3_13 ON FactInvoiceLine_Summary3(VendorKey) ON [SECONDARY]
--CREATE INDEX I_FactInvoiceLine_Summary3_14 ON FactInvoiceLine_Summary3(AccountManagerKey) ON [SECONDARY]
----Create Index I_FactInvoiceLine_Summary3_15 on FactInvoiceLine_Summary3(ManagedByUserKey) ON [SECONDARY]
--CREATE INDEX I_FactInvoiceLine_Summary3_4 ON FactInvoiceLine_Summary3(InvoiceLineGroup1Key) ON [SECONDARY]
--CREATE INDEX I_FactInvoiceLine_Summary3_5 ON FactInvoiceLine_Summary3(InvoiceLineGroup2Key) ON [SECONDARY]
--CREATE INDEX I_FactInvoiceLine_Summary3_6 ON FactInvoiceLine_Summary3(InvoiceLineGroup3Key) ON [SECONDARY]
--CREATE INDEX I_FactInvoiceLine_Summary3_7 ON FactInvoiceLine_Summary3(AccountGroup1Key) ON [SECONDARY]
--CREATE INDEX I_FactInvoiceLine_Summary3_8 ON FactInvoiceLine_Summary3(AccountGroup2Key) ON [SECONDARY]
--CREATE INDEX I_FactInvoiceLine_Summary3_9 ON FactInvoiceLine_Summary3(AccountGroup3Key) ON [SECONDARY]
--CREATE INDEX I_FactInvoiceLine_Summary3_10 ON FactInvoiceLine_Summary3(ItemGroup1Key) ON [SECONDARY]
--CREATE INDEX I_FactInvoiceLine_Summary3_11 ON FactInvoiceLine_Summary3(ItemGroup2Key) ON [SECONDARY]
--CREATE INDEX I_FactInvoiceLine_Summary3_12 ON FactInvoiceLine_Summary3(ItemGroup3Key) ON [SECONDARY]

--CREATE INDEX I_FactInvoiceLine_Summary3_20 ON FactInvoiceLine_Summary3(TotalQuantity) ON [SECONDARY]
--CREATE INDEX I_FactInvoiceLine_Summary3_21 ON FactInvoiceLine_Summary3(TotalActualPrice) ON [SECONDARY]
--CREATE INDEX I_FactInvoiceLine_Summary3_22 ON FactInvoiceLine_Summary3(TotalActualCost) ON [SECONDARY]
--CREATE INDEX I_FactInvoiceLine_Summary3_23 ON FactInvoiceLine_Summary3(TotalActualRebate) ON [SECONDARY]

--SELECT COUNT(*) FactInvoiceLine_Summary3 FROM FactInvoiceLine_Summary3
-------------------------------------------------------------------------------------------------------------

----go
--SET @cmd = '
--CREATE VIEW dbo.FactInvoiceLine_Summary_AccountGroupAll1
--WITH SCHEMABINDING
--AS
--SELECT 
--InvoiceDateMonthKey, 
--AccountGroup1Key,
--AccountGroup2Key,
--AccountGroup3Key,
--Last12MonthsIndicator,
--Sum(TotalQuantity) TotalQuantity, 
--Sum(TotalActualPrice) TotalActualPrice, 
--Sum(TotalActualCost) TotalActualCost, 
--Sum(TotalActualRebate) TotalActualRebate,
--Count_Big(*) CountBig
--FROM dbo.FactInvoiceLine
--Group By
--InvoiceDateMonthKey,
--AccountGroup1Key,
--AccountGroup2Key,
--AccountGroup3Key,
--Last12MonthsIndicator
--'

--EXEC sp_executesql @cmd

--CREATE UNIQUE CLUSTERED INDEX UI_FactInvoiceLine_Summary_AccountGroupAll1 ON FactInvoiceLine_Summary_AccountGroupAll1
--(
--InvoiceDateMonthKey,
--AccountGroup1Key,
--AccountGroup2Key,
--AccountGroup3Key,
--Last12MonthsIndicator
--) ON [SECONDARY]


--CREATE INDEX I_FactInvoiceLine_Summary_AccountGroupAll1_3 ON FactInvoiceLine_Summary_AccountGroupAll1(InvoiceDateMonthKey) ON [SECONDARY]
--CREATE INDEX I_FactInvoiceLine_Summary_AccountGroupAll1_4 ON FactInvoiceLine_Summary_AccountGroupAll1(AccountGroup1Key) ON [SECONDARY]
--CREATE INDEX I_FactInvoiceLine_Summary_AccountGroupAll1_5 ON FactInvoiceLine_Summary_AccountGroupAll1(AccountGroup2Key) ON [SECONDARY]
--CREATE INDEX I_FactInvoiceLine_Summary_AccountGroupAll1_6 ON FactInvoiceLine_Summary_AccountGroupAll1(AccountGroup3Key) ON [SECONDARY]

--CREATE INDEX I_FactInvoiceLine_Summary_AccountGroupAll1_20 ON FactInvoiceLine_Summary_AccountGroupAll1(TotalQuantity) ON [SECONDARY]
--CREATE INDEX I_FactInvoiceLine_Summary_AccountGroupAll1_21 ON FactInvoiceLine_Summary_AccountGroupAll1(TotalActualPrice) ON [SECONDARY]
--CREATE INDEX I_FactInvoiceLine_Summary_AccountGroupAll1_22 ON FactInvoiceLine_Summary_AccountGroupAll1(TotalActualCost) ON [SECONDARY]
--CREATE INDEX I_FactInvoiceLine_Summary_AccountGroupAll1_23 ON FactInvoiceLine_Summary_AccountGroupAll1(TotalActualRebate) ON [SECONDARY]

--SELECT COUNT(*) FactInvoiceLine_Summary_AccountGroupAll1 FROM FactInvoiceLine_Summary_AccountGroupAll1
-----------------------------------------------------------------------------------
----go
--SET @cmd = '
--CREATE VIEW dbo.FactInvoiceLine_Summary_ItemGroupAll1
--WITH SCHEMABINDING
--AS
--SELECT 
--InvoiceDateMonthKey, 
--ItemGroup1Key,
--ItemGroup2Key,
--ItemGroup3Key,
--Last12MonthsIndicator,
--Sum(TotalQuantity) TotalQuantity, 
--Sum(TotalActualPrice) TotalActualPrice, 
--Sum(TotalActualCost) TotalActualCost, 
--Sum(TotalActualRebate) TotalActualRebate,
--Count_Big(*) CountBig
--FROM dbo.FactInvoiceLine
--Group By
--InvoiceDateMonthKey,
--ItemGroup1Key,
--ItemGroup2Key,
--ItemGroup3Key,
--Last12MonthsIndicator
--'

--EXEC sp_executesql @cmd

--CREATE UNIQUE CLUSTERED INDEX UI_FactInvoiceLine_Summary_ItemGroupAll1 ON FactInvoiceLine_Summary_ItemGroupAll1
--(
--InvoiceDateMonthKey,
--ItemGroup1Key,
--ItemGroup2Key,
--ItemGroup3Key,
--Last12MonthsIndicator
--) ON [SECONDARY]


--CREATE INDEX I_FactInvoiceLine_Summary_ItemGroupAll1_3 ON FactInvoiceLine_Summary_ItemGroupAll1(InvoiceDateMonthKey) ON [SECONDARY]
--CREATE INDEX I_FactInvoiceLine_Summary_ItemGroupAll1_4 ON FactInvoiceLine_Summary_ItemGroupAll1(ItemGroup1Key) ON [SECONDARY]
--CREATE INDEX I_FactInvoiceLine_Summary_ItemGroupAll1_5 ON FactInvoiceLine_Summary_ItemGroupAll1(ItemGroup2Key) ON [SECONDARY]
--CREATE INDEX I_FactInvoiceLine_Summary_ItemGroupAll1_6 ON FactInvoiceLine_Summary_ItemGroupAll1(ItemGroup3Key) ON [SECONDARY]

--CREATE INDEX I_FactInvoiceLine_Summary_ItemGroupAll1_20 ON FactInvoiceLine_Summary_ItemGroupAll1(TotalQuantity) ON [SECONDARY]
--CREATE INDEX I_FactInvoiceLine_Summary_ItemGroupAll1_21 ON FactInvoiceLine_Summary_ItemGroupAll1(TotalActualPrice) ON [SECONDARY]
--CREATE INDEX I_FactInvoiceLine_Summary_ItemGroupAll1_22 ON FactInvoiceLine_Summary_ItemGroupAll1(TotalActualCost) ON [SECONDARY]
--CREATE INDEX I_FactInvoiceLine_Summary_ItemGroupAll1_23 ON FactInvoiceLine_Summary_ItemGroupAll1(TotalActualRebate) ON [SECONDARY]

--SELECT COUNT(*) FactInvoiceLine_Summary_ItemGroupAll1 FROM FactInvoiceLine_Summary_ItemGroupAll1
---------------------------------------------------------
----go
--SET @cmd = '
--CREATE VIEW dbo.FactInvoiceLine_Summary_InvoiceLineGroupAll1
--WITH SCHEMABINDING
--AS
--SELECT 
--InvoiceDateMonthKey, 
--InvoiceLineGroup1Key, 
--InvoiceLineGroup2Key, 
--InvoiceLineGroup3Key, 
--Last12MonthsIndicator,
--Sum(TotalQuantity) TotalQuantity, 
--Sum(TotalActualPrice) TotalActualPrice, 
--Sum(TotalActualCost) TotalActualCost, 
--Sum(TotalActualRebate) TotalActualRebate,
--Count_Big(*) CountBig
--FROM dbo.FactInvoiceLine
--Group By
--InvoiceDateMonthKey,
--InvoiceLineGroup1Key, 
--InvoiceLineGroup2Key, 
--InvoiceLineGroup3Key,
--Last12MonthsIndicator
--'
--EXEC sp_executesql @cmd


--CREATE UNIQUE CLUSTERED INDEX UI_FactInvoiceLine_Summary_InvoiceLineGroupAll1 ON FactInvoiceLine_Summary_InvoiceLineGroupAll1
--(
--InvoiceDateMonthKey,
--InvoiceLineGroup1Key, 
--InvoiceLineGroup2Key, 
--InvoiceLineGroup3Key,
--Last12MonthsIndicator
--) ON [SECONDARY]


--CREATE INDEX I_FactInvoiceLine_Summary_InvoiceLineGroupAll1_1 ON FactInvoiceLine_Summary_InvoiceLineGroupAll1(InvoiceLineGroup1Key) ON [SECONDARY]
--CREATE INDEX I_FactInvoiceLine_Summary_InvoiceLineGroupAll1_2 ON FactInvoiceLine_Summary_InvoiceLineGroupAll1(InvoiceLineGroup2Key) ON [SECONDARY]
--CREATE INDEX I_FactInvoiceLine_Summary_InvoiceLineGroupAll1_3 ON FactInvoiceLine_Summary_InvoiceLineGroupAll1(InvoiceLineGroup3Key) ON [SECONDARY]

--CREATE INDEX I_FactInvoiceLine_Summary_InvoiceLineGroupAll1_20 ON FactInvoiceLine_Summary_InvoiceLineGroupAll1(TotalQuantity) ON [SECONDARY]
--CREATE INDEX I_FactInvoiceLine_Summary_InvoiceLineGroupAll1_21 ON FactInvoiceLine_Summary_InvoiceLineGroupAll1(TotalActualPrice) ON [SECONDARY]
--CREATE INDEX I_FactInvoiceLine_Summary_InvoiceLineGroupAll1_22 ON FactInvoiceLine_Summary_InvoiceLineGroupAll1(TotalActualCost) ON [SECONDARY]
--CREATE INDEX I_FactInvoiceLine_Summary_InvoiceLineGroupAll1_23 ON FactInvoiceLine_Summary_InvoiceLineGroupAll1(TotalActualRebate) ON [SECONDARY]

--SELECT COUNT(*) FactInvoiceLine_Summary_InvoiceLineGroupAll1 FROM FactInvoiceLine_Summary_InvoiceLineGroupAll1
---------------------------------------------------------
----go
--SET @cmd = '
--CREATE VIEW dbo.FactInvoiceLine_Summary_Item1
--WITH SCHEMABINDING
--AS
--SELECT 
--ItemKey,
--VendorKey,
--InvoiceDateMonthKey, 
--ItemGroup1Key,
--ItemGroup2Key,
--ItemGroup3Key,
--Last12MonthsIndicator,
--Sum(TotalQuantity) TotalQuantity, 
--Sum(TotalActualPrice) TotalActualPrice, 
--Sum(TotalActualCost) TotalActualCost, 
--Sum(TotalActualRebate) TotalActualRebate,
--Count_Big(*) CountBig
--FROM dbo.FactInvoiceLine
--Group By
--ItemKey,
--VendorKey,
--InvoiceDateMonthKey,
--ItemGroup1Key,
--ItemGroup2Key,
--ItemGroup3Key,
--Last12MonthsIndicator
--'

--EXEC sp_executesql @cmd

--CREATE UNIQUE CLUSTERED INDEX UI_FactInvoiceLine_Summary_Item1 ON FactInvoiceLine_Summary_Item1
--(
--ItemKey,
--VendorKey,
--InvoiceDateMonthKey,
--ItemGroup1Key,
--ItemGroup2Key,
--ItemGroup3Key,
--Last12MonthsIndicator
--) ON [SECONDARY]


--CREATE INDEX I_FactInvoiceLine_Summary_Item1_1 ON FactInvoiceLine_Summary_Item1(ItemKey) ON [SECONDARY]
--CREATE INDEX I_FactInvoiceLine_Summary_Item1_3 ON FactInvoiceLine_Summary_Item1(VendorKey) ON [SECONDARY]
--CREATE INDEX I_FactInvoiceLine_Summary_Item1_2 ON FactInvoiceLine_Summary_Item1(InvoiceDateMonthKey) ON [SECONDARY]
--CREATE INDEX I_FactInvoiceLine_Summary_Item1_4 ON FactInvoiceLine_Summary_Item1(ItemGroup1Key) ON [SECONDARY]
--CREATE INDEX I_FactInvoiceLine_Summary_Item1_5 ON FactInvoiceLine_Summary_Item1(ItemGroup2Key) ON [SECONDARY]
--CREATE INDEX I_FactInvoiceLine_Summary_Item1_6 ON FactInvoiceLine_Summary_Item1(ItemGroup3Key) ON [SECONDARY]

--CREATE INDEX I_FactInvoiceLine_Summary_Item1_20 ON FactInvoiceLine_Summary_Item1(TotalQuantity) ON [SECONDARY]
--CREATE INDEX I_FactInvoiceLine_Summary_Item1_21 ON FactInvoiceLine_Summary_Item1(TotalActualPrice) ON [SECONDARY]
--CREATE INDEX I_FactInvoiceLine_Summary_Item1_22 ON FactInvoiceLine_Summary_Item1(TotalActualCost) ON [SECONDARY]
--CREATE INDEX I_FactInvoiceLine_Summary_Item1_23 ON FactInvoiceLine_Summary_Item1(TotalActualRebate) ON [SECONDARY]

--SELECT COUNT(*) FactInvoiceLine_Summary_Item1 FROM FactInvoiceLine_Summary_Item1
---------------------------------------------------------
----go
--SET @cmd = '
--CREATE VIEW dbo.FactInvoiceLine_Summary_Account1
--WITH SCHEMABINDING
--AS
--SELECT 
--AccountKey,
--AccountManagerKey,
----ManagedByUserKey,
--InvoiceDateMonthKey,
--AccountGroup1Key,
--AccountGroup2Key,
--AccountGroup3Key,
--Last12MonthsIndicator,
--Sum(TotalQuantity) TotalQuantity, 
--Sum(TotalActualPrice) TotalActualPrice, 
--Sum(TotalActualCost) TotalActualCost, 
--Sum(TotalActualRebate) TotalActualRebate,
--Count_Big(*) CountBig
--FROM dbo.FactInvoiceLine
--Group By
--AccountKey,
--AccountManagerKey,
----ManagedByUserKey,
--InvoiceDateMonthKey,
--AccountGroup1Key,
--AccountGroup2Key,
--AccountGroup3Key,
--Last12MonthsIndicator
--'

--EXEC sp_executesql @cmd

--CREATE UNIQUE CLUSTERED INDEX UI_FactInvoiceLine_Summary_Account1 ON FactInvoiceLine_Summary_Account1
--(
--AccountKey, 
--AccountManagerKey,
----ManagedByUserKey,
--InvoiceDateMonthKey,
--AccountGroup1Key,
--AccountGroup2Key,
--AccountGroup3Key,
--Last12MonthsIndicator
--) ON [SECONDARY]


--CREATE INDEX I_FactInvoiceLine_Summary_Account1_1 ON FactInvoiceLine_Summary_Account1(AccountKey) ON [SECONDARY]
--CREATE INDEX I_FactInvoiceLine_Summary_Account1_6 ON FactInvoiceLine_Summary_Account1(AccountManagerKey) ON [SECONDARY]
----Create Index I_FactInvoiceLine_Summary_Account1_7 on FactInvoiceLine_Summary_Account1(ManagedByUserKey) ON [SECONDARY]
--CREATE INDEX I_FactInvoiceLine_Summary_Account1_2 ON FactInvoiceLine_Summary_Account1(InvoiceDateMonthKey) ON [SECONDARY]
--CREATE INDEX I_FactInvoiceLine_Summary_Account1_3 ON FactInvoiceLine_Summary_Account1(AccountGroup1Key) ON [SECONDARY]
--CREATE INDEX I_FactInvoiceLine_Summary_Account1_4 ON FactInvoiceLine_Summary_Account1(AccountGroup2Key) ON [SECONDARY]
--CREATE INDEX I_FactInvoiceLine_Summary_Account1_5 ON FactInvoiceLine_Summary_Account1(AccountGroup3Key) ON [SECONDARY]

--CREATE INDEX I_FactInvoiceLine_Summary_Account1_20 ON FactInvoiceLine_Summary_Account1(TotalQuantity) ON [SECONDARY]
--CREATE INDEX I_FactInvoiceLine_Summary_Account1_21 ON FactInvoiceLine_Summary_Account1(TotalActualPrice) ON [SECONDARY]
--CREATE INDEX I_FactInvoiceLine_Summary_Account1_22 ON FactInvoiceLine_Summary_Account1(TotalActualCost) ON [SECONDARY]
--CREATE INDEX I_FactInvoiceLine_Summary_Account1_23 ON FactInvoiceLine_Summary_Account1(TotalActualRebate) ON [SECONDARY]

--SELECT COUNT(*) FactInvoiceLine_Summary_Account1 FROM FactInvoiceLine_Summary_Account1
---------------------------------------------------------
----go

--SET @cmd = '
--CREATE VIEW dbo.FactInvoiceLine_Summary_Account2
--WITH SCHEMABINDING
--AS
--SELECT 
--AccountKey,
--AccountManagerKey,
----ManagedByUserKey,
--InvoiceDateMonthKey,
--AccountGroup1Key,
--AccountGroup2Key,
--AccountGroup3Key,
--InvoiceLineGroup1Key, 
--InvoiceLineGroup2Key, 
--InvoiceLineGroup3Key, 
--Last12MonthsIndicator,
--Sum(TotalQuantity) TotalQuantity, 
--Sum(TotalActualPrice) TotalActualPrice, 
--Sum(TotalActualCost) TotalActualCost, 
--Sum(TotalActualRebate) TotalActualRebate,
--Count_Big(*) CountBig
--FROM dbo.FactInvoiceLine
--Group By
--AccountKey,
--AccountManagerKey,
----ManagedByUserKey,
--InvoiceDateMonthKey,
--AccountGroup1Key,
--AccountGroup2Key,
--AccountGroup3Key,
--InvoiceLineGroup1Key, 
--InvoiceLineGroup2Key, 
--InvoiceLineGroup3Key,
--Last12MonthsIndicator'

--EXEC sp_executesql @cmd  CREATE UNIQUE CLUSTERED INDEX UI_FactInvoiceLine_Summary_Account2 ON FactInvoiceLine_Summary_Account2
--(
--AccountKey,
--AccountManagerKey,
----ManagedByUserKey,
--InvoiceDateMonthKey,
--AccountGroup1Key,
--AccountGroup2Key,
--AccountGroup3Key,
--InvoiceLineGroup1Key, 
--InvoiceLineGroup2Key, 
--InvoiceLineGroup3Key,
--Last12MonthsIndicator
--) ON [SECONDARY]


--CREATE INDEX I_FactInvoiceLine_Summary_Account2_1 ON FactInvoiceLine_Summary_Account2(AccountKey) ON [SECONDARY]
--CREATE INDEX I_FactInvoiceLine_Summary_Account2_6 ON FactInvoiceLine_Summary_Account2(AccountManagerKey) ON [SECONDARY]
----Create Index I_FactInvoiceLine_Summary_Account2_7 on FactInvoiceLine_Summary_Account2(ManagedByUserKey) ON [SECONDARY]
--CREATE INDEX I_FactInvoiceLine_Summary_Account2_2 ON FactInvoiceLine_Summary_Account2(InvoiceDateMonthKey) ON [SECONDARY]
--CREATE INDEX I_FactInvoiceLine_Summary_Account2_3 ON FactInvoiceLine_Summary_Account2(AccountGroup1Key) ON [SECONDARY]
--CREATE INDEX I_FactInvoiceLine_Summary_Account2_4 ON FactInvoiceLine_Summary_Account2(AccountGroup2Key) ON [SECONDARY]
--CREATE INDEX I_FactInvoiceLine_Summary_Account2_5 ON FactInvoiceLine_Summary_Account2(AccountGroup3Key) ON [SECONDARY] CREATE INDEX I_FactInvoiceLine_Summary_Account2_8 ON FactInvoiceLine_Summary_Account2(InvoiceLineGroup1Key) ON [SECONDARY]
--CREATE INDEX I_FactInvoiceLine_Summary_Account2_9 ON FactInvoiceLine_Summary_Account2(InvoiceLineGroup2Key) ON [SECONDARY]
--CREATE INDEX I_FactInvoiceLine_Summary_Account2_10 ON FactInvoiceLine_Summary_Account2(InvoiceLineGroup3Key) ON [SECONDARY]


--CREATE INDEX I_FactInvoiceLine_Summary_Account2_20 ON FactInvoiceLine_Summary_Account2(TotalQuantity) ON [SECONDARY]
--CREATE INDEX I_FactInvoiceLine_Summary_Account2_21 ON FactInvoiceLine_Summary_Account2(TotalActualPrice) ON [SECONDARY]
--CREATE INDEX I_FactInvoiceLine_Summary_Account2_22 ON FactInvoiceLine_Summary_Account2(TotalActualCost) ON [SECONDARY]
--CREATE INDEX I_FactInvoiceLine_Summary_Account2_23 ON FactInvoiceLine_Summary_Account2(TotalActualRebate) ON [SECONDARY]

--SELECT COUNT(*) FactInvoiceLine_Summary_Account2 FROM FactInvoiceLine_Summary_Account2

---------------------------------------------------------
--SET @cmd = '
--CREATE VIEW dbo.FactInvoiceLine_Summary_Account3
--WITH SCHEMABINDING
--AS
--SELECT 
--AccountKey, 
--InvoiceDateMonthKey, 
--ItemGroup1Key,
--ItemGroup2Key,
--ItemGroup3Key,
--Last12MonthsIndicator,
--Sum(TotalQuantity) TotalQuantity, 
--Sum(TotalActualPrice) TotalActualPrice, 
--Sum(TotalActualCost) TotalActualCost, 
--Sum(TotalActualRebate) TotalActualRebate,
--Count_Big(*) CountBig
--FROM dbo.FactInvoiceLine
--Group By
--AccountKey, 
--InvoiceDateMonthKey, 
--ItemGroup1Key,
--ItemGroup2Key,
--ItemGroup3Key,
--Last12MonthsIndicator
--'

--EXEC sp_executesql @cmd

--CREATE UNIQUE CLUSTERED INDEX UI_FactInvoiceLine_Summary_Account3 ON FactInvoiceLine_Summary_Account3
--(
--AccountKey, 
--InvoiceDateMonthKey,
--ItemGroup1Key,
--ItemGroup2Key,
--ItemGroup3Key,
--Last12MonthsIndicator
--) ON [SECONDARY]



--CREATE INDEX I_FactInvoiceLine_Summary_Account3_1 ON FactInvoiceLine_Summary_Account3(AccountKey) ON [SECONDARY]
--CREATE INDEX I_FactInvoiceLine_Summary_Account3_3 ON FactInvoiceLine_Summary_Account3(InvoiceDateMonthKey) ON [SECONDARY]
--CREATE INDEX I_FactInvoiceLine_Summary_Account3_10 ON FactInvoiceLine_Summary_Account3(ItemGroup1Key) ON [SECONDARY]
--CREATE INDEX I_FactInvoiceLine_Summary_Account3_11 ON FactInvoiceLine_Summary_Account3(ItemGroup2Key) ON [SECONDARY]
--CREATE INDEX I_FactInvoiceLine_Summary_Account3_12 ON FactInvoiceLine_Summary_Account3(ItemGroup3Key) ON [SECONDARY]

--CREATE INDEX I_FactInvoiceLine_Summary_Account3_20 ON FactInvoiceLine_Summary_Account3(TotalQuantity) ON [SECONDARY]
--CREATE INDEX I_FactInvoiceLine_Summary_Account3_21 ON FactInvoiceLine_Summary_Account3(TotalActualPrice) ON [SECONDARY]
--CREATE INDEX I_FactInvoiceLine_Summary_Account3_22 ON FactInvoiceLine_Summary_Account3(TotalActualCost) ON [SECONDARY]
--CREATE INDEX I_FactInvoiceLine_Summary_Account3_23 ON FactInvoiceLine_Summary_Account3(TotalActualRebate) ON [SECONDARY]

--SELECT COUNT(*) FactInvoiceLine_Summary_Account3 FROM FactInvoiceLine_Summary_Account3
---------------------------------------------------------

--SET @cmd = '

--CREATE VIEW dbo.FactInvoiceLine_Summary_Account4
--WITH SCHEMABINDING
--AS
--SELECT 
--AccountKey, 
--Last12MonthsIndicator,
--Sum(TotalQuantity) TotalQuantity, 
--Sum(TotalActualPrice) TotalActualPrice, 
--Sum(TotalActualCost) TotalActualCost, 
--Sum(TotalActualRebate) TotalActualRebate,
--Count_Big(*) CountBig
--FROM dbo.FactInvoiceLine
--Group By
--AccountKey,
--Last12MonthsIndicator'

--EXEC sp_executesql @cmd

--CREATE UNIQUE CLUSTERED INDEX UI_FactInvoiceLine_Summary_Account4 ON FactInvoiceLine_Summary_Account4
--(
--AccountKey,
--Last12MonthsIndicator) ON [SECONDARY]


--CREATE INDEX I_FactInvoiceLine_Summary_Account4_1 ON FactInvoiceLine_Summary_Account4(AccountKey) ON [SECONDARY]

--CREATE INDEX I_FactInvoiceLine_Summary_Account4_20 ON FactInvoiceLine_Summary_Account4(TotalQuantity) ON [SECONDARY]
--CREATE INDEX I_FactInvoiceLine_Summary_Account4_21 ON FactInvoiceLine_Summary_Account4(TotalActualPrice) ON [SECONDARY]
--CREATE INDEX I_FactInvoiceLine_Summary_Account4_22 ON FactInvoiceLine_Summary_Account4(TotalActualCost) ON [SECONDARY]
--CREATE INDEX I_FactInvoiceLine_Summary_Account4_23 ON FactInvoiceLine_Summary_Account4(TotalActualRebate) ON [SECONDARY]

--SELECT COUNT(*) FactInvoiceLine_Summary_Account4 FROM FactInvoiceLine_Summary_Account4
-------------------------------------------------------------------------------------------------------------


----go
--SET @cmd = '
--CREATE VIEW dbo.FactInvoiceLine_Summary_AccountManager1
--WITH SCHEMABINDING
--AS
--SELECT 
--AccountManagerKey,
----ManagedByUserKey,
--InvoiceDateMonthKey,
--Last12MonthsIndicator,
--Sum(TotalQuantity) TotalQuantity, 
--Sum(TotalActualPrice) TotalActualPrice, 
--Sum(TotalActualCost) TotalActualCost, 
--Sum(TotalActualRebate) TotalActualRebate,
--Count_Big(*) CountBig
--FROM dbo.FactInvoiceLine
--Group By
--AccountManagerKey,
----ManagedByUserKey,
--InvoiceDateMonthKey,
--Last12MonthsIndicator
--'

--EXEC sp_executesql @cmd

--CREATE UNIQUE CLUSTERED INDEX UI_FactInvoiceLine_Summary_AccountManager1 ON FactInvoiceLine_Summary_AccountManager1
--(
--AccountManagerKey,
----ManagedByUserKey,
--InvoiceDateMonthKey,
--Last12MonthsIndicator
--) ON [SECONDARY]


--CREATE INDEX I_FactInvoiceLine_Summary_AccountManager1_6 ON FactInvoiceLine_Summary_AccountManager1(AccountManagerKey) ON [SECONDARY]
----Create Index I_FactInvoiceLine_Summary_AccountManager1_7 on FactInvoiceLine_Summary_AccountManager1(ManagedByUserKey) ON [SECONDARY]
--CREATE INDEX I_FactInvoiceLine_Summary_AccountManager1_2 ON FactInvoiceLine_Summary_AccountManager1(InvoiceDateMonthKey) ON [SECONDARY]

--CREATE INDEX I_FactInvoiceLine_Summary_AccountManager1_20 ON FactInvoiceLine_Summary_AccountManager1(TotalQuantity) ON [SECONDARY]
--CREATE INDEX I_FactInvoiceLine_Summary_AccountManager1_21 ON FactInvoiceLine_Summary_AccountManager1(TotalActualPrice) ON [SECONDARY]
--CREATE INDEX I_FactInvoiceLine_Summary_AccountManager1_22 ON FactInvoiceLine_Summary_AccountManager1(TotalActualCost) ON [SECONDARY]
--CREATE INDEX I_FactInvoiceLine_Summary_AccountManager1_23 ON FactInvoiceLine_Summary_AccountManager1(TotalActualRebate) ON [SECONDARY]

--SELECT COUNT(*) FactInvoiceLine_Summary_AccountManager1 FROM FactInvoiceLine_Summary_AccountManager1
---------------------------------------------------------
--SET @cmd = '
--CREATE VIEW dbo.FactInvoiceLine_Summary_AccountCount
--WITH SCHEMABINDING
--AS
--SELECT 
--AccountManagerKey,
--AccountKey, 
--InvoiceDateMonthKey,
--Last12MonthsIndicator,
--Count_Big(*) CountBig
--FROM dbo.FactInvoiceLine
--Group By
--AccountManagerKey,
--AccountKey, 
--InvoiceDateMonthKey,
--Last12MonthsIndicator
--'

--EXEC sp_executesql @cmd

--CREATE UNIQUE CLUSTERED INDEX UI_FactInvoiceLine_Summary_AccountCount ON FactInvoiceLine_Summary_AccountCount
--(
--AccountManagerKey,
--AccountKey, 
--InvoiceDateMonthKey,
--Last12MonthsIndicator) ON [SECONDARY]


--CREATE INDEX I_FactInvoiceLine_Summary_AccountCount_1 ON FactInvoiceLine_Summary_AccountCount(AccountKey) ON [SECONDARY]
--CREATE INDEX I_FactInvoiceLine_Summary_AccountCount_2 ON FactInvoiceLine_Summary_AccountCount(InvoiceDateMonthKey) ON [SECONDARY]

--SELECT COUNT(*) FactInvoiceLine_Summary_AccountCount FROM FactInvoiceLine_Summary_AccountCount
-------------------------------------------------------------------------------------------------------------

--SET @cmd = '
--CREATE VIEW dbo.FactInvoiceLine_Summary_ItemCount
--WITH SCHEMABINDING
--AS
--SELECT 
--AccountManagerKey,
--ItemKey, 
--InvoiceDateMonthKey,
--Last12MonthsIndicator,
--Count_Big(*) CountBig
--FROM dbo.FactInvoiceLine
--Group By
--AccountManagerKey,
--ItemKey, 
--InvoiceDateMonthKey,
--Last12MonthsIndicator
--'

--EXEC sp_executesql @cmd

--CREATE UNIQUE CLUSTERED INDEX UI_FactInvoiceLine_Summary_ItemCount ON FactInvoiceLine_Summary_ItemCount
--(
--AccountManagerKey,
--ItemKey, 
--InvoiceDateMonthKey,
--Last12MonthsIndicator) ON [SECONDARY]


--CREATE INDEX I_FactInvoiceLine_Summary_ItemCount_1 ON FactInvoiceLine_Summary_ItemCount(ItemKey) ON [SECONDARY]
--CREATE INDEX I_FactInvoiceLine_Summary_ItemCount_2 ON FactInvoiceLine_Summary_ItemCount(InvoiceDateMonthKey) ON [SECONDARY]

--SELECT COUNT(*) FactInvoiceLine_Summary_ItemCount FROM FactInvoiceLine_Summary_ItemCount
-------------------------------------------------------------------------------------------------------------

-------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------

---- WITHOUT Last12MonthsIndicator!!

-------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------

--SET @cmd = '
--CREATE VIEW dbo.FactInvoiceLine_SummaryLast12Months1
--WITH SCHEMABINDING
--AS
--SELECT 
----InvoiceLineKey, 
--AccountKey,
--ItemKey, 
--VendorKey, 
--AccountManagerKey,
----ManagedByUserKey,
----InvoiceDateDayKey, 

----ClientUniqueIdentifier, 
----InvoiceLineJunkKey, 
--InvoiceLineGroup1Key, 
--InvoiceLineGroup2Key, 
--InvoiceLineGroup3Key, 
--AccountGroup1Key,
--AccountGroup2Key,
--AccountGroup3Key,
--ItemGroup1Key,
--ItemGroup2Key,
--ItemGroup3Key,
--Last12MonthsIndicator,
--Sum(TotalQuantity) TotalQuantity, 
--Sum(TotalActualPrice) TotalActualPrice, 
--Sum(TotalActualCost) TotalActualCost, 
--Sum(TotalActualRebate) TotalActualRebate,
--Count_Big(*) CountBig
--FROM dbo.FactInvoiceLine
--Group By
----InvoiceLineKey, 
--AccountKey, 
--ItemKey, 
--VendorKey, 
--AccountManagerKey,
----ManagedByUserKey,
----InvoiceDateDayKey, 

----ClientUniqueIdentifier, 
----InvoiceLineJunkKey, 
--InvoiceLineGroup1Key, 
--InvoiceLineGroup2Key, 
--InvoiceLineGroup3Key,
--AccountGroup1Key,
--AccountGroup2Key,
--AccountGroup3Key,
--ItemGroup1Key,
--ItemGroup2Key,
--ItemGroup3Key,
--Last12MonthsIndicator'

--EXEC sp_executesql @cmd

--CREATE UNIQUE CLUSTERED INDEX UI_FactInvoiceLine_SummaryLast12Months1 ON FactInvoiceLine_SummaryLast12Months1
--(
----InvoiceLineKey, 
--AccountKey, 
--ItemKey,
--VendorKey,
--AccountManagerKey,
----ManagedByUserKey,
----InvoiceDateDayKey, 

----ClientUniqueIdentifier, 
----InvoiceLineJunkKey, 
--InvoiceLineGroup1Key,
--InvoiceLineGroup2Key, 
--InvoiceLineGroup3Key,
--AccountGroup1Key,
--AccountGroup2Key,
--AccountGroup3Key,
--ItemGroup1Key,
--ItemGroup2Key,
--ItemGroup3Key,
--Last12MonthsIndicator
--) ON [SECONDARY]





--CREATE INDEX I_FactInvoiceLine_SummaryLast12Months1_1 ON FactInvoiceLine_SummaryLast12Months1(AccountKey) ON [SECONDARY]
--CREATE INDEX I_FactInvoiceLine_SummaryLast12Months1_2 ON FactInvoiceLine_SummaryLast12Months1(ItemKey) ON [SECONDARY]
--CREATE INDEX I_FactInvoiceLine_SummaryLast12Months1_13 ON FactInvoiceLine_SummaryLast12Months1(VendorKey) ON [SECONDARY]
--CREATE INDEX I_FactInvoiceLine_SummaryLast12Months1_14 ON FactInvoiceLine_SummaryLast12Months1(AccountManagerKey) ON [SECONDARY]
----Create Index I_FactInvoiceLine_SummaryLast12Months1_15 on FactInvoiceLine_SummaryLast12Months1(ManagedByUserKey) ON [SECONDARY]
--CREATE INDEX I_FactInvoiceLine_SummaryLast12Months1_4 ON FactInvoiceLine_SummaryLast12Months1(InvoiceLineGroup1Key) ON [SECONDARY]
--CREATE INDEX I_FactInvoiceLine_SummaryLast12Months1_5 ON FactInvoiceLine_SummaryLast12Months1(InvoiceLineGroup2Key) ON [SECONDARY]
--CREATE INDEX I_FactInvoiceLine_SummaryLast12Months1_6 ON FactInvoiceLine_SummaryLast12Months1(InvoiceLineGroup3Key) ON [SECONDARY]
--CREATE INDEX I_FactInvoiceLine_SummaryLast12Months1_7 ON FactInvoiceLine_SummaryLast12Months1(AccountGroup1Key) ON [SECONDARY]
--CREATE INDEX I_FactInvoiceLine_SummaryLast12Months1_8 ON FactInvoiceLine_SummaryLast12Months1(AccountGroup2Key) ON [SECONDARY]
--CREATE INDEX I_FactInvoiceLine_SummaryLast12Months1_9 ON FactInvoiceLine_SummaryLast12Months1(AccountGroup3Key) ON [SECONDARY]
--CREATE INDEX I_FactInvoiceLine_SummaryLast12Months1_10 ON FactInvoiceLine_SummaryLast12Months1(ItemGroup1Key) ON [SECONDARY]
--CREATE INDEX I_FactInvoiceLine_SummaryLast12Months1_11 ON FactInvoiceLine_SummaryLast12Months1(ItemGroup2Key) ON [SECONDARY]
--CREATE INDEX I_FactInvoiceLine_SummaryLast12Months1_12 ON FactInvoiceLine_SummaryLast12Months1(ItemGroup3Key) ON [SECONDARY]

--CREATE INDEX I_FactInvoiceLine_SummaryLast12Months1_20 ON FactInvoiceLine_SummaryLast12Months1(TotalQuantity) ON [SECONDARY]
--CREATE INDEX I_FactInvoiceLine_SummaryLast12Months1_21 ON FactInvoiceLine_SummaryLast12Months1(TotalActualPrice) ON [SECONDARY]
--CREATE INDEX I_FactInvoiceLine_SummaryLast12Months1_22 ON FactInvoiceLine_SummaryLast12Months1(TotalActualCost) ON [SECONDARY]
--CREATE INDEX I_FactInvoiceLine_SummaryLast12Months1_23 ON FactInvoiceLine_SummaryLast12Months1(TotalActualRebate) ON [SECONDARY]

--SELECT COUNT(*) FactInvoiceLine_SummaryLast12Months1 FROM FactInvoiceLine_SummaryLast12Months1
-------------------------------------------------------------------------------------------------------------

--SET @cmd = '
--CREATE VIEW dbo.FactInvoiceLine_SummaryLast12Months2
--WITH SCHEMABINDING
--AS
--SELECT 
--AccountKey, 
--ItemKey, 
--ItemGroup1Key,
--Last12MonthsIndicator,
--Sum(TotalQuantity) TotalQuantity, 
--Sum(TotalActualPrice) TotalActualPrice, 
--Sum(TotalActualCost) TotalActualCost,  Count_Big(*) CountBig
--FROM dbo.FactInvoiceLine
--Group By
--AccountKey, 
--ItemKey, 
--ItemGroup1Key,
--Last12MonthsIndicator'

--EXEC sp_executesql @cmd

--CREATE UNIQUE CLUSTERED INDEX UI_FactInvoiceLine_SummaryLast12Months2 ON FactInvoiceLine_SummaryLast12Months2
--(
--AccountKey, 
--ItemKey,
--ItemGroup1Key,
--Last12MonthsIndicator) ON [SECONDARY]


--CREATE INDEX I_FactInvoiceLine_SummaryLast12Months2_2 ON FactInvoiceLine_SummaryLast12Months2(ItemKey) ON [SECONDARY]
--CREATE INDEX I_FactInvoiceLine_SummaryLast12Months2_10 ON FactInvoiceLine_SummaryLast12Months2(ItemGroup1Key) ON [SECONDARY]

--CREATE INDEX I_FactInvoiceLine_SummaryLast12Months2_20 ON FactInvoiceLine_SummaryLast12Months2(TotalQuantity) ON [SECONDARY]
--CREATE INDEX I_FactInvoiceLine_SummaryLast12Months2_21 ON FactInvoiceLine_SummaryLast12Months2(TotalActualPrice) ON [SECONDARY]
--CREATE INDEX I_FactInvoiceLine_SummaryLast12Months2_22 ON FactInvoiceLine_SummaryLast12Months2(TotalActualCost) ON [SECONDARY]

--SELECT COUNT(*) FactInvoiceLine_SummaryLast12Months2 FROM FactInvoiceLine_SummaryLast12Months2
-------------------------------------------------------------------------------------------------------------

--SET @cmd = '
--CREATE VIEW dbo.FactInvoiceLine_SummaryLast12Months3
--WITH SCHEMABINDING
--AS
--SELECT 
--AccountKey, 
--ItemKey, 
--VendorKey, 
--AccountManagerKey,
----ManagedByUserKey,
--InvoiceLineGroup1Key, 
--InvoiceLineGroup2Key, 
--InvoiceLineGroup3Key, 
--AccountGroup1Key,
--AccountGroup2Key,
--AccountGroup3Key,
--ItemGroup1Key,
--ItemGroup2Key,
--ItemGroup3Key,
--Last12MonthsIndicator,
--Sum(TotalQuantity) TotalQuantity, 
--Sum(TotalActualPrice) TotalActualPrice, 
--Sum(TotalActualCost) TotalActualCost, 
--Sum(TotalActualRebate) TotalActualRebate,
--Count_Big(*) CountBig
--FROM dbo.FactInvoiceLine
--Group By
--AccountKey, 
--ItemKey, 
--VendorKey, 
--AccountManagerKey,
----ManagedByUserKey,
--InvoiceLineGroup1Key, 
--InvoiceLineGroup2Key, 
--InvoiceLineGroup3Key,
--AccountGroup1Key,
--AccountGroup2Key,
--AccountGroup3Key,
--ItemGroup1Key,
--ItemGroup2Key,
--ItemGroup3Key,
--Last12MonthsIndicator'

--EXEC sp_executesql @cmd

--CREATE UNIQUE CLUSTERED INDEX UI_FactInvoiceLine_SummaryLast12Months3 ON FactInvoiceLine_SummaryLast12Months3
--(
--AccountKey, 
--ItemKey, 
--VendorKey, 
--AccountManagerKey,
----ManagedByUserKey,
--InvoiceLineGroup1Key, 
--InvoiceLineGroup2Key, 
--InvoiceLineGroup3Key,
--AccountGroup1Key,
--AccountGroup2Key,
--AccountGroup3Key,
--ItemGroup1Key,
--ItemGroup2Key,
--ItemGroup3Key,
--Last12MonthsIndicator) ON [SECONDARY]


--CREATE INDEX I_FactInvoiceLine_SummaryLast12Months3_1 ON FactInvoiceLine_SummaryLast12Months3(AccountKey) ON [SECONDARY]
--CREATE INDEX I_FactInvoiceLine_SummaryLast12Months3_2 ON FactInvoiceLine_SummaryLast12Months3(ItemKey) ON [SECONDARY]
--CREATE INDEX I_FactInvoiceLine_SummaryLast12Months3_13 ON FactInvoiceLine_SummaryLast12Months3(VendorKey) ON [SECONDARY]
--CREATE INDEX I_FactInvoiceLine_SummaryLast12Months3_14 ON FactInvoiceLine_SummaryLast12Months3(AccountManagerKey) ON [SECONDARY]
----Create Index I_FactInvoiceLine_SummaryLast12Months3_15 on FactInvoiceLine_SummaryLast12Months3(ManagedByUserKey) ON [SECONDARY]
--CREATE INDEX I_FactInvoiceLine_SummaryLast12Months3_4 ON FactInvoiceLine_SummaryLast12Months3(InvoiceLineGroup1Key) ON [SECONDARY]
--CREATE INDEX I_FactInvoiceLine_SummaryLast12Months3_5 ON FactInvoiceLine_SummaryLast12Months3(InvoiceLineGroup2Key) ON [SECONDARY] CREATE INDEX I_FactInvoiceLine_SummaryLast12Months3_6 ON FactInvoiceLine_SummaryLast12Months3(InvoiceLineGroup3Key) ON [SECONDARY]
--CREATE INDEX I_FactInvoiceLine_SummaryLast12Months3_7 ON FactInvoiceLine_SummaryLast12Months3(AccountGroup1Key) ON [SECONDARY]
--CREATE INDEX I_FactInvoiceLine_SummaryLast12Months3_8 ON FactInvoiceLine_SummaryLast12Months3(AccountGroup2Key) ON [SECONDARY]
--CREATE INDEX I_FactInvoiceLine_SummaryLast12Months3_9 ON FactInvoiceLine_SummaryLast12Months3(AccountGroup3Key) ON [SECONDARY]
--CREATE INDEX I_FactInvoiceLine_SummaryLast12Months3_10 ON FactInvoiceLine_SummaryLast12Months3(ItemGroup1Key) ON [SECONDARY]
--CREATE INDEX I_FactInvoiceLine_SummaryLast12Months3_11 ON FactInvoiceLine_SummaryLast12Months3(ItemGroup2Key) ON [SECONDARY]
--CREATE INDEX I_FactInvoiceLine_SummaryLast12Months3_12 ON FactInvoiceLine_SummaryLast12Months3(ItemGroup3Key) ON [SECONDARY]

--CREATE INDEX I_FactInvoiceLine_SummaryLast12Months3_20 ON FactInvoiceLine_SummaryLast12Months3(TotalQuantity) ON [SECONDARY]
--CREATE INDEX I_FactInvoiceLine_SummaryLast12Months3_21 ON FactInvoiceLine_SummaryLast12Months3(TotalActualPrice) ON [SECONDARY]
--CREATE INDEX I_FactInvoiceLine_SummaryLast12Months3_22 ON FactInvoiceLine_SummaryLast12Months3(TotalActualCost) ON [SECONDARY]
--CREATE INDEX I_FactInvoiceLine_SummaryLast12Months3_23 ON FactInvoiceLine_SummaryLast12Months3(TotalActualRebate) ON [SECONDARY]
--CREATE INDEX I_FactInvoiceLine_SummaryLast12Months3_25 ON FactInvoiceLine_SummaryLast12Months3(Last12MonthsIndicator) INCLUDE (AccountGroup1Key, AccountKey, CountBig, InvoiceLineGroup1Key, ItemKey, TotalActualCost, TotalActualPrice, TotalQuantity)

--SELECT COUNT(*) FactInvoiceLine_SummaryLast12Months3 FROM FactInvoiceLine_SummaryLast12Months3
-------------------------------------------------------------------------------------------------------------

----go
--SET @cmd = '
--CREATE VIEW dbo.FactInvoiceLine_SummaryLast12Months_AccountGroupAll1
--WITH SCHEMABINDING
--AS
--SELECT 
--AccountGroup1Key,
--AccountGroup2Key,
--AccountGroup3Key,
--Last12MonthsIndicator,
--Sum(TotalQuantity) TotalQuantity, 
--Sum(TotalActualPrice) TotalActualPrice, 
--Sum(TotalActualCost) TotalActualCost, 
--Sum(TotalActualRebate) TotalActualRebate,
--Count_Big(*) CountBig
--FROM dbo.FactInvoiceLine
--Group By
--AccountGroup1Key,
--AccountGroup2Key,
--AccountGroup3Key,
--Last12MonthsIndicator
--'

--EXEC sp_executesql @cmd

--CREATE UNIQUE CLUSTERED INDEX UI_FactInvoiceLine_SummaryLast12Months_AccountGroupAll1 ON FactInvoiceLine_SummaryLast12Months_AccountGroupAll1
--(
--AccountGroup1Key,
--AccountGroup2Key,
--AccountGroup3Key,
--Last12MonthsIndicator
--) ON [SECONDARY]


--CREATE INDEX I_FactInvoiceLine_SummaryLast12Months_AccountGroupAll1_4 ON FactInvoiceLine_SummaryLast12Months_AccountGroupAll1(AccountGroup1Key) ON [SECONDARY]
--CREATE INDEX I_FactInvoiceLine_SummaryLast12Months_AccountGroupAll1_5 ON FactInvoiceLine_SummaryLast12Months_AccountGroupAll1(AccountGroup2Key) ON [SECONDARY]
--CREATE INDEX I_FactInvoiceLine_SummaryLast12Months_AccountGroupAll1_6 ON FactInvoiceLine_SummaryLast12Months_AccountGroupAll1(AccountGroup3Key) ON [SECONDARY]

--CREATE INDEX I_FactInvoiceLine_SummaryLast12Months_AccountGroupAll1_20 ON FactInvoiceLine_SummaryLast12Months_AccountGroupAll1(TotalQuantity) ON [SECONDARY]
--CREATE INDEX I_FactInvoiceLine_SummaryLast12Months_AccountGroupAll1_21 ON FactInvoiceLine_SummaryLast12Months_AccountGroupAll1(TotalActualPrice) ON [SECONDARY]
--CREATE INDEX I_FactInvoiceLine_SummaryLast12Months_AccountGroupAll1_22 ON FactInvoiceLine_SummaryLast12Months_AccountGroupAll1(TotalActualCost) ON [SECONDARY]
--CREATE INDEX I_FactInvoiceLine_SummaryLast12Months_AccountGroupAll1_23 ON FactInvoiceLine_SummaryLast12Months_AccountGroupAll1(TotalActualRebate) ON [SECONDARY]

--SELECT COUNT(*) FactInvoiceLine_SummaryLast12Months_AccountGroupAll1 FROM FactInvoiceLine_SummaryLast12Months_AccountGroupAll1
-----------------------------------------------------------------------------------
----go
--SET @cmd = '
--CREATE VIEW dbo.FactInvoiceLine_SummaryLast12Months_ItemGroupAll1
--WITH SCHEMABINDING
--AS
--SELECT 
--ItemGroup1Key,
--ItemGroup2Key,
--ItemGroup3Key,
--Last12MonthsIndicator,
--Sum(TotalQuantity) TotalQuantity, 
--Sum(TotalActualPrice) TotalActualPrice, 
--Sum(TotalActualCost) TotalActualCost, 
--Sum(TotalActualRebate) TotalActualRebate,
--Count_Big(*) CountBig
--FROM dbo.FactInvoiceLine
--Group By
--ItemGroup1Key,
--ItemGroup2Key,
--ItemGroup3Key,
--Last12MonthsIndicator
--'

--EXEC sp_executesql @cmd

--CREATE UNIQUE CLUSTERED INDEX UI_FactInvoiceLine_SummaryLast12Months_ItemGroupAll1 ON FactInvoiceLine_SummaryLast12Months_ItemGroupAll1
--(
--ItemGroup1Key,
--ItemGroup2Key,
--ItemGroup3Key,
--Last12MonthsIndicator
--) ON [SECONDARY]


--CREATE INDEX I_FactInvoiceLine_SummaryLast12Months_ItemGroupAll1_4 ON FactInvoiceLine_SummaryLast12Months_ItemGroupAll1(ItemGroup1Key) ON [SECONDARY]
--CREATE INDEX I_FactInvoiceLine_SummaryLast12Months_ItemGroupAll1_5 ON FactInvoiceLine_SummaryLast12Months_ItemGroupAll1(ItemGroup2Key) ON [SECONDARY]
--CREATE INDEX I_FactInvoiceLine_SummaryLast12Months_ItemGroupAll1_6 ON FactInvoiceLine_SummaryLast12Months_ItemGroupAll1(ItemGroup3Key) ON [SECONDARY]

--CREATE INDEX I_FactInvoiceLine_SummaryLast12Months_ItemGroupAll1_20 ON FactInvoiceLine_SummaryLast12Months_ItemGroupAll1(TotalQuantity) ON [SECONDARY]
--CREATE INDEX I_FactInvoiceLine_SummaryLast12Months_ItemGroupAll1_21 ON FactInvoiceLine_SummaryLast12Months_ItemGroupAll1(TotalActualPrice) ON [SECONDARY]
--CREATE INDEX I_FactInvoiceLine_SummaryLast12Months_ItemGroupAll1_22 ON FactInvoiceLine_SummaryLast12Months_ItemGroupAll1(TotalActualCost) ON [SECONDARY]
--CREATE INDEX I_FactInvoiceLine_SummaryLast12Months_ItemGroupAll1_23 ON FactInvoiceLine_SummaryLast12Months_ItemGroupAll1(TotalActualRebate) ON [SECONDARY]

--SELECT COUNT(*) FactInvoiceLine_SummaryLast12Months_ItemGroupAll1 FROM FactInvoiceLine_SummaryLast12Months_ItemGroupAll1
---------------------------------------------------------
----go
--SET @cmd = '
--CREATE VIEW dbo.FactInvoiceLine_SummaryLast12Months_InvoiceLineGroupAll1
--WITH SCHEMABINDING
--AS
--SELECT 
--InvoiceLineGroup1Key, 
--InvoiceLineGroup2Key, 
--InvoiceLineGroup3Key, 
--Last12MonthsIndicator,
--Sum(TotalQuantity) TotalQuantity, 
--Sum(TotalActualPrice) TotalActualPrice, 
--Sum(TotalActualCost) TotalActualCost, 
--Sum(TotalActualRebate) TotalActualRebate,
--Count_Big(*) CountBig
--FROM dbo.FactInvoiceLine
--Group By
--InvoiceLineGroup1Key, 
--InvoiceLineGroup2Key, 
--InvoiceLineGroup3Key,
--Last12MonthsIndicator
--'
--EXEC sp_executesql @cmd


--CREATE UNIQUE CLUSTERED INDEX UI_FactInvoiceLine_SummaryLast12Months_InvoiceLineGroupAll1 ON FactInvoiceLine_SummaryLast12Months_InvoiceLineGroupAll1
--(
--InvoiceLineGroup1Key, 
--InvoiceLineGroup2Key, 
--InvoiceLineGroup3Key,
--Last12MonthsIndicator
--) ON [SECONDARY]


--CREATE INDEX I_FactInvoiceLine_SummaryLast12Months_InvoiceLineGroupAll1_1 ON FactInvoiceLine_SummaryLast12Months_InvoiceLineGroupAll1(InvoiceLineGroup1Key) ON [SECONDARY]
--CREATE INDEX I_FactInvoiceLine_SummaryLast12Months_InvoiceLineGroupAll1_2 ON FactInvoiceLine_SummaryLast12Months_InvoiceLineGroupAll1(InvoiceLineGroup2Key) ON [SECONDARY]
--CREATE INDEX I_FactInvoiceLine_SummaryLast12Months_InvoiceLineGroupAll1_3 ON FactInvoiceLine_SummaryLast12Months_InvoiceLineGroupAll1(InvoiceLineGroup3Key) ON [SECONDARY]

--CREATE INDEX I_FactInvoiceLine_SummaryLast12Months_InvoiceLineGroupAll1_20 ON FactInvoiceLine_SummaryLast12Months_InvoiceLineGroupAll1(TotalQuantity) ON [SECONDARY]
--CREATE INDEX I_FactInvoiceLine_SummaryLast12Months_InvoiceLineGroupAll1_21 ON FactInvoiceLine_SummaryLast12Months_InvoiceLineGroupAll1(TotalActualPrice) ON [SECONDARY]
--CREATE INDEX I_FactInvoiceLine_SummaryLast12Months_InvoiceLineGroupAll1_22 ON FactInvoiceLine_SummaryLast12Months_InvoiceLineGroupAll1(TotalActualCost) ON [SECONDARY]
--CREATE INDEX I_FactInvoiceLine_SummaryLast12Months_InvoiceLineGroupAll1_23 ON FactInvoiceLine_SummaryLast12Months_InvoiceLineGroupAll1(TotalActualRebate) ON [SECONDARY]

--SELECT COUNT(*) FactInvoiceLine_SummaryLast12Months_InvoiceLineGroupAll1 FROM FactInvoiceLine_SummaryLast12Months_InvoiceLineGroupAll1
---------------------------------------------------------
----go
--SET @cmd = '
--CREATE VIEW dbo.FactInvoiceLine_SummaryLast12Months_Item1
--WITH SCHEMABINDING
--AS
--SELECT 
--ItemKey,
--VendorKey,
--ItemGroup1Key,
--ItemGroup2Key,
--ItemGroup3Key,
--Last12MonthsIndicator,
--Sum(TotalQuantity) TotalQuantity, 
--Sum(TotalActualPrice) TotalActualPrice, 
--Sum(TotalActualCost) TotalActualCost, 
--Sum(TotalActualRebate) TotalActualRebate,
--Count_Big(*) CountBig
--FROM dbo.FactInvoiceLine
--Group By
--ItemKey,
--VendorKey,
--ItemGroup1Key,
--ItemGroup2Key,
--ItemGroup3Key,
--Last12MonthsIndicator
--'

--EXEC sp_executesql @cmd

--CREATE UNIQUE CLUSTERED INDEX UI_FactInvoiceLine_SummaryLast12Months_Item1 ON FactInvoiceLine_SummaryLast12Months_Item1
--(
--ItemKey,
--VendorKey,
--ItemGroup1Key,
--ItemGroup2Key,
--ItemGroup3Key,
--Last12MonthsIndicator
--) ON [SECONDARY]


--CREATE INDEX I_FactInvoiceLine_SummaryLast12Months_Item1_1 ON FactInvoiceLine_SummaryLast12Months_Item1(ItemKey) ON [SECONDARY]
--CREATE INDEX I_FactInvoiceLine_SummaryLast12Months_Item1_3 ON FactInvoiceLine_SummaryLast12Months_Item1(VendorKey) ON [SECONDARY]
--CREATE INDEX I_FactInvoiceLine_SummaryLast12Months_Item1_4 ON FactInvoiceLine_SummaryLast12Months_Item1(ItemGroup1Key) ON [SECONDARY]
--CREATE INDEX I_FactInvoiceLine_SummaryLast12Months_Item1_5 ON FactInvoiceLine_SummaryLast12Months_Item1(ItemGroup2Key) ON [SECONDARY]
--CREATE INDEX I_FactInvoiceLine_SummaryLast12Months_Item1_6 ON FactInvoiceLine_SummaryLast12Months_Item1(ItemGroup3Key) ON [SECONDARY]

--CREATE INDEX I_FactInvoiceLine_SummaryLast12Months_Item1_20 ON FactInvoiceLine_SummaryLast12Months_Item1(TotalQuantity) ON [SECONDARY]
--CREATE INDEX I_FactInvoiceLine_SummaryLast12Months_Item1_21 ON FactInvoiceLine_SummaryLast12Months_Item1(TotalActualPrice) ON [SECONDARY]
--CREATE INDEX I_FactInvoiceLine_SummaryLast12Months_Item1_22 ON FactInvoiceLine_SummaryLast12Months_Item1(TotalActualCost) ON [SECONDARY]
--CREATE INDEX I_FactInvoiceLine_SummaryLast12Months_Item1_23 ON FactInvoiceLine_SummaryLast12Months_Item1(TotalActualRebate) ON [SECONDARY]
-- SELECT COUNT(*) FactInvoiceLine_SummaryLast12Months_Item1 FROM FactInvoiceLine_SummaryLast12Months_Item1
---------------------------------------------------------
----go
--SET @cmd = '
--CREATE VIEW dbo.FactInvoiceLine_SummaryLast12Months_Account1
--WITH SCHEMABINDING
--AS
--SELECT 
--AccountKey,
--AccountManagerKey,
----ManagedByUserKey,
--AccountGroup1Key,
--AccountGroup2Key,
--AccountGroup3Key,
--Last12MonthsIndicator,
--Sum(TotalQuantity) TotalQuantity, 
--Sum(TotalActualPrice) TotalActualPrice, 
--Sum(TotalActualCost) TotalActualCost, 
--Sum(TotalActualRebate) TotalActualRebate,
--Count_Big(*) CountBig
--FROM dbo.FactInvoiceLine
--Group By
--AccountKey,
--AccountManagerKey,
----ManagedByUserKey,
--AccountGroup1Key,
--AccountGroup2Key,
--AccountGroup3Key,
--Last12MonthsIndicator
--'

--EXEC sp_executesql @cmd

--CREATE UNIQUE CLUSTERED INDEX UI_FactInvoiceLine_SummaryLast12Months_Account1 ON FactInvoiceLine_SummaryLast12Months_Account1
--(
--AccountKey, 
--AccountManagerKey,
----ManagedByUserKey,
--AccountGroup1Key,
--AccountGroup2Key,
--AccountGroup3Key,
--Last12MonthsIndicator
--) ON [SECONDARY]


--CREATE INDEX I_FactInvoiceLine_SummaryLast12Months_Account1_1 ON FactInvoiceLine_SummaryLast12Months_Account1(AccountKey) ON [SECONDARY]
--CREATE INDEX I_FactInvoiceLine_SummaryLast12Months_Account1_6 ON FactInvoiceLine_SummaryLast12Months_Account1(AccountManagerKey) ON [SECONDARY]
----Create Index I_FactInvoiceLine_SummaryLast12Months_Account1_7 on FactInvoiceLine_SummaryLast12Months_Account1(ManagedByUserKey) ON [SECONDARY]
--CREATE INDEX I_FactInvoiceLine_SummaryLast12Months_Account1_3 ON FactInvoiceLine_SummaryLast12Months_Account1(AccountGroup1Key) ON [SECONDARY]
--CREATE INDEX I_FactInvoiceLine_SummaryLast12Months_Account1_4 ON FactInvoiceLine_SummaryLast12Months_Account1(AccountGroup2Key) ON [SECONDARY]
--CREATE INDEX I_FactInvoiceLine_SummaryLast12Months_Account1_5 ON FactInvoiceLine_SummaryLast12Months_Account1(AccountGroup3Key) ON [SECONDARY]

--CREATE INDEX I_FactInvoiceLine_SummaryLast12Months_Account1_20 ON FactInvoiceLine_SummaryLast12Months_Account1(TotalQuantity) ON [SECONDARY]
--CREATE INDEX I_FactInvoiceLine_SummaryLast12Months_Account1_21 ON FactInvoiceLine_SummaryLast12Months_Account1(TotalActualPrice) ON [SECONDARY]
--CREATE INDEX I_FactInvoiceLine_SummaryLast12Months_Account1_22 ON FactInvoiceLine_SummaryLast12Months_Account1(TotalActualCost) ON [SECONDARY]
--CREATE INDEX I_FactInvoiceLine_SummaryLast12Months_Account1_23 ON FactInvoiceLine_SummaryLast12Months_Account1(TotalActualRebate) ON [SECONDARY]

--SELECT COUNT(*) FactInvoiceLine_SummaryLast12Months_Account1 FROM FactInvoiceLine_SummaryLast12Months_Account1
---------------------------------------------------------
----go

--SET @cmd = '
--CREATE VIEW dbo.FactInvoiceLine_SummaryLast12Months_Account2
--WITH SCHEMABINDING
--AS
--SELECT 
--AccountKey,
--AccountManagerKey,
----ManagedByUserKey,
--AccountGroup1Key,
--AccountGroup2Key,
--AccountGroup3Key,
--InvoiceLineGroup1Key, 
--InvoiceLineGroup2Key, 
--InvoiceLineGroup3Key, 
--Last12MonthsIndicator,
--Sum(TotalQuantity) TotalQuantity, 
--Sum(TotalActualPrice) TotalActualPrice, 
--Sum(TotalActualCost) TotalActualCost, 
--Sum(TotalActualRebate) TotalActualRebate,
--Count_Big(*) CountBig
--FROM dbo.FactInvoiceLine
--Group By
--AccountKey,
--AccountManagerKey,
----ManagedByUserKey,
--AccountGroup1Key,
--AccountGroup2Key,
--AccountGroup3Key,
--InvoiceLineGroup1Key, 
--InvoiceLineGroup2Key, 
--InvoiceLineGroup3Key,
--Last12MonthsIndicator'

--EXEC sp_executesql @cmd  CREATE UNIQUE CLUSTERED INDEX UI_FactInvoiceLine_SummaryLast12Months_Account2 ON FactInvoiceLine_SummaryLast12Months_Account2
--(
--AccountKey,
--AccountManagerKey,
----ManagedByUserKey,
--AccountGroup1Key,
--AccountGroup2Key,
--AccountGroup3Key,
--InvoiceLineGroup1Key, 
--InvoiceLineGroup2Key, 
--InvoiceLineGroup3Key,
--Last12MonthsIndicator
--) ON [SECONDARY]


--CREATE INDEX I_FactInvoiceLine_SummaryLast12Months_Account2_1 ON FactInvoiceLine_SummaryLast12Months_Account2(AccountKey) ON [SECONDARY]
--CREATE INDEX I_FactInvoiceLine_SummaryLast12Months_Account2_6 ON FactInvoiceLine_SummaryLast12Months_Account2(AccountManagerKey) ON [SECONDARY]
----Create Index I_FactInvoiceLine_SummaryLast12Months_Account2_7 on FactInvoiceLine_SummaryLast12Months_Account2(ManagedByUserKey) ON [SECONDARY]
--CREATE INDEX I_FactInvoiceLine_SummaryLast12Months_Account2_3 ON FactInvoiceLine_SummaryLast12Months_Account2(AccountGroup1Key) ON [SECONDARY]
--CREATE INDEX I_FactInvoiceLine_SummaryLast12Months_Account2_4 ON FactInvoiceLine_SummaryLast12Months_Account2(AccountGroup2Key) ON [SECONDARY]
--CREATE INDEX I_FactInvoiceLine_SummaryLast12Months_Account2_5 ON FactInvoiceLine_SummaryLast12Months_Account2(AccountGroup3Key) ON [SECONDARY] CREATE INDEX I_FactInvoiceLine_SummaryLast12Months_Account2_8 ON FactInvoiceLine_SummaryLast12Months_Account2(InvoiceLineGroup1Key) ON [SECONDARY]
--CREATE INDEX I_FactInvoiceLine_SummaryLast12Months_Account2_9 ON FactInvoiceLine_SummaryLast12Months_Account2(InvoiceLineGroup2Key) ON [SECONDARY]
--CREATE INDEX I_FactInvoiceLine_SummaryLast12Months_Account2_10 ON FactInvoiceLine_SummaryLast12Months_Account2(InvoiceLineGroup3Key) ON [SECONDARY]


--CREATE INDEX I_FactInvoiceLine_SummaryLast12Months_Account2_20 ON FactInvoiceLine_SummaryLast12Months_Account2(TotalQuantity) ON [SECONDARY]
--CREATE INDEX I_FactInvoiceLine_SummaryLast12Months_Account2_21 ON FactInvoiceLine_SummaryLast12Months_Account2(TotalActualPrice) ON [SECONDARY]
--CREATE INDEX I_FactInvoiceLine_SummaryLast12Months_Account2_22 ON FactInvoiceLine_SummaryLast12Months_Account2(TotalActualCost) ON [SECONDARY]
--CREATE INDEX I_FactInvoiceLine_SummaryLast12Months_Account2_23 ON FactInvoiceLine_SummaryLast12Months_Account2(TotalActualRebate) ON [SECONDARY]

--SELECT COUNT(*) FactInvoiceLine_SummaryLast12Months_Account2 FROM FactInvoiceLine_SummaryLast12Months_Account2

---------------------------------------------------------
--SET @cmd = '
--CREATE VIEW dbo.FactInvoiceLine_SummaryLast12Months_Account3
--WITH SCHEMABINDING
--AS
--SELECT 
--AccountKey, 
--AccountGroup1Key,
--AccountGroup2Key,
--AccountGroup3Key,
--ItemGroup1Key,
--ItemGroup2Key,
--ItemGroup3Key,
--Last12MonthsIndicator,
--Sum(TotalQuantity) TotalQuantity, 
--Sum(TotalActualPrice) TotalActualPrice, 
--Sum(TotalActualCost) TotalActualCost, 
--Sum(TotalActualRebate) TotalActualRebate,
--Count_Big(*) CountBig
--FROM dbo.FactInvoiceLine
--Group By
--AccountKey, 
--AccountGroup1Key,
--AccountGroup2Key,
--AccountGroup3Key,
--ItemGroup1Key,
--ItemGroup2Key,
--ItemGroup3Key,
--Last12MonthsIndicator
--'

--EXEC sp_executesql @cmd

--CREATE UNIQUE CLUSTERED INDEX UI_FactInvoiceLine_SummaryLast12Months_Account3 ON FactInvoiceLine_SummaryLast12Months_Account3
--(
--AccountKey, 
--AccountGroup1Key,
--AccountGroup2Key,
--AccountGroup3Key,
--ItemGroup1Key,
--ItemGroup2Key,
--ItemGroup3Key,
--Last12MonthsIndicator
--) ON [SECONDARY]



--CREATE INDEX I_FactInvoiceLine_SummaryLast12Months_Account3_1 ON FactInvoiceLine_SummaryLast12Months_Account3(AccountKey) ON [SECONDARY]
--CREATE INDEX I_FactInvoiceLine_SummaryLast12Months_Account3_10 ON FactInvoiceLine_SummaryLast12Months_Account3(ItemGroup1Key) ON [SECONDARY]
--CREATE INDEX I_FactInvoiceLine_SummaryLast12Months_Account3_11 ON FactInvoiceLine_SummaryLast12Months_Account3(ItemGroup2Key) ON [SECONDARY]
--CREATE INDEX I_FactInvoiceLine_SummaryLast12Months_Account3_12 ON FactInvoiceLine_SummaryLast12Months_Account3(ItemGroup3Key) ON [SECONDARY]
--CREATE INDEX I_FactInvoiceLine_SummaryLast12Months_Account3_32 ON FactInvoiceLine_SummaryLast12Months_Account3(AccountGroup1Key) ON [SECONDARY]
--CREATE INDEX I_FactInvoiceLine_SummaryLast12Months_Account3_33 ON FactInvoiceLine_SummaryLast12Months_Account3(AccountGroup2Key) ON [SECONDARY]
--CREATE INDEX I_FactInvoiceLine_SummaryLast12Months_Account3_34 ON FactInvoiceLine_SummaryLast12Months_Account3(AccountGroup3Key) ON [SECONDARY]
--CREATE INDEX I_FactInvoiceLine_SummaryLast12Months_Account3_20 ON FactInvoiceLine_SummaryLast12Months_Account3(TotalQuantity) ON [SECONDARY]
--CREATE INDEX I_FactInvoiceLine_SummaryLast12Months_Account3_21 ON FactInvoiceLine_SummaryLast12Months_Account3(TotalActualPrice) ON [SECONDARY]
--CREATE INDEX I_FactInvoiceLine_SummaryLast12Months_Account3_22 ON FactInvoiceLine_SummaryLast12Months_Account3(TotalActualCost) ON [SECONDARY]
--CREATE INDEX I_FactInvoiceLine_SummaryLast12Months_Account3_23 ON FactInvoiceLine_SummaryLast12Months_Account3(TotalActualRebate) ON [SECONDARY]

--SELECT COUNT(*) FactInvoiceLine_SummaryLast12Months_Account3 FROM FactInvoiceLine_SummaryLast12Months_Account3
--------------------------------------------------------------------------------------------------------

----go
--SET @cmd = '
--CREATE VIEW dbo.FactInvoiceLine_SummaryLast12Months_AccountManager1
--WITH SCHEMABINDING
--AS
--SELECT 
--AccountManagerKey,
----ManagedByUserKey,
--Last12MonthsIndicator,
--Sum(TotalQuantity) TotalQuantity, 
--Sum(TotalActualPrice) TotalActualPrice, 
--Sum(TotalActualCost) TotalActualCost, 
--Sum(TotalActualRebate) TotalActualRebate,
--Count_Big(*) CountBig
--FROM dbo.FactInvoiceLine
--Group By
--AccountManagerKey,
----ManagedByUserKey,
--Last12MonthsIndicator
--'

--EXEC sp_executesql @cmd

--CREATE UNIQUE CLUSTERED INDEX UI_FactInvoiceLine_SummaryLast12Months_AccountManager1 ON FactInvoiceLine_SummaryLast12Months_AccountManager1
--(
--AccountManagerKey,
----ManagedByUserKey,
--Last12MonthsIndicator
--) ON [SECONDARY]


--CREATE INDEX I_FactInvoiceLine_SummaryLast12Months_AccountManager1_6 ON FactInvoiceLine_SummaryLast12Months_AccountManager1(AccountManagerKey) ON [SECONDARY]
----Create Index I_FactInvoiceLine_SummaryLast12Months_AccountManager1_7 on FactInvoiceLine_SummaryLast12Months_AccountManager1(ManagedByUserKey) ON [SECONDARY]
--CREATE INDEX I_FactInvoiceLine_SummaryLast12Months_AccountManager1_20 ON FactInvoiceLine_SummaryLast12Months_AccountManager1(TotalQuantity) ON [SECONDARY]
--CREATE INDEX I_FactInvoiceLine_SummaryLast12Months_AccountManager1_21 ON FactInvoiceLine_SummaryLast12Months_AccountManager1(TotalActualPrice) ON [SECONDARY]
--CREATE INDEX I_FactInvoiceLine_SummaryLast12Months_AccountManager1_22 ON FactInvoiceLine_SummaryLast12Months_AccountManager1(TotalActualCost) ON [SECONDARY]
--CREATE INDEX I_FactInvoiceLine_SummaryLast12Months_AccountManager1_23 ON FactInvoiceLine_SummaryLast12Months_AccountManager1(TotalActualRebate) ON [SECONDARY]

--SELECT COUNT(*) FactInvoiceLine_SummaryLast12Months_AccountManager1 FROM FactInvoiceLine_SummaryLast12Months_AccountManager1

---------------------------------------------------------
--SET @cmd = '
--CREATE VIEW dbo.FactInvoiceLine_SummaryLast12Months_AccountCount
--WITH SCHEMABINDING
--AS
--SELECT 
--AccountManagerKey,
--AccountKey, 
--Last12MonthsIndicator,
--Count_Big(*) CountBig
--FROM dbo.FactInvoiceLine
--Group By
--AccountManagerKey,
--AccountKey, 
--Last12MonthsIndicator
--'

--EXEC sp_executesql @cmd

--CREATE UNIQUE CLUSTERED INDEX UI_FactInvoiceLine_SummaryLast12Months_AccountCount ON FactInvoiceLine_SummaryLast12Months_AccountCount
--(
--AccountManagerKey,
--AccountKey, 
--Last12MonthsIndicator) ON [SECONDARY]


--CREATE INDEX I_FactInvoiceLine_SummaryLast12Months_AccountCount_1 ON FactInvoiceLine_SummaryLast12Months_AccountCount(AccountKey) ON [SECONDARY]
--SELECT COUNT(*) FactInvoiceLine_SummaryLast12Months_AccountCount FROM FactInvoiceLine_SummaryLast12Months_AccountCount
-------------------------------------------------------------------------------------------------------------

--SET @cmd = '
--CREATE VIEW dbo.FactInvoiceLine_SummaryLast12Months_ItemCount
--WITH SCHEMABINDING
--AS
--SELECT 
--AccountManagerKey,
--ItemKey, 
--Last12MonthsIndicator,
--Count_Big(*) CountBig
--FROM dbo.FactInvoiceLine
--Group By
--AccountManagerKey,
--ItemKey, 
--Last12MonthsIndicator
--'

--EXEC sp_executesql @cmd

--CREATE UNIQUE CLUSTERED INDEX UI_FactInvoiceLine_SummaryLast12Months_ItemCount ON FactInvoiceLine_SummaryLast12Months_ItemCount
--(
--AccountManagerKey,
--ItemKey, 
--Last12MonthsIndicator) ON [SECONDARY]


--CREATE INDEX I_FactInvoiceLine_SummaryLast12Months_ItemCount_1 ON FactInvoiceLine_SummaryLast12Months_ItemCount(ItemKey) ON [SECONDARY]
--SELECT COUNT(*) FactInvoiceLine_SummaryLast12Months_ItemCount FROM FactInvoiceLine_SummaryLast12Months_ItemCount

-------------------------------

--SET @cmd = '
--CREATE VIEW dbo.FactInvoiceLine_Vendor3
--WITH SCHEMABINDING
--AS
--SELECT 
----InvoiceLineKey, 
----AccountKey, 
----ItemKey, 
--VendorKey, 
----AccountManagerKey,
----ManagedByUserKey,
----InvoiceDateDayKey, 
--InvoiceDateMonthKey, 
----ClientUniqueIdentifier, 
----InvoiceLineJunkKey, 
--InvoiceLineGroup1Key, 
---- InvoiceLineGroup2Key, 
---- InvoiceLineGroup3Key, 
--AccountGroup1Key,
---- AccountGroup2Key,
---- AccountGroup3Key,
---- ItemGroup1Key,
---- ItemGroup2Key,
---- ItemGroup3Key,
---- Last12MonthsIndicator,
--Sum(TotalQuantity) TotalQuantity, 
--Sum(TotalActualPrice) TotalActualPrice, 
--Sum(TotalActualCost) TotalActualCost, 
----Sum(TotalActualRebate) TotalActualRebate,
--Count_Big(*) CountBig
--FROM dbo.FactInvoiceLine
--Group By
----InvoiceLineKey, 
----AccountKey, 
----ItemKey, 
--VendorKey, 
----AccountManagerKey,
----ManagedByUserKey,
----InvoiceDateDayKey, 
--InvoiceDateMonthKey,
----ClientUniqueIdentifier, 
----InvoiceLineJunkKey, 
--InvoiceLineGroup1Key, 
---- InvoiceLineGroup2Key, 
---- InvoiceLineGroup3Key,
--AccountGroup1Key
---- AccountGroup2Key,
---- AccountGroup3Key,
---- ItemGroup1Key,
---- ItemGroup2Key,
---- ItemGroup3Key,
---- Last12MonthsIndicator
--'

--EXEC sp_executesql @cmd

--CREATE UNIQUE CLUSTERED INDEX UI_FactInvoiceLine_Vendor3 ON FactInvoiceLine_Vendor3
--(
----InvoiceLineKey, 
----AccountKey, 
----ItemKey,
--VendorKey,
----AccountManagerKey,
----ManagedByUserKey,
----InvoiceDateDayKey, 
--InvoiceDateMonthKey,
----ClientUniqueIdentifier, 
----InvoiceLineJunkKey, 
--InvoiceLineGroup1Key,
----InvoiceLineGroup2Key, 
----InvoiceLineGroup3Key,
--AccountGroup1Key
----AccountGroup2Key,
----AccountGroup3Key,
----ItemGroup1Key,
----ItemGroup2Key,
----ItemGroup3Key,
----Last12MonthsIndicator
--) ON [SECONDARY]
----



----Create Index I_FactInvoiceLine_Vendor3_1 on FactInvoiceLine_Vendor3(AccountKey) ON [SECONDARY]
----Create Index I_FactInvoiceLine_Vendor3_2 on FactInvoiceLine_Vendor3(ItemKey) ON [SECONDARY]
--CREATE INDEX I_FactInvoiceLine_Vendor3_13 ON FactInvoiceLine_Vendor3(VendorKey) ON [SECONDARY]
----Create Index I_FactInvoiceLine_Vendor3_14 on FactInvoiceLine_Vendor3(AccountManagerKey) ON [SECONDARY]
----Create Index I_FactInvoiceLine_Vendor3_15 on FactInvoiceLine_Vendor3(ManagedByUserKey) ON [SECONDARY]
--CREATE INDEX I_FactInvoiceLine_Vendor3_3 ON FactInvoiceLine_Vendor3(InvoiceDateMonthKey) ON [SECONDARY]
--CREATE INDEX I_FactInvoiceLine_Vendor3_4 ON FactInvoiceLine_Vendor3(InvoiceLineGroup1Key) ON [SECONDARY]
---- Create Index I_FactInvoiceLine_Vendor3_5 on FactInvoiceLine_Vendor3(InvoiceLineGroup2Key) ON [SECONDARY]
---- Create Index I_FactInvoiceLine_Vendor3_6 on FactInvoiceLine_Vendor3(InvoiceLineGroup3Key) ON [SECONDARY]
-- CREATE INDEX I_FactInvoiceLine_Vendor3_7 ON FactInvoiceLine_Vendor3(AccountGroup1Key) ON [SECONDARY]
---- Create Index I_FactInvoiceLine_Vendor3_8 on FactInvoiceLine_Vendor3(AccountGroup2Key) ON [SECONDARY]
---- Create Index I_FactInvoiceLine_Vendor3_9 on FactInvoiceLine_Vendor3(AccountGroup3Key) ON [SECONDARY]
---- Create Index I_FactInvoiceLine_Vendor3_10 on FactInvoiceLine_Vendor3(ItemGroup1Key) ON [SECONDARY]
---- Create Index I_FactInvoiceLine_Vendor3_11 on FactInvoiceLine_Vendor3(ItemGroup2Key) ON [SECONDARY]
---- Create Index I_FactInvoiceLine_Vendor3_12 on FactInvoiceLine_Vendor3(ItemGroup3Key) ON [SECONDARY]
---- 
---- Create Index I_FactInvoiceLine_Vendor3_20 on FactInvoiceLine_Vendor3(TotalQuantity) ON [SECONDARY]
---- Create Index I_FactInvoiceLine_Vendor3_21 on FactInvoiceLine_Vendor3(TotalActualPrice) ON [SECONDARY]
---- Create Index I_FactInvoiceLine_Vendor3_22 on FactInvoiceLine_Vendor3(TotalActualCost) ON [SECONDARY]
---- Create Index I_FactInvoiceLine_Vendor3_23 on FactInvoiceLine_Vendor3(TotalActualRebate) ON [SECONDARY]


--SELECT COUNT(*) FactInvoiceLine_Vendor3 FROM FactInvoiceLine_Vendor3
-------------------------------------------------------------------------------------------------------------

-------------------------------
--SET @cmd = '
--CREATE VIEW dbo.FactInvoiceLine_Vendor1
--WITH SCHEMABINDING
--AS
--SELECT 
----InvoiceLineKey, 
----AccountKey, 
----ItemKey, 
--VendorKey, 
----AccountManagerKey,
----ManagedByUserKey,
----InvoiceDateDayKey, 
--InvoiceDateMonthKey, 
----ClientUniqueIdentifier, 
----InvoiceLineJunkKey, 
--InvoiceLineGroup1Key, 
---- InvoiceLineGroup2Key, 
---- InvoiceLineGroup3Key, 
----AccountGroup1Key,
---- AccountGroup2Key,
-- AccountGroup3Key,
---- ItemGroup1Key,
---- ItemGroup2Key,
---- ItemGroup3Key,
---- Last12MonthsIndicator,
--Sum(TotalQuantity) TotalQuantity, 
--Sum(TotalActualPrice) TotalActualPrice, 
--Sum(TotalActualCost) TotalActualCost, 
----Sum(TotalActualRebate) TotalActualRebate,
--Count_Big(*) CountBig
--FROM dbo.FactInvoiceLine
--Group By
----InvoiceLineKey, 
----AccountKey, 
----ItemKey, 
--VendorKey, 
----AccountManagerKey,
----ManagedByUserKey,
----InvoiceDateDayKey, 
--InvoiceDateMonthKey,
----ClientUniqueIdentifier, 
----InvoiceLineJunkKey, 
--InvoiceLineGroup1Key, 
---- InvoiceLineGroup2Key, 
---- InvoiceLineGroup3Key,
----AccountGroup1Key
---- AccountGroup2Key,
-- AccountGroup3Key
---- ItemGroup1Key,
---- ItemGroup2Key,
---- ItemGroup3Key,
---- Last12MonthsIndicator
--'

--EXEC sp_executesql @cmd

--CREATE UNIQUE CLUSTERED INDEX UI_FactInvoiceLine_Vendor1 ON FactInvoiceLine_Vendor1
--(
----InvoiceLineKey, 
----AccountKey, 
----ItemKey,
--VendorKey,
----AccountManagerKey,
----ManagedByUserKey,
----InvoiceDateDayKey, 
--InvoiceDateMonthKey,
----ClientUniqueIdentifier, 
----InvoiceLineJunkKey, 
--InvoiceLineGroup1Key,
----InvoiceLineGroup2Key, 
----InvoiceLineGroup3Key,
----AccountGroup1Key
----AccountGroup2Key,
--AccountGroup3Key
----ItemGroup1Key,
----ItemGroup2Key,
----ItemGroup3Key,
----Last12MonthsIndicator
--) ON [SECONDARY]
----



----Create Index I_FactInvoiceLine_Vendor1_1 on FactInvoiceLine_Vendor1(AccountKey) ON [SECONDARY]
----Create Index I_FactInvoiceLine_Vendor1_2 on FactInvoiceLine_Vendor1(ItemKey) ON [SECONDARY]
--CREATE INDEX I_FactInvoiceLine_Vendor1_13 ON FactInvoiceLine_Vendor1(VendorKey) ON [SECONDARY]
----Create Index I_FactInvoiceLine_Vendor1_14 on FactInvoiceLine_Vendor1(AccountManagerKey) ON [SECONDARY]
----Create Index I_FactInvoiceLine_Vendor1_15 on FactInvoiceLine_Vendor1(ManagedByUserKey) ON [SECONDARY]
--CREATE INDEX I_FactInvoiceLine_Vendor1_3 ON FactInvoiceLine_Vendor1(InvoiceDateMonthKey) ON [SECONDARY]
--CREATE INDEX I_FactInvoiceLine_Vendor1_4 ON FactInvoiceLine_Vendor1(InvoiceLineGroup1Key) ON [SECONDARY]
---- Create Index I_FactInvoiceLine_Vendor1_5 on FactInvoiceLine_Vendor1(InvoiceLineGroup2Key) ON [SECONDARY]
---- Create Index I_FactInvoiceLine_Vendor1_6 on FactInvoiceLine_Vendor1(InvoiceLineGroup3Key) ON [SECONDARY]
---- Create Index I_FactInvoiceLine_Vendor1_7 on FactInvoiceLine_Vendor1(AccountGroup1Key) ON [SECONDARY]
---- Create Index I_FactInvoiceLine_Vendor1_8 on FactInvoiceLine_Vendor1(AccountGroup2Key) ON [SECONDARY]
-- CREATE INDEX I_FactInvoiceLine_Vendor1_9 ON FactInvoiceLine_Vendor1(AccountGroup3Key) ON [SECONDARY]
---- Create Index I_FactInvoiceLine_Vendor1_10 on FactInvoiceLine_Vendor1(ItemGroup1Key) ON [SECONDARY]
---- Create Index I_FactInvoiceLine_Vendor1_11 on FactInvoiceLine_Vendor1(ItemGroup2Key) ON [SECONDARY]
---- Create Index I_FactInvoiceLine_Vendor1_12 on FactInvoiceLine_Vendor1(ItemGroup3Key) ON [SECONDARY]
---- 
---- Create Index I_FactInvoiceLine_Vendor1_20 on FactInvoiceLine_Vendor1(TotalQuantity) ON [SECONDARY]
---- Create Index I_FactInvoiceLine_Vendor1_21 on FactInvoiceLine_Vendor1(TotalActualPrice) ON [SECONDARY]
---- Create Index I_FactInvoiceLine_Vendor1_22 on FactInvoiceLine_Vendor1(TotalActualCost) ON [SECONDARY]
---- Create Index I_FactInvoiceLine_Vendor1_23 on FactInvoiceLine_Vendor1(TotalActualRebate) ON [SECONDARY]


--SELECT COUNT(*) FactInvoiceLine_Vendor1 FROM FactInvoiceLine_Vendor1

-------------------------------

--SET @cmd = '
--CREATE VIEW dbo.FactInvoiceLine_Vendor2
--WITH SCHEMABINDING
--AS
--SELECT 
----InvoiceLineKey, 
----AccountKey, 
----ItemKey, 
--VendorKey, 
----AccountManagerKey,
----ManagedByUserKey,
----InvoiceDateDayKey, 
--InvoiceDateMonthKey, 
----ClientUniqueIdentifier, 
----InvoiceLineJunkKey, 
--InvoiceLineGroup1Key, 
---- InvoiceLineGroup2Key, 
---- InvoiceLineGroup3Key, 
----AccountGroup1Key,
---- AccountGroup2Key,
---- AccountGroup3Key,
---- ItemGroup1Key,
-- ItemGroup2Key,
---- ItemGroup3Key,
---- Last12MonthsIndicator,
--Sum(TotalQuantity) TotalQuantity, 
--Sum(TotalActualPrice) TotalActualPrice, 
--Sum(TotalActualCost) TotalActualCost, 
----Sum(TotalActualRebate) TotalActualRebate,
--Count_Big(*) CountBig
--FROM dbo.FactInvoiceLine
--Group By
----InvoiceLineKey, 
----AccountKey, 
----ItemKey, 
--VendorKey, 
----AccountManagerKey,
----ManagedByUserKey,
----InvoiceDateDayKey, 
--InvoiceDateMonthKey,
----ClientUniqueIdentifier, 
----InvoiceLineJunkKey, 
--InvoiceLineGroup1Key, 
---- InvoiceLineGroup2Key, 
---- InvoiceLineGroup3Key,
----AccountGroup1Key
---- AccountGroup2Key,
---- AccountGroup3Key
---- ItemGroup1Key,
-- ItemGroup2Key
---- ItemGroup3Key,
---- Last12MonthsIndicator
--'

--EXEC sp_executesql @cmd

--CREATE UNIQUE CLUSTERED INDEX UI_FactInvoiceLine_Vendor2 ON FactInvoiceLine_Vendor2
--(
----InvoiceLineKey, 
----AccountKey, 
----ItemKey,
--VendorKey,
----AccountManagerKey,
----ManagedByUserKey,
----InvoiceDateDayKey, 
--InvoiceDateMonthKey,
----ClientUniqueIdentifier, 
----InvoiceLineJunkKey, 
--InvoiceLineGroup1Key,
----InvoiceLineGroup2Key, 
----InvoiceLineGroup3Key,
----AccountGroup1Key
----AccountGroup2Key,
----AccountGroup3Key
----ItemGroup1Key,
--ItemGroup2Key
----ItemGroup3Key,
----Last12MonthsIndicator
--) ON [SECONDARY]
----



----Create Index I_FactInvoiceLine_Vendor2_1 on FactInvoiceLine_Vendor2(AccountKey) ON [SECONDARY]
----Create Index I_FactInvoiceLine_Vendor2_2 on FactInvoiceLine_Vendor2(ItemKey) ON [SECONDARY]
--CREATE INDEX I_FactInvoiceLine_Vendor2_13 ON FactInvoiceLine_Vendor2(VendorKey) ON [SECONDARY]
----Create Index I_FactInvoiceLine_Vendor2_14 on FactInvoiceLine_Vendor2(AccountManagerKey) ON [SECONDARY]
----Create Index I_FactInvoiceLine_Vendor2_15 on FactInvoiceLine_Vendor2(ManagedByUserKey) ON [SECONDARY]
--CREATE INDEX I_FactInvoiceLine_Vendor2_3 ON FactInvoiceLine_Vendor2(InvoiceDateMonthKey) ON [SECONDARY]
--CREATE INDEX I_FactInvoiceLine_Vendor2_4 ON FactInvoiceLine_Vendor2(InvoiceLineGroup1Key) ON [SECONDARY]
---- Create Index I_FactInvoiceLine_Vendor2_5 on FactInvoiceLine_Vendor2(InvoiceLineGroup2Key) ON [SECONDARY]
---- Create Index I_FactInvoiceLine_Vendor2_6 on FactInvoiceLine_Vendor2(InvoiceLineGroup3Key) ON [SECONDARY]
---- Create Index I_FactInvoiceLine_Vendor2_7 on FactInvoiceLine_Vendor2(AccountGroup1Key) ON [SECONDARY]
---- Create Index I_FactInvoiceLine_Vendor2_8 on FactInvoiceLine_Vendor2(AccountGroup2Key) ON [SECONDARY]
---- Create Index I_FactInvoiceLine_Vendor2_9 on FactInvoiceLine_Vendor2(AccountGroup3Key) ON [SECONDARY]
---- Create Index I_FactInvoiceLine_Vendor2_10 on FactInvoiceLine_Vendor2(ItemGroup1Key) ON [SECONDARY]
-- CREATE INDEX I_FactInvoiceLine_Vendor2_11 ON FactInvoiceLine_Vendor2(ItemGroup2Key) ON [SECONDARY]
---- Create Index I_FactInvoiceLine_Vendor2_12 on FactInvoiceLine_Vendor2(ItemGroup3Key) ON [SECONDARY]
---- 
---- Create Index I_FactInvoiceLine_Vendor2_20 on FactInvoiceLine_Vendor2(TotalQuantity) ON [SECONDARY]
---- Create Index I_FactInvoiceLine_Vendor2_21 on FactInvoiceLine_Vendor2(TotalActualPrice) ON [SECONDARY]
---- Create Index I_FactInvoiceLine_Vendor2_22 on FactInvoiceLine_Vendor2(TotalActualCost) ON [SECONDARY]
---- Create Index I_FactInvoiceLine_Vendor2_23 on FactInvoiceLine_Vendor2(TotalActualRebate) ON [SECONDARY]

--SELECT COUNT(*) FactInvoiceLine_Vendor2 FROM FactInvoiceLine_Vendor2
-----------------------------------

--SET @cmd = '
--CREATE VIEW dbo.FactInvoiceLine_Vendor4
--WITH SCHEMABINDING
--AS
--SELECT 
--VendorKey, 
--InvoiceDateMonthKey, 
--ItemGroup3Key,
--Sum(TotalQuantity) TotalQuantity, 
--Sum(TotalActualPrice) TotalActualPrice, 
--Sum(TotalActualCost) TotalActualCost, 
--Count_Big(*) CountBig
--FROM dbo.FactInvoiceLine
--Group By
--VendorKey, 
--InvoiceDateMonthKey,
--ItemGroup3Key
--'

--EXEC sp_executesql @cmd

--CREATE UNIQUE CLUSTERED INDEX UI_FactInvoiceLine_Vendor4 ON FactInvoiceLine_Vendor4
--(
--VendorKey,
--InvoiceDateMonthKey,
--ItemGroup3Key
--) ON [SECONDARY]

--CREATE INDEX I_FactInvoiceLine_Vendor4_13 ON FactInvoiceLine_Vendor4(VendorKey) ON [SECONDARY]
--CREATE INDEX I_FactInvoiceLine_Vendor4_3 ON FactInvoiceLine_Vendor4(InvoiceDateMonthKey) ON [SECONDARY]
--CREATE INDEX I_FactInvoiceLine_Vendor4_12 ON FactInvoiceLine_Vendor4(ItemGroup3Key) ON [SECONDARY]

--SELECT COUNT(*) FactInvoiceLine_Vendor4 FROM FactInvoiceLine_Vendor4


-----------------------------------






EXEC LogDCPEvent 'ETL - dbo.MVIEWS_Create', 'E'




GO
