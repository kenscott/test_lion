SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [lion].[GetBoilerFieldView] @AccountOwnerType nvarchar(255), @AccountOwner nvarchar(255), @HD95Only bit = 1, @Region nvarchar(255)
as



SELECT [RowType]
      ,CAST(NEWID() as nvarchar(255)) AS RecID
      ,[AccountKey]
      ,[ItemKey]
      ,[InvoiceLineGroup1Key]
      ,[Area]
      ,[LLSPG Description]
      ,[Product Description]
      ,[Account Owner]
      ,[Branch]
      ,[AccountNumber]
      ,[AccountName]
      ,[Account Sales]
      ,[LLSPG]
      ,[Product Code]
      ,[Total Quantity]
      ,[Product Sales]
      ,[Fixed Price Indicator]
      ,[Prod KVI]
      ,[KVI Override]
      ,[Default Discount]
      ,[Last Terms Change]
      ,[SPG Disc 1]
      ,[SPG Disc 2]
      ,[Exception Discount]
      ,[Last Item Discount]
      ,[Exception Fixed Price]
      ,[Fixed Price Start Date]
      ,[Fixed Price Expiry Date]
      ,[Last Sale Date]
      ,[Last Price Derivation]
      ,[Last Item Price]
      ,[Adjusted Last Item Price]
      ,[Recommended Discount]
      ,[Recommended Price]
      ,[Recommended Impact] as [RecImpact]
	  ,[New SPG Discount 1]
      ,[New SPG Discount 2]
      ,[New Exception Fixed Price]
      ,[New Exception Discount]
      ,[New Exception Price Expiry Date]
      ,[Expected Adjustment Discount]
      ,[Brand]
      ,[Discount @ Green]
      ,[Discount @ Amber]
      ,[Discount @ Red]
      ,[Trade Price]
  FROM [lion].[BoilerTerms]
 WHERE [Account Owner Type] = @AccountOwnerType and 
       [Account Owner] LIKE @AccountOwner and
	   ((@HD95Only = 1 and [LLSPG] = 'HD95') OR (@HD95Only = 0 and [LLSPG] <> 'HD95')) and
	   LEFT(Area, 2) = @Region
order by [ID]
GO
