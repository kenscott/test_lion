SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




CREATE
PROCEDURE [dbo].[DBA_FixOrphanUsers]
AS

/*
This code was taken from http://www.sqlservercentral.com/scripts/contributions/1300.asp and
was written by sanjayattray (sanjayattray) 90640
Description: To map all your database users with master..syslogins. save this procedure in current database and execute it. All users will be mapped with logins.
*/

SET NOCOUNT ON

DECLARE @username NVARCHAR(256)

DECLARE fixusers CURSOR
FOR
SELECT UserName = Name FROM sysusers
WHERE 
	issqluser = 1 
	AND (sid IS NOT NULL AND sid <> 0x0)
	AND SUSER_SNAME(sid) IS NULL
	AND name NOT IN ('dbo', 'guest')
ORDER BY Name

OPEN fixusers

FETCH NEXT FROM fixusers INTO @username

WHILE @@FETCH_STATUS = 0
BEGIN
	PRINT 'UserName: ' + @username

	EXEC sp_change_users_login 'update_one', @username, @username

	FETCH NEXT FROM fixusers INTO @username
END

CLOSE fixusers
DEALLOCATE fixusers





GO
