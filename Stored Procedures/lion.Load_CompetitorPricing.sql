SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [lion].[Load_CompetitorPricing]
WITH EXECUTE AS CALLER
AS
/*

EXEC lion.Load_CompetitorPricing

*/


SET ANSI_WARNINGS OFF
SET ARITHABORT OFF
SET ARITHIGNORE ON

SET NOCOUNT ON

EXEC LogDCPEvent 'ETL - Load_CompetitorPricing', 'B'


DECLARE 
	@RowsI Quantity_Normal_type, 
	@RowsU Quantity_Normal_type, 
	@RowsD Quantity_Normal_type

SET @RowsI = 0
SET @RowsU = 0
SET @RowsD = 0


TRUNCATE TABLE lion.CompetitorPricing

; WITH StagingData AS (
	SELECT DISTINCT
		di.ItemKey,
		Retailer,
		YourSKU AS SKU,
		CAST(Price AS MONEY) AS Price,
		ROW_NUMBER() OVER (PARTITION BY 
			di.ItemKey,
			Retailer,
			YourSKU		
		ORDER BY 
			di.ItemKey,
			Retailer,
			YourSKU		
		) AS RowRank
	FROM 
		Lion_Staging.dbo.[360PI] p
	JOIN dbo.DimItem di
		ON di.ItemNumber = N'1' + p.YourSKU
	WHERE
		p.Retailer <> 'Wolseley'
)
INSERT lion.CompetitorPricing (
	ItemKey,
	Retailer,
	SKU,
	Price
)
SELECT
	ItemKey,
	Retailer,
	SKU,
	Price
FROM StagingData
WHERE RowRank = 1
SET @RowsI = @RowsI + @@RowCount


SELECT TOP 10 * FROM lion.CompetitorPricing


EXEC LogDCPEvent 'ETL - Load_CompetitorPricing', 'E', @RowsI, @RowsU, @RowsD

















GO
