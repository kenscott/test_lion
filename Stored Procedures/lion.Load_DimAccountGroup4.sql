SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [lion].[Load_DimAccountGroup4]
WITH EXECUTE AS CALLER
AS
EXEC LogDCPEvent 'ETL - DimAccountGroup4', 'B'

EXEC dba_indexrebuild 'DimAccountGroup4', 'Drop'


--This table will contain a row for every row in the dimension table that has been changed in the staging table (type 1 update will be done)
CREATE TABLE #UpdateKeys (KeyID INTEGER)

DECLARE @RowsI Quantity_Normal_type, @RowsU Quantity_Normal_type, @RowsD Quantity_Normal_type
SET @RowsI = 0
SET @RowsU = 0
SET @RowsD = 0

--This is used to look up the Day key in the DimDay table
DECLARE @TodayDayKey Key_Small_type
SET @TodayDayKey = (SELECT DayKey FROM DimDay WHERE CONVERT(VARCHAR(10), SQLDate, 120) = CONVERT(VARCHAR(10), GETDATE(), 120))

--Truncate table DimAccountGroup4

IF NOT EXISTS (SELECT 1 FROM DimAccountGroup4 WHERE AG4Level1 = 'Unknown')
BEGIN
	INSERT DimAccountGroup4	(
		AG4Level1, 
		AG4Level2, 
		AG4Level3, 
		AG4Level4, 
		AG4Level5,
		AG4Level1UDVarchar1, 
		CreationDayKey, 
		ModificationDayKey)
	VALUES (
		'Unknown', 
		'Unknown', 
		'Unknown', 
		'Unknown', 
		'Unknown', 
		'Unknown', 
		@TodayDayKey, 
		@TodayDayKey)
	SET @RowsI = @RowsI + @@RowCount
END


--INSERT DimAccountGroup4 (
--	AG4Level1, 
--	AG4Level2, 
--	AG4Level3, 
--	AG4Level4, 
--	AG4Level5, 
--	CreationDayKey, 
--	ModificationDayKey)
--SELECT DISTINCT 
--	Region, 
--	'None', 
--	'None', 
--	'None', 
--	'None', 
--	@TodayDayKey, 
--	@TodayDayKey
--FROM
--	(SELECT DISTINCT 
--		Region AS Region
--	FROM Lion_Staging.dbo.BusUnit
--	) StagingAG4
--LEFT JOIN DimAccountGroup4 ON 
--	DimAccountGroup4.AG4Level1 = StagingAG4.Region
--WHERE DimAccountGroup4.AG4Level1 IS NULL
--SET @RowsI = @RowsI + @@RowCount




SELECT TOP 10 * FROM DimAccountGroup4

EXEC dba_indexrebuild 'DimAccountGroup4', 'Create'

EXEC LogDCPEvent 'ETL - DimAccountGroup4', 'E', @RowsI, @RowsU, @RowsD




GO
