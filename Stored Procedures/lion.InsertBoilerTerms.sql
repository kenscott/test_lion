SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE [lion].[InsertBoilerTerms] 
            @RowType nvarchar(255) = null
           ,@AccountKey int = null
           ,@ItemKey int = null
           ,@InvoiceLineGroup1Key int = null
           ,@AccountOwnerType nvarchar(255) = null 
           ,@AccountOwner nvarchar(255) = null
           ,@Branch nvarchar(255) = null
           ,@AccountNumber nvarchar(255) = null
           ,@AccountName nvarchar(255) = null
           ,@Area nvarchar(255) = null
           ,@LLSPGDescription nvarchar(255) = null
           ,@ProductDescription nvarchar(255) = null
           ,@LLSPG nvarchar(255) = null
           ,@ProductCode nvarchar(255) = null
           ,@ProdKVI nvarchar(255) = null
           ,@KVIOverride nvarchar(255) = null
           ,@DefaultDiscount decimal(19,8) = null
           ,@LastTermsChange nvarchar(255) = null
           ,@SPGDisc1 decimal(19,8) = null
           ,@SPGDisc2 decimal(19,8) = null
           ,@ExceptionDiscount decimal(19,8) = null
           ,@ExceptionFixedPrice nvarchar(255) = null
           ,@FixedPriceStartDate nvarchar(255) = null
           ,@FixedPriceExpiryDate nvarchar(255) = null
		   ,@LastItemDiscount decimal(19,8) = null
           ,@TotalQuantity decimal(19,8) = null
           ,@ProductSales decimal(19,8) = null
           ,@AccountSales decimal(19,8) = null
		   ,@FixedPriceIndicator nvarchar(255) = null
           ,@LastSaleDate nvarchar(255) = null
           ,@NewSPGDiscount1 decimal(19,8) = null
           ,@NewSPGDiscount2 decimal(19,8) = null
           ,@NewExceptionFixedPrice decimal(19,8) = null
           ,@NewExceptionDiscount decimal(19,8) = null
           ,@NewExceptionPriceExpiryDate nvarchar(255) = null
           ,@ExpectedAdjustmentDiscount decimal(19,8) = null
           ,@LastPriceDerivation nvarchar(255) = null
           ,@LastItemPrice decimal(19,8) = null
           ,@AdjustedLastItemPrice decimal(19,8) = null
           ,@RecommendedDiscount decimal(19,8) = null
           ,@RecommendedPrice decimal(19,8) = null
           ,@RecommendedImpact decimal(19,8) = null
           ,@AcceptedImpact decimal(19,8) = null
           ,@Brand nvarchar(255) = null
           ,@DiscountGreen decimal(19,8) = null
           ,@DiscountAmber decimal(19,8) = null
           ,@DiscountRed decimal(19,8) = null
           ,@TradePrice decimal(19,8) = null
AS

SET NOCOUNT ON
SET ANSI_WARNINGS OFF 

if @RowType <> ''
begin
	INSERT INTO [lion].[BoilerTerms]
           ([RowType]
           ,[AccountKey]
           ,[ItemKey]
           ,[InvoiceLineGroup1Key]
           ,[Account Owner Type]
           ,[Account Owner]
           ,[Branch]
           ,[AccountNumber]
           ,[AccountName]
           ,[Area]
           ,[LLSPG Description]
           ,[Product Description]
           ,[LLSPG]
           ,[Product Code]
           ,[Prod KVI]
           ,[KVI Override]
           ,[Default Discount]
           ,[Last Terms Change]
           ,[SPG Disc 1]
           ,[SPG Disc 2]
           ,[Exception Discount]
           ,[Exception Fixed Price]
           ,[Fixed Price Start Date]
           ,[Fixed Price Expiry Date]
		   ,[Last Item Discount]
           ,[Total Quantity]
           ,[Product Sales]
           ,[Account Sales]
		   ,[Fixed Price Indicator]
           ,[Last Sale Date]
           ,[New SPG Discount 1]
           ,[New SPG Discount 2]
           ,[New Exception Fixed Price]
           ,[New Exception Discount]
           ,[New Exception Price Expiry Date]
           ,[Expected Adjustment Discount]
           ,[Last Price Derivation]
           ,[Last Item Price]
           ,[Adjusted Last Item Price]
           ,[Recommended Discount]
           ,[Recommended Price]
           ,[Recommended Impact]
           ,[Accepted Impact]
           ,[Brand]
           ,[Discount @ Green]
           ,[Discount @ Amber]
           ,[Discount @ Red]
           ,[Trade Price])
		 VALUES
			   (
			    IIF(@RowType='Null',null,@RowType)
			   ,@AccountKey
			   ,@ItemKey 
			   ,@InvoiceLineGroup1Key 
			   ,@AccountOwnerType
			   ,@AccountOwner
			   ,@Branch
			   ,IIF(@AccountNumber ='Null',null,@AccountNumber )
			   ,IIF(@AccountName ='Null',null,@AccountName )
			   ,IIF(@Area ='Null',null,@Area )
			   ,IIF(@LLSPGDescription ='Null',null,@LLSPGDescription )
			   ,IIF(@ProductDescription='Null',null,@ProductDescription)
			   ,IIF(@LLSPG ='Null',null,@LLSPG )
			   ,IIF(@ProductCode ='Null',null,@ProductCode )
			   ,IIF(@ProdKVI ='Null',null,@ProdKVI )
			   ,IIF(@KVIOverride ='Null',null,@KVIOverride )
			   ,@DefaultDiscount 
			   ,IIF(@LastTermsChange ='Null',null,convert(datetime,@LastTermsChange) )
			   ,@SPGDisc1 
			   ,@SPGDisc2 
			   ,@ExceptionDiscount 
			   ,IIF(@ExceptionFixedPrice ='Null',null,@ExceptionFixedPrice )
			   ,IIF(@FixedPriceStartDate ='Null',null,@FixedPriceStartDate )
			   ,IIF(@FixedPriceExpiryDate ='Null',null,@FixedPriceExpiryDate )
			   ,@LastItemDiscount
			   ,@TotalQuantity 
			   ,@ProductSales 
			   ,@AccountSales 
			   ,@FixedPriceIndicator
			   ,IIF(@LastSaleDate ='Null',null,convert(datetime,@LastSaleDate) ) 
			   ,@NewSPGDiscount1 
			   ,@NewSPGDiscount2 
			   ,@NewExceptionFixedPrice 
			   ,@NewExceptionDiscount 
			   ,IIF(@NewExceptionPriceExpiryDate ='Null',null,convert(datetime,@NewExceptionPriceExpiryDate) )
			   ,@ExpectedAdjustmentDiscount
			   ,@LastPriceDerivation
			   ,@LastItemPrice
			   ,@AdjustedLastItemPrice
			   ,@RecommendedDiscount
			   ,@RecommendedPrice
			   ,@RecommendedImpact
			   ,@AcceptedImpact 
			   ,IIF(@Brand ='Null',null,@Brand )
			   ,@DiscountGreen
			   ,@DiscountAmber
			   ,@DiscountRed
			   ,@TradePrice)

end

SELECT @@ROWCOUNT as NumRowsAffected



GO
