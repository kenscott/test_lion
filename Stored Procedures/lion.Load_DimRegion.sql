SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [lion].[Load_DimRegion]
WITH EXECUTE AS CALLER
AS
SET NOCOUNT ON


EXEC LogDCPEvent 'ETL - Load_DimRegion', 'B'


DECLARE 
	@RowsI INT, 
	@RowsU INT, 
	@RowsD INT,
	@BeginRowCount INT,
	@TodayDayKey Key_Small_type

SET @RowsI = 0
SET @RowsU = 0
SET @RowsD = 0

SET @TodayDayKey = (SELECT DayKey FROM DimDay WHERE CONVERT(VARCHAR(10), SQLDate, 120) = CONVERT(VARCHAR(10), GETDATE(), 120))


 --EXEC DBA_IndexRebuild 'DimRegion', 'Drop'
 --TRUNCATE TABLE dbo.DimRegion
 --EXEC DBA_IndexRebuild 'DimRegion', 'Create'
 --EXEC lion.Load_DimRegion

IF NOT EXISTS (SELECT 1 FROM dbo.DimRegion WHERE Region = 'Unknown')
BEGIN
	INSERT dbo.DimRegion (
		Region,
		RegionName,
		CreationDayKey, 
		ModificationDayKey)
	VALUES (
		'Unknown',
		'Unknown', 
		@TodayDayKey, 
		@TodayDayKey)
	SET @RowsI = @RowsI + @@RowCount
END


SELECT @BeginRowCount = COUNT(*) FROM dbo.DimRegion

; WITH StagingData AS (
	SELECT DISTINCT 
		b.Region AS Region, 
		u.BranchName AS RegionName
	FROM Lion_Staging.dbo.BusUnit b
	INNER JOIN Lion_Staging.dbo.BusUnit u
	ON b.Region = u.BranchCode and u.BusUnitTypeDesc = 'WOLCEN_REGION'
	WHERE b.Region <> ''
	)

MERGE dbo.DimRegion AS Tgt
USING 
	(
	SELECT 
		Region,
		RegionName
	FROM StagingData
) AS Src
ON Tgt.Region = Src.Region
WHEN MATCHED AND Tgt.RegionName <> Src.RegionName
THEN UPDATE 
	SET
		Tgt.ModificationDayKey = @TodayDayKey,
		Tgt.RegionName = Src.RegionName
WHEN NOT MATCHED 
	THEN INSERT (
		Region,
		RegionName,
		CreationDayKey, 
		ModificationDayKey
	)
	VALUES (
		Region,
		RegionName,
		@TodayDayKey, 
		@TodayDayKey
	)
;

SELECT 
	@RowsU = @RowsU + (@@ROWCOUNT - (COUNT(*) - @BeginRowCount)),
	@RowsI = @RowsI + (COUNT(*) - @BeginRowCount)
FROM dbo.DimRegion


SELECT TOP 10 * FROM dbo.DimRegion

EXEC LogDCPEvent 'ETL - Load_DimRegion', 'E', @RowsI, @RowsU, @RowsD









GO
