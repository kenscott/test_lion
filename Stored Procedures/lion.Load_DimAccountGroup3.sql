SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





CREATE PROCEDURE [lion].[Load_DimAccountGroup3]
AS

EXEC LogDCPEvent 'ETL - DimAccountGroup3', 'B'

--EXEC dba_indexrebuild 'DimAccountGroup3', 'Drop'


--This table will contain a row for every row in the dimension table that has been changed in the staging table (type 1 update will be done)
CREATE TABLE #UpdateKeys (KeyID INTEGER)

DECLARE @RowsI Quantity_Normal_type, @RowsU Quantity_Normal_type, @RowsD Quantity_Normal_type
SET @RowsI = 0
SET @RowsU = 0
SET @RowsD = 0

--This is used to look up the Day key in the DimDay table
DECLARE @TodayDayKey Key_Small_type
SET @TodayDayKey = (SELECT DayKey FROM DimDay WHERE CONVERT(VARCHAR(10), SQLDate, 120) = CONVERT(VARCHAR(10), GETDATE(), 120))

--TRUNCATE TABLE DimAccountGroup3

IF NOT EXISTS (SELECT 1 FROM DimAccountGroup3 WHERE AG3Level1 = 'Unknown')
BEGIN
	INSERT DimAccountGroup3	(
		AG3Level1, 
		AG3Level2, 
		AG3Level3, 
		AG3Level4, 
		AG3Level5,
		AG3Level1UDVarchar1, 
		CreationDayKey, 
		ModificationDayKey)
	VALUES (
		'Unknown', 
		'Unknown', 
		'Unknown', 
		'Unknown', 
		'Unknown', 
		'Unknown Trading Status Category',
		@TodayDayKey, 
		@TodayDayKey)
	SET @RowsI = @RowsI + @@RowCount
END


---- first some basic cleanup
--UPDATE dbo.DimAccountGroup3 SET	AG3Level1UDVarchar1 = 'Unknown' WHERE AG3Level1 = 'Unknown'
--SET @RowsU = @@RowCount + @RowsU


INSERT DimAccountGroup3 (
	AG3Level1, 
	AG3Level2, 
	AG3Level3, 
	AG3Level4, 
	AG3Level5,
	AG3Level1UDVarchar1,
	CreationDayKey, 
	ModificationDayKey)
SELECT DISTINCT 
	TrandingStatusCategory,--1
	'None',--2
	'None', 
	'None', 
	'None',
	TrandingStatusCategoryDescription AS AG3Level1UDVarchar1,
	@TodayDayKey, 
	@TodayDayKey
FROM 
	(SELECT '' AS TrandingStatusCategory, 'OK TO TRADE' AS TrandingStatusCategoryDescription
	UNION SELECT '0', 'OK TO TRADE 0'
	UNION SELECT '1', 'OK TO TRADE 1'
	UNION SELECT '3', 'WARNING NEAR CREDIT LIMIT'
	UNION SELECT '4', 'WARNING BREACHED CREDIT'
	UNION SELECT '6', 'HOLD CREDIT LIMIT'
	UNION SELECT '2', 'DUNNING WARNING'
	UNION SELECT '5', 'HOLD DUNNING'
	UNION SELECT '9', 'MANUAL CREDIT LIMIT HOLD'
	UNION SELECT '8', 'MANUAL HOLD 1'
	UNION SELECT '10', 'MANUAL HOLD 2'
	UNION SELECT '7', 'LEGAL' ) Staging
WHERE Staging.TrandingStatusCategory NOT IN (SELECT AG3Level1 FROM DimAccountGroup3)	--INSERTs all new dimension rows
SET @RowsI = @RowsI + @@RowCount


SELECT TOP 10 * FROM DimAccountGroup3

--EXEC dba_indexrebuild 'DimAccountGroup3', 'Create'

EXEC LogDCPEvent 'ETL - DimAccountGroup3', 'E', @RowsI, @RowsU, @RowsD




GO
