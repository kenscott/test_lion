SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO







-------------------------------------

CREATE          
PROCEDURE [dbo].[RBP_PopulatePlaybookDataPointGroupScope]
@PlaybookDataPointGroupKey INT, @LimitDataPlaybookKey INT
AS
SET QUOTED_IDENTIFIER ON 
SET ANSI_NULLS ON 
SET ARITHABORT ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
SET CONCAT_NULL_YIELDS_NULL ON
SET NUMERIC_ROUNDABORT OFF

DECLARE @USql NVARCHAR(MAX)
DECLARE @AccountExclusionIndicator NVARCHAR(50)
DECLARE 
	@RowsI Quantity_Normal_type, 
	@RowsU Quantity_Normal_type, 
	@RowsD Quantity_Normal_type, 
	@WhereClause  NVARCHAR(MAX), 
	@SQLStmt NVARCHAR(MAX), 
	@FieldList NVARCHAR(MAX)
DECLARE @JoinClause  NVARCHAR(MAX), @HavingClause  NVARCHAR(MAX)
DECLARE @ProjectKey Key_Normal_Type
SET @ProjectKey = (SELECT ProjectKey FROM PlaybookDataPointGroup WHERE PlaybookDataPointGroupKey = @PlaybookDataPointGroupKey)

--Y if we should use this table to exclude anything
SELECT @AccountExclusionIndicator = AccountExclusionIndicator 
FROM PlaybookProject PP
JOIN ATKProjectSpecification PS ON PS.ProjectSpecificationKey = PP.ProjectSpecificationKey


SELECT @WhereClause = ISNULL(WhereClause, ''),
	   @JoinClause = ISNULL(JoinClause, ''),
	   @HavingClause = ISNULL(HavingClause, '')
FROM ATKLimitDataPlaybook 
WHERE LimitDataPlaybookKey = @LimitDataPlaybookKey

IF @WhereClause <> '' AND @WhereClause NOT LIKE 'and %'
	SET @WhereClause = 'and ' + @WhereClause
IF @HavingClause <> '' AND @HavingClause NOT LIKE 'and %'
	SET @HavingClause = 'and ' + @HavingClause

--If we should exclude accounts
IF @AccountExclusionIndicator = 'Y'
BEGIN
	--Inserts all accounts that have data point exclusions
	INSERT INTO PlaybookProjectAccountExclusion(ProjectKey, AccountKey, WebUserKey, ExclusionType, ExclusionReason, Version, CreationDate, ModificationDate, CreationUser, ModificationUser, ModificationUserAddress, CreationUserAddress)
	SELECT @ProjectKey, AccountKey, WebUserKey, ExclusionType, ExclusionReason, Version, CreationDate, ModificationDate, CreationUser, ModificationUser, ModificationUserAddress, CreationUserAddress FROM ATKAccountExclusion
	WHERE ExclusionType = 'DataPoint'

	SET @USql = 'Update Statistics PlaybookProjectAccountExclusion'
	EXEC (@USql)

	--Now, modify the dynamic SQL to use these exclusions:
	SET @JoinClause = @JoinClause + '
	join (Select AccountKey from PlaybookProjectAccountExclusion where ProjectKey = '+CAST(@ProjectKey AS NVARCHAR(50))+') AccountExclusions
		on AccountExclusions.AccountKey = FactInvoiceLine.AccountKey
	'

	--Add a line feed and "And" if necessary
	IF @WhereClause NOT LIKE '%and%'
		SET @WhereClause = @WhereClause + '
	and'


	SET @WhereClause = @WhereClause + ' Where AccountExclusions.AccountKey is NULL --Exclude anything found in the exclusion table
	'
END

EXEC LogDCPEvent 'DCP --- RBP_PopulatePlaybookDataPointGroupScope: Delete Old Data Points', 'B',0,0,0,@ProjectKey
SET @RowsI = 0
SET @RowsU = 0
SET @RowsD = 0

--First, cleanup the old data points
DELETE FROM PlaybookPricingGroupPriceBand WHERE PlaybookPricingGroupKey IN (SELECT PlaybookPricingGroupKey FROM PlaybookPricingGroupQuantityBand_View WHERE PlaybookDataPointGroupKey = @PlaybookDataPointGroupKey)
SET @RowsD = @RowsD + @@RowCount
DELETE FROM ProposalPrice WHERE ProposalPriceKey IN (SELECT ProposalPriceKey FROM ProposalPrice WHERE PlaybookDataPointGroupKey = @PlaybookDataPointGroupKey)
SET @RowsD = @RowsD + @@RowCount
DELETE FROM ProposalCost WHERE ProposalCostKey IN (SELECT ProposalCostKey FROM ProposalCost_View WHERE PlaybookDataPointGroupKey = @PlaybookDataPointGroupKey)
SET @RowsD = @RowsD + @@RowCount

DELETE FROM PlaybookPriceProposal WHERE PlaybookDataPointGroupKey = @PlaybookDataPointGroupKey
SET @RowsD = @RowsD + @@RowCount
DELETE FROM PlaybookDataPointOptimalPrice WHERE PlaybookDataPointOptimalPriceKey IN (SELECT PlaybookDataPointOptimalPriceKey FROM PlaybookDataPointOptimalPrice_View WHERE PlaybookDataPointGroupKey = @PlaybookDataPointGroupKey)
SET @RowsD = @RowsD + @@RowCount
DELETE FROM PlaybookPricingGroupOptimalPrice WHERE PlaybookPricingGroupOptimalPriceKey IN (SELECT PlaybookPricingGroupOptimalPriceKey FROM PlaybookPricingGroupOptimalPrice_View WHERE PlaybookDataPointGroupKey = @PlaybookDataPointGroupKey)
SET @RowsD = @RowsD + @@RowCount
DELETE FROM PlaybookPricingGroupQuantityBand WHERE PlaybookPricingGroupQuantityBandKey IN (SELECT PlaybookPricingGroupQuantityBandKey FROM PlaybookPricingGroupQuantityBand_View WHERE PlaybookDataPointGroupKey = @PlaybookDataPointGroupKey)
SET @RowsD = @RowsD + @@RowCount
DELETE FROM PlaybookDatapointPricingGroup WHERE PlaybookDatapointPricingGroupKey IN (SELECT PlaybookDatapointPricingGroupKey FROM PlaybookDatapointPricingGroup_View WHERE PlaybookDataPointGroupKey = @PlaybookDataPointGroupKey)
SET @RowsD = @RowsD + @@RowCount
DELETE FROM PlaybookPricingGroupQuantityBand WHERE PlaybookPricingGroupKey IN (SELECT PlaybookPricingGroupKey FROM PlaybookPricingGroupQuantityBand_View WHERE PlaybookDataPointGroupKey = @PlaybookDataPointGroupKey)
SET @RowsD = @RowsD + @@RowCount
DELETE FROM PlaybookPricingGroup WHERE PlaybookDataPointGroupKey = @PlaybookDataPointGroupKey
SET @RowsD = @RowsD + @@RowCount

DELETE FROM PlaybookDataPoint WHERE PlaybookDataPointGroupKey = @PlaybookDataPointGroupKey
SET @RowsD = @RowsD + @@RowCount

DELETE FROM PlaybookDataPointGroupAccountItemBands WHERE PlaybookDataPointGroupKey = @PlaybookDataPointGroupKey
SET @RowsD = @RowsD + @@RowCount
DELETE FROM PlaybookDataPointGroupFullScopeCoreNonCore WHERE PlaybookDataPointGroupKey = @PlaybookDataPointGroupKey
SET @RowsD = @RowsD + @@RowCount
DELETE FROM PlaybookDataPointGroupFullScopeAccountBands WHERE PlaybookDataPointGroupKey = @PlaybookDataPointGroupKey
SET @RowsD = @RowsD + @@RowCount
DELETE FROM PlaybookDataPointGroupItemBands WHERE PlaybookDataPointGroupKey = @PlaybookDataPointGroupKey
SET @RowsD = @RowsD + @@RowCount
DELETE FROM PlaybookDataPointGroupScenarioScopeAccountBands WHERE PlaybookDataPointGroupKey = @PlaybookDataPointGroupKey
SET @RowsD = @RowsD + @@RowCount

DELETE FROM PlaybookProjectAccountStatus WHERE ProjectKey = @ProjectKey
SET @RowsD = @RowsD + @@RowCount
DELETE FROM PlaybookProjectAccountManagerStatus WHERE ProjectKey = @ProjectKey
SET @RowsD = @RowsD + @@RowCount

DELETE FROM PlaybookDataPointGroupScope WHERE PlaybookDataPointGroupKey = @PlaybookDataPointGroupKey
SET @RowsD = @RowsD + @@RowCount

DELETE FROM PlaybookProjectAccountExclusion WHERE ProjectKey = @ProjectKey AND ExclusionType = 'DataPoint'
SET @RowsD = @RowsD + @@RowCount

EXEC LogDCPEvent 'DCP --- RBP_PopulatePlaybookDataPointGroupScope: Delete Old Data Points', 'E', @RowsI, @RowsU, @RowsD

-------------------------------------------------------------------------------------

--SELECT '@JoinClause: ', @JoinClause
--SELECT '***'
--
--SELECT 'WHERE: ', @WhereClause
--SELECT '***'
--
--SELECT '@HavingClause: ', @HavingClause
--SELECT '***'




EXEC LogDCPEvent 'DCP --- RBP_PopulatePlaybookDataPointGroupScope: Insert PlaybookDataPointGroupScope', 'B',0,0,0,@ProjectKey
SET @RowsI = 0
SET @RowsU = 0
SET @RowsD = 0


SET @SQLStmt = '
Insert Into PlaybookDataPointGroupScope(
PlaybookDataPointGroupKey,
AccountKey, 
ItemKey,
InvoiceLineGroup1Key, 
Total12MonthQuantity,
TotalActual12MonthPrice,
TotalActual12MonthCost,
Total12MonthInvoiceLineCount)
Select 
	@PlaybookDataPointGroupKey as ''PlaybookDataPointGroupKey'',
	FactInvoiceLine.AccountKey, 
	FactInvoiceLine.ItemKey, 
	FactInvoiceLine.InvoiceLineGroup1Key, 
	Sum(TotalQuantity) as ''TotalQuantity'',
	Sum(TotalActualPrice) as ''TotalActualPrice'',
	Sum(TotalActualCost) as ''TotalActualCost'',
	Count(*) as Total12MonthInvoiceLineCount
FROM dbo.FactInvoiceLine
' + @JoinClause+'
Where Last12MonthsIndicator = ''Y''
' + @WhereClause + '
Group by FactInvoiceLine.AccountKey, FactInvoiceLine.ItemKey, FactInvoiceLine.InvoiceLineGroup1Key
Having Sum(TotalActualPrice) <> 0
and Sum(TotalActualCost) <> 0
and Sum(TotalQuantity) <> 0 
' + @HavingClause+'

Set @RowsI = @RowsI + @@RowCount'

SET @FieldList = N'@PlaybookDataPointGroupKey INT, @LimitDataPlaybookKey INT, @RowsI INT OUT'

PRINT @SqlStmt

EXEC SP_EXECUTESQL @SQLStmt, @FieldList, @PlaybookDataPointGroupKey, @LimitDataPlaybookKey, @RowsI OUT

SET @USql = 'Update Statistics PlaybookDataPointGroupScope'
EXEC (@USql)


EXEC LogDCPEvent 'DCP --- RBP_PopulatePlaybookDataPointGroupScope: Insert PlaybookDataPointGroupScope', 'E', @RowsI, @RowsU, @RowsD












GO
