SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO










CREATE PROCEDURE [lion].[Load_PlaybookPricingGroupPriceBandActive]
AS

SET NOCOUNT ON
SET QUOTED_IDENTIFIER ON 
SET ANSI_NULLS ON 
SET ARITHABORT ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
SET CONCAT_NULL_YIELDS_NULL ON
SET NUMERIC_ROUNDABORT OFF

EXEC LogDCPEvent 'ETL - lion.Load_PlaybookPricingGroupPriceBandActive', 'B'

DECLARE 
	@RowsI Quantity_Normal_type, 
	@RowsU Quantity_Normal_type, 
	@RowsD Quantity_Normal_type
	
	
SET @RowsI = 0
SET @RowsU = 0
SET @RowsD = 0



EXEC LogDCPEvent 'ETL - lion.Load_PlaybookPricingGroupPriceBandActive', 'B'

TRUNCATE TABLE lion.PlaybookPricingGroupPriceBandActive

INSERT lion.PlaybookPricingGroupPriceBandActive (
	PlaybookPricingGroupKey,
	--GroupValues,
	--GroupingColumns,
	LowerBandQuantity,
	UpperBandQuantity,
	MemberRank,
	RulingMemberCount,
	PricingRuleSequenceNumber,
	PDPGPP,
	PDPDiscountPercent
)
SELECT DISTINCT
	ppgpb.PlaybookPricingGroupKey,
	--GroupValues,
	--GroupingColumns,
	LowerBandQuantity,
	UpperBandQuantity,
	MemberRank,
	RulingMemberCount,
	PricingRuleSequenceNumber,
	PDPGPP,
	PDPDiscountPercent
FROM dbo.PlaybookPricingGroupPriceBand ppgpb
INNER JOIN dbo.PlaybookPricingGroup ppg (NOLOCK)
	ON ppg.PlaybookPricingGroupKey = ppgpb.PlaybookPricingGroupKey
WHERE EXISTS (SELECT 1 FROM FactLastPriceAndCost l WHERE l.RPBPlaybookPricingGroupKey = ppgpb.PlaybookPricingGroupKey)
SET @RowsI = @@RowCount

---- since lion does not currently have any quantity bands (band sequence number), we can change the unique clustered index on the temp table
--CREATE UNIQUE CLUSTERED INDEX iuc1 ON lion.PlaybookPricingGroupPriceBandActive (PlaybookPricingGroupKey, MemberRank)
--CREATE UNIQUE INDEX i1 ON lion.PlaybookPricingGroupPriceBandActive (PlaybookPricingGroupKey, LowerBandQuantity, UpperBandQuantity, MemberRank)

EXEC LogDCPEvent 'ETL - lion.Load_PlaybookPricingGroupPriceBandActive', 'E', @RowsI, @RowsU, @RowsD
GO
