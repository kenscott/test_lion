SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO






--DBA_RemainingFKRebuild 'Y'

CREATE PROCEDURE [dbo].[DBA_RemainingFKRebuild] (@Test NVARCHAR(1) = NULL)
AS
SET nocount ON
DECLARE @Command NVARCHAR(50), @IndexName NVARCHAR(500), @IndexDesc NVARCHAR(500), @Cols NVARCHAR(500), @ClusteredIndicator CHAR(1), @UniqueIndicator CHAR(1), @PI INT, @FileGroupName NVARCHAR(500), @PKIndicator CHAR(1), @cmd NVARCHAR(500), @DBAID INT, @UniqueText NVARCHAR(50)
DECLARE @ChildTable NVARCHAR(500), @ChildColumn NVARCHAR(500), @PrimaryTable NVARCHAR(500), @PrimaryColumn NVARCHAR(500), @KeyNo INT, @AllChildCols NVARCHAR(500), @AllPrimaryCols NVARCHAR(500), @ClusteredText NVARCHAR(50)
DECLARE @FK NVARCHAR(500)
DECLARE
	@ChildSchemaName NVARCHAR(256),
	@PrimarySchemaName NVARCHAR(256)
	
SET @Command = 'Create'

--Clean up any FKs that were already created and somehow got left in this table
DELETE f
FROM DBA_FK_Rebuild f
JOIN (
	SELECT 
		SysObjects.Name, 
		SCHEMA_NAME(sysObjects.uid) AS SchemaName
	FROM SysObjects
) o 
ON o.Name = f.FKName
AND o.SchemaName = f.ChildSchemaName

DECLARE FKInsert CURSOR FOR
SELECT FKName,ChildSchemaName,ChildTableName,PrimarySchemaName,PrimaryTableName,ChildCols,PrimaryCols
FROM DBA_FK_Rebuild
OPEN FKInsert
FETCH NEXT FROM FKInsert INTO @FK, @ChildSchemaName,@ChildTable,@PrimarySchemaName,@PrimaryTable,@AllChildCols,@AllPrimaryCols
WHILE @@fetch_status = 0
BEGIN
	SET @Cmd = 'Alter table ['+@ChildSchemaName+'].['+@ChildTable+'] add constraint ['+@FK+'] Foreign Key ('+ @AllChildCols+') references ['+@PrimarySchemaName+'].['+@PrimaryTable+']('+@AllPrimaryCols+')'
	PRINT @Cmd	

	IF @Test IS NULL
	BEGIN	
		INSERT DBA_IndexRebuild_Log (TableName,Command,Statement,BeginTime,EndTime)
		VALUES ('['+@ChildSchemaName+'].['+@ChildTable+']', @Command, @cmd, GETDATE(), NULL)
		SET @DBAID = @@identity

		EXEC (@cmd)

		UPDATE DBA_IndexRebuild_Log SET EndTime = GETDATE() WHERE DBAID = @DBAID
		DELETE FROM DBA_FK_Rebuild WHERE FKName = @FK AND ChildTableName = @ChildTable AND ChildSchemaName = @ChildSchemaName
	END
	FETCH NEXT FROM FKInsert INTO @FK, @ChildSchemaName,@ChildTable,@PrimarySchemaName,@PrimaryTable,@AllChildCols,@AllPrimaryCols
END
CLOSE FKInsert
DEALLOCATE FKInsert


GO
