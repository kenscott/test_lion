SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [lion].[GetTermsResults]
AS 

DECLARE @StartDate DATETIME
SET @StartDate = '29-DEC-2014'

truncate table lion.CentralTerms
truncate table lion.CentralTermsArchive

INSERT INTO [lion].[CentralTerms]
           ([RowType]
           ,[AccountKey]
           ,[LLSPG]
           ,[ItemKey]
           ,[InvoiceLineGroup1Key]
           ,[Prod KVI]
           ,[KVI Override]
           ,[Default Discount]
           ,[OrigSPGDisc1]
           ,[OrigSPGDisc2]
           ,[RecSPGDisc1]
           ,[RecSPGDisc2]
           ,[AccSPGDisc1]
           ,[AccSPGDisc2]
           ,[OrigExceptionDisc]
           ,[RecExceptionDisc]
           ,[AccExceptionDisc]
           ,[OrigFixedPrice]
           ,[RecExceptionFixedPrice]
           ,[AccExceptionFixedPrice]
           ,[Stuck]
           ,[ProjectedImpact]
           ,[EffectiveDate]
		   ,[Returned])
 select o.RowType, o.AccountKey, o.LLSPG, o.ItemKey, o.InvoiceLineGroup1Key, o.[Prod KVI], o.[KVI Override], o.[Default Discount],
        o.[SPG Disc 1] as [OrigSPGDisc1], o.[SPG Disc 2] as [OrigSPGDisc2], 
	    o.[New SPG Discount 1] as [RecSPGDisc1], o.[New SPG Discount 2] as [RecSPGDisc2], 
		i.[New SPG Discount 1] as [AccSPGDisc1], i.[New SPG Discount 2] as [AccSPGDisc2], 
		o.[Exception Discount] as [OrigExceptionDisc],
		o.[New Exception Discount] as [RecExceptionDisc],
		i.[New Exception Discount] as [AccExceptionDisc], 
		CASE WHEN RTRIM(o.[Exception Fixed Price]) = '' THEN NULL ELSE o.[Exception Fixed Price] END as [OrigFixedPrice],
		o.[New Exception Fixed Price] as [RecExceptionFixedPrice],
		i.[New Exception Fixed Price] as [AccExceptionFixedPrice],
		CONVERT(bit, 0) as [Stuck],
		CONVERT(decimal(19,8),0) as [ProjectedImpact],
		@StartDate as [EffectiveDate],
		CASE WHEN i.AccountKey IS NOT NULL THEN 1 ELSE 0 END AS [Returned]
   from lion.OutgoingTerms o left outer join
        lion.IncomingTerms i on o.AccountKey = i.AccountKey and
		                        o.LLSPG = i.LLSPG and
								ISNULL(o.ItemKey,0) = ISNULL(i.ItemKey,0)
order by o.ID, i.ID

UPDATE ct
SET ct.active = IIF(k.keep = ct.id, 1, 0)
  FROM lion.CentralTerms ct
INNER JOIN
(
  select RowType, AccountKey, LLSPG, ItemKey, MAX(id) as keep
  from lion.CentralTerms
  group by RowType, AccountKey, LLSPG, ItemKey
  having count(*) >1
) k
ON ct.RowType = k.RowType and
   ct.AccountKey = k.AccountKey and
   ct.LLSPG = k.LLSPG and 
   isnull(ct.ItemKey,0) = ISNULL(k.ItemKey, 0)

set identity_insert lion.CentralTermsArchive on

INSERT INTO [lion].[CentralTermsArchive]
           ([ID]
		   ,[RowType]
           ,[OutgoingRecID]
           ,[IncomingRecID]
           ,[AccountKey]
           ,[LLSPG]
           ,[ItemKey]
           ,[InvoiceLineGroup1Key]
           ,[Prod KVI]
           ,[KVI Override]
           ,[Default Discount]
           ,[OrigSPGDisc1]
           ,[OrigSPGDisc2]
           ,[RecSPGDisc1]
           ,[RecSPGDisc2]
           ,[AccSPGDisc1]
           ,[AccSPGDisc2]
           ,[OrigExceptionDisc]
           ,[RecExceptionDisc]
           ,[AccExceptionDisc]
           ,[OrigFixedPrice]
           ,[RecExceptionFixedPrice]
           ,[AccExceptionFixedPrice]
           ,[ProjectedImpact]
           ,[EffectiveDate]
           ,[RecImpact]
           ,[AccImpact]
           ,[Active])
select [ID]
		   ,[RowType]
           ,[OutgoingRecID]
           ,[IncomingRecID]
           ,[AccountKey]
           ,[LLSPG]
           ,[ItemKey]
           ,[InvoiceLineGroup1Key]
           ,[Prod KVI]
           ,[KVI Override]
           ,[Default Discount]
           ,[OrigSPGDisc1]
           ,[OrigSPGDisc2]
           ,[RecSPGDisc1]
           ,[RecSPGDisc2]
           ,[AccSPGDisc1]
           ,[AccSPGDisc2]
           ,[OrigExceptionDisc]
           ,[RecExceptionDisc]
           ,[AccExceptionDisc]
           ,[OrigFixedPrice]
           ,[RecExceptionFixedPrice]
           ,[AccExceptionFixedPrice]
           ,[ProjectedImpact]
           ,[EffectiveDate]
           ,[RecImpact]
           ,[AccImpact]
           ,[Active]
 from lion.CentralTerms where Active = 0

set identity_insert lion.CentralTermsArchive off

delete from lion.CentralTerms where Active = 0

; with SPGTerms as (
select * 
  from lion.CentralTerms 
 where RowType = 'LLSPG' and
       Returned = 1
)

update ct
   set ct.AccExceptionDisc = st.AccCmpDisc
  from lion.CentralTerms ct inner join
       SPGTerms st on ct.AccountKey = st.AccountKey and
	                  ct.LLSPG = st.LLSPG
where ct.AccExceptionDisc is null and 
      ct.RowType = 'Detail' and
	  ct.Returned = 1

update ct
   set ct.CustomerTermsKey=t.CustomerTermsKey
  from lion.CentralTerms ct inner join
	   lion.CustomerTerms t on ct.AccountKey = t.AccountKey and
	                           ((ct.ItemKey = (select ItemKey from lion.vwItem where ItemNumber = '1' + t.ExceptionProduct) and
								  t.PyramidCode = 1 and
							      ct.RowType = 'Detail'
								) or 
								(ct.LLSPG = t.SPGCode and 
							     ct.RowType = 'LLSPG' and
							     t.PyramidCode = 1
								)
							   )
 where ct.Returned = 1							   

 /*
select *
  from lion.CustomerTerms t left outer join
       lion.vwItem i on i.ItemNumber = '1' + t.ExceptionProduct inner join 
       lion.CentralTerms ct on ct.AccountKey = t.AccountKey and
	                           ct.ItemKey = i.ItemKey and
							   ct.RowType = 'Detail' and
							   t.PyramidCode = 1
 where ct.Returned = 1 and ct.CustomerTermsKey is null
 						   



select  count(*)-- ct.RowType,ct.AccountKey, t.AccountKey, ct.LLSPG, t.SPGCode, ct.ItemKey, t.ExceptionProduct, ct.RecSPGDisc1, ct.AccSPGDisc1, ct.Accepted, ct.AccCmpDisc, t.Usualdiscount1, t.Usualdiscount2, ct.AccExceptionDisc, t.ExceptionDiscount, ct.RecCmpDisc, ct.RecExceptionDisc, ct.Accepted, ct.Stuck
  from lion.CentralTerms ct --inner join
       --lion.CustomerTerms t on ct.CustomerTermsKey = t.CustomerTermsKey
 where accepted = 1 and CustomerTErmsKey is null

select  *-- ct.RowType,ct.AccountKey, t.AccountKey, ct.LLSPG, t.SPGCode, ct.ItemKey, t.ExceptionProduct, ct.RecSPGDisc1, ct.AccSPGDisc1, ct.Accepted, ct.AccCmpDisc, t.Usualdiscount1, t.Usualdiscount2, ct.AccExceptionDisc, t.ExceptionDiscount, ct.RecCmpDisc, ct.RecExceptionDisc, ct.Accepted, ct.Stuck
  from lion.CentralTerms ct --inner join
       --lion.CustomerTerms t on ct.CustomerTermsKey = t.CustomerTermsKey
 where accepted = 1 and CustomerTErmsKey is null


 
select  aCCcMPDisc, [Default Discount], *-- ct.RowType,ct.AccountKey, t.AccountKey, ct.LLSPG, t.SPGCode, ct.ItemKey, t.ExceptionProduct, ct.RecSPGDisc1, ct.AccSPGDisc1, ct.Accepted, ct.AccCmpDisc, t.Usualdiscount1, t.Usualdiscount2, ct.AccExceptionDisc, t.ExceptionDiscount, ct.RecCmpDisc, ct.RecExceptionDisc, ct.Accepted, ct.Stuck
  from lion.CentralTerms ct --inner join
       --lion.CustomerTerms t on ct.CustomerTermsKey = t.CustomerTermsKey
 where returned = 1 and CustomerTErmsKey is null and rowtype='LLSPG' and stuck=0

 select * from lion.CustomerTerms
 where AccountKey = 188727
 and SPGCode = 'PS34'

 */

 
update ct
   set ct.CustomerTermsKey=t.CustomerTermsKey
  from lion.CentralTerms ct inner join
	   lion.CustomerTerms t on ct.AccountKey = t.AccountKey and
							   ct.LLSPG = t.SPGCode and
							   ct.RowType = 'Detail' and
							   t.PyramidCode = 1
 where ct.CustomerTermsKey is null and ct.Returned = 1							   

update ct
   set ct.Stuck = 1,
       ct.EffectiveDate = @StartDate
  from lion.CentralTerms ct
 where ct.CustomerTermsKey is null and ct.Returned = 1 and Stuck = 0 and
       (RowType='LLSPG' and (AccCmpDisc is null or isnull(round(AccCmpDisc, 4), 0) = isnull(round([Default Discount], 4),0)))

update ct
   set ct.Stuck = 1,
       ct.EffectiveDate = CASE WHEN t.LastChangeDate >= @StartDate THEN t.LastChangeDate ELSE @StartDate END
  from lion.CentralTerms ct inner join
	   lion.CustomerTerms t on ct.CustomerTermsKey = t.CustomerTermsKey
 where ct.RowType = 'Detail' and ct.Returned = 1 and ISNULL(round(ct.AccExceptionDisc * 100,2),0) = ISNULL(CAST(t.ExceptionDiscount as decimal(19,8)), ISNULL(CAST(t.Usualdiscount1 as decimal(19,8)), 0) + ((1 - (ISNULL(CAST(t.UsualDiscount1 as decimal(19,8)), 0))) * ISNULL(CAST(t.UsualDiscount2 as decimal(19,8)), 0)))

update ct
   set ct.Stuck = 1,
       ct.EffectiveDate = CASE WHEN t.LastChangeDate >= @StartDate THEN t.LastChangeDate ELSE @StartDate END
  from lion.CentralTerms ct inner join
	   lion.CustomerTerms t on ct.CustomerTermsKey = t.CustomerTermsKey
 where ct.RowType = 'LLSPG' and ct.Returned = 1 and
       ISNULL(round(ct.AccSPGDisc1 * 100, 2),0) = ISNULL(t.Usualdiscount1,0) and
       ISNULL(round(ct.AccSPGDisc2 * 100, 2),0) = ISNULL(t.Usualdiscount2,0)

;with SPGTerms as (
 select * 
   from lion.CentralTerms 
  where RowType = 'LLSPG' and
        Returned = 1
 )

 update ct
   set ct.Stuck = st.Stuck,
       ct.EffectiveDate = st.EffectiveDate
   from lion.CentralTerms ct inner join
       SPGTerms st on ct.AccountKey = st.AccountKey and
	                  ct.LLSPG = st.LLSPG
 where ct.CustomerTermsKey is null and ct.Returned = 1 and ct.Stuck = 0 and
       (ct.RowType='Detail' and (ct.AccExceptionDisc is null or isnull(round(ct.AccExceptionDisc, 4), 0) = isnull(round(st.AccCmpDisc, 4),0)))


-- Missing Terms
-- select * from lion.CentralTerms where returned=1 and CustomerTermsKey is null and stuck = 0

-- Different Terms
-- select RowType,  AccCmpDisc, Usualdiscount1, Usualdiscount2, AccExceptionDisc, ExceptionDiscount, * from lion.CentralTerms ct inner join lion.CustomerTErms t on ct.CustomerTermsKey = t.CustomerTermsKey where returned=1 and stuck=0

;with LastDiscount as (
SELECT * FROM (
SELECT
	FIL.AccountKey, 
	FIL.ItemKey, 
	FIL.InvoiceLineGroup1Key,	
    ROW_NUMBER() OVER (Partition by FIL.AccountKey, FIL.ItemKey, FIL.InvoiceLineGroup1Key
	                   order by 	FIL.AccountKey, 
									FIL.ItemKey, 
									FIL.InvoiceLineGroup1Key, 
									FIL.InvoiceDateDayKey DESC, 
									FIL.TotalQuantity DESC, 
									FIL.TotalActualPrice DESC,
									FIL.ClientInvoiceID DESC,
									FIL.InvoiceLineUniqueIdentifier DESC) as RowNumber,
	fil.InvoiceLineUniqueIdentifier as LastInvoiceLineUniqueIdentifier,
	dd.SQLDate as LastItemDiscountInvoiceDate,
	fil.UDVarchar1 as LastItemDiscountPriceDerivation,
	ISNULL(	1 - ((fil.TotalActualPrice / NULLIF(fil.TotalQuantity, 0)) / NULLIF(fil.UnitListprice, 0.)), 0.) AS LastItemDiscountPercent
FROM dbo.FactInvoiceLine FIL INNER JOIN
     dbo.DimDay dd on fil.InvoiceDateDayKey = dd.DayKey INNER JOIN
	 lion.vwItem i on fil.ItemKey = i.ItemKey INNER JOIN
     lion.CentralTerms ct on fil.AccountKey = ct.AccountKey and fil.InvoiceLineGroup1Key = ct.InvoiceLineGroup1Key
	                      and dd.SQLDate < ct.EffectiveDate
WHERE 
 	TotalQuantity > 0.0 AND 
 	TotalActualPrice > 0.0 AND 
 	TotalActualCost > 0.0 AND 
	(ISNULL(TotalActualPrice, 0.0) - ISNULL(TotalActualCost, 0.0))/NULLIF(TotalActualPrice, 0) between -1.2 and 0.90
) a
where RowNumber = 1 )


, TotalSales as (
SELECT 
	fil.AccountKey, 
	fil.ItemKey, 
	SUM(TotalActualPrice) AS Total12MonthSales, 
	SUM(TotalActualCost) AS Total12MonthCost, 
	SUM(TotalQuantity) AS Total12MonthQuantity
FROM dbo.FactInvoiceLine fil inner join
     dbo.DimDay dd on fil.InvoiceDateDayKey = dd.DayKey inner join
	 lion.vwItem item on fil.ItemKey = item.ItemKey inner join
     lion.CentralTerms ct on fil.AccountKey = ct.AccountKey and
							 ((ct.ItemKey is not null and fil.ItemKey = ct.ItemKey) or 
	                          (ct.ItemKey is null and item.LLSPGCode = ct.LLSPG ) and
							  not exists (select *
										    from lion.CentralTerms ict
										   where ict.AccountKey = fil.AccountKey and
												 ict.ItemKey = fil.ItemKey)) and
							 ((ct.ItemKey is not null and fil.InvoiceLineGroup1Key = ct.InvoiceLineGroup1Key) or 
	                          (ct.ItemKey is null and fil.InvoiceLineGroup1Key = 4)  and
							  not exists (select *
										    from lion.CentralTerms ict
										   where ict.AccountKey = fil.AccountKey and
												 ict.ItemKey = fil.ItemKey))
WHERE dd.SQLDate between dateadd(dd, 1, dateadd(yy, -1, ct.EffectiveDate)) and ct.EffectiveDate and
      fil.InvoiceLineGroup1Key = 4
GROUP BY 
	fil.AccountKey, 
	fil.ItemKey
)

update ct
   set ct.RecImpact = (flpac.Total12MonthQuantity * item.CurrBranchTradePrice * (1 - case when ct.ItemKey is null then ct.RecCmpDisc 
																						  else ct.RecExceptionDisc end)) -
                      (flpac.Total12MonthQuantity * item.CurrBranchTradePrice * (1 - ld.LastItemDiscountPercent)),
	   ct.AccImpact = (flpac.Total12MonthQuantity * item.CurrBranchTradePrice * (1 - case when ct.ItemKey is null then ct.AccCmpDisc
																						  else ct.AccExceptionDisc end)) -
                      (flpac.Total12MonthQuantity * item.CurrBranchTradePrice * (1 - ld.LastItemDiscountPercent))
 from TotalSales flpac inner join
	  LastDiscount ld on flpac.AccountKey = ld.AccountKey and flpac.ItemKey = ld.ItemKey inner join
	  lion.vwItem item on item.ItemKey = flpac.ItemKey inner join
      lion.CentralTerms ct on flpac.AccountKey = ct.AccountKey and ((ct.ItemKey is not null and flpac.ItemKey = ct.ItemKey) or 
	                                                                (ct.ItemKey is null and item.LLSPGCode = ct.LLSPG and
																	 not exists (select *
																				   from lion.CentralTerms ict
																				  where ict.AccountKey = flpac.AccountKey and
																				        ict.ItemKey = flpac.ItemKey)))
delete from lion.TermsResults

;with PriorSales as (
SELECT 
	fil.AccountKey, 
	fil.ItemKey,
	SUM(TotalActualPrice) AS Total12MonthSales, 
	SUM(TotalActualCost) AS Total12MonthCost, 
	SUM(TotalQuantity) AS Total12MonthQuantity
FROM dbo.FactInvoiceLine fil inner join
     dbo.DimDay dd on fil.InvoiceDateDayKey = dd.DayKey inner join
	 lion.vwItem item on fil.ItemKey = item.ItemKey inner join
     lion.CentralTerms ct on fil.AccountKey = ct.AccountKey and
							 ((ct.ItemKey is not null and fil.ItemKey = ct.ItemKey) or 
	                          (ct.ItemKey is null and item.LLSPGCode = ct.LLSPG ) and
							  not exists (select *
										    from lion.CentralTerms ict
										   where ict.AccountKey = fil.AccountKey and
												 ict.ItemKey = fil.ItemKey))
WHERE dd.SQLDate between dateadd(dd, 1, dateadd(yy, -1, ct.EffectiveDate)) and ct.EffectiveDate and
      fil.InvoiceLineGroup1Key = 4
GROUP BY 
	fil.AccountKey, 
	fil.ItemKey
)

 ,ProcessResults as (
 select acct.Branch,
	   COUNT(*) - SUM(1 * Returned) as [CountPending],
	   SUM(1 * Returned) as [CountReturned],
       SUM(Accepted) as [CountAccepted],
	   CAST(SUM(Accepted) as decimal(19,8))/NULLIF(SUM(1 * Returned), 0) AS [PercentAccepted],
	   SUM(RecImpact * Returned) as [ProposedImpactAmount],
	   SUM(AccImpact * Returned * Accepted) as [AcceptedImpactAmount],
	   SUM(AccImpact * Returned * Accepted)/NULLIF(SUM(RecImpact),0) AS [PercentAmountAccepted],
	   SUM(CAST(Stuck as decimal(19,8)) * Returned) AS [CountStuck],
	   SUM(CAST(Stuck as decimal(19,8)) * Returned)/NULLIF(SUM(1 * Returned), 0) AS [PercentStuck],
	   SUM(AccImpact * Returned) as [ProjectedImpact]
  from lion.CentralTerms ct inner join
       lion.vwAccount acct on ct.AccountKey = acct.AccountKey
group by acct.Branch)

, LastDiscount as (
SELECT * FROM (
SELECT
	FIL.AccountKey, 
	FIL.ItemKey, 
	FIL.InvoiceLineGroup1Key,	
    ROW_NUMBER() OVER (Partition by FIL.AccountKey, FIL.ItemKey, FIL.InvoiceLineGroup1Key
	                   order by 	FIL.AccountKey, 
									FIL.ItemKey, 
									FIL.InvoiceLineGroup1Key, 
									FIL.InvoiceDateDayKey DESC, 
									FIL.TotalQuantity DESC, 
									FIL.TotalActualPrice DESC,
									FIL.ClientInvoiceID DESC,
									FIL.InvoiceLineUniqueIdentifier DESC) as RowNumber,
	fil.InvoiceLineUniqueIdentifier as LastInvoiceLineUniqueIdentifier,
	dd.SQLDate as LastItemDiscountInvoiceDate,
	fil.UDVarchar1 as LastItemDiscountPriceDerivation,
	ISNULL(	1 - ((fil.TotalActualPrice / NULLIF(fil.TotalQuantity, 0)) / NULLIF(fil.UnitListprice, 0.)), 0.) AS LastItemDiscountPercent
FROM dbo.FactInvoiceLine FIL INNER JOIN
     dbo.DimDay dd on fil.InvoiceDateDayKey = dd.DayKey INNER JOIN
	 lion.vwItem i on fil.ItemKey = i.ItemKey INNER JOIN
     lion.CentralTerms ct on fil.AccountKey = ct.AccountKey and fil.InvoiceLineGroup1Key = ct.InvoiceLineGroup1Key
	                      and dd.SQLDate < ct.EffectiveDate
WHERE 
 	TotalQuantity > 0.0 AND 
 	TotalActualPrice > 0.0 AND 
 	TotalActualCost > 0.0 AND 
	(ISNULL(TotalActualPrice, 0.0) - ISNULL(TotalActualCost, 0.0))/NULLIF(TotalActualPrice, 0) between -1.2 and 0.90
) a
where RowNumber = 1 )


,GrossMarginImpact as (
select acct.Branch, 
       sum(filpbs.TotalActualPrice - (filpbs.TotalQuantity * (item.CurrBranchTradePrice *  (1 - ld.LastItemDiscountPercent)))) as ActualImpact
  from lion.vwFactInvoiceLinePricebandScore filpbs inner join
       dbo.DimInvoiceLineJunk dilj on filpbs.InvoiceLineJunkKey = dilj.InvoiceLineJunkKey inner join
	   LastDiscount ld on filpbs.AccountKey = ld.AccountKey and filpbs.ItemKey = ld.ItemKey and filpbs.InvoiceLineGroup1Key = ld.InvoiceLineGroup1Key inner join
	   lion.vwAccount acct on filpbs.AccountKey = acct.AccountKey inner join
	   lion.vwAccountGroup1 ag1 on filpbs.AccountGroup1Key = ag1.AccountGroup1Key inner join
       lion.vwItem item on filpbs.ItemKey = item.ItemKey inner join
	   lion.vwFactInvoiceLineGroup1 ilg1 on filpbs.InvoiceLineGroup1Key = ilg1.InvoiceLineGroup1Key inner join
       dbo.DimDay dd on filpbs.InvoiceDateDayKey = dd.DayKey inner join 
	   lion.CentralTerms ct on filpbs.AccountKey = ct.AccountKey and
	                           ((ct.RowType = 'Detail' and filpbs.ItemKey = ct.ItemKey) or
							    (ct.RowType='LLSPG' and item.LLSPGCode = ct.LLSPG and 
								  not exists (select *
								                from lion.CentralTerms ict
											   where ict.AccountKey = filpbs.AccountKey and ict.ItemKey = filpbs.ItemKey)
								)
						       ) and
							   filpbs.InvoiceLineGroup1Key = coalesce(ct.InvoiceLineGroup1Key, 4) left outer join
	   lion.PilotSchedule ps on ps.Branch = acct.Branch left outer join
	   lion.CustomerTerms t on ct.AccountKey = t.AccountKey and
	                           ((((ct.ItemKey = item.itemKey and
							     item.ItemNumber = '1' + t.ExceptionProduct) and
								 t.PyramidCode = 1
							     ) and
							     ct.RowType = 'Detail') or 
								(ct.LLSPG = t.SPGCode and 
							     ct.RowType = 'LLSPG' and
							     t.PyramidCode = 1))
 where dd.SQLDate >= ct.EffectiveDate AND
       item.ItemPyramidCode = 1  AND 
	   acct.AccountName <> 'Unknown' AND 
	   item.ItemNumber <> 'Unknown' AND 
	   item.ItemObsolete <> 'Y' AND 
	   item.SpecialInd <> 'Y' AND 
	   ilg1.DeviatedIndicator <> 'Y' AND 
	   ilg1.ShipmentType <> 'D' AND 
	   filpbs.TotalQuantity <> 0 AND 
	   filpbs.TotalActualPrice <> 0 AND 
	   filpbs.PriceDerivation not in ('SPO', 'CCP', 'DP', 'DC') AND 
	   acct.BranchPrimaryBrand <> 'BURDN' AND
	   ct.Returned = 1
  group by acct.Branch
 )

 , SalesImpact as (
select acct.Branch,
       -- Total Sales Impact = (Projected Qty * New terms price) - (Prior Qty * Prior Price)
       sum(ISNULL(flpac.Total12MonthQuantity, 0) * 
	    (item.CurrBranchTradePrice * (1 - coalesce(AccExceptionDisc, AccCmpDisc, 0)))
	   ) -
	   sum(ISNULL(ps.Total12MonthQuantity, 0) * 
	    (item.CurrBranchTradePrice * (1 - coalesce(ld.LastItemDiscountPercent, 0)))
	   ) as [TotalSalesImpact],
	   -- Price Effect = Prior Qty * (New terms price - Prior Avg Price)
       sum(ISNULL(ps.Total12MonthQuantity, 0) * 
	    ((item.CurrBranchTradePrice * (1 - coalesce(AccExceptionDisc, AccCmpDisc, 0))) -
	     (item.CurrBranchTradePrice * (1 - coalesce(ld.LastItemDiscountPercent, 0)))
		)
	   ) as [PriceEffect] ,
	   -- Volume Effect = (Projected Qty - Prior Qty) * New Price
	   sum((ISNULL(flpac.Total12MonthQuantity,0) - ISNULL(ps.Total12MonthQuantity, 0)) *
	   	   (item.CurrBranchTradePrice * (1 - coalesce(AccExceptionDisc, AccCmpDisc, 0)))
	   ) as [VolumeEffect]
  from lion.vwFactLastPriceAndCost flpac inner join
	   LastDiscount ld on flpac.AccountKey = ld.AccountKey and flpac.ItemKey = ld.ItemKey and flpac.InvoiceLineGroup1Key = 4 inner join
       lion.vwAccount acct on flpac.AccountKey = acct.AccountKey inner join
       lion.vwItem item on flpac.ItemKey = item.ItemKey inner join
	   lion.CentralTerms ct on flpac.AccountKey = ct.AccountKey and
	                           ((ct.RowType = 'Detail' and flpac.ItemKey = ct.ItemKey) or
							    (ct.RowType = 'LLSPG' and item.LLSPGCode = ct.LLSPG and 
								  not exists (select *
								                from lion.CentralTerms ict
											   where ict.AccountKey = flpac.AccountKey and ict.ItemKey = flpac.ItemKey)
								)
						       ) and
							   flpac.InvoiceLineGroup1Key = 4 full outer join
		PriorSales ps on flpac.AccountKey = ps.AccountKey and flpac.ItemKey = ps.ItemKey and flpac.InvoiceLineGroup1Key = 4
  WHERE ct.Returned = 1
group by acct.Branch
)

insert into lion.TermsResults
 select pr.Branch, pr.CountPending, pr.CountReturned, pr.CountAccepted, pr.PercentAccepted, pr.ProposedImpactAmount, pr.AcceptedImpactAmount, pr.PercentAmountAccepted,
        pr.CountStuck, pr.PercentStuck, isnull(gmi.ActualImpact, 0) as ActualImpact, pr.ProjectedImpact, si.PriceEffect, si.VolumeEffect, si.TotalSalesImpact 
   from ProcessResults pr LEFT OUTER JOIN
        GrossMarginImpact gmi on pr.Branch = gmi.Branch LEFT OUTER JOIN
		SalesImpact si on pr.Branch = si.Branch
GO
