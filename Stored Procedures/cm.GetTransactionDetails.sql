SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE [cm].[GetTransactionDetails]
	@ParamClientInvoiceLineUniqueID float

WITH EXECUTE AS CALLER
AS

/*

exec cm.GetTransactionDetails 626078061

*/

SET NOCOUNT ON



SELECT 
	fil.* ,
	ISNULL( v.VendorDescription, 'n/a' ) as VendorDescription
	FROM cm.FactInvoiceLine fil (NOLOCK)
	LEFT JOIN DimVendor v (NOLOCK) ON 
		v.VendorNumber = fil.VendorNumber
	WHERE fil.ClientInvoiceLineUniqueID = @ParamClientInvoiceLineUniqueID


GO
