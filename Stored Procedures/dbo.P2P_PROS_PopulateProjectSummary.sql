SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO






CREATE
PROCEDURE [dbo].[P2P_PROS_PopulateProjectSummary] @ProjectKey Key_Normal_Type
AS

SET NOCOUNT ON
SET QUOTED_IDENTIFIER ON 
SET ANSI_NULLS ON 
SET ARITHABORT ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
SET CONCAT_NULL_YIELDS_NULL ON
SET NUMERIC_ROUNDABORT OFF

EXEC LogDCPEvent 'DCP - P2P_PROS_PopulateProjectSummary', 'B', 0, 0, 0, @ProjectKey


DECLARE 
	@RowsI INT, 
	@RowsU INT, 
	@RowsD INT,
	@UnknownInvoiceLineGroup2Key Key_Normal_Type,
	@SQLStmt NVARCHAR(MAX)
	
	
SET @RowsI = 0
SET @RowsU = 0
SET @RowsD = 0

SET @UnknownInvoiceLineGroup2Key = (SELECT MIN(InvoiceLineGroup2Key) FROM DimInvoiceLineGroup2 WHERE InvoiceLineGroup2UDVarchar1 IN ('None', 'Unknown'))

DELETE FROM PlaybookProjectFullScopeSummary 
WHERE ProjectKey = @ProjectKey
SET @RowsD = @RowsD + @@RowCount


INSERT PlaybookProjectFullScopeSummary (
	ProjectKey,
	AccountManagerKey,
	AccountKey,
	ItemKey,
	InvoiceLineGroup1Key,
	TotalActual12MonthPrice,
	TotalActual12MonthCost,
	TotalActual12MonthQuantity,
	TotalInvoiceLineCount,
	LastInvoiceLineGroup2Key,
	LastItemPrice,
	LastItemCost
)
SELECT 
	@ProjectKey,
	fil.AccountManagerKey,
	fil.AccountKey, 
	fil.ItemKey,
	fil.InvoiceLineGroup1Key,
	SUM(fil.TotalActualPrice) AS TotalActualPrice,
	SUM(fil.TotalActualCost) AS TotalActualCost,
	SUM(fil.TotalQuantity) AS TotalQuantity,
	COUNT(*) AS TotalInvoiceLineCount,
	ISNULL(flpc.LastInvoiceLineGroup2Key, @UnknownInvoiceLineGroup2Key) AS LastInvoiceLineGroup2Key,
	flpc.LastItemPrice,
	flpc.LastItemCost
FROM dbo.FactInvoiceLine fil
INNER JOIN 
	(
	SELECT DISTINCT
		AccountKey
	FROM ProposalPrice
	WHERE ProjectKey = @ProjectKey
	) #Accounts ON
	#Accounts.AccountKey = fil.AccountKey
LEFT JOIN FactLastPriceAndCost flpc ON 
	fil.AccountKey = flpc.AccountKey AND 
	fil.ItemKey = flpc.ItemKey AND 
	fil.InvoiceLineGroup1Key = flpc.InvoiceLineGroup1Key
WHERE Last12MonthsIndicator = 'Y'
GROUP BY
	fil.AccountManagerKey,
	fil.AccountKey,
	fil.ItemKey,
	fil.InvoiceLineGroup1Key,
	flpc.LastInvoiceLineGroup2Key,
	flpc.LastItemPrice,
	flpc.LastItemCost


SET @RowsI = @RowsI + @@RowCount


SET @SQLStmt = 'Update Statistics PlaybookProjectFullScopeSummary'
EXEC (@SQLStmt)

EXEC LogDCPEvent 'DCP - P2P_PROS_PopulateProjectSummary', 'E', @RowsI, @RowsU, @RowsD, @ProjectKey










GO
