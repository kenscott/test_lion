SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE PROCEDURE [dbo].[Load_FactCoreNonCore]
AS

DECLARE @RowsI Quantity_Normal_type, @RowsU Quantity_Normal_type, @RowsD Quantity_Normal_type
SET @RowsI = 0
SET @RowsU = 0
SET @RowsD = 0

EXEC LogDCPEvent 'ETL - dbo.FactCoreNonCore - Insert', 'B'

EXEC DBA_IndexRebuild 'FactCoreNonCore', 'Drop_NoClustered'

TRUNCATE TABLE FactCoreNonCore

INSERT INTO FactCoreNonCore(
AccountKey, 
ItemGroup1Key, 
PercentItemGroup1SalesToTotalAccountSales)
SELECT
	AccountItemTotals.AccountKey,
	AccountItemTotals.ItemGroup1Key,
	CASE TotalAccountSales
		WHEN 0 THEN 0
		ELSE TotalAccountItemSales/TotalAccountSales
	END  AS 'PercentItemGroup1SalesToTotalAccountSales'
	FROM 
		(SELECT AccountKey, ItemGroup1Key, SUM(TotalActualPrice) TotalAccountItemSales
		FROM FactInvoiceLine FIL
		WHERE Last12MonthsIndicator = 'Y'
		GROUP BY AccountKey, ItemGroup1Key) AccountItemTotals
	JOIN 
		(SELECT AccountKey, SUM(TotalActualPrice) TotalAccountSales
		FROM FactInvoiceLine FIL
		WHERE Last12MonthsIndicator = 'Y'
		GROUP BY AccountKey) AccountTotals
	ON AccountItemTotals.AccountKey = AccountTotals.AccountKey
SET @RowsI = @RowsI + @@RowCOunt

EXEC LogDCPEvent 'ETL - dbo.FactCoreNonCore - Insert', 'E', @RowsI, @ROwsU, @RowsD


EXEC LogDCPEvent 'ETL - dbo.FactCoreNonCore - Indexes', 'B'
EXEC DBA_IndexRebuild 'FactCoreNonCore', 'Create'
EXEC LogDCPEvent 'ETL - dbo.FactCoreNonCore - Indexes', 'E', @RowsI, @ROwsU, @RowsD




GO
