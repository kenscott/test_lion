SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE [lion].[Load_DimInvoiceLineGroup3]
AS

EXEC LogDCPEvent 'ETL - Load_DimInvoiceLineGroup3', 'B'

--This table will contain a row for every row in the dimension table that has been changed in the staging table (type 1 update will be done)
--CREATE TABLE #UpdateKeys (KeyID INTEGER)

DECLARE @RowsI Quantity_Normal_type, @RowsU Quantity_Normal_type, @RowsD Quantity_Normal_type
SET @RowsI = 0
SET @RowsU = 0
SET @RowsD = 0

DECLARE @TodayDayKey Key_Small_Type

SET @TodayDayKey = (SELECT DayKey FROM DimDay WHERE CONVERT(VARCHAR(10), SQLDate, 120) = CONVERT(VARCHAR(10), GETDATE(), 120))

--This finds every combination of ShipmentType, ConType, and LineType, assigning a Key value to each combination.  
--There are a limited # of combinations so this allows all three to be stored as one key value in the InvoiceLine table (Saving space and processing time)

EXEC DBA_IndexRebuild 'DimInvoiceLineGroup3', 'Drop'

TRUNCATE TABLE DimInvoiceLineGroup3

IF NOT EXISTS (SELECT 1 FROM DimInvoiceLineGroup3 WHERE InvoiceLineGroup3UDVarchar1 = 'Unknown')
BEGIN
	INSERT DimInvoiceLineGroup3 (
		InvoiceLineGroup3UDVarchar1,
		InvoiceLineGroup3UDVarchar2,
		InvoiceLineGroup3UDVarchar3,
		InvoiceLineGroup3UDVarchar4,
		InvoiceLineGroup3UDVarchar5,
		CreationDayKey,
		ModificationDayKey)
	VALUES (
		'Unknown',
		'Unknown',
		'Unknown',
		'Unknown',
		'Unknown',
		@TodayDayKey,
		@TodayDayKey)
	SET @RowsI = @RowsI + @@RowCount
END


INSERT INTO dbo.DimInvoiceLineGroup3 (
	InvoiceLineGroup3UDVarchar1,
	InvoiceLineGroup3UDVarchar2,
	InvoiceLineGroup3UDVarchar3,
	InvoiceLineGroup3UDVarchar4,
	InvoiceLineGroup3UDVarchar5,
	CreationDayKey,
	ModificationDayKey
) 
SELECT
	'N',
	'None',
	'None',
	'None',
	'None',
	@TodayDayKey,
	@TodayDayKey
WHERE 'N' NOT IN (SELECT InvoiceLineGroup3UDVarchar1 FROM dbo.DimInvoiceLineGroup3)


INSERT INTO dbo.DimInvoiceLineGroup3 (
	InvoiceLineGroup3UDVarchar1,
	InvoiceLineGroup3UDVarchar2,
	InvoiceLineGroup3UDVarchar3,
	InvoiceLineGroup3UDVarchar4,
	InvoiceLineGroup3UDVarchar5,
	CreationDayKey,
	ModificationDayKey
) 
SELECT
	'Y',
	'None',
	'None',
	'None',
	'None',	
	@TodayDayKey,
	@TodayDayKey
WHERE 'Y' NOT IN (SELECT InvoiceLineGroup3UDVarchar1 FROM dbo.DimInvoiceLineGroup3)


EXEC DBA_IndexRebuild 'DimInvoiceLineGroup3', 'Create'

SELECT TOP 10 * FROM DimInvoiceLineGroup3

--DROP TABLE #UpdateKeys

EXEC LogDCPEvent 'ETL - Load_DimInvoiceLineGroup3', 'E', @RowsI, @RowsU, @RowsD

GO
