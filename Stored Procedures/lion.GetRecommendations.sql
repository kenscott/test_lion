SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE [lion].[GetRecommendations]

    @Account Description_Small_type,
	@MaxSIChange UDDecimal_type = 0.00,
	@MaxChange UDDecimal_type = 0.02
AS
    SET NOCOUNT ON
    SET ANSI_WARNINGS OFF 

    DECLARE @LowerTradeThreshold UDDecimal_type, @TradeFactor UDDecimal_type, @DefaultAction NVARCHAR(255)
    SET @LowerTradeThreshold = -.01
	SET @TradeFactor = 1 + @LowerTradeThreshold
	SET @DefaultAction = 'Accept'

	DECLARE @AccountKey UDInteger_type
	SELECT @AccountKey = AccountKey
	 FROM lion.vwAccount WHERE AccountNumber = @Account

	DECLARE @LLSPGInstruction NVARCHAR(255), @ItemInstruction NVARCHAR(255), @MarketInstruction NVARCHAR(255), 
	        @UnusedInstruction NVARCHAR(255), @UnknownInstruction NVARCHAR(255), @NonQualException NVARCHAR(255),
			@LowSalesInstruction NVARCHAR(255), @RecentChange NVARCHAR(255), @TBDInstruction NVARCHAR(255)

	SET @LLSPGInstruction	 = '1 - LLSPG Disc %'
	SET @ItemInstruction	 = '2 - Item Disc %'
	SET @MarketInstruction	 = '3 - Market (No Terms)'
	SET @UnusedInstruction	 = '4 - Unused Exception'
	SET @NonQualException    = '5 - Non-Qual Exception'
	SET @UnknownInstruction	 = '6 - Unknown'
	SET @LowSalesInstruction = '7 - Low Sales'
	SET @RecentChange        = '8 - Recent Change'
	SET @TBDInstruction		 = 'TBD'

	-- Get ACCOUNT contract LLSPG Terms
	SELECT DISTINCT
		    SPG AS SPGCode,
			1 - (1 - ISNULL(UsualDiscount1 / 100, 0)) * (1 - ISNULL(UsualDiscount2 / 100, 0)) AS Discount
	INTO #LLSPGDefaultTerms
	FROM lion.AccountContractTerms
    WHERE SPGExpiryDate > GETDATE() AND ContractName = 'ACCOUNT'

	-- Get ACCOUNT contract product exceptions
	SELECT DISTINCT
		    ExceptionProduct AS Product,
			ExceptionDiscount / 100 AS Discount
	INTO #ProductExceptionDefaultTerms
	FROM lion.AccountContractTerms
	WHERE ExceptionProduct IS NOT NULL AND
	      SPGExpiryDate > GETDATE() AND ContractName = 'ACCOUNT'

	-- Get existing LLSPG-level terms for given account
    SELECT DISTINCT
            ct.AccountKey ,
            [PyramidCode] ,
            ct.[SPGCode] ,
			s.[LLSPGDescription] ,
            [OwningBrandCode] ,
            ct.UsualDiscount1 / 100 AS [SPG Disc 1] ,
            ct.UsualDiscount2 / 100 AS [SPG Disc 2] ,
            1 - (1 - ISNULL(ct.UsualDiscount1 / 100, 0)) * (1 - ISNULL(ct.UsualDiscount2 / 100, 0)) AS [Disc] ,
            [DeleteFlag] ,
            [LastChangeInitials] ,
            CONVERT(DATE, LEFT(ct.LastChangeDate, CHARINDEX(' ', ct.LastChangeDate))) AS [LastChangeDate] ,
            [LastChangeBranch]
    INTO    #LLSPGTerms
    FROM    [lion].[CustomerTerms] ct
            INNER JOIN lion.vwAccount a ON ct.AccountKey = a.AccountKey
			LEFT OUTER JOIN lion.vwLLSPG s ON ct.PyramidCode = s.ItemPyramidCode AND
			                s.LLSPGCode = ct.SPGCode
    WHERE   a.AccountNumber = @Account

	-- Get LLSPGs purchased by the customer
    SELECT  DISTINCT
            f.AccountKey,
            i.ItemPyramidCode,
            i.LLSPGCode,
			i.LLSPGDescription,
            acct.BranchPrimaryBrand
    INTO    #PrelimSoldLLSPG
    FROM    dbo.FactLastPriceAndCost f
            INNER JOIN lion.vwAccount acct ON f.AccountKey = acct.AccountKey
            INNER JOIN lion.vwItem i ON f.ItemKey = i.ItemKey
            INNER JOIN lion.vwFactInvoiceLineGroup1 g1 ON f.InvoiceLineGroup1Key = g1.InvoiceLineGroup1Key
    WHERE   acct.AccountNumber = @Account
            AND g1.ShipmentType = 'S'
            AND g1.DeviatedIndicator = 'N'


	-- Combine existing LLSPG-level terms with other LLSPGs purchased by the customer
    SELECT  DISTINCT
            COALESCE(s.AccountKey, t.AccountKey) AS AccountKey ,
            COALESCE(s.ItemPyramidCode, CONVERT(INT, t.PyramidCode)) AS PyramidCode ,
            COALESCE(s.LLSPGCode, t.SPGCode) AS LLSPGCode ,
			COALESCE(s.LLSPGDescription, t.LLSPGDescription) AS LLSPGDescription ,
            COALESCE(t.[OwningBrandCode], s.BranchPrimaryBrand) AS [Brand] ,
            t.[SPG Disc 1] ,
            t.[SPG Disc 2] ,
            t.[Disc] ,
            t.[DeleteFlag] ,
            t.[LastChangeInitials] ,
            t.[LastChangeDate] ,
            t.[LastChangeBranch] ,
            CONVERT(NVARCHAR(255), '') AS [Price Instruction Type/Level] ,
            CONVERT(decimal(38,8), 0.000000) AS [Rec'mnd Discount] ,
            CASE WHEN ( s.AccountKey IS NULL) 
			     THEN IIF(ISNULL(t.[LastChangeDate], '1-JAN-2015') <= DATEADD(dd, -184, CAST(getdate() AS DATE)), 'Delete', 'Skip')
                 ELSE @DefaultAction
            END AS [Action] ,
			' ' AS KVI
    INTO    #PrelimLLSPG
    FROM    #PrelimSoldLLSPG s
            FULL OUTER JOIN #LLSPGTerms t ON t.AccountKey = s.AccountKey
                                             AND CONVERT(int, t.PyramidCode) = s.ItemPyramidCode
                                             AND t.SPGCode = s.LLSPGCode


	-- Get existing product exception terms
    SELECT  [CustomerTermsKey] ,
            ct.AccountKey ,
            i.ItemKey ,
            [PyramidCode] ,
            [SPGCode] ,
            [OwningBrandCode] ,
            [UsualDiscount1] / 100 AS UsualDiscount1,
            [UsualDiscount2] / 100 AS UsualDiscount2 ,
            [DeleteFlag] ,
            [LastChangeInitials] ,
            CONVERT(DATE, LEFT(ct.LastChangeDate, CHARINDEX(' ', ct.LastChangeDate))) AS [LastChangeDate] ,
            [LastChangeTime] ,
            [LastChangePLID] ,
            [LastChangeBranch] ,
            [ExceptionProduct] ,
            [ExceptionDiscount] / 100 as ExceptionDiscount,
            [ExceptionFixedPrice] ,
            [ExceptionFromDate] ,
            [ExceptionToDate] ,
            [ExceptionPCF]
	INTO #ItemTerms
    FROM    lion.CustomerTerms ct
            INNER JOIN lion.vwAccount a ON ct.AccountKey = a.AccountKey
            INNER JOIN lion.vwItem i ON i.ProductCode = ct.ExceptionProduct
                                                      AND i.ItemPyramidCode = 1
    WHERE   a.AccountNumber = @Account
            AND ct.ExceptionProduct <> ''
            AND CONVERT (INT, ct.PyramidCode) = 1


;WITH SPGTerms AS (
  SELECT ct.AccountKey, ct.SPGCode, MAX(CONVERT(DATE,ct.LastChangeDate)) AS LastChangeDate
    FROM lion.CustomerTerms ct
   WHERE ct.AccountKey = @AccountKey AND ct.PyramidCode = '1'
  GROUP BY ct.AccountKey, ct.SPGCode
)

    SELECT  f.AccountKey ,
	        lil.ItemGroup3Key,
            f.ItemKey ,
            f.InvoiceLineGroup1Key , 
		-- Product Information
            i.LLSPGCode ,
            i.LLSPGCode + ' - ' + i.LLSPGDescription AS LLSPG ,
            i.ItemNumber AS [Product Code] ,
            i.ItemDescription AS [Product Description] ,
            acct.BranchPrimaryBrand AS [Brand] ,
			i.CurrentCost as [Current Cost] ,
		-- Action
            CASE WHEN (t.Price = 0 ) THEN IIF(ISNULL(s.LastChangeDate, '1-JAN-2015') <= DATEADD(dd, -184, CAST(getdate() as DATE)), 'Delete', 'Skip')
			     WHEN it.ItemKey is not null THEN @DefaultAction 
			     WHEN ( lil.PriceDerivation IN ('ADJ', 'DC', 'DP', 'SPO') AND f.UDVarchar1 <> 'Y') THEN 'Skip' -- f.UDVarchar1 = StrategicItem
				 WHEN (i.KnownValueInd = 'Y' and 
			                  not exists (select * 
							                from lion.AccountSPG aspg 
										   where aspg.AccountKey = f.AccountKey and 
										         aspg.LLSPGCode IN (i.LLSPGCode, 'ALL')))
                 THEN 'Skip'
				 ELSE @DefaultAction END AS [Action] ,
            CASE WHEN ( t.Price = 0 ) THEN @UnknownInstruction
			     WHEN it.ItemKey is not null or f.UDVarchar1 = 'Y' THEN @ItemInstruction
			     WHEN ( lil.PriceDerivation IN ('ADJ', 'DC', 'DP', 'SPO') AND f.UDVarchar1 <> 'Y') THEN @NonQualException -- f.UDVarchar1 = StrategicItem
			     WHEN ( f.UDVarchar1 <> 'Y' AND dd.SQLDate < ISNULL(s.LastChangeDate, dd.SQLDate)) THEN @NonQualException -- f.UDVarchar1 = StrategicItem
				 WHEN (i.KnownValueInd = 'Y' and 
			                  not exists (select * 
							                from lion.AccountSPG aspg 
										   where aspg.AccountKey = f.AccountKey and 
										         aspg.LLSPGCode IN (i.LLSPGCode, 'ALL'))) OR
                      ( alip.Max - t.Price ) / NULLIF(t.Price, 0) >= @LowerTradeThreshold
                 THEN @MarketInstruction
                 --WHEN alip.Max <= r.Price
                 --THEN IIF(f.UDVarchar1 = 'Y', @ItemInstruction, @TBDInstruction)
				 ELSE @TBDInstruction
                 --WHEN alip.Max <= a.Price
                 --THEN @TBDInstruction
                 --WHEN alip.Max <= g.Price THEN @MarketInstruction
                 --ELSE @MarketInstruction
            END AS [Price Instruction Type/Level] ,

		-- Accepted/Recommended Pricing
		  -- AcceptedPrice
		  -- AcceptedDiscount
		  -- AcceptedImpact
		  -- AcceptedPriceLevel
            CASE WHEN it.ItemKey is not null and f.UDVarchar1='N'
			     THEN IIF(it.ExceptionFixedPrice is not null, 
				          it.ExceptionFixedPrice, 
				          IIF((t.Price * (1-it.ExceptionDiscount)) + (t.Price * @MaxChange) < g.Price, (t.Price * (1-it.ExceptionDiscount)) + (t.Price * @MaxChange), g.Price)
						 )
				 WHEN i.KnownValueInd = 'Y' and 
			                  not exists (select * 
							                from lion.AccountSPG aspg 
										   where aspg.AccountKey = f.AccountKey and 
										         aspg.LLSPGCode IN (i.LLSPGCode, 'ALL'))
				 THEN t.Price
				 WHEN it.ItemKey is not null and f.UDVarchar1='Y' -- Strategic Item
				 THEN IIF(it.ExceptionFixedPrice is not null, 
				          it.ExceptionFixedPrice, 
				          IIF((t.Price * (1-it.ExceptionDiscount)) + (t.Price * @MaxSIChange) < g.Price, (t.Price * (1-it.ExceptionDiscount)) + (t.Price * @MaxSIChange), g.Price)
						 )
			     WHEN lil.PriceDerivation IN ('ADJ', 'DC', 'DP', 'SPO') and f.UDVarchar1 <> 'Y'
			     THEN alip.Max
				 WHEN f.UDVarchar1='Y' AND alip.Max <= g.Price
					  -- Move by 0.5% up to green
				 THEN IIF(alip.Max + (t.Price * @MaxSIChange) < g.Price, alip.Max + (t.Price * @MaxSIChange), g.Price)
			     WHEN ( alip.Max - t.Price ) / NULLIF(t.Price, 0) >= @LowerTradeThreshold
					  -- Apply Trade Price Matching Logic
                 THEN IIF(alip.Max + (t.Price * @MaxChange) < t.Price, alip.Max + (t.Price * @MaxChange), t.Price)
                 WHEN alip.Max <= g.Price
					  -- Move by 2% up to green
				 THEN IIF(alip.Max + (t.Price * @MaxChange) < g.Price, alip.Max + (t.Price * @MaxChange), g.Price)
					  -- If already charging above green, use the last price
                 ELSE alip.Max
            END AS [Rec'mnd Price] ,
            ROUND(CASE WHEN it.ItemKey is not null and f.UDVarchar1='N' 
					   THEN IIF(it.ExceptionFixedPrice is not null, 
								NULL, 
								(1 - ( IIF((t.Price * (1-it.ExceptionDiscount)) + (t.Price * @MaxChange) < g.Price, (t.Price * (1-it.ExceptionDiscount)) + (t.Price * @MaxChange), g.Price) / NULLIF(t.Price, 0)))
							   )
					   WHEN i.KnownValueInd = 'Y' and 
			                  not exists (select * 
							                from lion.AccountSPG aspg 
										   where aspg.AccountKey = f.AccountKey and 
										         aspg.LLSPGCode IN (i.LLSPGCode, 'ALL'))
					   THEN 0
				       WHEN it.ItemKey is not null and f.UDVarchar1='Y' -- Strategic Item
				       THEN IIF(it.ExceptionFixedPrice is not null, 
				                null, 
				                (1 - IIF((t.Price * (1-it.ExceptionDiscount)) + (t.Price * @MaxSIChange) < g.Price, (t.Price * (1-it.ExceptionDiscount)) + (t.Price * @MaxSIChange), g.Price) / NULLIF(t.Price, 0))
						       ) 
			           WHEN lil.PriceDerivation IN ('ADJ', 'DC', 'DP', 'SPO') and f.UDVarchar1 <> 'Y'
			           THEN 1-(alip.Max / NULLIF(t.Price, 0))
				       WHEN f.UDVarchar1='Y' AND alip.Max <= g.Price
				      	  -- Move by 0.5% up to green
				       THEN (1 - ( IIF(alip.Max + (t.Price * @MaxSIChange) < g.Price, alip.Max + (t.Price * @MaxSIChange), g.Price) / NULLIF(t.Price, 0)))
				       WHEN ( alip.Max - t.Price ) / NULLIF(t.Price, 0) >= @LowerTradeThreshold
						  -- Apply Trade Price Matching Logic
					   THEN (1 - ( IIF(alip.Max + (t.Price * @MaxChange) < t.Price, alip.Max + (t.Price * @MaxChange), t.Price) / NULLIF(t.Price, 0)))
					   WHEN alip.Max <= g.Price
						  -- Move by 2% up to green
					   THEN (1 - ( IIF(alip.Max + (t.Price * @MaxChange) < g.Price, alip.Max + (t.Price * @MaxChange), g.Price) / NULLIF(t.Price, 0)))
						  -- If already charging above green, use the last price
					   ELSE (1 - (alip.Max / NULLIF(t.Price, 0)))
				   END
            , 4 ) AS [Rec'mnd Discount] ,
            CASE WHEN it.ItemKey is not null
			     THEN 'Excpt'
			     WHEN i.KnownValueInd = 'Y' and 
			          not exists (select * 
					                from lion.AccountSPG aspg 
								   where aspg.AccountKey = f.AccountKey and 
								         aspg.LLSPGCode IN (i.LLSPGCode, 'ALL'))
				 THEN 'Trade'
				 WHEN lil.PriceDerivation IN ('ADJ', 'DC', 'DP', 'SPO')
			     THEN 'Last'
			     WHEN ( alip.Max - t.Price ) / NULLIF(t.Price, 0) >= @LowerTradeThreshold
                 THEN 'Trade'
                 WHEN alip.Max <= r.Price
                 THEN 'Red'
                 WHEN alip.Max <= a.Price
                 THEN 'Amber'
                 WHEN alip.Max <= g.Price 
				 THEN 'Green'
                 ELSE 'Last'
            END AS [Rec'mnd Price Logic] ,
            ((CASE WHEN it.ItemKey is not null and f.UDVarchar1='N' 
				   THEN IIF(it.ExceptionFixedPrice is not null, 
							 NULL, 
							 (1 - ( IIF((t.Price * (1-it.ExceptionDiscount)) + (t.Price * @MaxChange) < g.Price, (t.Price * (1-it.ExceptionDiscount)) + (t.Price * @MaxChange), g.Price) / NULLIF(t.Price, 0)))
							)
				   WHEN i.KnownValueInd = 'Y' and 
			                not exists (select * 
							            from lion.AccountSPG aspg 
										where aspg.AccountKey = f.AccountKey and 
										        aspg.LLSPGCode IN (i.LLSPGCode, 'ALL'))
				   THEN 0
				   WHEN it.ItemKey is not null and f.UDVarchar1='Y' -- Strategic Item
				   THEN IIF(it.ExceptionFixedPrice is not null, 
				             it.ExceptionFixedPrice, 
				             IIF((t.Price * (1-it.ExceptionDiscount)) + (t.Price * @MaxSIChange) < g.Price, (t.Price * (1-it.ExceptionDiscount)) + (t.Price * @MaxSIChange), g.Price)
						    )
			       WHEN lil.PriceDerivation IN ('ADJ', 'DC', 'DP', 'SPO')
			       THEN alip.Max
				   WHEN f.UDVarchar1='Y' AND alip.Max <= g.Price
				      	-- Move by 0.5% up to green
				   THEN IIF(alip.Max + (t.Price * @MaxSIChange) < g.Price, alip.Max + (t.Price * @MaxSIChange), g.Price)
				   WHEN ( alip.Max - t.Price ) / NULLIF(t.Price, 0) >= @LowerTradeThreshold
					   -- Apply Trade Price Matching Logic
				   THEN (1 - ( IIF(alip.Max + (t.Price * @MaxChange) < t.Price, alip.Max + (t.Price * @MaxChange), t.Price) / NULLIF(t.Price, 0)))
				   WHEN alip.Max <= g.Price
						-- Move by 2% up to green
				   THEN (1 - ( IIF(alip.Max + (t.Price * @MaxChange) < g.Price, alip.Max + (t.Price * @MaxChange), g.Price) / NULLIF(t.Price, 0)))
						-- If already charging above green, use the last price
				   ELSE (1 - (alip.Max / NULLIF(t.Price, 0)))
			   END) - i.CurrentCost - (f.LastItemPrice - f.LastItemCost)
			) * f.Total12MonthQuantity AS [Rec'mnd Impact] ,
            f.UDVarchar1 AS [Strategic Item] ,
		-- History
		    f.LastItemPrice as [Last Item Price] ,
			f.LastItemCost as [Last Item Cost],
            alip.Max AS [Adjusted Last Item Price] ,
            CASE WHEN alip.Max < a.Price THEN 'Red'
                 WHEN alip.Max < g.Price THEN 'Amber'
                 WHEN alip.Max >= g.Price THEN 'Green'
                 ELSE 'Unknown'
            END AS [Last Price Level (GAR)] ,
            f.LastItemDiscountPercent AS [Last Item Discount] ,
			(f.LastItemPrice - f.LastItemCost) / NULLIF(f.LastItemPrice, 0.0) as [Last Item GPP] ,
            l.GPP AS [Adjusted Last Item GPP] ,
            f.Total12MonthSales AS [Total Sales] ,
			f.Total12MonthQuantity * t.Price AS [Total Sales @ Trade],
            f.Total12MonthQuantity AS [Total Quantity] ,

		-- Price Bands
            r.Price AS [Red Price] ,
            a.Price AS [Amber Price] ,
			g.Price AS [Green Price (TradeAdj)] ,
            --t.Price AS [Trade Price] ,
			i.CurrBranchTradePrice AS [Trade Price] ,
			t.Price as [Calc Trade Price] ,
			pb.StretchGPP as [Green GPP] ,
			pb.TargetGPP as [Amber GPP] ,
			pb.FloorGPP as [Red GPP] ,
			tgpp.GPP as [Trade GPP] ,
            g1.DeviatedIndicator AS [Contract Claims] ,
            pba.PriceApproach AS [Price Approach] ,
            pba.Sales603010Bucket AS [Sales Size] ,
            
			--pba.PricingRuleSequenceNumber AS [PB Level] ,
            --pba.PlaybookPricingGroupKey ,

			pb.PricingRuleSequenceNumber AS [PB Level] ,
            pb.PlaybookPricingGroupKey ,

			lil.PriceDerivation ,
			dd.SQLDate as LastSaleDate ,
			s.LastChangeDate
    INTO    #Prelim
    FROM    dbo.FactLastPriceAndCost f
	        INNER JOIN lion.vwFactInvoiceLine lil on f.LastInvoiceLineUniqueIdentifier = lil.InvoiceLineUniqueIdentifier
			INNER JOIN dbo.DimDay dd on lil.InvoiceDateDayKey = dd.DayKey 
            INNER JOIN dbo.FactRecommendedPriceBand pb ON f.AccountKey = pb.AccountKey
                                                              AND f.ItemKey = pb.ItemKey
                                                              AND f.InvoiceLineGroup1Key = pb.InvoiceLineGroup1Key
            INNER JOIN lion.vwAccount acct ON f.AccountKey = acct.AccountKey
            INNER JOIN lion.vwItem i ON f.ItemKey = i.ItemKey
            INNER JOIN lion.vwFactInvoiceLineGroup1 g1 ON f.InvoiceLineGroup1Key = g1.InvoiceLineGroup1Key
            
			LEFT OUTER JOIN lion.RPBPriceBandAttributes pba ON pb.PlaybookPricingGroupKey = pba.PlaybookPricingGroupKey
			LEFT OUTER JOIN SPGTerms s ON f.AccountKey = s.AccountKey AND i.LLSPGCode = s.SPGCode
			LEFT OUTER JOIN #ItemTerms it on f.AccountKey = it.AccountKey and f.ItemKey = it.ItemKey
            
			--CROSS APPLY [lion].[fn_GetRPBPriceBandAttributes](pb.CoveringBandGroupValues, CoveringBandGroupingColumns) pba
			
			CROSS APPLY lion.fn_GetGPP(f.LastItemPrice, f.LastItemCost) l
            CROSS APPLY lion.fn_GetPriceEx(i.CurrentCost, pb.FloorGPP, @TradeFactor, i.CurrBranchTradePrice) r
            CROSS APPLY lion.fn_GetPriceEx(i.CurrentCost, pb.TargetGPP, @TradeFactor, i.CurrBranchTradePrice) a
            CROSS APPLY lion.fn_GetPriceEx(i.CurrentCost, pb.StretchGPP, @TradeFactor, i.CurrBranchTradePrice) g
			CROSS APPLY lion.fn_GetPriceEx(i.CurrentCost, l.GPP, @TradeFactor, i.CurrBranchTradePrice) lip  -- Last item's price at same margin as last item
			CROSS APPLY lion.fn_Max(lip.Price, f.LastItemPrice) alip
            CROSS APPLY lion.fn_GetPrice( i.CurrBranchTradePrice, 0) t
            CROSS APPLY lion.fn_GetGPP(t.Price, i.CurrentCost) tgpp
			--CROSS APPLY lion.fn_GetDiscPrice(t.Price, f.LastItemDiscountPercent) lip  -- Last item's price at same discount w/today's trade price
			CROSS APPLY lion.fn_GetGPP(f.LastItemPrice,i.CurrentCost) lgpp
            CROSS APPLY lion.fn_GetImpact(r.Price -i.CurrentCost, f.LastItemPrice - f.LastItemCost, f.Total12MonthQuantity) ri
            CROSS APPLY lion.fn_GetImpact(a.Price -i.CurrentCost, f.LastItemPrice - f.LastItemCost, f.Total12MonthQuantity) ai
            CROSS APPLY lion.fn_GetImpact(g.Price -i.CurrentCost, f.LastItemPrice - f.LastItemCost, f.Total12MonthQuantity) gi
            CROSS APPLY lion.fn_GetImpact(t.Price -i.CurrentCost, f.LastItemPrice - f.LastItemCost, f.Total12MonthQuantity) ti
            CROSS APPLY lion.fn_GetRisk(  t.Price -i.CurrentCost, f.LastItemPrice - f.LastItemCost, f.Total12MonthQuantity) tr
    WHERE   f.Total12MonthQuantity > 0
	        AND f.LastItemPrice > 0
			AND i.CurrBranchTradePrice > 0
	        AND lil.PriceDerivation NOT IN ('DC', 'OCD', 'OCL') 
	        AND acct.AccountNumber = @Account
 	        AND i.ItemPyramidCode = 1
            AND g1.ShipmentType = 'S'
            AND g1.DeviatedIndicator = 'N'

    SELECT  p.LLSPG ,
            p.LLSPGCode ,
			SUM([Total Sales @ Trade] * [Rec'mnd Discount]) /  -- Original Discount Amount
			   NULLIF(SUM([Total Sales @ Trade]), 0)
		    AS [Rec'mnd Discount]
    INTO    #LLSPG
    FROM    #Prelim p
    WHERE   [Price Instruction Type/Level] = @TBDInstruction and
	        [PriceDerivation] NOT IN ('DP','DC','ADJ','SPO') and
			[Strategic Item] = 'N' AND
			[Adjusted Last Item GPP] >= 0 AND
			p.LastSaleDate >= ISNULL(p.LastChangeDate, p.LastSaleDate)
	GROUP BY p.LLSPG, p.LLSPGCode
    ORDER BY 1

  UPDATE p
     SET p.[Price Instruction Type/Level] = @NonQualException,
	     p.[Action] = 'Skip'
    FROM #Prelim p LEFT OUTER JOIN
	     #ItemTerms it on p.AccountKey = it.AccountKey and p.ItemKey = it.ItemKey
    WHERE it.ItemKey is null and
	      [Strategic Item] = 'N' AND
	      (([Price Instruction Type/Level] = @TBDInstruction and
	        [PriceDerivation] IN ('DP','DC','ADJ','SPO')
		   ) OR
		   p.LastSaleDate < ISNULL(p.LastChangeDate, p.LastSaleDate)
		  )

    SELECT  l.LLSPG ,
            l.LLSPGCode ,
            CASE WHEN ROUND([Rec'mnd Discount], 1) = 1 THEN [Rec'mnd Discount] ELSE ROUND([Rec'mnd Discount] / .0025, 0) * .0025 END AS NewDiscount
    INTO    #Updates
    FROM    #LLSPG l

    UPDATE  p
    SET     [Price Instruction Type/Level] = @LLSPGInstruction ,
            [Rec'mnd Discount] = u.NewDiscount ,
            [Rec'mnd Price] = [Trade Price] * ( 1 - u.NewDiscount ) ,
            [Rec'mnd Impact] = ((([Trade Price] * ( 1 - u.NewDiscount )) - [Current Cost]) - ( [Last Item Price] - [Last Item Cost])) * p.[Total Quantity]
    FROM    #Prelim p
            INNER JOIN #Updates u ON p.LLSPGCode = u.LLSPGCode
	WHERE  p.[Price Instruction Type/Level] = @TBDInstruction AND
           (p.[Strategic Item] = 'N' OR
		    not exists (select * 
			              from #ItemTerms it 
						 where it.AccountKey = p.AccountKey and
							   it.ItemKey = p.ItemKey) OR
		    (p.[Strategic Item] = 'Y' and ABS(p.[Rec'mnd Discount] - u.NewDiscount) <= .01))

    UPDATE  pl
    SET     [Price Instruction Type/Level] = @LLSPGInstruction ,
            [Rec'mnd Discount] = u.NewDiscount
    FROM    #PrelimLLSPG pl
            INNER JOIN #Updates u ON pl.LLSPGCode = u.LLSPGCode

    UPDATE  p
    SET     [Price Instruction Type/Level] = @ItemInstruction ,
            [Rec'mnd Discount] = p.[Last Item Discount] ,
            [Rec'mnd Price] = [Trade Price] * ( 1 - p.[Last Item Discount] ) ,
            [Rec'mnd Impact] = ((([Trade Price] * ( 1 - p.[Last Item Discount] )) - [Current Cost]) - ( [Last Item Price] - [Last Item Cost])) * p.[Total Quantity],
			[Rec'mnd Price Logic] = 'Last',
			[Action] = 'Skip'
    FROM    #Prelim p
    WHERE  p.[Price Instruction Type/Level] = @TBDInstruction AND 
		   not exists (select * 
			             from #ItemTerms it 
						where it.AccountKey = p.AccountKey and
							  it.ItemKey = p.ItemKey) AND
		   not exists (select * 
			             from #LLSPGTerms lt 
						where lt.AccountKey = p.AccountKey and
							  lt.SPGCode = p.LLSPGCode) AND
		   not exists (select * 
			             from #PrelimLLSPG pl 
						where pl.AccountKey = p.AccountKey and
							  pl.LLSPGCode = p.LLSPGCode and
							  pl.[SPG Disc 1] is not null)

    UPDATE  #Prelim
    SET     [Price Instruction Type/Level] = @ItemInstruction
    WHERE   [Price Instruction Type/Level] = @TBDInstruction

	UPDATE pl
	   SET pl.KVI = k.MaxKVI
	  FROM #PrelimLLSPG pl inner join 
			( SELECT p.LLSPGCode, p.PyramidCode, MAX(i.KnownValueInd) AS MaxKVI
			 from #PrelimLLSPG p
					INNER JOIN lion.vwItem i ON p.LLSPGCode = i.LLSPGCode
					and p.PyramidCode = i.ItemPyramidCode
			GROUP BY p.LLSPGCode, p.PyramidCode ) k on pl.LLSPGCode = k.LLSPGCode and pl.PyramidCode = k.PyramidCode

    SELECT  'Detail' AS RowType ,
            COALESCE(p.[AccountKey], c.AccountKey) as AccountKey,
            COALESCE(p.[ItemKey], c.ItemKey) as ItemKey,
            [InvoiceLineGroup1Key] ,
            a.AccountNumber as AccountNumber ,
            a.AccountName as AccountName,
            a.Area as Area ,
            i.LLSPGCode AS LLSPG ,
            i.LLSPGDescription AS [LLSPG Description] ,
            SUBSTRING(i.ItemNumber, 2, 100) AS [Product Code] ,
            i.[ItemDescription] as [Product Description] ,
			[Current Cost] ,
            i.KnownValueInd AS KVI ,
            NULL AS [NDP Flag] ,
            d.Discount AS DefaultDiscount ,
            c.OwningBrandCode AS [Brand] ,
            c.UsualDiscount1 AS UsualDiscount1 ,
            c.UsualDiscount2 AS UsualDiscount2 ,
            c.ExceptionDiscount AS ExceptionDiscount ,
            1 - (1 - ISNULL(c.UsualDiscount1, 0)) * (1 - ISNULL(c.UsualDiscount2, 0)) AS [Disc] ,
            [Rec'mnd Discount] * -1 AS [Rec'mnd Discount] ,
            COALESCE([Price Instruction Type/Level], @UnusedInstruction) AS [Price Instruction Type/Level]  ,
            COALESCE([Action], IIF(ISNULL(c.[LastChangeDate], '1-JAN-2015') <= DATEADD(dd, -184, CAST(getdate() AS DATE)), 'Delete', 'Skip')) AS [Action] ,
            c.[LastChangeDate] AS [Last Change Date] ,
            c.[ExceptionFixedPrice] AS [Fixed Price] ,
            [Total Sales] AS [Item Sales] ,
            SUM([Total Sales]) OVER ( PARTITION BY [LLSPG] ) AS [LLSPG Sales] ,
            SUM([Total Sales]) OVER ( PARTITION BY p.[AccountKey] ) AS [Total Sales] ,
            [Rec'mnd Price] ,
            [Rec'mnd Price Logic] ,
            [Rec'mnd Impact] ,
            [Strategic Item] ,
            [Last Item Price] ,
            [Adjusted Last Item Price] ,
            [Last Price Level (GAR)] ,
            [Last Item Discount] ,
            [Last Item GPP] ,
			[Adjusted Last Item GPP] ,
            [Total Quantity] ,
            [Green Price (TradeAdj)] ,
            [Amber Price] ,
            [Red Price] ,
            [Trade Price] ,
			[Green GPP],
			[Amber GPP],
			[Red GPP],
			[Trade GPP],
            [Contract Claims] ,
            [Sales Size] ,
            [Price Approach] ,
            [PB Level] ,
            [CustomerTermsKey] ,
            a.AccountNumber AS AccountID,
            [PyramidCode] ,
            [SPGCode] ,
            [OwningBrandCode] ,
            [DeleteFlag] ,
            [LastChangeInitials] ,
            c.[LastChangeDate] ,
            [LastChangeTime] ,
            [LastChangePLID] ,
            [LastChangeBranch] ,
            [ExceptionProduct] ,
            [ExceptionFixedPrice] ,
            [ExceptionFromDate] ,
            [ExceptionToDate] ,
            [ExceptionPCF] ,
            [PlaybookPricingGroupKey] ,
			[PriceDerivation] ,
			[LastSaleDate] ,
			[Adjusted Last Item Price] AS AdjustedLastItemPrice ,
			[Calc Trade Price] AS TradePrice ,
			[Green Price (TradeAdj)] AS GreenPrice
    INTO    #AllResults
    FROM    #Prelim p
            FULL OUTER JOIN #ItemTerms c on c.AccountKey = p.AccountKey and c.ItemKey = p.ItemKey
            INNER JOIN lion.vwAccount a ON COALESCE(p.AccountKey, c.AccountKey) = a.AccountKey
            INNER JOIN lion.vwItem i ON COALESCE(p.ItemKey, c.ItemKey) = i.ItemKey
			LEFT OUTER JOIN #ProductExceptionDefaultTerms d ON i.ProductCode = d.Product
                                                      AND i.ItemPyramidCode = 1
    UNION
    SELECT  DISTINCT
            'LLSPG' AS RowType ,
            pl.[AccountKey] ,
            NULL AS [ItemKey] ,
            NULL AS [InvoiceLineGroup1Key] ,
            AccountNumber ,
            AccountName ,
            Area ,
            pl.LLSPGCode AS LLSPG ,
            LLSPGDescription AS [LLSPG Description] ,
            NULL AS [Product Code] ,
            NULL AS [Product Description] ,
			NULL AS [Current Cost] ,
            KVI ,
            CASE WHEN K.LLSPGCode is not null THEN 'Y' ELSE NULL END AS [NDP Flag] ,
            d.Discount AS DefaultDiscount ,
            [Brand] ,
            [SPG Disc 1] ,
            [SPG Disc 2] ,
            NULL AS ExceptionDiscount ,
            [Disc] ,
            [Rec'mnd Discount] * -1 AS [Rec'mnd Discount] ,
            [Price Instruction Type/Level] ,
            [Action] ,
            [LastChangeDate] AS [Last Change Date] ,
            NULL AS [Fixed Price] ,
            NULL AS [Item Sales] ,
            NULL AS [LLSPG Sales] ,
            NULL AS [Total Sales] ,
            NULL AS [Rec'mnd Price] ,
            NULL AS [Rec'mnd Price Logic] ,
            NULL AS [Rec'mnd Impact] ,
            NULL AS [Strategic Item] ,
            NULL AS [Last Item Price] ,
            NULL AS [Adjusted Last Item Price] ,
            NULL AS [Last Price Level (GAR)] ,
            NULL AS [Last Item Discount] ,
            NULL AS [Last Item GPP] ,
            NULL AS [Adjusted Last Item GPP] ,
            NULL AS [Total Quantity] ,
            NULL AS [Green Price (TradeAdj)] ,
            NULL AS [Amber Price] ,
            NULL AS [Red Price] ,
            NULL AS [Trade Price] ,
			NULL AS [Green GPP] ,
			NULL AS [Amber GPP] ,
			NULL AS [Red GPP] ,
			NULL AS [Trade GPP] ,
            NULL AS [Contract Claims] ,
            NULL AS [Sales Size] ,
            NULL AS [Price Approach] ,
            NULL AS [PB Level] ,
            NULL AS [CustomerTermsKey] ,
            NULL AS [AccountID] ,
            NULL AS [PyramidCode] ,
            NULL AS [SPGCode] ,
            NULL AS [OwningBrandCode] ,
            NULL AS [DeleteFlag] ,
            [LastChangeInitials] ,
            [LastChangeDate] ,
            NULL AS [LastChange time] ,
            NULL AS [LastChangePLID] ,
            [LastChangeBranch] ,
            NULL AS [ExceptionProduct] ,
            NULL AS [ExceptionFixed price] ,
            NULL AS [ExceptionFrom date] ,
            NULL AS [ExceptionToDate] ,
            NULL AS [ExceptionPCF] ,
            NULL AS [PlaybookPricingGroupKey] ,
			NULL AS [PriceDerivation] ,
			NULL AS [LastSaleDate] ,
			NULL AS [AdjustedLastItemPrice] ,
			NULL AS [TradePrice] ,
			NULL AS [GreenPrice]
    FROM    #PrelimLLSPG pl
            INNER JOIN lion.vwAccount a ON pl.AccountKey = a.AccountKey
			LEFT OUTER JOIN #LLSPGDefaultTerms d on pl.LLSPGCode = d.SPGCode
			LEFT OUTER JOIN lion.AccountSPG k on k.AccountKey = pl.AccountKey and (k.LLSPGCode = pl.LLSPGCode or k.LLSPGCode = 'ALL')

	UPDATE a
	   SET a.[Price Instruction Type/Level] = @ItemInstruction,
	       a.Action = @DefaultAction,
		   a.[Rec'mnd Price] =  CASE WHEN ( a.AdjustedLastItemPrice - a.TradePrice ) / NULLIF(a.TradePrice, 0) >= @LowerTradeThreshold
										  -- Apply Trade Price Matching Logic
									 THEN IIF(a.AdjustedLastItemPrice + (a.TradePrice * @MaxChange) < a.TradePrice, a.AdjustedLastItemPrice + (a.TradePrice * @MaxChange), a.TradePrice)
									 WHEN a.AdjustedLastItemPrice <= a.GreenPrice
										  -- Move by 2% up to green
									 THEN IIF(a.AdjustedLastItemPrice + (a.TradePrice * @MaxChange) < a.GreenPrice, a.AdjustedLastItemPrice + (a.TradePrice * @MaxChange), a.GreenPrice)
										  -- If already charging above green, use the last price
									 ELSE a.AdjustedLastItemPrice
							    END,
		   a.[Rec'mnd Discount] = CASE WHEN ( a.AdjustedLastItemPrice - a.TradePrice ) / NULLIF(a.TradePrice, 0) >= @LowerTradeThreshold
										  -- Apply Trade Price Matching Logic
									   THEN (IIF(a.AdjustedLastItemPrice + (a.TradePrice * @MaxChange) < a.TradePrice, a.AdjustedLastItemPrice + (a.TradePrice * @MaxChange), a.TradePrice) / NULLIF(a.TradePrice, 0)) - 1
									   WHEN a.AdjustedLastItemPrice <= a.GreenPrice
										  -- Move by 2% up to green
									   THEN (IIF(a.AdjustedLastItemPrice + (a.TradePrice * @MaxChange) < a.GreenPrice, a.AdjustedLastItemPrice + (a.TradePrice * @MaxChange), a.GreenPrice) / NULLIF(a.TradePrice, 0)) - 1
										  -- If already charging above green, use the last price
									   ELSE (a.AdjustedLastItemPrice / NULLIF(a.TradePrice, 0)) - 1
								 END

	 FROM #AllResults a LEFT OUTER JOIN
	      #ItemTerms it on a.AccountKey = it.AccountKey and a.ItemKey = it.ItemKey
	WHERE it.ItemKey is null AND
	      (((a.UsualDiscount1 = 0 OR a.[Price Instruction Type/Level] = @NonQualException) AND
	 	    a.ExceptionDiscount IS NOT NULL AND
		    a.[Action] <> 'Delete'
		   ) OR 
		   (a.ExceptionDiscount IS NOT NULL AND 
		    a.[Price Instruction Type/Level] = @LLSPGInstruction AND
            a.[Action] = 'Skip'
		   )
		  )

  SELECT i.LLSPGCode AS LLSPG, SUM(ISNULL(f.TotalNetPrice,f.TotalActualPrice)) AS Sales, SUM(ISNULL(f.TotalNetQuantity, f.TotalQuantity)) AS Quantity, COUNT(DISTINCT f.ItemKey) AS ItemsSold
    INTO #SixMonthSales
    FROM lion.vwFactInvoiceLine f INNER JOIN
         dbo.DimDay dd ON f.InvoiceDateDayKey = dd.DayKey INNER JOIN
		 lion.vwItem i ON f.ItemKey = i.ItemKey
   WHERE dd.SQLDate >= DATEADD(dd, -184, CAST(GETDATE() AS DATE)) and
         f.AccountKey = @AccountKey
  GROUP BY i.LLSPGCode
  
;WITH DefDiscLLSPG AS (
  SELECT al.LLSPG
    FROM #AllResults ad INNER JOIN
	     #AllResults al on ad.LLSPG = al.LLSPG and al.RowType = 'LLSPG' and ad.RowType = 'Detail'
   WHERE al.DefaultDiscount <> 0 and ad.[Action] <> 'Skip'
group by al.LLSPG
having count(*) = sum(case when ad.[Rec'mnd Discount] = -al.DefaultDiscount then 1 else 0 end)
)
--         
-- If all rows for an LLSPG have a recommended discount that equals the default discount, do not create new term (Skip)
--
UPDATE a
   SET a.[Action] = 'Skip'
  FROM #AllResults a INNER JOIN
       DefDiscLLSPG s ON a.LLSPG = s.LLSPG

;WITH MarketTerms AS (
  SELECT a.LLSPG
    FROM #AllResults a
   WHERE a.RowType <> 'LLSPG'
group by a.LLSPG
having count(*) = sum(case when a.[Price Instruction Type/Level] = @MarketInstruction then 1 else 0 end)
)
--         
-- If all detail rows for an LLSPG have a market recommendation and the SPG has no terms, do not create new term (Skip)
--

UPDATE a
   SET a.[Action] = 'Skip'
  FROM #AllResults a INNER JOIN
       MarketTerms s ON a.LLSPG = s.LLSPG

;WITH NoQuals AS (
  SELECT a.LLSPG
    FROM #AllResults a
   WHERE a.RowType <> 'LLSPG'
group by a.LLSPG
having count(*) = sum(case when a.[Price Instruction Type/Level] = @NonQualException then 1 else 0 end)
)
--         
-- If all detail rows for an LLSPG are non-qualifying exceptions and the SPG has no terms, do not create new term (Skip)
--
UPDATE a
   SET a.[Action] = 'Skip'
  FROM #AllResults a INNER JOIN
       NoQuals s ON a.LLSPG = s.LLSPG
 WHERE a.RowType = 'LLSPG' and
       a.UsualDiscount1 IS NULL

;WITH LowSales AS
( SELECT a.LLSPG
    FROM #AllResults a LEFT OUTER JOIN
	     #SixMonthSales s ON a.LLSPG = s.LLSPG
   WHERE a.RowType = 'LLSPG' AND
         a.UsualDiscount1 IS NULL AND 
		 a.Action <> 'Skip' AND
		 NOT ((ISNULL(s.Sales, 0) > 150 AND ISNULL(s.Quantity, 0) > 5) OR ISNULL(s.ItemsSold, 0) >= 5)
)
--         
-- If no terms exist at the SPG level, do not create a new terms recommendation – leave with no terms unless:
--		Sales value is > £150 in last 6 months AND the quantity is greater than 5
--		OR, there are 5 or more items sold in the last 6 months
--		Then recommend a new term
--
UPDATE a
   SET a.[Action] = 'Skip',
       a.[Price Instruction Type/Level] = @LowSalesInstruction
  FROM #AllResults a INNER JOIN
       LowSales s ON a.LLSPG = s.LLSPG


;WITH DeleteCandidates AS
( SELECT a.LLSPG
    FROM #AllResults a LEFT OUTER JOIN
	     #SixMonthSales s ON a.LLSPG = s.LLSPG
   WHERE a.RowType = 'LLSPG' AND
         a.UsualDiscount1 IS NOT NULL AND
         ISNULL(a.[LastChangeDate], '1-JAN-2015') <= DATEADD(dd, -184, CAST(getdate() AS DATE)) AND
		 (ISNULL(s.Sales, 0) < 30 OR (ISNULL(s.Quantity, 0) <= 1 AND ISNULL(s.ItemsSold, 0) <= 1))
)

--
-- Remove an SPG term older than 6 months if:
--	No sales
--	Only one item sold in the last 6 months with quantity of 1
--	OR sales <£30 in the last 6 months
--
UPDATE a
   SET a.[Action] = CASE WHEN (a.RowType = 'LLSPG' OR 
                               a.ExceptionDiscount IS NOT NULL OR 
							   a.ExceptionFixedPrice IS NOT NULL
							  ) AND 
							  ISNULL(a.[Last Change Date], '1-JAN-2015') <= DATEADD(dd, -184, CAST(getdate() AS DATE))
                         THEN 'Delete' 
						 ELSE 'Skip' END,
       a.[Price Instruction Type/Level] = @LowSalesInstruction
  FROM #AllResults a INNER JOIN
       DeleteCandidates d ON a.LLSPG = d.LLSPG

;WITH RecentlyChanged AS
( SELECT a.LLSPG
    FROM #AllResults a
   WHERE a.RowType = 'LLSPG' AND
         CAST(ISNULL(a.[Last Change Date], '1-JAN-2015') AS DATE) > DATEADD(MONTH, -3, CAST(getdate() AS DATE))
)
--
-- Leave existing terms unchanged (Skip) if they have changed within the last three months
--
UPDATE a
   SET a.[Action] = 'Skip',
       a.[Price Instruction Type/Level] = @RecentChange
  FROM #AllResults a INNER JOIN
       RecentlyChanged r ON a.LLSPG = r.LLSPG
 WHERE a.[UsualDiscount1] is not null


 ;WITH SkippedLLSPGs AS (
  SELECT a.LLSPG
    FROM #AllResults a
   WHERE a.RowType <> 'LLSPG'
group by a.LLSPG
having count(*) = sum(case when a.[Action]='Skip' then 1 else 0 end)
)
--         
-- If all detail rows are being skipped and LLSPG does not have an existing term, do not create a term (Skip)
--
UPDATE a
   SET a.[Action] = 'Skip'
  FROM #AllResults a INNER JOIN
       SkippedLLSPGs s ON a.LLSPG = s.LLSPG
 WHERE a.RowType = 'LLSPG' and
       a.UsualDiscount1 is null

-- Delete terms older than 6 months with no sales
-- Skip terms with no sales if they have been in place for less than six months or there is no last change date
;with LLSPGSales as (
  select LLSPG,
         SUM(isnull(r.[Item Sales], 0)) AS [LLSPG Sales]
    from #AllResults r
	group by LLSPG
)
UPDATE r
   SET r.Action = CASE WHEN ISNULL(r.LastChangeDate, GETDATE()) >= DATEADD(MM, -6, GETDATE()) THEN 'Skip' ELSE 'Delete' END
	FROM    #AllResults r INNER JOIN
			LLSPGSales s on r.LLSPG = s.LLSPG
	WHERE   isnull(s.[LLSPG Sales], 0) = 0 or
	        r.RowType = 'LLSPG' and ISNULL(r.[Rec'mnd Discount], 0) = 0 and
			 (select count(*) 
			    from #AllResults ir 
			   where ir.LLSPG = r.LLSPG and
					 ir.RowType = 'Detail' and
					 ir.[Price Instruction Type/Level] in (@ItemInstruction, @LLSPGInstruction, @LowSalesInstruction)) = 0


     SELECT DISTINCT r.LLSPG
	   into #NonPP
       FROM #AllResults r
	  WHERE r.Brand is not null and r.Brand not in ('', 'PLUMB', 'PARTS')
	  -- JIRA LPF-1413: Also exclude certain SPGs from account reviews
	  UNION SELECT DISTINCT ig3.LLSPGCode
	          FROM lion.vwItemGroup3 ig3 
			 WHERE ig3.LLSPGCode IN (
					'DA37',
					'DA40',
					'DA55',
					'DA60',
					'DC31',
					'DC53',
					'DC54',
					'DC55',
					'DG01',
					'DG05',
					'DP92',
					'DW11',
					'DW52',
					'DW59',
					'EV13'
				   )


    SELECT  SUM(IIF(r.Action='Skip',0,r.[Rec'mnd Impact])) OVER (PARTITION BY r.[LLSPG]) AS [LLSPG Impact],
			r.RowType,
			CAST(NEWID() AS NVARCHAR(255)) AS RecID,
			r.AccountKey,
			r.ItemKey,
			r.InvoiceLineGroup1Key,
			r.AccountNumber,
			r.AccountName,
			r.Area,
			r.LLSPG,
			r.[LLSPG Description],
			r.[Product Code],
			r.[Product Description],
			r.[Current Cost],
			r.KVI AS [Prod KVI],
			r.[NDP Flag] AS [Terms NDP Override],
			r.DefaultDiscount AS [Default Discount],
			r.Brand,
			l.[SPG Disc 1],
			l.[SPG Disc 2],
			r.ExceptionDiscount AS [Exception Discount],
			l.Disc AS [SPG Compounded Discount],
			IIF(r.Action = 'Skip', null, r.[Rec'mnd Discount]) as [Rec'mnd Discount],
			r.[Price Instruction Type/Level],
			r.[Action],
			r.[Last Change Date],
			r.[Fixed Price],
			r.[Item Sales] AS [Product Sales],
            SUM(r.[Item Sales]) OVER ( PARTITION BY r.[LLSPG] ) AS [LLSPG Sales] ,
            SUM(r.[Item Sales]) OVER ( PARTITION BY r.[AccountKey] ) AS [Total Account Sales] ,
			r.[Rec'mnd Price],
			r.[Rec'mnd Price Logic],
			r.[Rec'mnd Impact],
			r.[Strategic Item],
			r.[Adjusted Last Item Price],
			r.[Last Item Price],
			r.[Last Price Level (GAR)],
			r.[Last Item Discount],
			r.[Last Item GPP],
			r.[Adjusted Last Item GPP],
			r.[Total Quantity],
			r.[Green Price (TradeAdj)],
			r.[Amber Price],
			r.[Red Price],
			r.[Trade Price],
			r.[Green GPP],
			r.[Amber GPP],
			r.[Red GPP],
			r.[Trade GPP],
			r.[Contract Claims],
			r.[Sales Size],
			r.[Price Approach],
			r.[PB Level],
			r.CustomerTermsKey,
			r.AccountID,
			r.PyramidCode,
			r.SPGCode,
			r.OwningBrandCode,
			r.DeleteFlag,
			r.LastChangeInitials,
			CONVERT(DATE, r.LastChangeDate) AS [Last Terms Change Date],
			r.LastChangeTime,
			r.LastChangePLID,
			r.LastChangeBranch,
			r.ExceptionProduct,
			r.ExceptionFixedPrice AS [Exception Fixed Price],
			r.ExceptionFromDate AS [Fixed Price Start Date],
			r.ExceptionToDate AS [Fixed Price Expiry Date],
			r.ExceptionPCF,
			r.PlaybookPricingGroupKey,
			r.PriceDerivation AS [Last Price Derivation],
			r.LastSaleDate AS [Last Sale Date]
	FROM    #AllResults r LEFT OUTER JOIN
	        #LLSPGTerms l ON r.LLSPG = l.SPGCode LEFT OUTER JOIN
			#NonPP npp ON r.LLSPG = npp.LLSPG
	WHERE   npp.LLSPG IS NULL
	ORDER BY 1 DESC,  -- LLSPGImpact
	         llspg, rowtype DESC, [Price Instruction Type/Level], [Product Code]

DROP TABLE #LLSPGDefaultTerms
DROP TABLE #ProductExceptionDefaultTerms
DROP TABLE #LLSPG
DROP TABLE #LLSPGTerms
DROP TABLE #Prelim
DROP TABLE #PrelimSoldLLSPG
DROP TABLE #PrelimLLSPG
DROP TABLE #ItemTerms
DROP TABLE #Updates
DROP TABLE #NonPP
DROP TABLE #AllResults
DROP TABLE #SixMonthSales








GO
