SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [lion].[P2P_ETL]
	@FullTransactionReprocessFlag [varchar](1) = 'N',
	@RefreshBands [char](1) = 'N',
	@RefreshContractBands [char](1) = 'N'
WITH EXECUTE AS CALLER
AS
SET QUOTED_IDENTIFIER ON 
SET ANSI_NULLS ON 
SET ARITHABORT ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
SET CONCAT_NULL_YIELDS_NULL ON
SET NUMERIC_ROUNDABORT OFF
SET NOCOUNT ON
SET XACT_ABORT ON



DECLARE
	@ETLMsgStr NVARCHAR(4000)

SELECT @ETLMsgStr = N'ETL - P2P_ETL - FullTransactionReprocessFlag: ' + ISNULL(@FullTransactionReprocessFlag, N'') + '; RefreshBands: ' + ISNULL(@RefreshBands, N'') + '; RefreshContractBands: ' + ISNULL(@RefreshContractBands, N'')

EXEC LogDCPEvent @ETLMsgStr, N'B'



UPDATE dbo.ODSStatus
SET 
	StatusValue = 'N',
	StatusDescription = 'Data is currently being processed. Please try again later.'
	--,
	--ApplicationLogOutDateTime = GETDATE()




EXEC lion.DetermineCreditMatches

EXEC lion.ScrubStagingData


--SELECT * 
--FROM lion.vwRawDataExclusionReport
--ORDER by 1,2


-------1--------
EXEC dbo.MVIEWS_Drop
--EXEC dbo.MVIEWS_Results_Drop
EXEC dbo.MVIEWS_ProposalPrice_Drop

-------2--------
--EXEC dbo.Load_DimDay  --Days should already be loaded

-------3--------
--EXEC dbo.Load_DimMonth --Months should already be loaded

EXEC lion.Load_DimBrand

EXEC lion.Load_DimNetwork
EXEC lion.Load_DimArea
EXEC lion.Load_DimRegion

-------4--------
EXEC lion.Load_DimAccountGroup1 --This is the geographic (location) grouping

-------5--------
EXEC lion.Load_DimAccountGroup2 --This is the CustomerType type

-------6--------
EXEC lion.Load_DimAccountGroup3 --This is unused

-------6--------
EXEC lion.Load_DimAccountGroup4 --This is unused


/* --- with Price Flow (PF/PPE), users can be added via the application; thus we needed to move user processing around (and not early during ETL time)
-------7.0------
EXEC Lion_Staging.dbo.AdaptorUser

EXEC lion.Load_ODSPerson

-------8--------
EXEC lion.Load_DimPerson


EXEC lion.Load_WebUserDelegate

-------9--------
EXEC lion.Load_BridgeAccountManager	--Load the bridge based on UserAssoc. This is just the parent/child managed by relationship between account managers

--This takes all of the View/Modify relationships for each user and assigs either View or Modify to each distinct account manager in the hierarchy
--This also needs to run any time user accsss is changed, or at least for the users it was changed for!
EXEC lion.Load_WebUserAccountManagerAccessType
*/


-------10-------
--Loads all accounts.  Each account must have exactly one AccountManager (can be unknown)
EXEC lion.Load_DimAccount

EXEC lion.Load_DimAccountBrandSegment


-------11-------
EXEC lion.Load_UpdateDimAccountManager	-- Sets the PrimaryAccountGroup1Key in DimAccountManager

-------12-------
EXEC lion.Load_DimVendor

-------13-------
EXEC lion.Load_DimItemGroup1 --This is ProdBrandAtLLSPG

-------14-------
EXEC lion.Load_DimItemGroup2 --This is the commodity code

-------15-------
EXEC lion.Load_DimItemGroup3 --This is not used for lion

-------16-------
EXEC lion.Load_DimItem

EXEC lion.Load_ItemUOMConversion


-------17-------
EXEC lion.Load_DimInvoiceLineGroup1 -- ShipmentType grouping for lion

-------18-------
EXEC lion.Load_DimInvoiceLineGroup2 -- PriceCode grouping for bulldog

-------19-------
EXEC lion.Load_DimInvoiceLineGroup3 --Not used by bulldog

--------------
EXEC lion.Load_DimInvoiceLineJunk @FullTransactionReprocessFlag

-----20-------
EXEC lion.Load_FactInvoiceLine @FullTransactionReprocessFlag

-----21-------
-----21.1-----
--EXEC dbo.MVIEWS_Create

-------21.2-----
EXEC dbo.DBA_UpdateStatistics

-------22-------
EXEC dbo.Load_RollingPeriods




------- -------
--EXEC dbo.MVIEWS_ProposalPrice_Create


------- -------
--EXEC lion.Load_AccountGroup3Item


-------24-------
EXEC lion.Load_FactLastPriceAndCost


-------25.2-----
--EXEC dbo.Create_Additional_Indexes

-------25.1-----
EXEC dbo.DBA_UpdateStatistics

-----26-------
--EXEC dbo.Load_FactLastYearMonthlySummary	-- LP2P-515 ETL Improvement - Remove Sales Summary Step 


-----27-------
EXEC dbo.Load_FactSalesForecast

-----28-------
EXEC dbo.Load_FactCoreNonCore

--------------
EXEC lion.Load_FactAccountItemGroup3


/*** REFRESH PRICE BANDS ***/
IF @RefreshBands = 'Y'
BEGIN
	EXEC lion.P2P_RunProjectSpecification 21, 'N'

	UPDATE PlaybookProject 
	SET ProductionProjectIndicator = 'Y' 
	WHERE ApproachTypeKey = 7

	UPDATE [Param] SET ParamValue = 'N' WHERE ParamName = 'RefreshBands'

END



--------------
EXEC lion.Load_RPBPlaybookPricingGroupKey 'FactLastPriceAndCost', 'RPBPlaybookPricingGroupKey', 'AlternateRPBPlaybookPricingGroupKey', NULL, 'Y'



EXEC lion.Load_PlaybookPricingGroupPriceBandActive


EXEC lion.Load_FactRecommendedPriceBand


EXEC lion.Load_ATKManualPriceBand

--------------
--EXEC lion.Load_FactCurrentPriceAndCost


/* Scoring */
--EXEC dbo.Load_FactAISScore
EXEC lion.Load_FactInvoiceLinePricebandScore --@FullTransactionReprocessFlag


--EXEC lion.Load_CustomerClassValuesForDimAccount
/* Load Margin Erosion  */

--EXEC lion.Load_FactMarginErosion

EXEC lion.Load_CustomerTerms

EXEC lion.Load_AccountContractTerms

EXEC lion.Load_AccountSPG

EXEC lion.Load_CompetitorPricing

EXEC lion.Load_PricingContract

EXEC lion.Load_FactMarginErosion

EXEC dbo.DBA_RemainingFKRebuild
EXEC dbo.DBA_UpdateStatistics



--EXEC cc.Load_WUKContracts  --Per LPF-3691 now being called within ppe.PPE_ETL

/*** REFRESH PRICE BANDS ***/
IF @RefreshContractBands = 'Y'
BEGIN

	EXEC lion.P2P_RunProjectSpecification 17, 'Y'

	UPDATE PlaybookProject 
	SET ProductionProjectIndicator = 'Y' 
	WHERE ApproachTypeKey = 12

	UPDATE [Param] SET ParamValue = 'N' WHERE ParamName = 'RefreshContractBands'

END


--EXEC ppe.Set_RequestHierarchyChange
--EXEC ppe.Set_RequestAutoEscalate



/*** REFRESH PRICE BANDS ***/
IF @RefreshBands = 'Y'
BEGIN
	EXEC lion.Load_RPBPriceBandAttributes
END


--/*** REFRESHING PRICE BANDS ***/
--IF @RefreshBands = 'Y'
--BEGIN

--	DECLARE @SQLCmd NVARCHAR(4000)

--	EXEC LogDCPEvent 'DCP --- RefreshBands: vwBandedAISReview', 'B'
--	SELECT @SQLCmd = 'SELECT * INTO lion.RBP_Iteration_BandedAISReview_' + CONVERT(VARCHAR(10), GETDATE(), 112) + ' FROM lion.vwBandedAISReview'
--	EXECUTE sp_executesql @SQLCmd
--	EXEC LogDCPEvent 'DCP --- RefreshBands: vwBandedAISReview', 'E'

--	EXEC LogDCPEvent 'DCP --- RefreshBands: vwBandDatapointReview', 'B'
--	SELECT @SQLCmd = 'SELECT * INTO lion.RBP_Iteration_BandDatapointReview_' + CONVERT(VARCHAR(10), GETDATE(), 112) + ' FROM lion.vwBandDatapointReview'
--	EXECUTE sp_executesql @SQLCmd
--	EXEC LogDCPEvent 'DCP --- RefreshBands: vwBandDatapointReview', 'E'

--END


/* Final Cleanup */
EXEC dbo.DBA_RemainingFKRebuild
EXEC dbo.DBA_UpdateStatistics


--; WITH a AS (
--	SELECT GETDATE() AS CurrentDateTime		-- did not want to risk getdate() returning a different value in the update below
--)
UPDATE s
SET 
	StatusValue = 'Y',
	StatusDescription = 'The system is ready for use.',
	MaxInvoiceDate = (SELECT SQLDate AS MaxTransactionDate FROM dbo.DimDay WHERE DayKey = (SELECT MAX(InvoiceDateDayKey) FROM dbo.FactInvoiceLine))
	--,
	--ApplicationLogOutDateTime = 
	--	CASE
	--		WHEN CONVERT(NVARCHAR(10),CurrentDateTime, 121) = CONVERT(NVARCHAR(10), ApplicationLogOutDateTime, 121) THEN -- '( CURRENT_DATE + 1 DAY ) at 6:00pm  -- (day + 1) + fixed time'
	--			CASE -- if the new date would be on a Saturday or Sunday, then set the time to 1PM (else 6PM
	--				WHEN DATEPART(dw, DATETIMEFROMPARTS( YEAR(CurrentDateTime), MONTH(CurrentDateTime), DAY(CurrentDateTime) + 1, 18, 00, 00, 0 )) IN (1,7) THEN 
	--					DATETIMEFROMPARTS( YEAR(CurrentDateTime), MONTH(CurrentDateTime), DAY(CurrentDateTime) + 1, 13, 00, 00, 0 )
	--				ELSE
	--					DATETIMEFROMPARTS( YEAR(CurrentDateTime), MONTH(CurrentDateTime), DAY(CurrentDateTime) + 1, 18, 00, 00, 0 )
	--			END
	--		ELSE --'current date + fixed time'
	--			CASE -- if the new date would be on a Saturday or Sunday, then set the time to 1PM (else 6PM
	--				WHEN DATEPART(dw, DATETIMEFROMPARTS( YEAR(CurrentDateTime), MONTH(CurrentDateTime), DAY(CurrentDateTime), 18, 00, 00, 0 )) IN (1,7) THEN 
	--					DATETIMEFROMPARTS( YEAR(CurrentDateTime), MONTH(CurrentDateTime), DAY(CurrentDateTime), 13, 00, 00, 0 )
	--				ELSE
	--					DATETIMEFROMPARTS( YEAR(CurrentDateTime), MONTH(CurrentDateTime), DAY(CurrentDateTime), 18, 00, 00, 0 )
	--			END
	--	END
FROM dbo.ODSStatus s--, a



EXEC LogDCPEvent @ETLMsgStr, N'E'







GO
