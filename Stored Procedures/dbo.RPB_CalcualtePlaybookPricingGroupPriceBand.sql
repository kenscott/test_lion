SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




CREATE PROCEDURE [dbo].[RPB_CalcualtePlaybookPricingGroupPriceBand]
	@ProjectKey Key_Normal_type,
	@PlaybookDataPointGroupKey Key_Normal_type
AS

/*

EXEC dbo.RPB_CalcualtePlaybookPricingGroupPriceBand 7, 6
EXEC dbo.RPB_CalcualtePlaybookPricingGroupPriceBand 11, 10

*/

SET NOCOUNT ON

EXEC LogDCPEvent 'DCP --- dbo.RPB_CalcualtePlaybookPricingGroupPriceBand', 'B', 0, 0, 0, @ProjectKey


DECLARE
	@RowsI Quantity_Normal_type, 
	@RowsU Quantity_Normal_type, 
	@RowsD Quantity_Normal_type


SET @RowsI = 0
SET @RowsU = 0
SET @RowsD = 0


DELETE FROM PlaybookPricingGroupPriceBand
WHERE PlaybookPricingGroupKey IN (SELECT DISTINCT PlaybookPricingGroupKey FROM dbo.PlaybookPricingGroup WHERE PlaybookDataPointGroupKey = @PlaybookDataPointGroupKey)
SET @RowsD = @RowsD + @@ROWCOUNT


INSERT dbo.PlaybookPricingGroupPriceBand (
	PlaybookPricingGroupKey,
	LowerBandQuantity,
	UpperBandQuantity,
	PDPGPP,
	PDPDiscountPercent,
	MemberRank,
	MemberRankDiscount,
	RulingMemberCount,
	BandSequenceNumber,
	PlaybookPricingGroupQuantityBandKey,
	PricingRuleSequenceNumber
)
SELECT 
		pdppg.PlaybookPricingGroupKey,
		LowerBandQuantity,
		UpperBandQuantity,
		(pdppg.LastItemPrice-pdppg.LastItemCost)/pdppg.LastItemPrice AS PDPGPP,
		pdppg.LastItemDiscountPercent,
		ROW_NUMBER() OVER (PARTITION BY pdppg.PlaybookPricingGroupKey, BandSequenceNumber ORDER BY (pdppg.LastItemPrice-pdppg.LastItemCost)/pdppg.LastItemPrice ASC) AS MemberRank,
		ROW_NUMBER() OVER (PARTITION BY pdppg.PlaybookPricingGroupKey, BandSequenceNumber ORDER BY pdppg.LastItemDiscountPercent DESC) AS MemberRankDiscount,
		RulingMemberCount,
		BandSequenceNumber,
		PlaybookPricingGroupQuantityBandKey,
		PricingRuleSequenceNumber
FROM PlaybookDataPointPricingGroup pdppg
INNER JOIN dbo.ATKPricingRule apr
	ON apr.PricingRuleKey = pdppg.PricingRuleKey
INNER JOIN dbo.PlaybookDataPoint pdp
	ON pdp.PlaybookDataPointKey = pdppg.PlaybookDataPointKey
INNER JOIN dbo.PlaybookPricingGroupQuantityBand ppgqb
	ON ppgqb.PlaybookPricingGroupKey = pdppg.PlaybookPricingGroupKey
	AND pdppg.Total12MonthQuantity 
		BETWEEN ppgqb.LowerBandQuantity
			AND ppgqb.UpperBandQuantity 	--gets the datapoints with their associated bands
WHERE 
	PlaybookDataPointGroupKey = @PlaybookDataPointGroupKey
	AND RulingMemberFlag = 'Y'
	AND pdppg.LastItemPrice <> 0.0
SET @RowsI = @RowsI + @@ROWCOUNT


EXEC LogDCPEvent 'DCP --- dbo.RPB_CalcualtePlaybookPricingGroupPriceBand', 'E', @RowsI, @RowsU, @RowsD, @ProjectKey
GO
