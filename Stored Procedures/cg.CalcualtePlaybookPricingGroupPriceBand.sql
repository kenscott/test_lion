SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





CREATE PROCEDURE [cg].[CalcualtePlaybookPricingGroupPriceBand]
	@ProjectKey Key_Normal_type,
	@PlaybookDataPointGroupKey Key_Normal_type
AS

/*

EXEC cg.CalcualtePlaybookPricingGroupPriceBand 40, 40

*/

SET NOCOUNT ON

EXEC LogDCPEvent 'DCP --- cg.CalcualtePlaybookPricingGroupPriceBand', 'B', 0, 0, 0, @ProjectKey


DECLARE
	@RowsI INT, 
	@RowsU INT, 
	@RowsD INT

	--select top 10 * from cg.PlaybookPricingGroup (nolock)
SET @RowsI = 0
SET @RowsU = 0
SET @RowsD = 0


DELETE FROM cg.PlaybookPricingGroupPriceBand
WHERE PlaybookPricingGroupKey IN (SELECT DISTINCT PlaybookPricingGroupKey FROM dbo.PlaybookPricingGroup WHERE PlaybookDataPointGroupKey = @PlaybookDataPointGroupKey)
SET @RowsD = @RowsD + @@ROWCOUNT

	
INSERT cg.PlaybookPricingGroupPriceBand (
	PlaybookPricingGroupKey,
	PlaybookDataPointKey,
	ClaimBackPercent,
	MemberRank,
	RulingMemberCount,
	PricingRuleSequenceNumber
)
SELECT 
		pdppg.PlaybookPricingGroupKey,
		pdp.PlaybookDataPointKey,
		pdppg.ClaimBackPercent,
		ROW_NUMBER() OVER (PARTITION BY pdppg.PlaybookPricingGroupKey ORDER BY pdppg.ClaimBackPercent ASC) AS MemberRank,
		SUM(CASE
				WHEN RulingMemberFlag = 'Y' THEN 1
				ELSE 0
			END
		) OVER ( PARTITION BY pdppg.PlaybookPricingGroupKey ) AS RulingMemberCount,
		apr.PricingRuleSequenceNumber
FROM cg.PlaybookDataPointPricingGroup pdppg
INNER JOIN dbo.ATKPricingRule apr
	ON apr.PricingRuleKey = pdppg.PricingRuleKey
INNER JOIN cg.PlaybookDataPoint pdp
	ON pdp.PlaybookDataPointKey = pdppg.PlaybookDataPointKey
WHERE 
	PlaybookDataPointGroupKey = @PlaybookDataPointGroupKey
	AND RulingMemberFlag = 'Y'
	--AND pdppg.ClaimBackPercent <> 0.0
SET @RowsI = @RowsI + @@ROWCOUNT


EXEC LogDCPEvent 'DCP --- cg.CalcualtePlaybookPricingGroupPriceBand', 'E', @RowsI, @RowsU, @RowsD, @ProjectKey

GO
