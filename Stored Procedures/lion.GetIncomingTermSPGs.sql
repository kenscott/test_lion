SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		José A. Castaños
-- Create date: 17-Feb-2015
-- Description:	Gets SPG information
-- =============================================
CREATE PROCEDURE [lion].[GetIncomingTermSPGs]
AS
BEGIN
	SET NOCOUNT ON;

	;WITH SPGTerms as (
			select *
			  from lion.IncomingTerms it
			 where it.Exported = 0 AND
				   it.RowType = 'LLSPG' AND 
				   it.[New SPG Discount 1] IS NOT NULL AND
				   ISNUMERIC(it.[New SPG Discount 1]) = 1
		)

	,ExceptionTerms as (
		select *
		  from lion.IncomingTerms it
		 where it.Exported = 0 AND
			   it.RowType = 'Detail' AND 
				[New Exception Discount] IS NOT NULL AND 
				ISNUMERIC([New Exception Discount]) = 1
		)

	SELECT
			st.LLSPG,
			st.[LLSPG Description],
			count(*) as [# of Terms]
	FROM SPGTerms st left outer join
	     ExceptionTerms et on st.AccountKey = et.AccountKey and
		                      st.LLSPG = et.LLSPG and
							  st.Filename = et.Filename inner join
		 lion.vwAccount a on st.AccountKey = a.AccountKey inner join
		 lion.vwAccountGroup1 ag1 on a.Branch = ag1.Branch
	GROUP BY 
			st.LLSPG,
			st.[LLSPG Description]

END

GO
