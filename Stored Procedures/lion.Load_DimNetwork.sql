SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [lion].[Load_DimNetwork]
WITH EXECUTE AS CALLER
AS
SET NOCOUNT ON


EXEC LogDCPEvent 'ETL - Load_DimNetwork', 'B'


DECLARE 
	@RowsI INT, 
	@RowsU INT, 
	@RowsD INT,
	@BeginRowCount INT,
	@TodayDayKey Key_Small_type

SET @RowsI = 0
SET @RowsU = 0
SET @RowsD = 0

SET @TodayDayKey = (SELECT DayKey FROM DimDay WHERE CONVERT(VARCHAR(10), SQLDate, 120) = CONVERT(VARCHAR(10), GETDATE(), 120))


-- EXEC DBA_IndexRebuild 'DimNetwork', 'Drop'
-- TRUNCATE TABLE dbo.DimNetwork
-- EXEC DBA_IndexRebuild 'DimNetwork', 'Create'


IF NOT EXISTS (SELECT 1 FROM dbo.DimNetwork WHERE NetworkID = 'Unknown')
BEGIN
	INSERT dbo.DimNetwork (
		NetworkID,
		NetworkDescription,
		CreationDayKey, 
		ModificationDayKey)
	VALUES (
		'Unknown',
		'Unknown', 
		@TodayDayKey, 
		@TodayDayKey)
	SET @RowsI = @RowsI + @@RowCount
END


SELECT @BeginRowCount = COUNT(*) FROM dbo.DimNetwork

; WITH StagingData AS (
	SELECT DISTINCT
		ISNULL(NetworkID, N'Unknown') AS NetworkID,
		ISNULL(NetworkDesc, N'Unknown') AS NetworkDescription,
		ROW_NUMBER() OVER (PARTITION BY ISNULL(NetworkID, N'Unknown') ORDER BY ISNULL(NetworkDesc, N'Unknown')) AS RowRank
	FROM Lion_Staging.dbo.BusUnit b
	WHERE
		b.BranchCode IS NOT NULL
		AND b.BranchCode <> N'NULL'
)
MERGE dbo.DimNetwork AS Tgt
USING 
	(
	SELECT 
		NetworkID,
		NetworkDescription
	FROM StagingData
	WHERE
		RowRank = 1
) AS Src
ON Tgt.NetworkID = Src.NetworkID
WHEN MATCHED AND Tgt.NetworkDescription <> Src.NetworkDescription
THEN UPDATE 
	SET
		Tgt.ModificationDayKey = @TodayDayKey,
		Tgt.NetworkDescription = Src.NetworkDescription
WHEN NOT MATCHED 
	THEN INSERT (
		NetworkID,
		NetworkDescription,
		CreationDayKey, 
		ModificationDayKey
	)
	VALUES (
		NetworkID,
		NetworkDescription,
		@TodayDayKey, 
		@TodayDayKey
	)
;

SELECT 
	@RowsU = @RowsU + (@@ROWCOUNT - (COUNT(*) - @BeginRowCount)),
	@RowsI = @RowsI + (COUNT(*) - @BeginRowCount)
FROM dbo.DimNetwork


SELECT TOP 10 * FROM dbo.DimNetwork

EXEC LogDCPEvent 'ETL - Load_DimNetwork', 'E', @RowsI, @RowsU, @RowsD








GO
