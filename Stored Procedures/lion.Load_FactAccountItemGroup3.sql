SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [lion].[Load_FactAccountItemGroup3]
WITH EXECUTE AS CALLER
AS

/*

EXEC lion.Load_FactAccountItemGroup3

*/




EXEC LogDCPEvent 'ETL - lion.Load_FactAccountItemGroup3', 'B'

EXEC dba_indexrebuild 'FactAccountItemGroup3', 'Drop'

TRUNCATE TABLE dbo.FactAccountItemGroup3


DECLARE 
	@RowsI INT, 
	@RowsU INT, 
	@RowsD INT

SET @RowsI = 0
SET @RowsU = 0
SET @RowsD = 0


DECLARE @MaxInvoiceDateDayKey Key_Small_type

SELECT @MaxInvoiceDateDayKey = MAX(InvoiceDateDayKey)
FROM dbo.FactInvoiceLine 
WHERE Last12MonthsIndicator = 'Y'


INSERT dbo.FactAccountItemGroup3
(
	AccountKey,
	ItemGroup3Key,
	ContractClaimsPercent
)
SELECT
	AccountKey,
	ItemGroup3Key,
	--ISNULL(SUM(fil.UDDecimal2) / NULLIF(SUM(fil.TotalActualPrice), 0.), 0.) AS PercentContractClaims
	ISNULL(
		SUM(
			CASE
				WHEN ISNULL(fil.UDVarchar3, N'') <> N'' THEN fil.TotalActualPrice
				ELSE 0.
			END)
		/
		NULLIF(SUM(fil.TotalActualPrice), 0.)
	, 0.) AS PercentContractClaims
FROM 
	dbo.FactInvoiceLine fil
WHERE
	fil.InvoiceDateDayKey >= (@MaxInvoiceDateDayKey - 59)
GROUP BY
	AccountKey,
	ItemGroup3Key
SET @RowsI = @RowsI + @@RowCount


SELECT TOP 10 * FROM FactAccountItemGroup3

EXEC dba_indexrebuild 'FactAccountItemGroup3', 'Create'

EXEC LogDCPEvent 'ETL - lion.Load_FactAccountItemGroup3', 'E', @RowsI, @RowsU, @RowsD




GO
