SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [lion].[Load_WebUserDelegate]

AS
/*
EXEC lion.Load_WebUserDelegate
*/

SET NOCOUNT ON

DECLARE @RowsI Quantity_Normal_type, @RowsU Quantity_Normal_type, @RowsD Quantity_Normal_type
DECLARE @NullDimPersonKey Key_Normal_type

SET @RowsI = 0
SET @RowsU = 0
SET @RowsD = 0


EXEC LogDCPEvent 'ETL - Load_WebUserDelegate', 'B'



BEGIN TRAN


-- ensure everyone is a delegate of themselves
INSERT WebUserDelegate (DelegatorWebUserKey, DelegateWebUserKey, StartDate, EndDate, CreatedByWebUserKey, EmailStatus)
SELECT WebUserKey, WebUserKey, GETDATE(), CAST('9999-12-31' AS DATETIME), WebUserKey, 'N'
FROM dbo.WebUser wu
WHERE NOT EXISTS (SELECT 1 FROM dbo.WebUserDelegate d WHERE DelegatorWebUserKey = wu.WebUserKey OR d.DelegateWebUserKey = wu.WebUserKey)
SET @RowsI = @RowsI + @@ROWCOUNT


DECLARE @UpdatedRowData TABLE (
	DelegatorWebUserKey INT NOT NULL,
	DelegateWebUserKey INT NOT NULL,
	StartDate DATETIME NULL,
	EndDate DATETIME NULL
	)


DELETE wud -- per  LIO-587
	OUTPUT 
		DELETED.DelegatorWebUserKey,
		DELETED.DelegateWebUserKey,
		DELETED.StartDate,
		DELETED.EndDate
	INTO @UpdatedRowData
FROM dbo.WebUserDelegate wud
JOIN dbo.WebUser wu1 ON wu1.WebUserKey = wud.DelegatorWebUserKey
JOIN dbo.WebUser wu2 ON wu2.WebUserKey = wud.DelegateWebUserKey
WHERE wud.DelegatorWebUserKey <> wud.DelegateWebUserKey
AND ((wu2.AccountManagerKey /*delegate is not a peer with delegator */
			NOT IN (SELECT SubsidiaryAccountManagerKey AS AccountManagerKey 
					FROM dbo.BridgeAccountManager
					WHERE ParentAccountManagerKey=
						(SELECT ParentAccountManagerKey 
						FROM dbo.BridgeAccountManager bam
						WHERE bam.SubsidiaryAccountManagerKey = wu1.AccountManagerKey AND NumberOfLevels = 1))
			AND wu2.AccountManagerKey /*delegate is not a subordinate of delegator */
				NOT IN (SELECT SubsidiaryAccountManagerKey FROM dbo.BridgeAccountManager WHERE ParentAccountManagerKey = wu1.AccountManagerKey )
			AND wu2.AccountManagerKey /*delegate is not a manager of delegator */
				NOT IN (SELECT ParentAccountManagerKey FROM dbo.BridgeAccountManager WHERE SubsidiaryAccountManagerKey = wu2.AccountManagerKey ) 
						AND wu2.AccountManagerKey /*delegate is in the hiearchy per Toolson Wed 8/26/2015 2:42 PM email
										 (ie delete if all above is true and they are in hierarchy or, said another way, don't delete if 
										 they are not in the hierarchy */
				IN (SELECT ParentAccountManagerKey FROM dbo.BridgeAccountManager UNION SELECT SubsidiaryAccountManagerKey FROM dbo.BridgeAccountManager)  )
	OR wu1.AccountManagerKey /*delegator not in hiearchy */
		NOT IN (SELECT ParentAccountManagerKey FROM dbo.BridgeAccountManager UNION SELECT SubsidiaryAccountManagerKey FROM dbo.BridgeAccountManager) )
SET @RowsD = @RowsD + @@ROWCOUNT


INSERT dbo.WebUserDelegateLog (
	DelegatorWebUserKey,
	DelegateWebUserKey,
	ActorWebUserKey,
	[Action],
	StartDate,
	EndDate,
	EmailStatus)
SELECT
	DelegatorWebUserKey,
	DelegateWebUserKey,
	1 AS ActorWebUserKey,
	'DELETE' AS [Action],
	StartDate,
	EndDate,
	'P' AS EmailStatus
FROM @UpdatedRowData
SET @RowsI = @RowsI + @@ROWCOUNT


COMMIT TRAN



EXEC LogDCPEvent 'ETL - Load_WebUserDelegate', 'E', @RowsI, @RowsU, @RowsD

















GO
