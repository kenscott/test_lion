SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE                                 PROCEDURE [dbo].[P2P_RBP]
@ProjectKey INT, 
@ScenarioKey INT,
@PlaybookDataPointGroupKey INT,
@LimitDataPlaybookKey INT,
@SegmentFactorPlaybookKey INT, 
@PricingRulePlaybookKey INT, 
@ProposalRecommendationPlaybookKey INT,
@BeginStep INT,
@EndStep INT
AS
SET QUOTED_IDENTIFIER ON 
SET ANSI_NULLS ON 
SET ARITHABORT ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
SET CONCAT_NULL_YIELDS_NULL ON
SET NUMERIC_ROUNDABORT OFF
SET nocount ON
SET xact_abort ON

--Determines if this is a pricing or costing scenario
DECLARE @ApproachTypeKey INT
SELECT @ApproachTypeKey = ApproachTypeKey FROM ATKScenario WHERE ScenarioKey = @ScenarioKey

DECLARE @ApproachTypeDesc NVARCHAR(500)
SELECT @ApproachTypeDesc = ApproachTypeDesc FROM ATKApproachType WHERE ApproachTypeKey = @ApproachTypeKey



IF @BeginStep <= 1 AND @EndStep >= 1
BEGIN
	EXEC dbo.RBP_PopulatePlaybookDataPointGroupScope  @PlaybookDataPointGroupKey, @LimitDataPlaybookKey
END

IF @BeginStep <= 2 AND @EndStep >= 2
BEGIN
	EXEC dbo.RBP_PopulatePlaybookDataPointBands  @PlaybookDataPointGroupKey, @SegmentFactorPlaybookKey
END

IF @BeginStep <= 3 AND @EndStep >= 3
BEGIN
	EXEC dbo.RBP_PopulatePlaybookDataPoint @PlaybookDataPointGroupKey
END


IF @BeginStep <= 4 AND @EndStep >= 4
BEGIN
	EXEC dbo.RBP_PopulateProjectSummary @ProjectKey
END


IF @BeginStep <= 5 AND @EndStep >= 5
BEGIN
	EXEC dbo.RBP_SetPriceGroup @PlaybookDataPointGroupKey, @PricingRulePlaybookKey, @ScenarioKey
END


IF @BeginStep <= 6 AND @EndStep >= 6
BEGIN
	EXEC dbo.RBP_CalculatePricingGroupOptimals @PlaybookDataPointGroupKey
-- 	If @ApproachTypeDesc = 'Reality Based Pricing'
-- 	Begin
-- 		Exec dbo.RBP_PriceQuantityBandSmoothing @PlaybookDataPointGroupKey
-- 	End


END

IF @BeginStep <= 7 AND @EndStep >= 7
BEGIN
	EXEC dbo.RBP_AssignOptimalPrice @PlaybookDataPointGroupKey
END


IF @BeginStep <= 8 AND @EndStep >= 8
BEGIN
	EXEC dbo.RBP_PopulatePlaybookPriceProposal @PlaybookDataPointGroupKey, @ProposalRecommendationPlaybookKey
END




GO
