SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE procedure [lion].[GetISDBaselineGPP]
as 

SET NOCOUNT ON


select distinct acct.AccountKey, item.ItemKey, ilg1.InvoiceLineGroup1Key
into #Selector
FROM
  lion.vwAccount  acct INNER JOIN lion.vwFactInvoiceLinePricebandScore  filpbs ON (acct.AccountKey=filpbs.AccountKey)
   INNER JOIN lion.vwFactInvoiceLineGroup1  ilg1 ON (ilg1.InvoiceLineGroup1Key=filpbs.InvoiceLineGroup1Key)
   INNER JOIN lion.vwAccountGroup1  ag1 ON (ag1.AccountGroup1Key=filpbs.AccountGroup1Key)
   INNER JOIN lion.BaselineCT  bct ON (bct.Branch=ag1.Branch)
   INNER JOIN lion.PilotSchedule  ps ON (ag1.Branch=ps.Branch)
   INNER JOIN lion.vwDimDay  dd ON (dd.DayKey=filpbs.InvoiceDateDayKey)
   INNER JOIN lion.vwItem  item ON (item.ItemKey=filpbs.ItemKey and
item.VendorKey=filpbs.VendorKey)
   INNER JOIN dbo.DimInvoiceLineJunk  dilj ON (dilj.InvoiceLineJunkKey=filpbs.InvoiceLineJunkKey)
   inner join lion.vwFactLastPriceAndCost flpac on flpac.AccountKey = filpbs.AccountKey and flpac.ItemKey = filpbs.ItemKey and flpac.InvoiceLineGroup1Key = filpbs.InvoiceLineGroup1Key
   LEFT OUTER JOIN lion.FactBaselineAISDSalesBranch  fbasb ON (filpbs.AccountKey=fbasb.AccountKey and
filpbs.ItemKey=fbasb.ItemKey and
filpbs.InvoiceLineGroup1Key=fbasb.InvoiceLineGroup1Key and
filpbs.AccountGroup1Key=fbasb.AccountGroup1Key)
   LEFT OUTER JOIN lion.FactBaselineISDSalesBranch  fbisb ON (filpbs.ItemKey=fbisb.ItemKey and
filpbs.InvoiceLineGroup1Key=fbisb.InvoiceLineGroup1Key and 
filpbs.AccountGroup1Key=fbisb.AccountGroup1Key)
   LEFT OUTER JOIN lion.FactBaselineISD  fbi ON (filpbs.ItemKey=fbi.ItemKey and
filpbs.InvoiceLineGroup1Key=fbi.InvoiceLineGroup1Key
)
  
WHERE
  (
   dd.SQLDate  >=  '11/01/2014 00:0:0'
   AND
   ( item.ItemPyramidCode = 1  AND acct.AccountName <> 'Unknown' AND item.ItemNumber <> 'Unknown' AND item.ItemObsolete <> 'Y' AND item.SpecialInd <> 'Y' AND ilg1.DeviatedIndicator <> 'Y' AND ilg1.ShipmentType <> 'D' AND filpbs.TotalQuantity <> 0 AND filpbs.TotalActualPrice <> 0 AND filpbs.PriceDerivation not in ('SPO', 'CCP') AND acct.BranchPrimaryBrand <> 'BURDN'  )
   AND
   ( ((ps.BranchType = 'Pilot' and dd.SQLDate >= ps.LiveDate) or ps.BranchType = 'Control')  )
  )

select i.ItemNumber, ilg1.ShipmentType, ilg1.DeviatedIndicator as ContractClaims, Sales, Cost, GP, BaselineGPP 
from lion.FactBaselineISD f inner join
lion.vwitem i on f.ItemKey = i.ItemKey inner join
lion.vwFactInvoiceLineGroup1 ilg1 on f.InvoiceLineGroup1Key = ilg1.InvoiceLineGroup1Key inner join
(select distinct itemkey, invoicelinegroup1key from #Selector) s on s.itemkey  = i.itemkey and s.invoicelinegroup1key = ilg1.InvoiceLineGroup1Key

drop table #Selector
GO
