SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE [lion].[Load_PricingContract]
WITH EXECUTE AS CALLER
AS
SET NOCOUNT ON


EXEC LogDCPEvent 'ETL - Load_PricingContract', 'B'


DECLARE 
	@RowsI INT, 
	@RowsU INT

SET @RowsI = 0
SET @RowsU = 0

--Set Inactive flag for records not in staging
UPDATE lion.PricingContract
SET Inactive = 1
WHERE PricingContractKey IN (
SELECT c.PricingContractKey
FROM lion.PricingContract c
LEFT JOIN Lion_Staging.dbo.PricingContract s
ON s.ContractOwner = c.PricingContractOwner
AND s.ContractName = c.PricingContractName
WHERE s.ContractName IS NULL
)

SELECT @RowsU = @@ROWCOUNT


--Insert new records
INSERT INTO lion.PricingContract
	(PricingContractOwner,
	PricingContractName) 
SELECT DISTINCT s.ContractOwner, s.ContractName
FROM Lion_Staging.dbo.PricingContract s
LEFT JOIN lion.PricingContract c
ON c.PricingContractOwner = s.ContractOwner
AND c.PricingContractName = s.ContractName
WHERE c.PricingContractKey IS NULL

SELECT @RowsI = @@ROWCOUNT


SELECT TOP 10 * FROM lion.PricingContract

EXEC LogDCPEvent 'ETL - Load_PricingContract', 'E', @RowsI, @RowsU, 0










GO
