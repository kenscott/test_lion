SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO







CREATE PROCEDURE [cc].[Get_WebUserContractBranches](@WebUserKey INT )
/*
Purpose: Provide a list of branches to be used on the Contract Manager Header Tab when selecting the contract owner.
	
Application:  This procedure was intended for use on the Contract Manager Header Tab to populate the contract
owner drop-down.

Examples:
	exec cc.get_WebUserContractBranches 1934 
	exec lion.get_WebUserContractBranches 2082
	
	select delegatewebuserkey, count(*) from webuserdelegate  group by delegatewebuserkey having count(*) > 1
	select * from webrole
	
	select count(*), converted from dimaccountgroup1 group by converted

		
*/

AS 

SET NOCOUNT ON

--Contract Coordinator
IF 
	@WebUserKey IN
		( SELECT WebUserKey
			FROM WebUserRole wur
			JOIN WebRole wr ON wr.WebRoleKey = wur.WebRoleKey
		    WHERE wr.WebRoleName = 'ContractCoordinator'   
	)
BEGIN
	SELECT DISTINCT
		2 AS SortOrder,
		AccountGroup1Key AS PrimaryAccountGroup1Key, 
		--wud.DelegatorWebUserKey,
		AG1Level1 AS Branch, 
		AG1Level1UDVarchar1 AS BranchName, 
		Converted
	FROM dbo.DimAccountGroup1 dag1 
	WHERE Converted = 1

	
	ORDER BY 1, Branch ASC
END

--Regional Coordinator
ELSE IF 
	@WebUserKey IN
		( SELECT WebUserKey
			FROM WebUserRole wur
			JOIN WebRole wr ON wr.WebRoleKey = wur.WebRoleKey
		    WHERE wr.WebRoleName = 'RegionalCoordinator'   
	)
BEGIN
	SELECT DISTINCT
		2 AS SortOrder,
		AccountGroup1Key AS PrimaryAccountGroup1Key, 
		--wud.DelegatorWebUserKey,
		AG1Level1 AS Branch, 
		AG1Level1UDVarchar1 AS BranchName, 
		Converted
	FROM dbo.DimAccountGroup1
	WHERE AccountGroup1Key IN (
			SELECT dag1.AccountGroup1Key 
			FROM lion.RegionalCoordinatorLocation rcl
			INNER JOIN dbo.DimAccountGroup1 dag1
				ON dag1.AccountGroup1Key = rcl.LocationKey
			WHERE rcl.WebUserKey = @WebUserKey
				AND rcl.ContractLocationLevelKey = 1
			UNION
			SELECT dag1.AccountGroup1Key 
			FROM lion.RegionalCoordinatorLocation rcl
			INNER JOIN dbo.DimArea da
				ON da.AreaKey = rcl.LocationKey
			INNER JOIN dbo.DimAccountGroup1 dag1
				ON dag1.AreaKey = da.AreaKey
			WHERE rcl.WebUserKey = @WebUserKey
				AND rcl.ContractLocationLevelKey = 3
			UNION
			SELECT dag1.AccountGroup1Key 
			FROM lion.RegionalCoordinatorLocation rcl
			INNER JOIN dbo.DimRegion dr
				ON dr.RegionKey = rcl.LocationKey
			INNER JOIN dbo.DimAccountGroup1 dag1
				ON dag1.RegionKey = dr.RegionKey
			WHERE rcl.WebUserKey = @WebUserKey
				AND rcl.ContractLocationLevelKey = 4
			)
		AND Converted = 1

	UNION

	SELECT DISTINCT
		2 AS SortOrder,
		AccountGroup1Key AS PrimaryAccountGroup1Key, 
		AG1Level1 AS Branch, 
		AG1Level1UDVarchar1 AS BranchName, 
		Converted
	FROM dbo.BridgeAccountManager bam
	JOIN dbo.DimAccountManager damParent
		ON damParent.AccountManagerKey = bam.ParentAccountManagerKey
	JOIN dbo.WebUser wuParent
		ON wuParent.AccountManagerKey = damParent.AccountManagerKey
	JOIN dbo.DimAccountManager damSub
		ON damSub.AccountManagerKey = bam.SubsidiaryAccountManagerKey
	JOIN DimAccountGroup1 dag1
		ON dag1.AG1Level1PersonKey = damSub.DimPersonKey	
	JOIN dbo.WebUserRole wur
		ON wur.WebUserKey = wuParent.WebUserKey				
	WHERE Converted = 1
		AND wuParent.WebUserKey = @WebUserKey
		AND wur.WebRoleKey = 8  --Network Manager

	UNION

	SELECT DISTINCT
		2 AS SortOrder,
		AccountGroup1Key AS PrimaryAccountGroup1Key, 
		AG1Level1 AS Branch, 
		AG1Level1UDVarchar1 AS BranchName,
		dag1.Converted
	FROM dbo.WebUserDelegate wud
	JOIN dbo.WebUser wu 
		ON wu.WebUserKey = wud.DelegatorWebUserKey
	JOIN dbo.DimPerson dp 
		ON dp.ODSPersonKey = wu.odspersonkey
	JOIN dbo.DimAccountGroup1 dag1 
		ON dag1.AG1Level1PersonKey = dp.DimPersonKey 
		AND dag1.Converted = 1
	JOIN dbo.WebUserRole wur
		ON wur.WebUserKey = wud.DelegateWebUserKey

	WHERE wud.DelegateWebUserKey = @WebUserKey 
			AND wur.WebRoleKey = 9  --Branch Manager
	
	ORDER BY 1, Branch ASC
END

--Network Manager
ELSE IF 
	@WebUserKey IN
		( SELECT WebUserKey
			FROM WebUserRole wur
			JOIN WebRole wr ON wr.WebRoleKey = wur.WebRoleKey
		    WHERE wr.WebRoleName = 'NetworkManager'   
	)
BEGIN
	SELECT DISTINCT
		2 AS SortOrder,
		AccountGroup1Key AS PrimaryAccountGroup1Key, 
		AG1Level1 AS Branch, 
		AG1Level1UDVarchar1 AS BranchName, 
		Converted
	FROM dbo.BridgeAccountManager bam
	JOIN dbo.DimAccountManager damParent
		ON damParent.AccountManagerKey = bam.ParentAccountManagerKey
	JOIN dbo.WebUser wuParent
		ON wuParent.AccountManagerKey = damParent.AccountManagerKey
	JOIN dbo.DimAccountManager damSub
		ON damSub.AccountManagerKey = bam.SubsidiaryAccountManagerKey
	JOIN DimAccountGroup1 dag1
		ON dag1.AG1Level1PersonKey = damSub.DimPersonKey						-- https://enterbridge.atlassian.net/browse/LPF-4952
	WHERE Converted = 1
		AND wuParent.WebUserKey = @WebUserKey
END

--Branch Managers
ELSE
	SELECT DISTINCT
	2 AS SortOrder,
	--PrimaryAccountGroup1Key, 
	AccountGroup1Key AS PrimaryAccountGroup1Key, 
	--wud.DelegatorWebUserKey,
	AG1Level1 AS Branch, 
	AG1Level1UDVarchar1 AS BranchName,
	dag1.Converted
	FROM dbo.WebUserDelegate wud
	JOIN dbo.WebUser wu 
		ON wu.WebUserKey = wud.DelegatorWebUserKey
	JOIN dbo.DimPerson dp 
		ON dp.ODSPersonKey = wu.odspersonkey
	JOIN dbo.DimAccountGroup1 dag1 
		ON dag1.AG1Level1PersonKey = dp.DimPersonKey 
		AND dag1.Converted = 1

	--JOIN dbo.DimAccountManager dam 
	--	ON dam.DimPersonKey = dp.DimPersonKey
	--JOIN dbo.DimAccountGroup1 dag1 
	--	ON dag1.AccountGroup1Key = dam.PrimaryAccountGroup1Key AND dag1.Converted = 1

	WHERE wud.DelegateWebUserKey = @WebUserKey 

--UNION

--SELECT 1 AS SortOrder, -1, 'WUK', 'CENTER', 1


ORDER BY 1, Branch ASC







GO
