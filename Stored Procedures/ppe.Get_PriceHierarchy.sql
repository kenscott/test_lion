SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [ppe].[Get_PriceHierarchy] (@WebUserKey INT, @RequestKey INT) AS 
BEGIN

	DECLARE @AccountKey INT, @ItemKey INT, @ItemGroup3Key INT, @GreenPrice MONEY, @GreenDisc DEC(38,8)

	SELECT 
		@AccountKey=AccountKey,
		@ItemKey=ItemKey,
		@ItemGroup3Key = ItemGroup3Key
	FROM ppe.RequestMain
	WHERE RequestKey = @RequestKey

	SELECT TOP 1 
		@GreenDisc=gd.Discount,
		@GreenPrice=GreenPrice 
	FROM [lion].[vwFactInvoiceLinePricebandScore] filpbs
	CROSS APPLY ppe.fn_GetDiscount(filpbs.AccountKey, filpbs.ItemGroup3Key, filpbs.ItemKey, filpbs.GreenPrice, filpbs.UnitListPrice, 'G') gd
	WHERE filpbs.ItemGroup3Key=@ItemGroup3Key
	AND filpbs.AccountKey=@AccountKey
	AND filpbs.ItemKey = @ItemKey
	ORDER BY filpbs.InvoiceDateDayKey DESC

	SELECT 1 AS PriceLevel, 'Term' AS PriceType, 'Product' AS ProductLevel, 
		CASE WHEN ExceptionFixedPrice IS NOT NULL THEN 'Fixed'
			WHEN ExceptionDiscount IS NOT NULL THEN 'Disc'
			ELSE ''
		END AS PriceMethod,
		ExceptionDiscount/100.0 AS Disc,
		ExceptionFixedPrice AS Price
	FROM lion.CustomerTerms 
	WHERE AccountKey=@AccountKey
	AND ItemKey = @ItemKey
	AND ItemKey <> 1

	UNION

	SELECT 
		2 AS PriceLevel, 
		'Term' AS PriceType, 
		'SPG' AS ProductLevel, 
		'Disc' AS PriceMethod, 
		1.0 - (1.0 - ISNULL(Usualdiscount1,0)/100.0) * (1.0 - ISNULL(Usualdiscount2,0)/100.0) AS Disc,
		NULL AS Price
	FROM lion.CustomerTerms 
	WHERE AccountKey=@AccountKey
	AND ItemGroup3Key = @ItemGroup3Key

	UNION
	SELECT 
		3 AS PriceLevel, 
		'Contract' AS PriceType, 
		'Product' AS ProductLevel, 
		CASE WHEN ExceptionFixedPrice IS NOT NULL THEN 'Fixed'
			WHEN ExceptionDiscount IS NOT NULL THEN 'Disc'
			ELSE ''
		END AS PriceMethod, 
		ExceptionDiscount/100.0 AS Disc,
		ExceptionFixedPrice AS Price
	FROM lion.AccountContractTerms ct , dbo.DimAccount da
	WHERE ct.ItemGroup3Key = @ItemGroup3Key
	AND ct.ItemKey = @ItemKey
	AND da.AccountKey = @AccountKey
	AND ct.ItemKey <> 1
	AND da.AccountUDVarChar13 = ct.ContractName
	AND GETDATE() < ct.SPGExpiryDate

	UNION
	SELECT 
		4 AS PriceLevel, 
		'Contract' AS PriceType, 
		'SPG' AS ProductLevel, 
		'Disc' AS PriceMethod, 
		1.0 - (1.0 - ISNULL(Usualdiscount1,0)/100.0) * (1.0 - ISNULL(Usualdiscount2,0)/100.0) AS Disc,
		NULL AS Price
	FROM lion.AccountContractTerms ct, dbo.DimAccount da
	WHERE ct.ItemGroup3Key = @ItemGroup3Key
	AND ct.ItemKey = 1
	AND da.AccountKey = @AccountKey
	AND da.AccountUDVarChar13 = ct.ContractName
	AND GETDATE() < SPGExpiryDate

	UNION
	SELECT TOP 1
		5 AS PriceLevel, 
		'Green' AS PriceType, 
		'Product' AS ProductLevel, 
		'Fixed' AS PriceMethod, 
		@GreenDisc AS Disc,
		@GreenPrice AS Price
	FROM dbo.DimItem
	WHERE ItemKey = @ItemKey
	AND ItemKey <> 1

	UNION

	SELECT 
		6 AS PriceLevel, 
		'Green' AS PriceType, 
		'SPG' AS ProductLevel, 
		'Disc' AS PriceMethod, 
		(SELECT Discount FROM ppe.fn_GetDiscount(@AccountKey,@ItemGroup3Key,1,NULL,NULL,'G')) AS Disc,
		NULL AS Price

	UNION

	SELECT 
		7 AS PriceLevel, 
		'Trade' AS PriceType, 
		'Product' AS ProductLevel, 
		'Fixed' AS PriceMethod, 
		NULL AS Disc,
		CurrBranchTradePrice AS Price
	FROM lion.vwItem
	WHERE ItemKey = @ItemKey
	AND ItemKey <> 1

END
GO
