SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- Stored Procedure

CREATE PROCEDURE [dbo].[Load_ATKManualPriceBand]
AS
/*
EXEC dbo.Load_ATKManualPriceBand
EXEC DCPLOG

SELECT count(*) FROM dbo.ManualPriceBandOverride	-- ~638300
SELECT TOP 1000 * FROM dbo.ManualPriceBandOverride

SELECT * FROM ATKManualPriceBand
SELECT * FROM ManualPriceBandOverride

*/

SET NOCOUNT ON

--EXEC LogDCPEvent 'DCP - Load_ATKManualPriceBand', 'B'



--DECLARE 
--	@RowsI Quantity_Normal_type, 
--	@RowsU Quantity_Normal_type, 
--	@RowsD Quantity_Normal_type,
--	@TodayDayKey Key_Small_Type

--SET @RowsI = 0
--SET @RowsU = 0
--SET @RowsD = 0



--TRUNCATE TABLE dbo.ManualPriceBandOverride


--; WITH ShipemntType AS (
--	SELECT CAST(3 AS INT) AS InvoiceLineGroup1Key
--	UNION
--	SELECT CAST(4 AS INT) AS InvoiceLineGroup1Key
--), ExpandedData AS (
--	SELECT DISTINCT
--		ItemVendorKey AS VendorKey,
--		di.ItemGroup1Key AS ItemGroup1Key,
--		ShipemntType.InvoiceLineGroup1Key,
--		FloorGPP,
--		TargetGPP,
--		StretchGPP,
--		SequenceNumber,
--		ROW_NUMBER() OVER (PARTITION BY 
--			ItemVendorKey,
--			di.ItemGroup1Key,
--			ShipemntType.InvoiceLineGroup1Key
--			ORDER BY SequenceNumber) AS RowRank
--	FROM ATKManualPriceBand mpb
--	INNER JOIN dbo.DimItem di
--		ON di.ItemVendorKey = mpb.VendorKey
--		AND (di.ItemVendorKey = mpb.VendorKey OR mpb.VendorKey IS NULL)
--	INNER JOIN ShipemntType
--		ON (ShipemntType.InvoiceLineGroup1Key = mpb.InvoiceLineGroup1Key OR mpb.InvoiceLineGroup1Key IS NULL)
--)
--INSERT INTO dbo.ManualPriceBandOverride (
--	VendorKey,
--	ItemGroup1Key,
--	InvoiceLineGroup1Key,
--	FloorGPP,
--	TargetGPP,
--	StretchGPP
--)
--SELECT DISTINCT
--	VendorKey,
--	ItemGroup1Key,
--	InvoiceLineGroup1Key,
--	FloorGPP,
--	TargetGPP,
--	StretchGPP		
--FROM ExpandedData
--WHERE RowRank = 1
--SET @RowsI = @RowsI + @@RowCount



--EXEC LogDCPEvent 'DCP - Load_ATKManualPriceBand', 'E', @RowsI, @RowsU, @RowsD



SELECT 1 AS Result 





SET QUOTED_IDENTIFIER OFF



GO
