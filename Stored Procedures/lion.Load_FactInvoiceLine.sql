SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE PROCEDURE [lion].[Load_FactInvoiceLine]
	@FullTransactionReprocessFlag [varchar](1) = 'N'
WITH EXECUTE AS CALLER
AS
/*

TODO JJJ Need to handle more changes, like a new account manager for the account


EXEC dbo.MVIEWS_Drop
EXEC dbo.MVIEWS2_Drop

EXEC lion.Load_FactInvoiceLine 'Y', 'Y'

EXEC dbo.MVIEWS_Create
EXEC dbo.MVIEWS2_Create 

*/


SET ANSI_WARNINGS OFF
SET ARITHABORT OFF
SET ARITHIGNORE ON

SET NOCOUNT ON

EXEC LogDCPEvent 'ETL - Load_FactInvoiceLine', 'B'


DECLARE 
	@RowsI Quantity_Normal_type, 
	@RowsU Quantity_Normal_type, 
	@RowsD Quantity_Normal_type,
	@TodayDayKey Key_Small_Type,
	@Cmd VARCHAR(8000),
	@MinDayKey INT,
	@MaxDayKey INT,
	@NullAccountManagerKey Key_normal_Type,
	@UnknownDayKey Key_Normal_type,
	@UnknownMonthKey Key_Normal_type


SET @RowsI = 0
SET @RowsU = 0
SET @RowsD = 0

SET @TodayDayKey = (SELECT DayKey FROM DimDay WHERE CONVERT(VARCHAR(10), SQLDate, 120) = CONVERT(VARCHAR(10), GETDATE(), 120))

SET @MaxDayKey = (
	SELECT dd.DayKey
	FROM   
	(
		SELECT 
			MAX(CAST(DeliveryDate AS DATETIME)) AS InvoiceDate
		FROM Lion_Staging.dbo.Transactions
		WHERE ETLRowStatusCodeKey = 0
		--WHERE ISDATE(DeliveryDate) = 1
	) il
	INNER JOIN DimDay dd 
		ON CONVERT(VARCHAR(10), dd.SQLDate, 120) = CONVERT(VARCHAR(10), il.InvoiceDate, 120)
	)

SET @MinDayKey = @MaxDayKey - 364

----SELECT 'MAX: ', @MaxDayKey
----SELECT 'MIN: ', @MinDayKey


SET @NullAccountManagerKey = (SELECT AccountManagerKey FROM DimAccountManager WHERE DimPersonKey = (SELECT DimPErsonKey FROM DImPerson WHERE FullName = 'Unknown'))

SELECT
	@UnknownDayKey = DayKey,
	@UnknownMonthKey = MonthKey
FROM DimDay 
WHERE CalendarDay = 'Unknown'



IF @FullTransactionReprocessFlag = 'Y' --or (SELECT count(*) FROM FactInvoiceLine) = 0
BEGIN


	EXEC DBA_IndexRebuild 'FactInvoiceLine', 'Drop'
	TRUNCATE TABLE FactInvoiceLine


END
ELSE
BEGIN


	CREATE TABLE #UpdateKeys (KeyID INTEGER)


	--EXEC LogDCPEvent 'ETL - Load_FactInvoiceLine - Update: key list 1', 'B'
	--INSERT INTO #UpdateKeys (KeyID)
	--SELECT DISTINCT
	--	UniqueId
	--FROM Lion_Staging.dbo.TransactionsImportTemp
	--EXEC LogDCPEvent 'ETL - Load_FactInvoiceLine - Update: key list 1', 'E', 0, @@ROWCOUNT, 0

	EXEC LogDCPEvent 'ETL - Load_FactInvoiceLine - Update: key list 2', 'B'
	INSERT INTO  #UpdateKeys (KeyID)
	SELECT DISTINCT	
		ClientInvoiceLineUniqueID
	FROM dbo.FactInvoiceLine fil
	INNER JOIN dbo.DimDay dd
		ON dd.DayKey = fil.ModificationDayKey
	INNER JOIN Lion_Staging.dbo.Transactions t
		ON fil.ClientInvoiceLineUniqueID = t.UniqueId
	INNER JOIN Lion_Staging.dbo.RawFile rf
		ON rf.RawFileKey = t.RawFileKey
	WHERE
		(ISNULL(t.EffectiveQty, 0.) <> ISNULL(TotalNetQuantity , 0.)
		OR ISNULL(t.EffectiveLinePrice, 0.) <> ISNULL(TotalNetPrice, 0.)
		OR ISNULL(t.EffectiveLinePrice - t.EffectiveApTradingMarginPadRelease, 0.) <> ISNULL(TotalNetCost, 0.)
		OR ISNULL(t.CreditAdvanousInvoiceLineId, 0) <> ISNULL(LinkedInvoiceLineUniqueIdentifier, 0)
		OR t.AdvanousInvoiceLineId <> InvoiceLineUniqueIdentifier
		OR dd.SQLDate <> CAST( CAST(rf.ImportDateTime AS DATE) AS DATETIME)
		)
		--AND NOT EXISTS (SELECT 1 FROM #UpdateKeys where KeyID = ClientInvoiceLineUniqueID)

	EXEC LogDCPEvent 'ETL - Load_FactInvoiceLine - Update: key list 2', 'E', 0, @@ROWCOUNT, 0

	CREATE UNIQUE CLUSTERED INDEX I_1 ON #UpdateKeys(KeyID)
	
	--EXEC DBA_IndexRebuild 'FactInvoiceLine', 'Drop_NoClustered'

	DECLARE @DeletedRowData TABLE (
		InvoiceLineJunkKey INT NOT NULL,
		--PRIMARY KEY  --[PK_LdFILJunKeys] --PRIMARY KEY CLUSTERED 
		--	(
		--		InvoiceLineJunkKey ASC
		--	)
		UNIQUE CLUSTERED (InvoiceLineJunkKey)
	)

	EXEC LogDCPEvent 'ETL - Load_FactInvoiceLine - Delete', 'B'
	DELETE fil
	OUTPUT DELETED.InvoiceLineJunkKey
	INTO @DeletedRowData
	FROM dbo.FactInvoiceLine fil
	LEFT JOIN Lion_Staging.dbo.Transactions t
		ON fil.ClientInvoiceLineUniqueID = t.UniqueId
	WHERE 
		t.ETLRowStatusCodeKey IS NULL 
		OR t.ETLRowStatusCodeKey <> 0
	SET @RowsD = @RowsD + @@RowCount
	EXEC LogDCPEvent 'ETL - Load_FactInvoiceLine - Delete', 'E', @RowsI, @RowsU, @RowsD
	
	--LP2P-868, Modify ETL to Remove old Transactional Data, Ken Scott 05.23.2017
	EXEC LogDCPEvent 'ETL - Load_FactInvoiceLine - Delete junk dimension', 'B'
	DELETE dilj
	FROM dbo.DimInvoiceLineJunk dilj
	INNER JOIN @DeletedRowData d
		ON d.InvoiceLineJunkKey = dilj.InvoiceLineJunkKey
	SET @RowsD = @RowsD + @@RowCount
	EXEC LogDCPEvent 'ETL - Load_FactInvoiceLine - Delete junk dimension', 'E', @RowsI, @RowsU, @RowsD

    EXEC LogDCPEvent 'ETL - Load_FactInvoiceLine - Update', 'B'
	--sp_helpindex2 factinvoiceline
	--CREATE UNIQUE NONCLUSTERED INDEX [I_FactInvoiceLine_66] ON [dbo].[FactInvoiceLine]
	--(
	--	[ClientInvoiceLineUniqueID] ASC
	--)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
	UPDATE fil
	SET 
		fil.AccountKey =                    ISNULL(da.AccountKey, 1),
		AlternateAccountKey = 				ISNULL(daChild.AccountKey, 1),
		ItemKey = 							ISNULL(di.ItemKey, 1),
		VendorKey = 						ISNULL(di.ItemVendorKey, 1),
		AccountManagerKey = 				ISNULL(da.AccountManagerKey, 1),
		InvoiceDateDayKey = 				ISNULL(dd.DayKey, @UnknownDayKey),
		InvoiceDateMonthKey = 				ISNULL(dd.MonthKey, @UnknownMonthKey),
		InvoiceLineGroup1Key = 				ISNULL(dilg1.InvoiceLineGroup1Key, 1),
		InvoiceLineGroup2Key = 				ISNULL(dilg2.InvoiceLineGroup2Key, 1),
		InvoiceLineGroup3Key = 				ISNULL(dilg3.InvoiceLineGroup3Key, 1),
		InvoiceLineJunkKey = 				ISNULL(dilj.InvoiceLineJunkKey, 1),
		AccountGroup1Key = 					COALESCE(dag1.AccountGroup1Key, da.AccountGroup1Key, 1),
		AccountGroup2Key = 					ISNULL(da.AccountGroup2Key, 1),
		AccountGroup3Key = 					ISNULL(da.AccountGroup3Key, 1),
		--AccountGroup4Key = 					ISNULL(da.AccountGroup4Key, 1),
		ItemGroup1Key = 					ISNULL(di.ItemGroup1Key, 1),
		ItemGroup2Key = 					ISNULL(di.ItemGroup2Key, 1),
		ItemGroup3Key = 					ISNULL(di.ItemGroup3Key, 1),
		InvoiceLineUniqueIdentifier = 		t.AdvanousInvoiceLineId,
		LinkedInvoiceLineUniqueIdentifier = t.CreditAdvanousInvoiceLineId,
		ClientInvoiceID = 					ISNULL(t.BranchCode, N'') + N'-' + ISNULL(t.TransNo, N'') + N'-' + ISNULL(CAST(t.DelNo AS NVARCHAR(25)), N''),
		ClientInvoiceLineID = 				ISNULL(t.BranchCode, N'') + N'-' + ISNULL(t.TransNo, N'') + N'-' + ISNULL(CAST(t.DelNo AS NVARCHAR(25)), N'') + N'-' + ISNULL(CAST(t.LineNumber AS NVARCHAR(25)), N''),
		ClientInvoiceLineUniqueID = 		t.UniqueId,
		TotalQuantity = 					ISNULL(t.Qty, 0.),
		TotalActualPrice = 
			CASE
				WHEN t.UnitPrice IS NOT NULL AND t.Qty IS NOT NULL AND t.Qty <> 0. AND t.UnitPrice > 0. THEN 
					t.UnitPrice * t.Qty * ISNULL(SellingConvFactor, 1) / ISNULL(PricingConvFact, 1)
				ELSE 
					ISNULL(t.LinePrice, 0.)
			END,
		TotalActualCost = 
			CASE
				WHEN t.UnitPrice IS NOT NULL AND t.Qty IS NOT NULL AND t.Qty <> 0. AND t.UnitPrice > 0. THEN 
					t.UnitPrice * t.Qty * ISNULL(SellingConvFactor, 1) / ISNULL(PricingConvFact, 1)
				ELSE 
					ISNULL(t.LinePrice, 0.)
			END - (ISNULL(t.ApTradingMarginPadRelease, 0.)),
		TotalActualRebate = 0.0,
		UnitListPrice = ISNULL(t.TradingPrice, 0.) * (ISNULL(SellingConvFactor, 1) / ISNULL(PricingConvFact, 1)),
		TotalNetQuantity = EffectiveQty,
		TotalNetPrice = t.EffectiveLinePrice,
		TotalNetCost = t.EffectiveLinePrice - t.EffectiveApTradingMarginPadRelease,
		RedPrice = 
			CASE
				WHEN t.RedPrice <> '' AND ISNUMERIC(t.RedPrice) = 1  AND CAST(t.RedPrice AS DECIMAL(19,8)) > 0. THEN CAST(t.RedPrice AS DECIMAL(19,8)) * (ISNULL(SellingConvFactor, 1) / ISNULL(PricingConvFact, 1))
				ELSE fil.RedPrice
			END,
		AmberPrice = 
			CASE
				WHEN t.AmberPrice <> '' AND ISNUMERIC(t.AmberPrice) = 1  AND CAST(t.AmberPrice AS DECIMAL(19,8)) > 0. THEN CAST(t.AmberPrice AS DECIMAL(19,8)) * (ISNULL(SellingConvFactor, 1) / ISNULL(PricingConvFact, 1))
				ELSE fil.AmberPrice
			END,
		GreenPrice = 
			CASE
				WHEN t.GreenPrice <> '' AND ISNUMERIC(t.GreenPrice) = 1  AND CAST(t.GreenPrice AS DECIMAL(19,8)) > 0. THEN CAST(t.GreenPrice AS DECIMAL(19,8)) * (ISNULL(SellingConvFactor, 1) / ISNULL(PricingConvFact, 1))
				ELSE fil.GreenPrice
			END,
		BluePrice = 
			CASE
				WHEN t.BluePrice <> '' AND ISNUMERIC(t.BluePrice) = 1  AND CAST(t.BluePrice AS DECIMAL(19,8)) > 0. THEN CAST(t.BluePrice AS DECIMAL(19,8)) * (ISNULL(SellingConvFactor, 1) / ISNULL(PricingConvFact, 1))
				ELSE fil.BluePrice
			END,
		TerminalEmulator =        t.TerminalEmulator,
		PriceGuidanceIndicator =  NULLIF(LTRIM(RTRIM(t.PriceGuidanceIndicator)), ''),
		UDVarchar1 = 			  ISNULL(t.PriceDerivation, ''),
		UDVarchar2 = 			  ISNULL(t.VariablePackInd, ''),
		UDVarchar3 = 			  ISNULL(t.ClaimNo, ''),
		UDDecimal1 = 			  ISNULL(t.ApTradingMarginPadRelease, 0),
		UDDecimal2 = 			  ISNULL(t.CCProfit, 0),
		UDDecimal3 = 			  ISNULL(t.RetailPrice * (ISNULL(SellingConvFactor, 1) / ISNULL(PricingConvFact, 1)), 0),
		TotalSupplierCost = t.SuppliersNettCost,
		TotalNetSupplierCost = t.EffectiveSupplierCost,
		ModificationDayKey = ddMod.DayKey
	FROM dbo.FactInvoiceLine fil
	INNER JOIN #UpdateKeys u
		ON u.KeyID = fil.ClientInvoiceLineUniqueID
	INNER JOIN Lion_Staging.dbo.Transactions t
		ON t.UniqueId = fil.ClientInvoiceLineUniqueID
	LEFT JOIN dbo.DimAccountGroup1 dag1
		ON dag1.AG1Level1 = t.BranchCode
	LEFT JOIN dbo.DimAccount daChild
		ON daChild.AccountClientUniqueIdentifier = t.AccountID
	LEFT JOIN dbo.DimAccount da
		ON da.AccountNumber = ISNULL(daChild.AccountNumberParent, t.AccountID)
	LEFT JOIN dbo.DimItem di
		ON di.ItemClientUniqueIdentifier = ISNULL(t.PyramidCode, N'') + t.ProductCode
	LEFT JOIN dbo.ItemUOMConversion uc
		ON uc.ItemKey = di.ItemKey
	LEFT JOIN dbo.DimDay dd
		ON CONVERT(VARCHAR(10),dd.SQLDate,120) = CONVERT(VARCHAR(10), CAST(TransactionDate AS DATE), 120)

	INNER JOIN Lion_Staging.dbo.RawFile rf
		ON rf.RawFileKey = t.RawFileKey
	INNER JOIN dbo.DimDay ddMod
		ON CAST(ddMod.SQLDate AS DATE) = CAST(rf.ImportDateTime AS DATE)


	LEFT JOIN dbo.DimInvoiceLineGroup1 dilg1
		ON dilg1.InvoiceLineGroup1UDVarchar1 = 
			CASE 
				WHEN t.SupplyType = N'Direct' THEN 'D'
				WHEN t.SupplyType = N'Credit' THEN 'C'
				ELSE 'S'
			END
		AND dilg1.InvoiceLineGroup1UDVarchar2 =			-- Contract Claim Indicator (formally known as Deviated Indicator)
			CASE
				WHEN LTRIM(RTRIM(ISNULL(t.ClaimNo, N''))) = N'' THEN 'N'
				ELSE 'Y'
			END
	LEFT JOIN dbo.DimInvoiceLineGroup2 dilg2	-- PriceDerivation Bucket
		ON dilg2.InvoiceLineGroup2UDVarchar1 = LTRIM(RTRIM(ISNULL(t.PriceDerivation, N'')))
	LEFT JOIN dbo.DimInvoiceLineGroup3 dilg3	-- under (some) customer contract
		ON dilg3.InvoiceLineGroup3UDVarchar1 = 
			CASE
				WHEN LTRIM(RTRIM(ISNULL(t.PriceDerivation, N''))) IN (N'DC', N'FM', N'GT', N'IP', N'OCD', N'OCL', N'OTG') THEN 'Y'
				ELSE 'N'
			END
	LEFT JOIN DimInvoiceLineJunk dilj
		ON dilj.InvoiceLineUniqueIdentifier = t.AdvanousInvoiceLineId
	SET @RowsU = @RowsU + @@RowCount

	EXEC LogDCPEvent 'ETL - Load_FactInvoiceLine - Update', 'E', @RowsI, @RowsU, @RowsD

	DROP TABLE #UpdateKeys
END




EXEC LogDCPEvent 'ETL - Load_FactInvoiceLine - Insert', 'B'

; WITH RawData AS (

SELECT
	ISNULL(da.AccountKey, 1) AS AccountKey,
	ISNULL(daChild.AccountKey, 1) AS AlternateAccountKey,
	ISNULL(di.ItemKey, 1) AS ItemKey,
	ISNULL(di.ItemVendorKey, 1) AS VendorKey,
	ISNULL(da.AccountManagerKey, 1) AS AccountManagerKey,
	ISNULL(dd.DayKey, @UnknownDayKey) AS InvoiceDateDayKey,
	ISNULL(dd.MonthKey, @UnknownMonthKey) AS InvoiceDateMonthKey,
	ISNULL(dilg1.InvoiceLineGroup1Key, 1) AS InvoiceLineGroup1Key,
	ISNULL(dilg2.InvoiceLineGroup2Key, 1) AS InvoiceLineGroup2Key,
	ISNULL(dilg3.InvoiceLineGroup3Key, 1) AS InvoiceLineGroup3Key,
	ISNULL(dilj.InvoiceLineJunkKey, 1) AS InvoiceLineJunkKey,
	
	COALESCE(dag1.AccountGroup1Key, da.AccountGroup1Key, 1) AS AccountGroup1Key,  -- IMPORTANT: this links the sale to branch which may be different than the branch of the customer
	
	ISNULL(da.AccountGroup2Key, 1) AS AccountGroup2Key,
	ISNULL(da.AccountGroup3Key, 1) AS AccountGroup3Key,
	--ISNULL(da.AccountGroup4Key, 1) AS AccountGroup4Key,

	ISNULL(di.ItemGroup1Key, 1) AS ItemGroup1Key,
	ISNULL(di.ItemGroup2Key, 1) AS ItemGroup2Key,
	ISNULL(di.ItemGroup3Key, 1) AS ItemGroup3Key,
	
	t.AdvanousInvoiceLineId AS InvoiceLineUniqueIdentifier,		-- unique key either from client or us (as in AdvanousInvoiceLineId)
	t.CreditAdvanousInvoiceLineId AS LinkedInvoiceLineUniqueIdentifier,	-- link to anther line, using raw/staging id, for things like credits
	ISNULL(t.BranchCode, N'') + N'-' + ISNULL(t.TransNo, N'') + N'-' + ISNULL(CAST(t.DelNo AS NVARCHAR(25)), N'') AS ClientInvoiceID,
	ISNULL(t.BranchCode, N'') + N'-' + ISNULL(t.TransNo, N'') + N'-' + ISNULL(CAST(t.DelNo AS NVARCHAR(25)), N'') + N'-' + ISNULL(CAST(t.LineNumber AS NVARCHAR(25)), N'') AS ClientInvoiceLineID,
	t.UniqueId AS ClientInvoiceLineUniqueID,

	ISNULL(t.Qty, 0.) AS TotalQuantity,

	CASE
		WHEN t.UnitPrice IS NOT NULL AND t.Qty IS NOT NULL AND t.Qty <> 0. AND t.UnitPrice > 0. THEN 
			t.UnitPrice * t.Qty * ISNULL(SellingConvFactor, 1) / ISNULL(PricingConvFact, 1)
		ELSE 
			ISNULL(t.LinePrice, 0.)
	END AS TotalActualPrice,
	CASE
		WHEN t.UnitPrice IS NOT NULL AND t.Qty IS NOT NULL AND t.Qty <> 0. AND t.UnitPrice > 0. THEN 
			t.UnitPrice * t.Qty * ISNULL(SellingConvFactor, 1) / ISNULL(PricingConvFact, 1)
		ELSE 
			ISNULL(t.LinePrice, 0.)
	END - (ISNULL(t.ApTradingMarginPadRelease, 0.)) AS TotalActualCost,		-- total price - total margin
	0.0 AS TotalActualRebate,

	ISNULL(t.TradingPrice, 0.) * (ISNULL(SellingConvFactor, 1) / ISNULL(PricingConvFact, 1)) AS UnitListPrice,

	EffectiveQty AS TotalNetQuantity,
	t.EffectiveLinePrice AS TotalNetPrice,
	t.EffectiveLinePrice - t.EffectiveApTradingMarginPadRelease AS TotalNetCost,
	
	CASE
		WHEN t.RedPrice <> '' AND ISNUMERIC(t.RedPrice) = 1  AND CAST(t.RedPrice AS DECIMAL(19,8)) > 0. THEN CAST(t.RedPrice AS DECIMAL(19,8))
		ELSE NULL
	END * (ISNULL(SellingConvFactor, 1) / ISNULL(PricingConvFact, 1)) AS RedPrice,
	CASE
		WHEN t.AmberPrice <> '' AND ISNUMERIC(t.AmberPrice) = 1  AND CAST(t.AmberPrice AS DECIMAL(19,8)) > 0. THEN CAST(t.AmberPrice AS DECIMAL(19,8))
		ELSE NULL
	END * (ISNULL(SellingConvFactor, 1) / ISNULL(PricingConvFact, 1)) AS AmberPrice,
	CASE
		WHEN t.GreenPrice <> '' AND ISNUMERIC(t.GreenPrice) = 1  AND CAST(t.GreenPrice AS DECIMAL(19,8)) > 0. THEN CAST(t.GreenPrice AS DECIMAL(19,8))
		ELSE NULL
	END * (ISNULL(SellingConvFactor, 1) / ISNULL(PricingConvFact, 1)) AS GreenPrice,
	CASE
		WHEN t.BluePrice <> '' AND ISNUMERIC(t.BluePrice) = 1  AND CAST(t.BluePrice AS DECIMAL(19,8)) > 0. THEN CAST(t.BluePrice AS DECIMAL(19,8))
		ELSE NULL
	END * (ISNULL(SellingConvFactor, 1) / ISNULL(PricingConvFact, 1)) AS BluePrice,
	TerminalEmulator,
	NULLIF(LTRIM(RTRIM(PriceGuidanceIndicator)), '') AS PriceGuidanceIndicator,
	
	ISNULL(t.PriceDerivation, '') AS UDVarchar1, -- add to schema.. udvarchar?
	ISNULL(t.VariablePackInd, '') AS UDVarchar2,
	ISNULL(t.ClaimNo, '') AS UDVarchar3,
	
	ISNULL(t.ApTradingMarginPadRelease, 0) AS UDDecimal1,
	ISNULL(t.CCProfit, 0) AS UDDecimal2,
	ISNULL(t.RetailPrice * (ISNULL(SellingConvFactor, 1) / ISNULL(PricingConvFact, 1)), 0) AS UDDecimal3,
	t.SuppliersNettCost,
	t.EffectiveSupplierCost,

	CASE 
		WHEN dd.DayKey BETWEEN @MinDayKey AND @MaxDayKey THEN 'Y'
		ELSE 'N'
	END AS Last12MonthsIndicator,
	
	@TodayDayKey AS CreationDayKey,
	ddMod.DayKey AS ModificationDayKey,
	
	ROW_NUMBER() OVER (PARTITION BY t.UniqueId ORDER BY YMTH DESC, TransactionDate DESC, AdvanousInvoiceLineID DESC) AS RowRank

FROM Lion_Staging.dbo.Transactions t
LEFT JOIN dbo.DimAccountGroup1 dag1
	ON dag1.AG1Level1 = t.BranchCode
LEFT JOIN dbo.DimAccount daChild
	ON daChild.AccountClientUniqueIdentifier = t.AccountID
LEFT JOIN dbo.DimAccount da
	ON da.AccountNumber = ISNULL(daChild.AccountNumberParent, t.AccountID)
LEFT JOIN dbo.DimItem di
	ON di.ItemClientUniqueIdentifier = ISNULL(t.PyramidCode, N'') + t.ProductCode
LEFT JOIN dbo.ItemUOMConversion uc
	ON uc.ItemKey = di.ItemKey
LEFT JOIN dbo.DimDay dd
	ON CONVERT(VARCHAR(10),dd.SQLDate,120) = CONVERT(VARCHAR(10), CAST(TransactionDate AS DATE), 120)

INNER JOIN Lion_Staging.dbo.RawFile rf
	ON rf.RawFileKey = t.RawFileKey
INNER JOIN dbo.DimDay ddMod
	ON CAST(ddMod.SQLDate AS DATE) = CAST(rf.ImportDateTime AS DATE)

LEFT JOIN dbo.DimInvoiceLineGroup1 dilg1
	ON dilg1.InvoiceLineGroup1UDVarchar1 = 
		CASE 
			WHEN t.SupplyType = N'Direct' THEN 'D'
			WHEN t.SupplyType = N'Credit' THEN 'C'
			ELSE 'S'
		END
	AND dilg1.InvoiceLineGroup1UDVarchar2 =			-- Contract Claim Indicator (formally known as Deviated Indicator)
		CASE
			WHEN LTRIM(RTRIM(ISNULL(t.ClaimNo, N''))) = N'' THEN 'N'
			ELSE 'Y'
		END
LEFT JOIN dbo.DimInvoiceLineGroup2 dilg2	-- PriceDerivation Bucket
	ON dilg2.InvoiceLineGroup2UDVarchar1 = LTRIM(RTRIM(ISNULL(t.PriceDerivation, N'')))
LEFT JOIN dbo.DimInvoiceLineGroup3 dilg3	-- under (some) customer contract
	ON dilg3.InvoiceLineGroup3UDVarchar1 = 
		CASE
			WHEN LTRIM(RTRIM(ISNULL(t.PriceDerivation, N''))) IN (N'DC', N'FM', N'GT', N'IP', N'OCD', N'OCL', N'OTG') THEN 'Y'
			ELSE 'N'
		END
LEFT JOIN DimInvoiceLineJunk dilj
	ON dilj.InvoiceLineUniqueIdentifier = t.AdvanousInvoiceLineId
WHERE
	NOT EXISTS (SELECT 1 FROM dbo.FactInvoiceLine fil where fil.InvoiceLineUniqueIdentifier = t.AdvanousInvoiceLineId)
	AND t.ETLRowStatusCodeKey = 0
)

INSERT dbo.FactInvoiceLine (
	AccountKey,
	AlternateAccountKey,
	ItemKey,
	VendorKey,
	AccountManagerKey,
	InvoiceDateDayKey,
	InvoiceDateMonthKey,
	InvoiceLineGroup1Key,
	InvoiceLineGroup2Key,
	InvoiceLineGroup3Key,
	InvoiceLineJunkKey,
	AccountGroup1Key,
	AccountGroup2Key,
	AccountGroup3Key,
	--AccountGroup4Key,
	ItemGroup1Key,
	ItemGroup2Key,
	ItemGroup3Key,
	InvoiceLineUniqueIdentifier,		-- unique key either from client or us (as in AdvanousInvoiceLineId)
	LinkedInvoiceLineUniqueIdentifier,	-- link to anther line, using raw/staging id, for things like credits
	ClientInvoiceID,
	ClientInvoiceLineID,
	ClientInvoiceLineUniqueID,
	TotalQuantity,
	TotalActualPrice,
	TotalActualCost,
	TotalActualRebate,
	UnitListPrice,
	
	TotalNetQuantity,
	TotalNetPrice,
	TotalNetCost,

	RedPrice,
	AmberPrice,
	GreenPrice,
	BluePrice,
	TerminalEmulator,
	PriceGuidanceIndicator,
	
	UDVarchar1,
	UDVarchar2,
	UDVarchar3,

	UDDecimal1,
	UDDecimal2,
	UDDecimal3,

	Last12MonthsIndicator,	
	CreationDayKey,
	ModificationDayKey,
	TotalSupplierCost,
	TotalNetSupplierCost
)
SELECT
	AccountKey,
	AlternateAccountKey,
	ItemKey,
	VendorKey,
	AccountManagerKey,
	InvoiceDateDayKey,
	InvoiceDateMonthKey,
	InvoiceLineGroup1Key,
	InvoiceLineGroup2Key,
	InvoiceLineGroup3Key,
	InvoiceLineJunkKey,
	AccountGroup1Key,
	AccountGroup2Key,
	AccountGroup3Key,
	--AccountGroup4Key,
	ItemGroup1Key,
	ItemGroup2Key,
	ItemGroup3Key,
	InvoiceLineUniqueIdentifier,		-- unique key either from client or us (as in AdvanousInvoiceLineId)
	LinkedInvoiceLineUniqueIdentifier,	-- link to anther line, using raw/staging id, for things like credits
	ClientInvoiceID,
	ClientInvoiceLineID,
	ClientInvoiceLineUniqueID,
	TotalQuantity,
	TotalActualPrice,
	TotalActualCost,
	TotalActualRebate,
	UnitListPrice,
	
	TotalNetQuantity,
	TotalNetPrice,
	TotalNetCost,

	RedPrice,
	AmberPrice,
	GreenPrice,
	BluePrice,
	TerminalEmulator,
	PriceGuidanceIndicator,

	UDVarchar1,
	UDVarchar2,
	UDVarchar3,

	UDDecimal1,
	UDDecimal2,
	UDDecimal3,

	Last12MonthsIndicator,	
	CreationDayKey,
	ModificationDayKey,
	SuppliersNettCost,
	EffectiveSupplierCost
FROM RawData
WHERE 
	RowRank = 1
SET @RowsI = @RowsI + @@RowCount

EXEC LogDCPEvent 'ETL - Load_FactInvoiceLine - Insert', 'E', @RowsI, @RowsU, @RowsD


EXEC LogDCPEvent 'ETL - Load_FactInvoiceLine - Update RAG Prices', 'B'
UPDATE fil
SET
	fil.RedPrice = s.RedPrice,
	fil.AmberPrice = s.AmberPrice, 
	fil.GreenPrice = s.GreenPrice
FROM dbo.FactInvoiceLine fil
INNER JOIN dbo.FactInvoiceLinePricebandScore s
	ON s.InvoiceLineUniqueIdentifier = fil.InvoiceLineUniqueIdentifier
WHERE 
	(fil.RedPrice IS NULL
	OR fil.AmberPrice IS NULL
	OR fil.GreenPrice IS NULL)
	AND s.RedPrice IS NOT NULL
	AND s.AmberPrice IS NOT NULL
	AND s.GreenPrice IS NOT NULL
	AND fil.RedPrice <> s.RedPrice
	AND fil.AmberPrice <> s.AmberPrice
	AND fil.GreenPrice <> s.GreenPrice
SET @RowsU =  @RowsU + @@RowCount
EXEC LogDCPEvent 'ETL - Load_FactInvoiceLine - Update RAG Prices', 'E', @RowsI, @RowsU, @RowsD


EXEC LogDCPEvent 'ETL - Load_FactInvoiceLine - Update Last12MonthsIndicator', 'B'
UPDATE dbo.FactInvoiceLine
SET Last12MonthsIndicator = 
	CASE 
		WHEN InvoiceDateDayKey BETWEEN @MinDayKey AND @MaxDayKey THEN 'Y'
		ELSE 'N'
	END
WHERE
	Last12MonthsIndicator <>
	CASE 
		WHEN InvoiceDateDayKey BETWEEN @MinDayKey AND @MaxDayKey THEN 'Y'
		ELSE 'N'
	END
EXEC LogDCPEvent 'ETL - Load_FactInvoiceLine - Update Last12MonthsIndicator', 'E', @RowsI, @RowsU, @RowsD



EXEC LogDCPEvent 'ETL - Load_FactInvoiceLine - Index rebuild, FK rebuild, statistics update', 'B'

EXEC DBA_IndexRebuild 'FactInvoiceLine', 'Create'
EXEC DBA_RemainingFKRebuild

SET @Cmd = 'UPDATE STATISTICS FactInvoiceLine'
EXEC (@Cmd)
SET @Cmd = ''

EXEC LogDCPEvent 'ETL - Load_FactInvoiceLine - Index rebuild, FK rebuild, statistics update', 'E', @RowsI, @RowsU, @RowsD



SELECT TOP 10 * FROM dbo.FactInvoiceLine


EXEC LogDCPEvent 'ETL - Load_FactInvoiceLine', 'E', @RowsI, @RowsU, @RowsD




GO
