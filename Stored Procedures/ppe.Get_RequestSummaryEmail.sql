SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE [ppe].[Get_RequestSummaryEmail] AS
/*
	exec ppe.Get_RequestSummaryEmail
	
	select * from ppe.vw_requestsummaryemail
	
	select * from ppe.RequestStatus
*/

BEGIN

SELECT * 
FROM (
		SELECT ViewerWebUserKey, Email, Fullname, Statuskey, RequestCount, SummaryEmailIndicator
		FROM ppe.vw_requestsummaryemail v
	  ) P
PIVOT 
( 
	MAX(P.RequestCount) 
	FOR P.StatusKey IN ( [-1],[1],[2],[3],[4],[5],[6],[7] )
) AS pvt

WHERE SummaryEmailIndicator = 'Y' OR ( SummaryEmailIndicator = 'P' AND  [-1] > 0 )
ORDER BY email

END
GO
