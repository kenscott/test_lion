SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE PROCEDURE [ppe].[procArchiveLogs]
AS
    DECLARE @ArchiveMonths INT ,
        @ArchiveDate DATETIME ,
        @RowsI INT ,
        @RowsU INT ,
        @RowsD INT

    SET @RowsI = 0
    SET @RowsU = 0
    SET @RowsD = 0
    SELECT  @ArchiveMonths = CAST(ParamValue AS INT)
    FROM    dbo.Param
    WHERE   ParamName = 'ArchiveMonths'	
    SELECT  @ArchiveDate = DATEADD(MONTH, @ArchiveMonths, GETDATE())


    EXEC LogDCPEvent 'ppe.ArchiveLogs - ppe.RequestMain', 'B'
    BEGIN TRY
        BEGIN TRANSACTION
        INSERT  INTO ppe.RequestMainArchive
                ( RequestKey ,
                  AccountKey ,
                  ItemGroup3Key ,
                  ItemKey ,
                  InvoiceLineGroup1Key ,
                  TypeKey ,
                  StatusKey ,
                  CreatedByWebUserKey ,
                  ModifiedByWebUserKey ,
                  DefaultDiscount ,
                  CurrentSPGDiscount1 ,
                  CurrentSPGDiscount2 ,
                  ProposedSPGDiscount1 ,
                  ProposedSPGDiscount2 ,
                  NewSPGDiscount1 ,
                  NewSPGDiscount2 ,
                  NewSPGFromDate ,
                  ProposedExceptionDiscount ,
                  ProposedExceptionFixedPrice ,
                  ProposedExceptionStartDate ,
                  ProposedExceptionExpiryDate ,
                  OwningBrandCode ,
                  LastChangeInitials ,
                  LastChangeDateTime ,
                  LastChangeBranch ,
                  StrategicItemFlag ,
                  FixedPriceFlag ,
                  KVIFlag ,
                  KVIOverrideFlag ,
                  LastSaleDate ,
                  LastPriceDerivation ,
                  LastItemDiscount ,
                  LastItemPrice ,
                  LastItemGPP ,
                  AdjustedLastItemPrice ,
                  AdjustedLastItemGPP ,
                  RecommendedPrice ,
                  RecommendedPriceLogic ,
                  GreenPrice ,
                  AmberPrice ,
                  RedPrice ,
                  TradePrice ,
                  GreenGPP ,
                  AmberGPP ,
                  RedGPP ,
                  TradeGPP ,
                  LastPriceLevel ,
                  SalesSize ,
                  PriceApproach ,
                  PriceBandLevel ,
                  CurrentCost ,
                  Total12MonthSales ,
                  Total12MonthQty ,
                  RecommendedImpact ,
                  AcceptedImpact ,
                  Version ,
                  CurrentExceptionDiscount ,
                  CurrentExceptionFixedPrice ,
                  CurrentExceptionStartDate ,
                  CurrentExceptionExpiryDate ,
                  NewExceptionDiscount ,
                  NewExceptionFixedPrice ,
                  NewExceptionStartDate ,
                  NewExceptionExpiryDate ,
                  BatchKey ,
                  ModificationDate ,
                  CreationDate ,
                  NotificationEmailSentIndicator ,
                  LastActionKey ,
                  NewDeleteTerm ,
                  ProposedDeleteTerm ,
                  OwnerWebUserKey ,
                  PriorOwnerWebUserKey ,
                  NewTermType ,
                  RealityResponseCode ,
                  ReviewType ,
                  LastDiscount ,
                  SourceKey ,
                  SPGSkippedSales
                )
                SELECT  RequestKey ,
                        AccountKey ,
                        ItemGroup3Key ,
                        ItemKey ,
                        InvoiceLineGroup1Key ,
                        TypeKey ,
                        StatusKey ,
                        CreatedByWebUserKey ,
                        ModifiedByWebUserKey ,
                        DefaultDiscount ,
                        CurrentSPGDiscount1 ,
                        CurrentSPGDiscount2 ,
                        ProposedSPGDiscount1 ,
                        ProposedSPGDiscount2 ,
                        NewSPGDiscount1 ,
                        NewSPGDiscount2 ,
                        NewSPGFromDate ,
                        ProposedExceptionDiscount ,
                        ProposedExceptionFixedPrice ,
                        ProposedExceptionStartDate ,
                        ProposedExceptionExpiryDate ,
                        OwningBrandCode ,
                        LastChangeInitials ,
                        LastChangeDateTime ,
                        LastChangeBranch ,
                        StrategicItemFlag ,
                        FixedPriceFlag ,
                        KVIFlag ,
                        KVIOverrideFlag ,
                        LastSaleDate ,
                        LastPriceDerivation ,
                        LastItemDiscount ,
                        LastItemPrice ,
                        LastItemGPP ,
                        AdjustedLastItemPrice ,
                        AdjustedLastItemGPP ,
                        RecommendedPrice ,
                        RecommendedPriceLogic ,
                        GreenPrice ,
                        AmberPrice ,
                        RedPrice ,
                        TradePrice ,
                        GreenGPP ,
                        AmberGPP ,
                        RedGPP ,
                        TradeGPP ,
                        LastPriceLevel ,
                        SalesSize ,
                        PriceApproach ,
                        PriceBandLevel ,
                        CurrentCost ,
                        Total12MonthSales ,
                        Total12MonthQty ,
                        RecommendedImpact ,
                        AcceptedImpact ,
                        Version ,
                        CurrentExceptionDiscount ,
                        CurrentExceptionFixedPrice ,
                        CurrentExceptionStartDate ,
                        CurrentExceptionExpiryDate ,
                        NewExceptionDiscount ,
                        NewExceptionFixedPrice ,
                        NewExceptionStartDate ,
                        NewExceptionExpiryDate ,
                        BatchKey ,
                        ModificationDate ,
                        CreationDate ,
                        NotificationEmailSentIndicator ,
                        LastActionKey ,
                        NewDeleteTerm ,
                        ProposedDeleteTerm ,
                        OwnerWebUserKey ,
                        PriorOwnerWebUserKey ,
                        NewTermType ,
                        RealityResponseCode ,
                        ReviewType ,
                        LastDiscount ,
                        SourceKey ,
                        SPGSkippedSales
                FROM    ppe.RequestMain
                WHERE   ( CreationDate <= @ArchiveDate )

        SELECT  @RowsI = @@ROWCOUNT

        DELETE  FROM ppe.RequestMain
        WHERE   ( CreationDate <= @archiveDate )
        SELECT  @RowsD = @@ROWCOUNT
	
	
        COMMIT TRAN
    END TRY
    BEGIN CATCH
        IF @@TRANCOUNT > 0
            ROLLBACK TRAN
        EXEC dbo.LogDCPError
    END CATCH

    EXEC LogDCPEvent 'ppe.ArchiveLogs - ppe.RequestMain', 'E', @RowsI, @RowsU,
        @RowsD

    SET @RowsI = 0
    SET @RowsU = 0
    SET @RowsD = 0
    EXEC LogDCPEvent 'ppe.ArchiveLogs - ppe.RequestLog', 'B'


    BEGIN TRY
        BEGIN TRANSACTION;
        INSERT  INTO ppe.RequestLogArchive
                ( RequestLogKey ,
                  WebUserKey ,
                  RequestKey ,
                  OldStatusKey ,
                  NewStatusKey ,
                  RequestType ,
                  LogTime ,
                  RequestAction ,
                  EmailSent ,
                  OldOwnerWebUserKey ,
                  NewOwnerWebUserKey ,
                  Notes
                )
                SELECT  RequestLogKey ,
                        WebUserKey ,
                        RequestKey ,
                        OldStatusKey ,
                        NewStatusKey ,
                        RequestType ,
                        LogTime ,
                        RequestAction ,
                        EmailSent ,
                        OldOwnerWebUserKey ,
                        NewOwnerWebUserKey ,
                        Notes
                FROM    ppe.RequestLog
                WHERE   ( LogTime <= @ArchiveDate )
   
        SELECT  @RowsI = @@ROWCOUNT

        DELETE  FROM ppe.RequestLog
        WHERE   ( LogTime <= @ArchiveDate )

        SELECT  @RowsD = @@ROWCOUNT

        COMMIT TRAN
    END TRY
    BEGIN CATCH
        IF @@TRANCOUNT > 0
            ROLLBACK TRAN
        EXEC dbo.LogDCPError
    END CATCH

    EXEC LogDCPEvent 'ppe.ArchiveLogs - ppe.RequestLog', 'E', @RowsI, @RowsU,
        @RowsD

    EXEC LogDCPEvent 'ppe.ArchiveLogs', 'E', @RowsI, @RowsU, @RowsD



GO
