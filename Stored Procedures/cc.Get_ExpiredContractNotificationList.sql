SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


/*
	Author: James Dameron
	Date: 9/15/2016
	Jira Reference: LPF-2947
	Description:  Based on the supplied ContractOwner, this procedure will determine the individuals that
				  should be notified that a contract is expiring.

	Example usage:
		exec cc.Get_ExpiredContractNotificationList 'WUK'
		exec cc.Get_ExpiredContractNotificationList '1L'

	Reference queries:
		select * from webrole
*/

CREATE PROCEDURE [cc].[Get_ExpiredContractNotificationList] (@ContractOwner varchar(10))
AS 
BEGIN


IF @ContractOwner = 'WUK' 
BEGIN

	SELECT DISTINCT 
		wu.WebUserKey,
		wu.UserName,
		odsp.FullName,
		odsp.Email
		FROM WebUser wu
		JOIN WebUserRole wur
			ON wur.WebUserKey = wu.WebUserKey
		JOIN WebRole wr
			ON wr.WebRoleKey = wur.WebRoleKey AND wr.WebRoleName = 'ContractCoordinator'
		JOIN ODSPerson odsp
			ON odsp.ODSPersonKey = wu.ODSPersonKey

END
ELSE
BEGIN
	SELECT 
		wu.WebUserKey,
		wu.UserName,
		odsp.FullName,
		odsp.Email
		FROM WebUser wu
		JOIN ODSPerson odsp
			ON odsp.ODSPersonKey = wu.ODSPersonKey
		WHERE wu.UserName = 'Scott.Toolson'
END

END



GO
