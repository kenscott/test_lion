SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [lion].[Load_FactLastPriceAndCost]
WITH EXECUTE AS CALLER
AS
SET NOCOUNT ON
SET QUOTED_IDENTIFIER ON 
SET ANSI_NULLS ON 
SET ARITHABORT ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
SET CONCAT_NULL_YIELDS_NULL ON
SET NUMERIC_ROUNDABORT OFF


EXEC LogDCPEvent 'ETL - lion.FactLastPriceAndCost', 'B'



EXEC LogDCPEvent 'ETL - lion.FactLastPriceAndCost -    #LastAISAccountSales', 'B'

DECLARE 
	@RowsI Quantity_Normal_type, 
	@RowsU Quantity_Normal_type, 
	@RowsD Quantity_Normal_type,
	@TodayDayKey Key_Small_Type,
	@UnknownJunkKey Key_Normal_type,
	@SQLString NVARCHAR(4000)

	
SET @RowsI = 0
SET @RowsU = 0
SET @RowsD = 0

SET @UnknownJunkKey = (SELECT InvoiceLineJunkKey FROM dbo.DimInvoiceLineJunk WHERE InvoiceLineJunkUDVarchar1 = 'Unknown')

EXEC DBA_IndexRebuild 'FactLastPriceAndCost', 'Drop'


--TRUNCATE TABLE dbo.FactLastPriceAndCost
DELETE FROM FactLastPriceAndCost
SET @RowsD = @RowsD + @@RowCount


SELECT 
	AccountKey, 
	SUM(TotalActualPrice) AS TotalAccountSales,
	SUM(TotalQuantity) AS TotalAccountQty,
	COUNT(DISTINCT fil.ItemKey) AS TotalNumberOfItemSold,
	SUM(TotalActualPrice) - SUM(TotalActualCost) AS TotalAccountGP,
	COUNT(DISTINCT ItemUDVarChar5) AS TotalNumberOfSuperCategoriesSold
INTO #LastAISAccountSales
FROM dbo.FactInvoiceLine fil
INNER JOIN dbo.DimItem di
	ON di.ItemKey = fil.ItemKey
WHERE Last12MonthsIndicator = 'Y'
GROUP BY AccountKey

CREATE UNIQUE CLUSTERED INDEX I_TAS_1 ON #LastAISAccountSales(AccountKey)

EXEC LogDCPEvent 'ETL - lion.FactLastPriceAndCost -    #LastAISAccountSales', 'E', 0,0,0

EXEC LogDCPEvent 'ETL - lion.FactLastPriceAndCost -    #AccountItemSales', 'B'

SELECT
	AccountItemTotals.AccountKey,
	AccountItemTotals.ItemKey,
	CASE TotalAccountSales
		WHEN 0.0 THEN 0.0
		ELSE TotalAccountItemSales/TotalAccountSales
	END AS 'PercentAccountItemSalesToAccountSales',
	TotalNumberOfItemSold,
	TotalAccountGP,
	TotalNumberOfSuperCategoriesSold,
	TotalAccountSales,
	TotalAccountQty,
	TotalAccountItemSales,
	--TotalAccountItemQuantity,
	--TotalAccountItemFrequency,
	CumSales,
	CumQty,
	Frequency
	--PERCENT_RANK() OVER (PARTITION BY AccountTotals.AccountKey ORDER BY TotalAccountItemSales) AS SalesPercentRank,
	--PERCENT_RANK() OVER (PARTITION BY AccountTotals.AccountKey ORDER BY TotalAccountItemQuantity) AS QuantityPercentRank,
	--PERCENT_RANK() OVER (PARTITION BY AccountTotals.AccountKey ORDER BY TotalAccountItemFrequency) AS FrequencyPercentRank
INTO #AccountItemSales
FROM 
	(SELECT 
		AccountKey, 
		ItemKey, 
		SUM(TotalActualPrice) AS TotalAccountItemSales,
		--SUM(TotalQuantity) AS TotalAccountItemQuantity,
		--COUNT(*) AS TotalAccountItemFrequency,
		SUM(SUM(TotalActualPrice)) OVER (PARTITION BY AccountKey ORDER BY SUM(TotalActualPrice) ASC) AS CumSales,
        SUM(SUM(TotalQuantity)) OVER ( PARTITION BY AccountKey ORDER BY SUM(TotalQuantity) ASC) AS CumQty,
        SUM(CASE WHEN TotalActualPrice > 0. THEN 1
                 ELSE 0
            END) AS Frequency		
	FROM dbo.FactInvoiceLine
	WHERE Last12MonthsIndicator = 'Y'
	GROUP BY 
		AccountKey, 
		ItemKey
	) AccountItemTotals
JOIN #LastAISAccountSales AccountTotals ON 
	AccountItemTotals.AccountKey = AccountTotals.AccountKey

CREATE UNIQUE CLUSTERED INDEX I_AIS_1 ON #AccountItemSales(AccountKey, ItemKey)

EXEC LogDCPEvent 'ETL - lion.FactLastPriceAndCost -    #AccountItemSales', 'E', 0,0,0



EXEC LogDCPEvent 'ETL - lion.FactLastPriceAndCost -    #LastAISTotals', 'B'

SELECT 
	AccountKey, 
	ItemKey, 
	InvoiceLineGroup1Key, 
	SUM(TotalActualPrice) AS TotalAISSales, 
	SUM(TotalActualCost) AS TotalAISCost, 
	SUM(TotalQuantity) AS TotalAISQuantity, 
	AVG(TotalQuantity) AS AverageQuantityPerOrder, 
	COUNT(*) AS Total12MonthInvoiceLines, 
	COUNT(DISTINCT ClientInvoiceID) AS Total12MonthInvoices
INTO #LastAISTotals
FROM dbo.FactInvoiceLine
WHERE Last12MonthsIndicator = 'Y'
GROUP BY 
	AccountKey, 
	ItemKey, 
	InvoiceLineGroup1Key

CREATE CLUSTERED INDEX I_TAIS_1 ON #LastAISTotals (AccountKey, TotalAISSales DESC)

EXEC LogDCPEvent 'ETL - lion.FactLastPriceAndCost -    #LastAISTotals', 'E', 0,0,0


EXEC LogDCPEvent 'ETL - lion.FactLastPriceAndCost -    #AISTRiskRanking', 'B'

SELECT 
	AccountKey, 
	ItemKey, 
	InvoiceLineGroup1Key,
	(SELECT COUNT(*) FROM #LastAISTotals T2 WHERE T2.AccountKey = T.AccountKey AND T2.TotalAISSales >= T.TotalAISSales) AS 'RowRank',
	(SELECT COUNT(*) FROM #LastAISTotals T2 WHERE T2.AccountKey = T.AccountKey) AS AccountCount
INTO #AISTRiskRanking
FROM #LastAISTotals T

CREATE UNIQUE CLUSTERED INDEX I_TARR_1 ON #AISTRiskRanking (AccountKey, ItemKey, InvoiceLineGroup1Key)
EXEC LogDCPEvent 'ETL - lion.FactLastPriceAndCost -    #AISTRiskRanking', 'E', 0,0,0


EXEC LogDCPEvent 'ETL - lion.FactLastPriceAndCost -    #LikeItemCodeSales', 'B'

SELECT
	Account_LIC_ILG1Totals.AccountKey,
	Account_LIC_ILG1Totals.LikeItemCode,
	Account_LIC_ILG1Totals.InvoiceLineGroup1Key,
	CASE TotalAccountSales
		WHEN 0.0 THEN 0.0
		ELSE LikeItemCodeTotalSales/TotalAccountSales
	END AS 'PercentLikeItemCodeToTotalAccountSales',
	LikeItemCodeTotalInvoiceLines,
	LikeItemCodeTotalSales
INTO #LikeItemCodeSales
FROM 
	(
	SELECT 
		AccountKey, 
		LikeItemCode, 
		InvoiceLineGroup1Key, 
		SUM(TotalActualPrice) AS LikeItemCodeTotalSales, 
		COUNT(*) AS LikeItemCodeTotalInvoiceLines
	FROM dbo.FactInvoiceLine FIL
	JOIN dbo.DimItem DI ON 
		DI.ItemKEy = FIL.ItemKey
	WHERE Last12MonthsIndicator = 'Y'
	GROUP BY 
		AccountKey, 
		LikeItemCode, 
		InvoiceLineGroup1Key
	) Account_LIC_ILG1Totals
JOIN #LastAISAccountSales AccountTotals ON 
	Account_LIC_ILG1Totals.AccountKey = AccountTotals.AccountKey

CREATE CLUSTERED INDEX I_LICS_1 ON #LikeItemCodeSales(AccountKey,LikeItemCode,InvoiceLineGroup1Key)

EXEC LogDCPEvent 'ETL - lion.FactLastPriceAndCost -    #LikeItemCodeSales', 'E'



EXEC LogDCPEvent 'ETL - lion.FactLastPriceAndCost -    #LastAIS', 'B'

; WITH Lines AS (
	SELECT
		AccountKey, 
		ItemKey, 
		InvoiceLineGroup1Key,
		CASE
			WHEN 
				TotalNetQuantity IS NOT NULL
				AND TotalNetPrice IS NOT NULL
				AND TotalNetCost IS NOT NULL
				THEN TotalNetQuantity
			ELSE 
				TotalQuantity
		END AS TotalQuantity,
		CASE
			WHEN 
				TotalNetQuantity IS NOT NULL
				AND TotalNetPrice IS NOT NULL
				AND TotalNetCost IS NOT NULL
				THEN ISNULL(TotalNetPrice / NULLIF(TotalNetQuantity, 0.0), 0.0)
			ELSE 
				ISNULL(TotalActualPrice / NULLIF(TotalQuantity, 0.0), 0.0)
		END AS UnitPrice,
		CASE
			WHEN
				TotalNetQuantity IS NOT NULL
				AND TotalNetPrice IS NOT NULL
				AND TotalNetCost IS NOT NULL
				THEN ISNULL(TotalNetCost / NULLIF(TotalNetQuantity, 0.0), 0.0)
			ELSE 
				ISNULL(TotalActualCost / NULLIF(TotalQuantity, 0.0), 0.0)
		END AS UnitCost,
		CASE
			WHEN
				TotalNetQuantity IS NOT NULL
				AND TotalNetPrice IS NOT NULL
				AND TotalNetCost IS NOT NULL
				THEN 
					ISNULL(	(UnitListPrice - (TotalNetPrice/NULLIF(TotalNetQuantity, 0.0))) / NULLIF(UnitListPrice, 0.), 0.)
			ELSE 
				ISNULL(	(UnitListPrice - (TotalActualPrice/NULLIF(TotalQuantity, 0.0))) / NULLIF(UnitListPrice, 0.), 0.)
		END AS DiscountPercent,
		UnitListPrice,
		ISNULL(UDDecimal3/NULLIF(TotalQuantity, 0.0), 0.0) AS UnitRetailPrice,
		ISNULL(TotalActualRebate/NULLIF(TotalQuantity, 0.0), 0.0) AS UnitRebate,
		InvoiceLineGroup2Key,
		InvoiceLineGroup3Key,
		InvoiceDateDayKey,
		ClientInvoiceID,
		ClientInvoiceLineID,
		InvoiceLineUniqueIdentifier,
		LinkedInvoiceLineUniqueIdentifier,
		InvoiceLineJunkKey,
		Last12MonthsIndicator,
		UDVarchar1 AS PriceDerivationCode,
		TotalNetSupplierCost 
	FROM dbo.FactInvoiceLine
	WHERE Last12MonthsIndicator = 'Y'
), Lines2 AS (
	SELECT
		AccountKey, 
		ItemKey, 
		InvoiceLineGroup1Key,	
		UnitPrice,
		UnitCost,
		UnitRebate,
		TotalQuantity,
		DiscountPercent,
		UnitListPrice,
		UnitRetailPrice,
		InvoiceLineGroup2Key,
		InvoiceLineGroup3Key,
		InvoiceDateDayKey,
		ClientInvoiceID,
		ClientInvoiceLineID,
		InvoiceLineUniqueIdentifier,
		LinkedInvoiceLineUniqueIdentifier,
		InvoiceLineJunkKey,
		PriceDerivationCode,
		TotalNetSupplierCost,
		CASE
			WHEN 
				ISNULL(((UnitPrice-UnitCost)/NULLIF(UnitPrice, 0.0)), -9999) BETWEEN -1.2 AND 0.90 AND 
				TotalQuantity > 0.0 
				AND UnitPrice > 0.0 
				AND UnitCost > 0.0
				AND PriceDerivationCode <> N'SPO' -- PriceDerivation
				AND PriceDerivationCode <> N'CCP' -- PriceDerivation
	 			--AND LinkedInvoiceLineUniqueIdentifier IS NULL
	 			AND Last12MonthsIndicator = 'Y'
				THEN 'Y' -- good to use as last
			ELSE 'N' -- bad; to get used only if not "good"/last resort
		END AS ValidForLastIndicator
	FROM Lines
), RankedLines AS (
	SELECT
		AccountKey, 
		ItemKey, 
		InvoiceLineGroup1Key,	
		UnitPrice,
		UnitCost,
		UnitRebate,
		TotalQuantity,
		DiscountPercent,
		UnitListPrice,
		UnitRetailPrice,
		InvoiceLineGroup2Key,
		InvoiceLineGroup3Key,
		InvoiceDateDayKey,
		ClientInvoiceID,
		ClientInvoiceLineID,
		InvoiceLineUniqueIdentifier,
		LinkedInvoiceLineUniqueIdentifier,
		InvoiceLineJunkKey,
		ValidForLastIndicator,
		PriceDerivationCode,
		TotalNetSupplierCost,
		ROW_NUMBER() OVER (PARTITION BY AccountKey, ItemKey, InvoiceLineGroup1Key
			ORDER BY 
				ValidForLastIndicator DESC,  -- want Y's to be first
				InvoiceDateDayKey DESC,
				InvoiceLineUniqueIdentifier DESC
		) AS RowRank
	FROM Lines2
)
SELECT
* 
INTO #LastAIS
FROM RankedLines
WHERE
	RowRank = 1

CREATE UNIQUE CLUSTERED INDEX I_1 ON #LastAIS (AccountKey, ItemKey, InvoiceLineGroup1Key)

EXEC LogDCPEvent 'ETL - lion.FactLastPriceAndCost -    #LastAIS', 'E',0,0,0


EXEC LogDCPEvent 'ETL - lion.FactLastPriceAndCost -    #LastAISAlternate', 'B'

SELECT
	AccountKey,
	ItemKey,
	InvoiceLineGroup1Key,
	AlternateLastInvoiceLineUniqueIdentifier
INTO #LastAISAlternate
FROM ( 
	SELECT 
	fil.AccountKey, 
	fil.ItemKey, 
	fil.InvoiceLineGroup1Key, 
	fil.InvoiceLineUniqueIdentifier AS AlternateLastInvoiceLineUniqueIdentifier, 
	ROW_NUMBER() OVER (
		PARTITION BY fil.AccountKey, fil.ItemKey, fil.InvoiceLineGroup1Key 
		ORDER BY fil.AccountKey, 
			fil.ItemKey, 
			fil.InvoiceLineGroup1Key, 
			fil.InvoiceDateDayKey DESC, 
			fil.TotalQuantity DESC, 
			fil.TotalActualPrice DESC, 
			fil.ClientInvoiceID DESC, 
			fil.InvoiceLineUniqueIdentifier DESC) AS RowNumber
FROM dbo.FactInvoiceLine fil
WHERE 
	Last12MonthsIndicator = 'Y'
	AND TotalQuantity > 0.0 
	AND TotalActualPrice > 0.0
	AND TotalActualCost > 0.0
	AND (ISNULL(TotalActualPrice, 0.0) - ISNULL(TotalActualCost, 0.0))/NULLIF(TotalActualPrice, 0) BETWEEN -1.2 AND 0.90 
	AND ( fil.InvoiceLineGroup1Key < 5 OR (fil.InvoiceLineGroup1Key >= 5 AND (ISNULL(fil.UDDecimal2, 0) > 0)) ) 
) a 
WHERE 
	RowNumber = 1 

CREATE UNIQUE CLUSTERED INDEX I_1 ON #LastAISAlternate (AccountKey, ItemKey, InvoiceLineGroup1Key)

EXEC LogDCPEvent 'ETL - lion.FactLastPriceAndCost -    #LastAISAlternate', 'E',0,0,0


EXEC LogDCPEvent 'ETL - lion.FactLastPriceAndCost - Insert', 'B'

--Insert for any rows that we found a last price/cost for.  If we did not find one, we'll insert zeros here instead.
INSERT INTO dbo.FactLastPriceAndCost
(
	AccountKey, 
	ItemKey, 
	InvoiceLineGroup1Key, 
	LastSaleDayKey, 
	LastItemPrice, 
	LastItemCost,
	LastItemRebate,
	LastItemDiscountPercent,
	LastQuantity,
	LastInvoiceLineGroup2Key,
	LastInvoiceLineGroup3Key,
	AverageQuantityPerOrder,
	PercentAccountItemSalesToAccountSales,
	LikeItemCodePercent,
	Total12MonthInvoices,
	Total12MonthInvoiceLines,
	Total12MonthSales,
	Total12MonthCost,
	Total12MonthQuantity,	
	AISToAccountSalesRowRanking,
	LICS.LikeItemCodeTotalInvoiceLines,
	LICS.LikeItemCodeTotalSales,
	LastClientInvoiceID,
	LastClientInvoiceLineID,
	LastInvoiceLineUniqueIdentifier,
	LastInvoiceLineJunkKey,
	AlternateLastInvoiceLineUniqueIdentifier,
	UDDecimal1,
	UDDecimal2,
	UDDecimal3,
	UDDecimal4,
	UDDecimal5,
	UDDecimal6,
	UDVarchar1,
	UDVarchar2,
	UDVarchar3,
	UDVarchar4,
	UDVarchar5,
	UDVarchar6,
	UDVarchar7,
	UDVarchar8,
	UDVarchar9,
	UDVarchar10,
	CustomerBrandSegment,
	LastPriceDerivationCode,
	LastSupplierCost,
	CustomerSize
)
SELECT
	T.AccountKey,
	T.ItemKey, 
	T.InvoiceLineGroup1Key, 
	T.InvoiceDateDayKey, 
	CASE ValidForLastIndicator
		WHEN 'Y' THEN UnitPrice
		ELSE 0.0
	END AS LastItemPrice,
	CASE ValidForLastIndicator
		WHEN 'Y' THEN UnitCost
		ELSE 0.0
	END AS LastItemCost,
	CASE ValidForLastIndicator
		WHEN 'Y' THEN UnitRebate
		ELSE 0.0
	END AS LastItemRebate,	
	CASE ValidForLastIndicator
		WHEN 'Y' THEN DiscountPercent 
		ELSE 0.0
	END AS LastItemDiscountPercent,	

	CASE ValidForLastIndicator
		WHEN 'Y' THEN ISNULL(T.TotalQuantity, 0.) 
		ELSE 0.0
	END AS LastQuantity,	

	T.InvoiceLineGroup2Key,
	T.InvoiceLineGroup3Key,
	ISNULL(AIST.AverageQuantityPerOrder, 0.0),
	ISNULL(AIS.PercentAccountItemSalesToAccountSales, 0.0),
	ISNULL(LICS.PercentLikeItemCodeToTotalAccountSales, 0.0) AS LikeItemCodePercent,
	ISNULL(AIST.Total12MonthInvoices, 0.0),
	ISNULL(AIST.Total12MonthInvoiceLines, 0.0),
	ISNULL(AIST.TotalAISSales, 0.0) AS Total12MonthSales,
	ISNULL(AIST.TotalAISCost, 0.0) AS Total12MonthCost,
	ISNULL(AIST.TotalAISQuantity, 0.0) AS Total12MonthQuantity,
	CASE AISTRR.AccountCount 
		WHEN NULL THEN 0.0
		WHEN 0.0 THEN 0.0
		ELSE CAST(AISTRR.RowRank AS DECIMAL(19,8))/CAST(AISTRR.AccountCount AS DECIMAL(19,8)) 
	END AS AISToAccountSalesRowRanking,
	ISNULL(LICS.LikeItemCodeTotalInvoiceLines, 0.0),
	ISNULL(LICS.LikeItemCodeTotalSales, 0.0),
	ClientInvoiceID,
	ClientInvoiceLineID AS ClientInvoiceLineID,
	InvoiceLineUniqueIdentifier AS InvoiceLineUniqueIdentifier,
	ISNULL(InvoiceLineJunkKey, @UnknownJunkKey) AS LastInvoiceLineJunkKey,
	alt.AlternateLastInvoiceLineUniqueIdentifier,

	ISNULL(AIS.TotalAccountSales, 0.) AS UDDecimal1,
	ISNULL(AIS.TotalNumberOfItemSold, 0.) AS UDDecimal2,
	ISNULL(AIS.TotalAccountGP, 0.) AS UDDecimal3,
	ISNULL(AIS.TotalNumberOfSuperCategoriesSold, 0.) AS UDDecimal4,
	ISNULL(UnitListPrice, 0.) AS UDDecimal5,
	ISNULL(UnitRetailPrice, 0.) AS UDDecimal6,

	CASE
		WHEN AG2Level1 = 'CS' THEN 'N'
		WHEN 
			CumSales > ( TotalAccountSales * .9 )
			OR CumQty > ( TotalAccountQty * .9 )
			OR Frequency > 11 THEN 'Y'
		ELSE 'N'
	END  AS UDVarchar1,				-- StrategicItem
	CASE 
		WHEN ValidForLastIndicator = 'Y' AND ROUND((UnitListPrice - UnitCost) / NULLIF(UnitListPrice, 0),3) < .269 THEN 'Low'
		WHEN ValidForLastIndicator = 'Y' AND ROUND((UnitListPrice - UnitCost) / NULLIF(UnitListPrice, 0),3) >= .269 AND ROUND((UnitListPrice - UnitCost) / NULLIF(UnitListPrice, 0),3) < .690 THEN 'Mid'
		WHEN ValidForLastIndicator = 'Y' AND ROUND((UnitListPrice - UnitCost) / NULLIF(UnitListPrice, 0),3) >= .690 THEN 'High'
		ELSE 'Mid'
	END  AS UDVarchar2,				-- SubLLSPG	
	'N' AS UDVarchar3,				-- NewItemForAccountIndicator
	N'Small/Cash' AS UDVarchar4,	-- Sales603010Bucket
	'Low' AS UDVarchar5,			-- TotalSalesAccountBucket
	'Low' AS UDVarchar6,			-- frequency bucket
	'1' AS UDVarchar7,				-- CustBaseBucket
	'Low' AS UDVarchar8,			-- FreqPurchBucket 
	'Low' AS UDVarchar9,			-- TopSellerBucket
	 N'SmallMedium' AS UDVarchar10,	-- PriceApproach
	 NULL AS CustomerBrandSegment,
	 PriceDerivationCode AS LastPriceDerivationCode,
	 TotalNetSupplierCost AS LastSupplierCost,
	 CASE
		WHEN AG2Level1 = N'CS' THEN N'5-Cash'
		WHEN AIS.TotalAccountSales >  1000000 THEN N'1-Ultra'
		WHEN AIS.TotalAccountSales >=  128000 THEN N'2-High'
		WHEN AIS.TotalAccountSales >=   50000 THEN N'3-Medium'
		ELSE N'4-Low'
	END AS CustomerSize
FROM #LastAIS T
INNER JOIN dbo.DimAccount da
	ON da.AccountKey = T.AccountKey
INNER JOIN dbo.DimAccountGroup2 dag2
	ON dag2.AccountGroup2Key = da.AccountGroup2Key

LEFT JOIN #LastAISAlternate alt ON
	alt.AccountKey = T.AccountKey AND 
	alt.ItemKey = T.ItemKey AND 
	alt.InvoiceLineGroup1Key = T.InvoiceLineGroup1Key

LEFT JOIN #LastAISTotals AIST ON
	AIST.AccountKey = T.AccountKey AND 
	AIST.ItemKey = T.ItemKey AND 
	AIST.InvoiceLineGroup1Key = T.InvoiceLineGroup1Key
LEFT JOIN dbo.DimItem DI ON 
	DI.ItemKey = T.ItemKey
LEFT JOIN #AccountItemSales AIS ON 
	AIS.AccountKey = T.AccountKey AND 
	AIS.ItemKey = T.ItemKey
LEFT JOIN #AISTRiskRanking AISTRR ON 
	AISTRR.AccountKey = T.ACcountKey AND 
	AISTRR.ItemKey = T.ItemKey AND 
	AISTRR.InvoiceLineGroup1Key = T.InvoiceLineGroup1Key
LEFT JOIN #LikeItemCodeSales LICS ON 
	LICS.AccountKey = T.AccountKey AND 
	LICS.LikeItemCode = DI.LikeItemCode AND 
	LICS.InvoiceLineGroup1Key = T.InvoiceLineGroup1Key
LEFT JOIN dbo.DimInvoiceLineGroup2 dilg2
	ON dilg2.InvoiceLineGroup2Key = T.InvoiceLineGroup2Key
--LEFT JOIN dbo.FactRebate
--	ON FactRebate.AccountKey = t.AccountKey
--	AND FactRebate.ItemKey = t.ItemKey
SET @RowsI = @RowsI + @@RowCount

EXEC LogDCPEvent 'ETL - lion.FactLastPriceAndCost - Insert', 'E',  @RowsI, @RowsU, @RowsD


SET @RowsI = 0
SET @RowsU = 0
SET @RowsD = 0


SELECT TOP 10 * FROM dbo.FactLastPriceAndCost

EXEC LogDCPEvent 'ETL - lion.FactLastPriceAndCost - Indexes Create', 'B'

EXEC DBA_IndexRebuild 'FactLastPriceAndCost', 'Create'

EXEC LogDCPEvent 'ETL - lion.FactLastPriceAndCost - Indexes Create', 'E',0,0,0



EXEC LogDCPEvent 'ETL - lion.FactLastPriceAndCost - Create Attributes', 'B'


--UPDATE flpac
--SET 
--	UDDecimal1 = ISNULL(TotalAccountSales, 0.),
--	UDDecimal3 = ISNULL(TotalNumberOfItemSold, 0.),
--	UDDecimal4 = ISNULL(TotalAccountGP, 0.),
--	UDDecimal5 = ISNULL(TotalNumberOfSuperCategoriesSold, 0.)
--FROM dbo.FactLastPriceAndCost flpac
--LEFT JOIN #LastAISAccountSales a
--	ON a.AccountKey = flpac.AccountKey


---- RegionExceptionIndicator/AreaExceptionIndicator
--EXEC LogDCPEvent 'ETL - lion.FactLastPriceAndCost - Create Attributes: RegionExceptionIndicator', 'B'
--UPDATE flpac
--	SET UDVarchar3 = 
--		ISNULL(
--			CASE
--				WHEN dag1.AG1Level1UDVarchar2 = 'NI' THEN 'NI'
--				ELSE 'N'
--			END, 
--			'N')
--FROM dbo.FactLastPriceAndCost flpac
--INNER JOIN dbo.DimAccount da
--	ON flpac.AccountKey = da.AccountKey
--INNER JOIN dbo.DimAccountGroup1 dag1
--	ON da.AccountGroup1Key = dag1.AccountGroup1Key
--EXEC LogDCPEvent 'ETL - lion.FactLastPriceAndCost - Create Attributes: RegionExceptionIndicator', 'E', 0, @@ROWCOUNT, 0


-- NewItemForAccountIndicator: 'New' Sales if total item sales for Jan-Sep 2008 = 0 then flag the 2009 'New' Sales = Y. In other words if there are total item sales > 0 in 2008 then 'New' flag = N
EXEC LogDCPEvent 'ETL - lion.FactLastPriceAndCost - Create Attributes: NewItemForAccountIndicator', 'B'
; WITH SalesSummary AS (
	SELECT
		AccountKey,
		ItemKey,
		InvoiceLineGroup1Key,
		SUM(CASE WHEN MonthSequenceNumber IN (1, 2, 3) THEN TotalActualPrice ELSE 0.0 END) AS RecentSales,
		SUM(CASE WHEN MonthSequenceNumber NOT IN (1, 2, 3) THEN TotalActualPrice ELSE 0.0 END) AS PreviousSales
	FROM dbo.FactInvoiceLine fil
	INNER JOIN DimMonth dm ON
		dm.MonthKey = fil.InvoiceDateMonthKey
	WHERE
		 Last12MonthsIndicator = 'Y'
	GROUP BY 
		AccountKey,
		ItemKey,
		InvoiceLineGroup1Key
)
UPDATE flpac
	SET UDVarchar3 = 
		ISNULL(
			CASE
				WHEN PreviousSales = 0.0 AND RecentSales > 0.0 THEN 'Y'
				ELSE 'N'
			END, 
			'N')
FROM dbo.FactLastPriceAndCost flpac
INNER JOIN SalesSummary s 
	ON s.AccountKey = flpac.AccountKey
	AND s.ItemKey = flpac.ItemKey
	AND s.InvoiceLineGroup1Key = flpac.InvoiceLineGroup1Key
EXEC LogDCPEvent 'ETL - lion.FactLastPriceAndCost - Create Attributes: NewItemForAccountIndicator', 'E', 0, @@ROWCOUNT, 0



EXEC LogDCPEvent 'ETL - lion.FactLastPriceAndCost - Create Attributes: Sales603010Bucket, Sales6020155Bucket, and PriceApproach', 'B'

-- get a list of ALL accounts and their sales
SELECT
	DISTINCT
	flpac.AccountKey,
	UDDecimal1 AS TotalSales,
	N'D' AS Sales6020155Bucket
INTO #AccountListForSales6020155Bucket
FROM dbo.FactLastPriceAndCost flpac
INNER JOIN dbo.DimAccount da
	ON da.AccountKey = flpac.AccountKey
INNER JOIN dbo.DimAccountGroup2 dag2
	ON dag2.AccountGroup2Key = da.AccountGroup2Key
WHERE UDDecimal1 IS NOT NULL
	AND UDDecimal1 <> 0.
	AND ISNULL(AccountUDVarChar8, N'') NOT IN (N'National')
	AND AG2Level1 <> N'CS'

CREATE UNIQUE CLUSTERED INDEX I_1 ON #AccountListForSales6020155Bucket(AccountKey)

; WITH a AS (  -- A Substitute until a swith over to the use the temp table above
	SELECT
		DISTINCT
		al.AccountKey,
		TotalSales
	FROM #AccountListForSales6020155Bucket al
), RunningTotal AS (
	SELECT
		TotalSales,
		SUM(SUM(TotalSales)) OVER (
			ORDER BY TotalSales DESC 
			ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW
		) AS CumulativeTotal,
		SUM(SUM(TotalSales)) OVER () AS GrandTotal
	FROM
		a
	GROUP BY
		TotalSales
)
UPDATE al
	SET
		Sales6020155Bucket = 
			CASE
				WHEN CAST(CumulativeTotal AS DEC(19,8))/CAST(GrandTotal AS DEC(19,8)) < 0.60 THEN N'A'
				WHEN CAST(CumulativeTotal AS DEC(19,8))/CAST(GrandTotal AS DEC(19,8)) < 0.80 THEN N'B'
				WHEN CAST(CumulativeTotal AS DEC(19,8))/CAST(GrandTotal AS DEC(19,8)) < 0.95 THEN N'C'
				ELSE N'D'
			END
FROM #AccountListForSales6020155Bucket al
INNER JOIN dbo.DimAccount da
	ON da.AccountKey = al.AccountKey
INNER JOIN dbo.DimAccountGroup2 dag2
	ON dag2.AccountGroup2Key = da.AccountGroup2Key
INNER JOIN RunningTotal 
	ON RunningTotal.TotalSales = al.TotalSales

-- get a list of ALL accounts and their sales INCLUDING those that no sales (as their flpac records will need to be updated to the current bucket value)
SELECT
	DISTINCT
	flpac.AccountKey,
	UDDecimal1 AS TotalSales,
	N'Small/Cash' AS Sales603010Bucket
INTO #AccountList
FROM dbo.FactLastPriceAndCost flpac
INNER JOIN dbo.DimAccount da
	ON da.AccountKey = flpac.AccountKey
INNER JOIN dbo.DimAccountGroup2 dag2
	ON dag2.AccountGroup2Key = da.AccountGroup2Key
WHERE UDDecimal1 IS NOT NULL
	AND UDDecimal1 <> 0.
	AND ISNULL(AccountUDVarChar8, N'') NOT IN (N'National', N'Sales Team')
	AND AG2Level1 <> N'CS'

CREATE UNIQUE CLUSTERED INDEX I_1 ON #AccountList(AccountKey)

; WITH a AS (  -- A Substitute until a swith over to the use the temp table above
	SELECT
		DISTINCT
		al.AccountKey,
		TotalSales
	FROM #AccountList al
), RunningTotal AS (
	SELECT
		TotalSales,
		SUM(SUM(TotalSales)) OVER (
			ORDER BY TotalSales DESC 
			ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW
		) AS CumulativeTotal,
		SUM(SUM(TotalSales)) OVER () AS GrandTotal
	FROM
		a
	GROUP BY
		TotalSales
)
UPDATE al
	SET 
		Sales603010Bucket = 
			CASE
				WHEN CAST(CumulativeTotal AS DEC(19,8))/CAST(GrandTotal AS DEC(19,8)) < 0.6 THEN N'Large'
				WHEN CAST(CumulativeTotal AS DEC(19,8))/CAST(GrandTotal AS DEC(19,8)) < 0.9 THEN N'Medium'
				ELSE N'Small'
			END
FROM #AccountList al
INNER JOIN RunningTotal 
	ON RunningTotal.TotalSales = al.TotalSales

UPDATE da
	SET Sales603010Bucket =
			CASE
				WHEN ISNULL(AG2Level1, N'') = N'CS' THEN N'Small/Cash'
				WHEN ISNULL(AccountUDVarChar8, N'') = N'National' THEN N'National'
				WHEN ISNULL(AccountUDVarChar8, N'') = N'Sales Team' THEN N'Sales Team'
				WHEN ISNULL(al.Sales603010Bucket, N'') = N'Small' THEN N'Small/Cash'
				ELSE ISNULL(al.Sales603010Bucket, N'Small/Cash')	-- else computed Large or Medium
			END,
	Sales6020155Bucket =
			CASE
				WHEN ISNULL(AG2Level1, N'') = N'CS' THEN N'Small/Cash'
				WHEN ISNULL(AccountUDVarChar8, N'') = N'National' THEN N'National'
				WHEN ISNULL(al2.Sales6020155Bucket, N'') = N'D' THEN N'D'
				ELSE ISNULL(al2.Sales6020155Bucket, N'D')	-- else computed Large or Medium
			END
FROM dbo.DimAccount da
INNER JOIN dbo.DimAccountGroup2 dag2
	ON dag2.AccountGroup2Key = da.AccountGroup2Key
LEFT JOIN #AccountList al
	ON al.AccountKey = da.AccountKey
LEFT JOIN #AccountListForSales6020155Bucket al2
	ON al2.AccountKey = da.AccountKey

UPDATE flpac
	SET 
		UDVarChar4 = 
			CASE
				WHEN ISNULL(AG2Level1, N'') = N'CS' THEN N'Small/Cash'
				WHEN ISNULL(AccountUDVarChar8, N'') = N'National' THEN N'National'
				WHEN ISNULL(AccountUDVarChar8, N'') = N'Sales Team' THEN N'Sales Team'
				WHEN ISNULL(al.Sales603010Bucket, N'') = N'Small' THEN N'Small/Cash'
				ELSE ISNULL(al.Sales603010Bucket, N'Small/Cash')	-- else computed Large or Medium
			END,
		UDVarChar10 =											-- PriceApproach
			CASE
				-- 1.6b per Scott lpf-4947 WHEN ItemUDVarChar2 IS NOT NULL AND ItemUDVarChar2 IN (N'BURDN', N'FUSN', N'MPS', N'UPS') THEN N'Infrastructure'
				WHEN ISNULL(AG2Level1, N'') = N'CS' THEN N'Cash'
				WHEN ISNULL(AccountUDVarChar8, N'') = N'National' THEN N'National'
				WHEN ISNULL(AccountUDVarChar8, N'') = N'Sales Team' THEN N'LargeSalesTeam'
				WHEN ISNULL(al.Sales603010Bucket, N'') = N'Large' THEN N'LargeSalesTeam'
				WHEN ISNULL(al.Sales603010Bucket, N'') = N'Medium' THEN N'SmallMedium'
				WHEN ISNULL(al.Sales603010Bucket, N'') = N'Small' THEN N'SmallMedium'
				WHEN ISNULL(al.Sales603010Bucket, N'') = N'Small/Cash' THEN N'SmallMedium'
				ELSE N'SmallMedium'
			END
FROM dbo.FactLastPriceAndCost flpac
INNER JOIN dbo.DimAccount da
	ON da.AccountKey = flpac.AccountKey
INNER JOIN dbo.DimAccountGroup2 dag2
	ON dag2.AccountGroup2Key = da.AccountGroup2Key
INNER JOIN dbo.DimItem di
	ON di.ItemKey = flpac.ItemKey
LEFT JOIN #AccountList al
	ON al.AccountKey = flpac.AccountKey
EXEC LogDCPEvent 'ETL - lion.FactLastPriceAndCost - Create Attributes: Sales603010Bucket, Sales6020155Bucket, and PriceApproach', 'E', 0, @@ROWCOUNT, 0



--TotalSalesAccountBucket
EXEC LogDCPEvent 'ETL - lion.FactLastPriceAndCost - Create Attributes: TotalSalesAccountBucket', 'B'
; WITH TheTotalSalesCustomerValues AS (
	SELECT DISTINCT 
		UDDecimal1 AS TotalSalesCustomer
	FROM dbo.FactLastPriceAndCost
	WHERE UDDecimal1 IS NOT NULL
	AND UDDecimal1 <> 0.0
), TotalSalesCustomerTile AS (
	SELECT
		TotalSalesCustomer,
		NTILE(3) OVER (ORDER BY TotalSalesCustomer ) AS Tile
	FROM TheTotalSalesCustomerValues
)
UPDATE flpac
	SET 
		UDVarchar5 = 
			CASE
				WHEN TotalSalesCustomerTile.Tile = 1 THEN 'Low'
				WHEN TotalSalesCustomerTile.Tile = 2 THEN 'Med'
				WHEN TotalSalesCustomerTile.Tile = 3 THEN 'High'
				ELSE '???'
			END
FROM dbo.FactLastPriceAndCost flpac
INNER JOIN TotalSalesCustomerTile 
	ON TotalSalesCustomerTile.TotalSalesCustomer = flpac.UDDecimal1
EXEC LogDCPEvent 'ETL - lion.FactLastPriceAndCost - Create Attributes: TotalSalesAccountBucket', 'E', 0, @@ROWCOUNT, 0


-- frequency bucket
EXEC LogDCPEvent 'ETL - lion.FactLastPriceAndCost - Create Attributes: frequency bucket', 'B'
; WITH TheFrequencyValues AS (
	SELECT DISTINCT 
		ItemKey,
		InvoiceLineGroup1Key,
		Total12MonthInvoiceLines AS Frequency
	FROM dbo.FactLastPriceAndCost
	WHERE Total12MonthInvoiceLines IS NOT NULL
	AND Total12MonthInvoiceLines <> 0.0
), FrequencyTile AS (
	SELECT
		ItemKey, 
		InvoiceLineGroup1Key,
		Frequency,
		NTILE(3) OVER (PARTITION BY ItemKey, InvoiceLineGroup1Key ORDER BY Frequency ) AS Tile
	FROM TheFrequencyValues
)
UPDATE flpac
	SET 
		UDVarchar6 = 
			CASE
				WHEN FrequencyTile.Tile = 1 THEN 'Low'
				WHEN FrequencyTile.Tile = 2 THEN 'Med'
				WHEN FrequencyTile.Tile = 3 THEN 'High'
				ELSE '???'
			END
FROM dbo.FactLastPriceAndCost flpac
INNER JOIN FrequencyTile 
	ON FrequencyTile.ItemKey = flpac.ItemKey
	AND FrequencyTile.InvoiceLineGroup1Key = flpac.InvoiceLineGroup1Key
	AND FrequencyTile.Frequency = flpac.Total12MonthInvoiceLines
EXEC LogDCPEvent 'ETL - lion.FactLastPriceAndCost - Create Attributes: frequency bucket', 'E', 0, @@ROWCOUNT, 0


-- CustBaseBucket
EXEC LogDCPEvent 'ETL - lion.FactLastPriceAndCost - Create Attributes: CustBaseBucket', 'B'
; WITH a AS (
	SELECT
		ItemKey,
		COUNT(DISTINCT AccountKey) AS AccountCount
	FROM dbo.FactLastPriceAndCost
	--WHERE Last12MonthsIndicator = 'Y'
	GROUP BY 
		ItemKey
), Tile AS (
	SELECT
		ItemKey,
		AccountCount,
		NTILE(3) OVER (ORDER BY AccountCount) AS Tile
	FROM a
	WHERE 
		AccountCount > 1
)
UPDATE flpac
	SET 
		UDVarChar7 = 
			CASE
				WHEN Tile.Tile = 1 THEN 'Low'
				WHEN Tile.Tile = 2 THEN 'Med'
				WHEN Tile.Tile = 3 THEN 'High'
				ELSE CAST(a.AccountCount AS VARCHAR(25)) --'One'
			END
FROM dbo.FactLastPriceAndCost flpac
LEFT JOIN Tile 
	ON Tile.ItemKey = flpac.ItemKey
LEFT JOIN a 
	ON a.ItemKey = flpac.ItemKey
EXEC LogDCPEvent 'ETL - lion.FactLastPriceAndCost - Create Attributes: CustBaseBucket', 'E', 0, @@ROWCOUNT, 0


--FreqPurchBucket
EXEC LogDCPEvent 'ETL - lion.FactLastPriceAndCost - Create Attributes: FreqPurchBucket', 'B'
; WITH a AS (
	SELECT
		ItemKey,
		COUNT(*) AS ILCount
	FROM dbo.FactInvoiceLine fil
	WHERE Last12MonthsIndicator = 'Y'
	GROUP BY 
		ItemKey
), RunningTotal AS (
	SELECT
		ItemKey,
		ILCount,
		SUM(SUM(ILCount)) OVER (
			ORDER BY ILCount DESC 
			ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW
		) AS CumulativeTotal,
		SUM(SUM(ILCount)) OVER () AS GrandTotal
	FROM
		a
	GROUP BY
		ItemKey,
		ILCount
)
UPDATE flpac
	SET 
		UDVarChar8 = 
			CASE  
				WHEN CAST(CumulativeTotal AS DEC(19,8))/CAST(GrandTotal AS DEC(19,8)) < 0.2 THEN 'High'
				WHEN CAST(CumulativeTotal AS DEC(19,8))/CAST(GrandTotal AS DEC(19,8)) < 0.6 THEN 'Mid'
				ELSE 'Low'
			END
FROM dbo.FactLastPriceAndCost flpac
INNER JOIN RunningTotal 
	ON RunningTotal.ItemKey = flpac.ItemKey
EXEC LogDCPEvent 'ETL - lion.FactLastPriceAndCost - Create Attributes: FreqPurchBucket', 'E', 0, @@ROWCOUNT, 0


EXEC LogDCPEvent 'ETL - lion.FactLastPriceAndCost - Create Attributes: TopSellerBucket', 'B'
--TopSellerBucket
; WITH a AS (
	SELECT
		ItemKey,
		SUM(TotalActualPrice) AS TotalSales
	FROM dbo.FactInvoiceLine fil
	WHERE Last12MonthsIndicator = 'Y'
	GROUP BY 
		ItemKey
), RunningTotal AS (
	SELECT
		ItemKey,
		TotalSales,
		SUM(SUM(TotalSales)) OVER (
			ORDER BY TotalSales DESC 
			ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW
		) AS CumulativeTotal,
		SUM(SUM(TotalSales)) OVER () AS GrandTotal
	FROM
		a
	GROUP BY
		ItemKey,
		TotalSales
)
UPDATE flpac
	SET 
		UDVarChar9 = 
			CASE  
				WHEN CumulativeTotal/GrandTotal < 0.2 THEN 'High'
				WHEN CumulativeTotal/GrandTotal < 0.6 THEN 'Mid'
				ELSE 'Low'
			END
FROM dbo.FactLastPriceAndCost flpac
INNER JOIN RunningTotal 
	ON RunningTotal.ItemKey = flpac.ItemKey
EXEC LogDCPEvent 'ETL - lion.FactLastPriceAndCost - Create Attributes: TopSellerBucket', 'E', 0, @@ROWCOUNT, 0


EXEC LogDCPEvent 'ETL - lion.FactLastPriceAndCost - Create Attributes: CustomerBrandSegment - Pass 1', 'B'
--CustomerBrandSegment
/* UPDATE flpac SET CustomerBrandSegment = null FROM dbo.FactLastPriceAndCost flpac */

SELECT 
	asb.AccountKey,
	db.Brand,
	db.SegmentBrand,
	asb.Segment
INTO #CustomerBrands
FROM DimAccountBrandSegment asb
INNER JOIN DimBrand db
	ON db.BrandKey = asb.BrandKey

CREATE UNIQUE CLUSTERED INDEX I_1 ON #CustomerBrands (AccountKey, Brand)

UPDATE flpac
	SET 
		CustomerBrandSegment = SegmentBrand + N'-' + Segment
FROM dbo.FactLastPriceAndCost flpac
INNER JOIN dbo.DimItem di
	ON di.ItemKey = flpac.ItemKey
INNER JOIN #CustomerBrands cb
	ON cb.AccountKey = flpac.AccountKey
	AND cb.Brand = ItemUDVarChar2
EXEC LogDCPEvent 'ETL - lion.FactLastPriceAndCost - Create Attributes: CustomerBrandSegment - Pass 1', 'E', 0, @@ROWCOUNT, 0


EXEC LogDCPEvent 'ETL - lion.FactLastPriceAndCost - Create Attributes: CustomerBrandSegment - Pass 2', 'B'
UPDATE flpac
	SET 
		CustomerBrandSegment = SegmentBrand + N'-' + Segment
FROM dbo.FactLastPriceAndCost flpac
INNER JOIN dbo.DimAccount da
	ON da.AccountKey = flpac.AccountKey
INNER JOIN DimAccountGroup1 dag1
	ON dag1.AccountGroup1Key = da.AccountGroup1Key
INNER JOIN #CustomerBrands cb
	ON cb.AccountKey = flpac.AccountKey
	AND cb.Brand = AG1Level1UDVarchar6
WHERE
	flpac.CustomerBrandSegment IS NULL
	AND ISNULL(SegmentBrand, N'') <> N''
	AND cb.Brand <> N'BURDN'
EXEC LogDCPEvent 'ETL - lion.FactLastPriceAndCost - Create Attributes: CustomerBrandSegment - Pass 2', 'E', 0, @@ROWCOUNT, 0


EXEC LogDCPEvent 'ETL - lion.FactLastPriceAndCost - Create Attributes: CustomerBrandSegment - Pass 3', 'B'
UPDATE flpac
	SET 
		CustomerBrandSegment = db.Brand + + N'-' + N'ALL'
FROM dbo.FactLastPriceAndCost flpac
INNER JOIN dbo.DimItem di
	ON di.ItemKey = flpac.ItemKey
INNER JOIN DimBrand db
	ON db.Brand = di.ItemUDVarChar2
WHERE
	flpac.CustomerBrandSegment IS NULL
EXEC LogDCPEvent 'ETL - lion.FactLastPriceAndCost - Create Attributes: CustomerBrandSegment - Pass 3', 'E', 0, @@ROWCOUNT, 0



--EXEC LogDCPEvent 'ETL - lion.FactLastPriceAndCost - Create Attributes: SameDiscountBucket', 'B'
--SameDiscountBucket
--; WITH a AS (
--	SELECT DISTINCT 
--		AccountKey,
--		IG3Level1 AS LLSPGCode,
--		COUNT(DISTINCT di.ItemKey) AS TotalNumberOfItemSold,
--		MIN(ISNULL(	(UnitListPrice - (TotalActualPrice/NULLIF(TotalQuantity, 0.0))) / NULLIF(UnitListPrice, 0.), 0.)) AS MinDiscount,
--		MAX(ISNULL(	(UnitListPrice - (TotalActualPrice/NULLIF(TotalQuantity, 0.0))) / NULLIF(UnitListPrice, 0.), 0.)) AS MaxDiscount
--	FROM dbo.FactInvoiceLine fil
--	INNER JOIN dbo.DimItem di
--		ON di.ItemKey = fil.ItemKey
--	INNER JOIN dbo.DimItemGroup3 dig3
--		ON dig3.ItemGroup3Key = di.ItemGroup3Key
--	WHERE Last12MonthsIndicator = 'Y'
--	GROUP BY
--		AccountKey,
--		IG3Level1
--)
--UPDATE flpac
--SET 
--	flpac.UDVarchar24 = 
--		CASE
--			WHEN TotalNumberOfItemSold = 1 THEN 'One Item'
--			WHEN MaxDiscount - MinDiscount < 0.02 THEN 'Same'
--			WHEN MaxDiscount - MinDiscount >= 0.02 THEN 'Not Same'
--			ELSE NULL
--		END
--FROM dbo.FactLastPriceAndCost flpac
--INNER JOIN dbo.DimItem di
--	ON di.ItemKey = flpac.ItemKey
--INNER JOIN dbo.DimItemGroup3 dig3
--	ON dig3.ItemGroup3Key = di.ItemGroup3Key
--LEFT JOIN a
--	ON a.AccountKey = flpac.AccountKey
--	AND a.LLSPGCode = dig3.IG3Level1
--EXEC LogDCPEvent 'ETL - lion.FactLastPriceAndCost - Create Attributes: NewItemForAccountIndicator', 'E', 0, @@ROWCOUNT, 0






DROP TABLE #AccountList
DROP TABLE #AccountListForSales6020155Bucket
DROP TABLE #LastAISAccountSales
DROP TABLE #AccountItemSales
DROP TABLE #LastAISTotals
DROP TABLE #AISTRiskRanking
DROP TABLE #LikeItemCodeSales
DROP TABLE #LastAIS
DROP TABLE #CustomerBrands


SET @SQLString = 'UPDATE STATISTICS FactLastPriceAndCost'
EXECUTE sp_executesql @SQLString
SET @SQLString = ''


EXEC LogDCPEvent 'ETL - lion.FactLastPriceAndCost - Create Attributes', 'E',  @RowsI, @RowsU, @RowsD


--EXEC LogDCPEvent 'ETL - lion.FactLastPriceAndCost - Missing AIS Insert - fact table insert', 'E',  @RowsI, @RowsU, @RowsD
--EXEC LogDCPEvent 'ETL - lion.FactLastPriceAndCost - Missing AIS Insert', 'E',  @RowsI, @RowsU, @RowsD


EXEC LogDCPEvent 'ETL - lion.FactLastPriceAndCost', 'E',  @RowsI, @RowsU, @RowsD







GO
