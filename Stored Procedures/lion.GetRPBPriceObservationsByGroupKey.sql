SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE [lion].[GetRPBPriceObservationsByGroupKey]
	@ParamPlaybookPricingGroupKey Key_Normal_type
AS

/*
	
	EXEC lion.GetRPBPriceObservationsByGroupKey 651225

*/

SET NOCOUNT ON




SELECT 
	NEWID() AS GUID,
	dag1.AG1Level1 AS Branch,
	ItemNumber,
	ItemDescription,
	pdp.LastItemPrice,
	ISNULL( (pdp.LastItemPrice - pdp.LastItemCost) / pdp.LastItemPrice, 0.0) AS LastItemGPP,
	TotalActual12MonthPrice AS Total12MonthSales,
	SQLDate AS LastSaleDate,
	BandSequenceNumber,
	ppg.PlaybookPricingGroupKey
FROM dbo.PlaybookPricingGroup ppg
INNER JOIN dbo.PlaybookDataPointPricingGroup pdppg
	ON pdppg.PlaybookPricingGroupKey = ppg.PlaybookPricingGroupKey
INNER JOIN dbo.PlaybookDataPoint pdp
	ON pdp.PlaybookDataPointKey = pdppg.PlaybookDataPointKey
INNER JOIN dbo.DimDay dd
	ON dd.DayKey = pdp.LastSaleDayKey
INNER JOIN dbo.DimAccount da
	ON da.AccountKey = pdp.AccountKey
INNER JOIN dbo.DimAccountGroup1 dag1
	ON dag1.AccountGroup1Key = da.AccountGroup1Key
INNER JOIN dbo.DimItem di
	ON di.ItemKey = pdp.ItemKey
INNER JOIN dbo.PlaybookPricingGroupQuantityBand ppgqb
	ON ppgqb.PlaybookPricingGroupKey = ppg.PlaybookPricingGroupKey
	AND pdppg.Total12MonthQuantity BETWEEN ppgqb.LowerBandQuantity
							       AND     ppgqb.UpperBandQuantity 	--gets the datapoints with their associated bands
	
WHERE 
	pdppg.LastItemPrice <> 0.0
	AND ppg.PlaybookPricingGroupKey = @ParamPlaybookPricingGroupKey
	AND RulingMemberFlag = 'Y'
--ORDER BY
--	2, 4, 3




GO
