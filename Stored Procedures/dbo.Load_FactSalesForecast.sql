SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO






CREATE PROCEDURE [dbo].[Load_FactSalesForecast]
AS
SET QUOTED_IDENTIFIER ON 
SET ANSI_NULLS ON 
SET ARITHABORT ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
SET CONCAT_NULL_YIELDS_NULL ON
SET NUMERIC_ROUNDABORT OFF
--This needs to run every month.  Each month, it will first delete anything that's already in the FactSalesForecast
--for the current month.  Then, it'll generate forecasted results for each account/item combination that exists in
--the InvoiceLine table.  Ideally, we'll end up with one row per account/item per month (for items sold in the last 12 months).
--This will make for a very large table, and we should be partitioning this table on disk so that all
--data with the MostRecentForecastFlag = 'Y' is located together on disk, since that's almost always
--the value we'll be using.

EXEC LogDCPEvent 'ETL - dbo.FactSalesForecast - Insert', 'B'

DECLARE @RowsI Quantity_Normal_type, @RowsU Quantity_Normal_type, @RowsD Quantity_Normal_type
SET @RowsI = 0
SET @RowsU = 0
SET @RowsD = 0

DECLARE @TotalMonths Key_Small_Type

--The total # of full months
SET @TotalMonths = (SELECT COUNT(MonthKey) FROM DimMonth WHERE MonthSequenceNumber >= 1)

SET @RowsD = @RowsD + @@RowCount

------12 month--------
--Second, for any account/items that have been sold in the last 12 months, set the most recent flag to N
--This is done because we'll be creating a new forecast for these instead
EXEC DBA_IndexRebuild 'FactSalesForecast', 'Drop_NoClustered'

TRUNCATE TABLE FactSalesForecast

--Now, insert the current months forecast for 12, 6, 3, and N month annualized results
--This forecast is just the average quantity per month from the last X months of sales
SET @RowsU = @RowsU + @@RowCount

SET @RowsI = 0
SET @RowsU = 0
SET @RowsD = 0



INSERT INTO FactSalesForecast (AccountKey, ItemKey, ForecastedMonthlyQty, InvoiceLineGroup1Key, ForecastType)
	-----Last12MonthsIndicator-------
	SELECT 
	AccountKey, 
	ItemKey, 
	SUM(TotalQuantity)/12 AvgMonthlyQty,
	InvoiceLineGroup1Key,
	'Last 12 Months Indicator'
	FROM FactInvoiceLine FIL
	WHERE Last12MonthsIndicator = 'Y'
	GROUP BY AccountKey, ItemKey, InvoiceLineGroup1Key

UNION ALL

	-----12 month-------
	SELECT 
	AccountKey, 
	ItemKey, 
	SUM(TotalQuantity)/12 AvgMonthlyQty,
	InvoiceLineGroup1Key,
	'12 Month Annualized'
	FROM FactInvoiceLine FIL
	JOIN DimMonth DM ON DM.MonthKey = FIL.InvoiceDateMonthKey
	WHERE DM.MonthSequenceNumber BETWEEN 1 AND 12
	GROUP BY AccountKey, ItemKey, InvoiceLineGroup1Key

UNION ALL
	------6 month--------
	SELECT 
	AccountKey, 
	ItemKey, 
	SUM(TotalQuantity)/6 AvgMonthlyQty,
	InvoiceLineGroup1Key,
	'6 Month Annualized'
	FROM FactInvoiceLine FIL
	JOIN DimMonth DM ON DM.MonthKey = FIL.InvoiceDateMonthKey
	WHERE DM.MonthSequenceNumber BETWEEN 1 AND 6
	GROUP BY AccountKey, ItemKey, InvoiceLineGroup1Key

UNION ALL
	------3 month--------
	SELECT 
	AccountKey, 
	ItemKey, 
	SUM(TotalQuantity)/3 AvgMonthlyQty,
	InvoiceLineGroup1Key,
	'3 Month Annualized'
	FROM FactInvoiceLine FIL
	JOIN DimMonth DM ON DM.MonthKey = FIL.InvoiceDateMonthKey
	WHERE DM.MonthSequenceNumber BETWEEN 1 AND 3
	GROUP BY AccountKey, ItemKey, InvoiceLineGroup1Key

UNION ALL

	------N month--------
	SELECT 
	AccountKey, 
	ItemKey, 
	SUM(TotalQuantity)/@TotalMonths AvgMonthlyQty,
	InvoiceLineGroup1Key,
	'N Month Annualized'
	FROM FactInvoiceLine FIL
	JOIN DimMonth DM ON DM.MonthKey = FIL.InvoiceDateMonthKey
	WHERE DM.MonthSequenceNumber >= 1
	GROUP BY AccountKey, ItemKey, InvoiceLineGroup1Key

SET @RowsI = @RowsI + @@RowCount
---------------------------------------------

EXEC DBA_IndexRebuild 'FactSalesForecast', 'Create'

EXEC LogDCPEvent 'ETL - dbo.FactSalesForecast - Insert', 'E', @RowsI, @RowsU, @RowsD












GO
