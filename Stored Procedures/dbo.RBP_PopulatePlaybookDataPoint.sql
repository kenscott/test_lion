SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE PROCEDURE [dbo].[RBP_PopulatePlaybookDataPoint]
	@PlaybookDataPointGroupKey INT
AS

SET QUOTED_IDENTIFIER ON 
SET ANSI_NULLS ON 
SET ARITHABORT ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
SET CONCAT_NULL_YIELDS_NULL ON
SET NUMERIC_ROUNDABORT OFF

DECLARE @RowsI Quantity_Normal_type, @RowsU Quantity_Normal_type, @RowsD Quantity_Normal_type
DECLARE @CoreNonCoreThreshold Quantity_Normal_Type

DECLARE @ProjectKey INT
SELECT @ProjectKey = ProjectKey FROM PlaybookDataPointGroup WHERE PlaybookDataPointGroupKey = @PlaybookDataPointGroupKey
DECLARE @sql NVARCHAR(500)

EXEC LogDCPEvent 'DCP --- RBP_PopulatePlaybookDataPoint: Delete', 'B',0,0,0,@ProjectKey
SET @RowsI = 0
SET @RowsU = 0
SET @RowsD = 0

PRINT 'Delete from PlaybookDataPoint where PlaybookDataPointGroupKey = '+ CAST(@PlaybookDataPointGroupKey AS NVARCHAR(50))

DELETE FROM PlaybookDataPoint WHERE PlaybookDataPointGroupKey = @PlaybookDataPointGroupKey
SET @RowsD = @RowsD + @@RowCount
EXEC LogDCPEvent 'DCP --- RBP_PopulatePlaybookDataPoint: Delete', 'E',@RowsI, @RowsU, @RowsD,@ProjectKey



DECLARE @SegmentFactorPlaybookKey INT
SELECT @SegmentFactorPlaybookKey = SegmentFactorPlaybookKey FROM ATKScenario WHERE ScenarioKey = (SELECT ScenarioKey FROM PlaybookDataPointGroup_View WHERE PlaybookDataPointGroupKey = @PlaybookDataPointGroupKey)

SET @CoreNonCoreThreshold = (SELECT CoreNonCoreThreshold FROM ATKSegmentFactorPlaybook WHERE SegmentFactorPlaybookKey = @SegmentFactorPlaybookKey)

EXEC LogDCPEvent 'DCP --- RBP_PopulatePlaybookDataPoint: Insert PlaybookDataPoint', 'B',0,0,0,@ProjectKey
SET @RowsI = 0
SET @RowsU = 0
SET @RowsD = 0
--select * From PlaybookDataPoint
INSERT INTO PlaybookDataPoint
(
	PlaybookDataPointGroupKey,
	AccountKey, 
	ItemKey, 
	InvoiceLineGroup1Key, 
	Total12MonthQuantity,
	LastSaleDayKey,
	TotalActual12MonthPrice,
	TotalActual12MonthCost,
	--ScenarioScopeAccountTotalSalesBand, 
	--ScenarioScopeAccountTotalGrossProfitBand, 
	--ScenarioScopeAccountAvgGrossProfitPerOrderRank, 
	--ScenarioScopeAccountTotalSalesRank, 
	--ScenarioScopeBlendedRank, 
	FullScopeBlendedRank, 
	CoreNonCoreIndicator,
	AccountItemBandRank, 
	ItemBandRank, 
	FullScopeAccountTotalSalesBand, 
	FullScopeAccountTotalGrossProfitBand, 
	FullScopeAccountAvgGrossProfitPerOrderRank, 
	FullScopeAccountTotalSalesRank,
	LastItemPrice,
	LastItemCost,
	LastPercentAccountItemSalesToAccountSales,
	LastItemDiscountPercent
)
SELECT
	@PlaybookDataPointGroupKey,
	PDP.AccountKey, 
	PDP.ItemKey,
	PDP.InvoiceLineGroup1Key, 
	PDP.Total12MonthQuantity,
	FLPC.LastSaleDayKey,
	PDP.TotalActual12MonthPrice,
	PDP.TotalActual12MonthCost,
--	NULL AS 'ScenarioScopeTotalSalesBand',-- SAB.SalesBand AS 'ScenarioScopeTotalSalesBand',
--	NULL AS 'ScenarioScopeTotalGrossProfitBand',--SAB.GPBand AS 'ScenarioScopeTotalGrossProfitBand',
--	NULL AS 'ScenarioScopeAvgGrossProfitPerOrderRank',--SAB.GPRank AS 'ScenarioScopeAvgGrossProfitPerOrderRank',
--	NULL AS 'ScenarioScopeTotalSalesRank',--SAB.SalesRank AS 'ScenarioScopeTotalSalesRank',
--	NULL AS 'ScenarioScopeBlendedRank',--SAB.BlendedRank AS 'ScenarioScopeBlendedRank',
	AB.BlendedRank AS FullScopeBlendedRank,
	FCNC.CoreNonCoreIndicator,
	CAIB.AccountItemBand AS AccountItemBandRank,
	CIB.ItemBand AS ItemBandRank,
	AB.SalesBand AS FullScopeAccountTotalSalesBand,
	AB.GPBand AS FullScopeAccountTotalGrossProfitBand,
	AB.GPRank AS FullScopeAccountAvgGrossProfitPerOrderRank,
	AB.SalesRank AS FullScopeAccountTotalSalesRank,
	FLPC.LastItemPrice,
	FLPC.LastItemCost,
	FLPC.PercentAccountItemSalesToAccountSales AS LastPercentAccountItemSalesToAccountSales,
	FLPC.LastItemDiscountPercent
FROM dbo.PlaybookDataPointGroupScope PDP
JOIN dbo.FactLastPriceAndCost FLPC ON FLPC.AccountKey = PDP.AccountKey AND FLPC.ItemKey= PDP.ItemKey AND FLPC.InvoiceLineGroup1Key = PDP.InvoiceLineGroup1Key
JOIN dbo.DimItem DI ON DI.ItemKey = PDP.ItemKey
JOIN dbo.PlaybookDataPointGroupFullScopeAccountBands AB ON AB.AccountKey = PDP.AccountKey AND AB.PlaybookDataPointGroupKey = @PlaybookDataPointGroupKey
--JOIN dbo.PlaybookDataPointGroupScenarioScopeAccountBands SAB ON SAB.AccountKey = PDP.AccountKey AND SAB.PlaybookDataPointGroupKey = @PlaybookDataPointGroupKey
LEFT JOIN dbo.PlaybookDataPointGroupItemBands CIB ON CIB.ItemKey = PDP.ItemKey AND CIB.PlaybookDataPointGroupKey = @PlaybookDataPointGroupKey
LEFT JOIN dbo.PlaybookDataPointGroupAccountItemBands CAIB ON CAIB.AccountKey = PDP.AccountKey AND CAIB.ItemKey = PDP.ItemKey AND CAIB.PlaybookDataPointGroupKey = @PlaybookDataPointGroupKey
LEFT JOIN dbo.PlaybookDataPointGroupFullScopeCoreNonCore FCNC ON FCNC.AccountKey = PDP.AccountKey AND FCNC.ItemGroup1KEy = DI.ItemGroup1Key AND FCNC.PlaybookDataPointGroupKey = @PlaybookDataPointGroupKey
WHERE PDP.PlaybookDataPointGroupKey = @PlaybookDataPointGroupKey
SET @RowsI = @RowsI + @@RowCount

SET @sql = 'UPDATE STATISTICS PlaybookDataPoint'
EXEC (@sql)

SELECT TOP 10 * FROM dbo.PlaybookDataPoint

EXEC LogDCPEvent 'DCP --- RBP_PopulatePlaybookDataPoint: Insert PlaybookDataPoint', 'E', @RowsI, @RowsU, @RowsD














GO
