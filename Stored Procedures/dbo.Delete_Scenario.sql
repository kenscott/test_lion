SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




CREATE    PROCEDURE [dbo].[Delete_Scenario]
@ScenarioKey INT
AS

SET NOCOUNT ON

BEGIN TRANSACTION
SET Xact_Abort ON

--If the scenario has alredy been used:
IF EXISTS (
	SELECT 1 FROM PlaybookProject A
	JOIN ATKProjectSpecificationScenario B
	ON A.ProjectSpecificationKey = B.ProjectSpecificationKey
	WHERE ScenarioKey = @ScenarioKey)
BEGIN
	PRINT 'This scenario cannot be deleted because the following projects exist which have been created using it:'
	SELECT ProjectKey, ProjectDescription
	FROM PlaybookProject A
	JOIN ATKProjectSpecificationScenario B
	ON A.ProjectSpecificationKey = B.ProjectSpecificationKey
	WHERE ScenarioKey = @ScenarioKey
END
ELSE	--Else, we can delete the Scenario
BEGIN
	SELECT LimitDataPlaybookKey, SegmentFactorPlaybookKey, PricingRulePlaybookKey, ProposalRecommendationPlaybookKey
	INTO #TempScenario
	FROM ATkScenario
	WHERE ScenarioKey = @ScenarioKey

	--Delete the limit data playbook:	
	IF NOT EXISTS (SELECT * FROM ATkScenario WHERE ScenarioKey <> @ScenarioKey AND LimitDataPlaybookKey IN (SELECT LimitDataPlaybookKey FROM #TempScenario))
	BEGIN
		DELETE FROM ATKLimitDataPlaybook WHERE LimitDataPlaybookKey IN (SELECT LimitDataPlaybookKey FROM #TempScenario)
	END



	--Delete the segment playbook:	
	IF NOT EXISTS (SELECT * FROM ATkScenario WHERE ScenarioKey <> @ScenarioKey AND SegmentFactorPlaybookKey IN (SELECT SegmentFactorPlaybookKey FROM #TempScenario))
	BEGIN
		DELETE FROM ATKSegmentCustomer WHERE SegmentFactorPlaybookKey IN (SELECT SegmentFactorPlaybookKey FROM #TempScenario)
		DELETE FROM ATKSegmentFactorPlaybook WHERE SegmentFactorPlaybookKey IN (SELECT SegmentFactorPlaybookKey FROM #TempScenario)
	END

	--Delete the pricing playbook:	
	IF NOT EXISTS (SELECT * FROM ATkScenario WHERE ScenarioKey <> @ScenarioKey AND PricingRulePlaybookKey IN (SELECT PricingRulePlaybookKey FROM #TempScenario))
	BEGIN
		SELECT PRicingRuleKey 
		INTO #TempPricingRule 
		FROM ATKPricingRule 
		WHERE PricingRulePlaybookKey IN (SELECT PricingRulePlaybookKey FROM #TempScenario)
	
		
		DELETE FROM ATKPricingRuleAttribute WHERE PricingRuleKey IN (SELECT PricingRuleKey FROM #TempPricingRule)
	
		SELECT PricingRuleBandKey 
		INTO #TempPricingRuleBand
		FROM ATKPricingRuleBand
		WHERE PricingRuleKey IN (SELECT PricingRuleKey FROM #TempPricingRule)
	
		DELETE FROM ATKPricingBandOptimalPricePercent WHERE PricingRuleBandKey IN (SELECT PricingRuleBandKey FROM #TempPricingRuleBand)
	
		DELETE FROM ATKPricingRuleBand WHERE PricingRuleKey IN (SELECT PricingRuleKey FROM #TempPricingRule)
	
		DELETE FROM ATKPricingRule WHERE PricingRulePlaybookKey IN (SELECT PricingRulePlaybookKey FROM #TempScenario)
	
		DELETE FROM ATKPricingRulePlaybook WHERE PricingRulePlaybookKey IN (SELECT PricingRulePlaybookKey FROM #TempScenario)
	END

	--Delete the proposal playbook:	
	IF NOT EXISTS (SELECT * FROM ATkScenario WHERE ScenarioKey <> @ScenarioKey AND ProposalRecommendationPlaybookKey IN (SELECT ProposalRecommendationPlaybookKey FROM #TempScenario))
	BEGIN

		DELETE FROM ATKProposalRecommendation WHERE ProposalRecommendationPlaybookKey IN (SELECT ProposalRecommendationPlaybookKey FROM #TempScenario)
		DELETE FROM ATKProposalRecommendationPlaybook WHERE ProposalRecommendationPlaybookKey IN (SELECT ProposalRecommendationPlaybookKey FROM #TempScenario)

		DELETE FROM ATKProjectSpecificationScenario WHERE ScenarioKey = @ScenarioKey
	END

	--Delete any unused project specifications
	IF NOT EXISTS (SELECT * FROM ATkScenario s INNER JOIN ATKProjectSpecificationScenario pss ON pss.ScenarioKey = s.ScenarioKey WHERE s.ScenarioKey <> @ScenarioKey AND ProjectSpecificationKey IN (SELECT ProjectSpecificationKey FROM #TempScenario))
	BEGIN
		DELETE FROM ATKProjectSpecification WHERE ProjectSpecificationKey NOT IN (SELECT ProjectSpecificationKey FROM ATKProjectSpecificationScenario)
	END

	DELETE FROM ATKScenario WHERE ScenarioKey = @ScenarioKey

END

COMMIT TRANSACTION





GO
