SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO









CREATE PROCEDURE [dbo].[P2P_VCM_PopulateProposal]
	@ProjectKey INT, 
	@ScenarioKey INT,
	@PlaybookDataPointGroupKey INT,
	@LimitDataPlaybookKey INT,
	@PricingRulePlaybookKey INT, 
	@ProposalRecommendationPlaybookKey INT
	--,
	--@BeginStep INT,
	--@EndStep INT
AS

/*


	UOM,
	NewUOM,
	UOMConversionFactor,
	
	
delete from ProposalPrice
EXEC dbo.P2P_RunProjectSpecification 8 
, 'y'

*/

/*
UPDATE pp
SET pp.AccountManagerKey = da.AccountManagerKey
FROM ProposalPrice pp
INNER JOIN DimAccount da
	ON da.AccountKey = pp.AccountKey
WHERE pp.AccountManagerKey IS NULL
*/



SET QUOTED_IDENTIFIER ON 
SET ANSI_NULLS ON 
SET ARITHABORT ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
SET CONCAT_NULL_YIELDS_NULL ON
SET NUMERIC_ROUNDABORT OFF
SET NOCOUNT ON
SET XACT_ABORT ON



EXEC LogDCPEvent 'DCP --- VCM_PopulateProposal', 'B',0,0,0,@ProjectKey



DECLARE 
	@ApproachTypeKey Key_Normal_type,
	@SuppressionTypeKey Key_Small_Type,
	
	@MaxDayThreshold INT,
	@LimitDataJoin NVARCHAR(MAX),
	@LimitDataWhere NVARCHAR(MAX),
	@LimitDataHaving NVARCHAR(MAX),
	
	@ForecastType NVARCHAR(MAX),

	@ProposalRecommendationKey INT,
	@MinCostDecreaseThresholdPercent Percent_Type,
	@CostDecreaseCapPercent Percent_Type,
	@MinPriceIncreaseThresholdPercent Percent_Type,
	@PriceIncreaseCapPercent Percent_Type,
	@WhereClause1 NVARCHAR(MAX), 
	@JoinClause1 NVARCHAR(MAX), 
	@WhereClause2 NVARCHAR(MAX),
	@JoinClause2 NVARCHAR(MAX),  
	@WhereClause3 NVARCHAR(MAX),
	@JoinClause3 NVARCHAR(MAX),
	@AcceptRejectCode_PriceProposal NVARCHAR(MAX),
	@AcceptRejectCode_CostProposal NVARCHAR(MAX),
	@ReasonCode NVARCHAR(MAX),
	@DynamicEffectiveDate NVARCHAR(MAX),
	@DynamicSuppressionDate NVARCHAR(MAX),
	@SMEMinGPP percent_type, 
	@SMEMaxGPP percent_Type,
	@ProposedPriceOverride NVARCHAR(MAX),
	@ProposedCostOverride NVARCHAR(MAX),
	@DynamicUDVarchar1 NVARCHAR(MAX),
	@DynamicUDVarchar2 NVARCHAR(MAX),
	
	@SuppressionJoinClause NVARCHAR(MAX),
	@SQLStmt NVARCHAR(MAX),
	@RowsI Quantity_Normal_type, 
	@RowsU Quantity_Normal_type, 
	@RowsD Quantity_Normal_type, 
	@AcceptedCode Key_Small_type,
	@DeletedCode Key_Small_type,
	@MinInvoiceDateDayKey INT, 
	@MaxInvoiceDateDayKey INT,
	@DeferredCode INT,
	@EffectiveDate NVARCHAR(MAX),
	@ExpirationDate NVARCHAR(MAX),	
	@SuppressionDate NVARCHAR(MAX),
	@NextFridayDate DATETIME,
	@NextSaturdayDate DATETIME,
	@InvoiceLineGroup1KeyStock Key_Normal_type,
	@InvoiceLineGroup2KeyDeviatedCost Key_Normal_type


SET @RowsI = 0
SET @RowsU = 0
SET @RowsD = 0

SET @MaxInvoiceDateDayKey = (SELECT MAX(InvoiceDateDayKey) FROM FactInvoiceLine)
SET @MinInvoiceDateDayKey = @MaxInvoiceDateDayKey - @MaxDayThreshold

SELECT @ForecastType = ForecastType 
FROM ATKProposalRecommendationPlaybook
WHERE ProposalRecommendationPlaybookKey =  @ProposalRecommendationPlaybookKey

--There must be only 1 pass!!!!!
SELECT @ProposalRecommendationKey = ProposalRecommendationKey 
FROM ATKProposalRecommendation
WHERE ProposalRecommendationPlaybookKey = @ProposalRecommendationPlaybookKey

SELECT @DeferredCode = AcceptanceCodeKey			FROM dbo.ATKAcceptanceCode WHERE AcceptanceCodeDesc = N'Deferred'
SELECT @AcceptedCode = AcceptanceCodeKey			FROM dbo.ATKAcceptanceCode WHERE AcceptanceCodeDesc = N'Accepted'
SELECT @DeletedCode = AcceptanceCodeKey				FROM dbo.ATKAcceptanceCode WHERE AcceptanceCodeDesc = N'Deleted'


/* proposal recommendation */
SELECT 
	@MinCostDecreaseThresholdPercent = ISNULL(MinCostDecreaseThresholdPercent, 0),
	@CostDecreaseCapPercent = ISNULL(CostDecreaseCapPercent, 9999999),
	@MinPriceIncreaseThresholdPercent = ISNULL(MinPriceIncreaseThresholdPercent, 0),
	@WhereClause1 = ISNULL(WhereClause1, N''),
	@JoinClause1 = ISNULL(JoinClause1, N''),
	@WhereClause2 = ISNULL(WhereClause2, N''),
	@JoinClause2= ISNULL(JoinClause2, N''),
	@WhereClause3 = ISNULL(WhereClause3, N''),
	@JoinClause3 = ISNULL(JoinClause3, N''),
	@AcceptRejectCode_PriceProposal = ISNULL(AcceptRejectCode_PriceProposal, N'1'),  -- 2 = Accepted; 1 = Deferred
	@AcceptRejectCode_CostProposal  = ISNULL(AcceptRejectCode_CostProposal, N'1'),  -- 2 = Accepted; 1 = Deferred
	@SMEMinGPP = ISNULL(SMEMinGPP, 0),
	@SMEMaxGPP = ISNULL(SMEMaxGPP, 1),
	@ProposedPriceOverride = ISNULL(ProposedPriceOverride, N''),	
	@ProposedCostOverride = ISNULL(ProposedCostOverride, N''),	
	@ReasonCode = ISNULL(DynamicReasonCode, N'NULL'),
	@EffectiveDate = ISNULL(DynamicEffectiveDate, N''),
	@SuppressionDate = ISNULL(DynamicSuppressionDate, GETDATE()),
	@PriceIncreaseCapPercent = ISNULL(PriceIncreaseCapPercent, 9999999),
	@DynamicUDVarchar1 = ISNULL(DynamicUDVarchar1, N''),
	@DynamicUDVarchar2 = ISNULL(DynamicUDVarchar2, N'')
FROM ATKProposalRecommendation
WHERE ProposalRecommendationKey = @ProposalRecommendationKey


/* limit data */
SELECT @MaxDayThreshold = ISNULL(MaxDayThreshold, 0),
		@JoinClause1 = ISNULL(JoinClause, N''),
		@WhereClause1 = ISNULL(WhereClause, N''),
		@LimitDataHaving = ISNULL(HavingClause, N'')
FROM ATKLimitDataPlaybook 
WHERE LimitDataPlaybookKey = @LimitDataPlaybookKey

IF @WhereClause1 <> '' AND @WhereClause1 NOT LIKE 'AND %'
	SET @WhereClause1 = N'AND ' + @WhereClause1

IF @LimitDataHaving <> '' AND @LimitDataHaving NOT LIKE 'HAVING %'
	SET @LimitDataHaving = N'HAVING ' + @LimitDataHaving

IF @WhereClause1 <> '' AND @WhereClause1 NOT LIKE 'AND %'
	SET @WhereClause1 = N'AND ' + @WhereClause1
IF @WhereClause2 <> '' AND @WhereClause2 NOT LIKE 'AND %'
	SET @WhereClause2 = N'AND ' + @WhereClause2
IF @WhereClause3 <> '' AND @WhereClause3 NOT LIKE 'AND %'
	SET @WhereClause3 = N'AND ' + @WhereClause3


SELECT 
	@SuppressionTypeKey = SuppressionTypeKey,
	@ApproachTypeKey = ApproachTypeKey
FROM ATKScenario 
WHERE ScenarioKey = @ScenarioKey

SELECT @InvoiceLineGroup1KeyStock = (SELECT InvoiceLineGroup1Key FROM dbo.DimInvoiceLineGroup1 WHERE InvoiceLineGroup1UDVarchar1 = 'S')
SELECT @InvoiceLineGroup2KeyDeviatedCost = (SELECT InvoiceLineGroup2Key FROM dbo.DimInvoiceLineGroup2 WHERE InvoiceLineGroup2UDVarchar1 = 'Y')

--RAISERROR ('@InvoiceLineGroup1KeyStock: %d', 0, 1, @InvoiceLineGroup1KeyStock) WITH NOWAIT
--RAISERROR ('@@InvoiceLineGroup2KeyDeviatedCost: %d', 0, 1, @InvoiceLineGroup2KeyDeviatedCost) WITH NOWAIT

--SET @ExpirationDate = 'CASE
--			WHEN vcm.CostEffectiveDate < DATEADD(DAY, 7, GETDATE()) THEN DATEADD(DAY, 7, GETDATE())
--			ELSE DATEADD(DAY, -7, vcm.CostEffectiveDate)
--		END'

--IF @EffectiveDate = ''	-- then not overridden
--BEGIN
--	SELECT @EffectiveDate = '
--		CASE
--			WHEN RebateId IS NULL THEN vcm.CostEffectiveDate
--			ELSE 
--				CASE
--					WHEN 
--						ContractEndDate < CONVERT(VARCHAR(8), RebateEndDate, 112) 
--						AND ISNULL(ContractEndDate, '''') NOT IN (''None'', ''Unknown'', ''99999999'', ''00000000'', '''') 
--						AND ISDATE(ContractEndDate) = 1 
--						THEN CAST(ContractEndDate AS DATETIME)
--					ELSE 
--						RebateEndDate
--				END
--		END	'
--END

IF @EffectiveDate = N''	-- then not overridden
BEGIN
	SELECT @EffectiveDate = N'
		CASE
			WHEN FactLastPriceAndCost.RebateId IS NOT NULL AND RebateCode = ''F'' THEN RebateEndDate
			ELSE FutureCostChangeDate
		END	'
	SELECT @EffectiveDate = N' GETDATE() '
END


-- set expiration date to the Friday before the effective date
SELECT @ExpirationDate = N'
	CASE DATEPART(dw, EffectiveDate)
		WHEN 1 THEN DATEADD(dd, -2, EffectiveDate)  -- day of week is 1 which means a Sunday; go back two days to Friday
		WHEN 2 THEN DATEADD(dd, -3, EffectiveDate)
		WHEN 3 THEN DATEADD(dd, -4, EffectiveDate)
		WHEN 4 THEN DATEADD(dd, -5, EffectiveDate)
		WHEN 5 THEN DATEADD(dd, -6, EffectiveDate)
		WHEN 6 THEN DATEADD(dd, -7, EffectiveDate) -- a Friday, go back a week
		WHEN 7 THEN DATEADD(dd, -1, EffectiveDate)
		ELSE NULL
	END 
	'


SELECT @NextFridayDate = 
	CASE DATEPART(dw, GETDATE())
		WHEN 1 THEN DATEADD(dd, 5, GETDATE())
		WHEN 2 THEN DATEADD(dd, 4, GETDATE())
		WHEN 3 THEN DATEADD(dd, 3, GETDATE())
		WHEN 4 THEN DATEADD(dd, 2, GETDATE())
		WHEN 5 THEN DATEADD(dd, 1, GETDATE())
		WHEN 6 THEN DATEADD(dd, 7, GETDATE()) -- a friday, go back a week
		WHEN 7 THEN DATEADD(dd, 6, GETDATE())
		ELSE NULL
	END

SELECT @NextSaturdayDate = DATEADD(dd, 1, @NextFridayDate)


IF (SELECT SuppressionTypeCode 
	FROM ATKSuppressionType 
	WHERE SuppressionTypeKey = @SuppressionTypeKey) = N'Proposal'
BEGIN
	SET @SuppressionJoinClause = 
		N'LEFT JOIN (
			SELECT DISTINCT 
				AccountKey, 
				ItemKey, 
				InvoiceLineGroup1Key, 
				CostEffectiveDate, 
				FutureItemCost
			FROM dbo.ProposalPrice pp
			INNER JOIN dbo.PlaybookProject p
				ON p.ProjectKey = pp.ProjectKey
			WHERE 
				ProductionProjectIndicator = ''Y''
				AND ApproachTypeKey = '+CAST(@ApproachTypeKey AS NVARCHAR(25))+'
				--AND AcceptanceCodeKey <> '+CAST(@DeletedCode AS NVARCHAR(25))+'
			) SuppressionList
		ON SuppressionList.AccountKey = Data1.AccountKey
		AND SuppressionList.ItemKey = Data1.ItemKey
		AND SuppressionList.InvoiceLineGroup1Key = Data1.InvoiceLineGroup1Key
		--AND SuppressionList.CostEffectiveDate = Data1.CostEffectiveDate
		--AND SuppressionList.FutureItemCost = Data1.FutureItemCostInListUoM'	
	SET @WhereClause2 = @WhereClause2 + N'
		AND SuppressionList.AccountKey IS NULL
		'
END
ELSE
	SET @SuppressionJoinClause = N''



DELETE FROM ProposalPrice WHERE PlaybookDataPointGroupKey = @PlaybookDataPointGroupKey
SET @RowsD = @RowsD + @@RowCount



/* AUTO COMMIT/ACCEPT */
-- naturally expiring...
EXEC LogDCPEvent 'DCP --- VCM_PopulateProposal: System Auto Commit', 'B',0,0,0,@ProjectKey

UPDATE pp 
	SET 
		AcceptanceCodeKey = @AcceptedCode,
		ModificationDate = GETDATE(),
		ModificationUser = N'System Auto Commit',
		ModificationUserAddress = CAST(@ProjectKey AS VARCHAR(50)),
		ProposalPriceGroupKey = NULL,
		ProposalPriceBuyGroupKey = NULL,

		MaxStatus =
			CASE 
				WHEN pp.ProposalPriceGroupKey IS NOT NULL AND LEFT(ContractNo, 2) IN ('BG', 'HQ') THEN 'BG'
				WHEN LEFT(ContractNo, 2) = 'BG' THEN 'BG'
				ELSE 'P'
			END,								
		CommittedDate = GETDATE(),
		MaxStatusChangeDate = GETDATE(),
		MaxPrice = pp.NewPrice,
		MaxGPP = pp.NewGPP, 
		MaxDiscount = pp.NewDiscount,
		MaxPricingRule = 
			CASE
				WHEN UDVarchar1 = 'M' THEN '07'
				WHEN UDVarchar1 = 'P' THEN '01'
				WHEN UDVarchar1 = 'D' THEN '02'
				ELSE ''
			END,
		MaxEffectiveDate = GETDATE(),	-- pp.EffectiveDate,	https://enterbridge.atlassian.net/browse/BPB-3948
		MaxExpirationDate = CAST('12/31/2049' AS DATETIME)		-- https://enterbridge.atlassian.net/browse/BPB-2742
FROM dbo.ProposalPrice pp
INNER JOIN dbo.PlaybookProject p
	ON p.ProjectKey = pp.ProjectKey
WHERE 
--p.ApproachTypeKey = @ApproachTypeKey
--AND 
pp.AcceptanceCodeKey <> @AcceptedCode
AND pp.AcceptanceCodeKey <> @DeletedCode
AND pp.ExpirationDate <= GETDATE()
AND ProductionProjectIndicator = 'Y'

EXEC LogDCPEvent 'DCP --- VCM_PopulateProposal: System Auto Commit', 'E',0,@@ROWCOUNT,0,@ProjectKey



--/*** DELETE empty proposal price groups ***/
--EXEC LogDCPEvent 'DCP --- VCM_PopulateProposal: Mark as deleted empty proposal price groups - Pass 1', 'B',0,0,0,@ProjectKey
--UPDATE g
--SET 
--	g.AcceptanceCodeKey = 4
--FROM dbo.ProposalPriceGroup g
--LEFT JOIN ProposalPrice pp
--	ON pp.ProposalPriceGroupKey = g.ProposalPriceGroupKey
--WHERE 
--	pp.ProposalPriceGroupKey IS NULL
--	AND g.AcceptanceCodeKey = 1
--EXEC LogDCPEvent 'DCP --- VCM_PopulateProposal: Mark as deleted empty proposal price groups - Pass 1', 'E',0,@@ROWCOUNT,0,@ProjectKey



/*
Identify potential proposals

For matching proposals WITH A DIFFERNET COST OR EFFECTIVE DATE, then set them to committed
insert potentials where not matching existing proposals (on cost and date)
*/

EXEC LogDCPEvent 'DCP --- VCM_PopulateProposal: Insert PlaybookVCMProposal', 'B', 0, 0, 0 , @ProjectKey

SET @SQLStmt = N'
TRUNCATE TABLE [dbo].[PlaybookVCMProposal]
; WITH Data1 AS (
	SELECT
		FactLastPriceAndCost.AccountKey,
		FactLastPriceAndCost.ItemKey,
		FactLastPriceAndCost.InvoiceLineGroup1Key,
		DimAccount.AccountManagerKey,
		FILSUM.Cnt AS Total12MonthFrequency,
		FILSUM.TotalQuantity AS Total12MonthQuantity,
		FILSUM.TotalPrice AS Total12MonthPrice,
		FILSUM.TotalCost AS Total12MonthCost,
		FactLastPriceAndCost.LastItemPrice,		
		FactLastPriceAndCost.LastItemCost,
		(FactLastPriceAndCost.LastItemPrice - FactLastPriceAndCost.LastItemCost) / FactLastPriceAndCost.LastItemPrice AS LastGPP,
		FactLastPriceAndCost.LastSaleDayKey,
		FactLastPriceAndCost.LastInvoiceLineGroup2Key,
		-- est. working price and cost and gpp
		CurrentItemPrice,
		CurrentItemCost,
		ISNULL((CurrentItemPrice - CurrentItemCost) / NULLIF(CurrentItemPrice, 0.0), 0.0) AS CurrentGPP,
		ISNULL(CurrentDiscount, 0.) AS CurrentDiscount,
		FactCurrentPriceAndCost.UDVarchar1 AS CurrentPriceSource,
		CurrentPriceMethod,
		RecentPriceDate,
		RecentItemCost,
		RecentDiscount,
		--ListPrice AS ListPrice,
		--ListPriceInListUoM AS ListPriceInListUoM,
		--(ListPrice * NewBaseCost) / StandardCost AS FutureEstimatedListPrice,  
		--ListPricingUoMConvFactor AS ListPricingUoMConvFactor,
		--ListPricingUoM AS ListPricingUoM,
		--UserCost AS CurrentWarehouseCommissionCost,
		--NewCommissionCost AS FutureItemCost,
		--CAST(NewCommissionCost * ISNULL(ListPricingUoMConvFactor, 1.) AS DECIMAL(19,8)) AS FutureItemCostInListUoM,		
		--FutureCostChangeDate AS CostEffectiveDate,
		--DropShipIndicator AS DropShipIndicator,	
		--StockStatus,
		--FactRebate.RebateId,
		--FactRebate.EndDate AS RebateEndDate,
		--FactRebate.RebateCode,
		--FactRebate.RebatedCommissionCost,
		--NewCommissionCost * FactRebate.RebatedCommissionCost / UserCost AS FutureRebatedCommissionCost,
		FactLastPriceAndCost.RPBPlaybookPricingGroupKey,
		ISNULL(TheFloor.BandSequenceNumber, 1) AS BandSequenceNumber,
		ISNULL(o.FloorGPP, TheFloor.PDPGPP) AS FloorGPP,
		ISNULL(o.TargetGPP, TheTarget.PDPGPP) AS TargetGPP,
		ISNULL(o.StretchGPP, TheStretch.PDPGPP) AS StretchGPP,
		' + CAST(@DynamicUDVarchar1 AS NVARCHAR(MAX)) + N' AS ShadowGPP,
		CASE
			WHEN ItemUDVarchar10 = ''1'' THEN FactLastPriceAndCost.UDVarchar6
			ELSE FactLastPriceAndCost.UDVarchar5
		END AS StrategicIndicator,
		junk.InvoiceLineJunkUDVarchar2 AS ContractNo,
		junk.InvoiceLineJunkUDVarchar6 AS RuleNo,
		junk.InvoiceLineJunkUDVarchar5 AS ContractEndDate,
		FactLastPriceAndCost.UDVarchar7 AS CDIIndicator,
		'+CAST(@EffectiveDate AS NVARCHAR(MAX))+N' AS EffectiveDate
	FROM dbo.FactLastPriceAndCost
	INNER JOIN dbo.DimAccount 
		ON DimAccount.AccountKey = FactLastPriceAndCost.AccountKey
	INNER JOIN dbo.DimAccountGroup1
		ON DimAccountGroup1.AccountGroup1Key = DimAccount.AccountGroup1Key
	INNER JOIN dbo.DimItem ON
		DimItem.ItemKey = FactLastPriceAndCost.ItemKey
	INNER JOIN dbo.DimInvoiceLineJunk junk
		ON junk.InvoiceLineJunkKey = LastInvoiceLineJunkKey
	INNER JOIN (
				SELECT 
					AccountKey, 
					ItemKey, 
					InvoiceLineGroup1Key,
					SUM(TotalQuantity) TotalQuantity,
					COUNT(*) Cnt,
					SUM(TotalActualCost) TotalCost,
					SUM(TotalActualPrice) TotalPrice
				FROM dbo.FactInvoiceLine fil 
				WHERE Last12MonthsIndicator = N''Y''
					AND InvoiceLineGroup1Key = ' + CAST(@InvoiceLineGroup1KeyStock AS NVARCHAR(MAX)) + N'
				GROUP BY 
					AccountKey, 
					ItemKey, 
					InvoiceLineGroup1Key
			) FILSUM
			ON FILSUM.AccountKey = FactLastPriceAndCost.AccountKey 
			AND FILSUM.ItemKey = FactLastPriceAndCost.ItemKey 
			AND FILSUM.InvoiceLineGroup1Key = FactLastPriceAndCost.InvoiceLineGroup1Key	
	INNER JOIN dbo.FactCurrentPriceAndCost
			ON FactCurrentPriceAndCost.AccountKey = FactLastPriceAndCost.AccountKey 
			AND FactCurrentPriceAndCost.ItemKey = FactLastPriceAndCost.ItemKey 
			AND FactCurrentPriceAndCost.InvoiceLineGroup1Key = FactLastPriceAndCost.InvoiceLineGroup1Key	
	INNER JOIN dbo.ATKAccountGroup1ItemGroup1 ATKAccountGroup1ItemGroup1
		ON ATKAccountGroup1ItemGroup1.AccountGroup1Key = DimAccount.AccountGroup1Key
		AND ATKAccountGroup1ItemGroup1.ItemGroup1Key = DimItem.ItemGroup1Key
	LEFT JOIN dbo.PlaybookPricingGroupPriceBand TheFloor
		ON TheFloor.PlaybookPricingGroupKey = FactLastPriceAndCost.RPBPlaybookPricingGroupKey
		AND FILSUM.TotalQuantity BETWEEN TheFloor.LowerBandQuantity AND TheFloor.UpperBandQuantity 
		AND TheFloor.MemberRank = 
			CASE
				WHEN  ROUND ( TheFloor.RulingMemberCount * FloorPct, 0) BETWEEN 1 AND TheFloor.RulingMemberCount THEN  ROUND ( TheFloor.RulingMemberCount * FloorPct, 0)
				ELSE 1
			END
	LEFT JOIN dbo.PlaybookPricingGroupPriceBand TheTarget
		ON TheTarget.PlaybookPricingGroupKey = FactLastPriceAndCost.RPBPlaybookPricingGroupKey
		AND FILSUM.TotalQuantity BETWEEN TheTarget.LowerBandQuantity AND TheTarget.UpperBandQuantity 
		AND TheTarget.MemberRank = 
			CASE
				WHEN  ROUND ( TheTarget.RulingMemberCount * TargetPct, 0) BETWEEN 1 AND TheTarget.RulingMemberCount THEN  ROUND ( TheTarget.RulingMemberCount * TargetPct, 0)
				ELSE 1
			END
	LEFT JOIN dbo.PlaybookPricingGroupPriceBand TheStretch
		ON TheStretch.PlaybookPricingGroupKey = FactLastPriceAndCost.RPBPlaybookPricingGroupKey
		AND FILSUM.TotalQuantity BETWEEN TheStretch.LowerBandQuantity AND TheStretch.UpperBandQuantity 
		AND TheStretch.MemberRank = 
			CASE
				WHEN  ROUND ( TheStretch.RulingMemberCount * StretchPct, 0) BETWEEN 1 AND TheStretch.RulingMemberCount THEN  ROUND ( TheStretch.RulingMemberCount * StretchPct, 0)
				ELSE 1
			END
	LEFT JOIN dbo.ManualPriceBandOverride o
		ON DimItem.ItemVendorKey = o.VendorKey
		AND DimItem.ItemGroup1Key = o.ItemGroup1Key
		AND FactLastPriceAndCost.InvoiceLineGroup1Key = o.InvoiceLineGroup1Key
	'+ CAST(@JoinClause1 AS NVARCHAR(MAX))+N'
	WHERE 1=1
		--AND NewComFirstOccurrenceIndicator = N''Y''
		AND FactLastPriceAndCost.InvoiceLineGroup1Key = ' + CAST(@InvoiceLineGroup1KeyStock AS NVARCHAR(MAX)) + N'
		--AND FactRebate.RebateId IS NULL -- with 6.0 rebated items are handled outside of VCM (and in Rebate Change {RC} proposals)
		AND (o.FloorGPP IS NOT NULL OR TheFloor.PDPGPP IS NOT NULL)
		AND LastItemPrice > 0.0
		AND LastItemCost > 0.0
		AND CurrentItemCost <> 0.0
		--AND UserCost <> 0.0
		AND (LastItemPrice - LastItemCost) / LastItemPrice <> 0.0
		AND CurrentItemPrice IS NOT NULL
		AND Total12MonthQuantity > 0.0
		--AND NewCommissionCost IS NOT NULL
		--AND NewCommissionCost > 0.
		--AND (NewCommissionCost - LastItemCost) / LastItemCost BETWEEN -0.50 AND 1.00  -- guardrails per http://enterbridgedc:8080/browse/BPB-994 
		--AND (NewCommissionCost - CurrentItemCost) / CurrentItemCost BETWEEN -0.50 AND 1.00  -- guardrails
		'+CAST(@WhereClause1 AS NVARCHAR(MAX)) + N'
), Data2 AS (
	SELECT
		Data1.AccountKey,
		Data1.ItemKey,
		Data1.InvoiceLineGroup1Key,
		AccountManagerKey,
		Total12MonthFrequency,
		Total12MonthQuantity,
		Total12MonthPrice,
		Total12MonthCost,
		LastItemPrice,
		LastItemCost,
		LastGPP,
		LastSaleDayKey,
		LastInvoiceLineGroup2Key,
		CurrentItemPrice,
		CurrentItemCost,
		CurrentGPP,
		CurrentDiscount,
		CurrentPriceSource,
		CurrentPriceMethod,
		RecentPriceDate,
		RecentItemCost,
		RecentDiscount,
		--Data1.FutureItemCost * RebatedCommissionCost / CurrentWarehouseCommissionCost AS FutureRebatedCommissionCost,
		RPBPlaybookPricingGroupKey,
		BandSequenceNumber,
		FloorGPP,
		TargetGPP,
		StretchGPP,
		--dbo.fn_GetGPPWithShadowIncrease (StrategicIndicator, CurrentGPP, ShadowGPP, FloorGPP, TargetGPP, StretchGPP) AS ShadowGPPIncrease,
		StrategicIndicator,
		ContractNo,
		RuleNo,
		ContractEndDate,
		CDIIndicator,
		EffectiveDate,
		ForecastedMonthlyQty,
		' + CAST(@ExpirationDate  AS NVARCHAR(MAX))+ N' AS ExpirationDate,
		--Data1.FutureItemCost AS RelevantCost
		LastItemCost AS RelevantCost	
	FROM Data1
	JOIN FactSalesForecast 
		ON FactSalesForecast.AccountKey = Data1.AccountKey 
		AND FactSalesForecast.ItemKey = Data1.ItemKey 
		AND FactSalesForecast.InvoiceLineGroup1Key = Data1.InvoiceLineGroup1Key 
		AND ForecastType = '''+CAST(@ForecastType AS NVARCHAR(MAX))+N'''
	'+CAST(@JoinClause2 AS NVARCHAR(MAX))+N'
	' + CAST(@SuppressionJoinClause AS NVARCHAR(MAX)) + N'		
	WHERE 1=1
		'+CAST(@WhereClause2 AS NVARCHAR(MAX)) + N'	
), Data3 AS (
	SELECT
		AccountKey,
		ItemKey,
		InvoiceLineGroup1Key,
		AccountManagerKey,
		Total12MonthFrequency,
		Total12MonthQuantity,
		Total12MonthPrice,
		Total12MonthCost,
		LastItemPrice,
		LastItemCost,
		LastGPP,
		LastSaleDayKey,
		LastInvoiceLineGroup2Key,
		CurrentItemPrice,
		CurrentItemCost,
		CurrentGPP,
		CurrentDiscount,
		CurrentPriceSource,
		CurrentPriceMethod,
		RecentPriceDate,
		RecentItemCost,
		RecentDiscount,
		RPBPlaybookPricingGroupKey,
		BandSequenceNumber,
		FloorGPP,
		TargetGPP,
		StretchGPP,
		--ShadowGPPIncrease,
		StrategicIndicator,
		ContractNo,
		RuleNo,
		ContractEndDate,
		CDIIndicator,
		CASE
			WHEN ExpirationDate < GETDATE() THEN CAST(''' + CAST(@NextSaturdayDate AS NVARCHAR(MAX)) + N''' AS DATETIME)
			ELSE EffectiveDate
		END AS EffectiveDate,
		CASE
			WHEN ExpirationDate < GETDATE() THEN CAST(''' + CAST(@NextFridayDate AS NVARCHAR(MAX)) + N''' AS DATETIME)
			ELSE ExpirationDate
		END AS ExpirationDate,		
		ForecastedMonthlyQty,	
		RelevantCost,
		RelevantCost / (1. - TargetGPP) AS NewPrice		
	FROM Data2
	WHERE 1=1
		--AND NOT (ABS(1. - (CurrentWarehouseCommissionCost/FutureItemCost)) <= .0049 AND CONVERT(date, GETDATE()) < CONVERT(date, CostEffectiveDate))
		--AND NOT (ABS(1. - (LastItemCost/FutureItemCost)) <= .0049 AND CONVERT(date, GETDATE()) >= CONVERT(date, CostEffectiveDate))
)
INSERT dbo.PlaybookVCMProposal
(
	AccountKey,
	ItemKey,
	InvoiceLineGroup1Key,
	AccountManagerKey,
	Total12MonthFrequency,
	Total12MonthQuantity,
	Total12MonthPrice,
	Total12MonthCost,
	LastItemPrice,
	LastItemCost,
	LastGPP,
	LastSaleDayKey,
	LastInvoiceLineGroup2Key,
	CurrentItemPrice,
	CurrentItemCost,
	CurrentGPP,
	CurrentDiscount,
	CurrentPriceSource,
	CurrentPriceMethod,
	RecentPriceDate,
	RecentItemCost,
	RecentDiscount,
	--ListPrice,
	--ListPriceInListUoM,
	--FutureEstimatedListPrice,
	--ListPricingUoMConvFactor,
	--ListPricingUoM,
	--CurrentWarehouseCommissionCost,
	--FutureItemCost,
	--FutureItemCostInListUoM,
	--CostEffectiveDate,
	--DropShipIndicator,
	--StockStatus,
	--RebateId,
	--RebateEndDate,
	----RebateCode,
	--RebatedCommissionCost,
	--FutureRebatedCommissionCost,
	RPBPlaybookPricingGroupKey,
	BandSequenceNumber,
	FloorGPP,
	TargetGPP,
	StretchGPP,
	--ShadowGPPIncrease,
	StrategicIndicator,
	ContractNo,
	RuleNo,
	ContractEndDate,
	CDIIndicator,
	EffectiveDate,
	ExpirationDate,
	ForecastedMonthlyQty,
	RelevantCost,
	NewPrice
)
SELECT
	AccountKey,
	ItemKey,
	InvoiceLineGroup1Key,
	AccountManagerKey,
	Total12MonthFrequency,
	Total12MonthQuantity,
	Total12MonthPrice,
	Total12MonthCost,
	LastItemPrice,
	LastItemCost,
	LastGPP,
	LastSaleDayKey,
	LastInvoiceLineGroup2Key,
	CurrentItemPrice,
	CurrentItemCost,
	CurrentGPP,
	CurrentDiscount,
	CurrentPriceSource,
	CurrentPriceMethod,
	RecentPriceDate,
	RecentItemCost,
	RecentDiscount,
	--ListPrice,
	--ListPriceInListUoM,
	--FutureEstimatedListPrice,
	--ListPricingUoMConvFactor,
	--ListPricingUoM,
	--CurrentWarehouseCommissionCost,
	--FutureItemCost,
	--FutureItemCostInListUoM,
	--CostEffectiveDate,
	--DropShipIndicator,
	--StockStatus,
	--RebateId,
	--RebateEndDate,
	----RebateCode,
	--RebatedCommissionCost,
	--FutureRebatedCommissionCost,
	RPBPlaybookPricingGroupKey,
	BandSequenceNumber,
	FloorGPP,
	TargetGPP,
	StretchGPP,
	--ShadowGPPIncrease,
	StrategicIndicator,
	ContractNo,
	RuleNo,
	ContractEndDate,
	CDIIndicator,
	EffectiveDate,
	ExpirationDate,
	ForecastedMonthlyQty,
	RelevantCost,
	NewPrice
FROM Data3
WHERE 
	NewPrice >= 0.0
	AND EffectiveDate <= DATEADD(MONTH, 24, GETDATE())
'

SELECT @RowsI = COUNT(*) FROM PlaybookVCMProposal
EXEC LogDCPEvent 'DCP --- VCM_PopulateProposal: Insert PlaybookVCMProposal', 'E', @RowsI, 0, 0 , @ProjectKey



RAISERROR ('Insert PlaybookVCMProposal sql statement... ', 0, 1) WITH NOWAIT 
EXEC dbo.PrintNVarCharMax @SQLStmt
RAISERROR ('---', 0, 1) WITH NOWAIT 
EXEC (@SQLStmt)



-- collision... if we see a new cost change then mark as "delete" any existing
SET @SQLStmt = N'
	DECLARE @RowsU Quantity_Normal_type
	SET @RowsU = 0

	EXEC LogDCPEvent ''DCP --- VCM_PopulateProposal: Delete colliding vcm proposals in ProposalPrice'', ''B'', 0, 0, 0 ,'+CAST(@ProjectKey AS VARCHAR(500))+'

	UPDATE pp 
		SET 
			pp.AcceptanceCodeKey = '+CAST(@DeletedCode AS VARCHAR(25))+N',
			pp.ProposalPriceGroupKey = NULL,
			pp.ProposalPriceBuyGroupKey = NULL,
			pp.MaxStatus = ''D'',
			pp.ModificationDate = GETDATE(),
			pp.ModificationUser = ''System Auto Delete - new proposal to be generated'',
			pp.ModificationUserAddress = '+CAST(@ProjectKey AS VARCHAR(25))+N'
	FROM dbo.ProposalPrice pp
	JOIN dbo.PlaybookVCMProposal new
		ON pp.AccountKey = new.AccountKey 
		AND pp.ItemKey = new.ItemKey 
		AND pp.InvoiceLineGroup1Key = new.InvoiceLineGroup1Key
	INNER JOIN dbo.PlaybookProject p
		ON p.ProjectKey = pp.ProjectKey		
	WHERE 1=1
		--AND	(new.FutureItemCostInListUoM <> pp.FutureItemCost OR new.CostEffectiveDate <> pp.CostEffectiveDate)  -- something changed, commit
		AND pp.AcceptanceCodeKey <> '+CAST(@AcceptedCode AS VARCHAR(25))+N'
		AND pp.AcceptanceCodeKey <> '+CAST(@DeletedCode AS VARCHAR(25))+N'
		AND ProductionProjectIndicator = ''Y''	
	SET @RowsU = @RowsU + @@ROWCOUNT

	EXEC LogDCPEvent ''DCP --- VCM_PopulateProposal: Delete colliding vcm proposals in ProposalPrice'', ''E'', 0, @RowsU, 0 ,'+CAST(@ProjectKey AS VARCHAR(500))+N'
'
-- allow for pricing projects to also factor in
--AND p.ApproachTypeKey = '+CAST(@ApproachTypeKey AS VARCHAR(25))+'

RAISERROR ('update/autocommiting colliding vcm proposal sql statement... ', 0, 1) WITH NOWAIT 
SELECT @SQLStmt
RAISERROR ('---', 0, 1) WITH NOWAIT 
EXEC (@SQLStmt)
		


SET @SQLStmt = N'
DECLARE @RowsI Quantity_Normal_type
SET @RowsI = 0

EXEC LogDCPEvent ''DCP --- VCM_PopulateProposal: Insert ProposalPrice'', ''B'', 0, 0, 0 ,'+CAST(@ProjectKey AS VARCHAR(500))+N'

			
INSERT INTO ProposalPrice
(
	ProjectKey,
	PlaybookDataPointGroupKey,
	AccountKey,
	ItemKey,
	InvoiceLineGroup1Key,
	AccountManagerKey,
	AcceptanceCodeKey,
	UserAcceptanceCodeKey,
	DefaultAcceptanceCodeKey,
	UOM,
	NewUOM,
	UOMConversionFactor,
	ProposedPrice,
	NewPrice, 
	TotalActual12MonthCost,
	TotalActual12MonthPrice,
	TotalActual12MonthQuantity,
	TotalActual12MonthFrequency,
	LastItemCost,
	LastItemPrice,
	CurrentItemAISPrice,
	CurrentItemAISCost,
	CurrentItemStandardCost,
	--FutureItemCost,		
	Forecasted12MonthQuantity,
	AcceptanceStatusDate,
	LastSaleDayKey,
	LastInvoiceLineGroup2Key,
	EffectiveDate,
	SuppressionDate,
	ReasonCodeKey,
	UDDecimal1,
	UDDecimal2,
	UDDecimal3,
	UDVarchar1,
	UDVarchar2,
	UDVarchar3,
	UDVarchar4,
	ExpirationDate,
	--CostEffectiveDate,
	NewGPP,
	NewDiscount,
	--FutureEstimatedListPrice,
	--ListPriceInListUoM,
	--ListPricingUoMConvFactor,
	--ListPricingUoM,
	RPBPlaybookPricingGroupKey,
	BandSequenceNumber,
	ContractNo,
	RuleNo,
	ContractEndDate,
	DropShipIndicator,
	StockStatus,
	RebateId,
	RebateEndDate,
	RelevantCost,
	RecentItemCost,
	RecentItemPrice,
	RecentDiscount,
	RecentPriceDate,
	FloorGPP,
	TargetGPP
)
SELECT
	'+CAST(@ProjectKey AS VARCHAR(500))+N' AS ProjectKey,
	'+CAST(@PlaybookDataPointGroupKey AS VARCHAR(500))+N' AS PlaybookDataPointGroupKey,
	AccountKey,
	ItemKey,
	InvoiceLineGroup1Key,
	AccountManagerKey,
	'+@AcceptRejectCode_PriceProposal+N' AS AcceptanceCodeKey,
	'+CAST(@DeferredCode AS VARCHAR(25))+N' AS UserAcceptanceCodeKey,
	'+@AcceptRejectCode_PriceProposal+N' AS DefaultAcceptanceCodeKey,
	ListPricingUoM AS UOM,
	ListPricingUoM AS NewUOM,
	1 AS UOMConversionFactor,
	NewPrice * ISNULL(ListPricingUoMConvFactor, 1) AS ProposedPrice,
	NewPrice * ISNULL(ListPricingUoMConvFactor, 1) AS NewPrice,
	Total12MonthCost,
	Total12MonthPrice,
	Total12MonthQuantity / ISNULL(ListPricingUoMConvFactor, 1),
	Total12MonthFrequency,
	LastItemCost * ISNULL(ListPricingUoMConvFactor, 1),
	LastItemPrice * ISNULL(ListPricingUoMConvFactor, 1),
	CurrentItemPrice * ISNULL(ListPricingUoMConvFactor, 1) AS CurrentItemAISPrice,
	CurrentItemCost * ISNULL(ListPricingUoMConvFactor, 1) AS CurrentItemAISCost,
	CurrentWarehouseCommissionCost * ISNULL(ListPricingUoMConvFactor, 1) AS CurrentItemStandardCost,
	--FutureItemCostInListUoM,
	
	(ForecastedMonthlyQty*12) / ISNULL(ListPricingUoMConvFactor, 1) AS Forecasted12MonthQuantity,
	
	GETDATE() AS AcceptanceStatusDate,
	LastSaleDayKey,
	LastInvoiceLineGroup2Key,
	EffectiveDate,
	'''+@SuppressionDate+N''' AS SuppressionDate,		
	'+@ReasonCode+N' AS ReasonCodeKey,
	RebatedCommissionCost * ISNULL(ListPricingUoMConvFactor, 1) AS UDDecimal1,
	FutureRebatedCommissionCost * ISNULL(ListPricingUoMConvFactor, 1) AS UDDecimal2,
	CurrentDiscount AS UDDecimal3,
	CurrentPriceMethod AS UDVarchar1,
	StrategicIndicator AS UDVarchar2, -- UDVarchar2
	CDIIndicator AS UDVarchar3,
	CurrentPriceSource AS UDVarchar4,
	ExpirationDate,
	--CostEffectiveDate,
	ISNULL((NewPrice - RelevantCost) / NULLIF(NewPrice, 0.0), 0.0) AS NewGPP,
	(FutureEstimatedListPrice - NewPrice) / NULLIF(FutureEstimatedListPrice, 0.0) AS NewDiscount,
	--CAST(FutureEstimatedListPrice * ISNULL(ListPricingUoMConvFactor, 1) AS DEC(19,8)) AS FutureEstimatedListPrice,
	--ListPriceInListUoM,

	--ListPricingUoMConvFactor,
	--ListPricingUoM,
	
	RPBPlaybookPricingGroupKey,
	BandSequenceNumber,
	ContractNo,
	RuleNo,
	ContractEndDate,
	DropShipIndicator,
	StockStatus,
	RebateId,
	RebateEndDate,
	RelevantCost * ISNULL(ListPricingUoMConvFactor, 1) AS RelevantCost,
	RecentItemCost * ISNULL(ListPricingUoMConvFactor, 1),
	CurrentItemPrice * ISNULL(ListPricingUoMConvFactor, 1) AS RecentItemPrice,
	RecentDiscount,
	RecentPriceDate,
	FloorGPP,
	TargetGPP	
FROM dbo.PlaybookVCMProposal--ProposalDataWithNewPrice
WHERE 
	NewPrice >= 0.0

SET @RowsI = @RowsI + @@RowCount

EXEC LogDCPEvent ''DCP --- VCM_PopulateProposal: Insert ProposalPrice'', ''E'', @RowsI, 0, 0 
'

RAISERROR ('INSERT INTO ProposalPrice Stmt:', 0, 1) WITH NOWAIT 
EXEC dbo.PrintNVarCharMax @SQLStmt
RAISERROR ('---', 0, 1) WITH NOWAIT 
EXEC (@SQLStmt)


/*

; WITH ProposalData AS (
	SELECT
		vcm.AccountKey,
		vcm.ItemKey,
		vcm.InvoiceLineGroup1Key,
		vcm.AccountManagerKey,
		Total12MonthFrequency,
		Total12MonthQuantity,
		Total12MonthPrice,
		Total12MonthCost,
		LastItemPrice,
		LastItemCost,
		LastSaleDayKey,
		LastInvoiceLineGroup2Key,
		CurrentItemPrice,
		CurrentItemCost,
		CurrentPriceSource,
		ListPrice,
		ListPriceInListUoM,
		FutureEstimatedListPrice,
		ListPricingUoMConvFactor,
		ListPricingUoM,
		CurrentWarehouseCommissionCost,
		CurrentGPP,
		CurrentDiscount,
		CurrentPriceMethod,
		RecentPriceDate,
		RecentItemCost,
		RecentDiscount,
		vcm.FutureItemCost,
		vcm.FutureItemCostInListUoM,
		vcm.CostEffectiveDate,	
		RPBPlaybookPricingGroupKey,
		BandSequenceNumber,
		FloorGPP,
		TargetGPP,
		StrategicIndicator,
		ShadowGPPIncrease,
		ContractNo,
		RuleNo,
		ContractEndDate,
		CDIIndicator,
		CASE 
			WHEN FutureRebatedCommissionCost IS NOT NULL AND FutureRebatedCommissionCost < vcm.FutureItemCost THEN FutureRebatedCommissionCost
			ELSE
				CASE
					WHEN vcm.CostEffectiveDate <= GETDATE() THEN vcm.FutureItemCost
					ELSE 
						CASE
							WHEN  DATEADD(DAY, 7,RecentPriceDate) >= GETDATE() THEN RecentItemCost
							ELSE CurrentWarehouseCommissionCost
						END
				END
		END AS RelevantCost,
		DropShipIndicator,
		StockStatus,
		RebateId,
		RebateEndDate,
		RebatedCommissionCost,
		FutureRebatedCommissionCost,
		--CASE 
		--	WHEN RuleNo = ''01'' OR RuleNo = ''1'' THEN ''P''
		--	WHEN RuleNo = ''02'' OR RuleNo = ''2'' OR RuleNo = ''03'' OR RuleNo = ''3'' THEN ''D''
		--	ELSE ''M''
		--END AS PriceMethod,
		ForecastedMonthlyQty,
		'+@EffectiveDate+' AS EffectiveDate
	FROM dbo.PlaybookVCMProposal vcm
	JOIN FactSalesForecast 
		ON FactSalesForecast.AccountKey = vcm.AccountKey 
		AND FactSalesForecast.ItemKey = vcm.ItemKey 
		AND FactSalesForecast.InvoiceLineGroup1Key = vcm.InvoiceLineGroup1Key 
		AND ForecastType = '''+@ForecastType+'''
	'+@JoinClause2+'
	' + @SuppressionJoinClause + '		
	WHERE 1=1
		'+@WhereClause2 + '
), ProposalDataWithNewPrice AS (
SELECT
	AccountKey,
	ItemKey,
	InvoiceLineGroup1Key,
	AccountManagerKey,
	CASE
		WHEN CurrentPriceMethod = ''D'' AND ListPrice > 0. AND CurrentDiscount > 0. THEN
			CASE 
				WHEN FutureEstimatedListPrice - (FutureEstimatedListPrice * CurrentDiscount) <= 0.		-- when new price <= 0
					--THEN RelevantCost / (1. - TargetGPP)		-- then step back and use target gpp
					THEN FutureEstimatedListPrice
				WHEN ( (FutureEstimatedListPrice - (FutureEstimatedListPrice * CurrentDiscount)) - RelevantCost ) / (FutureEstimatedListPrice - (FutureEstimatedListPrice * CurrentDiscount)) < 0. -- when recommended gpp < 0 
					--THEN RelevantCost / (1. - TargetGPP)		-- then step back and use target gpp
					THEN FutureEstimatedListPrice
				ELSE 
					FutureEstimatedListPrice - (FutureEstimatedListPrice * CurrentDiscount)
			END			
		WHEN CurrentItemPrice = 0.0
			THEN 0.0
		WHEN CurrentGPP < 0.0 
			THEN RelevantCost / (1. - TargetGPP)
		WHEN (CurrentItemCost >= RelevantCost) -- hold price
			THEN CurrentItemPrice
		ELSE
			RelevantCost / (1. - ShadowGPPIncrease)
	END AS NewPrice,		
	Total12MonthQuantity,
	Total12MonthFrequency,
	Total12MonthCost,
	Total12MonthPrice,
	LastItemCost,
	LastItemPrice,
	LastSaleDayKey,
	LastInvoiceLineGroup2Key,
	EffectiveDate,
	RebatedCommissionCost,
	CurrentWarehouseCommissionCost,
	CurrentPriceMethod,
	StrategicIndicator,
	CDIIndicator,
	' + @ExpirationDate + ' AS ExpirationDate,
	CostEffectiveDate,
	CurrentDiscount,
	FutureItemCost,
	FutureItemCostInListUoM,
	ListPrice,
	ListPriceInListUoM,
	FutureEstimatedListPrice,
	ListPricingUoMConvFactor,
	ListPricingUoM,
	RPBPlaybookPricingGroupKey,
	BandSequenceNumber,
	ContractNo,
	RuleNo,
	ContractEndDate,
	DropShipIndicator,
	StockStatus,
	RebateId,
	RebateEndDate,
	RelevantCost,
	ForecastedMonthlyQty,
	CurrentItemCost,
	CurrentItemPrice,
	CurrentPriceSource,
	RecentPriceDate,
	RecentItemCost,
	RecentDiscount,
	FutureRebatedCommissionCost,
	FloorGPP,
	TargetGPP	
FROM ProposalData
)
*/
------ collisions... 
--EXEC LogDCPEvent 'PopulateRebateChangeProposal: Delete colliding RC proposals in ProposalPrice', 'B', 0, 0, 0 ,@ProjectKey

--; WITH Data AS (
--	SELECT
--		pp.AccountKey,
--		pp.ItemKey,
--		pp.InvoiceLineGroup1Key
--	FROM ProposalPrice pp
--	INNER JOIN dbo.PlaybookProject p
--		ON p.ProjectKey = pp.ProjectKey	
--	WHERE 
--		ProductionProjectIndicator = 'Y'
--		AND pp.AcceptanceCodeKey <> @AcceptedCode
--		AND pp.AcceptanceCodeKey <> @DeletedCode
--	GROUP BY
--		pp.AccountKey,
--		pp.ItemKey,
--		pp.InvoiceLineGroup1Key
--	HAVING
--		COUNT(*) > 1
--)
--UPDATE pp 
--	SET 
--		pp.AcceptanceCodeKey = @DeletedCode,
--		pp.ProposalPriceGroupKey = NULL,
--		pp.ProposalPriceBuyGroupKey = NULL,
--		pp.MaxStatus = 'D',
--		pp.ModificationDate = GETDATE(),
--		pp.ModificationUser = 'System Auto Delete - new proposal to be generated',
--		pp.ModificationUserAddress = CAST(@ProjectKey AS VARCHAR(25))
--FROM dbo.ProposalPrice pp
--INNER JOIN Data d
--	ON d.AccountKey = pp.AccountKey
--	AND d.ItemKey = pp.ItemKey
--	AND d.InvoiceLineGroup1Key = pp.InvoiceLineGroup1Key
--INNER JOIN dbo.PlaybookProject p
--	ON p.ProjectKey = pp.ProjectKey	
--WHERE
--	pp.ProjectKey <> @ProjectKey		-- keep the newest proposal - from this project
	
--	(vcm.FutureItemCostInListUoM <> pp.FutureItemCost OR vcm.CostEffectiveDate <> pp.CostEffectiveDate)  -- something changed, commit
	
	
--	AND pp.AcceptanceCodeKey <> @AcceptedCode
--	AND pp.AcceptanceCodeKey <> @DeletedCode
--	AND ProductionProjectIndicator = 'Y'	
--SET @RowsU = @RowsU + @@ROWCOUNT	

--EXEC LogDCPEvent 'PopulateRebateChangeProposal: Delete colliding vcm proposals in ProposalPrice', 'E', 0, @RowsU, 0 ,@ProjectKey







--EXEC LogDCPEvent 'DCP --- VCM_PopulateProposal: Assign proposals to groups', 'B',0,0,0,@ProjectKey

--EXEC dbo.RefreshProposalPriceGroupAssignment 1, null, null, null, null

--EXEC LogDCPEvent 'DCP --- VCM_PopulateProposal: Assign proposals to groups', 'E',0,0,0,@ProjectKey





EXEC LogDCPEvent 'DCP --- VCM_PopulateProposal', 'E', @RowsI, @RowsU, @RowsD, @ProjectKey






























GO
