SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [lion].[DeleteTerms] 
			@Filename nvarchar(4000)
AS

SET NOCOUNT ON
SET ANSI_WARNINGS OFF 

if (@Filename <> '')
begin
	delete from lion.IncomingTerms
	where FileName = @FileName
end

select @@ROWCOUNT AS RowsDeleted


GO
