SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE [lion].[DetermineCreditMatches]
AS
BEGIN

SET NOCOUNT ON


EXEC LogDCPEvent N'ETL - DetermineCreditMatches', N'B'

DECLARE
	@MatchedSalesAdvanousInvoiceLineId INT,
	@CreditAdvanousInvoiceLineId INT,
	@CreditCrdOrigInvNo NVARCHAR(50),
	@CreditPyramidCode NVARCHAR(25),
	@CreditProductCode NVARCHAR(100),
	@CreditQty DECIMAL(13,2),
	@CreditTransactionDate DATE,
	@CreditUnitPrice DECIMAL(19,3), 
	@CreditLinePrice DECIMAL(13,3),
	@CreditApTradingMarginPadRelease DECIMAL(19,8),
	@RowsI INT, 
	@RowsU INT, 
	@RowsD INT


SET @RowsI = 0
SET @RowsU = 0
SET @RowsD = 0



UPDATE t
SET 
	CreditAdvanousInvoiceLineId = NULL,
	EffectiveQty = NULL,
	EffectiveLinePrice = NULL,
	EffectiveApTradingMarginPadRelease = NULL
FROM Lion_Staging.dbo.Transactions t


DECLARE Credits CURSOR 
FAST_FORWARD
FOR
	SELECT AdvanousInvoiceLineId, CrdOrigInvNo, PyramidCode, ProductCode, Qty, TransactionDate, UnitPrice, LinePrice, ApTradingMarginPadRelease
	FROM Lion_Staging.dbo.Transactions CreditTransactionLine (nolock)
	WHERE
		CreditTransactionLine.SupplyType = N'Credit'	-- the potential credit must be credit as defined by the supply type
		AND CreditTransactionLine.CrdOrigInvNo <> N''		-- the credit line must have a real inv number		
		AND EXISTS (SELECT 1
			FROM Lion_Staging.dbo.Transactions SalesTransactionLine
			WHERE 
				CreditTransactionLine.CrdOrigInvNo IS NOT NULL
				AND SalesTransactionLine.SLedgerNo IS NOT NULL 
				AND SalesTransactionLine.ProductCode IS NOT NULL 
				AND CreditTransactionLine.Qty IS NOT NULL
				AND SalesTransactionLine.Qty IS NOT NULL
				AND CreditTransactionLine.TransactionDate IS NOT NULL
				AND SalesTransactionLine.TransactionDate IS NOT NULL
				AND SalesTransactionLine.SupplyType IS NOT NULL
				AND CreditTransactionLine.CrdOrigInvNo = SalesTransactionLine.SLedgerNo
				AND ISNULL(CreditTransactionLine.PyramidCode, N'') = ISNULL(SalesTransactionLine.PyramidCode, N'')
				AND CreditTransactionLine.ProductCode = SalesTransactionLine.ProductCode
				AND ABS(CreditTransactionLine.Qty) <= SalesTransactionLine.Qty
				AND CreditTransactionLine.TransactionDate >= SalesTransactionLine.TransactionDate
				AND ISNULL(SalesTransactionLine.CrdOrigInvNo, N'') = N''		-- cannot be a credit for another line
				AND ISNULL(SalesTransactionLine.SLedgerNo, N'') <> N''			-- the transaction must have a real SalesLedger number (for some reason there are a lot of blanks)
				AND ISNULL(SalesTransactionLine.SupplyType, N'') <> N'Credit'	-- the transaction must not be credit as defined by the supply type
			)
	ORDER BY 
		TransactionDate DESC, 
		Qty DESC,
		TransNo DESC,
		AdvanousInvoiceLineId DESC

OPEN Credits


FETCH NEXT FROM Credits INTO @CreditAdvanousInvoiceLineId, @CreditCrdOrigInvNo, @CreditPyramidCode, @CreditProductCode, @CreditQty, @CreditTransactionDate, @CreditUnitPrice, @CreditLinePrice, @CreditApTradingMarginPadRelease

WHILE @@FETCH_STATUS = 0
BEGIN

	SET @MatchedSalesAdvanousInvoiceLineId = NULL

	SELECT TOP 1 @MatchedSalesAdvanousInvoiceLineId = AdvanousInvoiceLineId
	FROM Lion_Staging.dbo.Transactions SalesTransactionLine
		WHERE 
			@CreditCrdOrigInvNo = SalesTransactionLine.SLedgerNo
			AND ISNULL(@CreditPyramidCode, N'') = ISNULL(SalesTransactionLine.PyramidCode, N'')
			AND @CreditProductCode = SalesTransactionLine.ProductCode
			AND ABS(@CreditQty) <= SalesTransactionLine.Qty
			AND @CreditTransactionDate >= SalesTransactionLine.TransactionDate
			AND ISNULL(SalesTransactionLine.CrdOrigInvNo, N'') = N''		-- cannot be a credit for another line
			AND ISNULL(SalesTransactionLine.SLedgerNo, N'') <> N''			-- the transaction must have a real SalesLedger number (for some reason there are a lot of blanks)
			AND ISNULL(SalesTransactionLine.SupplyType, N'') <> N'Credit'	-- the transaction must not be credit as defined by the supply type
			AND CreditAdvanousInvoiceLineId IS NULL
	ORDER BY
		SalesTransactionLine.TransactionDate DESC, 
		SalesTransactionLine.TransNo DESC,
		CASE
			WHEN ABS(@CreditQty) = SalesTransactionLine.Qty THEN 1
			ELSE 2
		END,
		SalesTransactionLine.AdvanousInvoiceLineId DESC

	UPDATE t
	SET 
		CreditAdvanousInvoiceLineId = @CreditAdvanousInvoiceLineId,
		EffectiveQty = t.Qty + @CreditQty,
		/*EffectiveTradingPrice = t.TradingPrice + CreditTransactionLine.TradingPrice,*/
		EffectiveLinePrice = --t.LinePrice + CreditTransactionLine.LinePrice,
							CASE
								WHEN t.UnitPrice IS NOT NULL AND t.Qty IS NOT NULL AND t.Qty <> 0. AND t.UnitPrice > 0. THEN 
									t.UnitPrice * t.Qty * ISNULL(SellingConvFactor, 1) / ISNULL(PricingConvFact, 1)
								ELSE t.LinePrice
							END
							+ 
							CASE
								WHEN @CreditUnitPrice IS NOT NULL AND @CreditQty IS NOT NULL THEN 
									@CreditUnitPrice * @CreditQty
								ELSE @CreditLinePrice
							END,
		EffectiveApTradingMarginPadRelease = t.ApTradingMarginPadRelease + @CreditApTradingMarginPadRelease,
		EffectiveSupplierCost = t.SuppliersNettCost
	FROM Lion_Staging.dbo.Transactions t
	LEFT JOIN dbo.DimItem di 
		ON di.ItemNumber = ISNULL(t.PyramidCode, N'') + t.ProductCode
	LEFT JOIN dbo.ItemUOMConversion uc
		ON uc.ItemKey = di.ItemKey
	WHERE t.AdvanousInvoiceLineId = @MatchedSalesAdvanousInvoiceLineId
	SET @RowsU = @@ROWCOUNT + @RowsU

	FETCH NEXT FROM Credits INTO @CreditAdvanousInvoiceLineId, @CreditCrdOrigInvNo, @CreditPyramidCode, @CreditProductCode, @CreditQty, @CreditTransactionDate, @CreditUnitPrice, @CreditLinePrice, @CreditApTradingMarginPadRelease

END
CLOSE Credits
DEALLOCATE Credits

EXEC LogDCPEvent N'ETL - DetermineCreditMatches', N'E', @RowsI, @RowsU, @RowsD



END

GO
