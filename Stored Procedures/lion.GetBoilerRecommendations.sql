SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE [lion].[GetBoilerRecommendations]

	@RegionFilter Description_Small_type,
	@AreaFilter Description_Small_type,
	@NetworkFilter Description_Small_type,
	@BranchFilter Description_Small_type,
	@AccountOwnerTypeFilter Description_Small_type = ''
AS

/*
		drop table #ProductExceptionDefaultTerms
		drop table #LLSPGDefaultTerms
		drop table #LLSPG
		drop table #LLSPGTerms
		drop table #Prelim
		drop table #PrelimSoldLLSPG
		drop table #PrelimLLSPG
		drop table #ItemTerms
		drop table #Updates
		drop table #AllResults

		DECLARE
			@LLSPGCode Description_Small_type,
			@PilotBranchFilter Description_Small_type,
			@RegionFilter Description_Small_type,
			@AreaFilter Description_Small_type,
			@NetworkFilter Description_Small_type,
			@BranchFilter Description_Small_type,
			@CustomerSegmentFilter Description_Small_type


		SET @LLSPGCode = 'HL37'
		SET @PilotBranchFilter ='No'
		SET @RegionFilter = ''
		SET @AreaFilter = ''
		SET @NetworkFilter = ''
		SET @BranchFilter = ''
		SET @CustomerSegmentFilter = '2'
*/

    SET NOCOUNT ON
    SET ANSI_WARNINGS OFF 

    DECLARE @LowerTradeThreshold UDDecimal_type, @TradeFactor UDDecimal_type, @DefaultAction nvarchar(255)
    SET @LowerTradeThreshold = -.05
	SET @TradeFactor = 1 + @LowerTradeThreshold
	SET @DefaultAction = 'Accept'

	DECLARE @LLSPGInstruction nvarchar(255), @ItemInstruction nvarchar(255), @MarketInstruction nvarchar(255), 
	        @UnusedInstruction nvarchar(255), @UnknownInstruction nvarchar(255), @TBDInstruction nvarchar(255)

	SET @LLSPGInstruction	= '1 - LLSPG Disc %'
	SET @ItemInstruction	= '2 - Item Disc %'
	SET @MarketInstruction	= '3 - Market (No Terms)'
	SET @UnusedInstruction	= '4 - Unused Exception'
	SET @UnknownInstruction	= '5 - Unknown'
	SET @TBDInstruction		= 'TBD'

	-- Get ACCOUNT contract LLSPG Terms
	SELECT DISTINCT
		    SPG AS SPGCode,
			CONVERT(decimal(38,8), UsualDiscount1) as Discount
	INTO #LLSPGDefaultTerms
	FROM lion.AccountContractTerms t inner join
	     lion.BoilerLLSPGs bl on t.SPG = bl.LLSPGCode

	-- Get ACCOUNT contract product exceptions
	SELECT DISTINCT
	        bl.LLSPGCode,
		    ExceptionProduct AS Product,
			CONVERT(decimal(38,8), ExceptionDiscount) as Discount
	INTO #ProductExceptionDefaultTerms
	FROM lion.AccountContractTerms t inner join
	     lion.BoilerLLSPGs bl on t.SPG = bl.LLSPGCode
	WHERE ExceptionProduct IS NOT NULL

	-- Get existing LLSPG-level terms for all accounts
    SELECT DISTINCT
            ct.AccountKey ,
            [PyramidCode] ,
            ct.[SPGCode] ,
			s.[LLSPGDescription] ,
            [OwningBrandCode] ,
            CONVERT(decimal(38,8), CASE WHEN ct.UsualDiscount1 ='' THEN NULL ELSE ct.UsualDiscount1 END / 100.000) AS [SPG Disc 1] ,
            CONVERT(decimal(38,8), CASE WHEN ct.UsualDiscount2 ='' THEN NULL ELSE ct.UsualDiscount2 END / 100.000) AS [SPG Disc 2] ,
            CONVERT(decimal(38,8),
			 CASE WHEN ct.UsualDiscount2 <> ''
                 THEN CASE WHEN ct.UsualDiscount1 <> ''
                           THEN CONVERT(DECIMAL(38, 8), CASE WHEN ct.UsualDiscount1 ='' THEN NULL ELSE ct.UsualDiscount1 END)
                                + ( ( 100
                                      - CONVERT(DECIMAL(38, 8), CASE WHEN ct.UsualDiscount1 ='' THEN NULL ELSE ct.UsualDiscount1 END) )
                                    * CONVERT(DECIMAL(38, 8), CASE WHEN ct.UsualDiscount2 ='' THEN NULL ELSE ct.UsualDiscount2 END)
                                    / 100 )
                           ELSE CONVERT(DECIMAL(38, 8), CASE WHEN ct.UsualDiscount2 ='' THEN NULL ELSE ct.UsualDiscount2 END)
                      END
                 ELSE CONVERT(DECIMAL(38, 8), CASE WHEN ct.UsualDiscount1 ='' THEN NULL ELSE ct.UsualDiscount1 END)
            END / 100) AS [Disc] ,
            [DeleteFlag] ,
            [LastChangeInitials] ,
            CONVERT(DATE, LEFT(LastChangeDate, CHARINDEX(' ', LastChangeDate))) AS [LastChangeDate] ,
            [LastChangeBranch]
    INTO    #LLSPGTerms
    FROM    [lion].[CustomerTerms] ct
	        INNER JOIN lion.BoilerLLSPGs bl on ct.SPGCode = bl.LLSPGCode 
            INNER JOIN lion.vwAccount a ON ct.AccountKey = a.AccountKey
			LEFT OUTER JOIN lion.vwLLSPG s on ct.PyramidCode = s.ItemPyramidCode and
			                s.LLSPGCode = ct.SPGCode
    WHERE   ct.PyramidCode = 1 and a.CustomerType <> 'CS'

	-- Get customers who purchase boilers
    SELECT  DISTINCT
            f.AccountKey,
            i.ItemPyramidCode,
            i.LLSPGCode,
			i.LLSPGDescription,
            acct.BranchPrimaryBrand
    INTO    #PrelimSoldLLSPG
    FROM    lion.vwFactInvoiceLinePricebandScore f
        INNER JOIN lion.vwAccount acct ON f.AccountKey = acct.AccountKey
            INNER JOIN lion.vwItem i ON f.ItemKey = i.ItemKey
            INNER JOIN lion.vwFactInvoiceLineGroup1 g1 ON f.InvoiceLineGroup1Key = g1.InvoiceLineGroup1Key
		    INNER JOIN lion.BoilerLLSPGs bl on bl.LLSPGCode = i.LLSPGCode
			INNER JOIN dbo.DimDay dd on f.InvoiceDateDayKey = dd.DayKey
    WHERE   i.ItemPyramidCode = 1
            AND g1.ShipmentType = 'S'
			AND acct.CustomerType <> 'CS' 
			AND f.PriceDerivation NOT IN ('DC', 'DDC')
			AND dd.SQLDate BETWEEN '1-NOV-2014' AND '31-MAR-2015'

	-- Combine existing LLSPG-level terms with other LLSPGs purchased by the customers
    SELECT  DISTINCT
            COALESCE(s.AccountKey, t.AccountKey) AS AccountKey ,
            COALESCE(s.ItemPyramidCode, CONVERT(INT, t.PyramidCode)) AS PyramidCode ,
            COALESCE(s.LLSPGCode, t.SPGCode) AS LLSPGCode ,
			COALESCE(s.LLSPGDescription, t.LLSPGDescription) AS LLSPGDescription ,
            COALESCE(t.[OwningBrandCode], s.BranchPrimaryBrand) AS [Brand] ,
            t.[SPG Disc 1] ,
            t.[SPG Disc 2] ,
            t.[Disc] ,
            t.[DeleteFlag] ,
            t.[LastChangeInitials] ,
            t.[LastChangeDate] ,
            t.[LastChangeBranch] ,
            CONVERT(NVARCHAR(255), '') AS [Price Instruction Type/Level] ,
            CONVERT(decimal(38,8), 0.000000) AS [Rec'mnd Discount] ,
            CASE WHEN ( s.AccountKey IS NULL ) THEN 'Accept'
                 ELSE @DefaultAction
            END AS [Action] ,
			' ' AS KVI
    INTO    #PrelimLLSPG
    FROM    #PrelimSoldLLSPG s
            FULL OUTER JOIN #LLSPGTerms t ON t.AccountKey = s.AccountKey
                                             AND CONVERT(int, t.PyramidCode) = s.ItemPyramidCode
                                             AND t.SPGCode = s.LLSPGCode

    -- Get existing product exception terms
    SELECT  [CustomerTermsKey] ,
            ct.AccountKey ,
            i.ItemKey ,
            [PyramidCode] ,
            [SPGCode] ,
            [OwningBrandCode] ,
            [UsualDiscount1] ,
            [UsualDiscount2] ,
            [DeleteFlag] ,
            [LastChangeInitials] ,
            CONVERT(DATE, LEFT(LastChangeDate, CHARINDEX(' ', LastChangeDate))) AS [LastChangeDate] ,
            [LastChangeTime] ,
            [LastChangePLID] ,
            [LastChangeBranch] ,
            [ExceptionProduct] ,
            [ExceptionDiscount] ,
            [ExceptionFixedPrice] ,
            [ExceptionFromDate] ,
            [ExceptionToDate] ,
            [ExceptionPCF]
	INTO #ItemTerms
    FROM    lion.CustomerTerms ct
			INNER JOIN lion.BoilerLLSPGs bl on ct.SPGCode = bl.LLSPGCode
            INNER JOIN lion.vwAccount a ON ct.AccountKey = a.AccountKey
            INNER JOIN lion.vwItem i ON i.ItemNumber = '1'
                                                      + ct.ExceptionProduct
                                                      AND i.ItemPyramidCode = 1
    WHERE   ct.ExceptionProduct <> ''
            AND CONVERT (INT, ct.PyramidCode) = 1
			AND a.CustomerType <> 'CS'



;with flpac as (
SELECT * FROM (
SELECT
	fil.AccountKey, 
	fil.ItemKey, 
	fil.InvoiceLineGroup1Key,	
	fil.InvoiceLineUniqueIdentifier as LastInvoiceLineUniqueIdentifier,
    ROW_NUMBER() OVER (Partition by fil.AccountKey, fil.ItemKey, fil.InvoiceLineGroup1Key
	                   order by 	fil.AccountKey, 
									fil.ItemKey, 
									fil.InvoiceLineGroup1Key, 
									fil.InvoiceDateDayKey DESC, 
									fil.TotalQuantity DESC, 
									fil.TotalActualPrice DESC,
									fil.ClientInvoiceID DESC,
									fil.InvoiceLineUniqueIdentifier DESC) as RowNumber,
	fil.TotalActualPrice / NULLIF(fil.TotalQuantity, 0) as LastItemPrice,
	fil.TotalActualCost / NULLIF(fil.TotalQuantity, 0) as LastItemCost,
	ISNULL(fil.UDDecimal2, 0) / NULLIF(fil.TotalQuantity, 0)  as LastCCProfit,
	ISNULL(	1 - ((fil.TotalActualPrice / NULLIF(fil.TotalQuantity, 0)) / NULLIF(fil.UnitListprice, 0.)), 0.) AS LastItemDiscountPercent,
	fil.UDVarchar1 as LastPriceDerivation
FROM dbo.FactInvoiceLine fil inner join 
     dbo.DimDay dd on fil.InvoiceDateDayKey = dd.DayKey inner join
     lion.vwAccount a on fil.AccountKey = a.AccountKey inner join
     lion.vwItem i on fil.ItemKey = i.ItemKey inner join
	 lion.BoilerLLSPGs bl on bl.LLSPGCode = i.LLSPGCode
WHERE 
 	TotalQuantity > 0.0 AND 
 	TotalActualPrice > 0.0 AND 
 	TotalActualCost > 0.0 AND 
	(ISNULL(TotalActualPrice, 0.0) - ISNULL(TotalActualCost, 0.0))/NULLIF(TotalActualPrice, 0) between -1.2 and 0.90 AND
    (fil.InvoiceLineGroup1Key < 5 or 
		(fil.InvoiceLineGroup1Key >= 5 and (isnull(fil.UDDecimal2, 0) > 0))
	) AND
	fil.UDVarchar1 NOT IN ('DC', 'DDC') and
	a.CustomerSegment between '2' and '8' and
	dd.SQLDate BETWEEN '1-NOV-2014' AND '31-MAR-2015'
) a
where RowNumber = 1 )

,SalesSummary as (
SELECT
	fil.AccountKey, 
	fil.ItemKey, 
	fil.InvoiceLineGroup1Key,	
	sum(fil.TotalActualPrice) as TotalPeriodSales,
	sum(fil.TotalActualCost) as TotalPeriodCost,
	sum(fil.TotalQuantity) as TotalPeriodQuantity
FROM dbo.FactInvoiceLine fil inner join 
     dbo.DimDay dd on fil.InvoiceDateDayKey = dd.DayKey inner join
     lion.vwAccount a on fil.AccountKey = a.AccountKey inner join
     lion.vwItem i on fil.ItemKey = i.ItemKey inner join
	 lion.BoilerLLSPGs bl on bl.LLSPGCode = i.LLSPGCode
WHERE 
 	TotalQuantity > 0.0 AND 
 	TotalActualPrice > 0.0 AND 
 	TotalActualCost > 0.0 AND 
	(ISNULL(TotalActualPrice, 0.0) - ISNULL(TotalActualCost, 0.0))/NULLIF(TotalActualPrice, 0) between -1.2 and 0.90 AND
    (fil.InvoiceLineGroup1Key < 5 or 
		(fil.InvoiceLineGroup1Key >= 5 and (isnull(fil.UDDecimal2, 0) > 0))
	) AND
	a.CustomerSegment between '2' and '8' and
	dd.SQLDate BETWEEN '1-NOV-2014' AND '31-MAR-2015'
group by	fil.AccountKey, 
			fil.ItemKey, 
			fil.InvoiceLineGroup1Key
)
											 
    SELECT  f.AccountKey ,
            f.ItemKey ,
            f.InvoiceLineGroup1Key , 
		-- Product Information
            i.LLSPGCode ,
            i.LLSPGCode + ' - ' + i.LLSPGDescription AS LLSPG ,
            i.ItemNumber AS [Product Code] ,
            i.ItemDescription AS [Product Description] ,
            acct.BranchPrimaryBrand AS [Brand] ,
			i.CurrentBranchCost - i.Padding - i.CurrNotSettVal - f.LastCCProfit  [Current Cost] ,
		-- Action
            CASE WHEN (t.Price = 0 ) THEN 'Accept' ELSE @DefaultAction END AS [Action] ,
            CASE WHEN ( t.Price = 0 ) THEN @UnknownInstruction
			    -- All KVI products should be considered as overridable
				/*
				 WHEN (i.KnownValueInd = 'Y' and 
			                  not exists (select * 
							                from lion.AccountSPG aspg 
										   where aspg.AccountKey = f.AccountKey and 
										         aspg.LLSPGCode IN (i.LLSPGCode, 'ALL'))) OR
                      ( lip.Price - t.Price ) / NULLIF(t.Price, 0) >= @LowerTradeThreshold
                 THEN @MarketInstruction
				 */
                 --WHEN lip.Price <= r.Price
                 --THEN IIF(f.UDVarchar1 = 'Y', @ItemInstruction, @TBDInstruction)
				 ELSE @TBDInstruction
                 --WHEN lip.Price <= a.Price
                 --THEN @TBDInstruction
                 --WHEN lip.Price <= g.Price THEN @MarketInstruction
                 --ELSE @MarketInstruction
            END AS [Price Instruction Type/Level] ,

		-- Accepted/Recommended Pricing
		  -- AcceptedPrice
		  -- AcceptedDiscount
		  -- AcceptedImpact
		  -- AcceptedPriceLevel

		     
			-- All KVI products should be considered as overridable
			/*
            CASE WHEN i.KnownValueInd = 'Y' and 
			                  not exists (select * 
							                from lion.AccountSPG aspg 
										   where aspg.AccountKey = f.AccountKey and 
										         aspg.LLSPGCode IN (i.LLSPGCode, 'ALL'))
				 THEN t.Price
				 WHEN f.UDVarchar1='Y' -- Strategic Item
			     THEN lip.Price
			*/
			CASE WHEN ( lip.Price - t.Price ) / NULLIF(t.Price, 0) >= @LowerTradeThreshold
					  -- Apply Trade Price Matching Logic
                 THEN IIF(lip.Price + (t.Price * cp.ChangePct) < t.Price, lip.Price + (t.Price * cp.ChangePct), t.Price)
                 WHEN lip.Price < g.Price
					  -- Move by change percent up to green
				 THEN IIF(lip.Price + (t.Price * cp.ChangePct) < g.Price, lip.Price + (t.Price * cp.ChangePct), g.Price)
					  -- If already charging above green, use the last price
                 ELSE lip.Price
            END AS [Rec'mnd Price] ,
            ROUND(
				  /*
				  CASE WHEN i.KnownValueInd = 'Y' and 
			                  not exists (select * 
							                from lion.AccountSPG aspg 
										   where aspg.AccountKey = f.AccountKey and 
										         aspg.LLSPGCode IN (i.LLSPGCode, 'ALL'))
					   THEN 0
			           WHEN f.UDVarchar1='Y' -- Strategic Item
			           THEN (1 - (lip.Price / NULLIF(t.Price, 0)))
				  */
				  CASE WHEN ( lip.Price - t.Price ) / NULLIF(t.Price, 0) >= @LowerTradeThreshold
						  -- Apply Trade Price Matching Logic
					   THEN (1 - ( IIF(lip.Price + (t.Price * cp.ChangePct) < t.Price, lip.Price + (t.Price * cp.ChangePct), t.Price) / NULLIF(t.Price, 0)))
					   WHEN lip.Price < g.Price
						  -- Move by change percent up to green
					   THEN (1 - ( IIF(lip.Price + (t.Price * cp.ChangePct) < g.Price, lip.Price + (t.Price * cp.ChangePct), g.Price) / NULLIF(t.Price, 0)))
						  -- If already charging above green, use the last price
					   ELSE (1 - (lip.Price / NULLIF(t.Price, 0)))
				   END
            , 4 ) AS [Rec'mnd Discount] ,
			/*
            CASE WHEN i.KnownValueInd = 'Y' and 
			          not exists (select * 
					                from lion.AccountSPG aspg 
								   where aspg.AccountKey = f.AccountKey and 
								         aspg.LLSPGCode IN (i.LLSPGCode, 'ALL'))
				 THEN 'Trade'
				 WHEN f.UDVarchar1='Y' -- Strategic Item
			     THEN 'Last'
			*/
			CASE WHEN ( lip.Price - t.Price ) / NULLIF(t.Price, 0) >= @LowerTradeThreshold
                 THEN 'Trade'
                 WHEN lip.Price <= r.Price
                 THEN 'Red'
                 WHEN lip.Price <= a.Price
                 THEN 'Amber'
                 WHEN lip.Price <= g.Price 
				 THEN 'Green'
                 ELSE 'Last'
            END AS [Rec'mnd Price Logic] ,
            ((/*CASE WHEN i.KnownValueInd = 'Y' and 
			                  not exists (select * 
							                from lion.AccountSPG aspg 
										   where aspg.AccountKey = f.AccountKey and 
										         aspg.LLSPGCode IN (i.LLSPGCode, 'ALL'))
				 THEN t.Price
				 WHEN f.UDVarchar1='Y' -- Strategic Item
			     THEN lip.Price
			  */
			CASE WHEN ( lip.Price - t.Price ) / NULLIF(t.Price, 0) >= @LowerTradeThreshold
					  -- Apply Trade Price Matching Logic
                 THEN IIF(lip.Price + (t.Price * cp.ChangePct) < t.Price, lip.Price + (t.Price * cp.ChangePct), t.Price)
                 WHEN lip.Price < g.Price
					  -- Move by change percent up to green
				 THEN IIF(lip.Price + (t.Price * cp.ChangePct) < t.Price, lip.Price + (t.Price * cp.ChangePct), t.Price)
					  -- If already charging above green, use the last price
                 ELSE lip.Price
            END) - (i.CurrentBranchCost - i.Padding - i.CurrNotSettVal - f.LastCCProfit) - (f.LastItemPrice - f.LastItemCost)
			) * ss.TotalPeriodQuantity AS [Rec'mnd Impact] ,
            'NA' AS [Strategic Item] ,
		-- History
		    f.LastItemPrice as [Last Item Price] ,
			f.LastItemCost as [Last Item Cost],
            lip.Price AS [Adjusted Last Item Price] ,
            CASE WHEN lip.Price < a.Price THEN 'Red'
                 WHEN lip.Price < g.Price THEN 'Amber'
                 WHEN lip.Price >= g.Price THEN 'Green'
                 ELSE 'Unknown'
            END AS [Last Price Level (GAR)] ,
            f.LastItemDiscountPercent AS [Last Item Discount] ,
			(f.LastItemPrice - f.LastItemCost) / NULLIF(f.LastItemPrice, 0.0) as [Last Item GPP] ,
            l.GPP AS [Adjusted Last Item GPP] ,
            ss.TotalPeriodSales AS [Total Sales] ,
			ss.TotalPeriodQuantity * t.Price AS [Total Sales @ Trade],
            ss.TotalPeriodQuantity AS [Total Quantity] ,

		-- Price Bands
            r.Price AS [Red Price] ,
            a.Price AS [Amber Price] ,
			g.Price AS [Green Price (TradeAdj)] ,
            --t.Price AS [Trade Price] ,
			i.CurrBranchTradePrice AS [Trade Price] ,
			t.Price as [Calc Trade Price] ,
			pb.StretchGPP as [Green GPP] ,
			pb.TargetGPP as [Amber GPP] ,
			pb.FloorGPP as [Red GPP] ,
			tgpp.GPP as [Trade GPP] ,
            g1.DeviatedIndicator AS [Contract Claims] ,
            pba.PriceApproach AS [Price Approach] ,
            pba.Sales603010Bucket AS [Sales Size] ,
            pba.PricingRuleSequenceNumber AS [PB Level] ,
            pba.PlaybookPricingGroupKey ,
			lil.PriceDerivation ,
			dd.SQLDate as LastSaleDate
    INTO    #Prelim
    FROM    flpac f
			INNER JOIN SalesSummary ss on f.AccountKey = ss.AccountKey and f.ItemKey = ss.ItemKey and f.InvoiceLineGroup1Key = ss.InvoiceLineGroup1Key
	        INNER JOIN lion.vwFactInvoiceLine lil on f.LastInvoiceLineUniqueIdentifier = lil.InvoiceLineUniqueIdentifier
			INNER JOIN dbo.DimDay dd on lil.InvoiceDateDayKey = dd.DayKey 
            INNER JOIN dbo.FactRecommendedPriceBand pb ON f.AccountKey = pb.AccountKey
                                                              AND f.ItemKey = pb.ItemKey
                                                              AND f.InvoiceLineGroup1Key = pb.InvoiceLineGroup1Key
            INNER JOIN lion.vwAccount acct ON f.AccountKey = acct.AccountKey
            INNER JOIN lion.vwItem i ON f.ItemKey = i.ItemKey
			INNER JOIN lion.BoilerLLSPGs bl on bl.LLSPGCode = i.LLSPGCode
            INNER JOIN lion.vwFactInvoiceLineGroup1 g1 ON f.InvoiceLineGroup1Key = g1.InvoiceLineGroup1Key
            LEFT OUTER JOIN lion.vwRPBPriceBandAttributes pba ON pb.PlaybookPricingGroupKey = pba.PlaybookPricingGroupKey
            CROSS APPLY lion.fn_GetGPP(f.LastItemPrice, f.LastItemCost) l
            CROSS APPLY lion.fn_GetPriceEx(i.CurrentBranchCost - i.Padding - i.CurrNotSettVal, pb.FloorGPP, @TradeFactor, i.CurrBranchTradePrice) r
            CROSS APPLY lion.fn_GetPriceEx(i.CurrentBranchCost - i.Padding - i.CurrNotSettVal, pb.TargetGPP, @TradeFactor, i.CurrBranchTradePrice) a
            CROSS APPLY lion.fn_GetPriceEx(i.CurrentBranchCost - i.Padding - i.CurrNotSettVal, pb.StretchGPP, @TradeFactor, i.CurrBranchTradePrice) g
            CROSS APPLY lion.fn_GetPrice( i.CurrBranchTradePrice, 0) t
            CROSS APPLY lion.fn_GetGPP(t.Price, i.CurrentBranchCost - i.Padding - i.CurrNotSettVal) tgpp
			CROSS APPLY lion.fn_GetDiscPrice(t.Price, f.LastItemDiscountPercent) lip  -- Last item's price at same discount w/today's trade price
			CROSS APPLY lion.fn_GetGPP(f.LastItemPrice, i.CurrentBranchCost - i.Padding - i.CurrNotSettVal) lgpp
            CROSS APPLY lion.fn_GetImpact(r.Price - (i.CurrentBranchCost - i.Padding - i.CurrNotSettVal - f.LastCCProfit), f.LastItemPrice - f.LastItemCost, ss.TotalPeriodQuantity) ri
            CROSS APPLY lion.fn_GetImpact(a.Price - (i.CurrentBranchCost - i.Padding - i.CurrNotSettVal - f.LastCCProfit), f.LastItemPrice - f.LastItemCost, ss.TotalPeriodQuantity) ai
            CROSS APPLY lion.fn_GetImpact(g.Price - (i.CurrentBranchCost - i.Padding - i.CurrNotSettVal - f.LastCCProfit), f.LastItemPrice - f.LastItemCost, ss.TotalPeriodQuantity) gi
            CROSS APPLY lion.fn_GetImpact(t.Price - (i.CurrentBranchCost - i.Padding - i.CurrNotSettVal - f.LastCCProfit), f.LastItemPrice - f.LastItemCost, ss.TotalPeriodQuantity) ti
            CROSS APPLY lion.fn_GetRisk(  t.Price - (i.CurrentBranchCost - i.Padding - i.CurrNotSettVal - f.LastCCProfit), f.LastItemPrice - f.LastItemCost, ss.TotalPeriodQuantity) tr
			CROSS APPLY lion.fn_GetDiscountChangePct(lip.Price, g.Price, a.Price, r.Price) cp
    WHERE   i.ItemPyramidCode = 1
            AND g1.ShipmentType = 'S'
			AND acct.CustomerType <> 'CS'

    SELECT  AccountKey ,
            LLSPGCode ,
			SUM([Total Sales @ Trade] * [Rec'mnd Discount]) /  -- Original Discount Amount
			   NULLIF(SUM([Total Sales @ Trade]), 0)
		    AS [Rec'mnd Discount]
    INTO    #LLSPG
    FROM    #Prelim p
    WHERE   [Price Instruction Type/Level] = @TBDInstruction and
	        [PriceDerivation] NOT IN ('DP','DC','ADJ','SPO') and
			[Strategic Item] = 'N'
	GROUP BY AccountKey, LLSPGCode
    ORDER BY 1
    
    SELECT  l.AccountKey ,
            l.LLSPGCode ,
            CASE WHEN ROUND([Rec'mnd Discount], 1) = 1 THEN [Rec'mnd Discount] ELSE ROUND([Rec'mnd Discount] / .0025, 0) * .0025 END AS NewDiscount
    INTO    #Updates
    FROM    #LLSPG l

    UPDATE  p
    SET     [Price Instruction Type/Level] = @LLSPGInstruction ,
            [Rec'mnd Discount] = u.NewDiscount ,
            [Rec'mnd Price] = [Trade Price] * ( 1 - u.NewDiscount ) ,
            [Rec'mnd Impact] = ((([Trade Price] * ( 1 - u.NewDiscount )) - [Current Cost]) - ( [Last Item Price] - [Last Item Cost])) * p.[Total Quantity]
    FROM    #Prelim p
            INNER JOIN #Updates u ON p.AccountKey = u.AccountKey
    WHERE  p.[Price Instruction Type/Level] = @TBDInstruction AND
           (p.[Strategic Item] = 'N' OR
		    not exists (select * 
			              from #ItemTerms it 
						 where it.AccountKey = p.AccountKey and
							   it.ItemKey = p.ItemKey) OR
		    (p.[Strategic Item] = 'Y' and ABS(p.[Rec'mnd Discount] - u.NewDiscount) <= .01))

    UPDATE  pl
    SET     [Price Instruction Type/Level] = @LLSPGInstruction ,
            [Rec'mnd Discount] = u.NewDiscount
    FROM    #PrelimLLSPG pl
            INNER JOIN #Updates u ON pl.AccountKey = u.AccountKey

/*
    UPDATE  p
    SET     [Price Instruction Type/Level] = @ItemInstruction ,
            [Rec'mnd Discount] = p.[Last Item Discount] ,
            [Rec'mnd Price] = [Trade Price] * ( 1 - p.[Last Item Discount] ) ,
            [Rec'mnd Impact] = ((([Trade Price] * ( 1 - p.[Last Item Discount] )) - [Current Cost]) - ( [Last Item Price] - [Last Item Cost])) * p.[Total Quantity],
			[Rec'mnd Price Logic] = 'Last',
			[Action] = 'Skip'
    FROM    #Prelim p inner join lion.vwAccount a on p.AccountKey = a.AccountKey
    WHERE  p.[Price Instruction Type/Level] = @TBDInstruction AND 
		   not exists (select * 
			             from #ItemTerms it 
						where it.AccountKey = p.AccountKey and
							  it.ItemKey = p.ItemKey) AND
		   not exists (select * 
			             from #LLSPGTerms lt 
						where lt.AccountKey = p.AccountKey and
							  lt.SPGCode = p.LLSPGCode) AND
		   not exists (select * 
			             from #PrelimLLSPG pl 
						where pl.AccountKey = p.AccountKey and
							  pl.LLSPGCode = p.LLSPGCode and
							  pl.[SPG Disc 1] is not null)

    UPDATE  #Prelim
    SET     [Price Instruction Type/Level] = @ItemInstruction
    WHERE   [Price Instruction Type/Level] = @TBDInstruction
*/

	UPDATE pl
	   SET pl.KVI = k.MaxKVI
	  FROM #PrelimLLSPG pl inner join 
			( SELECT p.LLSPGCode, p.PyramidCode, MAX(i.KnownValueInd) AS MaxKVI
			 from #PrelimLLSPG p
					INNER JOIN lion.vwItem i ON p.LLSPGCode = i.LLSPGCode
					and p.PyramidCode = i.ItemPyramidCode
			GROUP BY p.LLSPGCode, p.PyramidCode ) k on pl.LLSPGCode = k.LLSPGCode and pl.PyramidCode = k.PyramidCode

    SELECT  'Detail' AS RowType ,
            COALESCE(p.[AccountKey], c.AccountKey) as AccountKey,
            COALESCE(p.[ItemKey], c.ItemKey) as ItemKey,
            [InvoiceLineGroup1Key] ,
            a.AccountNumber as AccountNumber ,
            a.AccountName as AccountName,
            a.Area as Area ,
            i.LLSPGCode AS LLSPG ,
            i.LLSPGDescription AS [LLSPG Description] ,
            SUBSTRING(i.ItemNumber, 2, 100) AS [Product Code] ,
            i.[ItemDescription] as [Product Description] ,
			[Current Cost] ,
            i.KnownValueInd AS KVI ,
            NULL AS [NDP Flag] ,
            CONVERT(DECIMAL(38, 8), d.Discount) / 100 AS DefaultDiscount ,
            c.OwningBrandCode AS [Brand] ,
            CONVERT(DECIMAL(38, 8), c.UsualDiscount1) / 100 AS UsualDiscount1 ,
            CONVERT(DECIMAL(38, 8), c.UsualDiscount2) / 100 AS UsualDiscount2 ,
            CONVERT(DECIMAL(38, 8), c.ExceptionDiscount) / 100 AS ExceptionDiscount ,
            CASE WHEN c.UsualDiscount2 <> ''
                 THEN CASE WHEN c.UsualDiscount1 <> ''
                           THEN CONVERT(DECIMAL(38, 8), c.UsualDiscount1)
                                + ( ( 100
                                      - CONVERT(DECIMAL(38, 8), c.UsualDiscount1) )
                                    * CONVERT(DECIMAL(38, 8), c.UsualDiscount2)
                                    / 100 )
                           ELSE CONVERT(DECIMAL(38, 8), c.UsualDiscount2)
                      END
                 ELSE CONVERT(DECIMAL(38, 8), c.UsualDiscount1)
            END / 100 AS [Disc] ,
            [Rec'mnd Discount] * -1 AS [Rec'mnd Discount] ,
			COALESCE([Price Instruction Type/Level], @UnusedInstruction) AS [Price Instruction Type/Level]  ,
            COALESCE([Action], 'Accept') AS [Action] ,
            c.[LastChangeDate] AS [Last Change Date] ,
            c.[ExceptionFixedPrice] AS [Fixed Price] ,
            [Total Sales] AS [Item Sales] ,
            SUM([Total Sales]) OVER ( PARTITION BY [LLSPG] ) AS [Total Sales] ,
            SUM([Total Sales]) OVER ( PARTITION BY p.[AccountKey] ) AS [Account Sales] ,
            [Rec'mnd Price] ,
            [Rec'mnd Price Logic] ,
            [Rec'mnd Impact] ,
            [Strategic Item] ,
            [Last Item Price] ,
            [Adjusted Last Item Price] ,
            [Last Price Level (GAR)] ,
            [Last Item Discount] ,
            [Last Item GPP] ,
			[Adjusted Last Item GPP] ,
            [Total Quantity] ,
            [Green Price (TradeAdj)] ,
            [Amber Price] ,
            [Red Price] ,
            [Trade Price] ,
			[Green GPP],
			[Amber GPP],
			[Red GPP],
			[Trade GPP],
            [Contract Claims] ,
            [Sales Size] ,
            [Price Approach] ,
            [PB Level] ,
            [CustomerTermsKey] ,
            a.AccountNumber AS AccountID,
            [PyramidCode] ,
            [SPGCode] ,
            [OwningBrandCode] ,
            [DeleteFlag] ,
            [LastChangeInitials] ,
            [LastChangeDate] ,
            [LastChangeTime] ,
            [LastChangePLID] ,
            [LastChangeBranch] ,
            [ExceptionProduct] ,
            [ExceptionFixedPrice] ,
            [ExceptionFromDate] ,
            [ExceptionToDate] ,
            [ExceptionPCF] ,
            [PlaybookPricingGroupKey] ,
			[PriceDerivation] ,
			[LastSaleDate]
    INTO    #AllResults
    FROM    #Prelim p
            FULL OUTER JOIN #ItemTerms c on c.AccountKey = p.AccountKey and c.ItemKey = p.ItemKey
            INNER JOIN lion.vwAccount a ON COALESCE(p.AccountKey, c.AccountKey) = a.AccountKey
            INNER JOIN lion.vwItem i ON COALESCE(p.ItemKey, c.ItemKey) = i.ItemKey
			LEFT OUTER JOIN #ProductExceptionDefaultTerms d ON i.ItemNumber = '1'
                                                      + d.Product
                                                      AND i.ItemPyramidCode = 1
    UNION
    SELECT  DISTINCT
            'LLSPG' AS RowType ,
            pl.[AccountKey] ,
            NULL AS [ItemKey] ,
            NULL AS [InvoiceLineGroup1Key] ,
            AccountNumber ,
            AccountName ,
            Area ,
            pl.LLSPGCode AS LLSPG ,
            LLSPGDescription AS [LLSPG Description] ,
            NULL AS [Product Code] ,
            NULL AS [Product Description] ,
			NULL AS [Current Cost] ,
            KVI ,
            CASE WHEN K.LLSPGCode is not null THEN 'Y' ELSE NULL END AS [NDP Flag] ,
            CONVERT(DECIMAL(38, 8), d.Discount) / 100 AS DefaultDiscount ,
            [Brand] ,
            [SPG Disc 1] ,
            [SPG Disc 2] ,
            NULL AS ExceptionDiscount ,
            [Disc] ,
            [Rec'mnd Discount] * -1 AS [Rec'mnd Discount] ,
            [Price Instruction Type/Level] ,
            [Action] ,
            [LastChangeDate] AS [Last Change Date] ,
            NULL AS [Fixed Price] ,
            NULL AS [Item Sales] ,
            NULL AS [LLSPG Sales] ,
            NULL AS [Total Sales] ,
            NULL AS [Rec'mnd Price] ,
            NULL AS [Rec'mnd Price Logic] ,
            NULL AS [Rec'mnd Impact] ,
            NULL AS [Strategic Item] ,
            NULL AS [Last Item Price] ,
            NULL AS [Adjusted Last Item Price] ,
            NULL AS [Last Price Level (GAR)] ,
            NULL AS [Last Item Discount] ,
            NULL AS [Last Item GPP] ,
            NULL AS [Adjusted Last Item GPP] ,
            NULL AS [Total Quantity] ,
            NULL AS [Green Price (TradeAdj)] ,
            NULL AS [Amber Price] ,
            NULL AS [Red Price] ,
            NULL AS [Trade Price] ,
			NULL AS [Green GPP] ,
			NULL AS [Amber GPP] ,
			NULL AS [Red GPP] ,
			NULL AS [Trade GPP] ,
            NULL AS [Contract Claims] ,
            NULL AS [Sales Size] ,
            NULL AS [Price Approach] ,
            NULL AS [PB Level] ,
            NULL AS [CustomerTermsKey] ,
            NULL AS [AccountID] ,
            NULL AS [PyramidCode] ,
            NULL AS [SPGCode] ,
            NULL AS [OwningBrandCode] ,
            NULL AS [DeleteFlag] ,
            [LastChangeInitials] ,
            [LastChangeDate] ,
            NULL AS [LastChange time] ,
            NULL AS [LastChangePLID] ,
            [LastChangeBranch] ,
            NULL AS [ExceptionProduct] ,
            NULL AS [ExceptionFixed price] ,
            NULL AS [ExceptionFrom date] ,
            NULL AS [ExceptionToDate] ,
            NULL AS [ExceptionPCF] ,
            NULL AS [PlaybookPricingGroupKey] ,
			NULL AS [PriceDerivation] ,
			NULL AS [LastSaleDate]
    FROM    #PrelimLLSPG pl
            INNER JOIN lion.vwAccount a ON pl.AccountKey = a.AccountKey
			LEFT OUTER JOIN #LLSPGDefaultTerms d on pl.LLSPGCode = d.SPGCode
			LEFT OUTER JOIN lion.AccountSPG k on (k.AccountKey = pl.AccountKey and k.LLSPGCode = pl.LLSPGCode) or (k.AccountKey = pl.AccountKey and  k.LLSPGCode = 'ALL')

    SELECT  SUM(r.[Rec'mnd Impact]) over (partition by r.AccountKey) as [Account Impact],
			r.RowType,
			CAST(NEWID() as nvarchar(255)) AS RecID,
			r.LLSPG,
			r.AccountKey,
			r.ItemKey,
			r.InvoiceLineGroup1Key,
			r.Area,
			r.[LLSPG Description],
			r.[Product Description],
			a.AccountManagerFullName as [Account Owner],
			a.Branch,
			r.AccountNumber,
			r.AccountName,
            SUM(r.[Item Sales]) OVER ( PARTITION BY r.AccountKey, r.[LLSPG] ) AS [LLSPG Sales] ,
			r.[Product Code],
			r.[Total Quantity],
			r.[Item Sales] as [Product Sales],
			r.[Strategic Item],
			a.FixedPriceIndicator as [Fixed Price Indicator],
			r.KVI as [Prod KVI],
			r.DefaultDiscount as [Default Discount],
			l.[SPG Disc 1],
			l.[SPG Disc 2],
			l.Disc as [SPG Compounded Discount],
			r.ExceptionDiscount as [Exception Discount],
			r.ExceptionFixedPrice as [Exception Fixed Price],
			r.ExceptionFromDate as [Fixed Price Start Date],
			r.ExceptionToDate as [Fixed Price Expiry Date],
			r.LastSaleDate as [Last Sale Date],
			r.PriceDerivation as [Last Price Derivation],
			r.[Last Item Discount],
			r.[Last Item Price],
			r.[Last Item GPP],
			r.[Adjusted Last Item Price],
			r.[Adjusted Last Item GPP],
			r.[Rec'mnd Discount],
			r.[Rec'mnd Price],
			r.[Rec'mnd Price Logic],
			r.[Action],
			r.Brand,
			r.[Green Price (TradeAdj)],
			r.[Amber Price],
			r.[Red Price],
			r.[Trade Price],
			r.[Green GPP],
			r.[Amber GPP],
			r.[Red GPP],
			r.[Trade GPP],
			r.[Price Instruction Type/Level],
			r.[Contract Claims],
			r.[Sales Size],
			r.[NDP Flag] as [Terms NDP Override],
			r.[Last Price Level (GAR)],
			r.[Price Approach],
			r.[PB Level],
			r.PlaybookPricingGroupKey,
			r.CustomerTermsKey,
			r.AccountID,
			r.PyramidCode,
			r.SPGCode,
			r.OwningBrandCode,
			r.DeleteFlag,
			r.LastChangeInitials,
			r.LastChangeTime,
			r.LastChangePLID,
			r.LastChangeBranch,
			r.ExceptionProduct,
			r.ExceptionPCF,
			r.[Fixed Price],
			CONVERT(DATE, r.LastChangeDate) as [Last Terms Change Date],
			r.[Rec'mnd Impact],
			r.[Current Cost],
            SUM(r.[Item Sales]) OVER ( PARTITION BY r.[AccountKey] ) AS [Account Sales] ,
			a.BranchName
			--r.[Last Change Date],
	FROM    #AllResults r LEFT OUTER JOIN
	        lion.vwAccount a on r.AccountKey = a.AccountKey LEFT OUTER JOIN
	        #LLSPGTerms l on r.LLSPG = l.SPGCode and r.AccountKey = l.AccountKey
	WHERE   ((@RegionFilter <> '' AND a.Region = @RegionFilter) OR (@RegionFilter = '')) AND
	        ((@AreaFilter <> '' AND a.Area = @AreaFilter) OR (@AreaFilter = '')) AND
	        ((@NetworkFilter <> '' AND a.Network = @NetworkFilter) OR (@NetworkFilter = '')) AND
	        ((@BranchFilter <> '' AND a.Branch = @BranchFilter) OR (@BranchFilter = '')) AND
			((@AccountOwnerTypeFilter <> '' AND IIF(a.AccountOwnerType='', 'Branch', a.AccountOwnerType) = @AccountOwnerTypeFilter) OR (@AccountOwnerTypeFilter = ''))
	order by a.AccountManagerFullName, 
			 1 DESC,  -- Account Impact
	         AccountKey, LLSPG, rowtype DESC, [Price Instruction Type/Level], [Product Code]

drop table #ProductExceptionDefaultTerms
drop table #LLSPGDefaultTerms
drop table #LLSPG
drop table #LLSPGTerms
drop table #Prelim
drop table #PrelimSoldLLSPG
drop table #PrelimLLSPG
drop table #ItemTerms
drop table #Updates
drop table #AllResults





GO
