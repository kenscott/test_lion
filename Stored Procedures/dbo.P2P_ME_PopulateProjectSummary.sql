SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





CREATE
PROCEDURE [dbo].[P2P_ME_PopulateProjectSummary] @ProjectKey Key_Normal_Type
AS

SET QUOTED_IDENTIFIER ON 
SET ANSI_NULLS ON 
SET ARITHABORT ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
SET CONCAT_NULL_YIELDS_NULL ON
SET NUMERIC_ROUNDABORT OFF

DECLARE @RowsI INT, @RowsU INT, @RowsD INT
SET @RowsI = 0
SET @RowsU = 0
SET @RowsD = 0

EXEC LogDCPEvent 'DCP --- P2P_ME_PopulateProjectSummary: Insert PlaybookProjectFullScopeSummary', 'B',0,0,0,@ProjectKey

DECLARE @UnknownInvoiceLineGroup2Key Key_Normal_Type

SET @UnknownInvoiceLineGroup2Key = (SELECT MIN(InvoiceLineGroup2Key) FROM DimInvoiceLineGroup2 WHERE InvoiceLineGroup2UDVarchar1 IN ('None', 'Unknown'))

DELETE FROM PlaybookProjectFullScopeSummary WHERE ProjectKey = @ProjectKey
SET @RowsD = @RowsD + @@RowCount



INSERT PlaybookProjectFullScopeSummary (
	ProjectKey,
	AccountManagerKey,
	AccountKey,
	ItemKey,
	InvoiceLineGroup1Key,
	TotalActual12MonthPrice,
	TotalActual12MonthCost,
	TotalActual12MonthQuantity,
	TotalInvoiceLineCount,
	LastInvoiceLineGroup2Key
)
SELECT 
	@ProjectKey,
	FIL.AccountManagerKey,
	FIL.AccountKey, 
	FIL.ItemKey,
	FIL.InvoiceLineGroup1Key,
	SUM(FIL.TotalActualPrice) TotalActualPrice,
	SUM(FIL.TotalActualCost) TotalActualCost,
	SUM(FIL.TotalQuantity) TotalQuantity,
	COUNT(*) AS 'TotalInvoiceLineCount',
	ISNULL(L.LastInvoiceLineGroup2Key, @UnknownInvoiceLineGroup2Key) AS LastInvoiceLineGroup2Key
FROM dbo.FactInvoiceLine FIL
LEFT JOIN FactLastPriceAndCost L ON FIL.AccountKey = L.AccountKey AND FIL.ItemKey = L.ItemKey AND FIL.InvoiceLineGroup1Key = L.InvoiceLineGroup1Key
WHERE Last12MonthsIndicator = 'Y'
AND FIL.AccountKey IN (SELECT DISTINCT AccountKey FROM PlaybookMarginErosionProposal WHERE PlaybookDataPointGroupKey IN (SELECT PlaybookDataPointGroupKey FROM PlaybookDataPointGroup WHERE ProjectKey = @ProjectKey))	--Only get accounts that are in this project
GROUP BY
	FIL.AccountManagerKey,
	FIL.AccountKey,
	FIL.ItemKey,
	FIL.InvoiceLineGroup1Key,
	L.LastInvoiceLineGroup2Key

SET @RowsI = @RowsI + @@RowCount

DECLARE @USql NVARCHAR(MAX)
SET @USql = 'Update Statistics PlaybookProjectFullScopeSummary'
EXEC (@USql)

EXEC LogDCPEvent 'DCP --- P2P_ME_PopulateProjectSummary: Insert PlaybookProjectFullScopeSummary', 'E', @RowsI, @RowsU, @RowsD









GO
