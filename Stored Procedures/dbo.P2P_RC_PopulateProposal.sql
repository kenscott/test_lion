SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO







CREATE PROCEDURE [dbo].[P2P_RC_PopulateProposal]
	@ProjectKey Key_Normal_type,
	@ScenarioKey Key_Normal_type,
	@PlaybookDataPointGroupKey Key_Normal_type,
	@LimitDataPlaybookKey Key_Normal_type,
	@PricingRulePlaybookKey Key_Normal_type, 
	@ProposalRecommendationPlaybookKey Key_Normal_type,
	@ParamDebug CHAR(1) = 'N'
AS

/*


*/

SET NOCOUNT ON
SET QUOTED_IDENTIFIER ON 
SET ANSI_NULLS ON 
SET ARITHABORT ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
SET CONCAT_NULL_YIELDS_NULL ON
SET NUMERIC_ROUNDABORT OFF
SET XACT_ABORT ON



EXEC LogDCPEvent 'DCP --- PopulateRebateChangeProposal', 'B', 0, 0, 0, @ProjectKey



DECLARE
	@RowsI Quantity_Normal_type, 
	@RowsU Quantity_Normal_type, 
	@RowsD Quantity_Normal_type,
	@SQLStmt NVARCHAR(MAX),
	
	@ApproachTypeKey Key_Normal_type,
	@SuppressionTypeKey Key_Small_Type,
	@ProposalRecommendationKey Key_Normal_type,
	@InvoiceLineGroup1KeyStock Key_Normal_type,
	@DynamicUDVarchar1 NVARCHAR(MAX),
	@SuppressionDate NVARCHAR(MAX),
	@ReasonCode NVARCHAR(MAX),
	@AcceptRejectCode_PriceProposal NVARCHAR(MAX),

	@AcceptedCode Key_Small_type,
	@DeletedCode Key_Small_type,
	@DeferredCode Key_Small_type,
	@WhereClause2 NVARCHAR(MAX),
	@ExpirationDate NVARCHAR(MAX),
	@SuppressionExistsClause NVARCHAR(MAX),
	@NextFridayDate DATETIME,
	@NextSaturdayDate DATETIME,
	@TodayDate DATETIME,
	@TodayDayKey INT

SET @RowsI = 0
SET @RowsU = 0
SET @RowsD = 0


SELECT 
		@TodayDate = SQLDate,
		@TodayDayKey = DayKey
FROM dbo.DimDay 
WHERE DayKey = ((SELECT MAX(InvoiceDateDayKey) FROM dbo.FactInvoiceLine) + 1)	-- fil date will be a Friday, so "today" is a "Saturday"


SELECT @ProposalRecommendationKey = ProposalRecommendationKey 
FROM ATKProposalRecommendation
WHERE ProposalRecommendationPlaybookKey = @ProposalRecommendationPlaybookKey


SELECT @InvoiceLineGroup1KeyStock = (SELECT InvoiceLineGroup1Key FROM dbo.DimInvoiceLineGroup1 WHERE InvoiceLineGroup1UDVarchar1 = 'S')

SELECT 
	--@MinCostDecreaseThresholdPercent = ISNULL(MinCostDecreaseThresholdPercent, 0),
	--@CostDecreaseCapPercent = ISNULL(CostDecreaseCapPercent, 9999999),
	--@MinPriceIncreaseThresholdPercent = ISNULL(MinPriceIncreaseThresholdPercent, 0),
	--@WhereClause1 = ISNULL(WhereClause1, ''),
	--@JoinClause1 = ISNULL(JoinClause1, ''),
	@WhereClause2 = ISNULL(WhereClause2, ''),
	--@JoinClause2= ISNULL(JoinClause2, ''),
	--@WhereClause3 = ISNULL(WhereClause3, ''),
	--@JoinClause3 = ISNULL(JoinClause3, ''),
	@AcceptRejectCode_PriceProposal = ISNULL(AcceptRejectCode_PriceProposal, '1'),  -- 2 = Accepted; 1 = Deferred
	--@AcceptRejectCode_CostProposal  = ISNULL(AcceptRejectCode_CostProposal, '1'),  -- 2 = Accepted; 1 = Deferred
	--@SMEMinGPP = ISNULL(SMEMinGPP, 0),
	--@SMEMaxGPP = ISNULL(SMEMaxGPP, 1),
	--@ProposedPriceOverride = ISNULL(ProposedPriceOverride, ''),	
	--@ProposedCostOverride = ISNULL(ProposedCostOverride, ''),	
	@ReasonCode = ISNULL(DynamicReasonCode, 'NULL'),
	--@EffectiveDate = ISNULL(DynamicEffectiveDate, ''),
	@SuppressionDate = ISNULL(DynamicSuppressionDate, @TodayDate),
	--@PriceIncreaseCapPercent = ISNULL(PriceIncreaseCapPercent, 9999999),
	@DynamicUDVarchar1 = ISNULL(DynamicUDVarchar1, '')
	--@DynamicUDVarchar2 = ISNULL(DynamicUDVarchar2, '')
FROM ATKProposalRecommendation
WHERE ProposalRecommendationKey = @ProposalRecommendationKey

IF @WhereClause2 <> '' AND @WhereClause2 NOT LIKE 'AND %'
	SET @WhereClause2 = N'AND ' + @WhereClause2


SELECT @DeferredCode = AcceptanceCodeKey FROM dbo.ATKAcceptanceCode WHERE AcceptanceCodeDesc = 'Deferred'
SELECT @AcceptedCode = AcceptanceCodeKey FROM dbo.ATKAcceptanceCode WHERE AcceptanceCodeDesc = 'Accepted'
SELECT @DeletedCode = AcceptanceCodeKey FROM dbo.ATKAcceptanceCode WHERE AcceptanceCodeDesc = 'Deleted'



-- set expiration date to the Friday before the effective date
SELECT @ExpirationDate = N'
	CASE DATEPART(dw, EffectiveDate)
		WHEN 1 THEN DATEADD(dd, -2, EffectiveDate)  -- day of week is 1 which means a Sunday; go back two days to Friday
		WHEN 2 THEN DATEADD(dd, -3, EffectiveDate)
		WHEN 3 THEN DATEADD(dd, -4, EffectiveDate)
		WHEN 4 THEN DATEADD(dd, -5, EffectiveDate)
		WHEN 5 THEN DATEADD(dd, -6, EffectiveDate)
		WHEN 6 THEN DATEADD(dd, -7, EffectiveDate) -- a Friday, go back a week
		WHEN 7 THEN DATEADD(dd, -1, EffectiveDate)
		ELSE NULL
	END 
	'



--SELECT @NextFridayDate = 
--	CASE DATEPART(dw, GETDATE())
--		WHEN 1 THEN DATEADD(dd, 5, GETDATE())
--		WHEN 2 THEN DATEADD(dd, 4, GETDATE())
--		WHEN 3 THEN DATEADD(dd, 3, GETDATE())
--		WHEN 4 THEN DATEADD(dd, 2, GETDATE())
--		WHEN 5 THEN DATEADD(dd, 1, GETDATE())
--		WHEN 6 THEN DATEADD(dd, 7, GETDATE()) -- a friday, go back a week
--		WHEN 7 THEN DATEADD(dd, 6, GETDATE())
--		ELSE NULL
--	END

SELECT @NextFridayDate = 
	CASE DATEPART(dw, @TodayDate)
		WHEN 1 THEN DATEADD(dd, 5, @TodayDate)
		WHEN 2 THEN DATEADD(dd, 4, @TodayDate)
		WHEN 3 THEN DATEADD(dd, 3, @TodayDate)
		WHEN 4 THEN DATEADD(dd, 2, @TodayDate)
		WHEN 5 THEN DATEADD(dd, 1, @TodayDate)
		WHEN 6 THEN DATEADD(dd, 7, @TodayDate) -- a friday, go back a week
		WHEN 7 THEN DATEADD(dd, 6, @TodayDate)
		ELSE NULL
	END

SELECT @NextSaturdayDate = DATEADD(dd, 1, @NextFridayDate)


SELECT 
	@SuppressionTypeKey = SuppressionTypeKey,
	@ApproachTypeKey = ApproachTypeKey
FROM ATKScenario 
WHERE ScenarioKey = @ScenarioKey

-- for now, I am simply not going to create another rebate change proposal for an AIS if there is already one
-- Note the suppression logic is slightly different in pass 1 (proposal types 5 and 7) - no need to compare dates
IF (SELECT SuppressionTypeCode 
	FROM ATKSuppressionType 
	WHERE SuppressionTypeKey = @SuppressionTypeKey) = 'Proposal'
BEGIN
	SET @SuppressionExistsClause = 
	N'AND NOT EXISTS (
		SELECT 1 
		FROM dbo.ProposalPrice pp
		INNER JOIN dbo.PlaybookProject p
			ON p.ProjectKey = pp.ProjectKey
		WHERE 
			ProductionProjectIndicator = ''Y''
			AND ApproachTypeKey = '+CAST(@ApproachTypeKey AS NVARCHAR(25))+'
			AND pp.AccountKey = ProposalData1.AccountKey
			AND pp.ItemKey = ProposalData1.ItemKey
			AND pp.InvoiceLineGroup1Key = ProposalData1.InvoiceLineGroup1Key
			AND pp.FutureItemCost = ProposalData1.FutureItemCostInListUoM 
		)'
END
ELSE
	SET @SuppressionExistsClause = ''


SET @SQLStmt = N'
EXEC LogDCPEvent ''DCP --- PopulateRebateChangeProposal: Insert PlaybookRCProposal Pass 1'', ''B'', 0, 0, 0 ,'+CAST(@ProjectKey AS NVARCHAR(500))+'

TRUNCATE TABLE dbo.PlaybookRCProposal

; WITH ProposalData1 AS (
SELECT
--TOP 3
	FactLastPriceAndCost.AccountKey,
	FactLastPriceAndCost.ItemKey,
	FactLastPriceAndCost.InvoiceLineGroup1Key,
	DimAccount.AccountManagerKey,
	TotalCost,
	TotalPrice,
	TotalQuantity,
	Cnt AS TotalFrequency,
	LastItemCost,
	LastItemPrice ,
	CurrentItemPrice,
	CurrentItemCost,
	ISNULL((CurrentItemPrice - CurrentItemCost) / NULLIF(CurrentItemPrice, 0.), 0.) AS CurrentGPP,
	ISNULL(CurrentDiscount, 0.) AS CurrentDiscount,
	--UserCost,
	(ForecastedMonthlyQty*12.) AS Forecasted12MonthQuantity,
	LastSaleDayKey,
	ISNULL(LastInvoiceLineGroup2Key, 1) AS LastInvoiceLineGroup2Key,
	FactLastPriceAndCost.RebatedCommissionCost,
	CurrentPriceMethod,
	CASE
		WHEN ItemUDVarchar10 = ''1'' THEN FactLastPriceAndCost.UDVarchar6
		ELSE FactLastPriceAndCost.UDVarchar5
	END AS StrategicIndicator,
	FactLastPriceAndCost.UDVarchar7 AS CDIIndicator,
	FactCurrentPriceAndCost.UDVarchar1 AS CurrentPriceSource,
	--ListPrice,
	--ListPriceInListUoM,
	--ListPricingUoMConvFactor,
	--ListPricingUoM,
	--ISNULL((ListPrice * NewBaseCost) / StandardCost, ListPrice) AS FutureEstimatedListPrice, --JJJ
	RPBPlaybookPricingGroupKey,
	ISNULL(TheFloor.BandSequenceNumber, 1) AS BandSequenceNumber,
	ISNULL(o.FloorGPP, TheFloor.PDPGPP) AS FloorGPP,
	ISNULL(o.TargetGPP, TheTarget.PDPGPP) AS TargetGPP,
	ISNULL(o.StretchGPP, TheStretch.PDPGPP) AS StretchGPP,
	' + CAST(@DynamicUDVarchar1 AS NVARCHAR(MAX)) + N' AS ShadowGPP,
	junk.InvoiceLineJunkUDVarchar2 AS ContractNo,
	junk.InvoiceLineJunkUDVarchar6 AS RuleNo,
	junk.InvoiceLineJunkUDVarchar5 AS ContractEndDate,
	--DropShipIndicator,
	FactLastPriceAndCost.RebateId,
	RebateEndDate,
	RecentItemCost,
	RecentDiscount,
	RecentPriceDate,
	--StockStatus,
	ForecastedMonthlyQty,	
	
	FactLastPriceAndCost.RebatedCommissionCost AS RelevantCost,
	FactLastPriceAndCost.RebatedCommissionCost AS FutureItemCost,
	CAST(FactLastPriceAndCost.RebatedCommissionCost *  1 AS DECIMAL(19,8)) AS FutureItemCostInListUoM,
	CAST(''' + CONVERT(NVARCHAR(10), @TodayDate, 121) + N''' AS DATETIME) AS EffectiveDate,	
	FactLastPriceAndCost.RebateId AS FutureRebateID,
	FactLastPriceAndCost.RebateEndDate AS FutureRebateEndDate,
	--,
	--CASE
	--	WHEN FactRebateHistory.AccountKey IS NULL THEN 7	-- no rebate last week but there is one now
	--	ELSE 5												-- current week''s rebated commission cost different from last week''s rebated commission cost
	--END AS ProposalType,
	5 AS proposaltype
	--ISNULL(FactRebateHistory.RebatedCommissionCost, LastItemCost) AS HistoryCost	
	FROM dbo.FactLastPriceAndCost
	INNER JOIN dbo.DimAccount DimAccount
		ON DimAccount.AccountKey = FactLastPriceAndCost.AccountKey
	INNER JOIN dbo.DimAccountGroup1
		ON DimAccountGroup1.AccountGroup1Key = DimAccount.AccountGroup1Key
	INNER JOIN dbo.DimItem ON
		DimItem.ItemKey = FactLastPriceAndCost.ItemKey
	INNER JOIN dbo.DimInvoiceLineJunk junk
		ON junk.InvoiceLineJunkKey = LastInvoiceLineJunkKey
	INNER JOIN (
				SELECT 
					AccountKey, 
					ItemKey, 
					InvoiceLineGroup1Key,
					SUM(TotalQuantity) TotalQuantity,
					COUNT(*) Cnt,
					SUM(TotalActualCost) TotalCost,
					SUM(TotalActualPrice) TotalPrice
				FROM dbo.FactInvoiceLine fil 
				WHERE Last12MonthsIndicator = ''Y''
					AND InvoiceLineGroup1Key = 4
				GROUP BY 
					AccountKey, 
					ItemKey, 
					InvoiceLineGroup1Key
			) FILSUM
			ON FILSUM.AccountKey = FactLastPriceAndCost.AccountKey 
			AND FILSUM.ItemKey = FactLastPriceAndCost.ItemKey 
			AND FILSUM.InvoiceLineGroup1Key = FactLastPriceAndCost.InvoiceLineGroup1Key	
	INNER JOIN dbo.FactCurrentPriceAndCost
			ON FactCurrentPriceAndCost.AccountKey = FactLastPriceAndCost.AccountKey 
			AND FactCurrentPriceAndCost.ItemKey = FactLastPriceAndCost.ItemKey 
			AND FactCurrentPriceAndCost.InvoiceLineGroup1Key = FactLastPriceAndCost.InvoiceLineGroup1Key	
	LEFT JOIN FactSalesForecast (NOLOCK)
		ON FactSalesForecast.AccountKey = FactLastPriceAndCost.AccountKey 
		AND FactSalesForecast.ItemKey = FactLastPriceAndCost.ItemKey 
		AND FactSalesForecast.InvoiceLineGroup1Key = FactLastPriceAndCost.InvoiceLineGroup1Key 
		AND ForecastType = ''Last 12 Months Indicator''
	INNER JOIN dbo.ATKAccountGroup1ItemGroup1 ATKAccountGroup1ItemGroup1
		ON ATKAccountGroup1ItemGroup1.AccountGroup1Key = DimAccount.AccountGroup1Key
		AND ATKAccountGroup1ItemGroup1.ItemGroup1Key = DimItem.ItemGroup1Key
	LEFT JOIN dbo.PlaybookPricingGroupPriceBand TheFloor
		ON TheFloor.PlaybookPricingGroupKey = FactLastPriceAndCost.RPBPlaybookPricingGroupKey
		AND FILSUM.TotalQuantity BETWEEN TheFloor.LowerBandQuantity AND TheFloor.UpperBandQuantity 
		AND TheFloor.MemberRank = 
			CASE
				WHEN  ROUND ( TheFloor.RulingMemberCount * FloorPct, 0) BETWEEN 1 AND TheFloor.RulingMemberCount THEN  ROUND ( TheFloor.RulingMemberCount * FloorPct, 0)
				ELSE 1
			END
	LEFT JOIN dbo.PlaybookPricingGroupPriceBand TheTarget
		ON TheTarget.PlaybookPricingGroupKey = FactLastPriceAndCost.RPBPlaybookPricingGroupKey
		AND FILSUM.TotalQuantity BETWEEN TheTarget.LowerBandQuantity AND TheTarget.UpperBandQuantity 
		AND TheTarget.MemberRank = 
			CASE
				WHEN  ROUND ( TheTarget.RulingMemberCount * TargetPct, 0) BETWEEN 1 AND TheTarget.RulingMemberCount THEN  ROUND ( TheTarget.RulingMemberCount * TargetPct, 0)
				ELSE 1
			END
	LEFT JOIN dbo.PlaybookPricingGroupPriceBand TheStretch
		ON TheStretch.PlaybookPricingGroupKey = FactLastPriceAndCost.RPBPlaybookPricingGroupKey
		AND FILSUM.TotalQuantity BETWEEN TheStretch.LowerBandQuantity AND TheStretch.UpperBandQuantity 
		AND TheStretch.MemberRank = 
			CASE
				WHEN  ROUND ( TheStretch.RulingMemberCount * StretchPct, 0) BETWEEN 1 AND TheStretch.RulingMemberCount THEN  ROUND ( TheStretch.RulingMemberCount * StretchPct, 0)
				ELSE 1
			END
	LEFT JOIN dbo.ManualPriceBandOverride o
		ON DimItem.ItemVendorKey = o.VendorKey
		AND DimItem.ItemGroup1Key = o.ItemGroup1Key
		AND FactLastPriceAndCost.InvoiceLineGroup1Key = o.InvoiceLineGroup1Key
	WHERE
		FactLastPriceAndCost.InvoiceLineGroup1Key = 4
		AND Total12MonthQuantity > 0.
		AND LastItemPrice >= 0.
		AND LastItemCost >= 0.
		AND CurrentItemCost <> 0.
		--AND UserCost <> 0.
		AND ISNULL((LastItemPrice - LastItemCost) / NULLIF(LastItemPrice, 0.), 0.) <> 0.
		AND CurrentItemPrice IS NOT NULL
		--AND FactLastPriceAndCost.RebatedCommissionCost <> ISNULL(FactRebateHistory.RebatedCommissionCost, 0.)  -- guardrail applied below
), ProposalData2 AS (
	SELECT
		ProposalData1.AccountKey,
		ProposalData1.ItemKey,
		ProposalData1.InvoiceLineGroup1Key,
		AccountManagerKey,
		TotalCost,
		TotalPrice,
		TotalQuantity,
		TotalFrequency,
		LastItemCost,
		LastItemPrice ,
		CurrentItemPrice,
		CurrentItemCost,
		CurrentGPP,
		CurrentDiscount,
		--UserCost,
		Forecasted12MonthQuantity,
		LastSaleDayKey,
		LastInvoiceLineGroup2Key,
		RebatedCommissionCost,
		CurrentPriceMethod,
		StrategicIndicator,
		CDIIndicator,
		CurrentPriceSource,
		--ListPrice,
		--ListPriceInListUoM,
		--ListPricingUoMConvFactor,
		--ListPricingUoM,
		--FutureEstimatedListPrice,
		RPBPlaybookPricingGroupKey,
		BandSequenceNumber,
		FloorGPP,
		TargetGPP,
		StretchGPP,
		ShadowGPP,
		ContractNo,
		RuleNo,
		ContractEndDate,
		--DropShipIndicator,
		RebateId,
		RebateEndDate,
		RecentItemCost,
		RecentDiscount,
		RecentPriceDate,
		--StockStatus,
		ForecastedMonthlyQty,	
		RelevantCost,
		ProposalData1.FutureItemCost,
		FutureItemCostInListUoM,
		EffectiveDate,
		FutureRebateID,
		FutureRebateEndDate,
		ProposalType,
		RelevantCost / (1. - TargetGPP) AS NewPrice,
		--CASE
		--	WHEN CurrentItemPrice = 0.
		--		THEN 0.
		--	WHEN CurrentGPP <= 0. AND ISNULL(TargetGPP, 0.) > 0.
		--		THEN RelevantCost / (1. - TargetGPP)
		--	WHEN (CurrentItemCost >= RelevantCost) -- hold price
		--		THEN CurrentItemPrice
		--	ELSE
		--		RelevantCost / (1. - dbo.fn_GetGPPWithShadowIncrease (StrategicIndicator, CurrentGPP, ShadowGPP, FloorGPP, TargetGPP, StretchGPP))
		--END AS NewPrice,
		
		GETDATE() AS ExpirationDate
	FROM ProposalData1
	WHERE 
		RelevantCost <> 0.
		AND ProposalData1.FutureItemCost IS NOT NULL
		AND ProposalData1.FutureItemCost >= 0.
		AND ISNULL((ProposalData1.FutureItemCost - LastItemCost) / NULLIF(LastItemCost, 0.), 0.) BETWEEN -0.50 AND 1.00  -- guardrails per http://enterbridgedc:8080/browse/BPB-994 
		AND ISNULL((ProposalData1.FutureItemCost - CurrentItemCost) / NULLIF(CurrentItemCost, 0.), 0.) BETWEEN -0.50 AND 1.00  -- guardrails
		--AND NOT ISNULL(ABS(1. - (HistoryCost/NULLIF(ProposalData1.FutureItemCost, 0.))), 0) <= .0049	--  BPB-3715; (BPB-3715 Do not create a proposal if the cost change is less than 0.49%)
		' + @SuppressionExistsClause + N'
)
INSERT INTO PlaybookRCProposal (
	AccountKey,
	ItemKey,
	InvoiceLineGroup1Key,
	AccountManagerKey,
	TotalCost,
	TotalPrice,
	TotalQuantity,
	TotalFrequency,
	LastItemCost,
	LastItemPrice,
	LastSaleDayKey,
	LastInvoiceLineGroup2Key,
	CurrentItemCost,
	CurrentItemPrice,
	CurrentGPP,
	CurrentDiscount,
	--UserCost,
	Forecasted12MonthQuantity,
	RebatedCommissionCost,
	CurrentPriceMethod,
	StrategicIndicator,
	CDIIndicator,
	CurrentPriceSource,
	--ListPrice,
	--ListPriceInListUoM,
	--FutureEstimatedListPrice,
	--ListPricingUoMConvFactor,
	--ListPricingUoM,
	RPBPlaybookPricingGroupKey,
	BandSequenceNumber,
	FloorGPP,
	TargetGPP,
	StretchGPP,
	ShadowGPP,
	ContractNo,
	RuleNo,
	ContractEndDate,
	--DropShipIndicator,
	RebateId,
	RebateEndDate,
	RecentItemCost,
	RecentDiscount,
	RecentPriceDate,
	--StockStatus,
	ForecastedMonthlyQty,
	RelevantCost,
	FutureItemCost,
	FutureItemCostInListUoM,
	EffectiveDate,
	CostEffectiveDate,
	ExpirationDate,
	FutureRebateId,
	FutureRebateEndDate,
	ProposalType,
	NewPrice
)
SELECT
	AccountKey,
	ItemKey,
	InvoiceLineGroup1Key,
	AccountManagerKey,
	TotalCost,
	TotalPrice,
	TotalQuantity,
	TotalFrequency,
	LastItemCost,
	LastItemPrice,
	LastSaleDayKey,
	LastInvoiceLineGroup2Key,
	CurrentItemCost,
	CurrentItemPrice,
	CurrentGPP,
	CurrentDiscount,
	--UserCost,
	Forecasted12MonthQuantity,
	RebatedCommissionCost,
	CurrentPriceMethod,
	StrategicIndicator,
	CDIIndicator,
	CurrentPriceSource,
	--ListPrice,
	--ListPriceInListUoM,
	--FutureEstimatedListPrice,
	--ListPricingUoMConvFactor,
	--ListPricingUoM,
	RPBPlaybookPricingGroupKey,
	BandSequenceNumber,
	FloorGPP,
	TargetGPP,
	StretchGPP,
	ShadowGPP,
	ContractNo,
	RuleNo,
	ContractEndDate,
	--DropShipIndicator,
	RebateId,
	RebateEndDate,
	RecentItemCost,
	RecentDiscount,
	RecentPriceDate,
	--StockStatus,
	ForecastedMonthlyQty,
	RelevantCost,
	FutureItemCost,
	FutureItemCostInListUoM,
	EffectiveDate,
	EffectiveDate,
	ExpirationDate,
	FutureRebateId,
	FutureRebateEndDate,
	ProposalType,
	NewPrice
FROM ProposalData2
WHERE 
	NewPrice >= 0.
	AND EffectiveDate <= DATEADD(MONTH, 24, CAST(''' + CONVERT(NVARCHAR(10), @TodayDate, 121) + N''' AS DATETIME))

EXEC LogDCPEvent ''DCP --- PopulateRebateChangeProposal: Insert PlaybookRCProposal Pass 1'', ''E'', @@ROWCOUNT, 0, 0 ,'+CAST(@ProjectKey AS NVARCHAR(500))+'

'

RAISERROR ('Insert PlaybookRCProposal sql statement... ', 0, 1) WITH NOWAIT 
EXEC dbo.PrintNVarCharMax @SQLStmt
RAISERROR ('---', 0, 1) WITH NOWAIT 
EXEC (@SQLStmt)


-- for now, I am simply not going to create another rebate change proposal for an AIS if there is already one
IF (SELECT SuppressionTypeCode 
	FROM ATKSuppressionType 
	WHERE SuppressionTypeKey = @SuppressionTypeKey) = 'Proposal'
BEGIN

	SET @SuppressionExistsClause = 
	'AND NOT EXISTS (
		SELECT 1 
		FROM dbo.ProposalPrice pp
		INNER JOIN dbo.PlaybookProject p
			ON p.ProjectKey = pp.ProjectKey
		WHERE 
			ProductionProjectIndicator = ''Y''
			AND ApproachTypeKey = '+CAST(@ApproachTypeKey AS NVARCHAR(25))+'
			AND pp.AccountKey = ProposalData1.AccountKey
			AND pp.ItemKey = ProposalData1.ItemKey
			AND pp.InvoiceLineGroup1Key = ProposalData1.InvoiceLineGroup1Key
			AND pp.CostEffectiveDate = ProposalData1.EffectiveDate 
			AND pp.FutureItemCost = ProposalData1.FutureItemCostInListUoM
		)'
END
ELSE
	SET @SuppressionExistsClause = ''

SET @SQLStmt = N'

EXEC LogDCPEvent ''DCP --- PopulateRebateChangeProposal: Insert PlaybookRCProposal Pass 2'', ''B'', 0, 0, 0 ,'+CAST(@ProjectKey AS NVARCHAR(25))+N'

; WITH ProposalData1 AS (
SELECT
--TOP 3
	FactLastPriceAndCost.AccountKey,
	FactLastPriceAndCost.ItemKey,
	FactLastPriceAndCost.InvoiceLineGroup1Key,
	DimAccount.AccountManagerKey,
	FILSUM.TotalCost,
	FILSUM.TotalPrice,
	FILSUM.TotalQuantity,
	Cnt AS TotalFrequency,
	FactLastPriceAndCost.LastItemCost,
	FactLastPriceAndCost.LastItemPrice,
	FactCurrentPriceAndCost.CurrentItemPrice,
	FactCurrentPriceAndCost.CurrentItemCost,
	ISNULL((FactCurrentPriceAndCost.CurrentItemPrice - FactCurrentPriceAndCost.CurrentItemCost) / NULLIF(FactCurrentPriceAndCost.CurrentItemPrice, 0.), 0.) AS CurrentGPP,
	ISNULL(FactCurrentPriceAndCost.CurrentDiscount, 0.) AS CurrentDiscount,
	--ag3i.UserCost,
	(FactSalesForecast.ForecastedMonthlyQty*12.) AS Forecasted12MonthQuantity,
	FactLastPriceAndCost.LastSaleDayKey,
	ISNULL(FactLastPriceAndCost.LastInvoiceLineGroup2Key, 1) AS LastInvoiceLineGroup2Key,
	FactLastPriceAndCost.RebatedCommissionCost,
	FactCurrentPriceAndCost.CurrentPriceMethod,
	CASE
		WHEN ItemUDVarchar10 = ''1'' THEN FactLastPriceAndCost.UDVarchar6
		ELSE FactLastPriceAndCost.UDVarchar5
	END AS StrategicIndicator,
	FactLastPriceAndCost.UDVarchar7 AS CDIIndicator,
	FactCurrentPriceAndCost.UDVarchar1 AS CurrentPriceSource,
	--ag3i.ListPrice,
	--ag3i.ListPriceInListUoM,
	--ag3i.ListPricingUoMConvFactor,
	--ag3i.ListPricingUoM,
	--ISNULL((ag3i.ListPrice * NewBaseCost) / NULLIF(StandardCost, 0.), ag3i.ListPrice) AS FutureEstimatedListPrice, --JJJ
	FactLastPriceAndCost.RPBPlaybookPricingGroupKey,
	ISNULL(TheFloor.BandSequenceNumber, 1) AS BandSequenceNumber,
	ISNULL(o.FloorGPP, TheFloor.PDPGPP) AS FloorGPP,
	ISNULL(o.TargetGPP, TheTarget.PDPGPP) AS TargetGPP,
	ISNULL(o.StretchGPP, TheStretch.PDPGPP) AS StretchGPP,
	' + CAST(@DynamicUDVarchar1 AS NVARCHAR(MAX)) + N' AS ShadowGPP,
	junk.InvoiceLineJunkUDVarchar2 AS ContractNo,
	junk.InvoiceLineJunkUDVarchar6 AS RuleNo,
	junk.InvoiceLineJunkUDVarchar5 AS ContractEndDate,
	--ag3i.DropShipIndicator,
	FactLastPriceAndCost.RebateId,
	FactLastPriceAndCost.RebateEndDate,
	FactCurrentPriceAndCost.RecentItemCost,
	FactCurrentPriceAndCost.RecentDiscount,
	FactCurrentPriceAndCost.RecentPriceDate,
	--ag3i.StockStatus,
	FactSalesForecast.ForecastedMonthlyQty,
	0 AS RelevantCost, -- convert back to base
	0 AS FutureItemCost, -- convert back to base
	0 AS FutureItemCostInListUoM,
		
	--dbo.fn_GetRebatedCommissionCost(
	--	NewCommissionCost / CostConv.ConversionFactor,  -- must send in values expressed in CostUoM
	--	NewBaseCost / CostConv.ConversionFactor,
	--	CommissionCostCode,
	--	CommissionCostAdapted,
	--	RebateCode,
	--	RebateCostAdapted
	--) * CostConv.ConversionFactor AS RelevantCost, -- convert back to base
	--dbo.fn_GetRebatedCommissionCost(
	--	NewCommissionCost / CostConv.ConversionFactor,  -- must send in values expressed in CostUoM
	--	NewBaseCost / CostConv.ConversionFactor,
	--	CommissionCostCode,
	--	CommissionCostAdapted,
	--	RebateCode,
	--	RebateCostAdapted
	--) * CostConv.ConversionFactor AS FutureItemCost, -- convert back to base
	--(
	--dbo.fn_GetRebatedCommissionCost(
	--	NewCommissionCost / CostConv.ConversionFactor,  -- must send in values expressed in CostUoM
	--	NewBaseCost / CostConv.ConversionFactor,
	--	CommissionCostCode,
	--	CommissionCostAdapted,
	--	RebateCode,
	--	RebateCostAdapted
	--) * CostConv.ConversionFactor) * ISNULL(ag3i.ListPricingUoMConvFactor, 1.) AS FutureItemCostInListUoM,

	--	CASE
	--		WHEN FactRebate.CommissionCostCode = ''%'' THEN 
	--			CASE FactRebate.RebateCode
	--				WHEN ''F'' THEN 
	--						FactRebate.RebateCost * CAST(ISNULL((NewCommissionCost / NULLIF(StandardCost, 0.0)), 1.0) AS NUMERIC(19,10))
							
	--				WHEN ''$'' THEN NewCommissionCost - FactRebate.RebateCost
	--				WHEN ''%'' THEN NewCommissionCost * (1.0 - (CAST(FactRebate.RebateCost AS DEC(19,8)) / 100.0))
	--				ELSE NULL
	--			END	* (1.0 + CAST(FactRebate.CommissionCost AS DEC(12,5))/100.0)
	--		WHEN FactRebate.CommissionCostCode = ''$'' THEN 
	--			CASE FactRebate.RebateCode
	--				WHEN ''F'' THEN RebateCost
	--				WHEN ''$'' THEN NewCommissionCost - RebateCost
	--				WHEN ''%'' THEN NewCommissionCost * (1.0 - (CAST(RebateCost AS DEC(19,8)) / 100.0))
	--				ELSE NULL
	--			END	+ CAST(FactRebate.CommissionCost AS DEC(12,5))
	--		WHEN FactRebate.CommissionCostCode = ''F'' THEN 
	--			FactRebate.CommissionCost					
	--		ELSE -- no commission cost override; RebatedStandardCost * StandardCommissionLoadPercent
	--			CASE FactRebate.RebateCode
	--				WHEN ''F'' THEN FactRebate.RebateCost
	--				WHEN ''$'' THEN StandardCost - FactRebate.RebateCost
	--				WHEN ''%'' THEN StandardCost * (1.0 - (CAST(FactRebate.RebateCost AS DEC(19,8)) / 100.0))
	--				ELSE NULL
	--			END * CAST(ISNULL((NewCommissionCost / NULLIF(StandardCost, 0.0)), 1.0) AS NUMERIC(19,10))
	--	END
	--AS FutureItemCost,
	
	--FutureCostChangeDate AS EffectiveDate,
	GETDATE() AS EffectiveDate,
	FactLastPriceAndCost.RebateId AS FutureRebateID,
	FactLastPriceAndCost.RebateEndDate AS FutureRebateEndDate,
	6 AS ProposalType
	
	FROM dbo.FactLastPriceAndCost
	INNER JOIN dbo.DimAccount DimAccount
		ON DimAccount.AccountKey = FactLastPriceAndCost.AccountKey
	INNER JOIN dbo.DimAccountGroup1
		ON DimAccountGroup1.AccountGroup1Key = DimAccount.AccountGroup1Key
	INNER JOIN dbo.DimItem ON
		DimItem.ItemKey = FactLastPriceAndCost.ItemKey
	INNER JOIN dbo.DimInvoiceLineJunk junk
		ON junk.InvoiceLineJunkKey = LastInvoiceLineJunkKey
	INNER JOIN (
				SELECT 
					AccountKey, 
					ItemKey, 
					InvoiceLineGroup1Key,
					SUM(TotalQuantity) TotalQuantity,
					COUNT(*) Cnt,
					SUM(TotalActualCost) TotalCost,
					SUM(TotalActualPrice) TotalPrice
				FROM dbo.FactInvoiceLine fil 
				WHERE Last12MonthsIndicator = ''Y''
					AND InvoiceLineGroup1Key = 4 
				GROUP BY 
					AccountKey, 
					ItemKey, 
					InvoiceLineGroup1Key
			) FILSUM
			ON FILSUM.AccountKey = FactLastPriceAndCost.AccountKey 
			AND FILSUM.ItemKey = FactLastPriceAndCost.ItemKey 
			AND FILSUM.InvoiceLineGroup1Key = FactLastPriceAndCost.InvoiceLineGroup1Key	
	INNER JOIN dbo.FactCurrentPriceAndCost
			ON FactCurrentPriceAndCost.AccountKey = FactLastPriceAndCost.AccountKey 
			AND FactCurrentPriceAndCost.ItemKey = FactLastPriceAndCost.ItemKey 
			AND FactCurrentPriceAndCost.InvoiceLineGroup1Key = FactLastPriceAndCost.InvoiceLineGroup1Key	
	LEFT JOIN FactSalesForecast (NOLOCK)
		ON FactSalesForecast.AccountKey = FactLastPriceAndCost.AccountKey 
		AND FactSalesForecast.ItemKey = FactLastPriceAndCost.ItemKey 
		AND FactSalesForecast.InvoiceLineGroup1Key = FactLastPriceAndCost.InvoiceLineGroup1Key 
		AND ForecastType = ''Last 12 Months Indicator''
	INNER JOIN dbo.ATKAccountGroup1ItemGroup1 ATKAccountGroup1ItemGroup1
		ON ATKAccountGroup1ItemGroup1.AccountGroup1Key = DimAccount.AccountGroup1Key
		AND ATKAccountGroup1ItemGroup1.ItemGroup1Key = DimItem.ItemGroup1Key
	LEFT JOIN dbo.PlaybookPricingGroupPriceBand TheFloor
		ON TheFloor.PlaybookPricingGroupKey = FactLastPriceAndCost.RPBPlaybookPricingGroupKey
		AND FILSUM.TotalQuantity BETWEEN TheFloor.LowerBandQuantity AND TheFloor.UpperBandQuantity 
		AND TheFloor.MemberRank = 
			CASE
				WHEN  ROUND ( TheFloor.RulingMemberCount * FloorPct, 0) BETWEEN 1 AND TheFloor.RulingMemberCount THEN  ROUND ( TheFloor.RulingMemberCount * FloorPct, 0)
				ELSE 1
			END
	LEFT JOIN dbo.PlaybookPricingGroupPriceBand TheTarget
		ON TheTarget.PlaybookPricingGroupKey = FactLastPriceAndCost.RPBPlaybookPricingGroupKey
		AND FILSUM.TotalQuantity BETWEEN TheTarget.LowerBandQuantity AND TheTarget.UpperBandQuantity 
		AND TheTarget.MemberRank = 
			CASE
				WHEN  ROUND ( TheTarget.RulingMemberCount * TargetPct, 0) BETWEEN 1 AND TheTarget.RulingMemberCount THEN  ROUND ( TheTarget.RulingMemberCount * TargetPct, 0)
				ELSE 1
			END
	LEFT JOIN dbo.PlaybookPricingGroupPriceBand TheStretch
		ON TheStretch.PlaybookPricingGroupKey = FactLastPriceAndCost.RPBPlaybookPricingGroupKey
		AND FILSUM.TotalQuantity BETWEEN TheStretch.LowerBandQuantity AND TheStretch.UpperBandQuantity 
		AND TheStretch.MemberRank = 
			CASE
				WHEN  ROUND ( TheStretch.RulingMemberCount * StretchPct, 0) BETWEEN 1 AND TheStretch.RulingMemberCount THEN  ROUND ( TheStretch.RulingMemberCount * StretchPct, 0)
				ELSE 1
			END
	LEFT JOIN dbo.ManualPriceBandOverride o
		ON DimItem.ItemVendorKey = o.VendorKey
		AND DimItem.ItemGroup1Key = o.ItemGroup1Key
		AND FactLastPriceAndCost.InvoiceLineGroup1Key = o.InvoiceLineGroup1Key
		
	--INNER JOIN FactRebate
	--	ON FactRebate.AccountKey = DimAccount.AccountKey 
	--	AND FactRebate.ItemKey = DimItem.ItemKey 
		
	--INNER JOIN ItemUOMConversion CostConv
	--	ON CostConv.ItemKey = DimItem.ItemKey
	--	AND CostConv.FromUOM = DimItem.BaseUnitOfMeasure
	--	AND CostConv.ToUOM = ag3i.CostUoM
	
	LEFT JOIN PlaybookRCProposal prp
		ON prp.AccountKey = FactLastPriceAndCost.AccountKey 
		AND prp.ItemKey = FactLastPriceAndCost.ItemKey 
		AND prp.InvoiceLineGroup1Key = FactLastPriceAndCost.InvoiceLineGroup1Key
	WHERE
		prp.AccountKey IS NULL -- check to see if AIS included in an earlier pass
		AND FactLastPriceAndCost.InvoiceLineGroup1Key = 4
		AND Total12MonthQuantity > 0.
		AND FactLastPriceAndCost.LastItemPrice >= 0.
		AND FactLastPriceAndCost.LastItemCost >= 0.
		AND FactCurrentPriceAndCost.CurrentItemCost <> 0.
		--AND ag3i.UserCost <> 0.
		AND ISNULL((FactLastPriceAndCost.LastItemPrice - FactLastPriceAndCost.LastItemCost) / NULLIF(FactLastPriceAndCost.LastItemPrice, 0.), 0.) <> 0.
		AND FactCurrentPriceAndCost.CurrentItemPrice IS NOT NULL

		-- the rebated commission cost is based on the current commission cost (itbal.usercost) and there a new comission cost pending
		--AND NewComFirstOccurrenceIndicator = ''N''
		--AND NewCommissionCost IS NOT NULL
		--AND NewCommissionCost > 0.
		--AND (
		--	(FactRebate.CommissionCostCode = ''%'' AND FactRebate.RebateCode = ''F'')
		--	OR (FactRebate.CommissionCostCode = ''%'' AND FactRebate.RebateCode = ''$'')
		--	OR (FactRebate.CommissionCostCode = ''%'' AND FactRebate.RebateCode = ''%'')
			
		--	OR (FactRebate.CommissionCostCode = ''$'' AND FactRebate.RebateCode = ''$'')
		--	OR (FactRebate.CommissionCostCode = ''$'' AND FactRebate.RebateCode = ''%'')

		--	OR (FactRebate.CommissionCostCode = '''')
		--)
), ProposalData2 AS (
	SELECT
		ProposalData1.AccountKey,
		ProposalData1.ItemKey,
		ProposalData1.InvoiceLineGroup1Key,
		AccountManagerKey,
		TotalCost,
		TotalPrice,
		TotalQuantity,
		TotalFrequency,
		LastItemCost,
		LastItemPrice ,
		CurrentItemPrice,
		CurrentItemCost,
		CurrentGPP,
		CurrentDiscount,
		--UserCost,
		Forecasted12MonthQuantity,
		LastSaleDayKey,
		LastInvoiceLineGroup2Key,
		RebatedCommissionCost,
		CurrentPriceMethod,
		StrategicIndicator,
		CDIIndicator,
		CurrentPriceSource,
		--ListPrice,
		--ListPriceInListUoM,
		--ListPricingUoMConvFactor,
		--ListPricingUoM,
		--FutureEstimatedListPrice,
		RPBPlaybookPricingGroupKey,
		BandSequenceNumber,
		FloorGPP,
		TargetGPP,
		StretchGPP,
		ShadowGPP,
		ContractNo,
		RuleNo,
		ContractEndDate,
		--DropShipIndicator,
		RebateId,
		RebateEndDate,
		RecentItemCost,
		RecentDiscount,
		RecentPriceDate,
		--StockStatus,
		ForecastedMonthlyQty,	
		RelevantCost,
		ProposalData1.FutureItemCost,
		FutureItemCostInListUoM,
		EffectiveDate,
		FutureRebateID,
		FutureRebateEndDate,
		ProposalType,
		RelevantCost / (1. - TargetGPP) AS NewPrice,
		--CASE
		--	WHEN CurrentItemPrice = 0.
		--		THEN 0.
		--	WHEN CurrentGPP <= 0. AND ISNULL(TargetGPP, 0.) > 0.
		--		THEN RelevantCost / (1. - TargetGPP)
		--	WHEN (CurrentItemCost >= RelevantCost) -- hold price
		--		THEN CurrentItemPrice
		--	ELSE
		--		RelevantCost / (1. - dbo.fn_GetGPPWithShadowIncrease (StrategicIndicator, CurrentGPP, ShadowGPP, FloorGPP, TargetGPP, StretchGPP))
		--END AS NewPrice,
		' + @ExpirationDate + N' AS ExpirationDate
	FROM ProposalData1
	WHERE 
		RelevantCost <> 0.
		AND ProposalData1.FutureItemCost IS NOT NULL
		AND ProposalData1.FutureItemCost >= 0.
		AND ISNULL((ProposalData1.FutureItemCost - LastItemCost) / NULLIF(LastItemCost, 0.), 0.) BETWEEN -0.50 AND 1.00  -- guardrails per http://enterbridgedc:8080/browse/BPB-994 
		AND ISNULL((ProposalData1.FutureItemCost - CurrentItemCost) / NULLIF(CurrentItemCost, 0.), 0.) BETWEEN -0.50 AND 1.00  -- guardrails
		AND NOT ISNULL(ABS(1. - (RebatedCommissionCost/NULLIF(ProposalData1.FutureItemCost, 0.))), 0) <= .0049	--  BPB-3715; (BPB-3715 Do not create a proposal if the cost change is less than 0.49%)
		' + @SuppressionExistsClause + N'
)
INSERT INTO PlaybookRCProposal (
	AccountKey,
	ItemKey,
	InvoiceLineGroup1Key,
	AccountManagerKey,
	TotalCost,
	TotalPrice,
	TotalQuantity,
	TotalFrequency,
	LastItemCost,
	LastItemPrice,
	LastSaleDayKey,
	LastInvoiceLineGroup2Key,
	CurrentItemCost,
	CurrentItemPrice,
	CurrentGPP,
	CurrentDiscount,
	--UserCost,
	Forecasted12MonthQuantity,
	RebatedCommissionCost,
	CurrentPriceMethod,
	StrategicIndicator,
	CDIIndicator,
	CurrentPriceSource,
	--ListPrice,
	--ListPriceInListUoM,
	--FutureEstimatedListPrice,
	--ListPricingUoMConvFactor,
	--ListPricingUoM,
	RPBPlaybookPricingGroupKey,
	BandSequenceNumber,
	FloorGPP,
	TargetGPP,
	StretchGPP,
	ShadowGPP,
	ContractNo,
	RuleNo,
	ContractEndDate,
	--DropShipIndicator,
	RebateId,
	RebateEndDate,
	RecentItemCost,
	RecentDiscount,
	RecentPriceDate,
	--StockStatus,
	ForecastedMonthlyQty,
	RelevantCost,
	FutureItemCost,
	FutureItemCostInListUoM,
	EffectiveDate,
	CostEffectiveDate,
	ExpirationDate,
	FutureRebateId,
	FutureRebateEndDate,
	ProposalType,
	NewPrice
)
SELECT
	AccountKey,
	ItemKey,
	InvoiceLineGroup1Key,
	AccountManagerKey,
	TotalCost,
	TotalPrice,
	TotalQuantity,
	TotalFrequency,
	LastItemCost,
	LastItemPrice,
	LastSaleDayKey,
	LastInvoiceLineGroup2Key,
	CurrentItemCost,
	CurrentItemPrice,
	CurrentGPP,
	CurrentDiscount,
	--UserCost,
	Forecasted12MonthQuantity,
	RebatedCommissionCost,
	CurrentPriceMethod,
	StrategicIndicator,
	CDIIndicator,
	CurrentPriceSource,
	--ListPrice,
	--ListPriceInListUoM,
	--FutureEstimatedListPrice,
	--ListPricingUoMConvFactor,
	--ListPricingUoM,
	RPBPlaybookPricingGroupKey,
	BandSequenceNumber,
	FloorGPP,
	TargetGPP,
	StretchGPP,
	ShadowGPP,
	ContractNo,
	RuleNo,
	ContractEndDate,
	--DropShipIndicator,
	RebateId,
	RebateEndDate,
	RecentItemCost,
	RecentDiscount,
	RecentPriceDate,
	--StockStatus,
	ForecastedMonthlyQty,
	RelevantCost,
	FutureItemCost,
	FutureItemCostInListUoM,
	EffectiveDate,
	EffectiveDate,
	ExpirationDate,
	FutureRebateId,
	FutureRebateEndDate,
	ProposalType,
	NewPrice
FROM ProposalData2
WHERE 
	NewPrice >= 0.
	AND EffectiveDate <= DATEADD(MONTH, 24, CAST(''' + CONVERT(NVARCHAR(10), @TodayDate, 121) + N''' AS DATETIME))

EXEC LogDCPEvent ''DCP --- PopulateRebateChangeProposal: Insert PlaybookRCProposal Pass 2'', ''E'', @@ROWCOUNT, 0, 0 ,'+CAST(@ProjectKey AS NVARCHAR(25))+N'

'

RAISERROR ('Insert PlaybookRCProposal sql statement... ', 0, 1) WITH NOWAIT 
EXEC dbo.PrintVarCharMax @SQLStmt
RAISERROR ('---', 0, 1) WITH NOWAIT 
EXEC (@SQLStmt)




SET @SQLStmt = N'

EXEC LogDCPEvent ''DCP --- PopulateRebateChangeProposal: Insert PlaybookRCProposal Pass 3'', ''B'', 0, 0, 0 ,'+CAST(@ProjectKey AS NVARCHAR(250))+'

DECLARE @MaxDate DATETIME
SELECT 	@MaxDate = SQLDate FROM dbo.DimDay WHERE DayKey = (SELECT MAX(InvoiceDateDayKey) FROM dbo.FactInvoiceLine)

; WITH ProposalData1 AS (
SELECT
--TOP 3
	FactLastPriceAndCost.AccountKey,
	FactLastPriceAndCost.ItemKey,
	FactLastPriceAndCost.InvoiceLineGroup1Key,
	DimAccount.AccountManagerKey,
	FILSUM.TotalCost,
	FILSUM.TotalPrice,
	FILSUM.TotalQuantity,
	Cnt AS TotalFrequency,
	FactLastPriceAndCost.LastItemCost,
	FactLastPriceAndCost.LastItemPrice ,
	FactCurrentPriceAndCost.CurrentItemPrice,
	FactCurrentPriceAndCost.CurrentItemCost,
	ISNULL((FactCurrentPriceAndCost.CurrentItemPrice - FactCurrentPriceAndCost.CurrentItemCost) / NULLIF(FactCurrentPriceAndCost.CurrentItemPrice, 0.), 0.) AS CurrentGPP,
	ISNULL(FactCurrentPriceAndCost.CurrentDiscount, 0.) AS CurrentDiscount,
	--ag3i.UserCost,
	(FactSalesForecast.ForecastedMonthlyQty*12.) AS Forecasted12MonthQuantity,
	FactLastPriceAndCost.LastSaleDayKey,
	ISNULL(FactLastPriceAndCost.LastInvoiceLineGroup2Key, 1) AS LastInvoiceLineGroup2Key,
	FactLastPriceAndCost.RebatedCommissionCost,
	FactCurrentPriceAndCost.CurrentPriceMethod,
	CASE
		WHEN ItemUDVarchar10 = ''1'' THEN FactLastPriceAndCost.UDVarchar6
		ELSE FactLastPriceAndCost.UDVarchar5
	END AS StrategicIndicator,
	FactLastPriceAndCost.UDVarchar7 AS CDIIndicator,
	FactCurrentPriceAndCost.UDVarchar1 AS CurrentPriceSource,
	--ag3i.ListPrice,
	--ag3i.ListPriceInListUoM,
	--ag3i.ListPricingUoMConvFactor,
	--ag3i.ListPricingUoM,
	--ISNULL((ag3i.ListPrice * NewBaseCost) / NULLIF(StandardCost, 0.), ag3i.ListPrice) AS FutureEstimatedListPrice, --JJJ
	FactLastPriceAndCost.RPBPlaybookPricingGroupKey,
	ISNULL(TheFloor.BandSequenceNumber, 1) AS BandSequenceNumber,
	ISNULL(o.FloorGPP, TheFloor.PDPGPP) AS FloorGPP,
	ISNULL(o.TargetGPP, TheTarget.PDPGPP) AS TargetGPP,
	ISNULL(o.StretchGPP, TheStretch.PDPGPP) AS StretchGPP,
	' + CAST(@DynamicUDVarchar1 AS NVARCHAR(MAX)) + N' AS ShadowGPP,
	junk.InvoiceLineJunkUDVarchar2 AS ContractNo,
	junk.InvoiceLineJunkUDVarchar6 AS RuleNo,
	junk.InvoiceLineJunkUDVarchar5 AS ContractEndDate,
	--ag3i.DropShipIndicator,
	FactLastPriceAndCost.RebateId,
	FactLastPriceAndCost.RebateEndDate,
	FactCurrentPriceAndCost.RecentItemCost,
	FactCurrentPriceAndCost.RecentDiscount,
	FactCurrentPriceAndCost.RecentPriceDate,
	--ag3i.StockStatus,
	FactSalesForecast.ForecastedMonthlyQty,	

	CASE
		WHEN RebateIdPreCurrentEnd IS NOT NULL THEN RebatedCommissionCostPreCurrentEnd	-- Proposal Type 1
		WHEN RebateIdPostCurrentEnd IS NOT NULL THEN RebatedCommissionCostPostCurrentEnd		-- Proposal Type 3
		--WHEN RebateIdPreCurrentEnd IS NULL AND RebateIdPostCurrentEnd IS NULL AND FactLastPriceAndCost.RebateId IS NOT NULL AND FactLastPriceAndCost.RebateEndDate <= DATEADD(DAY, 14, @MaxDate) THEN ag3i.UserCost	-- Proposal Type 4 - Rebate Expired
		ELSE 0.
	END AS RelevantCost,
	CASE
		WHEN RebateIdPreCurrentEnd IS NOT NULL THEN RebatedCommissionCostPreCurrentEnd	-- Proposal Type 1
		WHEN RebateIdPostCurrentEnd IS NOT NULL THEN RebatedCommissionCostPostCurrentEnd		-- Proposal Type 3
		--WHEN RebateIdPreCurrentEnd IS NULL AND RebateIdPostCurrentEnd IS NULL AND FactLastPriceAndCost.RebateId IS NOT NULL AND FactLastPriceAndCost.RebateEndDate <= DATEADD(DAY, 14, @MaxDate) THEN ag3i.UserCost	-- Proposal Type 4 - Rebate Expired
		ELSE 0.
	END AS FutureItemCost,
	--CAST(
		CASE
			WHEN RebateIdPreCurrentEnd IS NOT NULL THEN RebatedCommissionCostPreCurrentEnd	-- Proposal Type 1
			WHEN RebateIdPostCurrentEnd IS NOT NULL THEN RebatedCommissionCostPostCurrentEnd		-- Proposal Type 3
			--WHEN RebateIdPreCurrentEnd IS NULL AND RebateIdPostCurrentEnd IS NULL AND FactLastPriceAndCost.RebateId IS NOT NULL AND FactLastPriceAndCost.RebateEndDate <= DATEADD(DAY, 14, @MaxDate) THEN ag3i.UserCost	-- Proposal Type 4 - Rebate Expired
			ELSE 0.
		END
	*  1. AS FutureItemCostInListUoM,	
	CASE
		WHEN RebateIdPreCurrentEnd IS NOT NULL THEN RebateStartDatePreCurrentEnd	-- Proposal Type 1
		WHEN RebateIdPostCurrentEnd IS NOT NULL THEN FactLastPriceAndCost.RebateEndDate		-- Proposal Type 3
		WHEN RebateIdPreCurrentEnd IS NULL AND RebateIdPostCurrentEnd IS NULL AND FactLastPriceAndCost.RebateId IS NOT NULL AND FactLastPriceAndCost.RebateEndDate <= DATEADD(DAY, 14, @MaxDate) THEN FactLastPriceAndCost.RebateEndDate	-- Proposal Type 4 - Rebate Expired
		ELSE NULL
	END AS EffectiveDate,
	CASE
		WHEN RebateIdPreCurrentEnd IS NOT NULL THEN RebateIdPreCurrentEnd	-- Proposal Type 1
		WHEN RebateIdPostCurrentEnd IS NOT NULL THEN RebateIdPostCurrentEnd		-- Proposal Type 3
		WHEN RebateIdPreCurrentEnd IS NULL AND RebateIdPostCurrentEnd IS NULL AND FactLastPriceAndCost.RebateId IS NOT NULL AND FactLastPriceAndCost.RebateEndDate <= DATEADD(DAY, 14, @MaxDate) THEN NULL	-- Proposal Type 4 - Rebate Expired
		ELSE NULL
	END AS FutureRebateID,
	CASE
		WHEN RebateIdPreCurrentEnd IS NOT NULL THEN RebateEndDatePreCurrentEnd	-- Proposal Type 1
		WHEN RebateIdPostCurrentEnd IS NOT NULL THEN RebateEndDatePostCurrentEnd		-- Proposal Type 3
		WHEN RebateIdPreCurrentEnd IS NULL AND RebateIdPostCurrentEnd IS NULL AND FactLastPriceAndCost.RebateId IS NOT NULL AND FactLastPriceAndCost.RebateEndDate <= DATEADD(DAY, 14, @MaxDate) THEN NULL	-- Proposal Type 4 - Rebate Expired
		ELSE NULL
	END AS FutureRebateEndDate,
	CASE
		WHEN RebateIdPreCurrentEnd IS NOT NULL THEN 1	-- Proposal Type 1
		WHEN RebateIdPostCurrentEnd IS NOT NULL THEN 3		-- Proposal Type 3
		WHEN RebateIdPreCurrentEnd IS NULL AND RebateIdPostCurrentEnd IS NULL AND FactLastPriceAndCost.RebateId IS NOT NULL AND FactLastPriceAndCost.RebateEndDate <= DATEADD(DAY, 14, @MaxDate) THEN 4	-- Proposal Type 4 - Rebate Expired
		ELSE 0.
	END AS ProposalType
	FROM dbo.FactLastPriceAndCost
	INNER JOIN dbo.DimAccount DimAccount
		ON DimAccount.AccountKey = FactLastPriceAndCost.AccountKey
	INNER JOIN dbo.DimAccountGroup1
		ON DimAccountGroup1.AccountGroup1Key = DimAccount.AccountGroup1Key
	INNER JOIN dbo.DimItem ON
		DimItem.ItemKey = FactLastPriceAndCost.ItemKey
	INNER JOIN dbo.DimInvoiceLineJunk junk
		ON junk.InvoiceLineJunkKey = LastInvoiceLineJunkKey
	INNER JOIN (
				SELECT 
					AccountKey, 
					ItemKey, 
					InvoiceLineGroup1Key,
					SUM(TotalQuantity) TotalQuantity,
					COUNT(*) Cnt,
					SUM(TotalActualCost) TotalCost,
					SUM(TotalActualPrice) TotalPrice
				FROM dbo.FactInvoiceLine fil 
				WHERE Last12MonthsIndicator = ''Y''
					AND InvoiceLineGroup1Key = 4
				GROUP BY 
					AccountKey, 
					ItemKey, 
					InvoiceLineGroup1Key
			) FILSUM
			ON FILSUM.AccountKey = FactLastPriceAndCost.AccountKey 
			AND FILSUM.ItemKey = FactLastPriceAndCost.ItemKey 
			AND FILSUM.InvoiceLineGroup1Key = FactLastPriceAndCost.InvoiceLineGroup1Key	
	INNER JOIN dbo.FactCurrentPriceAndCost
			ON FactCurrentPriceAndCost.AccountKey = FactLastPriceAndCost.AccountKey 
			AND FactCurrentPriceAndCost.ItemKey = FactLastPriceAndCost.ItemKey 
			AND FactCurrentPriceAndCost.InvoiceLineGroup1Key = FactLastPriceAndCost.InvoiceLineGroup1Key	
	LEFT JOIN FactSalesForecast (NOLOCK)
		ON FactSalesForecast.AccountKey = FactLastPriceAndCost.AccountKey 
		AND FactSalesForecast.ItemKey = FactLastPriceAndCost.ItemKey 
		AND FactSalesForecast.InvoiceLineGroup1Key = FactLastPriceAndCost.InvoiceLineGroup1Key 
		AND ForecastType = ''Last 12 Months Indicator''
	INNER JOIN dbo.ATKAccountGroup1ItemGroup1 ATKAccountGroup1ItemGroup1
		ON ATKAccountGroup1ItemGroup1.AccountGroup1Key = DimAccount.AccountGroup1Key
		AND ATKAccountGroup1ItemGroup1.ItemGroup1Key = DimItem.ItemGroup1Key
	LEFT JOIN dbo.PlaybookPricingGroupPriceBand TheFloor
		ON TheFloor.PlaybookPricingGroupKey = FactLastPriceAndCost.RPBPlaybookPricingGroupKey
		AND FILSUM.TotalQuantity BETWEEN TheFloor.LowerBandQuantity AND TheFloor.UpperBandQuantity 
		AND TheFloor.MemberRank = 
			CASE
				WHEN  ROUND ( TheFloor.RulingMemberCount * FloorPct, 0) BETWEEN 1 AND TheFloor.RulingMemberCount THEN  ROUND ( TheFloor.RulingMemberCount * FloorPct, 0)
				ELSE 1
			END
	LEFT JOIN dbo.PlaybookPricingGroupPriceBand TheTarget
		ON TheTarget.PlaybookPricingGroupKey = FactLastPriceAndCost.RPBPlaybookPricingGroupKey
		AND FILSUM.TotalQuantity BETWEEN TheTarget.LowerBandQuantity AND TheTarget.UpperBandQuantity 
		AND TheTarget.MemberRank = 
			CASE
				WHEN  ROUND ( TheTarget.RulingMemberCount * TargetPct, 0) BETWEEN 1 AND TheTarget.RulingMemberCount THEN  ROUND ( TheTarget.RulingMemberCount * TargetPct, 0)
				ELSE 1
			END
	LEFT JOIN dbo.PlaybookPricingGroupPriceBand TheStretch
		ON TheStretch.PlaybookPricingGroupKey = FactLastPriceAndCost.RPBPlaybookPricingGroupKey
		AND FILSUM.TotalQuantity BETWEEN TheStretch.LowerBandQuantity AND TheStretch.UpperBandQuantity 
		AND TheStretch.MemberRank = 
			CASE
				WHEN  ROUND ( TheStretch.RulingMemberCount * StretchPct, 0) BETWEEN 1 AND TheStretch.RulingMemberCount THEN  ROUND ( TheStretch.RulingMemberCount * StretchPct, 0)
				ELSE 1
			END
	LEFT JOIN dbo.ManualPriceBandOverride o
		ON DimItem.ItemVendorKey = o.VendorKey
		AND DimItem.ItemGroup1Key = o.ItemGroup1Key
		AND FactLastPriceAndCost.InvoiceLineGroup1Key = o.InvoiceLineGroup1Key
	LEFT JOIN PlaybookRCProposal prp
		ON prp.AccountKey = FactLastPriceAndCost.AccountKey 
		AND prp.ItemKey = FactLastPriceAndCost.ItemKey 
		AND prp.InvoiceLineGroup1Key = FactLastPriceAndCost.InvoiceLineGroup1Key
	WHERE
		prp.AccountKey IS NULL -- check to see if AIS included in an earlier pass
		AND FactLastPriceAndCost.InvoiceLineGroup1Key = 4
		AND Total12MonthQuantity > 0.
		AND FactLastPriceAndCost.LastItemPrice >= 0.
		AND FactLastPriceAndCost.LastItemCost >= 0.
		AND FactCurrentPriceAndCost.CurrentItemCost <> 0.
		--AND ag3i.UserCost <> 0.
		AND ISNULL((FactLastPriceAndCost.LastItemPrice - FactLastPriceAndCost.LastItemCost) / NULLIF(FactLastPriceAndCost.LastItemPrice, 0.), 0.) <> 0.
		AND FactCurrentPriceAndCost.CurrentItemPrice IS NOT NULL
		AND FactLastPriceAndCost.RebateId IS NOT NULL
), ProposalData2 AS (
	SELECT
		ProposalData1.AccountKey,
		ProposalData1.ItemKey,
		ProposalData1.InvoiceLineGroup1Key,
		AccountManagerKey,
		TotalCost,
		TotalPrice,
		TotalQuantity,
		TotalFrequency,
		LastItemCost,
		LastItemPrice ,
		CurrentItemPrice,
		CurrentItemCost,
		CurrentGPP,
		CurrentDiscount,
		--UserCost,
		Forecasted12MonthQuantity,
		LastSaleDayKey,
		LastInvoiceLineGroup2Key,
		RebatedCommissionCost,
		CurrentPriceMethod,
		StrategicIndicator,
		CDIIndicator,
		CurrentPriceSource,
		--ListPrice,
		--ListPriceInListUoM,
		--ListPricingUoMConvFactor,
		--ListPricingUoM,
		--FutureEstimatedListPrice,
		RPBPlaybookPricingGroupKey,
		BandSequenceNumber,
		FloorGPP,
		TargetGPP,
		StretchGPP,
		ShadowGPP,
		ContractNo,
		RuleNo,
		ContractEndDate,
		--DropShipIndicator,
		RebateId,
		RebateEndDate,
		RecentItemCost,
		RecentDiscount,
		RecentPriceDate,
		--StockStatus,
		ForecastedMonthlyQty,	
		RelevantCost,
		ProposalData1.FutureItemCost,
		FutureItemCostInListUoM,
		EffectiveDate,
		FutureRebateID,
		FutureRebateEndDate,
		ProposalType,
		RelevantCost / (1. - TargetGPP) AS NewPrice,
		--CASE
		--	WHEN CurrentItemPrice = 0.
		--		THEN 0.
		--	WHEN CurrentGPP <= 0. AND ISNULL(TargetGPP, 0.) > 0.
		--		THEN RelevantCost / (1. - TargetGPP)
		--	WHEN (CurrentItemCost >= RelevantCost) -- hold price
		--		THEN CurrentItemPrice
		--	ELSE
		--		RelevantCost / (1. - dbo.fn_GetGPPWithShadowIncrease (StrategicIndicator, CurrentGPP, ShadowGPP, FloorGPP, TargetGPP, StretchGPP))
		--END AS NewPrice,
		' + @ExpirationDate + N' AS ExpirationDate
	FROM ProposalData1
	WHERE 
		RelevantCost <> 0.
		AND ProposalData1.FutureItemCost IS NOT NULL
		AND ProposalData1.FutureItemCost >= 0.
		AND ISNULL((ProposalData1.FutureItemCost - LastItemCost) / NULLIF(LastItemCost, 0.), 0.) BETWEEN -0.50 AND 1.00  -- guardrails per http://enterbridgedc:8080/browse/BPB-994 
		AND ISNULL((ProposalData1.FutureItemCost - CurrentItemCost) / NULLIF(CurrentItemCost, 0.), 0.) BETWEEN -0.50 AND 1.00  -- guardrails
		AND NOT ISNULL(ABS(1. - (RebatedCommissionCost/NULLIF(ProposalData1.FutureItemCost, 0.))), 0) <= .0049	--  BPB-3715; (BPB-3715 Do not create a proposal if the cost change is less than 0.49%)
		' + @SuppressionExistsClause + N'
)
INSERT INTO PlaybookRCProposal (
	AccountKey,
	ItemKey,
	InvoiceLineGroup1Key,
	AccountManagerKey,
	TotalCost,
	TotalPrice,
	TotalQuantity,
	TotalFrequency,
	LastItemCost,
	LastItemPrice,
	LastSaleDayKey,
	LastInvoiceLineGroup2Key,
	CurrentItemCost,
	CurrentItemPrice,
	CurrentGPP,
	CurrentDiscount,
	--UserCost,
	Forecasted12MonthQuantity,
	RebatedCommissionCost,
	CurrentPriceMethod,
	StrategicIndicator,
	CDIIndicator,
	CurrentPriceSource,
	--ListPrice,
	--ListPriceInListUoM,
	--FutureEstimatedListPrice,
	--ListPricingUoMConvFactor,
	--ListPricingUoM,
	RPBPlaybookPricingGroupKey,
	BandSequenceNumber,
	FloorGPP,
	TargetGPP,
	StretchGPP,
	ShadowGPP,
	ContractNo,
	RuleNo,
	ContractEndDate,
	--DropShipIndicator,
	RebateId,
	RebateEndDate,
	RecentItemCost,
	RecentDiscount,
	RecentPriceDate,
	--StockStatus,
	ForecastedMonthlyQty,
	RelevantCost,
	FutureItemCost,
	FutureItemCostInListUoM,
	EffectiveDate,
	CostEffectiveDate,
	ExpirationDate,
	FutureRebateId,
	FutureRebateEndDate,
	ProposalType,
	NewPrice
)
SELECT
	AccountKey,
	ItemKey,
	InvoiceLineGroup1Key,
	AccountManagerKey,
	TotalCost,
	TotalPrice,
	TotalQuantity,
	TotalFrequency,
	LastItemCost,
	LastItemPrice,
	LastSaleDayKey,
	LastInvoiceLineGroup2Key,
	CurrentItemCost,
	CurrentItemPrice,
	CurrentGPP,
	CurrentDiscount,
	--UserCost,
	Forecasted12MonthQuantity,
	RebatedCommissionCost,
	CurrentPriceMethod,
	StrategicIndicator,
	CDIIndicator,
	CurrentPriceSource,
	--ListPrice,
	--ListPriceInListUoM,
	--FutureEstimatedListPrice,
	--ListPricingUoMConvFactor,
	--ListPricingUoM,
	RPBPlaybookPricingGroupKey,
	BandSequenceNumber,
	FloorGPP,
	TargetGPP,
	StretchGPP,
	ShadowGPP,
	ContractNo,
	RuleNo,
	ContractEndDate,
	--DropShipIndicator,
	RebateId,
	RebateEndDate,
	RecentItemCost,
	RecentDiscount,
	RecentPriceDate,
	--StockStatus,
	ForecastedMonthlyQty,
	RelevantCost,
	FutureItemCost,
	FutureItemCostInListUoM,
	EffectiveDate,
	EffectiveDate,
	ExpirationDate,
	FutureRebateId,
	FutureRebateEndDate,
	ProposalType,
	NewPrice
FROM ProposalData2
WHERE 
	NewPrice >= 0.
	AND EffectiveDate <= DATEADD(MONTH, 24, CAST(''' + CONVERT(NVARCHAR(10), @TodayDate, 121) + N''' AS DATETIME))

EXEC LogDCPEvent ''DCP --- PopulateRebateChangeProposal: Insert PlaybookRCProposal Pass 3'', ''E'', @@ROWCOUNT, 0, 0 ,'+CAST(@ProjectKey AS NVARCHAR(500))+N'

'


RAISERROR ('Insert PlaybookRCProposal sql statement... ', 0, 1) WITH NOWAIT 
EXEC dbo.PrintVarCharMax @SQLStmt
RAISERROR ('---', 0, 1) WITH NOWAIT 
EXEC (@SQLStmt)




EXEC LogDCPEvent 'DCP --- PopulateRebateChangeProposal: Delete colliding vcm proposals in ProposalPrice', 'B', 0, 0, 0 , @ProjectKey

UPDATE pp 
	SET 
		pp.AcceptanceCodeKey = @DeletedCode,
		pp.ProposalPriceGroupKey = NULL,
		pp.ProposalPriceBuyGroupKey = NULL,
		pp.MaxStatus = 'D',
		pp.ModificationDate = GETDATE(),
		pp.ModificationUser = 'System Auto Delete - new proposal to be generated',
		pp.ModificationUserAddress = CAST(@ProjectKey AS NVARCHAR(25))
FROM dbo.ProposalPrice pp
JOIN dbo.PlaybookRCProposal new
	ON pp.AccountKey = new.AccountKey 
	AND pp.ItemKey = new.ItemKey 
	AND pp.InvoiceLineGroup1Key = new.InvoiceLineGroup1Key
INNER JOIN dbo.PlaybookProject p
	ON p.ProjectKey = pp.ProjectKey		
WHERE 
	(new.FutureItemCostInListUoM <> pp.FutureItemCost OR new.CostEffectiveDate <> pp.CostEffectiveDate)  -- something changed, commit
	AND pp.AcceptanceCodeKey <> @AcceptedCode
	AND pp.AcceptanceCodeKey <> @DeletedCode
	AND ProductionProjectIndicator = 'Y'	
SET @RowsU = @RowsU + @@ROWCOUNT

EXEC LogDCPEvent 'DCP --- PopulateRebateChangeProposal: Delete colliding vcm proposals in ProposalPrice', 'E', 0, @RowsU, 0, @ProjectKey



EXEC LogDCPEvent 'DCP --- PopulateRebateChangeProposal: Insert ProposalPrice', 'B', 0, 0, 0, @ProjectKey


--SELECT '@ProjectKey:',@ProjectKey
--SELECT '@PlaybookDataPointGroupKey:',@PlaybookDataPointGroupKey
--SELECT '@AcceptRejectCode_PriceProposal:',@AcceptRejectCode_PriceProposal
--SELECT '@DeferredCode:',@DeferredCode
--SELECT '@NextSaturdayDate:',@NextSaturdayDate
--SELECT '@NextFridayDate:',@NextFridayDate
--SELECT '@SuppressionDate:',@SuppressionDate
--SELECT '@ReasonCode:',@ReasonCode


INSERT dbo.ProposalPrice
(
	ProjectKey,
	PlaybookDataPointGroupKey,
	AccountKey,
	ItemKey,
	InvoiceLineGroup1Key,
	AccountManagerKey,
	AcceptanceCodeKey,
	UserAcceptanceCodeKey,
	DefaultAcceptanceCodeKey,
	UOM,
	NewUOM,
	UOMConversionFactor,
	ProposedPrice,
	NewPrice, 
	TotalActual12MonthCost,
	TotalActual12MonthPrice,
	TotalActual12MonthQuantity,
	TotalActual12MonthFrequency,
	LastItemCost,
	LastItemPrice,
	CurrentItemAISPrice,
	CurrentItemAISCost,
	--CurrentItemStandardCost,
	FutureItemCost,		
	Forecasted12MonthQuantity,
	AcceptanceStatusDate,
	LastSaleDayKey,
	LastInvoiceLineGroup2Key,
	EffectiveDate,
	SuppressionDate,
	ReasonCodeKey,
	UDDecimal1,
	UDDecimal2,
	UDDecimal3,
	UDVarchar1,
	UDVarchar2,
	UDVarchar3,
	UDVarchar4,
	ExpirationDate,
	CostEffectiveDate,
	NewGPP,
	--NewDiscount,
	--FutureEstimatedListPrice,
	--ListPriceInListUoM,
	--ListPricingUoMConvFactor,
	--ListPricingUoM,
	RPBPlaybookPricingGroupKey,
	BandSequenceNumber,
	ContractNo,
	RuleNo,
	ContractEndDate,
	--DropShipIndicator,
	--StockStatus,
	RebateId,
	RebateEndDate,
	RelevantCost,
	RecentItemCost,
	RecentItemPrice,
	RecentDiscount,
	RecentPriceDate,
	FloorGPP,
	TargetGPP,
	FutureRebateID,
	FutureRebateEndDate,
	ProposalType
)
SELECT
	@ProjectKey AS ProjectKey,
	@PlaybookDataPointGroupKey AS PlaybookDataPointGroupKey,
	AccountKey,
	ItemKey,
	InvoiceLineGroup1Key,
	AccountManagerKey,
	CAST(@AcceptRejectCode_PriceProposal AS SMALLINT) AS AcceptanceCodeKey,
	@DeferredCode AS UserAcceptanceCodeKey,
	CAST(@AcceptRejectCode_PriceProposal AS SMALLINT) AS DefaultAcceptanceCodeKey,
	ListPricingUoM AS UOM,
	ListPricingUoM AS NewUOM,
	1. AS UOMConversionFactor,
	NewPrice * 1 AS ProposedPrice,
	NewPrice * 1 AS NewPrice,
	TotalCost,
	TotalPrice,
	TotalQuantity / 1,
	TotalFrequency,
	LastItemCost * 1,
	LastItemPrice * 1,
	CurrentItemPrice * 1 AS CurrentItemAISPrice,
	CurrentItemCost * 1 AS CurrentItemAISCost,
	--UserCost * ISNULL(ListPricingUoMConvFactor, 1) AS CurrentItemStandardCost,
	FutureItemCostInListUoM,  -- FutureRebatedCommissionCost
	
	(ForecastedMonthlyQty*12) /  1 AS Forecasted12MonthQuantity,
	
	@TodayDate AS AcceptanceStatusDate,
	LastSaleDayKey,
	LastInvoiceLineGroup2Key,
	CASE
		WHEN ExpirationDate < @TodayDate THEN @NextSaturdayDate
		ELSE EffectiveDate
	END AS EffectiveDate,
	CAST(@SuppressionDate AS DATETIME) AS SuppressionDate,		
	CAST(NULLIF(@ReasonCode, 'NULL') AS SMALLINT) AS ReasonCodeKey,
	RebatedCommissionCost * 1 AS UDDecimal1,
	FutureItemCost * 1 AS UDDecimal2, -- FutureRebatedCommissionCost 
	CurrentDiscount AS UDDecimal3,
	CurrentPriceMethod AS UDVarchar1,
	StrategicIndicator AS UDVarchar2,
	CDIIndicator AS UDVarchar3,
	CurrentPriceSource AS UDVarchar4,
	CASE
		WHEN ExpirationDate < @TodayDate THEN @NextFridayDate
		ELSE ExpirationDate
	END AS ExpirationDate,
	EffectiveDate,
	ISNULL((NewPrice - RelevantCost) / NULLIF(NewPrice, 0.), 0.) AS NewGPP,
	--(FutureEstimatedListPrice - NewPrice) / NULLIF(FutureEstimatedListPrice, 0.) AS NewDiscount,
	--CAST(FutureEstimatedListPrice * ISNULL(ListPricingUoMConvFactor, 1) AS DEC(19,8)) AS FutureEstimatedListPrice,
	--ListPriceInListUoM,

	--ListPricingUoMConvFactor,
	--ListPricingUoM,
	
	RPBPlaybookPricingGroupKey,
	BandSequenceNumber,
	ContractNo,
	RuleNo,
	ContractEndDate,
	--DropShipIndicator,
	--StockStatus,
	RebateId,
	RebateEndDate,	
	RelevantCost * 1,
	RecentItemCost * 1,
	CurrentItemPrice * 1 AS RecentItemPrice,
	RecentDiscount,
	RecentPriceDate,
	FloorGPP,
	TargetGPP,
	FutureRebateId,
	FutureRebateEndDate,
	ProposalType
FROM PlaybookRCProposal
SET @RowsI = @RowsI + @@RowCount

EXEC LogDCPEvent 'DCP --- PopulateRebateChangeProposal: Insert ProposalPrice', 'E', @RowsI, 0, 0, @ProjectKey



--RAISERROR ('INSERT INTO ProposalPrice Stmt:', 0, 1) WITH NOWAIT 
--EXEC dbo.PrintVarCharMax @SQLStmt
--RAISERROR ('---', 0, 1) WITH NOWAIT 
--EXEC (@SQLStmt)



------ collisions... 
--EXEC LogDCPEvent 'PopulateRebateChangeProposal: Delete colliding RC proposals in ProposalPrice', 'B', 0, 0, 0 ,@ProjectKey

--; WITH Data AS (
--	SELECT
--		pp.AccountKey,
--		pp.ItemKey,
--		pp.InvoiceLineGroup1Key
--	FROM ProposalPrice pp
--	INNER JOIN dbo.PlaybookProject p
--		ON p.ProjectKey = pp.ProjectKey	
--	WHERE 
--		ProductionProjectIndicator = 'Y'
--		AND pp.AcceptanceCodeKey <> @AcceptedCode
--		AND pp.AcceptanceCodeKey <> @DeletedCode
--	GROUP BY
--		pp.AccountKey,
--		pp.ItemKey,
--		pp.InvoiceLineGroup1Key
--	HAVING
--		COUNT(*) > 1
--)
--UPDATE pp 
--	SET 
--		pp.AcceptanceCodeKey = @DeletedCode,
--		pp.ProposalPriceGroupKey = NULL,
--		pp.ProposalPriceBuyGroupKey = NULL,
--		pp.MaxStatus = 'D',
--		pp.ModificationDate = GETDATE(),
--		pp.ModificationUser = 'System Auto Delete - new proposal to be generated',
--		pp.ModificationUserAddress = CAST(@ProjectKey AS VARCHAR(25))
--FROM dbo.ProposalPrice pp
--INNER JOIN Data d
--	ON d.AccountKey = pp.AccountKey
--	AND d.ItemKey = pp.ItemKey
--	AND d.InvoiceLineGroup1Key = pp.InvoiceLineGroup1Key
--INNER JOIN dbo.PlaybookProject p
--	ON p.ProjectKey = pp.ProjectKey	
--WHERE
--	pp.ProjectKey <> @ProjectKey		-- keep the newest proposal - from this project
--	AND pp.AcceptanceCodeKey <> @AcceptedCode
--	AND pp.AcceptanceCodeKey <> @DeletedCode
--	AND ProductionProjectIndicator = 'Y'	
--SET @RowsU = @RowsU + @@ROWCOUNT	

--EXEC LogDCPEvent 'PopulateRebateChangeProposal: Delete colliding vcm proposals in ProposalPrice', 'E', 0, @RowsU, 0 ,@ProjectKey




UPDATE dbo.PlaybookProject
SET 
	GeneratedDate = GETDATE(),
	ProductionProjectIndicator = 'Y'
WHERE ProjectKey = @ProjectKey


EXEC LogDCPEvent 'DCP --- PopulateRebateChangeProposal: Deleting repeated AIS records - "The Club"', 'B'

DECLARE @MaxDate DATETIME
SELECT 	@MaxDate = SQLDate FROM dbo.DimDay WHERE DayKey = (SELECT MAX(InvoiceDateDayKey) FROM dbo.FactInvoiceLine)

UPDATE pp 
SET 
	AcceptanceCodeKey = 4, 
	MaxStatus = 'D',
	pp.ProposalPriceBuyGroupKey = NULL,
	pp.ProposalPriceGroupKey = NULL,
	pp.ModificationUser = 'Advanous - QA',
	pp.ModificationDate = GETDATE()
FROM dbo.ProposalPrice pp
WHERE
	pp.ProjectKey = @ProjectKey
	AND EXISTS (
		SELECT 1 
		FROM dbo.ProposalPrice pp2
		INNER JOIN dbo.PlaybookProject p
			ON p.ProjectKey = pp.ProjectKey
		WHERE 
			p.ProductionProjectIndicator = 'Y'
			AND p.ApproachTypeKey = 11
			AND pp2.AccountKey = pp.AccountKey
			AND pp2.ItemKey = pp.ItemKey
			AND pp2.InvoiceLineGroup1Key = pp.InvoiceLineGroup1Key
			AND pp2.CreationDate >= DATEADD(DAY, -30, @MaxDate)
			AND pp2.ProjectKey <> pp.ProjectKey
		)
SET @RowsU = @RowsU + @@ROWCOUNT	

EXEC LogDCPEvent 'DCP --- PopulateRebateChangeProposal: Deleting repeated AIS records - "The Club"', 'E', 0, @RowsU, 0, @ProjectKey



EXEC LogDCPEvent 'DCP --- PopulateRebateChangeProposal: "Soft" Deleting type 5 proposals', 'B'

UPDATE pp 
SET 

	AcceptanceCodeKey = 2,
	ModificationDate = GETDATE(),
	ModificationUser = 'Advanous - QA',
	ModificationUserAddress = CAST(@ProjectKey AS VARCHAR(50)),
	ProposalPriceGroupKey = NULL,
	ProposalPriceBuyGroupKey = NULL,
	MaxStatus = 'D',
	CommittedDate = GETDATE(),
	MaxStatusChangeDate = GETDATE(),
	MaxPrice = pp.NewPrice,
	MaxGPP = pp.NewGPP, 
	MaxDiscount = pp.NewDiscount,
	MaxPricingRule = 
		CASE
			WHEN UDVarchar1 = 'M' THEN '07'
			WHEN UDVarchar1 = 'P' THEN '01'
			WHEN UDVarchar1 = 'D' THEN '02'
			ELSE ''
		END,
	MaxEffectiveDate = GETDATE(),	-- pp.EffectiveDate,	https://enterbridge.atlassian.net/browse/BPB-3948
	MaxExpirationDate = CAST('12/31/2049' AS DATETIME)		-- https://enterbridge.atlassian.net/browse/BPB-2742
FROM dbo.ProposalPrice pp
WHERE
	pp.ProjectKey = @ProjectKey
	AND pp.AcceptanceCodeKey = 1
	AND ISNULL(pp.ProposalType, 0) = 5
SET @RowsU = @RowsU + @@ROWCOUNT	

EXEC LogDCPEvent 'DCP --- PopulateRebateChangeProposal: "Soft" Deleting type 5 proposals', 'E', 0, @RowsU, 0, @ProjectKey



--EXEC LogDCPEvent 'DCP --- PopulateRebateChangeProposal: Assign proposals to groups', 'B',0,0,0,@ProjectKey

--EXEC dbo.RefreshProposalPriceGroupAssignment 1, null, null, null, null

--EXEC LogDCPEvent 'DCP --- PopulateRebateChangeProposal: Assign proposals to groups', 'E',0,0,0,@ProjectKey

EXEC LogDCPEvent 'DCP --- PopulateRebateChangeProposal', 'E', @RowsI, @RowsU, @RowsD, @ProjectKey





GO
