SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




CREATE PROCEDURE [dbo].[LogDCPError]
    --  @ErrorInfo [NVARCHAR] (max) = NULL OUTPUT
    --, @ErrorProcedure [NVARCHAR] (255) = NULL
AS

/*
BEGIN TRY
    RAISERROR('Test', 12, 1)
    PRINT 'YOU SHOULD NOT SEE THIS TEXT IN THE RESULTING OUTPUT'
END TRY
BEGIN CATCH
    GOTO ER
END CATCH
ER:
    DECLARE @ErrorInfo [NVARCHAR] (max)
    EXEC [dbo].[LogDCPError]
        --@ErrorInfo = @ErrorInfo OUTPUT
    SELECT TOP 1 * FROM [dbo].[DCPError] ORDER BY [DCPErrorKey] DESC
    --SELECT @ErrorInfo
*/

SET NOCOUNT ON 


IF ISNULL(ERROR_NUMBER(), 0) != 0 AND XACT_STATE() <> -1
BEGIN
    DECLARE 
		@DCPErrorKey [int],
		@ErrorInfo [NVARCHAR] (MAX),
		@ErrorProcedure [NVARCHAR] (255)


    INSERT INTO [dbo].[DCPError] (
          ErrorProcedure
        , ErrorNumber
        , ErrorLine
        , ErrorSeverity
        , ErrorState
        , ErrorMessage
    )
    SELECT
          ERROR_PROCEDURE() ErrorProcedure
        , ERROR_NUMBER() ErrorNumber
        , ERROR_LINE() ErrorLine
        , ERROR_SEVERITY() ErrorSeverity
        , ERROR_STATE() ErrorState
        , ERROR_MESSAGE() ErrorMessage
    
    SELECT @DCPErrorKey = @@IDENTITY        

    SELECT @ErrorProcedure = ERROR_PROCEDURE()
    SELECT
          @ErrorInfo = 'ErrorNumber: ' + CONVERT([nvarchar] (32), e.[ErrorNumber]) + CHAR(13)
                     + 'ErrorSeverity: ' + CONVERT([nvarchar] (32), e.[ErrorSeverity]) + CHAR(13)
                     + 'ErrorState: ' + CONVERT([nvarchar] (32), e.[ErrorState]) + CHAR(13)
                     + 'ErrorProcedure: ' + ISNULL(e.[ErrorProcedure], '') + CHAR(13)
                     + 'ErrorLine: ' + ISNULL(CONVERT([nvarchar] (32), e.[ErrorLine]), '') + CHAR(13)
                     + 'ErrorMessage: ' + ISNULL(e.[ErrorMessage], '') + CHAR(13)
    FROM [dbo].[DCPError] AS [e]
    WHERE @DCPErrorKey = [DCPErrorKey]

	DECLARE
		@OnErrorEmailRecipients NVARCHAR(MAX),
  		@EmailSubject NVARCHAR (255), 
		@EmailBody NVARCHAR (MAX), 	
		@OnErrorSendEmailNotificationIndicator NCHAR(1)

	SELECT @OnErrorSendEmailNotificationIndicator = ParamValue FROM dbo.Param WHERE ParamName = 'OnErrorSendEmailNotificationIndicator'
	SELECT @OnErrorEmailRecipients = ParamValue FROM dbo.Param WHERE ParamName = 'OnErrorEmailRecipients'
    
	IF	ISNULL(@OnErrorSendEmailNotificationIndicator, 'N') = 'Y' AND
		@OnErrorEmailRecipients IS NOT NULL
	BEGIN

		SELECT @EmailSubject = N'dbo ETL: Error Encountered (' + ISNULL(DB_NAME(), '') + ')'
	    
		IF ISNULL(@ErrorProcedure, '') != ''
			SELECT @EmailSubject = @EmailSubject + ' In Proc ' + @ErrorProcedure

		EXEC [msdb].[dbo].[sp_send_dbmail]
			  @recipients = @OnErrorEmailRecipients
			, @subject    = @EmailSubject
			, @body = @ErrorInfo
			, @importance = N'High'
			, @exclude_query_output = 1
    END
        
    --PRINT @EmailSubject + CHAR(10) + @ErrorInfo
END




GO
