SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE 
PROCEDURE [lion].[Load_UpdateDimAccountManager]
AS

/*

EXEC lion.Load_UpdateDimAccountManager

*/

SET NOCOUNT ON


EXEC LogDCPEvent 'ETL - Load_UpdateDimAccountManager', 'B'



DECLARE
	@RowsI INT, 
	@RowsU INT, 
	@RowsD INT,
	@UnknownDimAccount Key_Normal_Type,
	@UnknownDimAccountGroup1 Key_Normal_Type

SET @RowsI = 0
SET @RowsU = 0
SET @RowsD = 0


SET @UnknownDimAccount = (SELECT AccountKey FROM DimAccount WHERE AccountClientUniqueIdentifier = 'Unknown')
SET @UnknownDimAccountGroup1 = (SELECT AccountGroup1Key FROM DimAccountGroup1 WHERE AG1Level1 = 'Unknown')



--/* set all to the unknown */
--UPDATE DimAccountManager 
--SET 
--	PrimaryAccountGroup1Key = 1,
--	SupervisingAccountManagerKey = 1



/* bearcat has logic to assign the branch to the user owning the most accounts; for lion, it is simplely making the assignment based on its hiearchy file.  I suppose that they could want a version of the Bearcat logic in the future */
EXEC LogDCPEvent 'ETL - Load_UpdateDimAccountManager: PrimaryAccountGroup1Key', 'B'

UPDATE dam
SET
	dam.PrimaryAccountGroup1Key = dag1.AccountGroup1Key
FROM dbo.DimAccountManager dam
INNER JOIN dbo.DimPerson dp 
	ON dp.DimPersonKey = dam.DimPersonKey
INNER JOIN dbo.DimAccountGroup1 dag1
	ON dag1.AG1Level1 = DimPersonUDVarchar1
WHERE
	DimPersonUDVarchar1 IS NOT NULL
SET @RowsU = @@ROWCOUNT + @RowsU

EXEC LogDCPEvent 'ETL - Load_UpdateDimAccountManager: PrimaryAccountGroup1Key', 'E', @RowsI, @RowsU, @RowsD



/* this is taken from Bearcat */
EXEC LogDCPEvent 'ETL - Load_UpdateDimAccountManager: SupervisingAccountManagerKey', 'B'
SET @RowsI = 0
SET @RowsU = 0
SET @RowsD = 0
; WITH cte AS (
	SELECT 
		bam.SubsidiaryAccountManagerKey AS AccountManagerKey,
		bam.ParentAccountManagerkey, 
		--wu.WebUserKey,
		ROW_NUMBER() OVER (
			PARTITION BY 
				bam.SubsidiaryAccountManagerKey
			ORDER BY
				NumberOfLevels ASC,
				bam.ParentAccountManagerkey ASC		-- should never get used as NumberOfLevels should be unique per bam.SubsidiaryAccountManagerKey
		) AS RowRank
		--,AccountManagerCode	
	FROM dbo.BridgeAccountManager bam 
	INNER JOIN dbo.DimAccountManager dam ON dam.AccountManagerKey = bam.ParentAccountManagerkey
	INNER JOIN dbo.WebUser wu ON wu.AccountManagerKey = bam.ParentAccountManagerkey
	--INNER JOIN dbo.WebUserRole wur ON wur.WebUserKey = wu.WebUserKey AND wur.WebRoleKey = 8 /* 8 = Network Manager */
	WHERE bam.SubsidiaryAccountManagerKey <> 	bam.ParentAccountManagerkey
)
UPDATE dam
SET dam.SupervisingAccountManagerKey = ISNULL(cte.ParentAccountManagerKey, 2)
FROM dbo.DimAccountManager dam
LEFT JOIN cte
	ON cte.AccountManagerKey = dam.AccountManagerKey
	AND RowRank = 1
SET @RowsU = @@ROWCOUNT + @RowsU
EXEC LogDCPEvent 'ETL - Load_UpdateDimAccountManager: SupervisingAccountManagerKey', 'E', @RowsI, @RowsU, @RowsD



EXEC LogDCPEvent 'ETL - Load_UpdateDimAccountManager', 'E', @RowsI, @RowsU, @RowsD








GO
