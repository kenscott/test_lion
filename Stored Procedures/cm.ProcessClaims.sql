SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE [cm].[ProcessClaims]
	@BatchKey INT,
	@WebUserKey INT

WITH EXECUTE AS CALLER
AS

-- EXEC cm.ProcessClaims 200951, 1

SET NOCOUNT OFF


--Set ClaimStatus = Processing
UPDATE c
SET ClaimStatus = 'C',
	ModificationDate = GETDATE()
--SELECT *
FROM cm.SupplierClaim c
WHERE BatchKey = @BatchKey
	AND ClaimStatus = 'R'

--Log Processing of Claims
INSERT INTO cm.ClaimLog
	(
	[WebUserKey],
	[SupplierClaimKey],
	[Notes],
	[CreationDate],
	[ClaimAction]
	)
SELECT 
	@WebUserKey,
	c.SupplierClaimKey,
	'From: Ready to ' + s.SupplierClaimStatusDescription + ' ; SupplierClaimKey =' + CAST(c.SupplierClaimKey AS varchar(100)),
	GETDATE(),
	'Process Supplier Claim'
FROM cm.SupplierClaim c
INNER JOIN cm.SupplierClaimStatus s
	ON s.SupplierClaimStatus = c.ClaimStatus
WHERE BatchKey = @BatchKey
	AND ClaimStatus = 'C'


--Recalculate Tallies and Ratio
EXEC cm.CalculateCurrentMonthTallyAutoReject @BatchKey

/* ----- Disable Reject Overlimit Tallies Functionality per LPF-6418 ------
----------------------------------------------------------------------------------
--Reject Over Limit Claim Lines for Items
SELECT c.BatchKey, sci.ContractItemKey, cj.ClaimJournalStatus, sci.LimitType, fil.Qty, fil.NettPrice, cj.ClaimLineValue, cj.ClaimJournalKey, fil.ClientInvoiceLineUniqueID, sci.LimitValue, sci.CurrentMonthTally, sci.PreviousMonthTally 
INTO #QuantityLimitItem
FROM cm.ClaimJournal cj
INNER JOIN cc.SupplierContract sc
	ON sc.ContractID = cj.CurrentContractID
INNER JOIN cc.SupplierContractItem sci
	ON sci.ContractKey = sc.ContractKey
INNER JOIN cm.SupplierClaim c
	ON c.ClaimMonth = cj.ClaimMonth
	AND c.VendorKey = cj.VendorKey
INNER JOIN cm.FactInvoiceLine fil
	ON fil.ClientInvoiceLineUniqueID = cj.ClientInvoiceLineUniqueID
	AND fil.ItemKey = sci.ItemKey
WHERE c.BatchKey = @BatchKey
	AND c.ClaimStatus = 'C'
	AND cj.ClaimJournalStatus = 'P'
	AND cj.ClaimType IN ('C', 'M')
	AND ISNULL(sci.PreviousMonthTally, 0) + ISNULL(sci.CurrentMonthTally, 0) > sci.LimitValue
	

WHILE (SELECT COUNT(*) FROM #QuantityLimitItem) >= 1

BEGIN

--Set Claim Journal Status to Rejected 
UPDATE cm.ClaimJournal
SET ClaimJournalStatus = 'X',
	RejectionReasonKey = 1
WHERE ClaimJournalKey IN (
SELECT TOP 1 ClaimJournalKey
FROM #QuantityLimitItem
ORDER BY ContractItemKey, ClientInvoiceLineUniqueID DESC
)
/* WRITE LOG FOR ABOVE REJECT*/
INSERT INTO cm.ClaimJournalLog
(
    WebUserKey,
    ClaimJournalKey,
    Notes,
    CreationDate,
    ClaimAction
)

SELECT 
	@WebUserKey,
	cj.ClaimJournalKey,
	'Status Changed from <P> to <X>; SupplierClaimKey = <' + CAST(cj.SupplierClaimKey AS VARCHAR(100)) + '> ; RejectionReason= <Auto-Rejected due to limit reached>' ,
	GETDATE(),
	'Process Supplier Claim'
FROM cm.ClaimJournal cj
WHERE cj.ClaimJournalKey IN (
SELECT TOP 1 ClaimJournalKey
FROM #QuantityLimitItem
ORDER BY ContractItemKey, ClientInvoiceLineUniqueID DESC
)


TRUNCATE TABLE #QuantityLimitItem

--Recalculate Tallies and Ratio
EXEC cm.CalculateCurrentMonthTallyAutoReject @BatchKey

INSERT INTO #QuantityLimitItem
SELECT c.BatchKey, sci.ContractItemKey, cj.ClaimJournalStatus, sci.LimitType, fil.Qty, fil.NettPrice, cj.ClaimLineValue, cj.ClaimJournalKey, fil.ClientInvoiceLineUniqueID, sci.LimitValue, sci.CurrentMonthTally, sci.PreviousMonthTally 
FROM cm.ClaimJournal cj
INNER JOIN cc.SupplierContract sc
	ON sc.ContractID = cj.CurrentContractID
INNER JOIN cc.SupplierContractItem sci
	ON sci.ContractKey = sc.ContractKey
INNER JOIN cm.SupplierClaim c
	ON c.ClaimMonth = cj.ClaimMonth
	AND c.VendorKey = cj.VendorKey
INNER JOIN cm.FactInvoiceLine fil
	ON fil.ClientInvoiceLineUniqueID = cj.ClientInvoiceLineUniqueID
	AND fil.ItemKey = sci.ItemKey
WHERE c.BatchKey = @BatchKey
	AND c.ClaimStatus = 'C'
	AND cj.ClaimJournalStatus = 'P'
	AND cj.ClaimType IN ('C', 'M')
	AND ISNULL(sci.PreviousMonthTally, 0) + ISNULL(sci.CurrentMonthTally, 0) > sci.LimitValue
	
--Loop back
END
-------------------------------------------------------------------------------------
--Reject Over Limit Claim Lines for Item Groups (MPGs/SPGs)
SELECT c.BatchKey, sci.ContractItemGroupXKey, cj.ClaimJournalStatus, sci.LimitType, fil.Qty, fil.NettPrice, cj.ClaimLineValue, cj.ClaimJournalKey, fil.ClientInvoiceLineUniqueID, sci.LimitValue, sci.CurrentMonthTally, sci.PreviousMonthTally 
INTO #QuantityLimitItemGroup
FROM cm.ClaimJournal cj
INNER JOIN cc.SupplierContract sc
	ON sc.ContractID = cj.CurrentContractID
INNER JOIN cc.SupplierContractItemGroupX sci
	ON sci.ContractKey = sc.ContractKey
LEFT JOIN dbo.DimItem di1
	ON di1.ItemGroup1Key = sci.ItemGroupXKey
	AND sci.ItemGroupLevel = 1
LEFT JOIN dbo.DimItem di3
	ON di3.ItemGroup3Key = sci.ItemGroupXKey
	AND sci.ItemGroupLevel = 3
INNER JOIN cm.FactInvoiceLine fil
	ON fil.ClientInvoiceLineUniqueID = cj.ClientInvoiceLineUniqueID
	AND fil.ItemKey = ISNULL(di1.ItemKey, di3.ItemKey)
INNER JOIN cm.SupplierClaim c
	ON c.ClaimMonth = cj.ClaimMonth
	AND c.VendorKey = cj.VendorKey
WHERE c.BatchKey = @BatchKey
	AND c.ClaimStatus = 'C'
	AND cj.ClaimJournalStatus = 'P'
	AND cj.ClaimType IN ('C', 'M')
	AND ISNULL(sci.PreviousMonthTally, 0) + ISNULL(sci.CurrentMonthTally, 0) > sci.LimitValue
	

WHILE (SELECT COUNT(*) FROM #QuantityLimitItemGroup) >= 1

BEGIN

--Set Claim Journal Status to Rejected 
UPDATE cm.ClaimJournal
SET ClaimJournalStatus = 'X',
	RejectionReasonKey = 1
WHERE ClaimJournalKey IN (
SELECT TOP 1 ClaimJournalKey
FROM #QuantityLimitItemGroup
ORDER BY ContractItemGroupXKey, ClientInvoiceLineUniqueID DESC
)
--WRITE LOG AGAIN for Reject Over Limit Claim Lines for Item Groups (MPGs/SPGs)
INSERT INTO cm.ClaimJournalLog
(
    WebUserKey,
    ClaimJournalKey,
    Notes,
    CreationDate,
    ClaimAction
)

SELECT 
	@WebUserKey,
	cj.ClaimJournalKey,
	'Status Changed from <P> to <X>; SupplierClaimKey = <' + CAST(cj.SupplierClaimKey AS VARCHAR(100)) + '> ; RejectionReason= <Auto-Rejected due to limit reached>' ,
	GETDATE(),
	'Process Supplier Claim'
FROM cm.ClaimJournal cj
WHERE cj.ClaimJournalKey IN (
SELECT TOP 1 ClaimJournalKey
FROM #QuantityLimitItemGroup
ORDER BY ContractItemGroupXKey, ClientInvoiceLineUniqueID DESC
)



TRUNCATE TABLE #QuantityLimitItemGroup

--Recalculate Tallies and Ratio
EXEC cm.CalculateCurrentMonthTallyAutoReject @BatchKey

INSERT INTO #QuantityLimitItemGroup
SELECT c.BatchKey, sci.ContractItemGroupXKey, cj.ClaimJournalStatus, sci.LimitType, fil.Qty, fil.NettPrice, cj.ClaimLineValue, cj.ClaimJournalKey, fil.ClientInvoiceLineUniqueID, sci.LimitValue, sci.CurrentMonthTally, sci.PreviousMonthTally 
FROM cm.ClaimJournal cj
INNER JOIN cc.SupplierContract sc
	ON sc.ContractID = cj.CurrentContractID
INNER JOIN cc.SupplierContractItemGroupX sci
	ON sci.ContractKey = sc.ContractKey
LEFT JOIN dbo.DimItem di1
	ON di1.ItemGroup1Key = sci.ItemGroupXKey
	AND sci.ItemGroupLevel = 1
LEFT JOIN dbo.DimItem di3
	ON di3.ItemGroup3Key = sci.ItemGroupXKey
	AND sci.ItemGroupLevel = 3
INNER JOIN cm.FactInvoiceLine fil
	ON fil.ClientInvoiceLineUniqueID = cj.ClientInvoiceLineUniqueID
	AND fil.ItemKey = ISNULL(di1.ItemKey, di3.ItemKey)
INNER JOIN cm.SupplierClaim c
	ON c.ClaimMonth = cj.ClaimMonth
	AND c.VendorKey = cj.VendorKey
WHERE c.BatchKey = @BatchKey
	AND c.ClaimStatus = 'C'
	AND cj.ClaimJournalStatus = 'P'
	AND cj.ClaimType IN ('C', 'M')
	AND ISNULL(sci.PreviousMonthTally, 0) + ISNULL(sci.CurrentMonthTally, 0) > sci.LimitValue
	
--Loop back
END
-------------------------------------------------------------------------------------------------------------------------------------
*/
--Reset PreviousMonthTally and CurrentMonthTally 
UPDATE cc.SupplierContractItem
SET PreviousMonthTally = ISNULL(PreviousMonthTally, 0) + ISNULL(CurrentMonthTally, 0),
	CurrentMonthTally = 0
WHERE ContractItemKey IN (
SELECT sci.ContractItemKey
FROM cm.ClaimJournal cj
INNER JOIN cc.SupplierContract sc
	ON sc.ContractID = cj.CurrentContractID
INNER JOIN cc.SupplierContractItem sci
	ON sci.ContractKey = sc.ContractKey
INNER JOIN cm.SupplierClaim c
	ON c.ClaimMonth = cj.ClaimMonth
	AND c.VendorKey = cj.VendorKey
WHERE c.BatchKey = @BatchKey
	AND c.ClaimStatus = 'C'
	AND cj.ClaimJournalStatus = 'P'
	AND cj.ClaimType IN ('C', 'M')
	)

UPDATE cc.SupplierContractItemGroupX
SET PreviousMonthTally = ISNULL(PreviousMonthTally, 0) + ISNULL(CurrentMonthTally, 0),
	CurrentMonthTally = 0
WHERE ContractItemGroupXKey IN (
SELECT sci.ContractItemGroupXKey
FROM cm.ClaimJournal cj
INNER JOIN cc.SupplierContract sc
	ON sc.ContractID = cj.CurrentContractID
INNER JOIN cc.SupplierContractItemGroupX sci
	ON sci.ContractKey = sc.ContractKey
INNER JOIN cm.SupplierClaim c
	ON c.ClaimMonth = cj.ClaimMonth
	AND c.VendorKey = cj.VendorKey
WHERE c.BatchKey = @BatchKey
	AND c.ClaimStatus = 'C'
	AND cj.ClaimJournalStatus = 'P'
	AND cj.ClaimType IN ('C', 'M')
	)

--Create Invoice Claims
--InvoiceClaimKey is the InvoiceClaimId
DECLARE @NewInvoiceClaim TABLE (
		VendorKey int,
		ClaimMonth int,
		ContractId varchar(9),
		InvoiceClaimKey int
		)

INSERT INTO [cm].[InvoiceClaim]
           ([VendorKey]
           ,[ClaimMonth]
           ,[ContractId]
           ,[CreationDate]
           ,[CreatedByWebUserKey]
		   ,[ClaimType])
	OUTPUT
	INSERTED.VendorKey,
	INSERTED.ClaimMonth,
	INSERTED.ContractId,
	INSERTED.InvoiceClaimKey
	INTO @NewInvoiceClaim
SELECT DISTINCT 
	cj.VendorKey,
	cj.ClaimMonth,
	cj.CurrentContractID,
	GETDATE(),
	@WebUserKey,
	'CM'
FROM cm.ClaimJournal cj
INNER JOIN cm.SupplierClaim sc
	ON sc.ClaimMonth = cj.ClaimMonth
	AND sc.VendorKey = cj.VendorKey
WHERE sc.BatchKey = @BatchKey
	AND sc.ClaimStatus = 'C'
	AND cj.ClaimType IN ('C', 'M')
ORDER BY cj.VendorKey, cj.ClaimMonth, cj.CurrentContractID


--Set Claim Keys and set status = Claimed in cm.ClaimJournal for matching with status = Pending
--LPF - 6269, added logging for updated claimsJournal

DECLARE @UpdateSupplierClaim TABLE (
		ClaimJournalKey KEY_NORMAL_TYPE,
		ClaimJournalStatus varchar(1),
		ClaimJournalStatusoriginal varchar(1)
		)


UPDATE cj
SET cj.SupplierClaimKey = c.SupplierClaimKey,
	cj.InvoiceClaimKey = ic.InvoiceClaimKey,
	cj.ClaimJournalStatus = 'C',
	cj.ADVBatchKey = NULL 
	OUTPUT 
			INSERTED.ClaimJournalKey,
			INSERTED.ClaimJournalStatus,
			DELETED.ClaimJournalStatus
			INTO @UpdateSupplierClaim
FROM cm.ClaimJournal cj
INNER JOIN cm.SupplierClaim c
	ON c.ClaimMonth = cj.ClaimMonth
	AND c.VendorKey = cj.VendorKey
LEFT JOIN @NewInvoiceClaim ic
	ON ic.ClaimMonth = cj.ClaimMonth
	AND ic.VendorKey = cj.VendorKey 
	AND ic.ContractId = cj.CurrentContractID
WHERE c.BatchKey = @BatchKey
	AND c.ClaimStatus = 'C'
	AND cj.ClaimJournalStatus = 'P'
	AND cj.ClaimType IN ('C', 'M')

--Log Processing of Claims
INSERT INTO cm.ClaimJournalLog
(
    WebUserKey,
    ClaimJournalKey,
    Notes,
    CreationDate,
    ClaimAction
)

SELECT 
	@WebUserKey,
	u.ClaimJournalKey,
	'Status Changed from P to C; SupplierClaimKey = ' + CAST(c.SupplierClaimKey AS VARCHAR(100)) + ' ; InvoiceClaimKey= ' + CAST(ic.InvoiceClaimKey AS VARCHAR(100)) ,
	GETDATE(),
	'Process Supplier Claim'
FROM cm.ClaimJournal cj
INNER JOIN  @UpdateSupplierClaim u ON
u.ClaimJournalKey = cj.ClaimJournalKey 
INNER JOIN cm.SupplierClaim c
	ON c.ClaimMonth = cj.ClaimMonth
	AND c.VendorKey = cj.VendorKey
LEFT JOIN cm.InvoiceClaim ic
	ON ic.ClaimMonth = cj.ClaimMonth
	AND ic.VendorKey = cj.VendorKey 
	AND ic.ContractId = cj.CurrentContractID
	AND ic.ClaimType = 'CM'
WHERE c.BatchKey = @BatchKey
	AND c.ClaimStatus = 'C'
	AND cj.ClaimType IN ('C', 'M')
--End Logging	
	
--Change status for rejected ClaimLines from X to U (Unclaimed)
DECLARE @UpdateRejectedClaimLines TABLE (
		ClaimJournalKey KEY_NORMAL_TYPE,
		ClaimJournalStatus varchar(1),
		ClaimJournalStatusOriginal varchar(1)
		)

UPDATE cj
SET cj.SupplierClaimKey = c.SupplierClaimKey,
	cj.ClaimJournalStatus = 'U',
	cj.ADVBatchKey = NULL 
	OUTPUT 
			INSERTED.ClaimJournalKey,
			INSERTED.ClaimJournalStatus,
			DELETED.ClaimJournalStatus
			INTO @UpdateRejectedClaimLines
FROM cm.ClaimJournal cj
INNER JOIN cm.SupplierClaim c
	ON c.ClaimMonth = cj.ClaimMonth
	AND c.VendorKey = cj.VendorKey
WHERE c.BatchKey = @BatchKey
	AND c.ClaimStatus = 'C'
	AND cj.ClaimJournalStatus = 'X'
	AND cj.ClaimType IN ('C', 'M')

INSERT INTO cm.ClaimJournalLog
(
    WebUserKey,
    ClaimJournalKey,
    Notes,
    CreationDate,
    ClaimAction
)

SELECT 
	@WebUserKey,
	u.ClaimJournalKey,
	'Status Changed from X to U',
	GETDATE(),
	'Process Supplier Claim'
FROM cm.ClaimJournal cj
INNER JOIN  @UpdateRejectedClaimLines u ON
u.ClaimJournalKey = cj.ClaimJournalKey 	
		

--Set ClaimMonth to Current Month for matching rec's with status = Review
DECLARE @ClaimJournalReview TABLE (
		ClaimJournalKey KEY_NORMAL_TYPE)

UPDATE cj
SET ClaimMonth = CAST(CONVERT(VARCHAR(4), DATEPART(yy, GETDATE())) + CONVERT(VARCHAR(2), DATEPART(mm, GETDATE())) AS INT)
OUTPUT 
			INSERTED.ClaimJournalKey
			INTO @ClaimJournalReview
--SELECT *
FROM cm.ClaimJournal cj
INNER JOIN cm.SupplierClaim c
	ON c.ClaimMonth = cj.ClaimMonth
	AND c.VendorKey = cj.VendorKey
WHERE c.BatchKey = @BatchKey
	AND c.ClaimStatus = 'C'
	AND cj.ClaimJournalStatus = 'R'
	AND cj.ClaimType IN ('C', 'M')
	

--Create new Supplier Claim for rec's left with a status of 'R' Review
DECLARE @NewSupplierClaimRowData TABLE (
		SupplierClaimKey KEY_NORMAL_TYPE,
		ClaimStatus VARCHAR(1))

INSERT INTO [cm].[SupplierClaim]
           ([VendorKey]
           ,[ClaimMonth]
           ,[CreationDate]
           ,[CreatedByWebUserKey]
           ,[ClaimStatus])
	OUTPUT 
			INSERTED.SupplierClaimKey,
			INSERTED.ClaimStatus
			INTO @NewSupplierClaimRowData
SELECT DISTINCT
		cj.VendorKey,
		cj.ClaimMonth, 
		GETDATE(),
		1,
		'P'
FROM cm.ClaimJournal cj
INNER JOIN @ClaimJournalReview r
	ON r.ClaimJournalKey = cj.ClaimJournalKey
LEFT JOIN cm.SupplierClaim c
	ON c.ClaimMonth = cj.ClaimMonth
	AND c.VendorKey = cj.VendorKey
	AND c.ClaimStatus IN ('P', 'R')  --don't create new if these exist 
WHERE c.SupplierClaimKey IS NULL  

--Log new Supplier Claim
INSERT INTO cm.ClaimLog
	(
	[WebUserKey],
	[SupplierClaimKey],
	[Notes],
	[CreationDate],
	[ClaimAction]
	)
SELECT 
	1,
	d.SupplierClaimKey,
	'From: blank To: ' + s.SupplierClaimStatusDescription + '; SupplierClaimKey = ' + CAST(d.SupplierClaimKey AS VARCHAR(100)),
	GETDATE(),
	'Create Supplier Claim'
FROM @NewSupplierClaimRowData d
INNER JOIN cm.SupplierClaimStatus s
	ON s.SupplierClaimStatus = d.ClaimStatus

GO
