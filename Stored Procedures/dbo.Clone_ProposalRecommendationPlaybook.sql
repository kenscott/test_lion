SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO








--CreateScript_ProposalRecommendationPlaybook 1

CREATE         PROCEDURE [dbo].[Clone_ProposalRecommendationPlaybook]
@ProposalRecommendationPlaybookKey INT,
@Command CHAR(1) = 'N',
@ApproachTypeKey INT
AS

DECLARE @SQL NVARCHAR(MAX)


DECLARE
@MinMonthsBetweenProposals NVARCHAR(MAX), 
@WhereClause1 NVARCHAR(MAX), 
@JoinClause1  NVARCHAR(MAX)

DECLARE 
@ForecastType Description_Small_type,
@DynamicUDVarchar1 Description_Small_type,
@DynamicUDVarchar2 Description_Small_type,
@ProposalRecommendationKey NVARCHAR(MAX), 
@ProposalRecommendationPassSequenceNumber NVARCHAR(MAX), 
@MinCostDecreaseThresholdPercent NVARCHAR(MAX), 
@CostDecreaseCapPercent NVARCHAR(MAX), 
@MinPriceIncreaseThresholdPercent NVARCHAR(MAX), 
@PriceIncreaseCapPercent NVARCHAR(MAX), 
@WhereClause2 NVARCHAR(MAX), 
@JoinClause2 NVARCHAR(MAX), 
@WhereClause3 NVARCHAR(MAX), 
@JoinClause3 NVARCHAR(MAX), 
@AcceptRejectCode_PriceProposal NVARCHAR(MAX), 
@AcceptRejectCode_CostProposal NVARCHAR(MAX), 
@DynamicReasonCode NVARCHAR(MAX), 
@DynamicEffectiveDate NVARCHAR(MAX), 
@DynamicSuppressionDate NVARCHAR(MAX), 
@SMEMinGPP NVARCHAR(MAX), 
@SMEMaxGPP NVARCHAR(MAX),
@ProposedPriceOverride NVARCHAR(MAX), 
@ProposedCostOverride NVARCHAR(MAX),
@DynamicDeleteWhere1 NVARCHAR(MAX),
@DynamicDeleteJoin1 NVARCHAR(MAX),
@DynamicDeleteGroupBy1 NVARCHAR(MAX),
@DynamicDeleteHaving1 NVARCHAR(MAX)



SELECT 
@ProposalRecommendationPlaybookKey = ISNULL(ProposalRecommendationPlaybookKey, ''), 
@MinMonthsBetweenProposals = ISNULL(MinMonthsBetweenProposals, ''), 
@WhereClause1 = ISNULL(REPLACE(WhereClause1,'''',''''''), ''), 
@JoinClause1 = ISNULL(REPLACE(JoinClause1,'''',''''''), ''),
@DynamicDeleteWhere1 = ISNULL(REPLACE(DynamicDeleteWhere1,'''',''''''), ''),
@DynamicDeleteJoin1 = ISNULL(REPLACE(DynamicDeleteJoin1,'''',''''''), ''),
@DynamicDeleteGroupBy1 = ISNULL(REPLACE(DynamicDeleteGroupBy1,'''',''''''), ''),
@DynamicDeleteHaving1 = ISNULL(REPLACE(DynamicDeleteHaving1,'''',''''''), ''),
@ForecastType = ISNULL(REPLACE(ForecastType,'''',''''''), '')
FROM ATKProposalRecommendationPlaybook
WHERE ProposalRecommendationPlaybookKey = @ProposalRecommendationPlaybookKey

IF @Command = 'N'
BEGIN

	INSERT INTO #ScenarioData SELECT  '-------------------------------------'
	INSERT INTO #ScenarioData SELECT  '--Insert new ATKProposalRecommendationPlaybook Row'
	INSERT INTO #ScenarioData SELECT  '-------------------------------------'
	INSERT INTO #ScenarioData SELECT  'Declare @NewProposalRecommendationPlaybookKey int'
	
	
	
	SET @SQL = 
	'INSERT INTO ATKProposalRecommendationPlaybook(
	MinMonthsBetweenProposals, WhereClause1, JoinClause1, DynamicDeleteWhere1, DynamicDeleteJoin1,DynamicDeleteGroupBy1,DynamicDeleteHaving1,ForecastType,Version)
	Values ('''+@MinMonthsBetweenProposals+''','''+@WhereClause1+''','''+@JoinClause1+''','''+@DynamicDeleteWhere1+''','''+@DynamicDeleteJoin1+''','''+@DynamicDeleteGroupBy1+''','''+@DynamicDeleteHaving1+''',''' + @ForecastType + ''',0)'
	
	INSERT INTO #ScenarioData SELECT  @SQL
	INSERT INTO #ScenarioData SELECT  'Set @NewProposalRecommendationPlaybookKey = @@Identity'
	INSERT INTO #ScenarioData SELECT  'Print ''Your new Recommendation Playbook Key is: ''+Cast(@NewProposalRecommendationPlaybookKey as nvarchar(10))'
END
ELSE IF @Command = 'U'
BEGIN
	INSERT INTO #ScenarioData SELECT  '------------------------------------------------------'
	INSERT INTO #ScenarioData SELECT  '--Updating ATKProposalRecommendationPlaybook: '+CAST(@ProposalRecommendationPlaybookKey AS NVARCHAR(100))
	INSERT INTO #ScenarioData SELECT  '------------------------------------------------------'
	INSERT INTO #ScenarioData SELECT  '	
	UPDATE ATKProposalRecommendationPlaybook SET
	MinMonthsBetweenProposals ='''+@MinMonthsBetweenProposals+''',
	WhereClause1 = '''+@WhereClause1+''',
	JoinClause1 ='''+@JoinClause1+''',
	DynamicDeleteWhere1 ='''+@DynamicDeleteWhere1+''',
	DynamicDeleteJoin1 ='''+@DynamicDeleteJoin1+''',
	DynamicDeleteGroupBy1 ='''+@DynamicDeleteGroupBy1+''',
	DynamicDeleteHaving1 ='''+@DynamicDeleteHaving1+''',
	ForecastType = '''+@ForecastType+'''

	Where ProposalRecommendationPlaybookKey = '+CAST(@ProposalRecommendationPlaybookKey AS NVARCHAR(100))

END
ELSE
BEGIN
	INSERT INTO #ScenarioData SELECT  '------------------------------------------------------'
	INSERT INTO #ScenarioData SELECT  'Your current ATKProposalRecommendationPlaybook is: '+CAST(@ProposalRecommendationPlaybookKey AS NVARCHAR(100))
	INSERT INTO #ScenarioData SELECT  '------------------------------------------------------'
	INSERT INTO #ScenarioData SELECT  '--MinMonthsBetweenProposals:   '+ISNULL(@MinMonthsBetweenProposals, 'NULL')
	INSERT INTO #ScenarioData SELECT  '--WhereClause1:                '+ISNULL(@WhereClause1, 'NULL')
	INSERT INTO #ScenarioData SELECT  '--JoinClause1:                 '+ISNULL(@JoinClause1, 'NULL')
	INSERT INTO #ScenarioData SELECT  '--DynamicDeleteWhere1:         '+ISNULL(@DynamicDeleteWhere1, 'NULL')
	INSERT INTO #ScenarioData SELECT  '--DynamicDeleteJoin1:         '+ISNULL(@DynamicDeleteJoin1, 'NULL')
	INSERT INTO #ScenarioData SELECT  '--DynamicDeleteGroupBy1:         '+ISNULL(@DynamicDeleteGroupBy1, 'NULL')
	INSERT INTO #ScenarioData SELECT  '--DynamicDeleteHaving1:         '+ISNULL(@DynamicDeleteHaving1, 'NULL')
	INSERT INTO #ScenarioData SELECT  '--ForecastType:         '+ISNULL(@ForecastType, 'NULL')

END
--Now we need to loop through all the segment customer rules for this playbook and insert them
DECLARE ProposalRecoms CURSOR FOR
SELECT  
ProposalRecommendationKey, ProposalRecommendationPassSequenceNumber,MinCostDecreaseThresholdPercent,CostDecreaseCapPercent,MinPriceIncreaseThresholdPercent,PriceIncreaseCapPercent,REPLACE(WhereClause1,'''',''''''),REPLACE(JoinClause1,'''',''''''),REPLACE(WhereClause2,'''',''''''),REPLACE(JoinClause2,'''',''''''),REPLACE(WhereClause3,'''',''''''),REPLACE(JoinClause3,'''',''''''),AcceptRejectCode_PriceProposal,AcceptRejectCode_CostProposal,DynamicReasonCode,DynamicEffectiveDate,DynamicSuppressionDate,SMEMinGPP,SMEMaxGPP, ISNULL(ProposedPriceOverride,''), ISNULL(ProposedCostOverride,''), DynamicUDVarchar1, DynamicUDVarchar2
FROM ATKProposalRecommendation
WHERE ProposalRecommendationPlaybookKey = @ProposalRecommendationPlaybookKey
OPEN ProposalRecoms
FETCH NEXT FROM ProposalRecoms INTO @ProposalRecommendationKey, @ProposalRecommendationPassSequenceNumber, @MinCostDecreaseThresholdPercent, @CostDecreaseCapPercent, @MinPriceIncreaseThresholdPercent, @PriceIncreaseCapPercent, @WhereClause1, @JoinClause1, @WhereClause2, @JoinClause2, @WhereClause3, @JoinClause3, @AcceptRejectCode_PriceProposal, @AcceptRejectCode_CostProposal, @DynamicReasonCode, @DynamicEffectiveDate, @DynamicSuppressionDate, @SMEMinGPP, @SMEMaxGPP, @ProposedPriceOverride, @ProposedCostOverride, @DynamicUDVarchar1, @DynamicUDVarchar2
WHILE @@Fetch_Status = 0
BEGIN

	IF @Command = 'N'
	BEGIN

		INSERT INTO #ScenarioData SELECT  '     -------------------------------------'
		INSERT INTO #ScenarioData SELECT  '     --Insert new ATKProposalRecommendation Row'
		INSERT INTO #ScenarioData SELECT  '     -------------------------------------'
	
		SET @SQL = '
	     INSERT INTO ATKProposalRecommendation( ProposalRecommendationPlaybookKey, ProposalRecommendationPassSequenceNumber, MinCostDecreaseThresholdPercent, CostDecreaseCapPercent, MinPriceIncreaseThresholdPercent, PriceIncreaseCapPercent, WhereClause1, JoinClause1, WhereClause2, JoinClause2, WhereClause3, JoinClause3, AcceptRejectCode_PriceProposal, AcceptRejectCode_CostProposal, DynamicReasonCode, DynamicEffectiveDate, DynamicSuppressionDate, SMEMinGPP, SMEMaxGPP, ProposedPriceOverride, ProposedCostOverride, Version, DynamicUDVarchar1, DynamicUDVarchar2)
	     Values (@NewProposalRecommendationPlaybookKey,'''+@ProposalRecommendationPassSequenceNumber+''','+ISNULL(@MinCostDecreaseThresholdPercent,'NULL')+','+ISNULL(@CostDecreaseCapPercent, 'NULL')+','+ISNULL(@MinPriceIncreaseThresholdPercent, 'NULL')+','+ ISNULL(@PriceIncreaseCapPercent, 'NULL')+','''+ ISNULL(@WhereClause1, '')+''','''+ ISNULL(@JoinClause1, '')+''','''+ ISNULL(@WhereClause2, '')+''','''+ ISNULL(@JoinClause2, '')+''','''+ ISNULL(@WhereClause3, '')+''','''+ ISNULL(@JoinClause3, '')+''','''+ ISNULL(@AcceptRejectCode_PriceProposal, '')+''','''+ ISNULL(@AcceptRejectCode_CostProposal,'')+''','+ISNULL(@DynamicReasonCode,'NULL')+','+ISNULL(@DynamicEffectiveDate, 'NULL')+','+ ISNULL(@DynamicSuppressionDate, 'NULL')+','+ ISNULL(@SMEMinGPP, 'NULL')+','+ ISNULL(@SMEMaxGPP, 'NULL')+','''+@ProposedPriceOverride+''','''+@ProposedCostOverride+''',0,'''+@DynamicUDVarchar1+''','''+@DynamicUDVarchar2+''')'
	
		INSERT INTO #ScenarioData SELECT  @SQL
	END
	ELSE IF @Command = 'U'
	BEGIN
		INSERT INTO #ScenarioData SELECT  '------------------------------------------------------'
		INSERT INTO #ScenarioData SELECT  '--Updating ATKProposalRecommendation: '+CAST(@ProposalRecommendationKey AS NVARCHAR(100))
		INSERT INTO #ScenarioData SELECT  '------------------------------------------------------'
		INSERT INTO #ScenarioData SELECT  '	
		UPDATE ATKProposalRecommendation SET
		ProposalRecommendationPassSequenceNumber='''+@ProposalRecommendationPassSequenceNumber+''',
		MinCostDecreaseThresholdPercent='+ISNULL(@MinCostDecreaseThresholdPercent,'NULL')+',
		CostDecreaseCapPercent='+ISNULL(@CostDecreaseCapPercent, 'NULL')+',
		MinPriceIncreaseThresholdPercent='+ISNULL(@MinPriceIncreaseThresholdPercent, 'NULL')+',
		PriceIncreaseCapPercent='+ ISNULL(@PriceIncreaseCapPercent, 'NULL')+',
		WhereClause1='''+ ISNULL(@WhereClause1, '')+''',
		JoinClause1='''+ ISNULL(@JoinClause1, '')+''',
		WhereClause2='''+ ISNULL(@WhereClause2, '')+''',
		JoinClause2='''+ ISNULL(@JoinClause2, '')+''',
		WhereClause3='''+ ISNULL(@WhereClause3, '')+''',
		JoinClause3='''+ ISNULL(@JoinClause3, '')+''',
		AcceptRejectCode_PriceProposal='''+ ISNULL(@AcceptRejectCode_PriceProposal, '')+''',
		AcceptRejectCode_CostProposal='''+ ISNULL(@AcceptRejectCode_CostProposal,'')+''',
		DynamicReasonCode='+ISNULL(@DynamicReasonCode,'NULL')+',
		DynamicEffectiveDate='+ISNULL(@DynamicEffectiveDate, 'NULL')+',
		DynamicSuppressionDate='+ ISNULL(@DynamicSuppressionDate, 'NULL')+',
		SMEMinGPP='+ ISNULL(@SMEMinGPP, 'NULL')+',
		SMEMaxGPP='+ ISNULL(@SMEMaxGPP, 'NULL')+',	
		ProposedPriceOverride=''' +@ProposedPriceOverride+''',
		ProposedCostOverride=''' +@ProposedCostOverride+''',
		DynamicUDVarchar1=''' +@DynamicUDVarchar1+''',
		DynamicUDVarchar2=''' +@DynamicUDVarchar2+'''
		Where ProposalRecommendationKey = '+CAST(@ProposalRecommendationKey AS NVARCHAR(100))

	END
	ELSE
	BEGIN
		INSERT INTO #ScenarioData SELECT  '      ------------------------------------------------------'
		INSERT INTO #ScenarioData SELECT  '      Your current ATKProposalRecommendationKey is: '+CAST(@ProposalRecommendationKey AS NVARCHAR(100))
		INSERT INTO #ScenarioData SELECT  '      ------------------------------------------------------'
		INSERT INTO #ScenarioData SELECT  '      --ProposalRecommendationPassSequenceNumber:   '+ISNULL(@ProposalRecommendationPassSequenceNumber, 'NULL')

		IF @ApproachTypeKey = 3 --Only show this for RBC
			INSERT INTO #ScenarioData SELECT  '      --MinCostDecreaseThresholdPercent:            '+ISNULL(@MinCostDecreaseThresholdPercent, 'NULL')
		IF @ApproachTypeKey = 3 --Only show this for RBC
			INSERT INTO #ScenarioData SELECT  '      --CostDecreaseCapPercent:                     '+ISNULL(@CostDecreaseCapPercent, 'NULL')
		IF @ApproachTypeKey = 2 --Only show this for RBP
			INSERT INTO #ScenarioData SELECT  '      --MinPriceIncreaseThresholdPercent:           '+ISNULL(@MinPriceIncreaseThresholdPercent, 'NULL')
		IF @ApproachTypeKey IN (2, 6) --Only show this for RBP & PROS
			INSERT INTO #ScenarioData SELECT  '      --PriceIncreaseCapPercent:                    '+ISNULL(@PriceIncreaseCapPercent, 'NULL')
		INSERT INTO #ScenarioData SELECT  '      --WhereClause1:                               '+ISNULL(@WhereClause1, 'NULL')
		INSERT INTO #ScenarioData SELECT  '      --JoinClause1:                                '+ISNULL(@JoinClause1, 'NULL')
		INSERT INTO #ScenarioData SELECT  '      --WhereClause2:                               '+ISNULL(@WhereClause2, 'NULL')
		INSERT INTO #ScenarioData SELECT  '      --JoinClause2:                                '+ISNULL(@JoinClause2, 'NULL')
		INSERT INTO #ScenarioData SELECT  '      --WhereClause3:                               '+ISNULL(@WhereClause3, 'NULL')
		INSERT INTO #ScenarioData SELECT  '      --JoinClause3:                                '+ISNULL(@JoinClause3, 'NULL')
		INSERT INTO #ScenarioData SELECT  '      --AcceptRejectCode_PriceProposal:             '+ISNULL(@AcceptRejectCode_PriceProposal, 'NULL')
		INSERT INTO #ScenarioData SELECT  '      --AcceptRejectCode_CostProposal:              '+ISNULL(@AcceptRejectCode_CostProposal, 'NULL')
		INSERT INTO #ScenarioData SELECT  '      --DynamicReasonCode:                          '+ISNULL(@DynamicReasonCode, 'NULL')
		INSERT INTO #ScenarioData SELECT  '      --DynamicEffectiveDate:                       '+ISNULL(@DynamicEffectiveDate, 'NULL')
		INSERT INTO #ScenarioData SELECT  '      --DynamicSuppressionDate:                     '+ISNULL(@DynamicSuppressionDate, 'NULL')
		IF @ApproachTypeKey IN (1, 4) --Only show this for ME
			INSERT INTO #ScenarioData SELECT  '      --SMEMinGPP:                                  '+ISNULL(@SMEMinGPP, 'NULL')
		IF @ApproachTypeKey IN (1, 4) --Only show this for ME
			INSERT INTO #ScenarioData SELECT  '      --SMEMaxGPP:                                  '+ISNULL(@SMEMaxGPP, 'NULL')
		INSERT INTO #ScenarioData SELECT  '      --ProposedPriceOverride:                      '+ISNULL(@ProposedPriceOverride, 'NULL')
		INSERT INTO #ScenarioData SELECT  '      --ProposedCostOverride:                       '+ISNULL(@ProposedCostOverride, 'NULL')
		INSERT INTO #ScenarioData SELECT  '      --DynamicUDVarchar1:                          '+ISNULL(@DynamicUDVarchar1, 'NULL')
		INSERT INTO #ScenarioData SELECT  '      --DynamicUDVarchar2:                          '+ISNULL(@DynamicUDVarchar2, 'NULL')

	END

	FETCH NEXT FROM ProposalRecoms INTO @ProposalRecommendationKey, @ProposalRecommendationPassSequenceNumber, @MinCostDecreaseThresholdPercent, @CostDecreaseCapPercent, @MinPriceIncreaseThresholdPercent, @PriceIncreaseCapPercent, @WhereClause1, @JoinClause1, @WhereClause2, @JoinClause2, @WhereClause3, @JoinClause3, @AcceptRejectCode_PriceProposal, @AcceptRejectCode_CostProposal, @DynamicReasonCode, @DynamicEffectiveDate, @DynamicSuppressionDate, @SMEMinGPP, @SMEMaxGPP, @ProposedPriceOverride, @ProposedCostOverride, @DynamicUDVarchar1, @DynamicUDVarchar2
END
CLOSE ProposalRecoms
DEALLOCATE ProposalRecoms

IF @Command = 'N'
BEGIN
	INSERT INTO #ScenarioData SELECT  '-------------------------------------'
	INSERT INTO #ScenarioData SELECT  '-------------------------------------'
END

















GO
