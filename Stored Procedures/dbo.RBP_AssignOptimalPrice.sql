SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



--RBP_AssignOptimalPrice 23, 'N'

CREATE                            PROCEDURE [dbo].[RBP_AssignOptimalPrice]
@PlaybookDataPointGroupKey INT, @Execute CHAR(1) = 'Y'
AS

------------------------------------------------------------------------------------------------
--TBW: 07/16/2005	Added a dynamic @WhereClause and @Join.  Later we should make these
--					core changes that are set through a table column.
------------------------------------------------------------------------------------------------

SET QUOTED_IDENTIFIER ON 
SET ANSI_NULLS ON 
SET ARITHABORT ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
SET CONCAT_NULL_YIELDS_NULL ON
SET NUMERIC_ROUNDABORT OFF
DECLARE @RowsI Quantity_Normal_type, @RowsU Quantity_Normal_type, @RowsD Quantity_Normal_type, @CostingRuleWhereClause NVARCHAR(2000), @tmpValuesAssigned NVARCHAR(2000)
SET @RowsI = 0
SET @RowsU = 0
SET @RowsD = 0

DECLARE @AccountExclusionIndicator NVARCHAR(50)
DECLARE @ProposalRecommendationPlaybookKey INT
DECLARE @SQLStmt NVARCHAR(MAX), @JoinClause_AssignOptimal description_Big_type

DECLARE @WhereClause1 Description_Huge_Type, @JoinClause1 Description_Huge_Type

DECLARE @ScenarioKey INT
DECLARE @ProjectKey key_normal_type
SELECT @ProjectKey = ProjectKey, @ScenarioKey = ScenarioKey FROM PlaybookDataPointGroup_View WHERE PlaybookDataPointGroupKey = @PlaybookDataPointGroupKey

SELECT @ProposalRecommendationPlaybookKey = ProposalRecommendationPlaybookKey FROM ATKScenario
WHERE ScenarioKey = @ScenarioKey


DECLARE @SuppressionTypeKey Key_Small_Type
SELECT @SuppressionTypeKey = SuppressionTypeKey FROM ATKScenario WHERE ScenarioKey = @ScenarioKey

DECLARE @SuppressionJoinClause NVARCHAR(MAX)

SELECT @WhereClause1 = ISNULL(WhereClause1, ''),
		@JoinClause1 = ISNULL(JoinClause1, '')
FROM ATKProposalRecommendationPlaybook
WHERE ProposalRecommendationPlaybookKey = @ProposalRecommendationPlaybookKey

IF @WhereClause1 <> '' AND @WhereClause1 NOT LIKE 'and %'
	SET @WhereClause1 = 'and ' + @WhereClause1


--Now, if we're going to suppress optimals from being generated, add the dynamic SQL here:
IF (SELECT SuppressionTypeCode 
	FROM ATKSuppressionType 
	WHERE SuppressionTypeKey = @SuppressionTypeKey) = 'Optimal'
BEGIN
	SET @SuppressionJoinClause = 
	'left join (Select Distinct AccountKey, ItemKey, InvoiceLineGroup1Key 
			from ProposalPrice
			Where SuppressionDate >= GetDate()) SuppressionList
		on SuppressionList.AccountKey = PlaybookDataPoint.AccountKey
			and SuppressionList.ItemKey = PlaybookDataPoint.ItemKey
			and SuppressionList.InvoiceLineGroup1Key = PlaybookDataPoint.InvoiceLineGroup1Key'
	SET @WhereClause1 = @WhereClause1 + '
		and SuppressionList.AccountKey is null
		'
END
ELSE
	SET @SuppressionJoinClause = ''


--Y if we should use this table to exclude anything
SELECT @AccountExclusionIndicator = AccountExclusionIndicator 
FROM PlaybookProject PP
JOIN ATKProjectSpecification PS ON PS.ProjectSpecificationKey = PP.ProjectSpecificationKey

DECLARE @USql NVARCHAR(MAX)
--If we should exclude accounts
IF @AccountExclusionIndicator = 'Y'
BEGIN
	--Inserts all accounts that have data point exclusions
	INSERT INTO PlaybookProjectAccountExclusion(ProjectKey, AccountKey, WebUserKey, ExclusionType, ExclusionReason, Version, CreationDate, ModificationDate, CreationUser, ModificationUser, ModificationUserAddress, CreationUserAddress)
	SELECT @ProjectKey, AccountKey, WebUserKey, ExclusionType, ExclusionReason, Version, CreationDate, ModificationDate, CreationUser, ModificationUser, ModificationUserAddress, CreationUserAddress 
	FROM ATKAccountExclusion
	WHERE ExclusionType = 'OptimalPrice'

	SET @USql = 'Update Statistics PlaybookProjectAccountExclusion'
	EXEC (@USql)

	--Now, modify the dynamic SQL to use these exclusions:
	SET @JoinClause1 = @JoinClause1 + '
	join (Select AccountKey from PlaybookProjectAccountExclusion where ProjectKey = '+CAST(@ProjectKey AS NVARCHAR(50))+') AccountExclusions
		on AccountExclusions.AccountKey = FactInvoiceLine.AccountKey
	'
	
	--Add a line feed and "And" if necessary
	IF @WhereClause1 NOT LIKE '%and%'
		SET @WhereClause1 = @WhereClause1 + '
	and'

	SET @WhereClause1 = @WhereClause1 + ' AccountExclusions.AccountKey is NULL --Exclude anything found in the exclusion table
	'
END


--Determines if this is a pricing or costing scenario
DECLARE @ApproachTypeKey INT
SELECT @ApproachTypeKey = ApproachTypeKey FROM ATKScenario WHERE ScenarioKey = @ScenarioKey

DECLARE @ApproachTypeDesc NVARCHAR(500)
SELECT @ApproachTypeDesc = ApproachTypeDesc FROM ATKApproachType WHERE ApproachTypeKey = @ApproachTypeKey


EXEC LogDCPEvent 'DCP --- RBP_AssignOptimalPrice', 'B',0,0,0,@ProjectKey

IF @Execute = 'Y'
BEGIN
	DELETE FROM ProposalPrice WHERE PlaybookDataPointGroupKey = @PlaybookDataPointGroupKey
	SET @RowsD = @RowsD + @@RowCount
	DELETE FROM PlaybookPriceProposal WHERE PlaybookDataPointGroupKey = @PlaybookDataPointGroupKey
	SET @RowsD = @RowsD + @@RowCount
	DELETE FROM PlaybookDataPointOptimalPrice WHERE PlaybookDataPointOptimalPriceKey IN (SELECT PlaybookDataPointOptimalPriceKey FROM PlaybookDataPointOptimalPrice_View WHERE PlaybookDataPointGroupKey = @PlaybookDataPointGroupKey)
	SET @RowsD = @RowsD + @@RowCount
END

--This SQL will need to be denamically generated eventually so that
--things other than blended rank can be used in the join to PlaybookPricingGroupOptimalPrice_View

SET @SQLStmt = 
'Insert PlaybookDataPointOptimalPrice 
(PlaybookPricingGroupOptimalPriceKey, 
PlaybookDataPointPricingGroupKey, 
OptimalPrice, 
OptimalCost)
Select 
(Select PlaybookPricingGroupOptimalPriceKey from PlaybookPricingGroupOptimalPrice where PlaybookPricingGroupQuantityBand.PlaybookPricingGroupQuantityBandKey = PlaybookPricingGroupOptimalPrice.PlaybookPricingGroupQuantityBandKey) PlaybookPricingGroupOptimalPriceKey,
PlaybookDataPointPricingGroup.PlaybookDataPointPricingGroupKey,
(Select 
	IsNull(OptimalPrice, IsNull(DimItem.FutureCost,IsNull(FactCurrentPriceAndCost.CurrentItemCost, IsNull(DimItem.ItemAccountManagerCost,PlaybookDataPoint.LastItemCost))) / (1-OptimalMargin)) --This computes what the price would be if we are pricing by margin.  Uses FutreCost, Current AIS cost, ItemACcountManagerCost, or LastItemCost (in order of availability)
	from PlaybookPricingGroupOptimalPrice  where PlaybookPricingGroupQuantityBand.PlaybookPricingGroupQuantityBandKey = PlaybookPricingGroupOptimalPrice.PlaybookPricingGroupQuantityBandKey) as ''OptimalPrice'',
(Select OptimalCost from PlaybookPricingGroupOptimalPrice  where PlaybookPricingGroupQuantityBand.PlaybookPricingGroupQuantityBandKey = PlaybookPricingGroupOptimalPrice.PlaybookPricingGroupQuantityBandKey) as ''OptimalCost''
from PlaybookDataPoint
	join PlaybookDataPointPricingGroup  on PlaybookDataPoint.PlaybookDataPointKey = PlaybookDataPointPricingGroup.PlaybookDataPointKey
	join dbo.PlaybookPricingGroupQuantityBand	On 
		PlaybookDataPoint.Total12MonthQuantity Between PlaybookPricingGroupQuantityBand.LowerBandQuantity and PlaybookPricingGroupQuantityBand.UpperBandQuantity 	--Assigns each data point to a band
		and PlaybookDataPointPricingGroup.PlaybookPricingGroupKey = PlaybookPricingGroupQuantityBand.PlaybookPricingGroupKey
	join DimItem on DimItem.ItemKey = PlaybookDataPoint.ItemKey
	left join FactCurrentPriceAndCost on
		FactCurrentPriceAndCost.ACcountKey = PlaybookDataPoint.AccountKEy
		and FactCurrentPriceAndCost.itemkey = PlaybookDataPoint.itemkey
		and FactCurrentPriceAndCost.invoicelinegroup1key = PlaybookDataPoint.invoicelinegroup1key


' + @JoinClause1 + '
' + @SuppressionJoinClause + '
 	Where 
		PlaybookDataPoint.PlaybookDataPointGroupKey = '+CAST(@PlaybookDataPointGroupKey AS NVARCHAR(500))+'  
'+ @WhereClause1


PRINT @SQLStmt

IF @Execute = 'Y'
	EXEC (@SQLStmt)

SET @RowsI = @RowsI + @@RowCount

SET @USql = 'Update Statistics PlaybookDataPointOptimalPrice'
EXEC (@USql)


EXEC LogDCPEvent 'DCP --- RBP_AssignOptimalPrice', 'E', @RowsI, @RowsU, @RowsD 
























GO
