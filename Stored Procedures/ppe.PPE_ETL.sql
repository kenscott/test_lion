SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [ppe].[PPE_ETL]
WITH EXECUTE AS CALLER
AS
SET QUOTED_IDENTIFIER ON 
SET ANSI_NULLS ON 
SET ARITHABORT ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
SET CONCAT_NULL_YIELDS_NULL ON
SET NUMERIC_ROUNDABORT OFF
SET NOCOUNT ON
SET XACT_ABORT ON



DECLARE
	@ETLMsgStr NVARCHAR(4000)

SELECT @ETLMsgStr = N'ETL - ppe.PPE_ETL'

EXEC LogDCPEvent @ETLMsgStr, N'B'



EXEC Lion_Staging.dbo.AdaptorUser

EXEC lion.Load_ODSPerson

EXEC lion.Load_DimPerson

EXEC lion.Load_WebUserRolesAndPermissions

EXEC lion.Load_BridgeAccountManager	--Load the bridge based on UserAssoc. This is just the parent/child managed by relationship between account managers

EXEC lion.Load_WebUserAccountManagerAccessType

EXEC lion.Load_WebUserDelegate


/* in theory, we could have just created uers that own accounts */
EXEC lion.Load_DimAccount
EXEC lion.Load_ResetAccountManagerKey
EXEC lion.Load_UpdateDimAccountManager	-- Sets the PrimaryAccountGroup1Key in DimAccountManager


EXEC ppe.Set_RequestHierarchyChange
EXEC ppe.Set_RequestAutoEscalate

EXEC lion.Load_DimVendor
EXEC cc.Load_WUKContracts
EXEC cc.VerifyContractLocation

/* Final Cleanup */
EXEC dbo.DBA_RemainingFKRebuild
--EXEC dbo.DBA_UpdateStatistics



EXEC LogDCPEvent @ETLMsgStr, N'E'






GO
