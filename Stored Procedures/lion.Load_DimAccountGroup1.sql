SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [lion].[Load_DimAccountGroup1]
WITH EXECUTE AS CALLER
AS
SET NOCOUNT ON

--This is the geographic location grouping for each account

EXEC LogDCPEvent 'ETL - DimAccountGroup1', 'B'

--This table will contain a row for every row in the dimension table that has been changed in the staging table (type 1 update will be done)
CREATE TABLE #UpdateKeys (KeyID INTEGER)

--EXEC DBA_IndexRebuild 'DimAccountGroup1', 'Drop'

DECLARE 
	@RowsI Quantity_Normal_type, 
	@RowsU Quantity_Normal_type, 
	@RowsD Quantity_Normal_type,
	@BeginRowCount INT,
	@TodayDayKey Key_Small_Type,
	@RehanEmail Description_Normal_type,
	@CoordinatorEmail Description_Normal_type,
	@RehanName Description_Normal_type,
	@CoordinatorName Description_Normal_type,
	@RehanPersonKey Key_Normal_Type,
	@CoordinatorPersonKey Key_Normal_Type

SET @RowsI = 0
SET @RowsU = 0
SET @RowsD = 0


-- TRUNCATE TABLE DimAccountGroup1


--This is used to look up the date key in the DimDate table
SET @TodayDayKey = (SELECT DayKey FROM DimDay WHERE CONVERT(VARCHAR(10), SQLDate, 120) = CONVERT(VARCHAR(10), GETDATE(), 120))


SET @RehanEmail = N'rehan.mogul@wolseley.co.uk'
SET @CoordinatorEmail = N'Coordinator'

SELECT
	@RehanName = FullName,
	@RehanPersonKey = DimPersonKey
FROM
	DimPerson 
WHERE Email = @RehanEmail

SELECT
	@CoordinatorName = FullName,
	@CoordinatorPersonKey = DimPersonKey
FROM
	DimPerson 
WHERE Email = @CoordinatorEmail


SELECT @BeginRowCount = COUNT(*) FROM dbo.DimAccountGroup1

; WITH HData AS (
	SELECT DISTINCT
		[Level1 Assignment] AS Branch,
		[Level1 Name] AS BranchManagerName,
		[Level1 Email Address] AS BranchManagerEmail,
		ROW_NUMBER() OVER (
			PARTITION BY [Level1 Assignment] 
				ORDER BY 
					CASE 
						WHEN [level1 title] = 'Branch Manager' THEN 1
						ELSE 2
					END,
				[Level1 Email Address] COLLATE SQL_Latin1_General_CP1_CS_AS DESC
		) AS RowRank
	FROM Lion_Staging.dbo.UserHierarchy uh
	WHERE
		ISNULL([Level1 Email Address], N'') <> N''
)
SELECT
	Branch,
	BranchManagerName,
	BranchManagerEmail
INTO #HData
FROM HData
WHERE HData.RowRank = 1

CREATE UNIQUE CLUSTERED INDEX I_1 ON #HData(Branch)



; WITH BrandData AS (
	SELECT DISTINCT
		[Level1 Assignment] AS Branch,
		[Level0 Name] AS BrandManagerName,
		[Level0 Email Address] AS BrandManagerEmail,
		MAX([Level0 Brand]) AS Brand
	FROM Lion_Staging.dbo.UserHierarchy uh
	WHERE
		ISNULL([Level0 Email Address], N'') <> N''
		AND [level1 title] = 'Branch Manager'
		AND [Level0 Email Address] = [Level1 Email Address]
	GROUP BY 
		[Level1 Assignment],
		[Level0 Name],
		[Level0 Email Address]
	HAVING COUNT(*) = 1
), BrandData2 AS (
	SELECT
		Branch,
		BrandManagerName,
		BrandManagerEmail,
		Brand,
		ROW_NUMBER() OVER (
			PARTITION BY 
				Branch
			ORDER BY 
				BrandManagerEmail COLLATE SQL_Latin1_General_CP1_CS_AS DESC
		) AS RowRank
	FROM BrandData
)
SELECT
	Branch,
	BrandManagerName,
	BrandManagerEmail,
	Brand
INTO #BrandData
FROM BrandData2
WHERE
	RowRank = 1

CREATE UNIQUE CLUSTERED INDEX I_1 ON #BrandData(Branch)


IF NOT EXISTS (SELECT 1 FROM DimAccountGroup1 WHERE AG1Level1 = 'Unknown')
BEGIN
	INSERT DimAccountGroup1	(
		AG1Level1,
		AG1Level2,
		AG1Level3,
		AG1Level4, 
		AG1Level5, 
		AG1Level1UDVarchar1, 
		AG1Level1UDVarchar2,
		AG1Level1UDVarchar3,
		AG1Level1UDVarchar4,
		AG1Level1UDVarchar5,
		AG1Level1UDVarchar20,
		CreationDayKey, 
		ModificationDayKey)
	VALUES (
		'Unknown',
		'Unknown', 
		'Unknown',
		'Unknown',
		'Unknown',
		'Unknown',
		'Unknown', 
		'Unknown',
		'Unknown',
		'Unknown',
		'N',
		@TodayDayKey, 
		@TodayDayKey)

	SET @RowsI = @RowsI + @@RowCount
END

UPDATE dbo.DimAccountGroup1 SET NetworkKey = 1 WHERE NetworkKey IS NULL
UPDATE dbo.DimAccountGroup1 SET AreaKey = 1 WHERE AreaKey IS NULL
UPDATE dbo.DimAccountGroup1 SET RegionKey = 1 WHERE RegionKey IS NULL
UPDATE dbo.DimAccountGroup1 SET BrandKey = 1 WHERE BrandKey IS NULL

;
WITH StagingData AS (
	SELECT DISTINCT
	
		b.BranchCode AS BranchCode,--1
		ISNULL(b.NetworkID, N'Unknown') AS NetworkID,--2
		ISNULL(b.ReportAreaId, N'Unknown') AS AreaId,--3
		ISNULL(b.ReportRegionId, N'Unknown') AS RegionId,--4
		
		ISNULL(n.NetworkKey, 1) AS NetworkKey,
		ISNULL(a.AreaKey, 1) AS AreaKey,
		ISNULL(r.RegionKey, 1) AS RegionKey,

		LTRIM(RTRIM(ISNULL(b.BranchName, ''))) AS BranchName,--1			
		ISNULL(b.NetworkDesc, N'Unknown') AS NetworkDescription,--2
		ISNULL(b.ReportAreaDesc, N'Unknown') AS AreaDescription,--3
		ISNULL(b.ReportRegionDesc, N'Unknown')AS RegionDescription,--4	

		N'Wolseley' AS CompanyName,--5

		CAST(ISNULL(NULLIF(bm.PlaybookRegionID, N'NULL'), N'National') AS NVARCHAR(250)) AS PlaybookRegionID,
		CAST(ISNULL(NULLIF(bm.PlaybookRegionDescription, N'NULL'), N'National') AS NVARCHAR(250)) AS PlaybookRegionDescription,
		CAST(ISNULL(NULLIF(bm.PlaybookAreaID, N'NULL'), N'National') AS NVARCHAR(250)) AS PlaybookAreaID,
		CAST(ISNULL(NULLIF(bm.PlaybookAreaDescription, N'NULL'), N'National') AS NVARCHAR(250)) AS PlaybookAreaDescription,

		CASE
			WHEN bm.PGActive IS NULL OR bm.PGActive <> N'Y' THEN N'N'
			ELSE N'Y'
		END AS PGActiveIndicator,		

		CAST(
			CASE
				WHEN LTRIM(RTRIM(ISNULL(b.PrimaryBrand, N''))) IN (N'BURDN') THEN N'BURDN'
				WHEN LTRIM(RTRIM(ISNULL(b.PrimaryBrand, N''))) IN (N'PIPE', N'CLIMT') THEN N'P&C'
				WHEN LTRIM(RTRIM(ISNULL(b.PrimaryBrand, N''))) IN (N'PLUMB', N'PARTS') THEN N'P&P'
				ELSE LTRIM(RTRIM(ISNULL(b.PrimaryBrand, N'')))
			END 
		AS NVARCHAR(250)) AS BrandGroup,

		CAST(
			CASE
				WHEN LTRIM(RTRIM(ISNULL(b.PrimaryBrand, N''))) IN (N'BURDN') THEN N'N'
				ELSE N'Y'
			END 
		AS NVARCHAR(250))  AS PlaybookBrandIndicator,

		ISNULL(
			CASE
				WHEN ISNULL(PlaybookRegionID, N'') = N'NI' THEN N'NI'
				ELSE N'N'
			END, 
		N'N') AS RegionExceptionIndicator,

		ISNULL(brand.BrandKey, 1) AS BrandKey,
		LTRIM(RTRIM(ISNULL(b.PrimaryBrand, N''))) AS PrimaryBrand,
		LTRIM(RTRIM(ISNULL(b.TradingStatus, N''))) AS TradingStatus,
		LTRIM(RTRIM(ISNULL(b.BranchType, N''))) AS BranchType,
		LTRIM(RTRIM(ISNULL(b.AddressLine1, N''))) AS AddressLine1,
		LTRIM(RTRIM(ISNULL(b.AddressLine2, N''))) AS AddressLine2,
		LTRIM(RTRIM(ISNULL(b.AddressLine3, N''))) AS AddressLine3,
		LTRIM(RTRIM(ISNULL(b.AddressLine4, N''))) AS AddressLine4,
		LTRIM(RTRIM(ISNULL(b.AddressLine5, N''))) AS AddressLine5,
		LTRIM(RTRIM(ISNULL(b.Postcode, N''))) AS Postcode,
		LTRIM(RTRIM(ISNULL(EmailAddress, N''))) AS Email,
		LTRIM(RTRIM(ISNULL(b.Telephone, N''))) AS Telephone,

		CASE
			WHEN PATINDEX(N'%[^a-z]%', LTRIM(RTRIM(ISNULL(b.Postcode, N'')))) > 1 THEN 
				SUBSTRING(LTRIM(RTRIM(ISNULL(b.Postcode, N''))), 1, PATINDEX(N'%[^a-z]%', LTRIM(RTRIM(ISNULL(b.Postcode, N'')))) - 1 )
			ELSE NULL
		END AS CustomerBranchPostCodeArea,
		CASE
			WHEN PATINDEX(N'%[^a-z]%', LTRIM(RTRIM(ISNULL(b.Postcode, N'')))) > 1 
			AND CHARINDEX(N' ',LTRIM(RTRIM(ISNULL(b.Postcode, N'')))) > 1 THEN 
				SUBSTRING(LTRIM(RTRIM(ISNULL(b.Postcode, N''))), PATINDEX(N'%[^a-z]%', LTRIM(RTRIM(ISNULL(b.Postcode, N'')))), 
					CHARINDEX(N' ',LTRIM(RTRIM(ISNULL(b.Postcode, N''))))
					- 
					PATINDEX(N'%[^a-z]%', LTRIM(RTRIM(ISNULL(b.Postcode, N''))))
				)
			ELSE NULL
		END AS CustomerBranchPostCodeDistrict,
		b.PyrCode AS PyramidCode,

		COALESCE(BranchManagerName, @RehanName, @CoordinatorName, 'Unknown') AS Manager,
		COALESCE(hd.BranchManagerEmail, @RehanEmail, @CoordinatorEmail, 'Unknown') AS BranchManagerEmail,
		COALESCE(dp.DimPersonKey, @RehanPersonKey, @CoordinatorPersonKey, 1) AS BranchManagerPersonKey,

		b.Region AS BusinessUnitRegion,

		CASE 
			WHEN BusUnitTypeDesc = 'BRANCH'
				AND b.PyrCode IN (1, 2)
				AND ISNULL(DateClosed, '')  = ''
				AND LTRIM(RTRIM(ISNULL(b.BranchName, ''))) NOT LIKE '%CLOSE%'
				AND LTRIM(RTRIM(ISNULL(b.BranchName, ''))) NOT LIKE '%MERGE%'
				AND LTRIM(RTRIM(ISNULL(b.BranchName, ''))) NOT LIKE '%MOVE%'
				AND ISNULL(b.Telephone, '') NOT LIKE '%CLOSE%'
				AND ISNULL(b.Telephone, '') NOT LIKE '%MERGE%'
				AND ISNULL(b.Telephone, '') NOT LIKE '%MOVE%'
				AND ISNULL(b.Manager, '') NOT LIKE '%CLOSE%'								-- updated as per https://enterbridge.atlassian.net/browse/LP2P-1036
				AND ISNULL(b.Manager, '') NOT LIKE '%MERGE%'
				AND ISNULL(b.Manager, '') NOT LIKE '%MOVE%'
				AND ISNULL(b.ReportAreaDesc, N'Unknown') NOT LIKE '%CLOSE%'
				AND ISNULL(b.ReportAreaDesc, N'Unknown') NOT LIKE '%MERGE%'
				AND ISNULL(b.ReportAreaDesc, N'Unknown') NOT LIKE '%MOVE%'
				AND ISNULL(b.ReportRegionDesc, N'Unknown') NOT LIKE '%CLOSE%'
				AND ISNULL(b.ReportRegionDesc, N'Unknown') NOT LIKE '%MERGE%'
				AND ISNULL(b.ReportRegionDesc, N'Unknown') NOT LIKE '%MOVE%'
			THEN 0
			ELSE 1
		END AS Inactive,																		-- update criteria for: https://enterbridge.atlassian.net/browse/LPF-3163

		ROW_NUMBER() OVER (
			PARTITION BY b.BranchCode
			ORDER BY 
				CASE
					WHEN BusUnitTypeDesc = N'BRANCH' THEN 1
					WHEN BusUnitTypeDesc = N'WOLCEN_AREA' THEN 2
					WHEN BusUnitTypeDesc = N'WOLCEN_REGION' THEN 3
					WHEN BusUnitTypeDesc = N'REPORTING_AREA' THEN 4
					WHEN BusUnitTypeDesc = N'REPORTING_REGION' THEN 5
					WHEN BusUnitTypeDesc = N'COMPANY' THEN 6
					WHEN BusUnitTypeDesc = N'DIVISION' THEN 7
					WHEN BusUnitTypeDesc = N'SALES_CELL' THEN 8
					WHEN BusUnitTypeDesc = N'FEEDER' THEN 9
					WHEN BusUnitTypeDesc = N'' THEN 10
					ELSE 11
				END,
				LTRIM(RTRIM(ISNULL(b.BranchName, N'')))) 
		AS RowRank
	FROM Lion_Staging.dbo.BusUnit b
	LEFT JOIN Lion_Staging.dbo.BranchMaster bm
		ON bm.BranchCode = b.BranchCode
	LEFT JOIN #HData hd
		ON hd.Branch = b.BranchCode
	LEFT JOIN #BrandData BrandData
		ON BrandData.Branch = b.BranchCode
	LEFT JOIN DimPerson dp
		ON dp.Email = hd.BranchManagerEmail
	LEFT JOIN dbo.DimNetwork n
		ON n.NetworkId = ISNULL(b.NetworkID, N'Unknown') 
	LEFT JOIN dbo.DimArea a
		ON a.Area = ISNULL(b.Area, N'Unknown') 
	LEFT JOIN dbo.DimRegion r
		ON r.Region = ISNULL(b.Region, N'Unknown') 
	LEFT JOIN dbo.DimBrand brand
		ON brand.Brand = ISNULL(BrandData.Brand,  LTRIM(RTRIM(ISNULL(b.PrimaryBrand, N''))) )
	WHERE
		b.BranchCode IS NOT NULL
		AND b.BranchCode <> N'NULL'
)
MERGE dbo.DimAccountGroup1 AS Tgt
USING 
	(
	SELECT

		BranchCode AS AG1Level1,--1
		NetworkID AS AG1Level2,--2
		AreaId AS AG1Level3,--3
		RegionId AS AG1Level4,--4
		CompanyName AS AG1Level5,--5

		NetworkKey,
		AreaKey,
		RegionKey,

		BranchName AS AG1Level1UDVarchar1,--1	
		NetworkDescription AS AG1Level2UDVarchar1,--2
		AreaDescription AS AG1Level3UDVarchar1,--3
		RegionDescription AS AG1Level4UDVarchar1,--4
		CompanyName AS AG1Level5UDVarchar1,--5

		PlaybookRegionID          AS AG1Level1UDVarchar2,
		PlaybookRegionDescription AS AG1Level1UDVarchar3,
		PlaybookAreaID            AS AG1Level1UDVarchar4,
		PlaybookAreaDescription   AS AG1Level1UDVarchar5,

		PrimaryBrand  AS AG1Level1UDVarchar6,
		BrandKey AS BrandKey,

		TradingStatus AS AG1Level1UDVarchar7,
		BranchType    AS AG1Level1UDVarchar8,
		AddressLine1  AS AG1Level1UDVarchar9,
		AddressLine2  AS AG1Level1UDVarchar10,
		AddressLine3  AS AG1Level1UDVarchar11,
		AddressLine4  AS AG1Level1UDVarchar12,
		AddressLine5  AS AG1Level1UDVarchar13,
		Postcode      AS AG1Level1UDVarchar14,
		Email         AS AG1Level1UDVarchar15,
		Telephone     AS AG1Level1UDVarchar16,

		CustomerBranchPostCodeArea AS AG1Level1UDVarchar17,

		CustomerBranchPostCodeDistrict AS AG1Level1UDVarchar18,
		PyramidCode AS AG1Level1UDVarchar19,
				
		--PlaybookBrandIndicator AS AG1Level1UDVarchar20,
		N'Y' AS AG1Level1UDVarchar20,
		BrandGroup AS AG1Level1UDVarchar21,
		PGActiveIndicator AS AG1Level1UDVarchar22,

		Manager AS AG1Level1UDVarchar23,
		BranchManagerEmail AS AG1Level1UDVarchar24,
		BranchManagerPersonKey AS AG1Level1PersonKey,
		
		RegionExceptionIndicator AS AG1Level1UDVarchar25,

		BusinessUnitRegion AS AG1Level1UDVarchar26,
		Inactive
	FROM 
		StagingData
	WHERE
		RowRank = 1
) AS Src
ON Tgt.AG1Level1 = Src.AG1Level1
WHEN MATCHED AND
	CHECKSUM(--CheckSum for target DimAccount table.  This needs to check all rows that we will UPDATE IF a find a change
		Tgt.AG1Level2,
		Tgt.AG1Level3,
		Tgt.AG1Level4,
		Tgt.AG1Level5,
		Tgt.NetworkKey,
		Tgt.AreaKey,
		Tgt.RegionKey,
		Tgt.BrandKey,
		Tgt.AG1Level1UDVarchar1, 
		Tgt.AG1Level1UDVarchar2, 
		Tgt.AG1Level1UDVarchar3, 
		Tgt.AG1Level1UDVarchar4,
		Tgt.AG1Level1UDVarchar5,
		Tgt.AG1Level1UDVarchar6,
		Tgt.AG1Level1UDVarchar7,
		Tgt.AG1Level1UDVarchar8,
		Tgt.AG1Level1UDVarchar9,
		Tgt.AG1Level1UDVarchar10,
		Tgt.AG1Level1UDVarchar11,
		Tgt.AG1Level1UDVarchar12,
		Tgt.AG1Level1UDVarchar13,
		Tgt.AG1Level1UDVarchar14,
		Tgt.AG1Level1UDVarchar15,
		Tgt.AG1Level1UDVarchar16,
		Tgt.AG1Level1UDVarchar17,
		Tgt.AG1Level1UDVarchar18,
		Tgt.AG1Level1UDVarchar19,
		Tgt.AG1Level1UDVarchar20,
		Tgt.AG1Level1UDVarchar21,
		Tgt.AG1Level1UDVarchar22,
		Tgt.AG1Level1UDVarchar23,
		Tgt.AG1Level1UDVarchar24,
		Tgt.AG1Level1PersonKey,
		Tgt.AG1Level1UDVarchar25,
		Tgt.AG1Level1UDVarchar26,
		Tgt.AG1Level2UDVarchar1,
		Tgt.AG1Level2UDVarchar1,
		Tgt.AG1Level3UDVarchar1,
		Tgt.AG1Level4UDVarchar1,
		Tgt.AG1Level5UDVarchar1,
		Tgt.Inactive)
	<>
	CHECKSUM(--CheckSum for staging table
		Src.AG1Level2,
		Src.AG1Level3,
		Src.AG1Level4,
		Src.AG1Level5,
		Src.NetworkKey,
		Src.AreaKey,
		Src.RegionKey,
		Src.BrandKey,
		Src.AG1Level1UDVarchar1, 
		Src.AG1Level1UDVarchar2, 
		Src.AG1Level1UDVarchar3, 
		Src.AG1Level1UDVarchar4,
		Src.AG1Level1UDVarchar5,
		Src.AG1Level1UDVarchar6,
		Src.AG1Level1UDVarchar7,
		Src.AG1Level1UDVarchar8,
		Src.AG1Level1UDVarchar9,
		Src.AG1Level1UDVarchar10,
		Src.AG1Level1UDVarchar11,
		Src.AG1Level1UDVarchar12,
		Src.AG1Level1UDVarchar13,
		Src.AG1Level1UDVarchar14,
		Src.AG1Level1UDVarchar15,
		Src.AG1Level1UDVarchar16,
		Src.AG1Level1UDVarchar17,
		Src.AG1Level1UDVarchar18,
		Src.AG1Level1UDVarchar19,
		Src.AG1Level1UDVarchar20,
		Src.AG1Level1UDVarchar21,
		Src.AG1Level1UDVarchar22,
		Src.AG1Level1UDVarchar23,
		Src.AG1Level1UDVarchar24,
		Src.AG1Level1PersonKey,
		Src.AG1Level1UDVarchar25,
		Src.AG1Level1UDVarchar26,
		Src.AG1Level2UDVarchar1,
		Src.AG1Level2UDVarchar1,
		Src.AG1Level3UDVarchar1,
		Src.AG1Level4UDVarchar1,
		Src.AG1Level5UDVarchar1,
		Src.Inactive)
	THEN UPDATE 
		SET
			Tgt.ModificationDayKey = @TodayDayKey,
			Tgt.AG1Level2 =             Src.AG1Level2,
			Tgt.AG1Level3 =				Src.AG1Level3,
			Tgt.AG1Level4 =				Src.AG1Level4,
			Tgt.AG1Level5 =				Src.AG1Level5,
			Tgt.NetworkKey =			Src.NetworkKey,
			Tgt.AreaKey =				Src.AreaKey,
			Tgt.RegionKey =				Src.RegionKey,
			Tgt.BrandKey =				Src.BrandKey,
			Tgt.AG1Level1UDVarchar1 =	Src.AG1Level1UDVarchar1, 
			Tgt.AG1Level1UDVarchar2 =	Src.AG1Level1UDVarchar2, 
			Tgt.AG1Level1UDVarchar3 =	Src.AG1Level1UDVarchar3, 
			Tgt.AG1Level1UDVarchar4 =	Src.AG1Level1UDVarchar4,
			Tgt.AG1Level1UDVarchar5 =	Src.AG1Level1UDVarchar5,
			Tgt.AG1Level1UDVarchar6 =	Src.AG1Level1UDVarchar6,
			Tgt.AG1Level1UDVarchar7 =	Src.AG1Level1UDVarchar7,
			Tgt.AG1Level1UDVarchar8 =	Src.AG1Level1UDVarchar8,
			Tgt.AG1Level1UDVarchar9 =	Src.AG1Level1UDVarchar9,
			Tgt.AG1Level1UDVarchar10 =	Src.AG1Level1UDVarchar10,
			Tgt.AG1Level1UDVarchar11 =	Src.AG1Level1UDVarchar11,
			Tgt.AG1Level1UDVarchar12 =	Src.AG1Level1UDVarchar12,
			Tgt.AG1Level1UDVarchar13 =	Src.AG1Level1UDVarchar13,
			Tgt.AG1Level1UDVarchar14 =	Src.AG1Level1UDVarchar14,
			Tgt.AG1Level1UDVarchar15 =	Src.AG1Level1UDVarchar15,
			Tgt.AG1Level1UDVarchar16 =	Src.AG1Level1UDVarchar16,
			Tgt.AG1Level1UDVarchar17 =	Src.AG1Level1UDVarchar17,
			Tgt.AG1Level1UDVarchar18 =	Src.AG1Level1UDVarchar18,
			Tgt.AG1Level1UDVarchar19 =  Src.AG1Level1UDVarchar19,
			Tgt.AG1Level1UDVarchar20 =  Src.AG1Level1UDVarchar20,
			Tgt.AG1Level1UDVarchar21 =  Src.AG1Level1UDVarchar21,
			Tgt.AG1Level1UDVarchar22 =  Src.AG1Level1UDVarchar22,
			Tgt.AG1Level1UDVarchar23 = Src.AG1Level1UDVarchar23,
			Tgt.AG1Level1UDVarchar24 = Src.AG1Level1UDVarchar24,
			Tgt.AG1Level1PersonKey = Src.AG1Level1PersonKey,
			Tgt.AG1Level1UDVarchar25 = Src.AG1Level1UDVarchar25,
			Tgt.AG1Level1UDVarchar26 = Src.AG1Level1UDVarchar26,
			Tgt.AG1Level2UDVarchar1  = 	Src.AG1Level2UDVarchar1,
			Tgt.AG1Level3UDVarchar1  = 	Src.AG1Level3UDVarchar1,
			Tgt.AG1Level4UDVarchar1  = 	Src.AG1Level4UDVarchar1,
			Tgt.AG1Level5UDVarchar1  = 	Src.AG1Level5UDVarchar1,
			Tgt.Inactive = Src.Inactive
WHEN NOT MATCHED 
	THEN INSERT (
		CreationDayKey,
		ModificationDayKey,
		AG1Level1,
		AG1Level2,
		AG1Level3,
		AG1Level4,
		AG1Level5,
		NetworkKey,
		AreaKey,
		RegionKey,
		BrandKey,
		AG1Level1UDVarchar1, 
		AG1Level1UDVarchar2, 
		AG1Level1UDVarchar3, 
		AG1Level1UDVarchar4,
		AG1Level1UDVarchar5,
		AG1Level1UDVarchar6,
		AG1Level1UDVarchar7,
		AG1Level1UDVarchar8,
		AG1Level1UDVarchar9,
		AG1Level1UDVarchar10,
		AG1Level1UDVarchar11,
		AG1Level1UDVarchar12,
		AG1Level1UDVarchar13,
		AG1Level1UDVarchar14,
		AG1Level1UDVarchar15,
		AG1Level1UDVarchar16,
		AG1Level1UDVarchar17,
		AG1Level1UDVarchar18,
		AG1Level1UDVarchar19,
		AG1Level1UDVarchar20,
		AG1Level1UDVarchar21,
		AG1Level1UDVarchar22,
		AG1Level1UDVarchar23,
		AG1Level1UDVarchar24,
		AG1Level1PersonKey,
		AG1Level1UDVarchar25,
		AG1Level1UDVarchar26,
		AG1Level2UDVarchar1,
		AG1Level3UDVarchar1,
		AG1Level4UDVarchar1,
		AG1Level5UDVarchar1,
		Inactive
		)
	VALUES (
		@TodayDayKey,
		@TodayDayKey,
		Src.AG1Level1,
		Src.AG1Level2,
		Src.AG1Level3,
		Src.AG1Level4,
		Src.AG1Level5,
		Src.NetworkKey,
		Src.AreaKey,
		Src.RegionKey,
		Src.BrandKey,
		Src.AG1Level1UDVarchar1, 
		Src.AG1Level1UDVarchar2, 
		Src.AG1Level1UDVarchar3, 
		Src.AG1Level1UDVarchar4,
		Src.AG1Level1UDVarchar5,
		Src.AG1Level1UDVarchar6,
		Src.AG1Level1UDVarchar7,
		Src.AG1Level1UDVarchar8,
		Src.AG1Level1UDVarchar9,
		Src.AG1Level1UDVarchar10,
		Src.AG1Level1UDVarchar11,
		Src.AG1Level1UDVarchar12,
		Src.AG1Level1UDVarchar13,
		Src.AG1Level1UDVarchar14,
		Src.AG1Level1UDVarchar15,
		Src.AG1Level1UDVarchar16,
		Src.AG1Level1UDVarchar17,
		Src.AG1Level1UDVarchar18,
		Src.AG1Level1UDVarchar19,
		Src.AG1Level1UDVarchar20,
		Src.AG1Level1UDVarchar21,
		Src.AG1Level1UDVarchar22,
		Src.AG1Level1UDVarchar23,
		Src.AG1Level1UDVarchar24,
		Src.AG1Level1PersonKey,
		Src.AG1Level1UDVarchar25,
		Src.AG1Level1UDVarchar26,
		Src.AG1Level2UDVarchar1,
		Src.AG1Level3UDVarchar1,
		Src.AG1Level4UDVarchar1,
		Src.AG1Level5UDVarchar1,
		Src.Inactive
		)
;

SELECT 
	@RowsU = @RowsU + (@@RowCOunt - (COUNT(*) - @BeginRowCount)),
	@RowsI =  COUNT(*) - @BeginRowCount
FROM dbo.DimAccountGroup1



DROP TABLE #HData
DROP TABLE #BrandData


--EXEC DBA_IndexRebuild 'DimAccountGroup1', 'Create'

SELECT TOP 10 * FROM DimAccountGroup1

EXEC LogDCPEvent 'ETL - DimAccountGroup1', 'E', @RowsI, @RowsU, @RowsD








GO
