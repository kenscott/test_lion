SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE PROC [cc].[Get_ContractXML] (@ContractKey INT)
AS BEGIN

	DECLARE @XMLText XML; 

	WITH XMLNAMESPACES ('http://www.wolseley.co.uk/apps/EnterpriseObjects/EBO/Contract/V1/Contract' as contractns,
					'http://www.wolseley.co.uk/apps/EnterpriseObjects/EBO/Common/V1/Common' AS commonns,
					'http://www.wolseley.co.uk/apps/EnterpriseObjects/EBO/Document/V1/Document' AS documentns,
					'http://www.wolseley.co.uk/apps/EnterpriseObjects/EBO/Party/V1/Party' AS partyns,
					'http://www.wolseley.co.uk/apps/EnterpriseObjects/EBO/Customer/V1/Customer' AS customerns,
					'http://www.wolseley.co.uk/apps/EnterpriseObjects/EBO/Supplier/V1/Supplier' AS supplierns,
					'http://www.wolseley.co.uk/apps/EnterpriseObjects/EBO/Product/V1/Product' AS productns,
					'http://www.wolseley.co.uk/apps/EnterpriseObjects/EBO/Order/V1/Order' AS orderns)

	SELECT @XMLText =  (SELECT
	ContractKey AS 'commonns:ID',
	ContractKey AS 'contractns:ContractId',
	CAST(StartDate AS DATETIME) AS 'contractns:ContractEffectiveStartDate',
	CAST(ExpirationDate AS DATETIME) AS 'contractns:ContractEffectiveEndDate',
	(SELECT ISNULL(StatusValue,'Unknown') FROM cc.SupplierContractStatus dcs WHERE dcs.StatusKey = SupplierContract.StatusKey) AS 'contractns:ContractStatus',

	--/*ContractDocument*/
	(SELECT ContractDocumentKey AS 'commonns:ID', 
		ColumbusDocumentId AS 'documentns:DocumentId',
		(SELECT CreatedBy.CreationDate AS 'commonns:CreatedDateTime', ISNULL(wu.UserName,'Unknown') AS 'commonns:UserId' FROM cc.SupplierContractDocument CreatedBy 
			INNER JOIN dbo.WebUser wu ON CreatedBy.CreatedByWebUserKey = wu.WebUserKey
			WHERE CreatedBy.ContractDocumentKey = ContractDocument.ContractDocumentKey
			AND CreatedBy.ContractKey = ContractDocument.ContractKey FOR XML PATH('commonns:CreatedBy') , TYPE, ELEMENTS),		
			DocumentName AS 'documentns:DocumentFilePath'
		FROM cc.SupplierContractDocument ContractDocument WHERE ContractDocument.ContractKey = SupplierContract.ContractKey FOR XML PATH('contractns:ContractDocument') , TYPE, ELEMENTS),

	/*ContractNotes*/
	(SELECT ContractNoteKey AS 'commonns:ID', 
			(SELECT Username FROM dbo.WebUser WHERE WebUserKey = ContractNotes.WebUserKey)  AS 'commonns:Author',
			CreationDate  AS 'commonns:EntryDatestamp',
			Notes  AS 'commonns:Content' 
		FROM cc.SupplierContractNote ContractNotes WHERE ContractNotes.ContractKey = SupplierContract.ContractKey ORDER BY CreationDate DESC FOR XML PATH('contractns:ContractNotes') , TYPE, ELEMENTS),

	--/*RelatedContract*/
	CASE WHEN PricingContractIndicator <> '' THEN 
			(SELECT SupplierContract.ContractKey AS 'contractns:ContractId'
				,SupplierContract.PricingContractID AS 'contractns:RelatedContractId'
				,SupplierContract.PricingContractIndicator AS 'contractns:RelatedContractTypeCode'
				,'Pricing' AS 'contractns:ContractRelationshipTypeName'
			FROM cc.SupplierContract RelatedContract WHERE RelatedContract.ContractKey = SupplierContract.ContractKey FOR XML PATH('contractns:RelatedContract') , TYPE, ELEMENTS)
		ELSE NULL
	END,

	/*OrganisationUnitContract*/
	(SELECT ContractKey AS 'commonns:ID'
			,Location AS 'partyns:OrganisationUnitId' 
			,UPPER(LocationLevelName) AS 'partyns:OrganisationUnitTypeName'
		,(SELECT /*LPF-3469*/
						CAST(ISNULL(OrganisationUnitContract.StartDate,EffectiveTimePeriod.StartDate ) AS DATETIME) AS 'commonns:StartDateTime', 
						CAST(ISNULL(OrganisationUnitContract.ExpirationDate,EffectiveTimePeriod.ExpirationDate ) AS DATETIME) AS 'commonns:EndDateTime' 
					FROM cc.SupplierContract EffectiveTimePeriod 
					WHERE EffectiveTimePeriod.ContractKey=SupplierContract.ContractKey FOR XML PATH('commonns:EffectiveTimePeriod') , TYPE, ELEMENTS)  
		,(SELECT CreatedOnDT AS 'commonns:CreatedDateTime', CreatedByUserName AS 'commonns:UserId' FROM cc.[vw_ContractLocation] CreatedBy 
			WHERE CreatedBy.LocationKey=OrganisationUnitContract.LocationKey 
			AND CreatedBy.ContractLocationLevelKey = OrganisationUnitContract.ContractLocationLevelKey
			AND CreatedBy.ContractKey = OrganisationUnitContract.ContractKey FOR XML PATH('commonns:CreatedBy') , TYPE, ELEMENTS) 

		,(SELECT ModifiedOnDT AS 'commonns:AmendedDateTime', ModifiedByUserName AS 'commonns:UserId' FROM cc.[vw_ContractLocation] AmendedBy 
			WHERE AmendedBy.LocationKey=OrganisationUnitContract.LocationKey 
			AND AmendedBy.ContractLocationLevelKey = OrganisationUnitContract.ContractLocationLevelKey
			AND AmendedBy.ContractKey = OrganisationUnitContract.ContractKey FOR XML PATH('commonns:AmendedBy') , TYPE, ELEMENTS) 
	FROM cc.[vw_ContractLocation] OrganisationUnitContract WHERE SupplierContract.ContractKey=OrganisationUnitContract.ContractKey FOR XML PATH('contractns:OrganisationUnitContract') , TYPE, ELEMENTS XSINIL) ,

	/*ExcludedCustomerAccountContract*/
	CASE WHEN ExcludeCashAccounts = 1 THEN
		(SELECT ContractKey AS 'commonns:ID', 'CS' AS 'customerns:CustomerAccountTypeCode'
			FROM cc.SupplierContract ExcludedCustomerAccountContract WHERE ExcludedCustomerAccountContract.ContractKey = SupplierContract.ContractKey 
				FOR XML  PATH('contractns:ExcludedCustomerAccountContract') , TYPE, ELEMENTS)
		ELSE NULL
	END,
	/*CustomerAccountContract*/
	(SELECT ContractKey AS 'commonns:ID'
		, AccountNumber AS 'customerns:CustomerAccountId'
		, ContractKey AS 'contractns:ContractId'
		,(SELECT /*LPF-3469*/
				 CAST(ISNULL(CustomerAccountContract.StartDate,CustomerAccountContractEffectiveTimePeriod.StartDate ) AS DATETIME) AS 'commonns:StartDateTime', 
				 CAST(ISNULL(CustomerAccountContract.ExpirationDate,CustomerAccountContractEffectiveTimePeriod.ExpirationDate ) AS DATETIME) AS 'commonns:EndDateTime' 
			FROM cc.SupplierContract CustomerAccountContractEffectiveTimePeriod 
			WHERE CustomerAccountContractEffectiveTimePeriod.ContractKey=SupplierContract.ContractKey FOR XML PATH('customerns:CustomerAccountContractEffectivePeriod') , TYPE, ELEMENTS)
		,(SELECT NULL AS 'commonns:ID', NULL AS Author,NULL AS 'commonns:EntryDatestamp', NULL AS Content 
			FROM cc.[vw_ContractAccount] CustomerAccountContractNotes 
			WHERE CustomerAccountContractNotes.ContractAccountKey=CustomerAccountContract.ContractAccountKey FOR XML PATH('customerns:CustomerAccountContractNotes') , TYPE, ELEMENTS) 

		,CASE SupplierContract.IncludeAllAccounts WHEN 1 THEN 'true' ELSE 'false' END AS 'customerns:ExcludeCustomerAccountIndicator'

		,(SELECT CreatedOnDT AS 'commonns:CreatedDateTime', CreatedByUserName AS 'commonns:UserId' FROM cc.[vw_ContractAccount] CreatedBy 
			WHERE CreatedBy.AccountKey=CustomerAccountContract.AccountKey 
			AND CreatedBy.ContractKey = CustomerAccountContract.ContractKey FOR XML PATH('commonns:CreatedBy') , TYPE, ELEMENTS) 

		,(SELECT ModifiedOnDT AS 'commonns:AmendedDateTime', ModifiedByUserName AS 'commonns:UserId' FROM cc.[vw_ContractAccount] AmendedBy 
			WHERE AmendedBy.AccountKey=CustomerAccountContract.AccountKey 
			AND AmendedBy.ContractKey = CustomerAccountContract.ContractKey FOR XML PATH('commonns:AmendedBy') , TYPE, ELEMENTS) 

	FROM cc.[vw_ContractAccount] CustomerAccountContract WHERE CustomerAccountContract.ContractKey= SupplierContract.ContractKey FOR XML PATH('customerns:CustomerAccountContract') , TYPE, ELEMENTS),

	/*ContractContactName*/
	CASE WHEN ContactName IS NULL THEN NULL ELSE (SELECT NULL Prefix, ContactName AS 'partyns:FullName', NULL FirstName, NULL MiddleName, NULL LastName, NULL PreferredName 
		FROM cc.SupplierContract ContractContactName WHERE ContractContactName.ContractKey = SupplierContract.ContractKey FOR XML PATH('contractns:ContractContactName')  , TYPE, ELEMENTS) END,
	
	/*ContractContactEmail*/
	CASE WHEN ContactEmail IS NULL THEN NULL ELSE (SELECT NULL AS 'commonns:UseCode'
			,ContactEmail AS 'partyns:EMailAddress'
			,NULL AS 'commonns:PreferredIndicator'
		FROM cc.SupplierContract ContractContactEmail WHERE ContractContactEmail.ContractKey = SupplierContract.ContractKey FOR XML PATH('contractns:ContractContactEmail') , TYPE, ELEMENTS) END,

	/*CreatedBy*/
	(SELECT ISNULL(CreatedBy.ContractCreationDT,CreatedBy.ContractModificationDate) 'commonns:CreatedDateTime',ISNULL(wu.UserName,'Unknown') AS 'commonns:UserId'
		FROM cc.SupplierContract CreatedBy 
		LEFT JOIN dbo.WebUser wu ON CreatedBy.CreatedByWebUserKey = wu.WebUserKey
		WHERE CreatedBy.ContractKey = SupplierContract.ContractKey FOR XML PATH('commonns:CreatedBy') , TYPE, ELEMENTS),

	/*AmendedBy*/
	(SELECT 
			CASE ContractSourceKey WHEN 1 THEN ContractModificationDate ELSE ContractModificationDate END AS 'commonns:AmendedDateTime', 
			CASE ContractSourceKey WHEN 1 THEN ContractModificationUser ELSE ISNULL(wu.UserName,'Unknown') END AS 'commonns:UserId'
		FROM cc.SupplierContract AmendedBy 
		LEFT JOIN dbo.WebUser wu ON AmendedBy.ModifiedByWebUserKey = wu.WebUserKey
		WHERE AmendedBy.ContractKey = SupplierContract.ContractKey FOR XML PATH('commonns:AmendedBy') , TYPE, ELEMENTS),

	Originator AS 'contractns:SupplierContractOriginator',
	ContractNumber AS 'contractns:SupplierContractNumber',
	ContractOwner AS 'contractns:SupplierContractOwner',
	ContractReference AS 'contractns:SupplierContractReference',
	(SELECT dv.VendorNumber FROM dbo.DimVendor dv WHERE dv.VendorKey = SupplierContract.VendorKey) AS 'supplierns:SupplierId',
	ContractType1 AS 'contractns:SupplierContractTypeCode',
	--NULL AS SupplierContractDescription,
	--SupplierContract.FixedPriceDate AS 'contractns:FixedPriceDate',
	--SupplierContract.ContractAmount AS 'contractns:TotalSupplierContractAmount',
	CASE SupplierContract.JobFlag WHEN 1 THEN SupplierContract.JobSite   ELSE NULL END  AS 'contractns:SiteDescription', --LPF-3349
	CASE SupplierContract.JobFlag WHEN 1 THEN 'true' ELSE 'false' END AS 'contractns:SiteContract',--LPF-3349
	CASE SupplierContract.JobFlag WHEN 1 THEN SupplierContract.Developer ELSE NULL END  AS 'contractns:SiteDeveloper',--LPF-3349

	SupplierContract.PyramidCode AS 'productns:PyramidCode',
	ContractSourceKey AS 'contractns:SupplierContractSystemOwner',
	CASE SupplierContract.AuthorizedIndicator WHEN 'Y' THEN 'true' ELSE 'false' END  AS 'contractns:SupplierContractAuthorisedIndicator',
	SupplierContract.PurchaseType AS 'orderns:PurchaseTypeCode',
	CASE SupplierContract.IncludeAllLocations WHEN 1 THEN 'true' ELSE 'false' END AS 'contractns:AllOrganisationUnitsIndicator',
	CASE SupplierContract.IncludeAllAccounts WHEN 1 THEN 'true' ELSE 'false' END AS 'contractns:AllCustomerAccountsIndicator',


	/*SupplierContractItem - Items */
	(SELECT sci.ContractKey AS 'commonns:ID'
	
		,NULL  AS 'productns:SPGId'
		,NULL  AS 'productns:MPGId'
		,di.ItemUDVarchar20 AS 'productns:ProductId'
		,CASE ISNULL(ClaimType,'P') WHEN 'A' THEN ClaimBackAmount ELSE NULL END AS 'contractns:DiscountAmount' --LPF-3334
		,CASE ISNULL(ClaimType,'P') WHEN 'P' THEN /*times 100 per LPF-3651*/ 100.0 * ClaimBackPerc ELSE NULL END AS 'contractns:DiscountPercentage' --LPF-3334

		,(SELECT /*LPF-3469*/
					 CAST(ISNULL(sci.StartDT,EffectiveTimePeriod.StartDate ) AS DATETIME) AS 'commonns:StartDateTime', 
					 CAST(ISNULL(sci.ExpiryDT,EffectiveTimePeriod.ExpirationDate ) AS DATETIME) AS 'commonns:EndDateTime' 
					FROM cc.SupplierContract EffectiveTimePeriod 
					WHERE EffectiveTimePeriod.ContractKey=SupplierContract.ContractKey FOR XML PATH('commonns:EffectiveTimePeriod') , TYPE, ELEMENTS)  			 
		,LimitType AS 'contractns:QuantityTypeCode'
		,LimitValue AS 'contractns:QuantityLimit'
		,NULL AS ExcludeSupplierContractItem
		,RedClaimBackPercent AS 'contractns:RedClaimPercentage'
		,AmberClaimBackPercent AS 'contractns:AmberClaimPercentage'
		,GreenClaimBackPercent AS 'contractns:GreenClaimPercentage'
		,(SELECT CreatedBy.CreatedOnDT AS 'commonns:CreatedDateTime', wu1.UserName AS 'commonns:UserId' 
			FROM cc.SupplierContractItem CreatedBy 
			LEFT JOIN dbo.WebUser wu1 ON wu1.WebUserKey = CreatedBy.CreatedByWebUserKey
			WHERE CreatedBy.ContractItemKey = sci.ContractItemKey FOR XML PATH('commonns:CreatedBy') , TYPE, ELEMENTS) 

		,(SELECT ModifiedOnDT AS 'commonns:AmendedDateTime', wu2.UserName AS 'commonns:UserId' 
			FROM cc.SupplierContractItem AmendedBy 
			LEFT JOIN dbo.WebUser wu2 ON wu2.WebUserKey = AmendedBy.ModifiedByWebUserKey
			WHERE AmendedBy.ContractItemKey = sci.ContractItemKey FOR XML PATH('commonns:AmendedBy') , TYPE, ELEMENTS) 

		,CASE ISNULL(ClaimType,'P') WHEN 'C' THEN ClaimCost ELSE NULL END AS 'contractns:NetCost' --waiting for details per LPF-3334
	FROM cc.SupplierContractItem sci 
	JOIN dbo.DimItem di ON di.ItemKey = sci.ItemKey
	JOIN dbo.DimItemGroup1 dig1 ON dig1.ItemGroup1Key = di.ItemGroup1Key
	JOIN dbo.DimItemGroup3 dig3 ON dig3.ItemGroup3Key = di.ItemGroup3Key
	WHERE SupplierContract.ContractKey=sci.ContractKey FOR XML PATH('contractns:SupplierContractItem') , TYPE, ELEMENTS),


	/*SupplierContractItem SPGs */
	(SELECT igx.ContractKey AS 'commonns:ID'	
		
		,dig3.IG3Level1  AS 'productns:SPGId'
		,NULL  AS 'productns:MPGId'
		,NULL AS 'productns:ProductId'
		,CASE ISNULL(ClaimType,'P') WHEN 'A' THEN ClaimBackAmount ELSE NULL END AS 'contractns:DiscountAmount' --LPF-3334
		,CASE ISNULL(ClaimType,'P') WHEN 'P' THEN /*times 100 per LPF-3651*/ 100.0 * ClaimBackPerc ELSE NULL END AS 'contractns:DiscountPercentage' --LPF-3334

		,(SELECT /*LPF-3469*/
					 CAST(ISNULL(igx.StartDT,EffectiveTimePeriod.StartDate ) AS DATETIME) AS 'commonns:StartDateTime', 
					 CAST(ISNULL(igx.ExpiryDT,EffectiveTimePeriod.ExpirationDate ) AS DATETIME) AS 'commonns:EndDateTime' 
					FROM cc.SupplierContract EffectiveTimePeriod 
					WHERE EffectiveTimePeriod.ContractKey=SupplierContract.ContractKey FOR XML PATH('commonns:EffectiveTimePeriod') , TYPE, ELEMENTS)  			 
		,LimitType AS 'contractns:QuantityTypeCode'
		,LimitValue AS 'contractns:QuantityLimit'
		,NULL AS ExcludeSupplierContractItem
		,RedClaimBackPercent AS 'contractns:RedClaimPercentage'
		,AmberClaimBackPercent AS 'contractns:AmberClaimPercentage'
		,GreenClaimBackPercent AS 'contractns:GreenClaimPercentage'
		,(SELECT CreatedBy.CreatedOnDT AS 'commonns:CreatedDateTime', wu1.UserName AS 'commonns:UserId' 
			FROM cc.SupplierContractItemGroupX CreatedBy 
			LEFT JOIN dbo.WebUser wu1 ON wu1.WebUserKey = CreatedBy.CreatedByWebUserKey
			WHERE CreatedBy.ContractItemGroupXKey = igx.ContractItemGroupXKey FOR XML PATH('commonns:CreatedBy') , TYPE, ELEMENTS) 

		,(SELECT ModifiedOnDT AS 'commonns:AmendedDateTime', wu2.UserName AS 'commonns:UserId' 
			FROM cc.SupplierContractItemGroupX AmendedBy 
			LEFT JOIN dbo.WebUser wu2 ON wu2.WebUserKey = AmendedBy.ModifiedByWebUserKey
			WHERE AmendedBy.ContractItemGroupXKey = igx.ContractItemGroupXKey FOR XML PATH('commonns:AmendedBy') , TYPE, ELEMENTS) 

		,CASE ISNULL(ClaimType,'P') WHEN 'C' THEN ClaimCost ELSE NULL END AS 'contractns:NetCost' --waiting for details per LPF-3334
	FROM cc.SupplierContractItemGroupX igx 
	JOIN dbo.DimItemGroup3 dig3 ON dig3.ItemGroup3Key = igx.ItemGroupXKey

	WHERE igx.ItemGroupLevel = 3   ---SPG
	AND SupplierContract.ContractKey=igx.ContractKey FOR XML PATH('contractns:SupplierContractItem') , TYPE, ELEMENTS),
	
	
	/*SupplierContractItem MPGs */
	(SELECT igx.ContractKey AS 'commonns:ID'	
		
		,NULL  AS 'productns:SPGId'
		,dig1.IG1Level1  AS 'productns:MPGId'
		,NULL AS 'productns:ProductId'
		,CASE ISNULL(ClaimType,'P') WHEN 'A' THEN ClaimBackAmount ELSE NULL END AS 'contractns:DiscountAmount' --LPF-3334
		,CASE ISNULL(ClaimType,'P') WHEN 'P' THEN /*times 100 per LPF-3651*/ 100.0 * ClaimBackPerc ELSE NULL END AS 'contractns:DiscountPercentage' --LPF-3334

		,(SELECT /*LPF-3469*/
					 CAST(ISNULL(igx.StartDT,EffectiveTimePeriod.StartDate ) AS DATETIME) AS 'commonns:StartDateTime', 
					 CAST(ISNULL(igx.ExpiryDT,EffectiveTimePeriod.ExpirationDate ) AS DATETIME) AS 'commonns:EndDateTime' 
					FROM cc.SupplierContract EffectiveTimePeriod 
					WHERE EffectiveTimePeriod.ContractKey=SupplierContract.ContractKey FOR XML PATH('commonns:EffectiveTimePeriod') , TYPE, ELEMENTS)  			 
		,LimitType AS 'contractns:QuantityTypeCode'
		,LimitValue AS 'contractns:QuantityLimit'
		,NULL AS ExcludeSupplierContractItem
		,RedClaimBackPercent AS 'contractns:RedClaimPercentage'
		,AmberClaimBackPercent AS 'contractns:AmberClaimPercentage'
		,GreenClaimBackPercent AS 'contractns:GreenClaimPercentage'
		,(SELECT CreatedBy.CreatedOnDT AS 'commonns:CreatedDateTime', wu1.UserName AS 'commonns:UserId' 
			FROM cc.SupplierContractItemGroupX CreatedBy 
			LEFT JOIN dbo.WebUser wu1 ON wu1.WebUserKey = CreatedBy.CreatedByWebUserKey
			WHERE CreatedBy.ContractItemGroupXKey = igx.ContractItemGroupXKey FOR XML PATH('commonns:CreatedBy') , TYPE, ELEMENTS) 

		,(SELECT ModifiedOnDT AS 'commonns:AmendedDateTime', wu2.UserName AS 'commonns:UserId' 
			FROM cc.SupplierContractItemGroupX AmendedBy 
			LEFT JOIN dbo.WebUser wu2 ON wu2.WebUserKey = AmendedBy.ModifiedByWebUserKey
			WHERE AmendedBy.ContractItemGroupXKey = igx.ContractItemGroupXKey FOR XML PATH('commonns:AmendedBy') , TYPE, ELEMENTS) 

		,CASE ISNULL(ClaimType,'P') WHEN 'C' THEN ClaimCost ELSE NULL END AS 'contractns:NetCost' --waiting for details per LPF-3334
	FROM cc.SupplierContractItemGroupX igx 
	JOIN dbo.DimItemGroup1 dig1 ON dig1.ItemGroup1Key = igx.ItemGroupXKey

	WHERE igx.ItemGroupLevel = 1   ---MPG
	AND SupplierContract.ContractKey=igx.ContractKey FOR XML PATH('contractns:SupplierContractItem') , TYPE, ELEMENTS),


	   CAST(SupplierContract.FixedPriceDate AS DATETIME) AS 'contractns:PriceFixedDate',
		CASE 
			WHEN SupplierContract.IncludeChildAccounts = 1 THEN 'true'
			ELSE 'false'
		END AS 'contractns:UseParentChildAccount',
		CASE 
			WHEN SupplierContract.VariablePackMatching = 'P' THEN 'S'
			ELSE SupplierContract.VariablePackMatching
		END	AS 'contractns:UsePackComponent',

	/*SupplierReference*/
	(SELECT scr.CreatedOnDT AS 'contractns:CreatedDate',
				ISNULL(wu.UserName,'Unknown') AS 'contractns:CreatedUserID',
				scr.ModifiedOnDT AS 'contractns:AmendDate',
				ISNULL(wum.UserName,'Unknown') AS 'contractns:AmendUserID',
				scr.ContractReference AS 'contractns:SupplierReferenceID',
				CAST(scr.StartDate AS DATETIME) AS 'contractns:StartDate',
				CAST(scr.ExpirationDate AS DATETIME) AS 'contractns:ExpiryDate'
			FROM cc.SupplierContractReference scr
			LEFT JOIN dbo.WebUser wu
				ON wu.WebUserKey = scr.CreatedByWebUserKey
			LEFT JOIN dbo.WebUser wum
				ON wum.WebUserKey = scr.ModifiedByWebUserKey
			WHERE scr.ContractKey = SupplierContract.ContractKey FOR XML PATH('contractns:ContractSupplierReference') , TYPE, ELEMENTS)
	
	 
	FROM cc.SupplierContract SupplierContract
	WHERE ContractKey = @ContractKey
	FOR XML PATH('contractns:SupplierContract'), ELEMENTS
	)

	SELECT @XMLText AS Results

END


GO
