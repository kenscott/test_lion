SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



-- =============================================
-- Author:		José A. Castaños
-- Create date: 17-Feb-2015
-- Description:	Gets incoming terms for a customer
-- =============================================
CREATE PROCEDURE [lion].[GetIncomingCustomerTerms]
	@AccountNumber Description_Small_type
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @RequestID as nvarchar(255)

	SET @RequestID = CAST(NEWID() as nvarchar(255))

	;WITH SPGTerms as (
			select *
			  from lion.IncomingTerms it
			 where it.AccountNumber = @AccountNumber AND
				   it.Exported = 0 AND
				   it.RowType = 'LLSPG' AND 
				   it.[New SPG Discount 1] IS NOT NULL AND
				   ISNUMERIC(it.[New SPG Discount 1]) = 1
		)

	,ExceptionTerms as (
		select *
		  from lion.IncomingTerms it
		 where it.AccountNumber = @AccountNumber AND
			   it.Exported = 0 AND
			   it.RowType = 'Detail' AND 
				[New Exception Discount] IS NOT NULL AND 
				ISNUMERIC([New Exception Discount]) = 1
		)

	SELECT
			ISNULL(st.LLSPG, et.LLSPG) as SPG,
			ISNULL(st.[LLSPG Description], et.[LLSPG Description]) as [SPG Description],
			ag1.BranchPrimaryBrand as [Brand],
			ROUND(st.[New SPG Discount 1] * 100, 2) as [Usual Discount 1],
			ROUND(st.[New SPG Discount 2] * 100, 2) as [Usual Discount 2],
			IIF(st.[New SPG Discount 1] = et.[New Exception Discount], NULL, et.[Product Code]) as [Exception Product],
			IIF(st.[New SPG Discount 1] = et.[New Exception Discount], NULL, et.[Product Description]) as [Exception Product Description],
			IIF(st.[New SPG Discount 1] = et.[New Exception Discount], NULL, ROUND(et.[New Exception Discount] * 100, 2)) as [Exception Discount],
			NULL as [Exception Fixed Price],
			NULL as [Fixed Price Start Date],
			NULL as [Fixed Price End Date],
			NULL as [Fixed Price Per],
			NULL as [SPG1],
			NULL as [Current Brand],
			NULL as [Current Usual Discount 1],
			NULL as [Current Usual Discount 2],
			NULL as [Exception Product2],
			NULL as [Exception Product Description3],
			NULL as [Current Exception Discount],
			NULL as [Current Exception Fixed Price],
			NULL as [Current Fixed Price Start Date],
			NULL as [Current Fixed Price End Date],
			NULL as [Current Fixed Price Per],
			NULL as [Alternative Current Brands],
			NULL as [Alternative Future Brands],
			ISNULL(et.ID, st.ID) AS [ID],
			@RequestID as [RequestID]
	INTO #Results
	FROM SPGTerms st left outer join
	     ExceptionTerms et on st.AccountKey = et.AccountKey and
		                      st.LLSPG = et.LLSPG and
							  st.Filename = et.Filename inner join
		 lion.vwAccount a on st.AccountKey = a.AccountKey inner join
		 lion.vwAccountGroup1 ag1 on a.Branch = ag1.Branch
   WHERE st.AccountNumber = @AccountNumber  AND
		 (et.[New Exception Discount] <> st.[New SPG Discount 1] or
		  et.[New Exception Discount] is null or 
		  (et.[New Exception Discount] = st.[New SPG Discount 1] and 
		   not exists (select * 
					     from ExceptionTerms iet
						where iet.AccountKey = st.AccountKey and
						      iet.LLSPG = st.LLSPG and 
							  iet.[New Exception Discount] <> st.[New SPG Discount 1]) and
		   et.ID = (select min(ID) 
			          from ExceptionTerms iet 
					 where iet.AccountKey = st.AccountKey and
					       iet.LLSPG = st.LLSPG)
		  )
		 )

	UPDATE it
	   SET it.WolcenRequestID = @RequestID
	  FROM lion.IncomingTerms it inner join
	       #Results r on it.ID = r.ID

 SELECT * FROM #Results 
	ORDER BY 1, 6
END



GO
