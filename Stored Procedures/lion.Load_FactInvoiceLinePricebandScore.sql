SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [lion].[Load_FactInvoiceLinePricebandScore]
WITH EXECUTE AS CALLER
AS
SET NOCOUNT ON

EXEC LogDCPEvent 'DCP --- FactInvoiceLinePricebandScore', 'B'

DECLARE 
	@RowsI Quantity_Normal_type, 
	@RowsU Quantity_Normal_type, 
	@RowsD Quantity_Normal_type,
	@TodayDayKey Key_Small_Type,
	@InvoiceDateMonthKey AS DateKey_type

SET @RowsI = 0
SET @RowsU = 0
SET @RowsD = 0


SELECT 
	@InvoiceDateMonthKey = MAX(InvoiceDateMonthKey) - 14
FROM 
	dbo.FactInvoiceLine


/***************/
/***************
	The code below is to deal with "gaps" in missing data for the non-terms cube (the cube uses lion.vwFactNonTermsTransactions
which uses lion.vwFactInvoiceLinePricebandScore2 which uses a union of lion.FactInvoiceLinePricebandScore_Historical and
lion.vwFactInvoiceLinePricebandScore [which uses dbo.FactInvoiceLinePricebandScore table]). Every so often, a data gap appears
in the cube and someone manually goes in and adds data to lion.FactInvoiceLinePricebandScore_Historical. See JIRA numbers 
LP2P-240, LP2P-597, etc.

	The idea here is that when rows are about to age out of the dbo.FactInvoiceLinePricebandScore table, put their data into 
lion.FactInvoiceLinePricebandScore_Historical before they are gone. This code below needs to be tested. I wrote it and ran 
it but I did not take the time to set up the data to properly execute a good test.

	Once implemented AND the lion.FactInvoiceLinePricebandScore_Historical has been trimmed with appropriate data, the 
lion.vwFactInvoiceLinePricebandScore2 view should be changed so as to remove the "WHERE InvoiceDateMonthKey >= xxx." filter.

					delete s
					FROM lion.FactInvoiceLinePricebandScore_Historical s
					WHERE  EXISTS (SELECT 1 FROM FactInvoiceLinePricebandScore  h WHERE h.ClientInvoiceLineUniqueID = s.ClientInvoiceLineUniqueID )		-- and not already in the history table

****************/
EXEC LogDCPEvent 'DCP --- FactInvoiceLinePricebandScore - Moving data to historical table', 'B'

IF NOT EXISTS (
		SELECT 1
		FROM sys.indexes 
		WHERE object_id = OBJECT_ID('lion.FactInvoiceLinePricebandScore_Historical')
		AND name='iu_1'
	)
	CREATE UNIQUE NONCLUSTERED  INDEX [iu_1] ON lion.FactInvoiceLinePricebandScore_Historical (ClientInvoiceLineUniqueID);

INSERT INTO lion.FactInvoiceLinePricebandScore_Historical (
	ScoreKey,
	AccountKey,
	AlternateAccountKey,
	ItemKey,
	VendorKey,
	AccountManagerKey,
	InvoiceDateDayKey,
	InvoiceDateMonthKey,
	InvoiceLineGroup1Key,
	InvoiceLineGroup2Key,
	InvoiceLineGroup3Key,
	InvoiceLineJunkKey,
	AccountGroup1Key,
	AccountGroup2Key,
	AccountGroup3Key,
	ItemGroup1Key,
	ItemGroup2Key,
	ItemGroup3Key,
	InvoiceLineUniqueIdentifier,
	LinkedInvoiceLineUniqueIdentifier,
	ClientInvoiceID,
	ClientInvoiceLineID,
	ClientInvoiceLineUniqueID,
	TotalQuantity,
	TotalActualPrice,
	TotalActualCost,
	UnitListPrice,
	TotalNetQuantity,
	TotalNetPrice,
	TotalNetCost,
	RedPrice,
	AmberPrice,
	GreenPrice,
	BluePrice,
	TerminalEmulator,
	PriceGuidanceIndicator,
	UDVarchar1,
	UDDecimal1,
	UDDecimal2,
	FloorGPP,
	TargetGPP,
	StretchGPP,
	Score,
	RPBPlaybookPricingGroupKey,
	FloorImpact,
	TargetImpact,
	StretchImpact)
SELECT
	ScoreKey,
	AccountKey,
	AlternateAccountKey,
	ItemKey,
	VendorKey,
	AccountManagerKey,
	InvoiceDateDayKey,
	InvoiceDateMonthKey,
	InvoiceLineGroup1Key,
	InvoiceLineGroup2Key,
	InvoiceLineGroup3Key,
	InvoiceLineJunkKey,
	AccountGroup1Key,
	AccountGroup2Key,
	AccountGroup3Key,
	ItemGroup1Key,
	ItemGroup2Key,
	ItemGroup3Key,
	InvoiceLineUniqueIdentifier,
	LinkedInvoiceLineUniqueIdentifier,
	ClientInvoiceID,
	ClientInvoiceLineID,
	ClientInvoiceLineUniqueID,
	TotalQuantity,
	TotalActualPrice,
	TotalActualCost,
	UnitListPrice,
	TotalNetQuantity,
	TotalNetPrice,
	TotalNetCost,
	RedPrice,
	AmberPrice,
	GreenPrice,
	BluePrice,
	TerminalEmulator,
	PriceGuidanceIndicator,
	UDVarchar1,
	UDDecimal1,
	UDDecimal2,
	FloorGPP,
	TargetGPP,
	StretchGPP,
	Score,
	RPBPlaybookPricingGroupKey,
	FloorImpact,
	TargetImpact,
	StretchImpact
FROM FactInvoiceLinePricebandScore s
WHERE InvoiceDateMonthKey < @InvoiceDateMonthKey		-- the FactInvoiceLinePricebandScore rows that are about to be deleted out
AND NOT EXISTS (SELECT 1 FROM lion.FactInvoiceLinePricebandScore_Historical h WHERE h.ClientInvoiceLineUniqueID = s.ClientInvoiceLineUniqueID )		-- and not already in the history table
EXEC LogDCPEvent 'DCP --- FactInvoiceLinePricebandScore - Moving data to historical table', 'E', @@ROWCOUNT, 0, 0 
/***************/
/***************/


EXEC DBA_IndexRebuild 'FactInvoiceLinePricebandScore', 'Drop'
TRUNCATE TABLE FactInvoiceLinePricebandScore



EXEC LogDCPEvent 'DCP --- FactInvoiceLinePricebandScore - Insert', 'B'


; WITH a AS (
	SELECT
		fil.AccountKey,
		fil.AlternateAccountKey,
		fil.ItemKey,
		VendorKey,
		AccountManagerKey,
		InvoiceDateDayKey,
		InvoiceDateMonthKey,
		fil.InvoiceLineGroup1Key,
		InvoiceLineGroup2Key,
		InvoiceLineGroup3Key,
		InvoiceLineJunkKey,
		fil.AccountGroup1Key,
		AccountGroup2Key,
		AccountGroup3Key,
		fil.ItemGroup1Key,
		fil.ItemGroup2Key,
		fil.ItemGroup3Key,
		InvoiceLineUniqueIdentifier,		-- unique key either from client or us (as in AdvanousInvoiceLineId)
		LinkedInvoiceLineUniqueIdentifier,	-- link to anther line, using raw/staging id, for things like credits
		ClientInvoiceID,
		ClientInvoiceLineID,
		ClientInvoiceLineUniqueID,
		TotalQuantity,
		TotalActualPrice,
		TotalActualCost,
		--TotalActualRebate,
		UnitListPrice,
		TotalNetQuantity,
		TotalNetPrice,
		TotalNetCost,
		CAST( ISNULL( (fil.TotalNetPrice/NULLIF(fil.TotalNetQuantity, 0.)) , (fil.TotalActualPrice/NULLIF(fil.TotalQuantity, 0.)) ) AS DEC(19,8) ) AS UnitPrice,
		CAST( ISNULL(fil.RedPrice,   RAGCalc.RedPrice  ) AS DEC(19,8) ) AS RedPrice,
		CAST( ISNULL(fil.AmberPrice, RAGCalc.AmberPrice) AS DEC(19,8) ) AS AmberPrice,
		CAST( ISNULL(fil.GreenPrice, RAGCalc.GreenPrice) AS DEC(19,8) ) AS GreenPrice,
		fil.BluePrice,
		fil.TerminalEmulator,
		fil.PriceGuidanceIndicator,
		fil.UDVarchar1,		-- PriceDerivation
		fil.UDVarchar3,		-- claimno
		fil.UDDecimal2,		-- CCProfit
		FloorGPP,
		TargetGPP,
		StretchGPP,
		PlaybookPricingGroupKey,
		fil.TotalSupplierCost
	FROM
		dbo.FactInvoiceLine fil
	INNER JOIN dbo.DimItem di
		ON di.ItemKey = fil.ItemKey
	INNER JOIN FactRecommendedPriceBand frpb 
		ON frpb.AccountKey = fil.AccountKey
		AND frpb.ItemKey = fil.ItemKey
		AND frpb.InvoiceLineGroup1Key = fil.InvoiceLineGroup1Key
	--INNER JOIN dbo.ItemUOMConversion c
	--	ON c.ItemKey = fil.ItemKey
	OUTER APPLY lion.fn_GetTradePriceMatchedRAGPrices (
		ItemUDDecimal1,
		ISNULL( (TotalNetCost/NULLIF(TotalNetQuantity, 0.)), (TotalActualCost/NULLIF(TotalQuantity, 0.)) ),
		FloorGPP,
		TargetGPP,
		StretchGPP,
		1,
		1
	) RAGCalc
	WHERE 
		--fil.Last12MonthsIndicator = 'Y'
		--AND 
		FloorGPP IS NOT NULL
		--AND fil.RedPrice IS NULL
		AND InvoiceDateMonthKey >= @InvoiceDateMonthKey
)
INSERT  dbo.FactInvoiceLinePricebandScore 
(
	AccountKey,
	AlternateAccountKey,
	ItemKey,
	VendorKey,
	AccountManagerKey,
	InvoiceDateDayKey,
	InvoiceDateMonthKey,
	InvoiceLineGroup1Key,
	InvoiceLineGroup2Key,
	InvoiceLineGroup3Key,
	InvoiceLineJunkKey,
	AccountGroup1Key,
	AccountGroup2Key,
	AccountGroup3Key,
	ItemGroup1Key,
	ItemGroup2Key,
	ItemGroup3Key,
	InvoiceLineUniqueIdentifier,
	LinkedInvoiceLineUniqueIdentifier,
	ClientInvoiceID,
	ClientInvoiceLineID,
	ClientInvoiceLineUniqueID,
	TotalQuantity,
	TotalActualPrice,
	TotalActualCost,
	UnitListPrice,
	TotalNetQuantity,
	TotalNetPrice,
	TotalNetCost,
	RedPrice,
	AmberPrice,
	GreenPrice,
	BluePrice,
	TerminalEmulator,
	PriceGuidanceIndicator,
	UDVarchar1,
	ClaimNo,
	UDDecimal1,		-- CCProfit
	--UDDecimal2,
	--Last12MonthsIndicator,
	FloorGPP,
	TargetGPP,
	StretchGPP,
	Score,
	FloorImpact,
	TargetImpact,
	StretchImpact,
	RPBPlaybookPricingGroupKey,
	TotalSupplierCost 
)
SELECT  
	AccountKey,
	AlternateAccountKey,
	ItemKey,
	VendorKey,
	AccountManagerKey,
	InvoiceDateDayKey,
	InvoiceDateMonthKey,
	InvoiceLineGroup1Key,
	InvoiceLineGroup2Key,
	InvoiceLineGroup3Key,
	InvoiceLineJunkKey,
	AccountGroup1Key,
	AccountGroup2Key,
	AccountGroup3Key,
	ItemGroup1Key,
	ItemGroup2Key,
	ItemGroup3Key,
	InvoiceLineUniqueIdentifier,		-- unique key either from client or us (as in AdvanousInvoiceLineId)
	LinkedInvoiceLineUniqueIdentifier,	-- link to anther line, using raw/staging id, for things like credits
	ClientInvoiceID,
	ClientInvoiceLineID,
	ClientInvoiceLineUniqueID,
	TotalQuantity,
	TotalActualPrice,
	TotalActualCost,
	--TotalActualRebate,
	UnitListPrice,
	TotalNetQuantity,
	TotalNetPrice,
	TotalNetCost,
	RedPrice,
	AmberPrice,
	GreenPrice,
	BluePrice,
	TerminalEmulator,
	PriceGuidanceIndicator,
	UDVarchar1,		-- PriceDerivation
	UDVarchar3,		-- claimno
	UDDecimal2,		-- CCProfit
	FloorGPP,
	TargetGPP,
	StretchGPP,

	sc.Score,
	--CASE WHEN CASE UnitPrice
	--			WHEN NULL THEN 0
	--			WHEN 0 THEN 0
	--			ELSE UnitPrice
	--			END > GreenPrice THEN 5
	--		WHEN CASE UnitPrice
	--			WHEN NULL THEN 0
	--			WHEN 0 THEN 0
	--			ELSE UnitPrice
	--			END BETWEEN ( AmberPrice + ( .25 * ( GreenPrice - AmberPrice ) ) )
	--				AND     GreenPrice THEN 4
	--		WHEN CASE UnitPrice
	--			WHEN NULL THEN 0
	--			WHEN 0 THEN 0
	--			ELSE UnitPrice
	--			END BETWEEN ( AmberPrice - ( .25 * ( AmberPrice - RedPrice ) ) )
	--				AND     ( AmberPrice + ( .25 * ( GreenPrice - AmberPrice ) ) )
	--		THEN 3
	--		WHEN CASE UnitPrice
	--			WHEN NULL THEN 0
	--			WHEN 0 THEN 0
	--			ELSE UnitPrice
	--			END BETWEEN RedPrice
	--				AND     ( AmberPrice - ( .25 * ( AmberPrice - RedPrice ) ) )
	--		THEN 2
	--		WHEN CASE UnitPrice
	--			WHEN NULL THEN 0
	--			WHEN 0 THEN 0
	--			ELSE UnitPrice
	--			END < RedPrice THEN 1
	--END AS Score,
    --CASE WHEN ( FloorGPP - ( ( TotalActualPrice
    --                            - TotalActualCost )
    --                            / NULLIF(TotalActualPrice, 0.) ) ) > 0
    --        THEN ( FloorGPP - ( ( TotalActualPrice
    --                            - TotalActualCost )
    --                            / NULLIF(TotalActualPrice, 0.) ) )
    --            * TotalActualPrice
    --        ELSE 0
    --END AS FloorImpact,
    --CASE WHEN ( TargetGPP - ( ( TotalActualPrice
    --                            - TotalActualCost )
    --                            / NULLIF(TotalActualPrice,
    --                                    0.) ) ) > 0
    --        THEN ( TargetGPP - ( ( TotalActualPrice
    --                            - TotalActualCost )
    --                            / NULLIF(TotalActualPrice,
    --                                    0.) ) )
    --            * TotalActualPrice
    --        ELSE 0
    --END AS TargetImpact,
    --CASE WHEN ( StretchGPP - ( ( TotalActualPrice
    --                                - TotalActualCost )
    --                            / NULLIF(TotalActualPrice,
    --                                    0.) ) ) > 0
    --        THEN ( StretchGPP - ( ( TotalActualPrice
    --                                - TotalActualCost )
    --                            / NULLIF(TotalActualPrice,
    --                                    0.) ) )
    --            * TotalActualPrice
    --        ELSE 0
    --END AS StretchImpact,

    CASE 
		WHEN ((RedPrice * TotalQuantity) - TotalActualCost) - (TotalActualPrice - TotalActualCost) > 0
            THEN ((RedPrice * TotalQuantity) - TotalActualCost) - (TotalActualPrice - TotalActualCost)
            ELSE 0
    END AS FloorImpact,

    CASE 
		WHEN ((AmberPrice * TotalQuantity) - TotalActualCost) - (TotalActualPrice - TotalActualCost) > 0
            THEN ((AmberPrice * TotalQuantity) - TotalActualCost) - (TotalActualPrice - TotalActualCost)
            ELSE 0
    END AS TargetImpact,

    CASE 
		WHEN ((GreenPrice * TotalQuantity) - TotalActualCost) - (TotalActualPrice - TotalActualCost) > 0
            THEN ((GreenPrice * TotalQuantity) - TotalActualCost) - (TotalActualPrice - TotalActualCost)
            ELSE 0
    END AS StretchImpact,
    PlaybookPricingGroupKey,
	TotalSupplierCost
FROM
	a
CROSS APPLY lion.fn_GetSCore( UnitPrice  ,
                                     RedPrice,
                                     ( AmberPrice - ( .25 * ( AmberPrice - RedPrice ) ) ), 
                                     ( AmberPrice + ( .25 * ( GreenPrice - AmberPrice ) ) ),
                                     GreenPrice) sc
--WHERE
--	NOT EXISTS (SELECT 1 FROM dbo.FactInvoiceLinePricebandScore filpbs where filpbs.InvoiceLineUniqueIdentifier = a.InvoiceLineUniqueIdentifier)
SET @RowsI = @RowsI + @@RowCount


EXEC LogDCPEvent 'DCP --- FactInvoiceLinePricebandScore - Insert', 'E', @RowsI, @RowsU, @RowsD


EXEC LogDCPEvent 'ETL - FactInvoiceLinePricebandScore - Index rebuild', 'B'
EXEC DBA_IndexRebuild 'FactInvoiceLinePricebandScore', 'Create'
EXEC DBA_RemainingFKRebuild
EXEC LogDCPEvent 'ETL - FactInvoiceLinePricebandScore - Index rebuild', 'E', @RowsI, @RowsU, @RowsD



EXEC LogDCPEvent 'ETL - FactInvoiceLinePricebandScore - Update FIL RAG Prices', 'B'
UPDATE fil
SET
	fil.RedPrice = s.RedPrice,
	fil.AmberPrice = s.AmberPrice, 
	fil.GreenPrice = s.GreenPrice
FROM dbo.FactInvoiceLine fil
INNER JOIN dbo.FactInvoiceLinePricebandScore s
	ON s.InvoiceLineUniqueIdentifier = fil.InvoiceLineUniqueIdentifier
WHERE 
	(fil.RedPrice IS NULL
	OR fil.AmberPrice IS NULL
	OR fil.GreenPrice IS NULL)
	AND s.RedPrice IS NOT NULL
	AND s.AmberPrice IS NOT NULL
	AND s.GreenPrice IS NOT NULL
SET @RowsU = @RowsU + @@RowCount
EXEC LogDCPEvent 'ETL - FactInvoiceLinePricebandScore - Update FIL RAG Prices', 'E', @RowsI, @RowsU, @RowsD



EXEC LogDCPEvent 'DCP --- FactInvoiceLinePricebandScore', 'E', @RowsI, @RowsU, @RowsD


GO
