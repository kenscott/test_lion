SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [cm].[ImportManualClaim]
		@BatchKey INT,
		@WebUserKey INT

AS
/*   --Testing
DECLARE @BatchKey INT
DECLARE @WebUserKey INT
SET @BatchKey = 200956
SET @WebUserKey=1972
*/
SET NOCOUNT ON


--Data Validation
UPDATE i SET ErrorMessage = ISNULL(ErrorMessage, '') + 'Invalid ContractID ' + i.ContractID + '; '
	FROM cm.ManualClaimImport i
	LEFT JOIN cc.SupplierContract sc
		ON sc.ContractID = i.ContractID
		AND sc.StatusKey IN (4, 6)  --Pending or Approved
	WHERE i.BatchKey = @BatchKey
		AND sc.ContractKey IS NULL
		
UPDATE i SET ErrorMessage = ISNULL(ErrorMessage, '') + 'Invalid SupplierID ' + i.SupplierID + '; '
	FROM cm.ManualClaimImport i
	LEFT JOIN dbo.DimVendor dv
		ON dv.VendorNumber = i.SupplierID
	LEFT JOIN cc.SupplierContract sc
		ON sc.VendorKey = dv.VendorKey
		AND sc.ContractID = i.ContractID
	WHERE i.BatchKey = @BatchKey
		AND (dv.VendorKey IS NULL OR sc.ContractKey IS NULL)

UPDATE i SET ErrorMessage = ISNULL(ErrorMessage, '') + 'Invalid SupplierRef ' + i.SupplierRef + '; '
	FROM cm.ManualClaimImport i
	LEFT JOIN cc.SupplierContract sc
		ON sc.ContractID = i.ContractId
	LEFT JOIN cc.SupplierContractReference r
		ON r.ContractKey = sc.ContractKey
		AND r.ContractReference = i.SupplierRef
	WHERE i.BatchKey = @BatchKey
		AND r.ContractKey IS NULL

UPDATE i SET ErrorMessage = ISNULL(ErrorMessage, '') + 'Invalid UniqueTransactionID ' + CAST(i.UniqueTransactionID AS VARCHAR(20)) + '; '
	FROM cm.ManualClaimImport i
	LEFT JOIN cm.FactInvoiceLine fil
		ON fil.ClientInvoiceLineUniqueID = i.UniqueTransactionID
	LEFT JOIN cc.SupplierContract sc
		ON sc.ContractID = i.ContractID
	LEFT JOIN cc.SupplierContractItem sci
		ON sci.ContractKey = sc.ContractKey
		AND sci.ItemKey = fil.ItemKey
	LEFT JOIN dbo.DimItem di
		ON di.ItemKey = fil.ItemKey
	LEFT JOIN cc.SupplierContractItemGroupX mpg
		ON mpg.ContractKey = sc.ContractKey
		AND mpg.ItemGroupLevel = 1
		AND mpg.ItemGroupXKey = di.ItemGroup1Key
	LEFT JOIN cc.SupplierContractItemGroupX spg
		ON spg.ContractKey = sc.ContractKey
		AND spg.ItemGroupLevel = 3
		AND spg.ItemGroupXKey = di.ItemGroup3Key
	LEFT JOIN cc.SupplierContractLocation scl
		ON scl.ContractKey = sc.ContractKey
		AND scl.LocationName = fil.BranchCode
	LEFT JOIN cc.SupplierContractAccount sca
		ON sca.ContractKey = sc.ContractKey
		AND sca.AccountKey = fil.AccountKey
	WHERE i.BatchKey = @BatchKey
		AND (i.SupplierID <> fil.VendorNumber
		OR fil.ClientInvoiceLineUniqueID IS NULL
		OR (sci.ContractItemKey IS NULL AND mpg.ContractItemGroupXKey IS NULL AND spg.ContractItemGroupXKey IS NULL)
		OR (scl.ContractLocationKey IS NULL AND sc.IncludeAllLocations = 0)
		OR (sca.ContractAccountKey IS NULL AND sc.IncludeAllAccounts = 0))

UPDATE i SET ErrorMessage = ISNULL(ErrorMessage, '') + 'ProductQty must be between 0.01 and 99999.99; '
	FROM cm.ManualClaimImport i
	WHERE i.BatchKey = @BatchKey
		AND (i.ProductQty < 0.01 OR i.ProductQty > 99999.99)

UPDATE i SET ErrorMessage = ISNULL(ErrorMessage, '') + 'ProductClaimEach must be between 0.001 and 99999.999; '
	FROM cm.ManualClaimImport i
	WHERE i.BatchKey = @BatchKey
		AND (i.ProductClaimEach < 0.001 OR i.ProductClaimEach > 99999.999)

UPDATE i SET ErrorMessage = ISNULL(ErrorMessage, '') + 'Comment must be 2000 characters or less; '
	FROM cm.ManualClaimImport i
	WHERE i.BatchKey = @BatchKey
		AND LEN(i.Comment) > 2000

UPDATE i SET ErrorMessage = ISNULL(ErrorMessage, '') + 'Missing required field(s); '
	FROM cm.ManualClaimImport i
	WHERE i.BatchKey = @BatchKey
		AND (i.Period IS NULL
		OR i.ContractID IS NULL
		OR i.SupplierID IS NULL
		OR i.SupplierRef IS NULL
		OR i.UniqueTransactionID IS NULL
		OR i.ProductQty IS NULL
		OR i.ProductClaimEach IS NULL)

--If NO Errors...
IF (SELECT COUNT(*) 
		FROM cm.ManualClaimImport i
		WHERE BatchKey = @BatchKey AND ErrorMessage IS NOT NULL) = 0
BEGIN

INSERT INTO [cm].[ClaimJournal]
           ([ClaimMonth]
           ,[VendorNumber]
           ,[VendorDescription]
           ,[CurrentContractID]
           ,[ClaimType]
           ,[ClaimLineValue]
           ,[ClaimJournalStatus]
           ,[ClaimJournalStatusDate]
           ,[VendorKey]
           ,[CreationDate]
           ,[CreatedByWebUserKey]
           ,[ImportBatchKey]
           ,[Comment]
		   ,[SupplierRef]
		   ,[ClientInvoiceLineUniqueID]
		   ,[ProductQty]
		   )
     SELECT
            CAST(CONVERT(VARCHAR(4), DATEPART(yy, GETDATE())) + CONVERT(VARCHAR(2), DATEPART(mm, GETDATE())) AS INT)
           ,mci.SupplierID
           ,mci.SupplierRef
           ,mci.ContractID
           ,'M'
           ,mci.ProductClaimEach
           ,'P'
           ,GETDATE()
           ,dv.VendorKey 
           ,GETDATE()
           ,@WebUserKey 
           ,mci.BatchKey 
           ,mci.Comment
		   ,mci.SupplierRef
		   ,mci.UniqueTransactionID
		   ,mci.ProductQty

	FROM 
		cm.ManualClaimImport mci
	INNER JOIN dbo.DimVendor dv
		ON dv.VendorNumber = mci.SupplierID

	WHERE mci.BatchKey = @BatchKey

END

--Create supplier claim if one does not exist for the current month
DECLARE @NewSupplierClaimRowData TABLE (
		SupplierClaimKey KEY_NORMAL_TYPE,
		ClaimStatus varchar(1))

INSERT INTO [cm].[SupplierClaim]
           ([VendorKey]
           ,[ClaimMonth]
           ,[CreationDate]
           ,[CreatedByWebUserKey]
           ,[ClaimStatus])

	OUTPUT 
			INSERTED.SupplierClaimKey,
			INSERTED.ClaimStatus
			INTO @NewSupplierClaimRowData
    
	SELECT DISTINCT 
		cj.VendorKey,
		CAST(CONVERT(VARCHAR(4), DATEPART(yy, GETDATE())) + CONVERT(VARCHAR(2), DATEPART(mm, GETDATE())) AS INT), 
		GETDATE(),
		1,
		'P'
	FROM cm.ClaimJournal cj
	LEFT JOIN cm.SupplierClaim c
		ON c.VendorKey = cj.VendorKey
		AND c.ClaimMonth = cj.ClaimMonth
		AND c.ClaimStatus IN ('P', 'R')  --don't create new if these exist 
	WHERE cj.ImportBatchKey = @BatchKey
		AND c.SupplierClaimKey IS NULL
		
--Log new Supplier Claim
INSERT INTO cm.ClaimLog
	(
	[WebUserKey],
	[SupplierClaimKey],
	[Notes],
	[CreationDate],
	[ClaimAction]
	)
SELECT 
	1,
	d.SupplierClaimKey,
	'From: <blank> To: ' + s.SupplierClaimStatusDescription,
	GETDATE(),
	'Create Supplier Claim'
FROM @NewSupplierClaimRowData d
INNER JOIN cm.SupplierClaimStatus s
	ON s.SupplierClaimStatus = d.ClaimStatus

--If the supplier claims status is R (ready for claim) and an associated supplier claim line has been created, the supplier claim should change to P (pending)
	DECLARE @SupplierClaimUpdateRowData TABLE (
		SupplierClaimKey KEY_NORMAL_TYPE,
		ClaimStatus varchar(1))
	
	UPDATE sc
	SET ClaimStatus = 'P'
	OUTPUT INSERTED.SupplierClaimKey,
			INSERTED.ClaimStatus
		INTO @SupplierClaimUpdateRowData
	FROM cm.SupplierClaim sc
	INNER JOIN cm.ClaimJournal cj
		ON cj.VendorKey = sc.VendorKey
		AND cj.ClaimMonth = sc.ClaimMonth
	WHERE cj.ImportBatchKey = @BatchKey
		AND sc.ClaimStatus = 'R'

--Log Supplier Claim status change
INSERT INTO cm.ClaimLog
	(
	[WebUserKey],
	[SupplierClaimKey],
	[Notes],
	[CreationDate],
	[ClaimAction]
	)
SELECT 
	1,
	d.SupplierClaimKey,
	'From: Ready to Claim  To: ' + s.SupplierClaimStatusDescription,
	GETDATE(),
	'Supplier Claim System Status Change'
FROM @SupplierClaimUpdateRowData d
INNER JOIN cm.SupplierClaimStatus s
	ON s.SupplierClaimStatus = d.ClaimStatus

--Create log entry
INSERT INTO cm.ClaimJournalLog (WebUserKey, ClaimJournalKey, Notes, CreationDate, ClaimAction)
SELECT
	CreatedByWebUserKey,
	ClaimJournalKey,
	'From: blank To: ' + s.ClaimJournalStatusDescription,
	GETDATE(),
	'Create Manual Claim Line'
FROM cm.ClaimJournal cj
INNER JOIN cm.ClaimJournalStatus s
	ON s.ClaimJournalStatus = cj.ClaimJournalStatus
WHERE cj.ImportBatchKey = @BatchKey


SELECT
	ManualClaimImportKey,
	RowNumber,
	ContractID,
	SupplierID,
	UniqueTransactionID,
	ErrorMessage
FROM cm.ManualClaimImport
WHERE BatchKey = @BatchKey
	AND ErrorMessage IS NOT NULL


GO
