SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO











--RBP_CalculateItemBands 136,1


CREATE
PROCEDURE [dbo].[RBP_CalculateItemBands]
(
   @PlaybookDataPointGroupKey INT,
   @SegmentFactorPlaybookKey INT
)

AS
BEGIN
SET QUOTED_IDENTIFIER ON 
SET ANSI_NULLS ON 
SET ARITHABORT ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
SET CONCAT_NULL_YIELDS_NULL ON
SET NUMERIC_ROUNDABORT OFF


DECLARE @RowsI Quantity_Normal_type, @RowsU Quantity_Normal_type, @RowsD Quantity_Normal_type

DECLARE @ProjectKey key_normal_type
SELECT @ProjectKey = ProjectKey FROM PlaybookDataPointGroup WHERE PlaybookDataPointGroupKey = @PlaybookDataPointGroupKey

EXEC LogDCPEvent 'DCP --- RBP_CalculateItemBands', 'B',0,0,0,@ProjectKey
SET @RowsI = 0
SET @RowsU = 0
SET @RowsD = 0


DELETE FROM PlaybookDataPointGroupItemBands 
WHERE PlaybookDataPointGroupKey = @PlaybookDataPointGroupKey
SET @RowsD = @RowsD + @@RowCOunt

DECLARE @SpanAlpha INT,
   @SpanBeta INT

SELECT @SpanAlpha = SpanAlpha, @SpanBeta = SpanBeta
FROM ATKSegmentFactorPlaybook WHERE SegmentFactorPlaybookKey = @SegmentFactorPlaybookKey

-- CREATE
-- PROCEDURE dbo.ATKSetCustomerItemBands2 (@ScenarioKey INT)
-- AS


DECLARE
	@LowerBoundSales DEC(38,8),
	@LowerBoundFrequency DEC(38,8),
	@LowerBoundQty DEC(38,8),
	@WeightSales DEC(38,8),
	@WeightFreq DEC(38,8),
	@WeightQty DEC(38,8)

SELECT  
	@LowerBoundSales = ItemSalesLowerLimit,
	@LowerBoundFrequency = ItemFrequencyLowerLimit,
	@LowerBoundQty = ItemQuantityLowerLimit,
	@WeightSales = ItemSalesWeight,
	@WeightFreq = ItemFrequencyWeight,
	@WeightQty = ItemQuantityWeight
FROM ATKSegmentFactorPlaybook
WHERE SegmentFactorPlaybookKey = @SegmentFactorPlaybookKey

--ALTER  Item Summary--

/*
Create table #TmpAccountItem (
AccountKey int,
ItemKey int,
ItemGroup1Key int,
TotalQuantity decimal(19,8),
TotalActualPrice decimal(19,8),
TotalActualCost decimal(19,8),
InvoiceCount int--,
--Primary Key (AccountKey, ItemKey)
)
*/

--Insert Into #TmpAccountItem
--(AccountKey,ItemKey,ItemGroup1Key,TotalQuantity,TotalActualPrice,TotalActualCost,InvoiceCount)
-- Get All Account/Item Totals
SELECT
	FIL.AccountKey,
	FIL.ItemKey,
	FIL.ItemGroup1Key,
	SUM(TotalQuantity) TotalQuantity,
	SUM(TotalActualPrice) TotalActualPrice,
	SUM(TotalActualCost) TotalActualCost,
	COUNT(*) AS 'InvoiceCount'
INTO #TmpAccountItem
FROM FactInvoiceLine FIL
WHERE Last12MonthsIndicator = 'Y'
GROUP BY FIL.AccountKey,
	FIL.ItemKey,
	FIL.ItemGroup1Key

CREATE CLUSTERED INDEX I_TAI_1 ON #TmpAccountItem(ItemKey, ItemGroup1Key)

DECLARE @tmpItemSummary TABLE (
ItemKey INT,
ItemGroup1Key INT, 
TQty DECIMAL(19,8), 
TFreq DECIMAL(19,8), 
TSales DECIMAL(19,8),
PRIMARY KEY (ItemKey, ItemGroup1Key)
)

INSERT INTO @tmpItemSummary 
(ItemKey,ItemGroup1Key,TQty,TFreq,TSales)
SELECT tai.ItemKey,tai.ItemGroup1Key, 
	SUM(tai.TotalQuantity) AS TQty, 
	SUM(tai.InvoiceCount) AS TFreq, 
	SUM(tai.TotalActualPrice) AS TSales
FROM #TmpAccountItem tai
WHERE tai.TotalQuantity > @LowerBoundQty 
	AND tai.TotalActualPrice > @LowerBoundSales 
	AND tai.InvoiceCount > @LowerBoundFrequency 

GROUP BY tai.ItemKey, tai.ItemGroup1Key



--MarketSummary -- 
DECLARE @tmpallitemssummary TABLE (
MktMinQty DECIMAL(19,8), 
MktAvgQty DECIMAL(19,8),
MktMaxQty DECIMAL(19,8), 
MktStDevQty DECIMAL(19,8),
MktMinFreq DECIMAL(19,8), 
MktAvgFreq DECIMAL(19,8),
MktMaxFreq DECIMAL(19,8), 
MktStDevFreq DECIMAL(19,8),
MktMinSales DECIMAL(19,8), 
MktAvgSales DECIMAL(19,8),
MktMaxSales DECIMAL(19,8), 
MktStDevSales DECIMAL(19,8)
)


INSERT INTO @tmpallitemssummary (
MktMinQty, 
MktAvgQty,
MktMaxQty, 
MktStDevQty,
MktMinFreq, 
MktAvgFreq,
MktMaxFreq, 
MktStDevFreq,
MktMinSales, 
MktAvgSales,
MktMaxSales, 
MktStDevSales
)
SELECT 	MIN(tis.tqty) AS 'MktMinQty', 
	AVG(tis.tqty) AS 'MktAvgQty',
	MAX(tis.tqty) AS 'MktMaxQty', 
	STDEV(tis.tqty) AS 'MktStDevQty',
	MIN(tis.tfreq) AS 'MktMinFreq', 
	AVG(tis.tfreq) AS 'MktAvgFreq',
	MAX(tis.tfreq) AS 'MktMaxFreq', 
	STDEV(tis.tfreq) AS 'MktStDevFreq',
	MIN(tis.tsales) AS 'MktMinSales', 
	AVG(tis.tsales) AS 'MktAvgSales',
	MAX(tis.tsales) AS 'MktMaxSales', 
	STDEV(tis.tsales) AS 'MktStDevSales'
FROM @tmpItemSummary tis




---------------------------------------------------------------------------------------
DECLARE @tmpallitemsummarywlimits TABLE (

MktMinQty DECIMAL(19,8), 
MktAvgQty DECIMAL(19,8),
MktMaxQty DECIMAL(19,8), 
MktStDevQty DECIMAL(19,8),
MktMinFreq DECIMAL(19,8), 
MktAvgFreq DECIMAL(19,8),
MktMaxFreq DECIMAL(19,8), 
MktStDevFreq DECIMAL(19,8),
MktMinSales DECIMAL(19,8), 
MktAvgSales DECIMAL(19,8),

MktMaxSales DECIMAL(19,8), 
MktStDevSales DECIMAL(19,8),
MktULQty DECIMAL(19,8),
MktULFreq DECIMAL(19,8),
MktULSales DECIMAL(19,8)
)


INSERT INTO @tmpallitemsummarywlimits (
MktMinQty, 
MktAvgQty,
MktMaxQty, 
MktStDevQty,
MktMinFreq, 
MktAvgFreq,
MktMaxFreq, 
MktStDevFreq,
MktMinSales, 
MktAvgSales,
MktMaxSales, 
MktStDevSales,
MktULQty,
MktULFreq,
MktULSales
)
SELECT MktMinQty, 
MktAvgQty,
MktMaxQty, 
MktStDevQty,
MktMinFreq, 
MktAvgFreq,
MktMaxFreq, 
MktStDevFreq,
MktMinSales, 
MktAvgSales,
MktMaxSales, 
MktStDevSales, 
	(MktAvgQty) + (MktStDevQty)*@SpanAlpha AS MktULQty,
	(MktAvgFreq) + (MktStDevFreq)*@SpanAlpha AS MktULFreq, 
	(MktAvgSales) + (MktStDevSales)*@SpanAlpha AS MktULSales
FROM @tmpallitemssummary

---------------------------------------------------------------------------------------


DECLARE @tmpitemsummary2 TABLE (
ItemKey INT, 
ItemGroup1Key INT, 
TQty DECIMAL(19,8), 
TFreq DECIMAL(19,8), 
TSales DECIMAL(19,8), 
mktulqty DECIMAL(19,8),
mktulfreq DECIMAL(19,8),
mktulsales DECIMAL(19,8),
PRIMARY KEY (ItemKey, ItemGroup1Key)
)

INSERT INTO @tmpitemsummary2 (
ItemKey, 
ItemGroup1Key, 
TQty, 
TFreq, 
TSales, 
mktulqty,
mktulfreq,
mktulsales
)
SELECT tai.ItemKey, 
	tai.ItemGroup1Key, 
	SUM(tai.TotalQuantity) AS TQty, 
	SUM(tai.InvoiceCount) AS TFreq, 
	SUM(tai.TotalActualPrice) AS TSales, 
	mktulqty,
	mktulfreq,
	mktulsales
FROM #TmpAccountItem tai
	,@tmpallitemsummarywlimits t
GROUP BY tai.ItemKey, tai.ItemGroup1Key, mktulqty,mktulfreq,mktulsales
HAVING 	(SUM(tai.TotalQuantity) > @LowerBoundQty AND (SUM(tai.TotalQuantity) < t.mktulqty)
	AND SUM(tai.TotalActualPrice) > @LowerBoundSales AND (SUM(tai.TotalActualPrice) < t.mktulsales)
	AND SUM(tai.InvoiceCount) > @LowerBoundFrequency AND (SUM(tai.InvoiceCount) < t.mktulfreq))



--------------------------------------------------------------------------
DECLARE @tmpallitemssummary2 TABLE (
MktMinQty DECIMAL(19,8), 
MktAvgQty DECIMAL(19,8),
MktMaxQty DECIMAL(19,8), 
MktStDevQty DECIMAL(19,8),
MktMinFreq DECIMAL(19,8), 
MktAvgFreq DECIMAL(19,8),
MktMaxFreq DECIMAL(19,8), 
MktStDevFreq DECIMAL(19,8),
MktMinSales DECIMAL(19,8), 
MktAvgSales DECIMAL(19,8),
MktMaxSales DECIMAL(19,8), 
MktStDevSales DECIMAL(19,8)
)


INSERT INTO @tmpallitemssummary2
SELECT 	MIN(tis2.tqty) AS 'MktMinQty', 
	AVG(tis2.tqty) AS 'MktAvgQty',
	MAX(tis2.tqty) AS 'MktMaxQty', 
	STDEV(tis2.tqty) AS 'MktStDevQty',
	MIN(tis2.tfreq) AS 'MktMinFreq', 
	AVG(tis2.tfreq) AS 'MktAvgFreq',
	MAX(tis2.tfreq) AS 'MktMaxFreq', 
	STDEV(tis2.tfreq) AS 'MktStDevFreq',
	MIN(tis2.tsales) AS 'MktMinSales', 
	AVG(tis2.tsales) AS 'MktAvgSales',
	MAX(tis2.tsales) AS 'MktMaxSales', 
	STDEV(tis2.tsales) AS 'MktStDevSales'
--into @tmpallitemssummary2
FROM @tmpItemSummary2 tis2



-----------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
DECLARE @tmpallitemsummarywlimits2 TABLE (
MktMinQty DECIMAL(19,8), 
MktAvgQty DECIMAL(19,8),
MktMaxQty DECIMAL(19,8), 
MktStDevQty DECIMAL(19,8),
MktMinFreq DECIMAL(19,8), 
MktAvgFreq DECIMAL(19,8),
MktMaxFreq DECIMAL(19,8), 
MktStDevFreq DECIMAL(19,8),
MktMinSales DECIMAL(19,8), 
MktAvgSales DECIMAL(19,8),
MktMaxSales DECIMAL(19,8), 
MktStDevSales DECIMAL(19,8),
MktULQty DECIMAL(19,8),
MktULFreq DECIMAL(19,8), 
MktULSales DECIMAL(19,8)
)

INSERT INTO @tmpallitemsummarywlimits2
SELECT 
	MktMinQty, 
	MktAvgQty,
	MktMaxQty, 
	MktStDevQty,
	MktMinFreq, 
	MktAvgFreq,
	MktMaxFreq, 

	MktStDevFreq,
	MktMinSales, 

	MktAvgSales,
	MktMaxSales, 
	MktStDevSales, 
	(MktAvgQty) + (MktStDevQty)*@SpanBeta AS MktULQty,
	(MktAvgFreq) + (MktStDevFreq)*@SpanBeta AS MktULFreq, 
	(MktAvgSales) + (MktStDevSales)*@SpanBeta AS MktULSales

FROM @tmpallitemssummary2

---------------------------------------------------------------------------------------

DECLARE @tmpitemsummary3 TABLE (
ItemKey INT, 
ItemGroup1Key INT, 
TQty DECIMAL(19,8), 
TFreq DECIMAL(19,8), 
TSales DECIMAL(19,8), 
mktulqty DECIMAL(19,8),
mktulfreq DECIMAL(19,8),
mktulsales DECIMAL(19,8),
PRIMARY KEY (ItemKey, ItemGroup1Key)
)

INSERT INTO @tmpitemsummary3
SELECT tai.ItemKey, 
	ItemGroup1Key, 
	SUM(tai.TotalQuantity) AS TQty, 
	SUM(tai.InvoiceCount) AS TFreq, 
	SUM(tai.TotalActualPrice) AS TSales, 
	mktulqty,
	mktulfreq,
	mktulsales
FROM #TmpAccountItem tai  
	, @tmpallitemsummarywlimits2 t2
GROUP BY tai.itemKey, ItemGroup1Key, mktulqty, mktulfreq, mktulsales
HAVING 	(SUM(tai.TotalQuantity) > @LowerBoundQty AND (SUM(tai.TotalQuantity) < t2.mktulqty)
	AND SUM(tai.TotalActualPrice) > @LowerBoundSales AND (SUM(tai.TotalActualPrice) < t2.mktulsales)
	AND SUM(tai.InvoiceCount) > @LowerBoundFrequency AND (SUM(tai.InvoiceCount) < t2.mktulfreq))



DECLARE @tmpallitemssummary3 TABLE (
MktMinQty DECIMAL(19,8), 
MktAvgQty DECIMAL(19,8),
MktMaxQty DECIMAL(19,8), 
MktStDevQty DECIMAL(19,8),
MktMinFreq DECIMAL(19,8), 
MktAvgFreq DECIMAL(19,8),
MktMaxFreq DECIMAL(19,8), 
MktStDevFreq DECIMAL(19,8),
MktMinSales DECIMAL(19,8), 
MktAvgSales DECIMAL(19,8),
MktMaxSales DECIMAL(19,8), 
MktStDevSales DECIMAL(19,8)
)

INSERT INTO @tmpallitemssummary3
SELECT 	MIN(tis3.tqty) AS 'MktMinQty', 
	AVG(tis3.tqty) AS 'MktAvgQty',
	MAX(tis3.tqty) AS 'MktMaxQty', 
	STDEV(tis3.tqty) AS 'MktStDevQty',
	MIN(tis3.tfreq) AS 'MktMinFreq', 
	AVG(tis3.tfreq) AS 'MktAvgFreq',
	MAX(tis3.tfreq) AS 'MktMaxFreq', 
	STDEV(tis3.tfreq) AS 'MktStDevFreq',
	MIN(tis3.tsales) AS 'MktMinSales', 
	AVG(tis3.tsales) AS 'MktAvgSales',
	MAX(tis3.tsales) AS 'MktMaxSales', 
	STDEV(tis3.tsales) AS 'MktStDevSales'
FROM @tmpItemSummary3 tis3



------------------------------------------------------------------------------------
DECLARE @tmpQuartiles TABLE (
QtyQuartile DECIMAL(19,8),
FreqQuartile DECIMAL(19,8),
SalesQuartile DECIMAL(19,8),
Mktmaxqty DECIMAL(19,8),
mktmaxfreq DECIMAL(19,8),
mktmaxsales DECIMAL(19,8))

INSERT INTO @tmpQuartiles
SELECT 	((MktAvgQty + 6*MktStDevQty)/4) AS 'QtyQuartile',
	((MktAvgFreq + 6*MktStDevFreq)/4) AS 'FreqQuartile',
	((MktAvgSales + 6*MktStDevSales)/4) AS 'SalesQuartile',
	Mktmaxqty,
	mktmaxfreq,
	mktmaxsales
--into @tmpQuartiles
FROM @tmpallitemssummary3




-------------------------------------------------------------------------------------
DECLARE @tmpSummaryList TABLE (
ItemKey INT, 
TQty DECIMAL(19,8), 
TFreq DECIMAL(19,8), 
TSales DECIMAL(19,8),
PRIMARY KEY (ItemKey)
)
INSERT INTO @tmpSummaryList
SELECT tai.ItemKey, 
	SUM(tai.TotalQuantity) AS TQty, 
	SUM(tai.InvoiceCount) AS TFreq, 
	SUM(tai.TotalActualPrice) AS TSales
FROM #TmpAccountItem tai 

GROUP BY tai.itemKey



--------------------------------------------------------------------------------------
DECLARE @tmpitemscore TABLE (
itemKey INT, 
tqty DECIMAL(19,8),
tfreq DECIMAL(19,8), 
tsales DECIMAL(19,8), 
QtyScore DECIMAL(19,8),
FreqScore DECIMAL(19,8),
SalesScore DECIMAL(19,8),
QtyWeight DECIMAL(19,8),
FreqWeight DECIMAL(19,8),
SalesWeight DECIMAL(19,8),
PRIMARY KEY (ItemKey)
)

INSERT INTO @tmpitemscore
SELECT tsl.itemKey, 
	tsl.tqty,tsl.tfreq, 
	tsl.tsales, 

	'QtyScore' =
	   CASE  
		WHEN tsl.TQty <= @LowerBoundQty  THEN 1 
		WHEN tsl.TQty > @LowerBoundQty AND tsl.TQty <= TQ.qtyquartile THEN 2
		WHEN tsl.TQty > TQ.qtyquartile AND tsl.TQty <= (TQ.qtyquartile*2) THEN 3
		WHEN tsl.TQty > (2*TQ.qtyquartile) AND tsl.TQty <= (TQ.qtyquartile*3) THEN 4
		WHEN tsl.TQty > (3*TQ.qtyquartile) AND tsl.TQty <= (TQ.mktmaxqty) THEN 5
		WHEN tsl.TQty > (TQ.mktmaxqty)  THEN 6
	   END,

	'FreqScore' =
	   CASE  

		WHEN tsl.TFreq <= @LowerBoundFrequency  THEN 1 
		WHEN tsl.TFreq > @LowerBoundFrequency AND tsl.TFreq <= TQ.freqquartile THEN 2
		WHEN tsl.TFreq > TQ.freqquartile AND tsl.TFreq <= (TQ.freqquartile*2) THEN 3
		WHEN tsl.TFreq > (2*TQ.freqquartile) AND tsl.TFreq <= (TQ.freqquartile*3) THEN 4
		WHEN tsl.TFreq > (3*TQ.freqquartile) AND tsl.TFreq <= (TQ.mktmaxfreq) THEN 5
		WHEN tsl.TFreq > (TQ.mktmaxfreq)  THEN 6
	   END,

	'SalesScore' =
	   CASE  
		WHEN tsl.TSales <= @LowerBoundSales  THEN 1 
		WHEN tsl.TSales > @LowerBoundSales AND tsl.TSales <= TQ.salesquartile THEN 2
		WHEN tsl.TSales > TQ.salesquartile AND tsl.TSales <= (TQ.salesquartile*2) THEN 3
		WHEN tsl.TSales > (2*TQ.salesquartile) AND tsl.TSales <= (TQ.salesquartile*3) THEN 4
		WHEN tsl.TSales > (3*TQ.salesquartile) AND tsl.TSales <= (TQ.mktmaxsales) THEN 5
		WHEN tsl.TSales > (TQ.mktmaxsales)  THEN 6
	   END,

	'QtyWeight' = @WeightQty,
	'FreqWeight' = @WeightFreq,
	'SalesWeight' = @WeightSales
FROM @tmpSummaryList tsl , @tmpQuartiles TQ

------------------------------------------------------------------------------------------

DECLARE @tmpitemscore2
TABLE (
ItemKey INT,
AggItemScore DECIMAL(19,8),
ConsItemScore DECIMAL(19,8),
tsales DECIMAL(19,8),
tfreq DECIMAL(19,8),
tqty DECIMAL(19,8),
PRIMARY KEY (ItemKey)
)

INSERT INTO @tmpitemscore2 (ItemKey,AggItemScore,ConsItemScore,tsales,tfreq,tqty)
SELECT 	itemKey, 
	((QtyScore*QtyWeight) + (FreqScore*FreqWeight)+ (SalesScore*SalesWeight)) AS 'AggItemScore',
	
	'ConsItemScore'=
		CASE
			WHEN QtyScore =6 THEN 6
			WHEN FreqScore =6 THEN 6
			WHEN SalesScore =6 THEN 6
			ELSE ((QtyScore*QtyWeight) + (FreqScore*FreqWeight)+ (SalesScore*SalesWeight))
		END,
	tsales,
	tfreq,
	tqty
FROM @tmpitemscore


-- -------------------------------------------------------------------------------------------
-- Declare @tmpfinalitemband table(
-- itemKey int, 
-- --aggItemScore decimal(19,8),
-- ItemBand nvarchar(50),
-- --tsales decimal(19,8),
-- --tfreq decimal(19,8),
-- --tqty decimal(19,8),
-- Primary Key (ItemKey)
-- )

INSERT INTO PlaybookDataPointGroupItemBands (PlaybookDataPointGroupKey, ItemKey, ItemBand)
SELECT @PlaybookDataPointGroupKey,
		ItemKey, 
		'ItemBand'=
		CASE
			WHEN Aggitemscore > 5.0 THEN 'A'
			WHEN Aggitemscore > 4.0 AND Aggitemscore <= 5.0 THEN 'B'
			WHEN Aggitemscore > 3.0 AND Aggitemscore <= 4.0 THEN 'C'
			WHEN Aggitemscore > 2.0 AND Aggitemscore <= 3.0 THEN 'D'
			WHEN Aggitemscore > 1.0 AND Aggitemscore <= 2.0 THEN 'E'
			WHEN Aggitemscore > 0.0 AND Aggitemscore <= 1.0 THEN 'F'
		END
FROM @tmpitemscore2 A
SET @RowsI = @RowsI + @@RowCount

DECLARE @USql NVARCHAR(MAX)
SET @USql = 'Update Statistics PlaybookDataPointGroupItemBands'
EXEC (@USql)

EXEC LogDCPEvent 'DCP --- RBP_CalculateItemBands', 'E', @RowsI, @RowsU, @RowsD

DROP TABLE #TmpAccountItem

RETURN

END





























GO
