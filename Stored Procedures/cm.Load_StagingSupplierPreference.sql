SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- Stored Procedure

CREATE PROCEDURE [cm].[Load_StagingSupplierPreference]

AS

SET NOCOUNT ON 


DECLARE
	@SQLString NVARCHAR(4000) = '',
	@ODSPrefix NVARCHAR(4000) = ''


SELECT 
	@ODSPrefix = ParamValue
FROM 
	dbo.[Param]
WHERE 
	ParamName = 'ClaimsManagerODSPrefix'


TRUNCATE TABLE cm.StagingSupplierPreference
SELECT @SQLString = 'DELETE FROM ' + @ODSPrefix + '..DW_LOAD_ADV.ADV_SUPPLIERS'
EXECUTE sp_executesql @SQLString

DECLARE @VendorKey INT,
	@ContactEmail varchar(250)

--Get Vendor Emails
CREATE TABLE #TempVendorEmail ([VendorKey] int, [VendorEmailString] varchar(4000))

INSERT INTO #TempVendorEmail
	(VendorKey)
SELECT DISTINCT VendorKey
FROM dbo.DimVendorContact

DECLARE VendorEmailCursor 
CURSOR 
FAST_FORWARD 
FOR
	SELECT 
		VendorKey,
		ContactEmail 
	FROM dbo.DimVendorContact 
	WHERE ContactEmail IS NOT NULL
		AND ContactEmail <> ' '

OPEN VendorEmailCursor

FETCH NEXT FROM VendorEmailCursor 
INTO 	
	@VendorKey,
	@ContactEmail

WHILE (@@FETCH_STATUS = 0)
BEGIN

	UPDATE #TempVendorEmail
	SET [VendorEmailString] = ISNULL([VendorEmailString], '') + @ContactEmail + '; '
	WHERE VendorKey = @VendorKey

	FETCH NEXT FROM VendorEmailCursor 
	INTO 	
		@VendorKey,
		@ContactEmail
END

CLOSE VendorEmailCursor
DEALLOCATE VendorEmailCursor


--Load table
INSERT INTO cm.StagingSupplierPreference
	([SupplierID]
    ,[AllowClaimsOnPending]
    ,[AllowClaimReturnCredit]
    ,[HistoricalClaimsPeriod]
	,[CreditMatchingPeriod]
    ,[PaymentMethod]
	,[ContactEmail])
SELECT 
	VendorNumber,
	AllowClaimsOnPending,
	AllowClaimReturnCredits,
	HistoricalClaimsPeriod,
	CreditMatchingPeriod,
	PaymentMethod,
	ve.VendorEmailString
FROM DimVendor dv
LEFT JOIN #TempVendorEmail ve
	ON ve.VendorKey = dv.VendorKey
WHERE Inactive = 0

SELECT @SQLString = '
INSERT INTO ' + @ODSPrefix + '..DW_LOAD_ADV.ADV_SUPPLIERS
	(RUNDATE
	,SUPPLIER_CODE
	,HISTORICAL_CLAIMS_PERIOD
	,PENDING_CONTRACT_IND
	,CLAIM_RETURN_CREDIT_IND
	,PAYMENT_METHOD
	,MATCH_CREDIT_TO_SALES_PERIOD
	,EMAIL_ADDRESSES)
SELECT
	GETDATE(),
	[SupplierID],
	[HistoricalClaimsPeriod],
	[AllowClaimsOnPending],
	[AllowClaimReturnCredit],
	[PaymentMethod],
	[CreditMatchingPeriod],
	[ContactEmail]
FROM cm.StagingSupplierPreference
'
EXECUTE sp_executesql @SQLString


SELECT GETDATE() 

GO
