SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE [lion].[PrepareCentralTerms]
AS

exec lion.PreparePriceFlowTerms

/*
--  Temporary fix for bad data in some spreadsheets
update  lion.IncomingTerms 
set [Exception Discount] = [Exception Discount]/100
where [Exception Discount] > 1

update lion.termstracker
set OriginalDiscount = OriginalDiscount / 100
where OriginalDiscount > 1

update lion.termstracker
set NewDiscount = NewDiscount / 100
where NewDiscount > 1


INSERT INTO lion.TermsTracker (ID, AccountKey, ItemGroup3Key, ItemKey, OriginalDiscount, OriginalDate, NewDiscount, NewWolcenDate,
                               [Total12MonthQuantity], [Sales @ Original], [Sales @ New], NewException, NewSPG)
select ID, AccountKey, ItemGroup3Key, ItemKey, 
	   ROUND(CASE WHEN RowType = 'LLSPG' 
	              THEN 1 - (1 - ISNULL([SPG Disc 1], 0)) * (1 - ISNULL([SPG Disc 2], 0))
				  -- If there was no exception before then consider the SPG discount the original discount
				  ELSE ISNULL([Exception Discount], 1 - (1 - ISNULL([SPG Disc 1], 0)) * (1 - ISNULL([SPG Disc 2], 0))) END, 4) as OriginalDiscount, 
	   cast(it.CreationDate as date) as OriginalDate,
       ROUND(CASE WHEN RowType = 'LLSPG' 
	              THEN 1 - (1 - ISNULL([New SPG Discount 1], 0)) * (1 - ISNULL([New SPG Discount 2], 0))
				  ELSE ISNULL([New Exception Discount], 1 - (1 - ISNULL([New SPG Discount 1], 0)) * (1 - ISNULL([New SPG Discount 2], 0))) END, 4) as NewDiscount, 
	   cast(NULL as date) as NewWolcenDate, 
	   cast(0 as decimal(19,8)) as [Total12MonthQuantity],
	   cast(0 as decimal(19,8)) as [Sales @ Original],
	   cast(0 as decimal(19,8)) as [Sales @ New],
	   CASE WHEN RowType = 'LLSPG' 
	        THEN 0
	        ELSE IIF([Exception Discount] IS NULL, 1, 0)
	   END as NewException,
	   CASE WHEN RowType = 'LLSPG' 
	        THEN IIF([SPG Disc 1] IS NULL AND [SPG Disc 2] IS NULL, 1, 0)
	        ELSE 0
	   END as NewSPG
  from lion.IncomingTerms it inner join
       lion.vwItemGroup3 ig3 on it.LLSPG = ig3.LLSPGCode and ig3.PYRCode = '1' and it.Tracked = 0

update it
   set it.Tracked = 1
  from lion.IncomingTerms it inner join
       lion.TermsTracker tt on it.ID = tt.ID
 where it.Tracked = 0

-- Delete duplicate terms, if any.  Possible if non-contract claims and contract claims sales exist for the same item.
delete from lion.termstracker where id in (
 select min(id)
  from lion.TermsTracker
 where Processed = 0
 group by  accountkey, itemgroup3key, itemkey
having count(*) > 1)

update tt
   set tt.CurrentIndicator = 0
  from lion.TermsTracker tt
 where tt.CurrentIndicator = 1

;with CurrTT as (
select AccountKey, ItemGroup3Key, ItemKey, max(ID) as ID 
  from lion.TermsTracker
group by AccountKey, ItemGroup3Key, ItemKey
)

update tt
   set tt.CurrentIndicator = 1
  from lion.TermsTracker tt inner join 
       CurrTT on tt.ID = CurrTT.ID

update tt
   set tt.NewWolcenDate = ct.LastChangeDate,
       tt.Processed = 0
  from lion.TermsTracker tt (nolock) inner join
       lion.vwItem i (nolock) on tt.ItemKey = i.ItemKey inner join
       lion.CustomerTerms ct (nolock) on (tt.AccountKey = ct.AccountKey and 
	                                      i.ItemNumber = '1' + ct.ExceptionProduct)
 where (tt.Processed = 0 or tt.NewWolcenDate is null) and ROUND(convert(decimal(18,8),ct.ExceptionDiscount) / 100, 3) = ROUND(tt.NewDiscount, 3)

update tt
   set tt.NewWolcenDate = ct.LastChangeDate,
       tt.Processed = 0
  from lion.TermsTracker tt (nolock) inner join
       lion.CustomerTerms ct (nolock) on (tt.AccountKey = ct.AccountKey and 
	                                      tt.ItemGroup3Key = ct.ItemGroup3Key and
										  tt.ItemKey is null)
 where (tt.Processed = 0 or tt.NewWolcenDate is null) and ROUND(1 - (1 - convert(decimal(19,8),ISNULL(ct.Usualdiscount1, 0)) / 100) * (1 - convert(decimal(19,8),ISNULL(ct.Usualdiscount2, 0)) / 100), 3) = ROUND(tt.NewDiscount, 3)

update tt
   set tt.NewWolcenDate = NULL,
       tt.Processed = 1
  from lion.TermsTracker tt
 where tt.NewWolcenDate < '2014-12-29'

update tt
   set OriginalDiscount = ROUND( 
                            ISNULL(
							    (SELECT sum(f.TradingPrice * ISNULL(f.TotalNetQuantity, f.TotalQuantity) - ISNULL(f.TotalNetPrice, f.TotalActualPrice))/nullif(sum(f.TradingPrice * ISNULL(f.TotalNetQuantity, ISNULL(f.TotalNetQuantity, f.TotalQuantity))), 0)
								   FROM lion.vwFactInvoiceLine f inner join
								        dbo.dimday dd on f.InvoiceDateDayKey = dd.DayKey inner join
								        lion.vwFactInvoiceLineGroup1 ilg1 on f.InvoiceLineGroup1Key = ilg1.InvoiceLineGroup1Key
								  WHERE f.Accountkey = tt.AccountKey and
								        f.ItemKey = tt.ItemKey and
								        dd.SQLDate < tt.NewWolcenDate and ilg1.ShipmentType = 'S'),
                                (SELECT OriginalDiscount 
								   FROM lion.TermsTracker itt 
								  WHERE itt.AccountKey = tt.AccountKey and
								        itt.ItemGroup3Key = tt.ItemGroup3Key and
										itt.ItemKey is null and
										itt.CurrentIndicator = 1))
				   , 4)
 from lion.TermsTracker tt
 where tt.Processed = 0 and tt.NewException = 1 and tt.NewWolcenDate is not null

update tt
   set OriginalDiscount = ROUND( 
                            ISNULL(
							    (SELECT sum(f.TradingPrice * ISNULL(f.TotalNetQuantity, ISNULL(f.TotalNetQuantity, f.TotalQuantity)) - ISNULL(f.TotalNetPrice, f.TotalActualPrice))/nullif(sum(f.TradingPrice * ISNULL(f.TotalNetQuantity, ISNULL(f.TotalNetQuantity, f.TotalQuantity))), 0)
								   from lion.vwFactInvoiceLine f INNER JOIN
										lion.vwFactInvoiceLineGroup1 ilg1 on f.InvoiceLineGroup1Key = ilg1.InvoiceLineGroup1Key INNER JOIN
										dbo.DimDay dd on f.InvoiceDateDayKey = dd.DayKey
								  where f.AccountKey = tt.AccountKey and f.ItemGroup3Key = tt.ItemGroup3Key and
								        f.PriceDerivation in ('DP','DDC','ADJ', 'MP')  and dd.SQLDate < tt.NewWolcenDate and
										ilg1.ShipmentType = 'S' 
								),
                                (SELECT OriginalDiscount 
								   FROM lion.TermsTracker itt 
								  WHERE itt.AccountKey = tt.AccountKey and
								        itt.ItemGroup3Key = tt.ItemGroup3Key and
										itt.ItemKey is null and
										itt.CurrentIndicator = 1))
				   , 4)
 from lion.TermsTracker tt
 where tt.Processed = 0 and 
      tt.NewSPG = 1 and tt.NewWolcenDate is not null
 	   
 ;with exceptions as (
  select *
    from lion.TermsTracker
   where Processed = 0
     and ItemKey is not null
     and NewWolcenDate is not null
)

, benefits as (
 select f.AccountKey,
        f.ItemKey,
		sum(ISNULL(f.TotalNetQuantity, f.TotalQuantity)) as Total12MonthQuantity, 
        sum(ISNULL(f.TotalNetQuantity, f.TotalQuantity) * (1 - e.OriginalDiscount) * i.CurrBranchTradePrice) as [Sales @ Original],
        sum(ISNULL(f.TotalNetQuantity, f.TotalQuantity) * (1 - e.NewDiscount) * i.CurrBranchTradePrice) as [Sales @ New]
   from lion.vwFactInvoiceLine f inner join
        exceptions e on f.AccountKey = e.AccountKey and
		                f.ItemKey = e.ItemKey inner join
		lion.vwItem i on f.ItemKey = i.ItemKey inner join
		dbo.DimDay dd on f.InvoiceDateDayKey = dd.DayKey
  where f.PriceDerivation IN ('GT', 'OTG') and
        dd.SQLDate between DATEADD(yy, -1, e.NewWolcenDate) and e.NewWolcenDate
 group by f.AccountKey, f.ItemKey
)

update tt
   set tt.Total12MonthQuantity = b.Total12MonthQuantity,
       tt.[Sales @ Original] = b.[Sales @ Original],
	   tt.[Sales @ New] = b.[Sales @ New]
  from lion.TermsTracker tt inner join
       benefits b on tt.AccountKey = b.AccountKey and
	                 tt.ItemKey = b.ItemKey
 where tt.Processed = 0

;with spgterms as (
  select *
    from lion.TermsTracker
   where ItemKey is null
     and NewWolcenDate is not null
	 and Processed = 0
)

, exceptions as (
  select *
    from lion.TermsTracker
   where ItemKey is not null
     and NewWolcenDate is not null
	 and Processed = 0
)

, benefits as (
 select f.AccountKey,
        f.ItemGroup3Key,
		sum(ISNULL(f.TotalNetQuantity, f.TotalQuantity)) as Total12MonthQuantity, 
        sum(ISNULL(f.TotalNetQuantity, f.TotalQuantity) * (1 - s.OriginalDiscount) * i.CurrBranchTradePrice) as [Sales @ Original],
        sum(ISNULL(f.TotalNetQuantity, f.TotalQuantity) * (1 - s.NewDiscount) * i.CurrBranchTradePrice) as [Sales @ New]
   from lion.vwFactInvoiceLine f inner join
		lion.vwItem i on f.ItemKey = i.ItemKey inner join
        spgterms s on f.AccountKey = s.AccountKey and
		              f.ItemGroup3Key = s.ItemGroup3Key inner join
		dbo.DimDay dd on f.InvoiceDateDayKey = dd.DayKey
  where f.PriceDerivation IN ('GT', 'OTG') and
        dd.SQLDate between DATEADD(yy, -1, s.NewWolcenDate) and s.NewWolcenDate and
		not exists (select * from exceptions e where e.AccountKey = f.AccountKey and e.ItemKey = f.ItemKey)

 group by f.AccountKey, f.ItemGroup3Key
)

update tt
   set tt.Total12MonthQuantity = b.Total12MonthQuantity,
       tt.[Sales @ Original] = b.[Sales @ Original],
	   tt.[Sales @ New] = b.[Sales @ New]
  from lion.TermsTracker tt inner join
       benefits b on tt.AccountKey = b.AccountKey and
	                 tt.ItemGroup3Key = b.ItemGroup3Key and
					 tt.ItemKey is null
 where tt.Processed = 0

 update ott
    set ott.LastInvoiceDateDayKey = dd.DayKey
   from lion.TermsTracker ott inner join
        lion.TermsTracker ntt on ott.AccountKey = ntt.AccountKey and
		                         ott.ItemGroup3Key = ntt.ItemGroup3Key and
								 ott.ItemKey = ntt.ItemKey and
								 ott.Processed = 1 and
								 ntt.Processed = 0 inner join
		dbo.DimDay dd on dd.SQLDate = ntt.NewWolcenDate

update lion.TermsTracker
   set Processed = 1
 where Processed = 0

delete from lion.TermsBenefits

insert into lion.TermsBenefits (ClientInvoicelineUniqueID, TermsTrackerID, [RuleNumber], [Actual Benefit],  [Potential Benefit])
select f.ClientInvoicelineUniqueID,
       tt.ID,

	   /*
		Rule 1: New discount is lower than original discount and actual discount (in transaction) is within 0.2% of new discount (+/-)
		Rule 2: New discount is lower than original discount and actual discount (in transaction) differs from new discount by more than 0.2% (+/-),
		        but the actual discount is better (lower) than the original discount
		Rule 3: New discount is lower than original discount and actual discount (in transaction) is worse (higher) than the original discount
		Rule 4: New discount is lower than original discount and actual discount (in transaction) is within 0.2% of original discount (+/-)
		Rule 5: New product exception (i.e., no terms existed for the account/product combination).  Needs to be applied first.
		Rule 6: New SPG discount and product has been sold before.  Calculate original discount based on sales of the product under
		        price derivations DP, DDC, ADJ, and MP
		Rule 7: New SPG discount and product has not been sold before.  Calculate original discount based on sales of products with the same MPG under
		        price derivations DP, DDC, ADJ, and MP
	   */

	   CASE WHEN tt.NewException = 1
	        THEN 5
			WHEN f.PriceDerivation in ('GT', 'OTG') AND tt.NewSPG = 1 and exists(select * 
																		           FROM lion.vwFactInvoiceLine ifil inner join 
																				        dbo.DimDay idd on ifil.InvoiceDateDayKey = idd.DayKey 
																				  WHERE ifil.AccountKey = tt.AccountKey and 
																				        ifil.ItemKey = f.ItemKey and 
																						idd.SQLDate < tt.NewWolcenDate and 
																						ifil.PriceDerivation in ('DP','DDC','ADJ', 'MP'))
			THEN 6
			/*WHEN f.PriceDerivation in ('GT', 'OTG') AND tt.NewSPG = 1 and exists(select * 
																				   FROM lion.vwFactInvoiceLine ifil inner join 
																				        dbo.DimDay idd on ifil.InvoiceDateDayKey = idd.DayKey INNER JOIN
                                                                                        lion.vwItem i ON ifil.ItemKey = i.ItemKey INNER JOIN
                                                                                        lion.vwItem mi ON mi.MPGCode = i.MPGCode AND 
																						                  mi.LLSPGCode = i.LLSPGCode AND 
																										  mi.ItemPyramidCode = i.ItemPyramidCode AND
                                                                                                          ISNULL(RTRIM(mi.MPGCode), '') <> ''
																				  WHERE ifil.AccountKey = tt.AccountKey and 
																				        ifil.ItemKey = f.ItemKey and 
																						idd.SQLDate < tt.NewWolcenDate and 
																						ifil.PriceDerivation in ('DP','DDC','ADJ', 'MP')) 
			THEN 7*/
	        WHEN f.PriceDerivation in ('GT', 'OTG') AND ROUND(tt.OriginalDiscount, 3) > ROUND(tt.NewDiscount, 3) AND ABS(ROUND((ISNULL(f.TotalNetQuantity, f.TotalQuantity) * f.TradingPrice - ISNULL(f.TotalNetPrice, f.TotalActualPrice)) / NULLIF((ISNULL(f.TotalNetQuantity, f.TotalQuantity) * f.TradingPrice), 0), 4) - ROUND(tt.NewDiscount, 4)) <= 0.002
	        THEN 1
			WHEN f.PriceDerivation in ('GT', 'OTG') AND ROUND(tt.OriginalDiscount, 3) > ROUND(tt.NewDiscount, 3) AND (ROUND((ISNULL(f.TotalNetQuantity, f.TotalQuantity) * f.TradingPrice - ISNULL(f.TotalNetPrice, f.TotalActualPrice)) / NULLIF((ISNULL(f.TotalNetQuantity, f.TotalQuantity) * f.TradingPrice), 0), 4) - ROUND(tt.NewDiscount, 4)) > 0.002
			THEN 2
			WHEN f.PriceDerivation in ('GT', 'OTG') AND ROUND(tt.OriginalDiscount, 3) > ROUND(tt.NewDiscount, 3) AND (ROUND(tt.OriginalDiscount, 4) - ROUND((ISNULL(f.TotalNetQuantity, f.TotalQuantity) * f.TradingPrice - ISNULL(f.TotalNetPrice, f.TotalActualPrice)) / NULLIF((ISNULL(f.TotalNetQuantity, f.TotalQuantity) * f.TradingPrice), 0), 4)) < -0.002
	        THEN 3
			WHEN f.PriceDerivation in ('GT', 'OTG') AND ROUND(tt.OriginalDiscount, 3) > ROUND(tt.NewDiscount, 3) AND ABS(ROUND(tt.OriginalDiscount, 4) - ROUND((ISNULL(f.TotalNetQuantity, f.TotalQuantity) * f.TradingPrice - ISNULL(f.TotalNetPrice, f.TotalActualPrice)) / NULLIF((ISNULL(f.TotalNetQuantity, f.TotalQuantity) * f.TradingPrice), 0), 4)) <= 0.002
	        THEN 4
			ELSE 0
	   END AS [RuleNumber],
	   CASE WHEN f.PriceDerivation in ('GT', 'OTG')
	        THEN ISNULL(f.TotalNetQuantity, f.TotalQuantity) * f.TradingPrice * (tt.OriginalDiscount - ((ISNULL(f.TotalNetQuantity, f.TotalQuantity) * f.TradingPrice - ISNULL(f.TotalNetPrice, f.TotalActualPrice)) / NULLIF((ISNULL(f.TotalNetQuantity, f.TotalQuantity) * f.TradingPrice), 0)))
			ELSE 0
	   END AS [Actual Benefit],
	   ISNULL(f.TotalNetQuantity, f.TotalQuantity) * f.TradingPrice * (tt.OriginalDiscount - tt.NewDiscount) as [Potential Benefit]
  from lion.vwFactInvoiceLine f (nolock) inner join
	   lion.vwItem i (nolock) on f.ItemKey = i.ItemKey inner join
       lion.TermsTracker tt on (tt.AccountKey = f.AccountKey and
		                        ISNULL(tt.ItemKey, f.ItemKey) = f.ItemKey and
								case when exists (select * 
								              from lion.TermsTracker itt
											 where itt.AccountKey = f.AccountKey and
											       itt.ItemKey = f.ItemKey) and
											tt.ItemKey is null
									 then 0
									 else tt.ItemGroup3Key
								 end = f.ItemGroup3Key
					           ) inner join
	   dbo.DimDay dd on f.InvoiceDateDayKey = dd.DayKey
 where dd.SQLDate >= tt.NewWolcenDate

update tt
   set tt.CurrentIndicator = 0
  from lion.TermsTracker tt
 where tt.CurrentIndicator = 1

;with CurrTT as (
select AccountKey, ItemGroup3Key, ItemKey, max(ID) as ID 
  from lion.TermsTracker
group by AccountKey, ItemGroup3Key, ItemKey
)

update tt
   set tt.CurrentIndicator = 1
  from lion.TermsTracker tt inner join 
       CurrTT on tt.ID = CurrTT.ID

DECLARE @StartDate DATETIME
SET @StartDate = '29-DEC-2014'

truncate table lion.CentralTerms
truncate table lion.CentralTermsArchive

INSERT INTO [lion].[CentralTerms]
           ([RowType]
           ,[AccountKey]
           ,[LLSPG]
           ,[ItemKey]
           ,[InvoiceLineGroup1Key]
           ,[Prod KVI]
           ,[KVI Override]
           ,[Default Discount]
           ,[OrigSPGDisc1]
           ,[OrigSPGDisc2]
           ,[RecSPGDisc1]
           ,[RecSPGDisc2]
           ,[AccSPGDisc1]
           ,[AccSPGDisc2]
           ,[OrigExceptionDisc]
           ,[RecExceptionDisc]
           ,[AccExceptionDisc]
           ,[OrigFixedPrice]
           ,[RecExceptionFixedPrice]
           ,[AccExceptionFixedPrice]
           ,[Stuck]
           ,[ProjectedImpact]
           ,[EffectiveDate]
		   ,[Returned])
 select o.RowType, o.AccountKey, o.LLSPG, o.ItemKey, o.InvoiceLineGroup1Key, o.[Prod KVI], o.[KVI Override], o.[Default Discount],
        o.[SPG Disc 1] as [OrigSPGDisc1], o.[SPG Disc 2] as [OrigSPGDisc2], 
	    o.[New SPG Discount 1] as [RecSPGDisc1], o.[New SPG Discount 2] as [RecSPGDisc2], 
		i.[New SPG Discount 1] as [AccSPGDisc1], i.[New SPG Discount 2] as [AccSPGDisc2], 
		o.[Exception Discount] as [OrigExceptionDisc],
		o.[New Exception Discount] as [RecExceptionDisc],
		i.[New Exception Discount] as [AccExceptionDisc], 
		CASE WHEN RTRIM(o.[Exception Fixed Price]) = '' THEN NULL ELSE o.[Exception Fixed Price] END as [OrigFixedPrice],
		o.[New Exception Fixed Price] as [RecExceptionFixedPrice],
		i.[New Exception Fixed Price] as [AccExceptionFixedPrice],
		CONVERT(bit, 0) as [Stuck],
		CONVERT(decimal(19,8),0) as [ProjectedImpact],
		@StartDate as [EffectiveDate],
		CASE WHEN i.AccountKey IS NOT NULL THEN 1 ELSE 0 END AS [Returned]
   from lion.OutgoingTerms o left outer join
        lion.IncomingTerms i on o.AccountKey = i.AccountKey and
		                        o.LLSPG = i.LLSPG and
								ISNULL(o.ItemKey,0) = ISNULL(i.ItemKey,0)
order by o.ID, i.ID

UPDATE ct
SET ct.active = IIF(k.keep = ct.id, 1, 0)
  FROM lion.CentralTerms ct
INNER JOIN
(
  select RowType, AccountKey, LLSPG, ItemKey, MAX(id) as keep
  from lion.CentralTerms
  group by RowType, AccountKey, LLSPG, ItemKey
  having count(*) >1
) k
ON ct.RowType = k.RowType and
   ct.AccountKey = k.AccountKey and
   ct.LLSPG = k.LLSPG and 
   isnull(ct.ItemKey,0) = ISNULL(k.ItemKey, 0)

set identity_insert lion.CentralTermsArchive on

INSERT INTO [lion].[CentralTermsArchive]
           ([ID]
		   ,[RowType]
           ,[OutgoingRecID]
           ,[IncomingRecID]
           ,[AccountKey]
           ,[LLSPG]
           ,[ItemKey]
           ,[InvoiceLineGroup1Key]
           ,[Prod KVI]
           ,[KVI Override]
           ,[Default Discount]
           ,[OrigSPGDisc1]
           ,[OrigSPGDisc2]
           ,[RecSPGDisc1]
           ,[RecSPGDisc2]
           ,[AccSPGDisc1]
           ,[AccSPGDisc2]
           ,[OrigExceptionDisc]
           ,[RecExceptionDisc]
           ,[AccExceptionDisc]
           ,[OrigFixedPrice]
           ,[RecExceptionFixedPrice]
           ,[AccExceptionFixedPrice]
           ,[ProjectedImpact]
           ,[EffectiveDate]
           ,[RecImpact]
           ,[AccImpact]
           ,[Active])
select [ID]
		   ,[RowType]
           ,[OutgoingRecID]
           ,[IncomingRecID]
           ,[AccountKey]
           ,[LLSPG]
           ,[ItemKey]
           ,[InvoiceLineGroup1Key]
           ,[Prod KVI]
           ,[KVI Override]
           ,[Default Discount]
           ,[OrigSPGDisc1]
           ,[OrigSPGDisc2]
           ,[RecSPGDisc1]
           ,[RecSPGDisc2]
           ,[AccSPGDisc1]
           ,[AccSPGDisc2]
           ,[OrigExceptionDisc]
           ,[RecExceptionDisc]
           ,[AccExceptionDisc]
           ,[OrigFixedPrice]
           ,[RecExceptionFixedPrice]
           ,[AccExceptionFixedPrice]
           ,[ProjectedImpact]
           ,[EffectiveDate]
           ,[RecImpact]
           ,[AccImpact]
           ,[Active]
 from lion.CentralTerms where Active = 0

set identity_insert lion.CentralTermsArchive off

delete from lion.CentralTerms where Active = 0

; with SPGCentralTerms as (
select * 
  from lion.CentralTerms 
 where RowType = 'LLSPG' and
       Returned = 1
)

update ct
   set ct.AccExceptionDisc = st.AccCmpDisc
  from lion.CentralTerms ct inner join
       SPGCentralTerms st on ct.AccountKey = st.AccountKey and
	                  ct.LLSPG = st.LLSPG
where ct.AccExceptionDisc is null and 
      ct.RowType = 'Detail' and
	  ct.Returned = 1

;with SPGCustomerTerms as (
	select AccountKey, SPGCode, min(CustomerTermsKey) as CustomerTermsKey
	  from lion.customerterms
	 group by AccountKey, SPGCode
)

update ct
   set ct.CustomerTermsKey=t.CustomerTermsKey,
       ct.EffectiveDate = iif(t.LastChangeDate > @StartDate, t.LastChangeDate, @StartDate)
  from lion.CentralTerms ct inner join
       SPGCustomerTerms st on ct.AccountKey = st.AccountKey and
	                          ct.LLSPG = st.SPGCode inner join
	   lion.CustomerTerms t on ct.AccountKey = t.AccountKey and
	                           ((ct.ItemKey = (select ItemKey from lion.vwItem where ItemNumber = '1' + t.ExceptionProduct) and
								  t.PyramidCode = 1 and
							      ct.RowType = 'Detail'
								) or 
								(ct.LLSPG = t.SPGCode and 
							     ct.RowType = 'LLSPG' and
							     t.PyramidCode = 1 and
								 t.CustomerTermsKey = st.CustomerTermsKey
								)
							   )
 where ct.Returned = 1

 /*
select *
  from lion.CustomerTerms t left outer join
       lion.vwItem i on i.ItemNumber = '1' + t.ExceptionProduct inner join 
       lion.CentralTerms ct on ct.AccountKey = t.AccountKey and
	                           ct.ItemKey = i.ItemKey and
							   ct.RowType = 'Detail' and
							   t.PyramidCode = 1
 where ct.Returned = 1 and ct.CustomerTermsKey is null
 						   



select  count(*)-- ct.RowType,ct.AccountKey, t.AccountKey, ct.LLSPG, t.SPGCode, ct.ItemKey, t.ExceptionProduct, ct.RecSPGDisc1, ct.AccSPGDisc1, ct.Accepted, ct.AccCmpDisc, t.Usualdiscount1, t.Usualdiscount2, ct.AccExceptionDisc, t.ExceptionDiscount, ct.RecCmpDisc, ct.RecExceptionDisc, ct.Accepted, ct.Stuck
  from lion.CentralTerms ct --inner join
       --lion.CustomerTerms t on ct.CustomerTermsKey = t.CustomerTermsKey
 where accepted = 1 and CustomerTErmsKey is null

select  *-- ct.RowType,ct.AccountKey, t.AccountKey, ct.LLSPG, t.SPGCode, ct.ItemKey, t.ExceptionProduct, ct.RecSPGDisc1, ct.AccSPGDisc1, ct.Accepted, ct.AccCmpDisc, t.Usualdiscount1, t.Usualdiscount2, ct.AccExceptionDisc, t.ExceptionDiscount, ct.RecCmpDisc, ct.RecExceptionDisc, ct.Accepted, ct.Stuck
  from lion.CentralTerms ct --inner join
       --lion.CustomerTerms t on ct.CustomerTermsKey = t.CustomerTermsKey
 where accepted = 1 and CustomerTErmsKey is null


 
select  aCCcMPDisc, [Default Discount], *-- ct.RowType,ct.AccountKey, t.AccountKey, ct.LLSPG, t.SPGCode, ct.ItemKey, t.ExceptionProduct, ct.RecSPGDisc1, ct.AccSPGDisc1, ct.Accepted, ct.AccCmpDisc, t.Usualdiscount1, t.Usualdiscount2, ct.AccExceptionDisc, t.ExceptionDiscount, ct.RecCmpDisc, ct.RecExceptionDisc, ct.Accepted, ct.Stuck
  from lion.CentralTerms ct --inner join
       --lion.CustomerTerms t on ct.CustomerTermsKey = t.CustomerTermsKey
 where returned = 1 and CustomerTErmsKey is null and rowtype='LLSPG' and stuck=0

 select * from lion.CustomerTerms
 where AccountKey = 188727
 and SPGCode = 'PS34'

 */

-- Update item-level CentralTerms that could not be matched as exceptions
-- Match to the LLPSG-level customer terms entry
update ct
   set ct.CustomerTermsKey=t.CustomerTermsKey,
       ct.EffectiveDate = iif(t.LastChangeDate > @StartDate, t.LastChangeDate, @StartDate)
  from lion.CentralTerms ct inner join
	   lion.CustomerTerms t on ct.AccountKey = t.AccountKey and
							   ct.LLSPG = t.SPGCode and
							   ct.RowType = 'Detail' and
							   t.PyramidCode = 1
 where ct.CustomerTermsKey is null and ct.Returned = 1							   

-- If the central LLSPG-level term is not matched to a customer term, but the discount matches the default discount then
-- consider the term stuck
update ct
   set ct.Stuck = 1,
       ct.EffectiveDate = @StartDate
  from lion.CentralTerms ct
 where ct.CustomerTermsKey is null and ct.Returned = 1 and Stuck = 0 and
       (RowType='LLSPG' and (AccCmpDisc is null or isnull(round(AccCmpDisc, 4), 0) = isnull(round([Default Discount], 4),0)))

update ct
   set ct.Stuck = 1,
       ct.EffectiveDate = iif(t.LastChangeDate > @StartDate, t.LastChangeDate, @StartDate)
  from lion.CentralTerms ct inner join
	   lion.CustomerTerms t on ct.CustomerTermsKey = t.CustomerTermsKey
 where ct.RowType = 'Detail' and ct.Returned = 1 and ISNULL(round(ct.AccExceptionDisc * 100,2),0) = ISNULL(CAST(t.ExceptionDiscount as decimal(19,8)), ISNULL(CAST(t.Usualdiscount1 as decimal(19,8)), 0) + ((1 - (ISNULL(CAST(t.UsualDiscount1 as decimal(19,8)), 0))) * ISNULL(CAST(t.UsualDiscount2 as decimal(19,8)), 0)))

update ct
   set ct.Stuck = 1,
       ct.EffectiveDate = iif(t.LastChangeDate > @StartDate, t.LastChangeDate, @StartDate)
  from lion.CentralTerms ct inner join
	   lion.CustomerTerms t on ct.CustomerTermsKey = t.CustomerTermsKey
 where ct.RowType = 'LLSPG' and ct.Returned = 1 and
       ISNULL(round(ct.AccSPGDisc1 * 100, 2),0) = ISNULL(t.Usualdiscount1,0) and
       ISNULL(round(ct.AccSPGDisc2 * 100, 2),0) = ISNULL(t.Usualdiscount2,0)

;with SPGCentralTerms as (
 select * 
   from lion.CentralTerms 
  where RowType = 'LLSPG' and
        Returned = 1
 )

 update ct
   set ct.Stuck = st.Stuck,
       ct.EffectiveDate = st.EffectiveDate
   from lion.CentralTerms ct inner join
       SPGCentralTerms st on ct.AccountKey = st.AccountKey and
	                  ct.LLSPG = st.LLSPG
 where ct.CustomerTermsKey is null and ct.Returned = 1 and ct.Stuck = 0 and
       (ct.RowType='Detail' and (ct.AccExceptionDisc is null or isnull(round(ct.AccExceptionDisc, 4), 0) = isnull(round(st.AccCmpDisc, 4),0)))

-- Missing Terms
-- select * from lion.CentralTerms where returned=1 and CustomerTermsKey is null and stuck = 0

-- Different Terms
-- select RowType,  AccCmpDisc, Usualdiscount1, Usualdiscount2, AccExceptionDisc, ExceptionDiscount, * 
-- from lion.CentralTerms ct inner join lion.CustomerTErms t on ct.CustomerTermsKey = t.CustomerTermsKey where returned=1 and stuck=0

;with LastDiscount as (
SELECT * FROM (
SELECT
    ct.EffectiveDate,
	ct.CustomerTermsKey,
	FIL.AccountKey, 
	FIL.ItemKey, 
	FIL.InvoiceLineGroup1Key,	
    ROW_NUMBER() OVER (Partition by FIL.AccountKey, FIL.ItemKey, FIL.InvoiceLineGroup1Key
	                   order by 	FIL.AccountKey, 
									FIL.ItemKey, 
									FIL.InvoiceLineGroup1Key,
									ct.RowType, 
									FIL.InvoiceDateDayKey DESC, 
									ISNULL(fil.TotalNetQuantity, fil.TotalQuantity) DESC, 
									ISNULL(fil.TotalNetPrice, fil.TotalActualPrice) DESC,
									FIL.ClientInvoiceID DESC,
									FIL.InvoiceLineUniqueIdentifier DESC) as RowNumber,
	fil.InvoiceLineUniqueIdentifier as LastInvoiceLineUniqueIdentifier,
	dd.SQLDate as LastItemDiscountInvoiceDate,
	fil.UDVarchar1 as LastItemDiscountPriceDerivation,
	ISNULL(	1 - ((ISNULL(fil.TotalNetPrice, fil.TotalActualPrice) / NULLIF(ISNULL(fil.TotalNetQuantity, fil.TotalQuantity), 0)) / NULLIF(fil.UnitListprice, 0.)), 0.) AS LastItemDiscountPercent
FROM dbo.FactInvoiceLine FIL INNER JOIN
     dbo.DimDay dd on fil.InvoiceDateDayKey = dd.DayKey INNER JOIN
	 lion.vwItem i on fil.ItemKey = i.ItemKey INNER JOIN
	 lion.vwAccount a on fil.AccountKey = a.AccountKey inner join
     lion.CentralTerms ct on fil.AccountKey = ct.AccountKey and
	 	                       ((ct.RowType = 'Detail' and fil.ItemKey = ct.ItemKey) or
							    (ct.RowType='LLSPG' and i.LLSPGCode = ct.LLSPG
								)
							   )
WHERE 
 	ISNULL(TotalNetQuantity, TotalQuantity) > 0.0 AND 
 	ISNULL(TotalNetPrice, TotalActualPrice) > 0.0 AND 
 	ISNULL(TotalNetCost, TotalActualCost) > 0.0 AND 
	(COALESCE(TotalNetPrice, TotalActualPrice, 0.0) - COALESCE(TotalNetCost, TotalActualCost, 0.0))/NULLIF(ISNULL(TotalNetPrice, TotalActualPrice), 0) between -1.2 and 0.90 and
	  fil.UDVarchar1 in ('GT', 'OTG', 'TP', 'DDC', 'LP') and
      dd.SQLDate < ct.EffectiveDate
) a
where RowNumber = 1 )


, TotalSales as (
SELECT 
	fil.AccountKey, 
	fil.ItemKey, 
	SUM(ISNULL(TotalNetPrice, TotalActualPrice)) AS Total12MonthSales, 
	SUM(ISNULL(TotalNetCost, TotalActualCost)) AS Total12MonthCost, 
	SUM(ISNULL(TotalNetQuantity, TotalQuantity)) AS Total12MonthQuantity
FROM dbo.FactInvoiceLine fil inner join
     dbo.DimDay dd on fil.InvoiceDateDayKey = dd.DayKey inner join
	 lion.vwItem item on fil.ItemKey = item.ItemKey inner join
     lion.CentralTerms ct on fil.AccountKey = ct.AccountKey and
							 ((ct.ItemKey is not null and fil.ItemKey = ct.ItemKey) or 
	                          (ct.ItemKey is null and item.LLSPGCode = ct.LLSPG ) and
							  not exists (select *
										    from lion.CentralTerms ict
										   where ict.AccountKey = fil.AccountKey and
												 ict.ItemKey = fil.ItemKey)) and
							 ((ct.ItemKey is not null and fil.InvoiceLineGroup1Key = ct.InvoiceLineGroup1Key) or 
	                          (ct.ItemKey is null and fil.InvoiceLineGroup1Key = 4)  and
							  not exists (select *
										    from lion.CentralTerms ict
										   where ict.AccountKey = fil.AccountKey and
												 ict.ItemKey = fil.ItemKey))
WHERE dd.SQLDate between dateadd(dd, 1, dateadd(yy, -1, ct.EffectiveDate)) and ct.EffectiveDate and
      fil.InvoiceLineGroup1Key = 4
GROUP BY 
	fil.AccountKey, 
	fil.ItemKey
)

update ct
   set ct.RecImpact = (flpac.Total12MonthQuantity * item.CurrBranchTradePrice * (1 - case when ct.ItemKey is null then ct.RecCmpDisc 
																						  else ct.RecExceptionDisc end)) -
                      (flpac.Total12MonthQuantity * item.CurrBranchTradePrice * (1 - ld.LastItemDiscountPercent)),
	   ct.AccImpact = (flpac.Total12MonthQuantity * item.CurrBranchTradePrice * (1 - case when ct.ItemKey is null then ct.AccCmpDisc
																						  else ct.AccExceptionDisc end)) -
                      (flpac.Total12MonthQuantity * item.CurrBranchTradePrice * (1 - ld.LastItemDiscountPercent))
 from TotalSales flpac inner join
	  LastDiscount ld on flpac.AccountKey = ld.AccountKey and flpac.ItemKey = ld.ItemKey inner join
	  lion.vwItem item on item.ItemKey = flpac.ItemKey inner join
      lion.CentralTerms ct on flpac.AccountKey = ct.AccountKey and ((ct.ItemKey is not null and flpac.ItemKey = ct.ItemKey) or 
	                                                                (ct.ItemKey is null and item.LLSPGCode = ct.LLSPG and
																	 not exists (select *
																				   from lion.CentralTerms ict
																				  where ict.AccountKey = flpac.AccountKey and
																				        ict.ItemKey = flpac.ItemKey)))


/*
delete from lion.TermsResults

;WITH PriorSales AS (
SELECT 
	fil.AccountKey, 
	fil.ItemKey,
	SUM(ISNULL(TotalNetPrice, TotalActualPrice)) AS Total12MonthSales, 
	SUM(ISNULL(TotalNetCost, TotalActualCost)) AS Total12MonthCost, 
	SUM(ISNULL(TotalNetQuantity, TotalQuantity)) AS Total12MonthQuantity
FROM dbo.FactInvoiceLine fil INNER JOIN
     dbo.DimDay dd ON fil.InvoiceDateDayKey = dd.DayKey INNER JOIN
	 lion.vwItem item ON fil.ItemKey = item.ItemKey INNER JOIN
     lion.CentralTerms ct ON fil.AccountKey = ct.AccountKey AND
							 ((ct.ItemKey IS NOT NULL AND fil.ItemKey = ct.ItemKey) OR 
	                          (ct.ItemKey IS NULL AND item.LLSPGCode = ct.LLSPG ) AND
							  NOT EXISTS (SELECT *
										    FROM lion.CentralTerms ict
										   WHERE ict.AccountKey = fil.AccountKey AND
												 ict.ItemKey = fil.ItemKey))
WHERE dd.SQLDate BETWEEN DATEADD(dd, 1, DATEADD(yy, -1, ct.EffectiveDate)) AND ct.EffectiveDate AND
      fil.InvoiceLineGroup1Key = 4
GROUP BY 
	fil.AccountKey, 
	fil.ItemKey
)

 ,ProcessResults AS (
 SELECT acct.Branch,
	   COUNT(*) - SUM(1 * Returned) AS [CountPending],
	   SUM(1 * Returned) AS [CountReturned],
       SUM(Accepted) AS [CountAccepted],
	   CAST(SUM(Accepted) AS DECIMAL(19,8))/NULLIF(SUM(1 * Returned), 0) AS [PercentAccepted],
	   SUM(RecImpact * Returned) AS [ProposedImpactAmount],
	   SUM(AccImpact * Returned * Accepted) AS [AcceptedImpactAmount],
	   SUM(AccImpact * Returned * Accepted)/NULLIF(SUM(RecImpact),0) AS [PercentAmountAccepted],
	   SUM(CAST(Stuck AS DECIMAL(19,8)) * Returned) AS [CountStuck],
	   SUM(CAST(Stuck AS DECIMAL(19,8)) * Returned)/NULLIF(SUM(1 * Returned), 0) AS [PercentStuck],
	   SUM(AccImpact * Returned) AS [ProjectedImpact]
  FROM lion.CentralTerms ct INNER JOIN
       lion.vwAccount acct ON ct.AccountKey = acct.AccountKey
GROUP BY acct.Branch)

, LastDiscount AS (
SELECT * FROM (
SELECT
    ct.EffectiveDate,
	ct.CustomerTermsKey,
	FIL.AccountKey, 
	FIL.ItemKey, 
	FIL.InvoiceLineGroup1Key,	
    ROW_NUMBER() OVER (PARTITION BY FIL.AccountKey, FIL.ItemKey, FIL.InvoiceLineGroup1Key
	                   ORDER BY 	FIL.AccountKey, 
									FIL.ItemKey, 
									FIL.InvoiceLineGroup1Key,
									ct.RowType, 
									FIL.InvoiceDateDayKey DESC, 
									ISNULL(fil.TotalNetQuantity, fil.TotalQuantity) DESC, 
									ISNULL(fil.TotalNetPrice, fil.TotalActualPrice) DESC,
									FIL.ClientInvoiceID DESC,
									FIL.InvoiceLineUniqueIdentifier DESC) AS RowNumber,
	fil.InvoiceLineUniqueIdentifier AS LastInvoiceLineUniqueIdentifier,
	dd.SQLDate AS LastItemDiscountInvoiceDate,
	fil.UDVarchar1 AS LastItemDiscountPriceDerivation,
	ISNULL(	1 - ((ISNULL(fil.TotalNetPrice, fil.TotalActualPrice) / NULLIF(ISNULL(fil.TotalNetQuantity, fil.TotalQuantity), 0)) / NULLIF(fil.UnitListprice, 0.)), 0.) AS LastItemDiscountPercent
FROM dbo.FactInvoiceLine FIL INNER JOIN
     dbo.DimDay dd ON fil.InvoiceDateDayKey = dd.DayKey INNER JOIN
	 lion.vwItem i ON fil.ItemKey = i.ItemKey INNER JOIN
	 lion.vwAccount a ON fil.AccountKey = a.AccountKey INNER JOIN
     lion.CentralTerms ct ON fil.AccountKey = ct.AccountKey AND
	 	                       ((ct.RowType = 'Detail' AND fil.ItemKey = ct.ItemKey) OR
							    (ct.RowType='LLSPG' AND i.LLSPGCode = ct.LLSPG
								)
							   )
WHERE 
 	ISNULL(TotalNetQuantity, TotalQuantity) > 0.0 AND 
 	ISNULL(TotalNetPrice, TotalActualPrice) > 0.0 AND 
 	ISNULL(TotalNetCost, TotalActualCost) > 0.0 AND 
	(COALESCE(TotalNetPrice, TotalActualPrice, 0.0) - COALESCE(TotalNetCost, TotalActualCost, 0.0))/NULLIF(ISNULL(TotalNetPrice, TotalActualPrice), 0) BETWEEN -1.2 AND 0.90 AND
	  fil.UDVarchar1 IN ('GT', 'OTG', 'TP', 'DDC', 'LP') AND
      dd.SQLDate < ct.EffectiveDate
) a
WHERE RowNumber = 1 )


,GrossMarginImpact AS (
SELECT acct.Branch, 
       SUM(ISNULL(filpbs.TotalNetPrice, filpbs.TotalActualPrice) - (ISNULL(filpbs.TotalNetQuantity, filpbs.TotalQuantity) * (item.CurrBranchTradePrice *  (1 - ld.LastItemDiscountPercent)))) AS ActualImpact
  FROM lion.vwFactInvoiceLinePricebandScore filpbs INNER JOIN
       dbo.DimInvoiceLineJunk dilj ON filpbs.InvoiceLineJunkKey = dilj.InvoiceLineJunkKey INNER JOIN
	   LastDiscount ld ON filpbs.AccountKey = ld.AccountKey AND filpbs.ItemKey = ld.ItemKey AND filpbs.InvoiceLineGroup1Key = ld.InvoiceLineGroup1Key INNER JOIN
	   lion.vwAccount acct ON filpbs.AccountKey = acct.AccountKey INNER JOIN
	   lion.vwAccountGroup1 ag1 ON filpbs.AccountGroup1Key = ag1.AccountGroup1Key INNER JOIN
       lion.vwItem item ON filpbs.ItemKey = item.ItemKey INNER JOIN
	   lion.vwFactInvoiceLineGroup1 ilg1 ON filpbs.InvoiceLineGroup1Key = ilg1.InvoiceLineGroup1Key INNER JOIN
       dbo.DimDay dd ON filpbs.InvoiceDateDayKey = dd.DayKey INNER JOIN 
	   lion.CentralTerms ct ON filpbs.AccountKey = ct.AccountKey AND
	                           ((ct.RowType = 'Detail' AND filpbs.ItemKey = ct.ItemKey) OR
							    (ct.RowType='LLSPG' AND item.LLSPGCode = ct.LLSPG AND 
								  NOT EXISTS (SELECT *
								                FROM lion.CentralTerms ict
											   WHERE ict.AccountKey = filpbs.AccountKey AND ict.ItemKey = filpbs.ItemKey)
								)
						       ) AND
							   filpbs.InvoiceLineGroup1Key = COALESCE(ct.InvoiceLineGroup1Key, 4) LEFT OUTER JOIN
	   lion.PilotSchedule ps ON ps.Branch = acct.Branch LEFT OUTER JOIN
	   lion.CustomerTerms t ON ct.AccountKey = t.AccountKey AND
	                           ((((ct.ItemKey = item.itemKey AND
							     item.ItemNumber = '1' + t.ExceptionProduct) AND
								 t.PyramidCode = 1
							     ) AND
							     ct.RowType = 'Detail') OR 
								(ct.LLSPG = t.SPGCode AND 
							     ct.RowType = 'LLSPG' AND
							     t.PyramidCode = 1))
 WHERE dd.SQLDate >= ct.EffectiveDate AND
       item.ItemPyramidCode = 1  AND 
	   acct.AccountName <> 'Unknown' AND 
	   item.ItemNumber <> 'Unknown' AND 
	   item.ItemObsolete <> 'Y' AND 
	   item.SpecialInd <> 'Y' AND 
	   ilg1.DeviatedIndicator <> 'Y' AND 
	   ilg1.ShipmentType <> 'D' AND 
	   ISNULL(filpbs.TotalNetQuantity, filpbs.TotalQuantity) <> 0 AND 
	   ISNULL(filpbs.TotalNetPrice, filpbs.TotalActualPrice) <> 0 AND 
	   filpbs.PriceDerivation IN ('GT', 'OTG', 'TP', 'DDC', 'LP') AND 
	   acct.BranchPrimaryBrand <> 'BURDN' AND
	   ct.Returned = 1
  GROUP BY acct.Branch
 )

 , SalesImpact AS (
SELECT acct.Branch,
       -- Total Sales Impact = (Projected Qty * New terms price) - (Prior Qty * Prior Price)
       SUM(ISNULL(flpac.Total12MonthQuantity, 0) * 
	    (item.CurrBranchTradePrice * (1 - COALESCE(AccExceptionDisc, AccCmpDisc, 0)))
	   ) -
	   SUM(ISNULL(ps.Total12MonthQuantity, 0) * 
	    (item.CurrBranchTradePrice * (1 - COALESCE(ld.LastItemDiscountPercent, 0)))
	   ) AS [TotalSalesImpact],
	   -- Price Effect = Prior Qty * (New terms price - Prior Avg Price)
       SUM(ISNULL(ps.Total12MonthQuantity, 0) * 
	    ((item.CurrBranchTradePrice * (1 - COALESCE(AccExceptionDisc, AccCmpDisc, 0))) -
	     (item.CurrBranchTradePrice * (1 - COALESCE(ld.LastItemDiscountPercent, 0)))
		)
	   ) AS [PriceEffect] ,
	   -- Volume Effect = (Projected Qty - Prior Qty) * New Price
	   SUM((ISNULL(flpac.Total12MonthQuantity,0) - ISNULL(ps.Total12MonthQuantity, 0)) *
	   	   (item.CurrBranchTradePrice * (1 - COALESCE(AccExceptionDisc, AccCmpDisc, 0)))
	   ) AS [VolumeEffect]
  FROM lion.vwFactLastPriceAndCost flpac INNER JOIN
	   LastDiscount ld ON flpac.AccountKey = ld.AccountKey AND flpac.ItemKey = ld.ItemKey AND flpac.InvoiceLineGroup1Key = 4 INNER JOIN
       lion.vwAccount acct ON flpac.AccountKey = acct.AccountKey INNER JOIN
       lion.vwItem item ON flpac.ItemKey = item.ItemKey INNER JOIN
	   lion.CentralTerms ct ON flpac.AccountKey = ct.AccountKey AND
	                           ((ct.RowType = 'Detail' AND flpac.ItemKey = ct.ItemKey) OR
							    (ct.RowType = 'LLSPG' AND item.LLSPGCode = ct.LLSPG AND 
								  NOT EXISTS (SELECT *
								                FROM lion.CentralTerms ict
											   WHERE ict.AccountKey = flpac.AccountKey AND ict.ItemKey = flpac.ItemKey)
								)
						       ) AND
							   flpac.InvoiceLineGroup1Key = 4 FULL OUTER JOIN
		PriorSales ps ON flpac.AccountKey = ps.AccountKey AND flpac.ItemKey = ps.ItemKey AND flpac.InvoiceLineGroup1Key = 4
  WHERE ct.Returned = 1
GROUP BY acct.Branch
)

INSERT INTO lion.TermsResults
 SELECT pr.Branch, pr.CountPending, pr.CountReturned, pr.CountAccepted, pr.PercentAccepted, pr.ProposedImpactAmount, pr.AcceptedImpactAmount, pr.PercentAmountAccepted,
        pr.CountStuck, pr.PercentStuck, ISNULL(gmi.ActualImpact, 0) AS ActualImpact, pr.ProjectedImpact, si.PriceEffect, si.VolumeEffect, si.TotalSalesImpact 
   FROM ProcessResults pr LEFT OUTER JOIN
        GrossMarginImpact gmi ON pr.Branch = gmi.Branch LEFT OUTER JOIN
		SalesImpact si ON pr.Branch = si.Branch
*/

*/







GO
