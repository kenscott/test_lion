SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [sup].[GetSupplierContractList]
      @vendorKey INT
    , @searchValue NVARCHAR(50) = NULL
	, @bolContractID BIT = 0
	, @bolContractRef BIT = 0
	, @bolVendorStock BIT = 0
	, @bolItemDesc BIT = 0
	, @bolLLSPGCode BIT = 0
	, @bolLLSPGDesc BIT = 0
	, @bolMPGCode BIT = 0
	, @bolMPGDesc BIT = 0
	, @bolLocationName BIT = 0
	, @bolLocationDesc BIT = 0
	, @bolAccountName BIT = 0
	, @bolJobSite BIT = 0
	, @bolDeveloper BIT = 0

WITH EXECUTE AS CALLER

AS
SET NOCOUNT ON;
SET ANSI_WARNINGS OFF ;

 /**-- TESTING 
DECLARE @vendorkey INTEGER;
DECLARE @searchValue NVARCHAR(50)
DECLARE @bolContractID BIT
DECLARE @bolContractRef BIT
DECLARE @bolVendorStock BIT
DECLARE @bolItemDesc BIT
DECLARE @bolLLSPGCode BIT
DECLARE @bolLLSPGDesc BIT
DECLARE @bolMPGCode BIT
DECLARE @bolMPGDesc BIT
DECLARE @bolLocationName BIT
DECLARE @bolLocationDesc BIT
DECLARE @bolAccountName BIT
DECLARE @bolJobSite BIT
DECLARE @bolDeveloper BIT


SET @vendorkey = 7341
SET @searchValue = N'IMVB'
SET @bolContractID = 0
SET @bolContractRef =0
SET @bolItemDesc = 1
SET @bolAccountName = 0
SET @bolDeveloper=0
SET @bolItemDesc = 0
SET @bolJobSite = 0
SET @bolLLSPGCode = 1
SET @bolLLSPGDesc = 1
SET @bolLocationDesc = 0
SET @bolLocationName = 0
SET @bolMPGCode = 1
SET @bolMPGDesc = 1
SET @bolVendorStock = 0
--SET @strAnd = ''

**/  --END TESTING


IF ISNULL(@searchValue, '') = ''
BEGIN
	 SELECT DISTINCT TOP 0
        [Status] = CASE
                       WHEN scr.ExpirationDate > GETDATE() THEN
                           'Active'
                       WHEN scr.ExpirationDate <= GETDATE() THEN
                           'Expired'
                       ELSE
                           'Expired'
                   END,
        scr.ContractReference AS 'Contract ID',
        sc.ContractID AS 'Wolseley Contract ID',
        scr.StartDate AS 'Start Date',
        scr.ExpirationDate AS 'Expiry Date',
        sc.ContactName AS 'Contact Name',
        sc.ContactEmail AS 'Contact Email',
        ISNULL(clmSum.PendingClaimAmounts,0)AS PendingClaimAmounts,
        ISNULL(clmSum.PendingClaims,0) AS PendingClaims,
        caHealth.Health,
        scr.ContractReferenceKey
    FROM cc.SupplierContract sc
        INNER JOIN cc.SupplierContractReference scr
            ON sc.ContractKey = scr.ContractKey
        OUTER APPLY lion.fn_getPendingSumByVendor(sc.ContractID) clmSum
        CROSS APPLY lion.fn_getHealth(sc.ContractKey) caHealth
    WHERE sc.VendorKey = @vendorKey AND scr.ExpirationDate > '12/31/2014' AND  sc.StatusKey = 6
	UNION ALL
    SELECT DISTINCT
        [Status] = CASE
                       WHEN scr.ExpirationDate > GETDATE() THEN
                           'Active'
                       WHEN scr.ExpirationDate <= GETDATE() THEN
                           'Expired'
                       ELSE
                           'Expired'
                   END,
        scr.ContractReference AS 'Contract ID',
        sc.ContractID AS 'Wolseley Contract ID',
        scr.StartDate AS 'Start Date',
        scr.ExpirationDate AS 'Expiry Date',
        sc.ContactName AS 'Contact Name',
        sc.ContactEmail AS 'Contact Email',
        ISNULL(clmSum.PendingClaimAmounts,0)AS PendingClaimAmounts,
        ISNULL(clmSum.PendingClaims,0) AS PendingClaims,
        caHealth.Health,
        scr.ContractReferenceKey
    FROM cc.SupplierContract sc
        INNER JOIN cc.SupplierContractReference scr
            ON sc.ContractKey = scr.ContractKey
        OUTER APPLY lion.fn_getPendingSumByVendor(sc.ContractID) clmSum
        CROSS APPLY lion.fn_getHealth(sc.ContractKey) caHealth
    WHERE sc.VendorKey = @vendorKey AND scr.ExpirationDate > '12/31/2014' AND  sc.StatusKey = 6;
END;

IF ISNULL(@searchValue, '') <> ''

BEGIN
	DECLARE @strAnd VARCHAR(10)
	DECLARE @SELECT VARCHAR(MAX)
	DECLARE @FROM VARCHAR(MAX)
	DECLARE @WHERE VARCHAR(MAX)
	DECLARE @sql VARCHAR(MAX)
    SELECT @searchValue = REPLACE(@searchValue, '%', '');
    SELECT @searchValue = '%' + @searchValue + '%';
	SET @SELECT = ' SELECT DISTINCT '
	SET @SELECT = @SELECT + '[Status] = CASE '
	SET @SELECT = @SELECT + 'WHEN scr.ExpirationDate > GETDATE() THEN ''ACTIVE'' '
	SET @SELECT = @SELECT + 'WHEN scr.ExpirationDate <= GETDATE() THEN ''Expired'' '
	SET @SELECT = @SELECT + 'ELSE  ''Expired '' '
	SET @SELECT = @SELECT + 'END, '
	SET @SELECT = @SELECT + 'scr.ContractReference AS ''CONTRACT ID'', '
	SET @SELECT = @SELECT + 'sc.ContractID AS ''Wolseley CONTRACT ID'', '
	SET @SELECT = @SELECT + 'scr.StartDate AS ''START Date'', '
	SET @SELECT = @SELECT + 'scr.ExpirationDate AS ''Expiry Date'', '
	SET @SELECT = @SELECT + ' sc.ContactName AS ''Contact NAME'', '
	SET @SELECT = @SELECT + ' sc.ContactEmail AS ''Contact Email'', '
	SET @SELECT = @SELECT + ' ISNULL(clmSum.PendingClaimAmounts,0) AS PendingClaimAmounts, '
	SET @SELECT = @SELECT + ' ISNULL(clmSum.PendingClaims,0) AS PendingClaims, '
	SET @SELECT = @SELECT + ' caHealth.Health, '
	SET @SELECT = @SELECT + ' scr.ContractReferenceKey'
	SET @FROM = ' FROM cc.SupplierContract sc '
	SET @FROM = @FROM + 'INNER JOIN cc.SupplierContractReference scr '
	SET @FROM = @FROM + ' ON sc.ContractKey = scr.ContractKey '
	IF @bolVendorStock=1 OR @bolItemDesc=1 
		BEGIN
			SET @FROM = @FROM + 'Left JOIN [cc].[SupplierContractItem] sci '
			SET @FROM = @FROM + ' ON sc.ContractKey = sci.ContractKey '
			SET @FROM = @FROM + 'Left JOIN dbo.DimItem di '
			SET @FROM = @FROM + ' ON sci.ItemKey = di.ItemKey '
			SET @FROM = @FROM + 'AND sc.VendorKey = di.ItemVendorKey '
		END
	IF @bolLocationDesc=1 OR @bolLocationName=1
		BEGIN
			SET @FROM = @FROM + 'Left JOIN cc.SupplierContractLocation scl '
			SET @FROM = @FROM + 'ON sc.ContractKey = scl.ContractKey '
		END
	IF @bolAccountName=1
		BEGIN
			SET @FROM = @FROM + 'Left JOIN cc.SupplierContractAccount sca '
			SET @FROM = @FROM +' ON sc.ContractKey = sca.ContractKey '
			SET @FROM = @FROM +'Left JOIN dbo.DimAccount da '
			SET @FROM = @FROM +'ON sca.AccountKey = da.AccountKey '
		END
	
	IF @bolLLSPGCode=1 OR @bolLLSPGDesc = 1
		BEGIN
			SET @FROM = @FROM + 'LEFT JOIN cc.SupplierContractItemGroupX sci3 '
			SET @FROM = @FROM + 'ON sc.ContractKey = sci3.ContractKey '
			SET @FROM = @FROM + 'AND sci3.ItemGroupLevel = 3 '
			SET @FROM = @FROM + 'LEFT JOIN lion.vwItemGroup3 ig3 '
			SET @FROM = @FROM + 'ON sci3.ItemGroupXKey = ig3.ItemGroup3Key '
		END
	IF @bolMPGCode=1 OR @bolMPGDesc=1
		BEGIN
			SET @FROM = @FROM + 'LEFT JOIN cc.SupplierContractItemGroupX sci1 '
			SET @FROM = @FROM + 'ON sc.ContractKey = sci1.ContractKey '
			SET @FROM = @FROM + 'AND sci1.ItemGroupLevel = 1 '
			SET @FROM = @FROM + 'LEFT JOIN lion.vwItemGroup1 ig1 '
			SET @FROM = @FROM + 'ON sci1.ItemGroupXKey = ig1.ItemGroup1Key 	'
		END
	--SET @FROM = @FROM +'OUTER APPLY lion.fn_getPendingSumByVendor('+ CAST(@vendorkey AS VARCHAR(50)) +') clmSum '
	SET @FROM = @FROM +'OUTER APPLY lion.fn_getPendingSumByVendor(sc.ContractID) clmSum '
	SET @FROM = @FROM +'CROSS APPLY lion.fn_getHealth(sc.ContractKey) caHealth '
	DECLARE @foundone INT
	SET @foundone = 0
	SET @WHERE = ' WHERE '
	IF @bolContractID = 1
		BEGIN
			SET @WHERE = @WHERE + '(sc.VendorKey = ' + CAST(@vendorKey AS VARCHAR(MAX)) + ' AND sc.ContractID LIKE ''' +  @searchValue + ''' AND scr.ExpirationDate > ''12/31/2014''' + ' AND sc.StatusKey = 6 ) '
			SET @strAnd = ' OR '
			SET @foundone = @foundone + 1
		END
	IF @bolContractRef = 1 
		BEGIN
			IF @foundone = 0 BEGIN SET @strAnd = '' END
			SET @WHERE = @WHERE + @strAnd + '(sc.VendorKey = ' + CAST(@vendorKey AS VARCHAR(MAX)) + ' AND scr.ContractReference LIKE ''' +  @searchValue + ''' AND scr.ExpirationDate > ''12/31/2014''' + ' AND sc.StatusKey = 6 ) '
			SET @strAnd = ' OR '
			SET @foundone = @foundone + 1
		END
	IF @bolVendorStock = 1
		BEGIN
			IF @foundone = 0 BEGIN SET @strAnd = '' END
			SET @WHERE = @WHERE + @strAnd + '(sc.VendorKey = ' + CAST(@vendorKey AS VARCHAR(MAX)) + ' AND di.VendorStockNumber LIKE ''' +  @searchValue + ''' AND scr.ExpirationDate > ''12/31/2014''' + ' AND sc.StatusKey = 6 ) '
			SET @strAnd = ' OR '
			SET @foundone = @foundone + 1
		END
	IF @bolItemDesc = 1
		BEGIN
			IF @foundone = 0 BEGIN SET @strAnd = '' END
			SET @WHERE = @WHERE + @strAnd + '(sc.VendorKey = ' + CAST(@vendorKey AS VARCHAR(MAX)) + ' AND di.ItemDescription LIKE ''' +  @searchValue + ''' AND scr.ExpirationDate > ''12/31/2014''' + ' AND sc.StatusKey = 6 ) '
			SET @strAnd = ' OR '
			SET @foundone = @foundone + 1
		END
	IF @bolLLSPGCode = 1
		BEGIN
			IF @foundone = 0 BEGIN SET @strAnd = '' END
			SET @WHERE = @WHERE + @strAnd + '(sc.VendorKey = ' + CAST(@vendorKey AS VARCHAR(MAX)) + ' AND ig3.LLSPGCode LIKE ''' +  @searchValue + ''' AND scr.ExpirationDate > ''12/31/2014''' + ' AND sc.StatusKey = 6 ) '
			SET @strAnd = ' OR '
			SET @foundone = @foundone + 1
		END
	IF @bolLLSPGDesc = 1
		BEGIN
			IF @foundone = 0 BEGIN SET @strAnd = '' END
			SET @WHERE = @WHERE + @strAnd + '(sc.VendorKey = ' + CAST(@vendorKey AS VARCHAR(MAX)) + ' AND ig3.LLSPGDescription LIKE ''' +  @searchValue + ''' AND scr.ExpirationDate > ''12/31/2014''' + ' AND sc.StatusKey = 6 ) '
			SET @strAnd = ' OR '
			SET @foundone = @foundone + 1
		END
	IF @bolMPGCode = 1
		BEGIN
			IF @foundone = 0 BEGIN SET @strAnd = '' END
			SET @WHERE = @WHERE + @strAnd + '(sc.VendorKey = ' + CAST(@vendorKey AS VARCHAR(MAX)) + ' AND ig1.MPGCode LIKE ''' +  @searchValue + ''' AND scr.ExpirationDate > ''12/31/2014''' + ' AND sc.StatusKey = 6 ) '
			SET @strAnd = ' OR '
			SET @foundone = @foundone + 1
		END
	IF @bolMPGDesc = 1
		BEGIN
			IF @foundone = 0 BEGIN SET @strAnd = '' END
			SET @WHERE = @WHERE + @strAnd + '(sc.VendorKey = ' + CAST(@vendorKey AS VARCHAR(MAX)) + ' AND ig1.MPGDescription LIKE ''' +  @searchValue + ''' AND scr.ExpirationDate > ''12/31/2014''' + ' AND sc.StatusKey = 6 ) '
			SET @strAnd = ' OR '
			SET @foundone = @foundone + 1
		END
 IF @bolLocationName = 1
		BEGIN
			IF @foundone = 0 BEGIN SET @strAnd = '' END
			SET @WHERE = @WHERE + @strAnd + '(sc.VendorKey = ' + CAST(@vendorKey AS VARCHAR(MAX)) + ' AND scl.LocationName  LIKE ''' +  @searchValue + ''' AND scr.ExpirationDate > ''12/31/2014''' + ' AND sc.StatusKey = 6 ) '
			SET @strAnd = ' OR '
			SET @foundone = @foundone + 1
		END
 IF @bolLocationDesc = 1
		BEGIN
			IF @foundone = 0 BEGIN SET @strAnd = '' END
			SET @WHERE = @WHERE + @strAnd + '(sc.VendorKey = ' + CAST(@vendorKey AS VARCHAR(MAX)) + ' AND scl.LocationDescription  LIKE ''' +  @searchValue + ''' AND scr.ExpirationDate > ''12/31/2014''' + ' AND sc.StatusKey = 6 ) '
			SET @strAnd = ' OR '
			SET @foundone = @foundone + 1
		END
 
 IF @bolAccountName = 1
		BEGIN
			IF @foundone = 0 BEGIN SET @strAnd = '' END
			SET @WHERE = @WHERE + @strAnd + '(sc.VendorKey = ' + CAST(@vendorKey AS VARCHAR(MAX)) + ' AND da.AccountName  LIKE ''' +  @searchValue + ''' AND scr.ExpirationDate > ''12/31/2014''' + ' AND sc.StatusKey = 6 ) '
			SET @strAnd = ' OR '
			SET @foundone = @foundone + 1
		END

IF @bolJobSite = 1
		BEGIN
			IF @foundone = 0 BEGIN SET @strAnd = '' END
			SET @WHERE = @WHERE + @strAnd + '(sc.VendorKey = ' + CAST(@vendorKey AS VARCHAR(MAX)) + ' AND sc.JobSite  LIKE ''' +  @searchValue + ''' AND scr.ExpirationDate > ''12/31/2014''' + ' AND sc.StatusKey = 6 ) '
			SET @strAnd = ' OR '
			SET @foundone = @foundone + 1
		END

IF @bolDeveloper = 1
		BEGIN
			IF @foundone = 0 BEGIN SET @strAnd = '' END
			SET @WHERE = @WHERE + @strAnd + '(sc.VendorKey = ' + CAST(@vendorKey AS VARCHAR(MAX)) + ' AND sc.Developer  LIKE ''' +  @searchValue + ''' AND scr.ExpirationDate > ''12/31/2014''' + ' AND sc.StatusKey = 6 ) '
			SET @strAnd = ' OR '
			SET @foundone = @foundone + 1
		END
  
  SET @sql = @select + @from + @where
 
  DECLARE @t TABLE (
 	[Status] [VARCHAR](7),
	[Contract ID] [VARCHAR](50),
	[Wolseley Contract ID] [VARCHAR](9) ,
	[Start Date] [DATE] ,
	[Expiry Date] [DATE] ,
	[Contact Name] [dbo].[Description_Normal_type] ,
	[Contact Email] [dbo].[Description_Normal_type] ,
	[PendingClaimAmounts] [DECIMAL](38, 8) ,
	[PendingClaims] [INT] ,
	[Health] [VARCHAR](6) ,
	[ContractReferenceKey] [INT] 
  )

  INSERT INTO @t
  EXEC(@Sql)
  SELECT
	*  
	FROM @t
END;
GO
