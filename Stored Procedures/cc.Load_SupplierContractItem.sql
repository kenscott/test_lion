SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE  PROCEDURE [cc].[Load_SupplierContractItem] AS 
	BEGIN

	SET NOCOUNT ON

	EXEC LogDCPEvent 'cc.Load_SupplierContractItem', 'B'

	DELETE cp --LPF-2522
	FROM Lion_Staging.dbo.ContractProduct cp
	LEFT JOIN (SELECT FullContractNumber, ProductsIncluded, MIN(RowIndex) MinRowIndex
				FROM Lion_Staging.dbo.ContractProduct
				GROUP BY  FullContractNumber, ProductsIncluded) d ON d.MinRowIndex = cp.RowIndex
	WHERE d.MinRowIndex IS NULL
	
	DECLARE @RowsI INT,
			@RowsU INT,
			@RowsD INT

-- load new/changed into SupplierContractItem
	INSERT INTO cc.SupplierContractItem(
		ContractKey ,
		ItemKey ,
		ClaimBackAmount,
		ClaimBackPerc,
		StartDT,
		ExpiryDT,
		ClaimType,
		CreatedOnDT
	)
	SELECT 
		sc.ContractKey,
		di.ItemKey,
		CASE 
			WHEN MAX(CAST(cp.ProductDiscountVal AS MONEY) / 100.0) <> 0 THEN MAX(CAST(cp.ProductDiscountVal AS MONEY) / 100.0) 
			ELSE 0.00 
		END,
		CASE 
			WHEN MAX(CAST(cp.ProductDiscountVal AS MONEY)) = 0 AND MAX(CAST(cp.ProductDiscountPct AS MONEY)) = 0 THEN NULL 
			WHEN MAX(CAST(cp.ProductDiscountPct AS MONEY) / 100.0) <> 0 THEN MAX(CAST(cp.ProductDiscountPct AS MONEY) / 100.0) 
			ELSE 0.0000 
		END,
		NULL, -- per LPF-2018 for now
		NULL, -- per LPF-2018 for now
		CASE
			WHEN (MAX(CAST(cp.ProductDiscountVal AS MONEY)) > 0 OR (MAX(CAST(cp.ProductDiscountVal AS MONEY)) = 0 and MAX(CAST(cp.ProductDiscountPct AS MONEY)) is null)) THEN 'A'
			WHEN (MAX(CAST(cp.ProductDiscountPct AS MONEY)) > 0 OR (MAX(CAST(cp.ProductDiscountPct AS MONEY)) = 0 and MAX(CAST(cp.ProductDiscountVal AS MONEY)) is null)) THEN 'P'
			ELSE 'A'
		END,
		sc.ContractCreationDT
	FROM Lion_Staging.dbo.ContractProduct cp
	JOIN cc.SupplierContract sc 
		ON cp.FullContractNumber = sc.FullContractNumber 
	JOIN dbo.DimItem di
		ON di.ItemUDVarchar20 = cp.ProductsIncluded 
		AND di.ItemUDVarchar1 = sc.PyramidCode
	LEFT JOIN cc.SupplierContractItem sci
		ON sci.ContractKey = sc.ContractKey
		AND sci.ItemKey = di.ItemKey
	WHERE sc.ContractSourceKey = 1
		AND sci.ContractItemKey IS NULL  --only load one time
	GROUP BY sc.ContractKey,
			 di.ItemKey,
			 sc.ContractCreationDT
	--HAVING  MAX(CAST(cp.ProductDiscountVal AS MONEY)) > 0.0   ---LPF-3377
	--OR MAX(CAST(cp.ProductDiscountPct AS MONEY)) > 0.0

	SELECT @RowsI = @@ROWCOUNT

--Check for Updates
	SELECT 
		sc.ContractKey,
		sci.ContractItemKey,
		di.ItemKey,
		CASE 
			WHEN MAX(CAST(cp.ProductDiscountVal AS MONEY) / 100.0) <> 0 THEN MAX(CAST(cp.ProductDiscountVal AS MONEY) / 100.0) 
			ELSE 0.00 
		END AS ClaimBackAmount,
		CASE 
			WHEN MAX(CAST(cp.ProductDiscountVal AS MONEY)) = 0 AND MAX(CAST(cp.ProductDiscountPct AS MONEY)) = 0 THEN NULL 
			WHEN MAX(CAST(cp.ProductDiscountPct AS MONEY) / 100.0) <> 0 THEN MAX(CAST(cp.ProductDiscountPct AS MONEY) / 100.0) 
			ELSE 0.0000 
		END AS ClaimBackPerc,
		CASE
			WHEN (MAX(CAST(cp.ProductDiscountVal AS MONEY)) > 0 OR (MAX(CAST(cp.ProductDiscountVal AS MONEY)) = 0 and MAX(CAST(cp.ProductDiscountPct AS MONEY)) is null)) THEN 'A'
			WHEN (MAX(CAST(cp.ProductDiscountPct AS MONEY)) > 0 OR (MAX(CAST(cp.ProductDiscountPct AS MONEY)) = 0 and MAX(CAST(cp.ProductDiscountVal AS MONEY)) is null)) THEN 'P'
			ELSE 'A'
		END AS ClaimType,
		sc.ContractCreationDT
	INTO #ContractItemUpdates
	FROM Lion_Staging.dbo.ContractProduct cp
	JOIN cc.SupplierContract sc 
		ON cp.FullContractNumber = sc.FullContractNumber 
	JOIN dbo.DimItem di
		ON di.ItemUDVarchar20 = cp.ProductsIncluded 
		AND di.ItemUDVarchar1 = sc.PyramidCode
	JOIN cc.SupplierContractItem sci
		ON sci.ContractKey = sc.ContractKey
		AND sci.ItemKey = di.ItemKey
	WHERE sc.ContractSourceKey = 1
	GROUP BY sc.ContractKey,
			 sci.ContractItemKey,
			 di.ItemKey,
			 sc.ContractCreationDT


	UPDATE cc.SupplierContractItem
	SET	ClaimBackAmount = u.ClaimBackAmount,
		ClaimBackPerc = u.ClaimBackPerc,
		ClaimType = u.ClaimType,
		ModifiedOnDT = GETDATE() 
	FROM #ContractItemUpdates u
	JOIN cc.SupplierContract sc
		ON sc.ContractKey = u.ContractKey 
	JOIN cc.SupplierContractItem sci
		ON u.ContractItemKey = sci.ContractItemKey
	WHERE sc.ContractSourceKey = 1
		AND (sci.ClaimBackAmount <> u.ClaimBackAmount
			OR sci.ClaimBackPerc <> u.ClaimBackPerc)	
			 	
	SELECT @RowsU = @@ROWCOUNT

--Delete records missing in the staging table
	DELETE FROM cc.SupplierContractItem
	WHERE ContractItemKey IN
	(SELECT sci.ContractItemKey
	FROM cc.SupplierContractItem sci
	JOIN cc.SupplierContract sc 
		ON sc.ContractKey = sci.ContractKey
	JOIN dbo.DimItem di
		ON di.ItemKey = sci.ItemKey
	LEFT JOIN Lion_Staging.dbo.ContractProduct cp
		ON cp.FullContractNumber = sc.FullContractNumber 
		AND cp.ProductsIncluded = di.ItemUDVarchar20
	WHERE sc.ContractSourceKey = 1
		AND cp.ContractNumber IS NULL)
		 
	SELECT @RowsD = @@ROWCOUNT

	EXEC LogDCPEvent 'cc.Load_SupplierContractItem', 'E', @RowsI, @RowsU, @RowsD

END
GO
