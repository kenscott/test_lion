SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

--   exec DCPLOG

CREATE PROCEDURE [lion].[Load_DimItemGroup3]
AS

SET NOCOUNT ON

EXEC LogDCPEvent 'ETL - Load_DimItemGroup3', 'B'

DECLARE
	@RowsI Quantity_Normal_type, 
	@RowsU Quantity_Normal_type, 
	@RowsD Quantity_Normal_type,
	@BeginRowCount INT

SET @RowsI = 0
SET @RowsU = 0
SET @RowsD = 0

EXEC DBA_IndexRebuild 'DimItemGroup3', 'Drop'


--truncate table DimItemGroup3

--This is used to look up the date key in the DimDate table
DECLARE @TodayDayKey Key_Small_Type
SET @TodayDayKey = (SELECT DayKey FROM DimDay WHERE CONVERT(VARCHAR(10), SQLDate, 120) = CONVERT(VARCHAR(10), GETDATE(), 120))

SELECT @BeginRowCount = COUNT(*) FROM dbo.DimItemGroup3


IF NOT EXISTS (SELECT 1 FROM DimItemGroup3 WHERE IG3Level1 = 'Unknown')
BEGIN
	INSERT DimItemGroup3 (
		IG3Level1, 
		IG3Level2, 
		IG3Level3, 
		IG3Level4, 
		IG3Level5, 
		IG3Level1UDVarchar1, 
		CreationDayKey, 
		ModificationDayKey
	)
	VALUES (
		'Unknown', 
		'Unknown',
		'Unknown',
		'Unknown',
		'Unknown',
		'Unknown',
		@TodayDayKey, 
		@TodayDayKey
	)
	SET @RowsI = @RowsI + @@RowCount
END


; WITH StagingDataA AS (
	SELECT 
		CASE
			WHEN p.ETLRowStatusCodeKey = 0 THEN N'Y'
			ELSE N'N'
		END AS IsValid,
		ISNULL(PYRCode, N'') AS PYRCode,
		ISNULL(GSPGCode, N'') AS GSPGCode,
		ISNULL(GSPGDescription, N'') AS GSPGDescription,
		ISNULL(HLSPGCode, N'') AS HLSPGCode,
		ISNULL(HLSPGDescription, N'') AS HLSPGDescription,
		ISNULL(LLSPGCode, N'') AS LLSPGCode,
		ISNULL(LLSPGDescription, N'') AS LLSPGDescription,
		ISNULL(ProdBrandAtLLSPG, N'') AS ProductBrand,
		ISNULL(brand.BrandKey, 1) AS BrandKey,
		COUNT(*) AS TheCount
	FROM Lion_Staging.dbo.Product p
	LEFT JOIN dbo.DimBrand brand
		ON brand.Brand = ISNULL(ProdBrandAtLLSPG, N'') 
	GROUP BY
		CASE
			WHEN p.ETLRowStatusCodeKey = 0 THEN N'Y'
			ELSE N'N'
		END,
		ISNULL(PYRCode, N''),
		ISNULL(GSPGCode, N''),
		ISNULL(GSPGDescription, N''),
		ISNULL(HLSPGCode, N''),
		ISNULL(HLSPGDescription, N''),
		ISNULL(LLSPGCode, N''),
		ISNULL(LLSPGDescription, N''),
		ISNULL(ProdBrandAtLLSPG, N''),
		ISNULL(brand.BrandKey, 1)
), StagingData AS (
	SELECT 
		PYRCode,
		GSPGCode,
		GSPGDescription,
		HLSPGCode,
		HLSPGDescription,
		LLSPGCode,
		LLSPGDescription,
		ProductBrand,
		BrandKey,
		IsValid,
		TheCount,
		ROW_NUMBER() OVER (
			PARTITION BY ISNULL(PYRCode, N''), ISNULL(LLSPGCode, N'') 
			ORDER BY 
				IsValid DESC,
				TheCount DESC, 
				ISNULL(LLSPGDescription, N''), 
				ISNULL(HLSPGCode, N''),
				ISNULL(HLSPGDescription, N''), 
				ISNULL(GSPGCode, N'') ,
				ISNULL(GSPGDescription, N''),
				ISNULL(ProductBrand, N'')
		) AS RowRank
	FROM
		StagingDataA
)
MERGE dbo.DimItemGroup3 AS Tgt
USING 
	(
	SELECT 
		BrandKey			AS BrandKey,
		ProductBrand		AS IG3Level5,
		PYRCode				AS IG3Level4,
		PYRCode				AS IG3Level4UDVarchar1,
		GSPGCode			AS IG3Level3,
		GSPGDescription		AS IG3Level3UDVarchar1,
		HLSPGCode			AS IG3Level2,
		HLSPGDescription	AS IG3Level2UDVarchar1,
		LLSPGCode			AS IG3Level1,
		LLSPGDescription	AS IG3Level1UDVarchar1
	FROM StagingData
	WHERE
		StagingData.RowRank = 1
) AS Src
ON Tgt.IG3Level1 = Src.IG3Level1
AND Tgt.IG3Level4 = Src.IG3Level4
WHEN MATCHED AND
	CHECKSUM(--CheckSum for target table.  This needs to check all rows that we will UPDATE IF a find a change
		Tgt.IG3Level2,
		Tgt.IG3Level3,
		Tgt.IG3Level5,
		Tgt.BrandKey,
		Tgt.IG3Level1UDVarchar1,
		Tgt.IG3Level2UDVarchar1,
		Tgt.IG3Level3UDVarchar1,
		Tgt.IG3Level4UDVarchar1
	)
	<>
	CHECKSUM(--CheckSum for target table.  This needs to check all rows that we will UPDATE IF a find a change
		Src.IG3Level2,
		Src.IG3Level3,
		Src.IG3Level5,
		Src.BrandKey,
		Src.IG3Level1UDVarchar1,
		Src.IG3Level2UDVarchar1,
		Src.IG3Level3UDVarchar1,
		Src.IG3Level4UDVarchar1
	)
THEN UPDATE 
	SET
		Tgt.ModificationDayKey = @TodayDayKey,
		Tgt.IG3Level2 = Src.IG3Level2,
		Tgt.IG3Level3 = Src.IG3Level3,
		Tgt.IG3Level5 = Src.IG3Level5,
		Tgt.BrandKey = Src.BrandKey,
		Tgt.IG3Level1UDVarchar1 = Src.IG3Level1UDVarchar1,
		Tgt.IG3Level2UDVarchar1 = Src.IG3Level2UDVarchar1,
		Tgt.IG3Level3UDVarchar1 = Src.IG3Level3UDVarchar1,
		Tgt.IG3Level4UDVarchar1 = Src.IG3Level4UDVarchar1
WHEN NOT MATCHED 
	THEN INSERT (
		IG3Level1,
		IG3Level2,
		IG3Level3,
		IG3Level4,
		IG3Level5, 
		BrandKey,
		IG3Level1UDVarchar1,
		IG3Level2UDVarchar1,
		IG3Level3UDVarchar1,
		IG3Level4UDVarchar1,
		IG3Level5UDVarchar1,
		CreationDayKey,
		ModificationDayKey
	)
	VALUES (
		IG3Level1,--1
		IG3Level2,--2
		IG3Level3,--3
		IG3Level4,--4
		IG3Level5,--5
		BrandKey,
		IG3Level1UDVarchar1,
		IG3Level2UDVarchar1,
		IG3Level3UDVarchar1,
		IG3Level4UDVarchar1,
		'None',--5
		@TodayDayKey, 
		@TodayDayKey
)
;
SELECT 
	@RowsU = @RowsU + (@@RowCOunt - (COUNT(*) - @BeginRowCount)),
	@RowsI =  COUNT(*) - @BeginRowCount
FROM dbo.DimItemGroup3


EXEC DBA_IndexRebuild 'DimItemGroup3', 'Create'

EXEC LogDCPEvent 'ETL - Load_DimItemGroup3', 'E', @RowsI, @RowsU, @RowsD

SELECT TOP 10 * FROM DimItemGroup3

GO
