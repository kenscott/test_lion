SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		José A. Castaños
-- Create date: 3-Feb-2016
-- Description:	Inserts products for marketing report into table
-- =============================================
CREATE PROCEDURE [lion].[InsertMarketingReportProduct] 
	-- Add the parameters for the stored procedure here
	@ProductCode UDVarchar_type , 
	@Username UDVarchar_type 
AS
BEGIN
	SET NOCOUNT ON;

    INSERT INTO lion.MarketingReportItem
            ( ItemKey ,
              ProductCode ,
              PyramidCode ,
              LastModifiedBy ,
              LastModifiedDate
            )
    SELECT i.ItemKey, @ProductCode, N'1', @Username, GETDATE()
	  FROM lion.vwItem i
	 WHERE i.ProductCode = @ProductCode AND
		   i.ItemPyramidCode = '1'
END
GO
