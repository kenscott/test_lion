SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE [cm].[Load_ClaimJournal] (@BatchId INT)

AS

DECLARE @UpdatedStatusRowData TABLE (
		ClaimJournalKey KEY_NORMAL_TYPE)

--Check for Status Change
INSERT INTO @UpdatedStatusRowData
SELECT cj.ClaimJournalKey
FROM cm.StagingClaimJournalIncoming scj
INNER JOIN cm.ClaimJournal cj
	ON cj.ODSClaimJournalKey = scj.ODSClaimJournalKey
WHERE scj.BatchID = @BatchId
	AND cj.ClaimJournalStatus <> scj.[Status]

--Update existing records
UPDATE cj
SET 
	[ClientInvoiceLineUniqueID] = scj.[TransactionID]
    ,[VendorNumber] = scj.[SupplierID]
    ,[VendorDescription] = scj.[SupplierName]
    ,[OriginalContractID] = scj.[POSContractID]
    ,[CurrentContractID] = scj.[ARPContractID]
    ,[ClaimLineValue] = scj.[ClaimLineValue]
    ,[TradingMargin] = scj.[TradingMargin]
    ,[ClaimJournalStatus] = scj.[Status]
    ,[ARPResult] = scj.[ARPResult]
    ,[ARPRecommendation] = scj.[ARPRecommendation]
    ,[VendorKey] = dv.VendorKey
    ,[BatchKey] = scj.[BatchID]
    ,[PyramidCode] = scj.[PyramidCode]
    ,[ClaimType] = scj.[ClaimType]
    ,[ClaimJournalStatusDate] = scj.[StatusDate]
    ,[ARPResultDate] = scj.[ARPResultDate]
    ,[ARPReason] = scj.[ARPReason]
	,[FinancialMargin] = scj.[FinancialMargin]
	,[ConfidentialTermsGPAdjustAmt] = scj.[ConfidentialTermsGPAdjustAmt]
	,[PriceDeferralGPAdjustAmt] = scj.[PriceDeferralGPAdjustAmt]
	,[SpecialDealGPAdjustAmt] = scj.[SpecialDealGPAdjustAmt]
	,[ClearanceDealGPAdjustAmt] = scj.[ClearanceDealGPAdjustAmt]

FROM cm.StagingClaimJournalIncoming scj
LEFT JOIN dbo.DimVendor dv
	ON dv.VendorNumber = scj.SupplierID
INNER JOIN cm.ClaimJournal cj
	ON cj.ODSClaimJournalKey = scj.ODSClaimJournalKey
WHERE scj.BatchID = @BatchId

--Create new claim journal records
INSERT INTO [cm].[ClaimJournal]
           ([ODSClaimJournalKey]
           ,[ClaimMonth]
           ,[ClientInvoiceLineUniqueID]
           ,[VendorNumber]
           ,[VendorDescription]
           ,[OriginalContractID]
           ,[CurrentContractID]
           ,[ClaimLineValue]
           ,[TradingMargin]
           ,[ClaimJournalStatus]
           ,[ARPResult]
           ,[ARPRecommendation]
           ,[VendorKey]
           ,[CreationDate]  --Today's date    
		   ,[ODSBatchKey]
           ,[PyramidCode]
           ,[ClaimType]
           ,[ClaimJournalStatusDate] 
           ,[ARPResultDate]
           ,[ARPReason]
		   ,[FinancialMargin]
		   ,[ConfidentialTermsGPAdjustAmt]
		   ,[PriceDeferralGPAdjustAmt]
		   ,[SpecialDealGPAdjustAmt]
		   ,[ClearanceDealGPAdjustAmt]
		   ,[SupplierRef]
		   ,[OriginalClientInvoiceLineUniqueID]
		   )
   
SELECT 
			scj.[ODSClaimJournalKey],
			CAST(CONVERT(VARCHAR(4), DATEPART(yy, GETDATE())) + CONVERT(VARCHAR(2), DATEPART(mm, GETDATE())) AS INT),
			scj.[TransactionID],
			scj.[SupplierID],
			scj.[SupplierName],
			scj.[POSContractID],
			scj.[ARPContractID],
			scj.[ClaimLineValue],
			scj.[TradingMargin],
			scj.[Status],
			scj.[ARPResult],
			scj.[ARPRecommendation],
			dv.VendorKey,
			GETDATE(),
			scj.[BatchID],
			scj.[PyramidCode],
			scj.[ClaimType],
			scj.[StatusDate],
			scj.[ARPResultDate],
			scj.[ARPReason],
			scj.[FinancialMargin],
			scj.[ConfidentialTermsGPAdjustAmt],
			scj.[PriceDeferralGPAdjustAmt],
			scj.[SpecialDealGPAdjustAmt],
			scj.[ClearanceDealGPAdjustAmt],
			scj.[SupplierReferenceNumber],
			scj.[OriginalTransactionID]

FROM cm.StagingClaimJournalIncoming scj
LEFT JOIN dbo.DimVendor dv
	ON dv.VendorNumber = scj.SupplierID
LEFT JOIN cm.ClaimJournal cj
	ON cj.ODSClaimJournalKey = scj.ODSClaimJournalKey
LEFT JOIN cc.SupplierContract sc
	ON sc.ContractID = scj.ARPContractID
WHERE cj.ClaimJournalKey IS NULL
	AND scj.BatchID = @BatchId
	AND scj.ARPResult <> 'ARP Deleted'  --ignore when ARP Deleted

--Log Import of Claim Line
INSERT INTO cm.ClaimJournalLog
	(
	[WebUserKey],
	[ClaimJournalKey],
	[Notes],
	[CreationDate],
	[ClaimAction]
	)
SELECT 
	1,
	cj.ClaimJournalKey,
	'From: blank To: ' + s.ClaimJournalStatusDescription + '; Batch ID = ' + CAST(@BatchId AS VARCHAR(100)) + '',
	GETDATE(),
	'Import Claim Line'
FROM cm.ClaimJournal cj
INNER JOIN cm.ClaimJournalStatus s
	ON s.ClaimJournalStatus = cj.ClaimJournalStatus
WHERE cj.ODSBatchKey = @BatchId

--If newClaimJournalRecord.ARPResult = ARPDeleted or ARPAdditional then update the original claim line status

DECLARE @updateSupplierStatusD TABLE (
	ClaimJournalKey KEY_NORMAL_TYPE,
	oldClaimJournalStatus varchar(1),
	creationdate datetime
)

UPDATE cj
SET ClaimJournalStatus = 'D',
	RejectionReasonKey = 2
OUTPUT INSERTED.ClaimJournalKey, DELETED.ClaimJournalStatus AS oldClaimJournalStatus, GETDATE() AS creationdate INTO @updateSupplierStatusD  
FROM cm.ClaimJournal cj
WHERE cj.ClaimJournalKey IN (
	SELECT cj.ClaimJournalKey
	FROM cm.StagingClaimJournalIncoming s
	INNER JOIN cm.ClaimJournal cj
		ON cj.ClientInvoiceLineUniqueID = s.TransactionID
		AND cj.CurrentContractID = s.ARPContractID
	WHERE s.ARPResult IN ('ARP Deleted', 'ARP Additional')
		AND cj.ClaimJournalStatus IN ('P', 'R', 'X')
		AND cj.ClaimType = 'C'
		AND cj.ODSBatchKey <> @BatchId
		AND s.BatchID = @BatchId
	)

INSERT INTO cm.ClaimJournalLog
	(
	[WebUserKey],
	[ClaimJournalKey],
	[Notes],
	[CreationDate],
	[ClaimAction]
	)
SELECT 1,
	  d.ClaimJournalKey,
	  'From: ' + s.ClaimJournalStatusDescription + ' to Deleted',
	  GETDATE(),
	 'Claim Journal Status Update'
FROM @updateSupplierStatusD d
INNER JOIN cm.ClaimJournalStatus s
	ON s.ClaimJournalStatus = d.oldClaimJournalStatus

--Find matches for all incoming ARP New and set status to review
DECLARE @updateSupplierStatusR TABLE (
	ClaimJournalKey KEY_NORMAL_TYPE,
	oldClaimJournalStatus varchar(1),
	creationdate datetime
)

DECLARE @Multiples TABLE (
	ClientInvoiceLineUniqueID int)
INSERT INTO @Multiples
	(ClientInvoiceLineUniqueID)
SELECT DISTINCT ClientInvoiceLineUniqueID
FROM cm.ClaimJournal
GROUP BY ClientInvoiceLineUniqueID
HAVING count(*) > 1

UPDATE cj
SET ClaimJournalStatus = 'R'
OUTPUT INSERTED.ClaimJournalKey, DELETED.ClaimJournalStatus AS oldClaimJournalStatus, GETDATE() AS creationdate INTO @updateSupplierStatusR  
FROM cm.ClaimJournal cj
WHERE cj.ClaimJournalKey IN (
	SELECT cj.ClaimJournalKey
	FROM cm.StagingClaimJournalIncoming s
	INNER JOIN @Multiples m
		ON m.ClientInvoiceLineUniqueID = s.TransactionID
	INNER JOIN cm.ClaimJournal cj
		ON cj.ClientInvoiceLineUniqueID = m.ClientInvoiceLineUniqueID
	WHERE s.ARPResult = 'ARP New'
		AND cj.ClaimJournalStatus IN ('P', 'X')
		AND cj.ClaimType = 'C'
		AND s.BatchID = @BatchId
	)

INSERT INTO cm.ClaimJournalLog
	(
	[WebUserKey],
	[ClaimJournalKey],
	[Notes],
	[CreationDate],
	[ClaimAction]
	)
SELECT 1,
	  d.ClaimJournalKey,
	  'From: ' + s.ClaimJournalStatusDescription + ' to Review',
	  GETDATE(),
	 'Claim Journal Status Update'
FROM @updateSupplierStatusR d
INNER JOIN cm.ClaimJournalStatus s
	ON s.ClaimJournalStatus = d.oldClaimJournalStatus


--Create any new Supplier Claim records
DECLARE @NewSupplierClaimRowData TABLE (
		SupplierClaimKey KEY_NORMAL_TYPE,
		ClaimStatus varchar(1))

INSERT INTO [cm].[SupplierClaim]
           ([VendorKey]
           ,[ClaimMonth]
           ,[CreationDate]
           ,[CreatedByWebUserKey]
           ,[ClaimStatus])

	OUTPUT 
			INSERTED.SupplierClaimKey,
			INSERTED.ClaimStatus
			INTO @NewSupplierClaimRowData
    
	SELECT DISTINCT 
		cj.VendorKey,
		CAST(CONVERT(VARCHAR(4), DATEPART(yy, GETDATE())) + CONVERT(VARCHAR(2), DATEPART(mm, GETDATE())) AS INT), 
		GETDATE(),
		1,
		'P'
	FROM cm.ClaimJournal cj
	LEFT JOIN cm.SupplierClaim c
		ON c.VendorKey = cj.VendorKey
		AND c.ClaimMonth = cj.ClaimMonth
		AND c.ClaimStatus IN ('P', 'R')  --don't create new if these exist 
	WHERE cj.ODSBatchKey = @BatchId
		AND c.SupplierClaimKey IS NULL

--Log new Supplier Claim
INSERT INTO cm.ClaimLog
	(
	[WebUserKey],
	[SupplierClaimKey],
	[Notes],
	[CreationDate],
	[ClaimAction]
	)
SELECT 
	1,
	d.SupplierClaimKey,
	'From: blank To: ' + s.SupplierClaimStatusDescription,
	GETDATE(),
	'Create Supplier Claim'
FROM @NewSupplierClaimRowData d
INNER JOIN cm.SupplierClaimStatus s
	ON s.SupplierClaimStatus = d.ClaimStatus

--If the supplier claims status is R (ready for claim) and an associated supplier claim line has changed status, the supplier claim should change to P (pending)
	DECLARE @SupplierClaimUpdateRowData TABLE (
		SupplierClaimKey KEY_NORMAL_TYPE,
		ClaimStatus VARCHAR(1))
	
	UPDATE sc
	SET ClaimStatus = 'P'
	OUTPUT INSERTED.SupplierClaimKey,
			INSERTED.ClaimStatus
		INTO @SupplierClaimUpdateRowData
	FROM cm.SupplierClaim sc
	INNER JOIN cm.ClaimJournal cj
		ON cj.VendorKey = sc.VendorKey
		AND cj.ClaimMonth = sc.ClaimMonth
	INNER JOIN @UpdatedStatusRowData u
		ON u.ClaimJournalKey = cj.ClaimJournalKey
	WHERE sc.ClaimStatus = 'R'

--Log Supplier Claim status change
INSERT INTO cm.ClaimLog
	(
	[WebUserKey],
	[SupplierClaimKey],
	[Notes],
	[CreationDate],
	[ClaimAction]
	)
SELECT 
	1,
	d.SupplierClaimKey,
	'From: Ready to Claim  To: ' + s.SupplierClaimStatusDescription,
	GETDATE(),
	'Supplier Claim System Status Change'
FROM @SupplierClaimUpdateRowData d
INNER JOIN cm.SupplierClaimStatus s
	ON s.SupplierClaimStatus = d.ClaimStatus

--Send email notification
DECLARE @Message VARCHAR(250)
DECLARE @RecordCount INT

SET @RecordCount = (SELECT COUNT(*) FROM cm.ClaimJournal WHERE ODSBatchKey = @BatchId)
SET @Message = CAST(@BatchId AS VARCHAR(20)) + ' Claim Journal Batch; ' + CAST(@RecordCount AS VARCHAR(20)) + ' # of Records Loaded'

EXEC msdb.dbo.sp_send_dbmail  
@recipients = 'melanie.zaremba@enterbridge.com; scott.toolson@enterbridge.com; devin.huch@enterbridge.com; kevin.kennedy@enterbridge.com',
@subject = 'Claim Journal Data Loaded',
@body = @Message, 
@body_format = 'HTML'

GO
