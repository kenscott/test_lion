SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [lion].[ScrubStagingData]
AS
BEGIN

	/*
	SELECT
		StatusCodeDescription,
		COUNT(*) AS RawTransactionCount
	FROM
		Lion_Staging.dbo.Transactions t (NOLOCK)
	INNER JOIN ETLRowStatusCode e
		ON e.ETLRowStatusCodeKey = t.ETLRowStatusCodeKey
	GROUP BY 
		StatusCodeDescription
	ORDER BY 2 DESC

	SELECT
		StatusCodeDescription,
		SUM(
			CASE
				WHEN T.ETLRowStatusCodeKey IS NOT NULL THEN 1
				ELSE 0
			END) AS RawTransactionCount
	FROM ETLRowStatusCode e (NOLOCK)
	LEFT JOIN Lion_Staging.dbo.Transactions t (NOLOCK)
		ON e.ETLRowStatusCodeKey = t.ETLRowStatusCodeKey
	GROUP BY 
		StatusCodeDescription
	ORDER BY 2 DESC
	*/



	SET NOCOUNT ON
	
	EXEC LogDCPEvent N'ETL - ScrubStagingData', N'B'

	DECLARE 
		@RowsI INT, 
		@RowsU INT, 
		@RowsD INT
	
	SET @RowsI = 0
	SET @RowsU = 0
	SET @RowsD = 0

	EXEC LogDCPEvent N'ETL - ScrubStagingData: Excluding Transactions that are old/clearing old values', N'B'
	DECLARE
		@MinInvoiceDate AS DATE

	--SET @MinInvoiceDate = (
	--	SELECT dm.FirstSQLDate
	--	FROM (
	--		SELECT 
	--			MAX(CAST(TransactionDate AS DATE)) AS MaxDate
	--		FROM Lion_Staging.dbo.Transactions
	--		) t
	--	INNER JOIN dbo.DimDay dd 
	--		ON CONVERT(VARCHAR(10),dd.SQLDate,120) = CONVERT(VARCHAR(10), MaxDate, 120)
	--	INNER JOIN dbo.DimMonth dm
	--		ON dm.MonthKey = dd.MonthKey - 14
	--)

	SET @MinInvoiceDate = (
		SELECT dd.SQLDate
		FROM (
			SELECT 
				MIN(CAST(TransactionDate AS DATE)) AS MinDate
			FROM Lion_Staging.dbo.Transactions
			) t
		INNER JOIN dbo.DimDay dd 
			ON CONVERT(VARCHAR(10),dd.SQLDate,120) = CONVERT(VARCHAR(10), MinDate, 120)
	)
	

	UPDATE t
	SET
		t.ETLRowStatusCodeKey = 
			CASE 
				WHEN TransactionDate < @MinInvoiceDate THEN 1
				ELSE 0
			END 
	FROM
		Lion_Staging.dbo.Transactions t
	WHERE 
		t.ETLRowStatusCodeKey <> 
			CASE 
				WHEN TransactionDate < @MinInvoiceDate THEN 1
				ELSE 0
			END 
	SET @RowsU = @@RowCOunt
	EXEC LogDCPEvent N'ETL - ScrubStagingData: Excluding Transactions that are old/clearing old values', N'E', 0, @RowsU, 0

	EXEC LogDCPEvent N'ETL - ScrubStagingData: Excluding Transactions - Missing PyramidCode + ProductCode', N'B'
	UPDATE t
	SET
		t.ETLRowStatusCodeKey = 2
	FROM
		Lion_Staging.dbo.Transactions t
	WHERE
		t.ETLRowStatusCodeKey = 0
		AND NOT EXISTS (
			SELECT 1 FROM Lion_Staging.dbo.Product p 
			WHERE 
				p.PYRCode = ISNULL(t.PyramidCode, N'')
				AND p.ProductCode = t.ProductCode
			)
	SET @RowsU = @@RowCOunt
	EXEC LogDCPEvent N'ETL - ScrubStagingData: Excluding Transactions - Missing PyramidCode + ProductCode', N'E', 0, @RowsU, 0

	EXEC LogDCPEvent N'ETL - ScrubStagingData: Excluding Transactions - Missing Customer', N'B'
	UPDATE t
	SET
		t.ETLRowStatusCodeKey = 3
	FROM
		Lion_Staging.dbo.Transactions t
	WHERE
		t.ETLRowStatusCodeKey = 0
		AND NOT EXISTS (SELECT 1 FROM Lion_Staging.dbo.Customer c WHERE c.AccountID = t.AccountID)
	SET @RowsU = @@RowCOunt
	EXEC LogDCPEvent N'ETL - ScrubStagingData: Excluding Transactions - Missing Customer', N'E', 0, @RowsU, 0


	EXEC lion.ScrubCustomers
	EXEC lion.ScrubProducts


	EXEC LogDCPEvent N'ETL - ScrubStagingData: Transactions where customer excluded', N'B'
	UPDATE t
	SET
		t.ETLRowStatusCodeKey = c.ETLRowStatusCodeKey
	FROM
		Lion_Staging.dbo.Transactions t
	INNER JOIN Lion_Staging.dbo.Customer c
		ON c.AccountID = t.AccountID
	WHERE
		t.ETLRowStatusCodeKey = 0
		AND c.ETLRowStatusCodeKey <> 0
	SET @RowsU = @@RowCOunt
	EXEC LogDCPEvent N'ETL - ScrubStagingData: Transactions where customer excluded', N'E', 0, @RowsU, 0
	

	EXEC LogDCPEvent N'ETL - ScrubStagingData: Transactions where product excluded', N'B'
	UPDATE t
	SET
		t.ETLRowStatusCodeKey = p.ETLRowStatusCodeKey
	FROM
		Lion_Staging.dbo.Transactions t
	INNER JOIN Lion_Staging.dbo.Product p
		ON p.PYRCode = ISNULL(t.PyramidCode, N'')
		AND p.ProductCode = t.ProductCode
	WHERE
		t.ETLRowStatusCodeKey = 0
		AND p.ETLRowStatusCodeKey <> 0
	SET @RowsU = @@RowCOunt
	EXEC LogDCPEvent N'ETL - ScrubStagingData: Transactions where product excluded', N'E', 0, @RowsU, 0



	EXEC LogDCPEvent N'ETL - ScrubStagingData: Excluding Transactions - CreditReason G, H, J', N'B'
	UPDATE t
	SET
		t.ETLRowStatusCodeKey = 9
	FROM
		Lion_Staging.dbo.Transactions t
	WHERE
		t.ETLRowStatusCodeKey = 0
		AND CreditReason IN (N'G', N'H', N'J')
	SET @RowsU = @@RowCOunt
	EXEC LogDCPEvent N'ETL - ScrubStagingData: Excluding Transactions - CreditReason G, H, J', N'E', 0, @RowsU, 0
	
	
	EXEC LogDCPEvent N'ETL - ScrubStagingData: Excluding Transactions - SalesOrigin H, R', N'B'
	UPDATE t
	SET
		t.ETLRowStatusCodeKey = 10
	FROM
		Lion_Staging.dbo.Transactions t
	WHERE
		t.ETLRowStatusCodeKey = 0
		AND SalesOrigin IN (N'H', N'R')
	SET @RowsU = @@RowCOunt
	EXEC LogDCPEvent N'ETL - ScrubStagingData: Excluding Transactions - SalesOrigin H, R', N'E', 0, @RowsU, 0
	

	EXEC LogDCPEvent N'ETL - ScrubStagingData: Excluding Transactions - Pack Components', N'B'
	UPDATE t
	SET
		t.ETLRowStatusCodeKey = 11
	FROM
		Lion_Staging.dbo.Transactions t
	WHERE
		t.ETLRowStatusCodeKey = 0
		AND VariablePackInd = N'C'
	SET @RowsU = @@RowCOunt
	EXEC LogDCPEvent N'ETL - ScrubStagingData: Excluding Transactions - Pack Components', N'E', 0, @RowsU, 0


	EXEC LogDCPEvent N'ETL - ScrubStagingData: Excluding Unmatched Credit Transaction', N'B'
	UPDATE t
		set ETLRowStatusCodeKey = 12
	FROM Lion_Staging.dbo.Transactions t
	WHERE
		t.ETLRowStatusCodeKey = 0			-- kfk 6/18
		AND t.SupplyType = N'Credit'		-- the potential credit must be credit as defined by the supply type
		AND t.CrdOrigInvNo <> N''			-- the credit line must have a real inv number		
		AND NOT EXISTS (SELECT 1 FROM Lion_Staging.dbo.Transactions t2 WHERE t2.CreditAdvanousInvoiceLineId = t.AdvanousInvoiceLineId)
	SET @RowsU = @@RowCOunt
	EXEC LogDCPEvent N'ETL - ScrubStagingData: Excluding Unmatched Credit Transaction', N'E', 0, @RowsU, 0


	EXEC LogDCPEvent N'ETL - ScrubStagingData: Excluding Credit for Excluded Transaction', N'B'
	UPDATE tCredit
	SET
		tCredit.ETLRowStatusCodeKey = 13
	FROM
		Lion_Staging.dbo.Transactions tCredit
	INNER JOIN Lion_Staging.dbo.Transactions tExcluded
		ON tCredit.AdvanousInvoiceLineId = tExcluded.CreditAdvanousInvoiceLineId
	WHERE
		tCredit.ETLRowStatusCodeKey = 0
		AND tCredit.CrdOrigInvNo <> N''
		AND tCredit.SupplyType = N'Credit'
		AND tExcluded.ETLRowStatusCodeKey <> 0	-- kfk 6/18
	SET @RowsU = @@RowCOunt
	EXEC LogDCPEvent N'ETL - ScrubStagingData: Excluding Credit for Excluded Transaction', N'E', 0, @RowsU, 0


	EXEC LogDCPEvent N'ETL - ScrubStagingData', N'E'


END
GO
