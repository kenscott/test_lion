SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




CREATE
PROCEDURE [lion].[Load_ODSPerson]
AS

SET NOCOUNT ON

DECLARE 
	@ExtID Description_Normal_Type, 
	@FullName Description_Normal_Type, 
	@Email Description_Normal_Type, 
	@UserName Description_Normal_Type, 
	@Password Description_Normal_Type, 
	@NumManageRelationships INT, 
	@NumViewRelationships INT,
	@RowsI Quantity_Normal_type, 
	@RowsU Quantity_Normal_type, 
	@RowsD Quantity_Normal_type,
	@TodayDayKey Key_Small_Type


--This is used to look up the date key in the DimDay table
EXEC LogDCPEvent 'ETL - Load_ODSPerson', 'B'

SET @RowsI = 0
SET @RowsU = 0
SET @RowsD = 0

SET @TodayDayKey = (SELECT DayKey FROM DimDay WHERE CONVERT(VARCHAR(10), SQLDate, 120) = CONVERT(VARCHAR(10), GETDATE(), 120))


IF (SELECT COUNT(*) FROM dbo.ODSPerson where FullName = 'Coordinator') <= 0
BEGIN
	SET IDENTITY_INSERT [dbo].[ODSPerson] ON

	INSERT INTO [dbo].[ODSPerson] ([ODSPersonKey], [FullName], [Email], [ExtID], [ODSPersonUDVarchar1], [ODSPersonUDVarchar2], [EmployeeID], [ReceivesEmailIndicator]) VALUES (1, 'Coordinator', 'scott.toolson@enterbridge.com', '0', NULL, NULL, NULL, 'Y')

	--IF (SELECT COUNT(*) FROM dbo.ODSPerson where FullName = 'Cognos Administrator') <= 1
	--	INSERT INTO [dbo].[ODSPerson] ([ODSPersonKey], [FullName], [Email], [ExtID], [ODSPersonUDVarchar1], [ODSPersonUDVarchar2], [EmployeeID], [ReceivesEmailIndicator]) VALUES (2, 'Cognos Administrator', 'scott.toolson@enterbridge.com', 'CognosAdmin', NULL, NULL, NULL, 'Y')
	
	SET IDENTITY_INSERT [dbo].[ODSPerson] OFF
END


UPDATE odsp
SET 
	LevelId = u.Purpose,
	odsp.Email = u.Email,
	ODSPersonUDVarchar1 = u.UDVarchar1,
	ODSPersonUDVarchar2 = u.UDVarchar2,
	ODSPersonUDVarchar3 = u.UDVarchar3
FROM dbo.ODSPerson odsp
INNER JOIN Lion_Staging.dbo.[User] u
	ON u.ExtId = odsp.ExtID
WHERE
	ISNULL(LevelId, '') <> ISNULL(u.Purpose, '')
	OR ISNULL(odsp.Email, '') <> ISNULL(u.Email, '')
	OR ISNULL(ODSPersonUDVarchar1, '') <> ISNULL(u.UDVarchar1, '')
	OR ISNULL(ODSPersonUDVarchar2, '') <> ISNULL(u.UDVarchar2, '')
	OR ISNULL(ODSPersonUDVarchar3, '') <> ISNULL(u.UDVarchar3, '')
SET @RowsU = @RowsU + @@ROWCOUNT


/* Per https://enterbridge.atlassian.net/browse/LPF-3677 , when a user is no longer in the hierarchy file then drop their roles */
/* By clearing the fields below (the LevelID in particular), the user's rols will be adjusted/deleted when lion.Load_WebUserRolesAndPermissions runs */
UPDATE odsp
SET 
	LevelId = NULL,
	ODSPersonUDVarchar1 = NULL,
	ODSPersonUDVarchar2 = NULL,
	ODSPersonUDVarchar3 = NULL
FROM dbo.ODSPerson odsp
LEFT JOIN Lion_Staging.dbo.[User] u
	ON u.ExtId = odsp.ExtID
WHERE
	u.ExtId IS NULL
	AND (
		odsp.LevelId IS NOT NULL
		OR odsp.ODSPersonUDVarchar1 IS NOT NULL
		OR odsp.ODSPersonUDVarchar2 IS NOT NULL
		OR odsp.ODSPersonUDVarchar3 IS NOT NULL
	)
	AND odsp.ExtId <> N'0'
SET @RowsU = @RowsU + @@ROWCOUNT
	

INSERT ODSPerson (FullName, Email, ExtID, LevelId, ODSPersonUDVarchar1, ODSPersonUDVarchar2, ODSPersonUDVarchar3)
SELECT DISTINCT
	u.FullName, 
	u.Email,
	u.ExtId,
	u.Purpose,
	u.UDVarchar1,
	u.UDVarchar2,
	u.UDVarchar3
FROM Lion_Staging.dbo.[User] u
WHERE u.ExtId NOT IN (SELECT DISTINCT ExtID FROM ODSPerson)
SET @RowsI = @RowsI + @@ROWCOUNT


-- SELECT TOP 10 * FROM ODSPerson

EXEC LogDCPEvent 'ETL - Load_ODSPerson', 'E', @RowsI, @RowsU, @RowsD




GO
