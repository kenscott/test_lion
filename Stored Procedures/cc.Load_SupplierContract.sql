SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE   PROCEDURE [cc].[Load_SupplierContract] AS 
BEGIN

	SET NOCOUNT ON

	EXEC LogDCPEvent 'cc.Load_SupplierContract', 'B'

	DECLARE @RowsI INT,
			@RowsU INT,
			@BeginRowCount INT

	DECLARE @NewContractKey TABLE (
		ContractKey KEY_NORMAL_TYPE
	)

	DECLARE @BeginContractKey TABLE (
		ContractKey KEY_NORMAL_TYPE
	)

	SET @RowsI = 0
	SET @RowsU = 0

	SELECT @BeginRowCount = COUNT(*) FROM cc.SupplierContract

	INSERT INTO @BeginContractKey
	SELECT ContractKey 
	FROM cc.SupplierContract

	;WITH Con AS	(SELECT COUNT(*) OVER (PARTITION BY OriginatorBranchOrRegion+ContractNumber) ConCount, * FROM Lion_Staging.dbo.Contract)

	MERGE cc.SupplierContract AS Tgt
	USING 
	(SELECT
			CONVERT(DATE, extFrom) AS RecordFromDate, -- RecordFromDate - datetime
			CONVERT(DATE, ExtTo) AS RecordToDate, -- RecordToDate - datetime
			NULL AS LoadKey, -- LoadKey - int
			NULL as ActionCode, -- ActionCode - varchar(1)
			NULL as RunDate, -- RunDate - datetime
			c.Region,
			c.OriginatorBranchOrRegion , -- Originator - varchar(3)
			c.ContractNumber , -- ContractNumber - varchar(6)
			CASE
				WHEN dr.RegionKey IS NOT NULL THEN 'WUK'  --Region
				WHEN c.ContractOwner IN ('BA', 'YA', 'EA', 'CA', 'DA', 'HA', 'GA', 'WA', 'TA', 'MA') THEN 'WUK'  --Wolcen_Region
				WHEN dag1.AccountGroup1Key IS NOT NULL THEN c.ContractOwner  --Branch
				ELSE 'WUK' 
			END AS ContractOwner, -- ContractOwner - varchar(10)
			v.VendorKey , -- VendorKey - int
			c.ContractReference, -- ContractReference - varchar(50)
			c.ContractStartDate, -- StartDate - date
			c.ContractExpiryDate , -- ExpirationDate - date
			c.SiteNameAddress  , -- JobSite - varchar(50)
			CASE WHEN c.DatePriceFixedFrom = '' THEN NULL ELSE CONVERT(DATE, c.DatePriceFixedFrom) END AS FixedPriceDate, -- FixedPriceDate - date
			 c.PSFNumber , -- PSFNumber - varchar(10)
			 ISNULL(c.AuthorisedFlag, 'N') AS AuthorizedIndicator , -- AuthorizedIndicator - varchar(1)
			 CAST(CAST(c.Pyramid AS INT) AS VARCHAR(2)) AS PyramidCode, -- PyramidCode - varchar(2)
			 CASE when c.TotalContractAmount ='' THEN NULL ELSE CAST( c.TotalContractAmount AS MONEY) END AS ContractAmount, -- ContractAmount - decimal
			 NULL AS RTB, -- RTB - varchar(1)
			 c.NextClaimNumber , -- NextClaimNumber - varchar(10)
			 c.PeriodLastClaimed, -- LastClaimPeriod - varchar(10)
			 c.LastUserUpdate, -- ContractModificationUser - varchar(10)
			 c.LastDateUpdate , -- ContractModificationDate - date
			 NULL AS ContractRegionalCreateDate, -- ContractRegionalCreateDate - date
			 NULL AS ContractRegionalCreateTime, -- ContractRegionalCreateTime - varchar(10)
			 NULL AS ContractRegionalCreateRoutine, -- ContractRegionalCreateRoutine - varchar(20)
			 NULL AS ContractRegionalModificationDate, -- ContractRegionalModificationDate - date
			 NULL AS ContractRegionalModificationTime, -- ContractRegionalModificationTime - varchar(10)
			 NULL AS HTD, -- HTD - varchar(20)
			 ISNULL(c.ContractType1, 'C') AS ContractType1, -- ContractType1 - varchar(1)
			  c.ContractType2 , -- ContractType2 - varchar(1)
			  c.ContractType3, -- ContractType3 - varchar(1)
			  0 AS ContractLevel, -- ContractLevel - smallint
			  1 AS JobFlag, -- JobFlag - bit
			 c.Region + c.OriginatorBranchOrRegion + c.ContractNumber AS FullContractNumber,   -- FullContractNumber - varchar(10)
			 1 AS ContractSourceKey,
			 CASE 
				WHEN ISDATE(ContractStartDate) = 1 THEN CONVERT(DATETIME, ContractStartDate, 103)   
				ELSE GETDATE()
			END AS ContractCreationDT,  --ContractCreationDT
			'' AS PricingContractIndicator,  --PricingContractIndicator
			'B' AS PurchaseType,  --PurchaseType
			CASE
				WHEN c.ContractOwner = 'WUK' OR dr.RegionKey IS NOT NULL --Advanous Contract or WUK Region Contract
				THEN 'Contract Claims'
				WHEN dag1.AccountGroup1Key IS NOT NULL AND dag1.AG1Level1UDVarchar1 <> ''
				THEN dag1.AG1Level1UDVarchar1                          --General Branch Description
				WHEN dag1.AccountGroup1Key IS NOT NULL AND dag1.AG1Level1UDVarchar23 <> ''
				THEN dag1.AG1Level1UDVarchar23							--Branch Manager Name
				ELSE 'Contract Claims'
			END AS ContactName,  --ContactName
			CASE
				WHEN c.ContractOwner = 'WUK' OR dr.RegionKey IS NOT NULL --Advanous Contract or WUK Region Contract
				THEN 'contract.claims@wolseley.co.uk'
				WHEN dag1.AccountGroup1Key IS NOT NULL AND dag1.AG1Level1UDVarchar15 <> ''
				THEN dag1.AG1Level1UDVarchar15                          --General Branch Email Address
				WHEN dag1.AccountGroup1Key IS NOT NULL AND dag1.AG1Level1UDVarchar24 <> ''
				THEN dag1.AG1Level1UDVarchar24							--Branch Manager Email Address
				ELSE 'contract.claims@wolseley.co.uk'
			END AS ContactEmail,  --ContactEmail
			(SELECT StatusKey FROM cc.SupplierContractStatus WHERE StatusDescription='Approved') AS StatusKey
	FROM Con c
	LEFT JOIN dbo.DimAccountGroup1 dag1
		ON dag1.AG1Level1 = c.ContractOwner
	LEFT JOIN dbo.DimRegion dr
		ON dr.Region = c.ContractOwner
	LEFT JOIN dbo.DimVendor v 
		ON v.VendorNumber = c.ContractSupplier
	--LEFT JOIN cc.SupplierContract sc
	--	ON sc.ContractID = c.OriginatorBranchOrRegion + c.ContractNumber
	WHERE (c.OriginatorBranchOrRegion = c.Region
	OR (c.OriginatorBranchOrRegion <> c.Region	
		AND c.OriginatorBranchOrRegion <> 'BA'
		AND c.OriginatorBranchOrRegion <> 'CA'
		AND c.OriginatorBranchOrRegion <> 'D9'
		AND c.OriginatorBranchOrRegion <> 'DA'
		AND c.OriginatorBranchOrRegion <> 'EA'
		AND c.OriginatorBranchOrRegion <> 'GA'
		AND c.OriginatorBranchOrRegion <> 'HA'
		AND c.OriginatorBranchOrRegion <> 'MA'
		AND c.OriginatorBranchOrRegion <> 'TA'
		AND c.OriginatorBranchOrRegion <> 'WA'
		AND c.OriginatorBranchOrRegion <> 'YA'))
	AND (c.ConCount = 1 OR c.Region = c.ContractOwner)
	AND (dag1.AccountGroup1Key IS NULL OR dag1.Converted = 0)  --only load contracts for Branches that have not been Converted
	) AS Src
ON Tgt.ContractID = Src.OriginatorBranchOrRegion + Src.ContractNumber
WHEN MATCHED AND Tgt.ContractSourceKey = 1   --only update Wolcen contracts 
	AND
	CHECKSUM(
	Tgt.[WolcenExtFrom],
	Tgt.[WolcenExtTo],
	Tgt.[Region],
	Tgt.[Originator],
	Tgt.[ContractNumber],
	Tgt.[ContractReference],
	Tgt.[StartDate],
	Tgt.[ExpirationDate],
	Tgt.[JobSite],
	Tgt.[FixedPriceDate],
	Tgt.[PSFNumber],
	Tgt.[AuthorizedIndicator],
	Tgt.[PyramidCode],
	Tgt.[ContractAmount],
	Tgt.[NextClaimNumber],
	Tgt.[LastClaimPeriod],
	Tgt.[ContractModificationUser],
	Tgt.[ContractModificationDate],
	Tgt.[ContractType1],
	Tgt.[ContractType2],
	Tgt.[ContractType3]
	)
	<>
	CHECKSUM(
	Src.RecordFromDate,
	Src.RecordToDate,
	Src.Region,
	Src.OriginatorBranchOrRegion , -- Originator - varchar(3)
	Src.ContractNumber , -- ContractNumber - varchar(6)
	Src.ContractReference, -- ContractReference - varchar(50)
	Src.ContractStartDate, -- StartDate - date
	Src.ContractExpiryDate , -- ExpirationDate - date
	Src.SiteNameAddress  ,
	Src.FixedPriceDate,
	Src.PSFNumber,
	Src.AuthorizedIndicator,
	Src.PyramidCode,
	Src.ContractAmount,
	Src.NextClaimNumber , -- NextClaimNumber - varchar(10)
	Src.PeriodLastClaimed, -- LastClaimPeriod - varchar(10)
	Src.LastUserUpdate, -- ContractModificationUser - varchar(10)
	Src.LastDateUpdate ,
	Src.ContractType1,
	Src.ContractType2,
	Src.ContractType3
	)
	THEN UPDATE 
		SET
		Tgt.[WolcenExtFrom] = Src.RecordFromDate,
		Tgt.[WolcenExtTo] = Src.RecordToDate,
		Tgt.[Region] = Src.Region,
		Tgt.[Originator] = Src.OriginatorBranchOrRegion,
		Tgt.[ContractNumber] = Src.ContractNumber,
		Tgt.[ContractReference] = Src.ContractReference,
		Tgt.[StartDate] = Src.ContractStartDate,
		Tgt.[ExpirationDate] = Src.ContractExpiryDate,
		Tgt.[JobSite] = Src.SiteNameAddress,
		Tgt.[FixedPriceDate] = Src.FixedPriceDate,
		Tgt.[PSFNumber] = Src.PSFNumber,
		Tgt.[AuthorizedIndicator] = Src.AuthorizedIndicator,
		Tgt.[PyramidCode] = Src.PyramidCode,
		Tgt.[ContractAmount] = Src.ContractAmount,
		Tgt.[NextClaimNumber] = Src.NextClaimNumber,
		Tgt.[LastClaimPeriod] = Src.PeriodLastClaimed,
		Tgt.[ContractModificationUser] = Src.LastUserUpdate,
		Tgt.[ContractModificationDate] = Src.LastDateUpdate,
		Tgt.[ContractType1] = Src.ContractType1,
		Tgt.[ContractType2] = Src.ContractType2,
		Tgt.[ContractType3] = Src.ContractType3,
		Tgt.[JobFlag] = 1
	WHEN NOT MATCHED 
	THEN INSERT (
			  WolcenExtFrom ,
			  WolcenExtTo ,
			  LoadKey ,
			  ActionCode ,
			  RunDate ,
			  Region,
			  Originator ,
			  ContractNumber ,
			  ContractOwner ,
			  VendorKey ,
			  ContractReference ,
			  StartDate ,
			  ExpirationDate ,
			  JobSite ,
			  FixedPriceDate ,
			  PSFNumber ,
			  AuthorizedIndicator ,
			  PyramidCode ,
			  ContractAmount ,
			  RTB ,
			  NextClaimNumber ,
			  LastClaimPeriod ,
			  ContractModificationUser ,
			  ContractModificationDate ,
			  ContractRegionalCreateDate ,
			  ContractRegionalCreateTime ,
			  ContractRegionalCreateRoutine ,
			  ContractRegionalModificationDate ,
			  ContractRegionalModificationTime ,
			  HTD ,
			  ContractType1 ,
			  ContractType2 ,
			  ContractType3 ,
			  ContractLevel ,
			  JobFlag ,
			  FullContractNumber,
			  ContractSourceKey,
			  ContractCreationDT,
			  PricingContractIndicator,
			  PurchaseType,
			  ContactName,
			  ContactEmail,
			  StatusKey
			)
			
	VALUES  (
			Src.RecordFromDate, -- RecordFromDate - datetime
			Src.RecordToDate, -- RecordToDate - datetime
			Src.LoadKey,
			Src.ActionCode, 
			Src.RunDate, 
			Src.Region,
			Src.OriginatorBranchOrRegion , -- Originator - varchar(3)
			Src.ContractNumber , -- ContractNumber - varchar(6)
			Src.ContractOwner, -- ContractOwner - varchar(10)
			Src.VendorKey , -- VendorKey - int
			Src.ContractReference, -- ContractReference - varchar(50)
			Src.ContractStartDate, -- StartDate - date
			Src.ContractExpiryDate , -- ExpirationDate - date
			Src.SiteNameAddress  , -- JobSite - varchar(50)
			Src.FixedPriceDate, -- FixedPriceDate - date
			Src.PSFNumber , -- PSFNumber - varchar(10)
			Src.AuthorizedIndicator , -- AuthorizedIndicator - varchar(1)
			Src.PyramidCode, -- PyramidCode - varchar(2)
			Src.ContractAmount, -- ContractAmount - decimal
			Src.RTB, -- RTB - varchar(1)
			Src.NextClaimNumber , -- NextClaimNumber - varchar(10)
			Src.PeriodLastClaimed, -- LastClaimPeriod - varchar(10)
			Src.LastUserUpdate, -- ContractModificationUser - varchar(10)
			Src.LastDateUpdate , -- ContractModificationDate - date
			Src.ContractRegionalCreateDate, -- ContractRegionalCreateDate - date
			Src.ContractRegionalCreateTime, -- ContractRegionalCreateTime - varchar(10)
			Src.ContractRegionalCreateRoutine, -- ContractRegionalCreateRoutine - varchar(20)
			Src.ContractRegionalModificationDate, -- ContractRegionalModificationDate - date
			Src.ContractRegionalModificationTime, -- ContractRegionalModificationTime - varchar(10)
			Src.HTD, -- HTD - varchar(20)
			Src.ContractType1, -- ContractType1 - varchar(1)
			Src.ContractType2 , -- ContractType2 - varchar(1)
			Src.ContractType3, -- ContractType3 - varchar(1)
			Src.ContractLevel, -- ContractLevel - smallint
			Src.JobFlag, -- JobFlag - bit
			Src.FullContractNumber,   -- FullContractNumber - varchar(10)
			Src.ContractSourceKey,
			Src.ContractCreationDT,  --ContractCreationDT
			Src.PricingContractIndicator,  --PricingContractIndicator
			Src.PurchaseType,  --PurchaseType
			Src.ContactName,  --ContactName
			Src.ContactEmail,  --ContactEmail
			Src.StatusKey
	)
	OUTPUT 
			INSERTED.ContractKey
			INTO @NewContractKey
	;
	
	
	
SELECT 
	@RowsU = @RowsU + (@@RowCOunt - (COUNT(*) - @BeginRowCount)),
	@RowsI =  COUNT(*) - @BeginRowCount
FROM cc.SupplierContract


	-- set LTM benefit for new contracts
	SELECT UDVarchar3 ContractID, SUM(UDDecimal2) CCProfit 
	INTO #t1
	FROM dbo.FactInvoiceLine 
	WHERE Last12MonthsIndicator='Y'
	GROUP BY UDVarchar3

	UPDATE sc SET CCProfit = t1.CCProfit
	FROM cc.SupplierContract sc
	JOIN #t1 t1 ON t1.ContractID = sc.Originator + sc.ContractNumber
	

	-- apply jobflag rules
	UPDATE sc SET JobFlag = 0
	FROM cc.SupplierContract sc
	WHERE (JobSite LIKE 'ALL%' 
		OR JobSite LIKE 'VARIOUS%' 
		OR JobSite  LIKE '%national agreement%'
		OR JobSite = '')
	AND sc.ContractSourceKey = 1 -- only change Wolcen contracts and ignore contracts touched by Contract Manager (2s and 3s)


	-- apply manually set jobs
	UPDATE sc SET JobFlag = o.[Adjusted Job Flag]
	FROM cc.SupplierContract sc
	JOIN Lion_Staging.dbo.ContractJobOverride o
		ON sc.Originator + sc.ContractNumber = o.[Contract]
	WHERE sc.ContractSourceKey = 1 -- only change Wolcen contracts and ignore contracts touched by Contract Manager (2s and 3s)

	
INSERT INTO cc.SupplierContractLog (
	WebUserKey, 
	ContractKey, 
	ContractID,
	vendorKey,
	ContractAction,
	Notes,
	CreationDate )
SELECT
	CreatedByWebUserKey,
	sc.ContractKey,
	sc.ContractID,
	sc.VendorKey,
	'Import WUK Contract',
	'From:<NULL> To:<Draft>',
	GETDATE()
FROM cc.SupplierContract sc
INNER JOIN @NewContractKey n
	ON n.ContractKey = sc.ContractKey
LEFT JOIN  @BeginContractKey b
	ON b.ContractKey = n.ContractKey
WHERE b.ContractKey IS NULL


	EXEC LogDCPEvent 'cc.Load_SupplierContract', 'E', @RowsI, @RowsU, 0

END
GO
