SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE [ppe].[Get_CurrentTerm] (@WebUserKey INT, @AccountKey INT, @ItemKey INT, @ItemGroup3Key INT) AS 
BEGIN
	--exec [ppe].[Get_CurrentTerm]  1,7,223958,3259
	SELECT 
		ct.AccountId,
		ct.SPGCode,
		NULL AS ProductNumber,
		ct.Usualdiscount1 AS SPGDiscount1,
		ct.Usualdiscount2 AS SPGDiscount2,
		NULL AS ExceptionProduct,
		NULL AS ExceptionDiscount,
		NULL AS ExceptionFixedPrice,
		NULL AS ExceptionFromDate,
		NULL AS ExceptionToDate,
		NULL AS TermType,
		0.00 AS TradePrice,
		1.0 - (1.0 - ISNULL(Usualdiscount1,0)/100.0) * (1.0 - ISNULL(Usualdiscount2,0)/100.0) AS TotalDiscount,
		0.00 AS TotalPrice
	FROM lion.CustomerTerms ct
	WHERE ct.AccountKey=@AccountKey
	AND ItemGroup3Key = @ItemGroup3Key
	AND ItemKey = 1

	UNION

	SELECT 
		ct.AccountId,
		ct.SPGCode,
		di.ItemNumber AS ProductNumber,
		ct.Usualdiscount1 AS SPGDiscount1,
		ct.Usualdiscount2 AS SPGDiscount2,
		ct.ExceptionProduct,
		ct.ExceptionDiscount,
		ct.ExceptionFixedPrice,
		ct.ExceptionFromDate,
		ct.ExceptionToDate,
		CASE WHEN ct.ExceptionDiscount IS NULL THEN 'FP' ELSE 'FD' END AS TermType,
		CASE WHEN ct.ExceptionDiscount IS NULL THEN 0.00 ELSE di.ItemUDDecimal1 END AS TradePrice,
		CASE 
			WHEN (ct.ExceptionDiscount/100.0) IS NULL THEN (di.ItemUDDecimal1-ct.ExceptionFixedPrice) / ItemUDDecimal1 
			ELSE (ct.ExceptionDiscount/100.0)
		END AS  TotalDiscount,
		CASE 
			WHEN ct.ExceptionDiscount IS NULL THEN ct.ExceptionFixedPrice
			ELSE (1.00 - (ct.ExceptionDiscount/100.0)) * di.ItemUDDecimal1
		END AS TotalPrice
	FROM lion.CustomerTerms ct
	JOIN dbo.DimItem di ON di.ItemKey = ct.ItemKey
	WHERE ct.AccountKey=@AccountKey
	AND ct.ItemGroup3Key = di.ItemGroup3Key
	AND ct.ItemKey = @ItemKey

END


GO
