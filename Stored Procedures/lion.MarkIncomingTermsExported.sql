SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



-- =============================================
-- Author:		José A. Castaños
-- Create date: 17-Feb-2015
-- Description:	Mark incoming terms records as exported to Wolcen file
-- =============================================
CREATE PROCEDURE [lion].[MarkIncomingTermsExported]
   @RequestID nvarchar(255)
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @sql nvarchar(max)
	DECLARE @now datetime

	set @now = getdate()

	UPDATE it 
   	   SET it.[Exported] = 1,
		   it.[ExportedDateTime] = @now
	  FROM lion.IncomingTerms it
	 WHERE it.WolcenRequestID = @RequestID

	UPDATE it 
   	   SET it.[Exported] = 1,
		   it.[ExportedDateTime] = @now,
		   it.[WolcenRequestID] = @RequestID
	  FROM lion.IncomingTerms it inner join 
	       lion.IncomingTerms req on it.AccountKey = req.AccountKey and
		                             it.LLSPG = req.LLSPG and
									 ISNULL(it.WolcenRequestID, '') <> @RequestID and
									 ISNULL(req.WolcenRequestID, '') = @RequestID
	 WHERE req.WolcenRequestID = @RequestID

	

END



GO
