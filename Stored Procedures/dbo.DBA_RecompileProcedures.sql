SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[DBA_RecompileProcedures]

AS

DECLARE @SQL NVARCHAR(MAX)

DECLARE sprocs CURSOR FOR 
	--SELECT 'exec sp_Recompile '''+u.name +'.'+o.name + '''' FROM sysobjects o JOIN sysusers u ON o.uid = u.uid

	SELECT DISTINCT 'EXEC sp_Recompile ''' + SCHEMA_NAME(o.schema_id) + '.' + o.name + '''' 
	FROM sys.objects o
	WHERE o.type = 'P'
	ORDER BY 1

OPEN sprocs

FETCH NEXT FROM sprocs INTO @SQL

WHILE @@FETCH_STATUS = 0
BEGIN

	PRINT @SQL
	EXEC sp_executesql @SQL
	FETCH NEXT FROM sprocs INTO @SQL

END

CLOSE sprocs
DEALLOCATE sprocs

GO
