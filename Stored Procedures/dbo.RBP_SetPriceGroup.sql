SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- Stored procedure
--RBP_SetPriceGroup 152, 1, 6

--Select * From PlaybookPricingGroup
--Select * From PlaybookDataPointPricingGroup_View
--Select * From PlaybookPricingGroupQuantityBand_View 
CREATE
PROCEDURE [dbo].[RBP_SetPriceGroup] (@PlaybookDataPointGroupKey INT, @PricingRulePlaybookKey INT, @ScenarioKey INT)

AS

SET QUOTED_IDENTIFIER ON 
SET ANSI_NULLS ON 
SET ARITHABORT ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
SET CONCAT_NULL_YIELDS_NULL ON
SET NUMERIC_ROUNDABORT OFF
SET NOCOUNT ON

DECLARE @IndexName NVARCHAR(1000)
DECLARE @RowsI Quantity_Normal_type, @RowsU Quantity_Normal_type, @RowsD Quantity_Normal_type, @PricingRuleWhereClause NVARCHAR(2000), @PricingRuleJoinClause NVARCHAR(2000), @PricingRuleAttributeSourceTable NVARCHAR(255), @tmpValuesAssigned NVARCHAR(MAX)
DECLARE @DataPointReUseJoinClause NVARCHAR(MAX), @DataPointReUseWhereClause NVARCHAR(MAX), @DataPointReUseIndicator CHAR(1)

SET @RowsI = 0
SET @RowsU = 0
SET @RowsD = 0

DECLARE @ProjectKey Key_Normal_type

SELECT @ProjectKey = ProjectKey FROM PlaybookDataPointGroup WHERE PlaybookDataPointGroupKey = @PlaybookDataPointGroupKey

EXEC LogDCPEvent 'DCP --- RBP_SetPriceGroup: Delete', 'B',0,0,0,@ProjectKey

--First, delete all data that already exists for this data point group.  This includes EVERYTHING that falls under Price groups, including proposals
DELETE FROM PlaybookPricingGroupPriceBand WHERE PlaybookPricingGroupKey IN (SELECT PlaybookPricingGroupKey FROM PlaybookPricingGroupQuantityBand_View WHERE PlaybookDataPointGroupKey = @PlaybookDataPointGroupKey)
SET @RowsD = @RowsD + @@RowCount
DELETE FROM ProposalPrice WHERE PlaybookDataPointGroupKey = @PlaybookDataPointGroupKey
SET @RowsD = @RowsD + @@RowCount
DELETE FROM PlaybookPriceProposal WHERE PlaybookDataPointGroupKey = @PlaybookDataPointGroupKey
SET @RowsD = @RowsD + @@RowCount
DELETE FROM PlaybookDataPointOptimalPrice WHERE PlaybookDataPointOptimalPriceKey IN (SELECT PlaybookDataPointOptimalPriceKey FROM PlaybookDatapointOptimalPrice_View WHERE PlaybookDataPointGroupKey = @PlaybookDataPointGroupKey)
SET @RowsD = @RowsD + @@RowCount
DELETE FROM PlaybookPricingGroupOptimalPrice WHERE PlaybookPricingGroupOptimalPriceKey IN (SELECT PlaybookPricingGroupOptimalPriceKey FROM PlaybookPricingGroupOptimalPrice_View WHERE PlaybookDataPointGroupKey = @PlaybookDataPointGroupKey)
SET @RowsD = @RowsD + @@RowCount
DELETE FROM PlaybookPricingGroupQuantityBand WHERE PlaybookPricingGroupQuantityBandKey IN (SELECT PlaybookPricingGroupQuantityBandKey FROM PlaybookPricingGroupQuantityBand_View WHERE PlaybookDataPointGroupKey = @PlaybookDataPointGroupKey)
SET @RowsD = @RowsD + @@RowCount
DELETE FROM PlaybookDataPointPricingGroup WHERE PlaybookDataPointPricingGroupKey IN (SELECT PlaybookDataPointPricingGroupKey FROM PlaybookDatapointPricingGroup_View WHERE PlaybookDataPointGroupKey = @PlaybookDataPointGroupKey)
SET @RowsD = @RowsD + @@RowCount
DELETE FROM PlaybookPricingGroup WHERE PlaybookDataPointGroupKey = @PlaybookDataPointGroupKey
SET @RowsD = @RowsD + @@RowCount

EXEC LogDCPEvent 'DCP --- RBP_SetPriceGroup: Delete', 'E', @RowsI, @RowsU, @RowsD

SET @RowsI = 0
SET @RowsU = 0
SET @RowsD = 0

--EXEC DBA_IndexRebuild 'PlaybookDataPoint', 'Drop'

--DELETE FROM DBA_Index_Rebuild WHERE IndexName LIKE 'i_atkPlaybookDataPoint._%'

DECLARE
	@PricingRuleKey INT,
--	@GroupNumber INT,
	@AttributeType Description_Small_type,
	@FieldType Description_Small_type,
	@First INT,
	@TempFieldList NVARCHAR(4000),
	@TempFieldVarList NVARCHAR(4000),
	@TempFieldVarListWithTypes NVARCHAR(4000),
	@TempFieldListVarAssignment NVARCHAR(4000),
	@TempJoinList NVARCHAR(4000),
	@PricingRuleAttributeJoinClause1 NVARCHAR(4000), 
	@PricingRuleAttributeJoinClause2 NVARCHAR(4000),

	@MinimumPoints INT,
	@MinimumSales DEC(38,8),
	@MinimumPointsType NVARCHAR(500),
	@RulingPointThreshold Int_Type,
--	@PricingRuleKey INT,
	@SQLStmt NVARCHAR(MAX),
	@SQLStmt2 NVARCHAR(MAX),
	@FieldList NVARCHAR(MAX),
	@IndexStmt NVARCHAR(MAX),
	@DropIndexStmt NVARCHAR(MAX)




--SELECT @PricingRuleKey = 0
--SELECT @GroupNumber = 0

-- This procedure requires that FieldName1 below is not NULL unless every FieldName* is NULL
-- Ergo, FieldName2 can be null even if FieldName3 is not null as long as FieldName1 is not null
-- Similarly FieldName1 must not be null if any of the other FieldNames are not null

--DEH Here....


DECLARE CurPriceGroup CURSOR FOR
SELECT PricingRuleKey, MinimumPoints, MinimumSales, MinimumPointsType, ISNULL(PricingRuleWhereClause, ''), ISNULL(PricingRuleJoinClause, ''), RulingPointThreshold, DataPointReUseIndicator
FROM ATKPricingRule
WHERE PricingRulePlaybookKey = @PricingRulePlaybookKey
ORDER BY PricingRuleSequenceNumber

OPEN CurPriceGroup
FETCH NEXT FROM CurPriceGroup INTO
	@PricingRuleKey, @MinimumPoints, @MinimumSales, @MinimumPointsType, @PricingRuleWhereClause, @PricingRuleJoinClause, @RulingPointThreshold, @DataPointReUseIndicator

WHILE (@@fetch_status <> -1)
BEGIN

--	print 'Pricing rule: '+cast(@PricingRuleKey as nvarchar(50))

	--Load the fields...............................................................
	SET @TempJoinList = @PricingRuleJoinClause + '
	'
	SET @TempFieldList = ''
	SET @tmpValuesAssigned = ''
	SET @TempFieldVarList = ''

	SET @TempFieldVarListWithTypes = ''
	SET @TempFieldListVarAssignment = ''

	DECLARE CurRuleField CURSOR FOR
	SELECT aprat.PricingRuleAttributeType, PricingRuleAttributeSourceTable, aprat.PricingRuleAttributeJoinClause1, aprat.PricingRuleAttributeJoinClause2
	FROM ATKPricingRuleAttribute apra
		JOIN ATKPricingRuleAttributeType  aprat ON  apra.PricingRuleAttributeTypeKey = aprat.PricingRuleAttributeTypeKey
	WHERE apra.PricingRuleKey = @PricingRuleKey
	ORDER BY apra.PricingRuleAttributeSequenceNumber ASC

	OPEN CurRuleField
	FETCH NEXT FROM CurRuleField INTO @AttributeType, @PricingRuleAttributeSourceTable, @PricingRuleAttributeJoinClause1, @PricingRuleAttributeJoinClause2


	SET @First = 1
	WHILE(@@fetch_status <> -1)
	BEGIN

		--SELECT TOP 1 @FieldType = DATA_TYPE + 
		--CASE DATA_TYPE
		--	WHEN 'varchar' THEN '(' + CAST(CHARACTER_MAXIMUM_LENGTH AS NVARCHAR(50)) + ')'
		--	WHEN 'nvarchar' THEN '(' + CAST(CHARACTER_MAXIMUM_LENGTH AS NVARCHAR(50)) + ')'
		--	WHEN 'numeric' THEN '(' + CAST(NUMERIC_PRECISION AS NVARCHAR(50)) + ',' + CAST(NUMERIC_SCALE AS NVARCHAR(50)) + ')'
		--	ELSE ''
		--END
		--FROM INFORMATION_SCHEMA.COLUMNS WHERE COLUMN_NAME = @AttributeType

		; WITH dt AS (
			SELECT DISTINCT
				DATA_TYPE +
				CASE DATA_TYPE
					WHEN 'varchar' THEN '(' + CAST(CHARACTER_MAXIMUM_LENGTH AS NVARCHAR(50)) + ')'
					WHEN 'nvarchar' THEN '(' + CAST(CHARACTER_MAXIMUM_LENGTH AS NVARCHAR(50)) + ')'
					WHEN 'numeric' THEN '(' + CAST(NUMERIC_PRECISION AS NVARCHAR(50)) + ',' + CAST(NUMERIC_SCALE AS NVARCHAR(50)) + ')'
					WHEN 'decimal' THEN '(' + CAST(NUMERIC_PRECISION AS NVARCHAR(50)) + ',' + CAST(NUMERIC_SCALE AS NVARCHAR(50)) + ')'
					ELSE ''
				END AS DataType,
				ROW_NUMBER() OVER (
					PARTITION BY 
						COLUMN_NAME
					ORDER BY 
						CASE
							WHEN TABLE_NAME = @PricingRuleAttributeSourceTable THEN 1
							ELSE 2
						END,  -- matching table then use it first
						LEN(
							DATA_TYPE +
							CASE DATA_TYPE
								WHEN 'varchar' THEN '(' + CAST(CHARACTER_MAXIMUM_LENGTH AS NVARCHAR(50)) + ')'
								WHEN 'nvarchar' THEN '(' + CAST(CHARACTER_MAXIMUM_LENGTH AS NVARCHAR(50)) + ')'
								WHEN 'numeric' THEN '(' + CAST(NUMERIC_PRECISION AS NVARCHAR(50)) + ',' + CAST(NUMERIC_SCALE AS NVARCHAR(50)) + ')'
								WHEN 'decimal' THEN '(' + CAST(NUMERIC_PRECISION AS NVARCHAR(50)) + ',' + CAST(NUMERIC_SCALE AS NVARCHAR(50)) + ')'
								ELSE ''
							END
						) DESC 
				) AS RowRank
			FROM INFORMATION_SCHEMA.COLUMNS
			WHERE COLUMN_NAME = @AttributeType
		)
		SELECT 
			TOP 1 
			@FieldType = DataType
		FROM dt
		WHERE RowRank = 1

		IF @First = 1
		BEGIN
			SET @TempFieldList = @PricingRuleAttributeSourceTable + '.' + @AttributeType
			SET @TempFieldVarList = '@'+@AttributeType
			SET @tmpValuesAssigned = 'Cast(@'+@AttributeType+' as nvarchar(500))'
			SET @TempFieldVarListWithTypes = '@'+@AttributeType+' '+@FieldType
			SET @TempFieldListVarAssignment = @PricingRuleAttributeSourceTable + '.' + @AttributeType+' = @'+@AttributeType
			SET @First = 0
		END
		ELSE
		BEGIN
			SET @TempFieldList = @TempFieldList+','+@PricingRuleAttributeSourceTable + '.' + @AttributeType
			SET @TempFieldVarList = @TempFieldVarList+',@'+@AttributeType
			SET @tmpValuesAssigned = @tmpValuesAssigned+'+ '',''+ Cast(@'+@AttributeType+' as nvarchar(500))'
			SET @TempFieldVarListWithTypes = @TempFieldVarListWithTypes+',@'+@AttributeType+' '+@FieldType
			SET @TempFieldListVarAssignment = @TempFieldListVarAssignment + ' AND '+ @PricingRuleAttributeSourceTable + '.' + @AttributeType+' = @'+@AttributeType
		END


		PRINT '@AttributeType:' + ISNULL(@AttributeType, '')
		PRINT '@FieldType:' + ISNULL(@FieldType, '')

		IF (@AttributeType IN ('AccountGroup1Key', 'AccountGroup2Key', 'AccountGroup3Key', 'AccountUDVarchar1','AccountUDVarchar3')
			OR @AttributeType LIKE 'Account%')
			AND @TempJoinList NOT LIKE '%Join DimAccount on DimAccount.AccountKey = PlaybookDataPoint.AccountKey%'
		BEGIN
			SET @TempJoinList = @TempJoinList + 'Join DimAccount on DimAccount.AccountKey = PlaybookDataPoint.AccountKey ' 
		END
		IF (
				@AttributeType IN ('ItemGroup1Key', 'ItemGroup2Key', 'ItemGroup3Key')
				OR
				@PricingRuleWhereClause LIKE '%ItemQuantityBand%'
			)			
			AND @TempJoinList NOT LIKE '%Join PlaybookDataPointGroupItemBands on PlaybookDataPointGroupItemBands.ItemKey = PlaybookDataPoint.ItemKey and PlaybookDataPointGroupItemBands.PlaybookDataPointGroupKey%'
		BEGIN
			SET @TempJoinList = @TempJoinList + 'Join PlaybookDataPointGroupItemBands on PlaybookDataPointGroupItemBands.ItemKey = PlaybookDataPoint.ItemKey and PlaybookDataPointGroupItemBands.PlaybookDataPointGroupKey = '+CAST(@PlaybookDataPointGroupKey AS NVARCHAR(50))+' ' 
		END

		IF (@AttributeType IN ('VendorKey', 'ItemVendorKey', 'ItemUDVarchar2', 'LikeItemCode')
			OR @AttributeType LIKE 'Item%')
			AND @TempJoinList NOT LIKE '%Join DimItem on DimItem.ItemKey = PlaybookDataPoint.ItemKey%'
		BEGIN
			SET @TempJoinList = @TempJoinList + 'Join DimItem on DimItem.ItemKey = PlaybookDataPoint.ItemKey ' 
		END
		
		IF @AttributeType IN ('VendorNumber', 'VendorDescription', 'VendorClientUniqueIdentifier')
			OR @AttributeType LIKE 'VendorUDVarchar%'
		BEGIN

			IF @TempJoinList NOT LIKE '%Join DimItem on DimItem.ItemKey = PlaybookDataPoint.ItemKey%'
				SET @TempJoinList = @TempJoinList + 'Join DimItem on DimItem.ItemKey = PlaybookDataPoint.ItemKey ' 
			IF @TempJoinList NOT LIKE '%JOIN DimVendor on DimVendor.VendorKey = DimItem.ItemVendorKey%'
				SET @TempJoinList = @TempJoinList + 'JOIN DimVendor on DimVendor.VendorKey = DimItem.ItemVendorKey ' 
		END

		IF @AttributeType IN ('InvoiceLineGroup1UDVarchar1', 'InvoiceLineGroup1UDVarchar2', 'InvoiceLineGroup1UDVarchar3', 'InvoiceLineGroup1UDVarchar4', 'InvoiceLineGroup1UDVarchar5')
		BEGIN

			IF @TempJoinList NOT LIKE '%Join DimInvoiceLineGroup1 on DimInvoiceLineGroup1.InvoiceLineGroup1Key = PlaybookDataPoint.InvoiceLineGroup1Key%'
				SET @TempJoinList = @TempJoinList + 'Join DimInvoiceLineGroup1 on DimInvoiceLineGroup1.InvoiceLineGroup1Key = PlaybookDataPoint.InvoiceLineGroup1Key ' 
		END
		
		IF @AttributeType IN ('AG1Level1UDVarchar2', 'AG1Level1UDVarchar3')
			OR @AttributeType LIKE 'AG1%'
		BEGIN

			IF @TempJoinList NOT LIKE '%Join DimAccount on DimAccount.AccountKey = PlaybookDataPoint.AccountKey%'
				SET @TempJoinList = @TempJoinList + 'Join DimAccount on DimAccount.AccountKey = PlaybookDataPoint.AccountKey ' 
			IF @TempJoinList NOT LIKE '%Join DimAccountGroup1 on DimAccountGroup1.AccountGroup1Key = DimAccount.AccountGroup1Key%'
				SET @TempJoinList = @TempJoinList + 'Join DimAccountGroup1 on DimAccountGroup1.AccountGroup1Key = DimAccount.AccountGroup1Key ' 
		END

		IF @AttributeType IN ('IG1Level1', 'IG1Level2', 'IG1Level3', 'IG1Level4', 'IG1Level5')
			OR @AttributeType LIKE 'IG1%'
		BEGIN

			IF @TempJoinList NOT LIKE '%Join DimItem on DimItem.ItemKey = PlaybookDataPoint.ItemKey%'
				SET @TempJoinList = @TempJoinList + 'Join DimItem on DimItem.ItemKey = PlaybookDataPoint.ItemKey ' 
			IF @TempJoinList NOT LIKE '%Join DimItemGroup1 on DimItemGroup1.ItemGroup1Key = DimItem.ItemGroup1Key%'
				SET @TempJoinList = @TempJoinList + 'Join DimItemGroup1 on DimItemGroup1.ItemGroup1Key = DimItem.ItemGroup1Key ' 
		END

		IF @AttributeType IN ('IG2Level1', 'IG2Level2', 'IG2Level3', 'IG2Level4', 'IG2Level5')
			OR @AttributeType LIKE 'IG2%'
		BEGIN

			IF @TempJoinList NOT LIKE '%Join DimItem on DimItem.ItemKey = PlaybookDataPoint.ItemKey%'
				SET @TempJoinList = @TempJoinList + 'Join DimItem on DimItem.ItemKey = PlaybookDataPoint.ItemKey ' 
			IF @TempJoinList NOT LIKE '%Join DimItemGroup2 on DimItemGroup2.ItemGroup2Key = DimItem.ItemGroup2Key%'
				SET @TempJoinList = @TempJoinList + 'Join DimItemGroup2 on DimItemGroup2.ItemGroup2Key = DimItem.ItemGroup2Key ' 
		END


		IF @AttributeType IN ('IG3Level1', 'IG3Level2', 'IG3Level3', 'IG3Level4', 'IG3Level5')
			OR @AttributeType LIKE 'IG3%'
		BEGIN

			IF @TempJoinList NOT LIKE '%Join DimItem on DimItem.ItemKey = PlaybookDataPoint.ItemKey%'
				SET @TempJoinList = @TempJoinList + 'Join DimItem on DimItem.ItemKey = PlaybookDataPoint.ItemKey ' 
			IF @TempJoinList NOT LIKE '%Join DimItemGroup3 on DimItemGroup3.ItemGroup3Key = DimItem.ItemGroup3Key%'
				SET @TempJoinList = @TempJoinList + 'Join DimItemGroup3 on DimItemGroup3.ItemGroup3Key = DimItem.ItemGroup3Key ' 
		END


		IF @AttributeType IN ('AG1Level1', 'AG1Level2', 'AG1Level3','AG1Level4','AG1Level5')
		BEGIN

			IF @TempJoinList NOT LIKE '%Join DimAccount on DimAccount.AccountKey = PlaybookDataPoint.AccountKey%'
				SET @TempJoinList = @TempJoinList + 'Join DimAccount on DimAccount.AccountKey = PlaybookDataPoint.AccountKey ' 
			IF @TempJoinList NOT LIKE '%Join DimAccountGroup1 on DimAccountGroup1.AccountGroup1Key = DimAccount.AccountGroup1Key%'
				SET @TempJoinList = @TempJoinList + 'Join DimAccountGroup1 on DimAccountGroup1.AccountGroup1Key = DimAccount.AccountGroup1Key ' 
		END

		IF @AttributeType IN ('AG2Level1', 'AG2Level2', 'AG2Level3', 'AG2Level4', 'AG2Level5')	
		BEGIN

			IF @TempJoinList NOT LIKE '%Join DimAccount on DimAccount.AccountKey = PlaybookDataPoint.AccountKey%'
				SET @TempJoinList = @TempJoinList + 'Join DimAccount on DimAccount.AccountKey = PlaybookDataPoint.AccountKey ' 
			IF @TempJoinList NOT LIKE '%Join DimAccountGroup2 on DimAccountGroup2.AccountGroup2Key = DimAccount.AccountGroup2Key%'
				SET @TempJoinList = @TempJoinList + 'Join DimAccountGroup2 on DimAccountGroup2.AccountGroup2Key = DimAccount.AccountGroup2Key ' 
		END

		IF @AttributeType IN ('AG3Level1', 'AG3Level2', 'AG3Level3', 'AG3Level4', 'AG3Level5')
		BEGIN

			IF @TempJoinList NOT LIKE '%Join DimAccount on DimAccount.AccountKey = PlaybookDataPoint.AccountKey%'
				SET @TempJoinList = @TempJoinList + 'Join DimAccount on DimAccount.AccountKey = PlaybookDataPoint.AccountKey ' 
			IF @TempJoinList NOT LIKE '%Join DimAccountGroup3 on DimAccountGroup3.AccountGroup3Key = DimAccount.AccountGroup3Key%'
				SET @TempJoinList = @TempJoinList + 'Join DimAccountGroup3 on DimAccountGroup3.AccountGroup3Key = DimAccount.AccountGroup3Key ' 
		END


		--Set the first join, if it's needed
		IF @TempJoinList NOT LIKE '%'+@PricingRuleAttributeJoinClause1+'%'
		BEGIN
			SET @TempJoinList = @TempJoinList + ' 
			' +	@PricingRuleAttributeJoinClause1 + ' ' 
		END

		--Set the second join, if it's needed
		IF @TempJoinList NOT LIKE '%'+@PricingRuleAttributeJoinClause2+'%'
		BEGIN
			SET @TempJoinList = @TempJoinList + '
			'+@PricingRuleAttributeJoinClause2 + ' ' 
		END


--Select * From ATKPRicingRuleAttributeType

		--print '      '+cast(@PricingRuleKey as nvarchar(10))+' '+@AttributeType+' '+@FieldType+' @'+@AttributeType

		FETCH NEXT FROM CurRuleField INTO @AttributeType, @PricingRuleAttributeSourceTable, @PricingRuleAttributeJoinClause1, @PricingRuleAttributeJoinClause2
	END
-- 
-- 	--For the where clause, we need to join to the banding table
-- 	If @PricingRuleWhereClause is not null
-- 		and @PricingRuleWhereClause like '%ItemQuantityBand%'
-- 	Begin
-- 		Set @TempJoinList = @TempJoinList + 'Join FactItemBand on FactItemBand.ItemKey = PlaybookDataPoint.ItemKey ' 
-- 	End

--	print '************ '
--	print 'FieldList: '+@TempFieldList
--	print 'FieldVarList: '+@TempFieldVarList
--	print 'FieldVarListWithTypes: '+@TempFieldVarListWithTypes
	CLOSE CurRuleField
	DEALLOCATE CurRuleField


--	SET @PricingRuleKey = @PricingRuleKey + 1

	DECLARE @IndexList NVARCHAR(4000)
	SET @IndexList = REPLACE(@TempFieldList, 'PlaybookDataPoint.', '')
	SET @IndexList = REPLACE(@IndexList, 'AccountGroup1Key', 'AccountKey')
	SET @IndexList = REPLACE(@IndexList, 'AccountGroup2Key', 'AccountKey')
	SET @IndexList = REPLACE(@IndexList, 'AccountGroup3Key', 'AccountKey')
	SET @IndexList = REPLACE(@IndexList, 'ItemGroup1Key', 'ItemKey')
	SET @IndexList = REPLACE(@IndexList, 'ItemGroup2Key', 'ItemKey')

	SET @IndexList = REPLACE(@IndexList, 'ItemGroup3Key', 'ItemKey')
	SET @IndexList = REPLACE(@IndexList, 'VendorKey', 'ItemKey')

	IF @PricingRuleWhereClause IS NOT NULL AND RTRIM(@PricingRuleWhereClause) <> ''
		SET @PricingRuleWhereClause = 'and '+@PricingRuleWhereClause
--	Else Set @PricingRuleWhereClause = ''

	--Don't build an index here, there should already be enough indexes.

-- 	Set @IndexName = 'i_atkPlaybookDataPoint._' + CAST(@PricingRuleKey AS nvarchar(50)) + '_' + CAST(@PlaybookDataPointGroupKey AS nvarchar(50))
-- 	SET @IndexStmt = 
-- 	'
-- 
-- 	ALTER    INDEX '+@IndexName +
-- 	' on PlaybookDataPoint (PlaybookDataPointGroupKey, ' + @IndexList + ')'
-- 
-- 	Set @DropIndexStmt = 'Drop index PlaybookDataPoint.'+@IndexName

-- 	print 'Index Stmt: '+@IndexStmt
-- 	print @DropIndexStmt
	--------------------------------------------------------
	---------------Handle Data Point Re-Use-----------------
	--------------------------------------------------------
	PRINT '@DataPointReUseIndicator: '+ISNULL(@DataPointReUseIndicator, '')

	IF @DataPointReUseIndicator = 'Y'	--If we'd like to re-use data points
	BEGIN
		SET @DataPointReUseJoinClause = ''
		SET @DataPointReUseWhereClause = ''
	END
	ELSE		--Else, do not re-use data points
	BEGIN
		SET @DataPointReUseJoinClause = 'LEFT JOIN PlaybookDataPointPricingGroup PDPPG on PDPPG.PlaybookDataPointKey = PlaybookDataPoint.PlaybookDataPointKey'
		SET @DataPointReUseWhereClause = 'PDPPG.PlaybookPricingGroupKey IS NULL and'
	END

	PRINT '@DataPointReUseJoinClause: '+@DataPointReUseJoinClause
	PRINT '@DataPointReUseWhereClause: '+@DataPointReUseWhereClause

	PRINT '@TempJoinList: '+@TempJoinList
	
	--------------------------------------------------------
	--------------------------------------------------------
	--------------------------------------------------------
/*
			AND (@MaxDayKey - PlaybookDataPoint.LastSaleDayKey) <= '+CAST(@RulingPointThreshold AS NVARCHAR(500))+'
			' + '
*/

	SET @SQLStmt = '

    Declare @PlaybookPricingGroupKey int
    Declare @MaxDayKey int

 	Declare @RowsI Quantity_Normal_type, @RowsU Quantity_Normal_type, @RowsD Quantity_Normal_type, @PricingRuleWhereClause nvarchar(2000), @PricingRuleAttributeSourceTable nvarchar(255), @tmpValuesAssigned nvarchar(2000)
 	set @RowsI = 0
 	set @RowsU = 0
 	set @RowsD = 0

    Select @MaxDayKey = max(InvoiceDateDayKey) from FactInvoiceLine 

	exec LogDCPEvent ''DCP --- RBP_SetPriceGroup: Insert PDPPG and PPG for PricingRuleKey '+CAST(@PricingRuleKey AS NVARCHAR(100))+' '', ''B'',0,0,0,'+CAST(@ProjectKey AS NVARCHAR(50))+' 

	DECLARE	@AffectedPopulation INT, @RulingPopulation INT' +ISNULL(','+@TempFieldVarListWithTypes,'') + '

'+	'SELECT	' + @TempFieldList +',

		@MaxDayKey - PlaybookDataPoint.LastSaleDayKey as ''DaysSinceSale'',
		TotalActual12MonthPrice
	Into #TempDaysSince
	FROM	PlaybookDataPoint
		' +@DataPointReUseJoinClause +'
		' + @TempJoinList + '		
	WHERE	'+@DataPointReUseWhereClause+'
			PlaybookDataPoint.PlaybookDataPointGroupKey = ' + CAST(@PlaybookDataPointGroupKey AS NVARCHAR(100)) + '
			' + @PricingRuleWhereClause + ' ' + '

	DECLARE a CURSOR FOR
		select ' + REPLACE(@TempFieldVarList, '@', '') +'
		from #TempDaysSince
		Where DaysSinceSale <= '+CAST(@RulingPointThreshold AS NVARCHAR(500))+'
		group by ' + REPLACE(@TempFieldVarList, '@', '') +'
		having count(*) >= '+CAST(@MinimumPoints AS NVARCHAR(500))+'
		and sum(TotalActual12MonthPrice) >= '+CAST(@MinimumSales AS NVARCHAR(500))+'

	OPEN a

	FETCH NEXT FROM a INTO ' + @TempFieldVarList + '
	WHILE @@fetch_status = 0
	BEGIN

		Insert PlaybookPricingGroup (PlaybookDataPointGroupKey, GroupingColumns, GroupValues, PricingRuleKey) 
		Values ('+ CAST(@PlaybookDataPointGroupKey AS NVARCHAR(100)) +', '''+CAST(@TempFieldList AS NVARCHAR(1000))+''','+CAST(@tmpValuesAssigned AS NVARCHAR(1000))+', '+CAST(@PricingRuleKey AS NVARCHAR(500))+')
		Set @RowsI = @RowsI + @@RowCount

		Set @PlaybookPricingGroupKey = @@Identity

		Insert into PlaybookDataPointPricingGroup (PlaybookDataPointKey,PlaybookPricingGroupKey,PricingRuleKey,RulingMemberFlag,Total12MonthQuantity,LastItemPrice, LastItemCost, LastItemDiscountPercent)
		Select
			PlaybookDataPoint.PlaybookDataPointKey,
			@PlaybookPricingGroupKey,
			'+CAST(@PricingRuleKey AS NVARCHAR(500))+',
			CASE
				WHEN (( @MaxDayKey - PlaybookDataPoint.LastSaleDayKey ) <= '+CAST(@RulingPointThreshold AS NVARCHAR(500))+') THEN ''Y''
			    	ELSE ''N''
			END as PricingGroupRulingMemberFlag,
			PlaybookDataPoint.Total12MonthQuantity,
			PlaybookDataPoint.LastItemPrice,
			PlaybookDataPoint.LastItemCost,
			PlaybookDataPoint.LastItemDiscountPercent

		From PlaybookDataPoint 
		'+@DataPointReUseJoinClause+'
				' + @TempJoinList + ' 
		WHERE   ' +@DataPointReUseWhereClause+'
				' + @TempFieldListVarAssignment + '
				and PlaybookDataPoint.PlaybookDataPointGroupKey = ' + CAST(@PlaybookDataPointGroupKey AS NVARCHAR(100))+ '
				' + @PricingRuleWhereClause + ' ' + '
		Set @RowsI = @RowsI + @@RowCount
				FETCH NEXT FROM a INTO ' + @TempFieldVarList + '
	END
	CLOSE a
	DEALLOCATE a

	exec LogDCPEvent ''DCP --- RBP_SetPriceGroup: Insert PDPPG and PPG for PricingRuleKey '+CAST(@PricingRuleKey AS NVARCHAR(100))+' '', ''E'', @RowsI, @RowsU, @RowsD

	'

	DECLARE @USql NVARCHAR(MAX)
	SET @USql = 'Update Statistics PlaybookPricingGroup'
	EXEC (@USql)
	SET @USql = 'Update Statistics PlaybookDataPointPricingGroup'
	EXEC (@USql)

--	Set @RowsI = @RowsI + (Select count(*) from PlaybookDataPointPricingGroup_View where PlaybookDataPointGroupKey = @PlaybookDataPointGroupKey)
--	Set @RowsI = @RowsI + (Select count(*) from PlaybookPricingGroup where PlaybookDataPointGroupKey = @PlaybookDataPointGroupKey)

--	SET @FieldList = N'@RulingPointThreshold Int_Type  '

--	PRINT @IndexStmt
--	print @tmpValuesAssigned
--	print @TempFieldListVarAssignment
---	Print @TempFieldList
	--PRINT @SQLStmt
	EXEC dbo.PrintNVarcharMax @SQLStmt
--	PRINT @FieldList
--	Print @TempJoinList

--	EXEC SP_EXECUTESQL @IndexStmt
--	EXEC SP_EXECUTESQL @DropIndexStmt
--sp_helpindex PlaybookDataPoint

--	EXEC SP_EXECUTESQL @SQLStmt, @FieldList



	EXEC (@SQLStmt)



	EXEC dbo.RBP_QuantityBandRecomputeCheck @PlaybookDataPointGroupKey


	EXEC dbo.RBP_PricingBands @PlaybookDataPointGroupKey, @PricingRulePlaybookKey, @PricingRuleKey, @RowsI OUT, @RowsU OUT, @RowsD OUT


	--Now, compute the quantity bands for all of the groups created by this rule
	--The reason we do this here is that we sometimes do not want data points included in a pricing group
	--If they have less than X number of data points in each quantity band.   If it's not placed in a group because
	--of this, the next rule may consider it instead.



	FETCH NEXT FROM CurPriceGroup INTO @PricingRuleKey,@MinimumPoints, @MinimumSales, @MinimumPointsType, @PricingRuleWhereClause, @PricingRuleJoinClause, @RulingPointThreshold, @DataPointReUseIndicator
END

--sp_help PlaybookDataPointPricingGroup
--sp_Helpconstraint PlaybookDataPointPricingGroup

CLOSE CurPriceGroup
DEALLOCATE CurPriceGroup




-- Exec RBP_PricingBands_2 @PlaybookDataPointGroupKey, @PricingRulePlaybookKey, @PricingRuleKey
-- Select count(*) From PlaybookPricingGroup where PlaybookDataPointGroupKey = 108
-- Select count(*) From PlaybookDataPointPricingGroup_View where PlaybookDataPointGroupKey = 108
-- Select count(*) From PlaybookPricingGroupQuantityBand_View where PlaybookDataPointGroupKey = 108









GO
