SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





CREATE  Procedure
[dbo].[Load_DimMonth]
as

declare @a datetime
declare @x int
set @x = 1

set nocount on

exec DBA_IndexRebuild 'DimMonth', 'Drop'

truncate table DimMonth

If not exists (Select 1 from DimMonth where FirstSQLDate is null)
begin
	INSERT INTO DimMonth
	(FirstSQLDate	,
	LastSQLDate	,
	CalendarMonth	,
	CalendarMonthinYear	,
	CalendarMonthNumber	,
	--CalendarMonthNumberinQuarter	,
	--CalendarMonthNumberinYear	,
	-- CalendarQuarter	,
	-- CalendarQuarterinYear	,
	-- CalendarQuarterNumber	,
	-- CalendarQuarterNumberinYear	,
	CalendarYear
	-- CalendarYearMonth	,
	-- FiscalMonth	,
	-- FiscalMonthinYear	,
	-- FiscalMonthNumber	,
	-- FiscalMonthNumberinQuarter	,
	-- FiscalMonthNumberinYear	,
	-- FiscalQuarter	,
	-- FiscalQuarterinYear	,
	-- FiscalQuarterNumber	,
	-- FiscalQuarterNumberinYear	,
	-- FiscalYear	,
	-- FiscalYearNumber	,
	-- FiscalYearMonth)
	)
	Values (
	NULL,--FirstSQLDate
	NULL,--LastSQLDate
	'Unknown',--CalendarMonth
	'Unknown',--CalendarMonthInYear
	null,--CalendarMonthNumber
	--CalendarMonthNumberinQuarter
	--CalendarMonthNumberinYear	,
	-- CalendarQuarter	,
	-- CalendarQuarterinYear	,
	-- CalendarQuarterNumber	,
	-- CalendarQuarterNumberinYear	,
	null-- CalendarYear	,
	-- CalendarYearMonth	,
	-- FiscalMonth	,
	-- FiscalMonthinYear	,
	-- FiscalMonthNumber	,
	-- FiscalMonthNumberinQuarter	,
	-- FiscalMonthNumberinYear	,
	-- FiscalQuarter	,
	-- FiscalQuarterinYear	,
	-- FiscalQuarterNumber	,
	-- FiscalQuarterNumberinYear	,
	-- FiscalYear	,
	-- FiscalYearNumber	,
	-- FiscalYearMonth	,
	-- Holiday	,
	-- HolidayIndicator	)
	)

end



set @a = '2000-01-01'
while @a < '2050-01-01'
begin
	INSERT INTO DimMonth
	(FirstSQLDate	,
	LastSQLDate	,
	CalendarMonth	,
	CalendarMonthinYear	,
	CalendarMonthNumber	,
	--CalendarMonthNumberinQuarter	,
	--CalendarMonthNumberinYear	,
	-- CalendarQuarter	,
	-- CalendarQuarterinYear	,
	-- CalendarQuarterNumber	,
	-- CalendarQuarterNumberinYear	,
	 CalendarYear
	-- CalendarYearMonth	,
	-- FiscalMonth	,
	-- FiscalMonthinYear	,
	-- FiscalMonthNumber	,
	-- FiscalMonthNumberinQuarter	,
	-- FiscalMonthNumberinYear	,
	-- FiscalQuarter	,
	-- FiscalQuarterinYear	,
	-- FiscalQuarterNumber	,
	-- FiscalQuarterNumberinYear	,
	-- FiscalYear	,
	-- FiscalYearNumber	,
	-- FiscalYearMonth)
	)
	Values (@a, --FirstSQLDate
			DateAdd(Day,-1,   DateAdd(Month,1,@a)    ),  --LastSQLDate
	Case DatePart(Month,@a)
		When 1 then 'January'
		When 2 then 'February'
		When 3 then 'March'
		When 4 then 'April'
		When 5 then 'May'
		When 6 then 'June'
		When 7 then 'July'
		When 8 then 'August'
		When 9 then 'September'
		When 10 then 'October'
		When 11 then 'November'
		When 12 then 'December'
	End,--CalendarMonth
	Case DatePart(Month,@a)
		When 1 then 'January'+ ' ' + Cast(DatePart(Year,@a) as varchar(50))
		When 2 then 'February'+ ' ' + Cast(DatePart(Year,@a) as varchar(50))
		When 3 then 'March'+ ' ' + Cast(DatePart(Year,@a) as varchar(50))
		When 4 then 'April'+ ' ' + Cast(DatePart(Year,@a) as varchar(50))
		When 5 then 'May'+ ' ' + Cast(DatePart(Year,@a) as varchar(50))
		When 6 then 'June'+ ' ' + Cast(DatePart(Year,@a) as varchar(50))
		When 7 then 'July'+ ' ' + Cast(DatePart(Year,@a) as varchar(50))
		When 8 then 'August'+ ' ' + Cast(DatePart(Year,@a) as varchar(50))
		When 9 then 'September'+ ' ' + Cast(DatePart(Year,@a) as varchar(50))
		When 10 then 'October'+ ' ' + Cast(DatePart(Year,@a) as varchar(50))
		When 11 then 'November'+ ' ' + Cast(DatePart(Year,@a) as varchar(50))
		When 12 then 'December'+ ' ' + Cast(DatePart(Year,@a) as varchar(50))
	End,--CalendarMonthInYear
	DatePart(Month,@a),--CalendarMonthNumber
	--CalendarMonthNumberinQuarter
	--CalendarMonthNumberinYear	,
-- CalendarQuarter	,
-- CalendarQuarterinYear	,
-- CalendarQuarterNumber	,
-- CalendarQuarterNumberinYear	,
	Cast(DatePart(Year,@a) as varchar(50))-- CalendarYear	,
-- CalendarYearMonth	,
-- FiscalMonth	,
-- FiscalMonthinYear	,
-- FiscalMonthNumber	,
-- FiscalMonthNumberinQuarter	,
-- FiscalMonthNumberinYear	,
-- FiscalQuarter	,
-- FiscalQuarterinYear	,
-- FiscalQuarterNumber	,
-- FiscalQuarterNumberinYear	,
-- FiscalYear	,
-- FiscalYearNumber	,
-- FiscalYearMonth	,
-- Holiday	,
-- HolidayIndicator	)
	)
	set @a = DateAdd(Month,1,@a)
end

If not exists (Select 1 from DimMonth where CalendarMonth = 'Future Month')
begin
	INSERT INTO DimMonth
	(FirstSQLDate	,
	LastSQLDate	,
	CalendarMonth	,
	CalendarMonthinYear	,
	CalendarMonthNumber	,
	--CalendarMonthNumberinQuarter	,
	--CalendarMonthNumberinYear	,
	-- CalendarQuarter	,
	-- CalendarQuarterinYear	,
	-- CalendarQuarterNumber	,
	-- CalendarQuarterNumberinYear	,
	CalendarYear
	-- CalendarYearMonth	,
	-- FiscalMonth	,
	-- FiscalMonthinYear	,
	-- FiscalMonthNumber	,
	-- FiscalMonthNumberinQuarter	,
	-- FiscalMonthNumberinYear	,
	-- FiscalQuarter	,
	-- FiscalQuarterinYear	,
	-- FiscalQuarterNumber	,
	-- FiscalQuarterNumberinYear	,
	-- FiscalYear	,
	-- FiscalYearNumber	,
	-- FiscalYearMonth)
	)
	Values (
	'2050-01-01',--FirstSQLDate
	'2050-01-02',--LastSQLDate
	'Future Month',--CalendarMonth
	'Future Month/Year',--CalendarMonthInYear
	null,--CalendarMonthNumber
	--CalendarMonthNumberinQuarter
	--CalendarMonthNumberinYear	,
	-- CalendarQuarter	,
	-- CalendarQuarterinYear	,
	-- CalendarQuarterNumber	,
	-- CalendarQuarterNumberinYear	,
	null-- CalendarYear	,
	-- CalendarYearMonth	,
	-- FiscalMonth	,
	-- FiscalMonthinYear	,
	-- FiscalMonthNumber	,
	-- FiscalMonthNumberinQuarter	,
	-- FiscalMonthNumberinYear	,
	-- FiscalQuarter	,
	-- FiscalQuarterinYear	,
	-- FiscalQuarterNumber	,
	-- FiscalQuarterNumberinYear	,
	-- FiscalYear	,
	-- FiscalYearNumber	,
	-- FiscalYearMonth	,
	-- Holiday	,
	-- HolidayIndicator	)
	)
end



exec DBA_IndexRebuild 'DimMonth', 'Create'

select top 10 * From DimMonth

--sp_help dimmonth
--Select * from DimMonth
--Select * from DimMonth





GO
