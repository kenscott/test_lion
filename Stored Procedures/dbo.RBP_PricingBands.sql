SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO














--Exec RBP_PricingBands_2 108,1,2,null,null,null
--Exec RBP_PricingBands_2 108,1,1,null,null,null
--Select * From PlaybookPricingGroupQuantityBand_View where PlaybookDataPointGroupKey = 108 order by RulingMemberCount
-- Select * From PlaybookDataPoint where ItemKEy = '18331' order by Total12MonthQuantity
-- Select * from PlaybookDataPointPricingGroup where PlaybookDataPointKey in (Select PlaybookDataPointKey From PlaybookDataPoint where ItemKEy = '18331') order by RulingMemberFlag --Total12MonthQuantity
-- 
-- Select * From PlaybookPricingGroup where GroupValues = '18331' and PlaybookDataPointGroupKey = 108
-- Select * from PlaybookPricingGroupQuantityBand_View where PlaybookPricingGroupKey in (Select PlaybookPricingGroupKey From PlaybookPricingGroup where GroupValues = '18331' and PlaybookDataPointGroupKey = 108)


CREATE                                          PROCEDURE [dbo].[RBP_PricingBands]
@PlaybookDataPointGroupKey INT, @PricingRulePlaybookKey INT, @PricingRuleKey INT, @RowsI INT OUT, @RowsU INT OUT, @RowsD INT OUT
AS
SET QUOTED_IDENTIFIER ON 
SET ANSI_NULLS ON 
SET ARITHABORT ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
SET CONCAT_NULL_YIELDS_NULL ON
SET NUMERIC_ROUNDABORT OFF
SET NoCount ON

-- set @RowsI = 0
-- set @RowsU = 0
-- set @RowsD = 0


DECLARE @ProjectKey key_normal_type
SELECT @ProjectKey = ProjectKey FROM PlaybookDataPointGroup WHERE PlaybookDataPointGroupKey = @PlaybookDataPointGroupKey

EXEC LogDCPEvent 'DCP --- RBP_PricingBands', 'B',0,0,0,@ProjectKey


DECLARE @DeleteCount INT
DECLARE @PricingRuleBandKey INT, @LowerBandPercent DECIMAL(19,8), @UpperBandPercent DECIMAL(19,8), @Logging NVARCHAR(10), @BandSequenceNumber INT, @ScenarioKey INT, @LowerBandQuantity Quantity_Normal_type, @UpperBandQuantity Quantity_Normal_type, @PercentOrQuantityIndicator NVARCHAR(25), @BandCount INT

--Delete everything that depends on quantity bands, since we're re-computing them
DELETE FROM ProposalPrice WHERE PlaybookDataPointGroupKey = @PlaybookDataPointGroupKey
SET @RowsD = @RowsD + @@RowCount
DELETE FROM PlaybookPriceProposal WHERE PlaybookDataPointGroupKey = @PlaybookDataPointGroupKey
SET @RowsD = @RowsD + @@RowCount
DELETE FROM PlaybookDataPointOptimalPrice WHERE PlaybookDataPointOptimalPriceKey IN (SELECT PlaybookDataPointOptimalPriceKey FROM PlaybookDataPointOptimalPrice_View WHERE PlaybookDataPointGroupKey = @PlaybookDataPointGroupKey)
SET @RowsD = @RowsD + @@RowCount
DELETE FROM PlaybookPricingGroupOptimalPrice WHERE PlaybookPricingGroupOptimalPriceKey IN (SELECT PlaybookPricingGroupOptimalPriceKey FROM PlaybookPricingGroupOptimalPrice_View WHERE PlaybookDataPointGroupKey = @PlaybookDataPointGroupKey)
SET @RowsD = @RowsD + @@RowCount
DELETE FROM PlaybookPricingGroupQuantityBand WHERE PlaybookPricingGroupQuantityBandKey IN (SELECT PlaybookPricingGroupQuantityBandKey FROM PlaybookPricingGroupQuantityBand_View WHERE PlaybookDataPointGroupKey = @PlaybookDataPointGroupKey AND PricingRuleKey = @PricingRuleKey)
SET @RowsD = @RowsD + @@RowCount

SET @Logging = 'N'


SET @BandSequenceNumber = 0
--Get a cursor containing each band that exists for this scenario
DECLARE BandCsr CURSOR FOR
SELECT PricingRuleBandKey, LowerBandPercent, UpperBandPercent, LowerBandQuantity, UpperBandQuantity, PercentOrQuantityIndicator, (SELECT COUNT(*) FROM ATKPricingRuleBand WHERE PricingRuleKey = @PricingRuleKey) AS 'BandCount'
FROM ATKPricingRuleBand 
WHERE PricingRuleKey = @PricingRuleKey
ORDER BY LowerBandPercent
OPEN BandCsr
FETCH NEXT FROM BandCsr INTO @PricingRuleBandKey, @LowerBandPercent, @UpperBandPercent, @LowerBandQuantity, @UpperBandQuantity, @PercentOrQuantityIndicator, @BandCount
WHILE @@fetch_status = 0
BEGIN

	SET @BandSequenceNumber = @BandSequenceNumber + 1

	--First lets check to see if we need to recompute the quantity bands
	--If the bands were already re-computed in the last 6 months, then we will carry over the most recent values.


-- 	print @PricingRuleBandKey
-- 	print @PercentOrQuantityIndicator
-- 	print @LowerBandPercent
-- 	print @UpperBandPercent
-- 	print @PlaybookDataPointGroupKey

	--Recompute quantity bands for pricing groups that need to be recomputed
	INSERT INTO PlaybookPricingGroupQuantityBand (PlaybookPricingGroupKey, PricingRuleBandKey, LowerBandQuantity, UpperBandQuantity, BandSequenceNumber/*, RulingMemberCount, NonRulingMemberCount*/)
-- 	Select 
-- 		A.PlaybookPricingGroupKey,
-- 		A.PricingRuleBandKey,
-- 		LowerBandQuantity,
-- 		UpperBandQuantity,
-- 		BandSequenceNumber,
-- 		dbo.fn_PriceQuantityBandMemberCount (PlaybookPricingGroupKey, 'Y', LowerBandQuantity, UpperBandQuantity) as 'RulingMemberCount',
-- 		dbo.fn_PriceQuantityBandMemberCount (PlaybookPricingGroupKey, 'N', LowerBandQuantity, UpperBandQuantity) as 'NonRulingMemberCount'
-- 	From 
--	(
	SELECT
		PlaybookPricingGroupKey,
		@PricingRuleBandKey AS 'PricingRuleBandKey',
		CASE 
			WHEN @BandCount = 1 THEN '-999999999'
			WHEN @PercentOrQuantityIndicator = 'Percent' THEN dbo.fn_PriceQuantityBand (PlaybookPricingGroupKey, @LowerBandPercent)
			WHEN @PercentOrQuantityIndicator = 'Quantity' THEN @LowerBandQuantity	
			ELSE NULL
		END AS 'LowerBandQuantity',
		CASE 
			WHEN @BandCount = 1 THEN '999999999'
			WHEN @PercentOrQuantityIndicator = 'Percent' THEN dbo.fn_PriceQuantityBand (PlaybookPricingGroupKey, @UpperBandPercent)
			WHEN @PercentOrQuantityIndicator = 'Quantity' THEN @UpperBandQuantity
			ELSE NULL
		END AS 'UpperBandQuantity',
		@BandSequenceNumber AS 'BandSequenceNumber'
	FROM	PlaybookPricingGroup
	WHERE	PlaybookDataPointGroupKey = @PlaybookDataPointGroupKey
			AND PlaybookPricingGroup.QuantityBandCreationMethod = 'Recomputed'
			AND PricingRuleKey = @PricingRuleKey--) A

	SET @RowsI = @RowsI + @@RowCount

	--Now, insert old quantity bands for groups that we do not want to recompute
	INSERT INTO PlaybookPricingGroupQuantityBand (PlaybookPricingGroupKey, PricingRuleBandKey, LowerBandQuantity, UpperBandQuantity, BandSequenceNumber)
	SELECT
		A.PlaybookPricingGroupKey,			
		@PricingRuleBandKey,
		(SELECT TOP 1 LowerBandQuantity FROM PlaybookPricingGroupQuantityBand B WHERE A.PreviousValuesPlaybookPricingGroupKey = B.PlaybookPricingGroupKey ORDER BY PlaybookPricingGroupQuantityBandKey DESC),--		B.LowerBandQuantity,
		(SELECT TOP 1 LowerBandQuantity FROM PlaybookPricingGroupQuantityBand B WHERE A.PreviousValuesPlaybookPricingGroupKey = B.PlaybookPricingGroupKey ORDER BY PlaybookPricingGroupQuantityBandKey DESC),--		B.UpperBandQuantity,
		@BandSequenceNumber
	FROM	PlaybookPricingGroup A
--		join PlaybookPricingGroupQuantityBand B on A.PreviousValuesPlaybookPricingGroupKey = B.PlaybookPricingGroupKey
	WHERE	PlaybookDataPointGroupKey = @PlaybookDataPointGroupKey
			AND A.QuantityBandCreationMethod = 'Used Previous Value'
			AND PricingRuleKey = @PricingRuleKey
	SET @RowsI = @RowsI + @@RowCount

	FETCH NEXT FROM BandCsr INTO @PricingRuleBandKey, @LowerBandPercent, @UpperBandPercent, @LowerBandQuantity, @UpperBandQuantity, @PercentOrQuantityIndicator, @BandCount

END
CLOSE BandCsr
DEALLOCATE BandCsr


DECLARE @USql NVARCHAR(MAX)
SET @USql = 'Update Statistics PlaybookPricingGroupQuantityBand'
EXEC (@USql)

--Now that we've figured out all of the bands, we'll need to resolve any conflits that have occurred
--A conflict is where the upper quantity from one band matches the lower quantity from the next band.
--In this case, all data points matching that quantity will be moved into the band
--that contains the most data points for that quantity already  
--example:  If Band 1 has 5 data points with quantity 10, and Band 2 has 2 data points with quantity=10,
--			All data points with quantity=10 would be moved into Band 1 by incrementing the lower quantity of band 2 by a fraction to 10.00000001 - X



DECLARE @MinBandKey INT, @MaxBandKey INT, @CurrentBandKey INT, @NextBandKey INT, @CurrentBandKeyLowerPct DECIMAL(19,8), @CurrentBandKeyUpperPct DECIMAL(19,8), @CurrentBandKeyUpperQty DECIMAL(19,8), @NextBandKeyLowerPct DECIMAL(19,8), @NextBandKeyLowerQty DECIMAL(19,8), @NextBandKeyUpperPct  DECIMAL(19,8), @PrevBand INT, @NextBandKeyUpperQty DECIMAL(19,8), @CurrentBandKeyLowerQty DECIMAL(19,8), @Result NVARCHAR(500), @PlaybookPricingGroupKey INT



--Loop once for each PricingGroup in this ProjectKey that has this PricingRuleKey
DECLARE PGCsr CURSOR FOR SELECT DISTINCT PlaybookPricingGroupKey FROM PlaybookPricingGroup WHERE PlaybookDataPointGroupKey = @PlaybookDataPointGroupKey AND PricingRuleKey = @PricingRuleKey
OPEN PGCsr
FETCH NEXT FROM PGCsr INTO @PlaybookPricingGroupKey
WHILE @@fetch_STatus = 0
BEGIN

	--Gets the lowest band # for this scenario (doesnt have to start at band #1)
	SET @MinBandKey = (SELECT MIN(PlaybookPricingGroupQuantityBandKey) FROM PlaybookPricingGroupQuantityBand WHERE PlaybookPricingGroupKey = @PlaybookPricingGroupKey)	--Gets the band with the lowest quantity
	--Gets the highest band # for this scenario
	SET @MaxBandKey = (SELECT MAX(PlaybookPricingGroupQuantityBandKey) FROM PlaybookPricingGroupQuantityBand WHERE PlaybookPricingGroupKey = @PlaybookPricingGroupKey)  --Gets the band with the highest upper quantity


	SET @CurrentBandKey = @MinBandKey
	--Gets the next highest band #
	SELECT @NextBandKey = (SELECT MIN(PlaybookPricingGroupQuantityBandKey) FROM PlaybookPricingGroupQuantityBand WHERE PlaybookPricingGroupKey = @PlaybookPricingGroupKey AND PlaybookPricingGroupQuantityBandKey > @CurrentBandKey)

	--Loop until there are no more bands left
	WHILE @CurrentBandKey < @MaxBandKey
	BEGIN
		--Get the %'s for the current band
		SELECT  @CurrentBandKeyLowerPct = B.LowerBandPercent, 
				@CurrentBandKeyUpperPct = B.UpperBandPercent,
				@CurrentBandKeyLowerQty = A.LowerBandQuantity,
				@CurrentBandKeyUpperQty = A.UpperBandQuantity
		FROM PlaybookPricingGroupQuantityBand A
			JOIN ATKPricingRuleBand B ON A.PricingRuleBandKey = B.PricingRuleBandKey
		WHERE A.PlaybookPricingGroupQuantityBandKey = @CurrentBandKey

		--Get the %'s for the next highest band
		SELECT @NextBandKeyLowerPct = B.LowerBandPercent, 
				@NextBandKeyUpperPct = B.UpperBandPercent,
				@NextBandKeyLowerQty = A.LowerBandQuantity,
				@NextBandKeyUpperQty = A.UpperBandQuantity
		FROM PlaybookPricingGroupQuantityBand A
			JOIN ATKPricingRuleBand B ON A.PricingRuleBandKey = B.PricingRuleBandKey
		WHERE A.PlaybookPricingGroupQuantityBandKey = @NextBandKey

		IF @Logging = 'Y'	
		BEGIN
			PRINT @CurrentBandKey
			PRINT @NextBandKey
			PRINT '-------'
		END
	
		--Only run this code if we have bands that overlap. If they don't overlap there is no problem
		IF @CurrentBandKeyUpperQty = @NextBandKeyLowerQty
		BEGIN
			IF @Logging = 'Y'
			BEGIN
				PRINT 'Match'
				PRINT @CurrentBandKeyUpperQty
			END
	
			--This SP will determine which band the overlapping quantity should go in
			EXEC dbo.RBP_PriceQuantityBandResolution @PlaybookPricingGroupKey, @CurrentBandKeyLowerPct, @CurrentBandKeyUpperPct, @NextBandKeyLowerPct, @NextBandKeyUpperPct, @CurrentBandKeyUpperQty, 'Y', @Result OUT

			IF @Logging = 'Y'	
			BEGIN
				PRINT 'Result of RBP_PriceQuantityBandResolution:' + @Result
			END
	
			IF @Result = 'QuantityGoesInBand1'
			BEGIN
				UPDATE PlaybookPricingGroupQuantityBand SET LowerBandQuantity = LowerBandQuantity + .00000001 WHERE PlaybookPricingGroupKey = @PlaybookPricingGroupKey AND PlaybookPricingGroupQuantityBandKey >= @NextBandKey
				SET @RowsU = @RowsU + @@RowCount
			END
			ELSE IF @Result = 'QuantityGoesInBand2'
			BEGIN
				UPDATE PlaybookPricingGroupQuantityBand SET UpperBandQuantity = UpperBandQuantity - .00000001 WHERE PlaybookPricingGroupKey = @PlaybookPricingGroupKey AND PlaybookPricingGroupQuantityBandKey = @CurrentBandKey
				SET @RowsU = @RowsU + @@RowCount
			END
	
		END
	
		--Increment the band
		SET @CurrentBandKey = @NextBandKey
		--Get the next band
		SET @NextBandKey = (SELECT MIN(PlaybookPricingGroupQuantityBandKey) FROM PlaybookPricingGroupQuantityBand WHERE PlaybookPricingGroupKey = @PlaybookPricingGroupKey AND PlaybookPricingGroupQuantityBandKey > @CurrentBandKey)
	END
	--At the very end, if any of the bands represent an invalid range (the upper is less than lower), we need to set them to null becuase no data points can ever fit in them anyway.
	UPDATE PlaybookPricingGroupQuantityBand SET LowerBandQuantity = NULL, UpperBandQuantity = NULL WHERE UpperBandQuantity < LowerBandQuantity AND PlaybookPricingGroupKey = @PlaybookPricingGroupKey
	SET @RowsU = @RowsU + @@RowCount
	--Now, we should automatically set the lowest band quantity to -NULL and the highest band quantity to NULL to represent an undefined beginning and ending range
	--print @MinBandKey

--	Update PlaybookPricingGroupQuantityBand set LowerBandQuantity = -99999999999 where PlaybookPricingGroupQuantityBandKey = @MinBandKey and PlaybookPricingGroupKey = @PlaybookPricingGroupKey
--	Update PlaybookPricingGroupQuantityBand set UpperBandQuantity = 99999999999 where PlaybookPricingGroupQuantityBandKey = @MaxBandKey and PlaybookPricingGroupKey = @PlaybookPricingGroupKey

	FETCH NEXT FROM PGCsr INTO @PlaybookPricingGroupKey
END
CLOSE PGCsr
DEALLOCATE PGCsr


--Finds the bands w/ the lowest quantity and removes its lower bound by setting it to -99999999999
UPDATE PlaybookPricingGroupQuantityBand SET LowerBandQuantity = -99999999999 
FROM PlaybookPricingGroupQuantityBand B
JOIN 
	(SELECT PlaybookPricingGroupKey,
	(SELECT MIN(BandSequenceNumber) FROM PlaybookPricingGroupQuantityBand B WHERE A.PlaybookPricingGroupKey = B.PlaybookPricingGroupKey AND LowerBandQuantity IS NOT NULL) AS MinBandSequenceNumber
	FROM PlaybookPricingGroupQuantityBand_View A
	WHERE PricingRuleKey = @PricingRuleKey AND PlaybookDataPointGroupKey = @PlaybookDataPointGroupKey
	GROUP BY PlaybookPricingGroupKey) C
ON B.PlaybookPricingGroupKey = C.PlaybookPricingGroupKey AND B.BandSequenceNumber = C.MinBandSequenceNumber
SET @RowsU = @RowsU + @@RowCount
--sp_help PlaybookPricingGroupQuantityBand_View

--Finds the bands w/ the highest quantity and removes its upper bound by setting it to 99999999999
UPDATE PlaybookPricingGroupQuantityBand SET UpperBandQuantity = 99999999999 
FROM PlaybookPricingGroupQuantityBand B
JOIN 
	(SELECT PlaybookPricingGroupKey,
	(SELECT MAX(BandSequenceNumber) FROM PlaybookPricingGroupQuantityBand B WHERE A.PlaybookPricingGroupKey = B.PlaybookPricingGroupKey AND LowerBandQuantity IS NOT NULL) AS MaxBandSequenceNumber
	FROM PlaybookPricingGroupQuantityBand_View A
	WHERE PricingRuleKey = @PricingRuleKey AND PlaybookDataPointGroupKey = @PlaybookDataPointGroupKey
	GROUP BY PlaybookPricingGroupKey) C
ON B.PlaybookPricingGroupKey = C.PlaybookPricingGroupKey AND B.BandSequenceNumber = C.MaxBandSequenceNumber
SET @RowsU = @RowsU + @@RowCount



--Now that we've straightened out the quantity bands, we need to set the # of DPs in each band
--Recompute quantity bands for pricing groups that need to be recomputed

UPDATE PlaybookPricingGroupQuantityBand 
SET RulingMemberCount =  dbo.fn_PriceQuantityBandMemberCount (PlaybookPricingGroupKey, 'Y', LowerBandQuantity, UpperBandQuantity),
	NonRulingMemberCount = dbo.fn_PriceQuantityBandMemberCount (PlaybookPricingGroupKey, 'N', LowerBandQuantity, UpperBandQuantity)
FROM PlaybookPricingGroupQuantityBand
WHERE PlaybookPricingGroupQuantityBandKey IN (SELECT PlaybookPricingGroupQuantityBandKey FROM PlaybookPricingGroupQuantityBand_View WHERE PlaybookDataPointGroupKey = @PlaybookDataPointGroupKey AND PricingRuleKey = @PricingRuleKey)
SET @RowsU = @RowsU + @@RowCount



--Now, if any quantity bands do not meet the minimum # of data points requirement, 
--remove the pricing group completely as if it had not been selected by the rule.
--This way, the next rule can try to pick it up instead.
DECLARE @MinimumPoints INT, @MinimumPointsType NVARCHAR(500)

SELECT 
@MinimumPoints = MinimumPoints, 
@MinimumPointsType = MinimumPointsType 
FROM ATKPricingRule WHERE PricingRulePlaybookKey = @PricingRulePlaybookKey AND PricingRuleKey = @PricingRuleKey



IF @MinimumPointsType = 'PerQuantityBand'
BEGIN
	SELECT DISTINCT PlaybookPricingGroupKey
	INTO #TempPDPGK
	FROM PlaybookPricingGroupQuantityBand_View 
	WHERE PlaybookDataPointGroupKey = @PlaybookDataPointGroupKey
	AND PricingRuleKey = @PricingRuleKey
	AND RulingMemberCount < @MinimumPoints

	SET @DeleteCount = (SELECT COUNT(*) FROM #TempPDPGK)
	PRINT 'Number of Pricing Groups being deleted because of not meeting min # of DPs per QB: ' +CAST(@DeleteCount AS NVARCHAR(100))

	DELETE FROM PlaybookPricingGroupQuantityBand 
	WHERE PlaybookPricingGroupKey IN (SELECT PlaybookPricingGroupKey FROM #TempPDPGK)
	SET @RowsD = @RowsD + @@RowCount

	DELETE FROM PlaybookDataPointPricingGroup
	WHERE PlaybookPricingGroupKey IN (SELECT PlaybookPricingGroupKey FROM #TempPDPGK)
	SET @RowsD = @RowsD + @@RowCount

	DELETE FROM PlaybookPricingGroup
	WHERE PlaybookPricingGroupKey IN (SELECT PlaybookPricingGroupKey FROM #TempPDPGK)
	SET @RowsD = @RowsD + @@RowCount

END

EXEC LogDCPEvent 'DCP --- RBP_PricingBands', 'E', @RowsI, @RowsU, @RowsD 
























GO
