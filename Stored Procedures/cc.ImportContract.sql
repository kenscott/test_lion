SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [cc].[ImportContract]
    @BatchKey INT ,
    @WebUserKey INT
AS
SET NOCOUNT ON

DECLARE @ContractKey INT ,
    @Originator VARCHAR(3) ,
    @NewContractNumber VARCHAR(6) ,
    @IsNewContract BIT ,
    @rowCheck INT

--FOR testing-------------
--DECLARE @BatchKey INT ,
--    @WebUserKey INT

--SET @BatchKey = '201406'
--SET @WebUserKey = 1
---------------------------

IF OBJECT_ID('tempdb.dbo.#LocationLevel', 'U') IS NOT NULL
    DROP TABLE #LocationLevel; 
IF OBJECT_ID('tempdb.dbo.#ContractSequence', 'U') IS NOT NULL
    DROP TABLE #ContractSequence; 
IF OBJECT_ID('tempdb.dbo.#TempContractNumber', 'U') IS NOT NULL
    DROP TABLE #TempContractNumber; 

UPDATE  i
SET     ErrorMessage = NULL
FROM    cc.SupplierContractImport i
WHERE   i.BatchKey = @BatchKey
            
/*LPF-5687--Contract Manager - Import | Authorized should be a required field as Y or N.*/
UPDATE  [cc].[SupplierContractImport]
SET     AuthorizedIndicator = 'N'
WHERE   ( BatchKey = @BatchKey
          AND AuthorizedIndicator <> 'Y'
        )
        OR ( BatchKey = @BatchKey
             AND AuthorizedIndicator IS NULL
           )


--Data Validation
UPDATE  i
SET     ErrorMessage = ISNULL(ErrorMessage, '')
        + 'New Record, Invalid PyramidCode ' + i.PyramidCode + '; '
FROM    cc.SupplierContractImport i
WHERE   i.BatchKey = @BatchKey
        AND PyramidCode NOT IN ( '1', '2' )
        AND i.ContractNumber = 'NEW'
	
UPDATE  i
SET     ErrorMessage = ISNULL(ErrorMessage, '')
        + 'New Record, Invalid Originator ' + i.Originator + '; '
FROM    cc.SupplierContractImport i
        LEFT JOIN dbo.DimAccountGroup1 dag1 ON dag1.AG1Level1 = i.Originator
                                               AND dag1.Inactive = 0
WHERE   i.BatchKey = @BatchKey
        AND i.Originator <> 'WUK'
        AND dag1.AccountGroup1Key IS NULL
        AND i.ContractNumber = 'NEW'

UPDATE  i
SET     ErrorMessage = ISNULL(ErrorMessage, '')
        + 'New Record, Invalid ContractOwner ' + i.ContractOwner + '; '
FROM    cc.SupplierContractImport i
        LEFT JOIN dbo.DimAccountGroup1 dag1 ON dag1.AG1Level1 = i.ContractOwner
                                               AND dag1.Inactive = 0
WHERE   i.BatchKey = @BatchKey
        AND i.ContractOwner <> 'WUK'
        AND dag1.AccountGroup1Key IS NULL
        AND i.ContractNumber = 'NEW'

UPDATE  i
SET     ErrorMessage = ISNULL(ErrorMessage, '')
        + 'New Record, Invalid Supplier ' + i.VendorNumber + '; '
FROM    cc.SupplierContractImport i
        LEFT JOIN dbo.DimVendor dv ON dv.VendorNumber = i.VendorNumber
                                      AND dv.Inactive = 0
WHERE   i.BatchKey = @BatchKey
        AND dv.VendorKey IS NULL
        AND i.ContractNumber = 'NEW'

UPDATE  i
SET     ErrorMessage = ISNULL(ErrorMessage, '')
        + 'New Record, All Accounts Invalid For Supplier ' + i.VendorNumber
        + '; '
FROM    cc.SupplierContractImport i
        INNER JOIN dbo.DimVendor dv ON dv.VendorNumber = i.VendorNumber
                                       AND dv.Inactive = 0
WHERE   i.BatchKey = @BatchKey
        AND dv.AllowAllAccounts = 'Never'
        AND i.Accounts = 'ALL'
        AND i.ContractNumber = 'NEW'

UPDATE  i
SET     ErrorMessage = ISNULL(ErrorMessage, '')
        + 'New Record, All Locations Invalid For Supplier ' + i.VendorNumber
        + '; '
FROM    cc.SupplierContractImport i
        INNER JOIN dbo.DimVendor dv ON dv.VendorNumber = i.VendorNumber
                                       AND dv.Inactive = 0
WHERE   i.BatchKey = @BatchKey
        AND dv.AllowAllLocations = 'Never'
        AND i.Locations = 'ALL'
        AND i.ContractNumber = 'NEW'

UPDATE  i
SET     ErrorMessage = ISNULL(ErrorMessage, '')
        + 'New Record, Missing Account; '
FROM    cc.SupplierContractImport i
        INNER JOIN cc.SupplierContractImportAccount ia ON ia.BatchKey = i.BatchKey
                                                          AND ia.SequenceNumber = i.SequenceNumber
WHERE   i.BatchKey = @BatchKey
        AND ia.AccountNumber IS NULL
        AND i.ContractNumber = 'NEW'

UPDATE  i
SET     ErrorMessage = ISNULL(ErrorMessage, '')
        + 'New Record, Invalid AccountNumber ' + ia.AccountNumber + '; '
FROM    cc.SupplierContractImport i
        INNER JOIN cc.SupplierContractImportAccount ia ON ia.BatchKey = i.BatchKey
                                                          AND ia.SequenceNumber = i.SequenceNumber
        LEFT JOIN dbo.DimAccount da ON da.AccountNumber = ia.AccountNumber
                                       AND da.Inactive = 0
WHERE   i.BatchKey = @BatchKey
        AND ia.AccountNumber <> 'ALL'
        AND da.AccountKey IS NULL
        AND i.ContractNumber = 'NEW'

UPDATE  i
SET     ErrorMessage = ISNULL(ErrorMessage, '')
        + 'New Record, Missing Location; '
FROM    cc.SupplierContractImport i
        INNER JOIN cc.SupplierContractImportLocation il ON il.BatchKey = i.BatchKey
                                                           AND il.SequenceNumber = i.SequenceNumber
WHERE   i.BatchKey = @BatchKey
        AND il.LocationValue IS NULL
        AND i.ContractNumber = 'NEW'

SELECT  il.SupplierContractImportLocationKey ,
        CASE WHEN dag1.AccountGroup1Key IS NOT NULL THEN 1
             WHEN dn.NetworkKey IS NOT NULL THEN 2
             WHEN da.AreaKey IS NOT NULL THEN 3
             WHEN dr.RegionKey IS NOT NULL THEN 4
        END AS LocationLevelKey
INTO    #LocationLevel
FROM    cc.SupplierContractImportLocation il
        LEFT JOIN dbo.DimAccountGroup1 dag1 ON dag1.AG1Level1 = il.LocationValue
                                               AND dag1.Inactive = 0
        LEFT JOIN dbo.DimNetwork dn ON dn.NetworkId = il.LocationValue
        LEFT JOIN dbo.DimArea da ON da.Area = il.LocationValue
        LEFT JOIN dbo.DimRegion dr ON dr.Region = il.LocationValue
WHERE   il.BatchKey = @BatchKey
        AND il.LocationValue <> 'ALL'

UPDATE  i
SET     ErrorMessage = ISNULL(ErrorMessage, '')
        + 'New Record, Invalid Location ' + il.LocationValue + '; '
FROM    cc.SupplierContractImport i
        INNER JOIN cc.SupplierContractImportLocation il ON il.BatchKey = i.BatchKey
                                                           AND il.SequenceNumber = i.SequenceNumber
        LEFT JOIN #LocationLevel ll ON ll.SupplierContractImportLocationKey = il.SupplierContractImportLocationKey
WHERE   i.BatchKey = @BatchKey
        AND ll.LocationLevelKey IS NULL
        AND il.LocationValue <> 'ALL'
        AND i.ContractNumber = 'NEW'

UPDATE  i
SET     ErrorMessage = ISNULL(ErrorMessage, '')
        + 'New Record, Invalid ContractType ' + i.ContractType + '; '
FROM    cc.SupplierContractImport i
WHERE   i.BatchKey = @BatchKey
        AND ContractType NOT IN ( 'C', 'O' )
        AND i.ContractNumber = 'NEW'

UPDATE  i
SET     ErrorMessage = ISNULL(ErrorMessage, '') + 'Invalid Start/ExpiryDate; '
FROM    cc.SupplierContractImport i
WHERE   i.BatchKey = @BatchKey
        AND ExpirationDate < StartDate
        AND i.ContractNumber = 'NEW'

UPDATE  i
SET     ErrorMessage = ISNULL(ErrorMessage, '')
        + 'New Record, Invalid ExpiryDate; '
FROM    cc.SupplierContractImport i
WHERE   i.BatchKey = @BatchKey
        AND ExpirationDate < '2015-01-01'
        AND i.ContractNumber = 'NEW'

UPDATE  i
SET     ErrorMessage = ISNULL(ErrorMessage, '')
        + 'New Record, Invalid StartDate; '
FROM    cc.SupplierContractImport i
WHERE   i.BatchKey = @BatchKey
        AND StartDate < '2015-01-01'
        AND i.ContractNumber = 'NEW'

UPDATE  i
SET     ErrorMessage = ISNULL(ErrorMessage, '')
        + 'New Record, Invalid PurchaseType ' + i.PurchaseType + '; '
FROM    cc.SupplierContractImport i
WHERE   i.BatchKey = @BatchKey
        AND PurchaseType NOT IN ( 'B', 'S', 'D' )
        AND i.ContractNumber = 'NEW'

UPDATE  i
SET     ErrorMessage = ISNULL(ErrorMessage, '')
        + 'New Record, Invalid AuthorizedIndicator ' + i.AuthorizedIndicator
        + '; '
FROM    cc.SupplierContractImport i
WHERE   i.BatchKey = @BatchKey
        AND AuthorizedIndicator NOT IN ( 'Y', 'N' )
        AND i.ContractNumber = 'NEW'

UPDATE  i
SET     ErrorMessage = ISNULL(ErrorMessage, '')
        + 'New Record, Invalid PricingContractType ' + i.PricingContractType
        + '; '
FROM    cc.SupplierContractImport i
WHERE   i.BatchKey = @BatchKey
        AND ISNULL(PricingContractType, ' ') NOT IN ( ' ', 'B', 'N' )
        AND i.ContractNumber = 'NEW'

UPDATE  i
SET     ErrorMessage = ISNULL(ErrorMessage, '')
        + 'New Record, Invalid PricingContractType ' + i.PricingContractType
        + '; '
FROM    cc.SupplierContractImport i
WHERE   i.BatchKey = @BatchKey
        AND ISNULL(i.PricingContractID, '') <> ''
        AND ISNULL(PricingContractType, ' ') = ' '
        AND i.ContractNumber = 'NEW'

UPDATE  i
SET     ErrorMessage = ISNULL(ErrorMessage, '')
        + 'New Record, Invalid PricingContractID ' + i.PricingContractID
        + '; '
FROM    cc.SupplierContractImport i
WHERE   i.BatchKey = @BatchKey
        AND i.ContractNumber = 'NEW'
        AND LEFT(ISNULL(i.PricingContractID, ''),50) NOT IN (
        SELECT DISTINCT
                PricingContractName  --was pricingContractKEY verified this is correct
        FROM    Lion.PricingContract )

UPDATE  i
SET     ErrorMessage = ISNULL(ErrorMessage, '')
        + 'New Record, Invalid LevelType ' + i.LevelType + '; '
FROM    cc.SupplierContractImport i
WHERE   i.BatchKey = @BatchKey
        AND LevelType NOT IN ( 'Product', 'SPG', 'MPG' )
        AND i.ContractNumber = 'NEW'

UPDATE  i
SET     ErrorMessage = ISNULL(ErrorMessage, '')
        + 'New Record, Invalid Product ' + i.LevelCode + '; '
FROM    cc.SupplierContractImport i
        LEFT JOIN dbo.DimItem di ON di.ItemUDVarchar20 = i.LevelCode   -- LPF-3910 CC-1033 Contract Import should use Product Code not Product ID (jed)
                                    AND di.ItemUDVarchar1 = i.PyramidCode  -- LPF-3954 cc.importcontract issue with change to product code (jed)
WHERE   i.LevelType = 'Product'
        AND i.BatchKey = @BatchKey
        AND di.ItemKey IS NULL
        AND i.ContractNumber = 'NEW'

UPDATE  i
SET     ErrorMessage = ISNULL(ErrorMessage, '') + 'New Record, Invalid MPG '
        + i.LevelCode + '; '
FROM    cc.SupplierContractImport i
        LEFT JOIN dbo.DimItemGroup1 dig1 ON dig1.IG1Level1 = i.LevelCode
                                            AND dig1.IG1Level2 = i.PyramidCode
WHERE   i.LevelType = 'MPG'
        AND i.BatchKey = @BatchKey
        AND dig1.ItemGroup1Key IS NULL
        AND i.ContractNumber = 'NEW'

UPDATE  i
SET     ErrorMessage = ISNULL(ErrorMessage, '') + 'New Record, Invalid SPG '
        + i.LevelCode + '; '
FROM    cc.SupplierContractImport i
        LEFT JOIN dbo.DimItemGroup3 dig3 ON dig3.IG3Level1 = i.LevelCode
                                            AND dig3.IG3Level4 = i.PyramidCode
WHERE   i.LevelType = 'SPG'
        AND i.BatchKey = @BatchKey
        AND dig3.ItemGroup3Key IS NULL
        AND i.ContractNumber = 'NEW'

-- LPF-3947 Duplicate Product/SPG/MPG not allowed in the same sequence (jed)
UPDATE  i
SET     ErrorMessage = ISNULL(ErrorMessage, '')
        + 'New Record, Duplicate product line ' + i.LevelCode + '; '
FROM    cc.SupplierContractImport i
WHERE   i.LevelType = 'Product'
        AND i.BatchKey = @BatchKey
        AND i.ContractNumber = 'NEW'
        AND ( SELECT    COUNT(*)
              FROM      cc.SupplierContractImport i2
              WHERE     i2.BatchKey = @BatchKey
                        AND i2.SequenceNumber = i.SequenceNumber
                        AND i2.LevelCode = i.LevelCode
            ) > 1

-- LPF-3947 Duplicate Product/SPG/MPG not allowed in the same sequence (jed)
UPDATE  i
SET     ErrorMessage = ISNULL(ErrorMessage, '')
        + 'New Record, Duplicate MPG line ' + i.LevelCode + '; '
FROM    cc.SupplierContractImport i
WHERE   i.LevelType = 'MPG'
        AND i.BatchKey = @BatchKey
        AND i.ContractNumber = 'NEW'
        AND ( SELECT    COUNT(*)
              FROM      cc.SupplierContractImport i2
              WHERE     i2.BatchKey = @BatchKey
                        AND i2.SequenceNumber = i.SequenceNumber
                        AND i2.LevelCode = i.LevelCode
            ) > 1

-- LPF-3947 Duplicate Product/SPG/MPG not allowed in the same sequence (jed)
UPDATE  i
SET     ErrorMessage = ISNULL(ErrorMessage, '')
        + 'New Record, Duplicate SPG line ' + i.LevelCode + '; '
FROM    cc.SupplierContractImport i
WHERE   i.LevelType = 'SPG'
        AND i.BatchKey = @BatchKey
        AND i.ContractNumber = 'NEW'
        AND ( SELECT    COUNT(*)
              FROM      cc.SupplierContractImport i2
              WHERE     i2.BatchKey = @BatchKey
                        AND i2.SequenceNumber = i.SequenceNumber
                        AND i2.LevelCode = i.LevelCode
            ) > 1

-- LPF-3952 cc.importcontract should set claimType (jed)
UPDATE  i
SET     ErrorMessage = ISNULL(ErrorMessage, '')
        + 'New Record, Must provide ClaimAmount, ClaimPercent or ClaimCost; '
FROM    cc.SupplierContractImport i
WHERE   i.BatchKey = @BatchKey
        AND i.ContractNumber = 'NEW'
        AND i.ClaimAmount IS NULL
        AND i.ClaimPercent IS NULL
        AND i.NetCost IS NULL

-- LPF-3944 - cc.ImportContract should error if row sets multiple claim values or no claim values (jed)
UPDATE  i
SET     ErrorMessage = ISNULL(ErrorMessage, '')
        + 'New Record, Must only provide one of ClaimAmount, ClaimPercent or ClaimCost; '
FROM    cc.SupplierContractImport i
WHERE   i.BatchKey = @BatchKey
        AND i.ContractNumber = 'NEW'
        AND ( CASE WHEN i.claimPercent IS NULL THEN 0
                   ELSE 1
              END + CASE WHEN i.claimAmount IS NULL THEN 0
                         ELSE 1
                    END + CASE WHEN i.NetCost IS NULL THEN 0
                               ELSE 1
                          END ) > 1

UPDATE  i
SET     ErrorMessage = ISNULL(ErrorMessage, '')
        + 'New Record, Invalid QuantityType ' + i.QuantityType + '; '
FROM    cc.SupplierContractImport i
WHERE   i.BatchKey = @BatchKey
        AND i.ContractNumber = 'NEW'
        AND ISNULL(QuantityType, ' ') NOT IN ( ' ', 'U', 'S', 'C' )

UPDATE  i
SET     ErrorMessage = ISNULL(ErrorMessage, '')
        + 'New Record, Invalid QuantityLimit '
        + CAST(i.QuantityLimit AS VARCHAR(20)) + '; '
FROM    cc.SupplierContractImport i
WHERE   i.BatchKey = @BatchKey
        AND i.ContractNumber = 'NEW'
        AND ISNULL(QuantityLimit, 0) < 0

--LPF-4468 Import Contract - Enforce rule to only add/amend products for the supplier
UPDATE  i
SET     ErrorMessage = ISNULL(ErrorMessage, '')
        + 'New Record, Not valid product for vendor ' + i.LevelCode + '; '
FROM    cc.SupplierContractImport i
        INNER JOIN dbo.DimVendor v ON v.VendorNumber = i.VendorNumber
        LEFT JOIN dbo.DimItem di ON v.VendorKey = di.ItemVendorKey
                                    AND di.ItemUDVarChar20 = i.LevelCode
                                    AND di.ItemUDVarchar1 = i.PyramidCode
WHERE   di.ItemUDVarChar20 IS NULL
        AND i.BatchKey = @Batchkey
        AND i.LevelType = 'Product'
        AND i.ContractNumber = 'NEW'



--If NO Errors...
IF ( SELECT COUNT(*)
     FROM   cc.SupplierContractImport i
     WHERE  BatchKey = @BatchKey
            AND ContractNumber = 'NEW'
            AND ErrorMessage IS NOT NULL
   ) = 0
    BEGIN

        DECLARE @NewContractKey TABLE
            (
              ContractKey KEY_NORMAL_TYPE ,
              BatchKey INT ,
              SequenceNumber VARCHAR(10)
            )

--Get one record per SequenceNumber
        SELECT  SequenceNumber ,
                MIN(SupplierContractImportKey) AS SupplierContractImportKey
        INTO    [#ContractSequence]
        FROM    cc.SupplierContractImport
        WHERE   ( BatchKey = @BatchKey )
                AND ( ContractNumber = 'NEW' )
        GROUP BY SequenceNumber

        INSERT  INTO [cc].[SupplierContract]
                ( Originator ,
                  ContractNumber ,
                  ContractOwner ,
                  VendorKey ,
                  ContractReference ,
                  StartDate ,
                  ExpirationDate ,
                  JobSite ,
                  AuthorizedIndicator ,
                  PyramidCode ,
                  ContractType1 ,
                  JobFlag ,
                  ContractSourceKey ,
                  CreatedByWebUserKey ,
                  ContractCreationDT ,
                  Developer ,
                  PricingContractIndicator ,
                  PricingContractID ,
                  PurchaseType ,
                  ContactName ,
                  ContactEmail ,
                  [Version] ,
                  StatusKey ,
                  BatchKey ,
                  SequenceNumber 
		        )
        OUTPUT  INSERTED.ContractKey ,
                INSERTED.BatchKey ,
                INSERTED.SequenceNumber
                INTO @NewContractKey
                SELECT DISTINCT
                        Originator ,
                        '000000' ,
                        ContractOwner ,
                        v.VendorKey ,
                        ContractReference ,
                        StartDate ,
                        ExpirationDate ,
                        JobSite ,
                        AuthorizedIndicator ,
                        PyramidCode ,
                        ContractType ,
                        CASE ISNULL(i.jobSite, '')
                          WHEN '' THEN 0
                          ELSE 1
                        END , --JobFlag  LPF-3945 If project or site is null or empty, ensure indicator is set to no (jed)
                        2 ,  --ContractSourceKey
                        @WebUserKey ,
                        GETDATE() ,  --ContractCreationDT
                        Developer ,
                        PricingContractType ,
                        PricingContractID ,
                        PurchaseType ,
                        ContactName ,
                        ContactEmail ,
                        0 ,   --Version
                        2 ,   --StatusKey -Draft
                        BatchKey ,
                        i.SequenceNumber
                FROM    cc.SupplierContractImport i
                        INNER JOIN dbo.DimVendor v ON v.VendorNumber = i.VendorNumber
                        INNER JOIN #ContractSequence cs ON cs.SequenceNumber = i.SequenceNumber
                                                           AND cs.SupplierContractImportKey = i.SupplierContractImportKey
                WHERE   ( i.BatchKey = @BatchKey )
                        AND ( i.ContractNumber = 'NEW' )

			

--SupplierContractAccount
        INSERT  INTO cc.SupplierContractAccount
                ( ContractKey ,
                  AccountKey ,
                  CreatedByWebUserKey ,
                  CreatedOnDT
                )
                SELECT  c.ContractKey ,
                        da.AccountKey ,
                        @WebUserKey ,
                        GETDATE()
                FROM    cc.SupplierContractImportAccount a
                        INNER JOIN @NewContractKey c ON c.BatchKey = a.BatchKey
                                                        AND c.SequenceNumber = a.SequenceNumber
                        INNER JOIN dbo.DimAccount da ON da.AccountNumber = a.AccountNumber
                WHERE   a.BatchKey = @BatchKey
                        AND a.AccountNumber <> 'ALL'


--SupplierContractLocation
        INSERT  INTO cc.SupplierContractLocation
                ( ContractKey ,
                  ContractLocationLevelKey ,
                  LocationKey ,
                  CreatedByWebUserKey ,
                  CreatedOnDT ,
                  LocationName ,
                  LocationDescription
                )
                SELECT  c.ContractKey ,
                        ll.LocationLevelKey ,
                        COALESCE(dag1.AccountGroup1Key, dn.NetworkKey,
                                 da.AreaKey, dr.RegionKey) ,
                        @WebUserKey ,
                        GETDATE() ,
                        COALESCE(dag1.AG1Level1, dn.NetworkId, da.Area,
                                 dr.Region) ,
                        COALESCE(dag1.AG1Level1UDVarchar1,
                                 dn.NetworkDescription, da.AreaName,
                                 dr.RegionName)
                FROM    cc.SupplierContractImportLocation l
                        INNER JOIN @NewContractKey c ON c.BatchKey = l.BatchKey
                                                        AND c.SequenceNumber = l.SequenceNumber
                        INNER JOIN #LocationLevel ll ON ll.SupplierContractImportLocationKey = l.SupplierContractImportLocationKey
                        LEFT JOIN dbo.DimAccountGroup1 dag1 ON dag1.AG1Level1 = l.LocationValue
                                                              AND ll.LocationLevelKey = 1
                        LEFT JOIN dbo.DimNetwork dn ON dn.NetworkId = l.LocationValue
                                                       AND ll.LocationLevelKey = 2
                        LEFT JOIN dbo.DimArea da ON da.Area = l.LocationValue
                                                    AND ll.LocationLevelKey = 3
                        LEFT JOIN dbo.DimRegion dr ON dr.Region = l.LocationValue
                                                      AND ll.LocationLevelKey = 4
                WHERE   l.BatchKey = @BatchKey
                        AND ll.LocationLevelKey IS NOT NULL
                        AND l.LocationValue <> 'ALL'

--SupplierContractItem
        INSERT  INTO cc.SupplierContractItem
                ( ContractKey ,
                  ItemKey ,
                  ClaimType ,          -- LPF-3952 cc.importcontract should set claimType (jed)
                  ClaimBackAmount ,
                  ClaimBackPerc ,
                  ClaimCost ,          -- LPF-3953 cc.importContract is ignoring net cost (jed)
                  LimitType ,
                  LimitValue ,
                  CreatedByWebUserKey ,
                  CreatedOnDT
                )
                SELECT DISTINCT
                        c.ContractKey ,
                        di.ItemKey ,
                        CASE WHEN i.ClaimPercent IS NOT NULL THEN 'P'
                             WHEN i.ClaimAmount IS NOT NULL THEN 'A'
                             WHEN i.NetCost IS NOT NULL THEN 'C'
                             ELSE ''
                        END AS ClaimType ,
                        i.ClaimAmount ,
                        i.ClaimPercent ,
                        i.NetCost ,
                        i.QuantityType ,
                        i.QuantityLimit ,
                        @WebUserKey ,
                        GETDATE()
                FROM    cc.SupplierContractImport i
                        INNER JOIN @NewContractKey c ON c.BatchKey = i.BatchKey
                                                        AND c.SequenceNumber = i.SequenceNumber
                        INNER JOIN dbo.DimItem di ON di.ItemUDVarChar20 = i.LevelCode    -- LPF-3910 CC-1033 - Contract Import should use Product Code not Product ID (jed)
                                                     AND di.ItemUDVarchar1 = i.PyramidCode  -- LPF-3954 cc.importcontract issue with change to product code (jed)
                WHERE   ( i.LevelType = 'Product' )
                        AND ( i.BatchKey = @BatchKey )
                        AND ( i.ContractNumber = 'NEW' )


--SupplierContractItemGroupX  --MPG
        INSERT  INTO cc.SupplierContractItemGroupX
                ( ContractKey ,
                  ItemGroupLevel ,
                  ItemGroupXKey ,
                  ClaimType ,              -- LPF-3952 cc.importcontract should set claimType (jed)
                  ClaimBackAmount ,
                  ClaimBackPerc ,
                  ClaimCost ,              -- LPF-3953 cc.importContract is ignoring net cost (jed)
                  LimitType ,
                  LimitValue ,
                  CreatedByWebUserKey ,
                  CreatedOnDT
                )
                SELECT DISTINCT
                        c.ContractKey ,
                        1 ,
                        dig1.ItemGroup1Key ,
                        CASE WHEN i.ClaimPercent IS NOT NULL THEN 'P'
                             WHEN i.ClaimAmount IS NOT NULL THEN 'A'
                             WHEN i.NetCost IS NOT NULL THEN 'C'
                             ELSE ''
                        END AS ClaimType ,      -- LPF-3952 cc.importcontract should set claimType (jed)
                        i.ClaimAmount ,
                        i.ClaimPercent ,
                        i.NetCost ,
                        i.QuantityType ,
                        i.QuantityLimit ,
                        @WebUserKey ,
                        GETDATE()
                FROM    cc.SupplierContractImport i
                        INNER JOIN @NewContractKey c ON c.BatchKey = i.BatchKey
                                                        AND c.SequenceNumber = i.SequenceNumber
                        INNER JOIN dbo.DimItemGroup1 dig1 ON dig1.IG1Level1 = i.LevelCode
                                                             AND dig1.IG1Level2 = i.PyramidCode
                WHERE   ( i.LevelType = 'MPG' )
                        AND ( i.BatchKey = @BatchKey )
                        AND ( i.ContractNumber = 'NEW' )

--SupplierContractItemGroupX  --SPG
        INSERT  INTO cc.SupplierContractItemGroupX
                ( ContractKey ,
                  ItemGroupLevel ,
                  ItemGroupXKey ,
                  ClaimType ,            -- LPF-3952 cc.importcontract should set claimType (jed)
                  ClaimBackAmount ,
                  ClaimBackPerc ,
                  ClaimCost ,            -- LPF-3953 cc.importContract is ignoring net cost
                  LimitType ,
                  LimitValue ,
                  CreatedByWebUserKey ,
                  CreatedOnDT
                )
                SELECT DISTINCT
                        c.ContractKey ,
                        3 ,
                        dig3.ItemGroup3Key ,
                        CASE WHEN i.ClaimPercent IS NOT NULL THEN 'P'
                             WHEN i.ClaimAmount IS NOT NULL THEN 'A'
                             WHEN i.NetCost IS NOT NULL THEN 'C'
                             ELSE ''
                        END AS ClaimType ,   -- LPF-3952 cc.importcontract should set claimType (jed)
                        i.ClaimAmount ,
                        i.ClaimPercent ,
                        i.NetCost ,
                        i.QuantityType ,
                        i.QuantityLimit ,
                        @WebUserKey ,
                        GETDATE()
                FROM    cc.SupplierContractImport i
                        INNER JOIN @NewContractKey c ON c.BatchKey = i.BatchKey
                                                        AND c.SequenceNumber = i.SequenceNumber
                        INNER JOIN dbo.DimItemGroup3 dig3 ON dig3.IG3Level1 = i.LevelCode
                                                             AND dig3.IG3Level4 = i.PyramidCode
                WHERE   ( i.LevelType = 'SPG' )
                        AND ( i.BatchKey = @BatchKey )
                        AND ( i.ContractNumber = 'NEW' )


--Add Contract Number
        CREATE TABLE #TempContractNumber
            (
              [ContractNumberCounter] VARCHAR(10)
            )

        DECLARE ContractNumberCursor CURSOR FAST_FORWARD
        FOR
            SELECT  ContractKey ,
                    Originator
            FROM    cc.SupplierContract sc
            WHERE   sc.BatchKey = @BatchKey
                    AND sc.ContractNumber = '000000'
				

        OPEN ContractNumberCursor

        FETCH NEXT FROM ContractNumberCursor 
INTO @ContractKey, @Originator

        WHILE ( @@FETCH_STATUS = 0 )
            BEGIN
                INSERT  INTO #TempContractNumber
                        EXEC cc.GetNextContractNumber @Originator;
                SELECT  @NewContractNumber = ContractNumberCounter
                FROM    #TempContractNumber

                UPDATE  cc.SupplierContract
                SET     ContractNumber = @NewContractNumber
                WHERE   ContractKey = @ContractKey

                FETCH NEXT FROM ContractNumberCursor 
	INTO @ContractKey, @Originator
            END

        CLOSE ContractNumberCursor
        DEALLOCATE ContractNumberCursor




--Update IncludeAllAccounts and IncludeAllLocations
        UPDATE  cc.SupplierContract
        SET     IncludeAllAccounts = 1
        WHERE   ContractKey IN (
                SELECT  sc.ContractKey
                FROM    cc.SupplierContract sc
                        LEFT JOIN cc.SupplierContractAccount sca ON sca.ContractKey = sc.ContractKey
                WHERE   sc.BatchKey = @BatchKey
                        AND sca.ContractAccountKey IS NULL )

        UPDATE  cc.SupplierContract
        SET     IncludeAllLocations = 1
        WHERE   ContractKey IN (
                SELECT  sc.ContractKey
                FROM    cc.SupplierContract sc
                        LEFT JOIN cc.SupplierContractLocation scl ON scl.ContractKey = sc.ContractKey
                WHERE   sc.BatchKey = @BatchKey
                        AND scl.ContractLocationKey IS NULL )

    END

DROP TABLE #LocationLevel

    
INSERT  INTO cc.SupplierContractLog
        ( WebUserKey ,
          ContractKey ,
          contractID ,
          ContractAction ,
          Notes ,
          vendorKey ,
          CreationDate
			  
        )
        SELECT  CreatedByWebUserKey ,
                sc.ContractKey ,
                sc.ContractID ,
                'Import Contract' ,
                'From:<NULL> To:<'
                + CAST(scs.StatusDescription AS VARCHAR(20)) + '>' ,
                sc.VendorKey ,
                GETDATE()
        FROM    cc.SupplierContract sc
                INNER JOIN @NewContractKey c ON c.BatchKey = sc.BatchKey
                                                AND c.SequenceNumber = sc.SequenceNumber
                INNER JOIN cc.SupplierContractStatus scs ON sc.StatusKey = scs.StatusKey
        WHERE   sc.BatchKey = @BatchKey

  
  
    --SELECT  SupplierContractImportKey ,
    --        RowNumber ,
    --        ErrorMessage
    --FROM    cc.SupplierContractImport
    --WHERE   BatchKey = @BatchKey
    --        AND ErrorMessage IS NOT NULL




IF OBJECT_ID('tempdb.dbo.#LocationLevel', 'U') IS NOT NULL
    DROP TABLE #LocationLevel; 
IF OBJECT_ID('tempdb.dbo.#ContractSequence', 'U') IS NOT NULL
    DROP TABLE #ContractSequence; 
IF OBJECT_ID('tempdb.dbo.#TempContractNumber', 'U') IS NOT NULL
    DROP TABLE #TempContractNumber; 

/*!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!*/
/* 

                                                                 UPDATE (AMEND) RECORD                                                                     */
--Data Validation for Ammend records.


UPDATE  i
SET     ErrorMessage = ISNULL(ErrorMessage, '')
        + 'Update Record, Invalid ContractID ' + i.PyramidCode + '; '
FROM    cc.SupplierContractImport i
WHERE   i.BatchKey = @BatchKey
        AND i.ContractNumber <> 'NEW'
        AND i.Originator + i.ContractNumber NOT IN (
        SELECT  ContractID
        FROM    cc.SupplierContract )
  
UPDATE  i
SET     ErrorMessage = ISNULL(ErrorMessage, '') + 'Invalid Start/ExpiryDate; '
FROM    cc.SupplierContractImport i
WHERE   i.BatchKey = @BatchKey
        AND ExpirationDate < StartDate
        AND i.ContractNumber <> 'NEW'
        AND i.ExpirationDate IS NOT NULL 
	

UPDATE  i
SET     ErrorMessage = ISNULL(ErrorMessage, '')
        + 'Update Record, Invalid ExpiryDate; '
FROM    cc.SupplierContractImport i
WHERE   i.BatchKey = @BatchKey
        AND ExpirationDate < '2015-01-01'
        AND i.ContractNumber <> 'NEW'
        AND i.ExpirationDate IS NOT NULL

UPDATE  i
SET     ErrorMessage = ISNULL(ErrorMessage, '')
        + 'Update Record, Invalid StartDate; '
FROM    cc.SupplierContractImport i
WHERE   i.BatchKey = @BatchKey
        AND i.StartDate < '2015-01-01'
        AND i.ContractNumber <> 'NEW'
        AND i.StartDate IS NOT NULL
 
UPDATE  i
SET     ErrorMessage = ISNULL(ErrorMessage, '')
        + 'Update Record, Invalid AuthorizedIndicator '
        + i.AuthorizedIndicator + '; '
FROM    cc.SupplierContractImport i
WHERE   i.BatchKey = @BatchKey
        AND AuthorizedIndicator NOT IN ( 'Y', 'N' )
        AND i.ContractNumber <> 'NEW'
        AND i.AuthorizedIndicator IS NOT NULL

--    UPDATE  i
--    SET     ErrorMessage = ISNULL(ErrorMessage, '')
--            + 'Update Record, Invalid PricingContractType '
--            + i.PricingContractType + '; '
--    FROM    cc.SupplierContractImport i
--    WHERE   i.BatchKey = @BatchKey
--            AND ISNULL(PricingContractType, ' ') NOT IN ( ' ', 'B', 'N' )
--            AND i.ContractNumber <> 'NEW'

--    UPDATE  i
--    SET     ErrorMessage = ISNULL(ErrorMessage, '')
--            + 'Update Record, Invalid PricingContractType '
--            + i.PricingContractType + '; '
--    FROM    cc.SupplierContractImport i
--    WHERE   i.BatchKey = @BatchKey
--            AND ISNULL(i.PricingContractID, '') <> ''
--            AND ISNULL(PricingContractType, ' ') = ' '
--            AND i.ContractNumber <> 'NEW'

--    UPDATE  i
--    SET     ErrorMessage = ISNULL(ErrorMessage, '')
--            + 'Update Record, Invalid PricingContractID '
--            + i.PricingContractID + '; '
--    FROM    cc.SupplierContractImport i
--    WHERE   i.BatchKey = @BatchKey
--            AND i.ContractNumber <> 'NEW'
--            AND ISNULL(i.PricingContractID, '') NOT IN (
--            SELECT  PricingContractKey
--            FROM    Lion.PricingContract )

	
UPDATE  i
SET     ErrorMessage = ISNULL(ErrorMessage, '')
        + 'Update Record, Invalid LevelType ' + i.LevelType + '; '
FROM    cc.SupplierContractImport i
WHERE   i.BatchKey = @BatchKey
        AND LevelType NOT IN ( 'Product', 'SPG', 'MPG', '' )
        AND ( LevelType IS NOT NULL )
        AND i.ContractNumber <> 'NEW'

UPDATE  i
SET     ErrorMessage = ISNULL(ErrorMessage, '')
        + 'Update Record, Invalid Product ' + i.LevelCode + '; '
FROM    cc.SupplierContractImport i
        LEFT JOIN dbo.DimItem di ON di.ItemUDVarchar20 = i.LevelCode   -- LPF-3910 CC-1033 Contract Import should use Product Code not Product ID (jed)
                                    AND di.ItemUDVarchar1 = i.PyramidCode  -- LPF-3954 cc.importcontract issue with change to product code (jed)
WHERE   i.LevelType = 'Product'
        AND i.BatchKey = @BatchKey
        AND di.ItemKey IS NULL
        AND i.ContractNumber <> 'NEW'
			

UPDATE  i
SET     ErrorMessage = ISNULL(ErrorMessage, '')
        + 'Update Record, Invalid MPG ' + i.LevelCode + '; '
FROM    cc.SupplierContractImport i
        LEFT JOIN dbo.DimItemGroup1 dig1 ON dig1.IG1Level1 = i.LevelCode
                                            AND dig1.IG1Level2 = i.PyramidCode
WHERE   i.LevelType = 'MPG'
        AND i.BatchKey = @BatchKey
        AND dig1.ItemGroup1Key IS NULL
        AND i.ContractNumber <> 'NEW'

UPDATE  i
SET     ErrorMessage = ISNULL(ErrorMessage, '')
        + 'Update Record, Invalid SPG ' + i.LevelCode + '; '
FROM    cc.SupplierContractImport i
        LEFT JOIN dbo.DimItemGroup3 dig3 ON dig3.IG3Level1 = i.LevelCode
                                            AND dig3.IG3Level4 = i.PyramidCode
WHERE   i.LevelType = 'SPG'
        AND i.BatchKey = @BatchKey
        AND dig3.ItemGroup3Key IS NULL
        AND i.ContractNumber <> 'NEW'

---- LPF-3947 Duplicate Product/SPG/MPG not allowed in the same sequence (jed)
UPDATE  i
SET     ErrorMessage = ISNULL(ErrorMessage, '')
        + 'Update Record, Duplicate product line ' + i.LevelCode + '; '
FROM    cc.SupplierContractImport i
WHERE   i.LevelType = 'Product'
        AND i.BatchKey = @BatchKey
        AND i.ContractNumber <> 'NEW'
        AND ( SELECT    COUNT(*)
              FROM      cc.SupplierContractImport i2
              WHERE     i2.BatchKey = @BatchKey
                        AND i2.SequenceNumber = i.SequenceNumber
                        AND i2.LevelCode = i.LevelCode
            ) > 1

---- LPF-3947 Duplicate Product/SPG/MPG not allowed in the same sequence (jed)
UPDATE  i
SET     ErrorMessage = ISNULL(ErrorMessage, '')
        + 'Update Record, Duplicate MPG line ' + i.LevelCode + '; '
FROM    cc.SupplierContractImport i
WHERE   i.LevelType = 'MPG'
        AND i.BatchKey = @BatchKey
        AND i.ContractNumber <> 'NEW'
        AND ( SELECT    COUNT(*)
              FROM      cc.SupplierContractImport i2
              WHERE     i2.BatchKey = @BatchKey
                        AND i2.SequenceNumber = i.SequenceNumber
                        AND i2.LevelCode = i.LevelCode
            ) > 1

---- LPF-3947 Duplicate Product/SPG/MPG not allowed in the same sequence (jed)
UPDATE  i
SET     ErrorMessage = ISNULL(ErrorMessage, '')
        + 'Update Record, Duplicate SPG line ' + i.LevelCode + '; '
FROM    cc.SupplierContractImport i
WHERE   i.LevelType = 'SPG'
        AND i.BatchKey = @BatchKey
        AND i.ContractNumber <> 'NEW'
        AND ( SELECT    COUNT(*)
              FROM      cc.SupplierContractImport i2
              WHERE     i2.BatchKey = @BatchKey
                        AND i2.SequenceNumber = i.SequenceNumber
                        AND i2.LevelCode = i.LevelCode
            ) > 1

   
   --Pricing Contract Checks
UPDATE  i
SET     ErrorMessage = ISNULL(ErrorMessage, '')
        + 'Invalid Pricing ContractID ' + i.PricingContractID
        + ' for Pricing Contract Type; '
FROM    cc.SupplierContractImport i
WHERE   i.PricingContractID = ''
        AND i.BatchKey = @BatchKey
        AND i.ContractNumber <> 'NEW'
        AND i.PricingContractType <> 'None'
        AND i.PricingContractType IS NOT NULL

UPDATE  i
SET     ErrorMessage = ISNULL(ErrorMessage, '')
        + 'Invalid Pricing ContractID ' + i.PricingContractID
        + ' for Pricing Contract Type; '
FROM    cc.SupplierContractImport i
WHERE   i.PricingContractID IS NULL
        AND i.BatchKey = @BatchKey
        AND i.ContractNumber <> 'NEW'
        AND i.PricingContractType <> 'None'
        AND i.PricingContractType IS NOT NULL

UPDATE  i
SET     ErrorMessage = ISNULL(ErrorMessage, '')
        + 'Invalid Pricing ContractID ' + i.PricingContractID
        + ' for Pricing Contract Type National; '
FROM    cc.SupplierContractImport i
WHERE   i.PricingContractID IS NULL
        AND i.BatchKey = @BatchKey
        AND i.ContractNumber <> 'NEW'
        AND i.PricingContractType = 'N'
        AND ISNULL(i.PricingContractID, '') NOT IN (
        SELECT DISTINCT
                PricingContractName
        FROM    Lion.PricingContract )

UPDATE  i
SET     ErrorMessage = ISNULL(ErrorMessage, '')
        + 'Invalid Pricing ContractID ' + i.PricingContractID
        + ' for Pricing Contract Type Branch; '
FROM    cc.SupplierContractImport i
WHERE   i.PricingContractID IS NULL
        AND i.BatchKey = @BatchKey
        AND i.ContractNumber <> 'NEW'
        AND i.PricingContractType = 'B'
        AND ISNULL(i.PricingContractID, '') NOT IN (
        SELECT DISTINCT
                pc.PricingContractName
        FROM    lion.PricingContract pc
                INNER JOIN cc.SupplierContract sc ON sc.ContractOwner = pc.PricingContractOwner
                                                     AND Inactive = 0
                INNER JOIN cc.SupplierContractImport i ON sc.Originator = i.Originator
                                                          AND sc.ContractNumber = i.ContractNumber
        WHERE   i.BatchKey = @BatchKey )


--LPF-5848	Contract Manager | Import - Import / Amend is able to edit otherwise locked contracts (kbs)
UPDATE  i
SET     ErrorMessage = ISNULL(ErrorMessage, '') + 'Contract ' + i.Originator
        + i.ContractNumber
        + ' is associated with non-converted accounts/owners ; '
FROM cc.SupplierContractImport i INNER JOIN 
[dbo].[DimAccountGroup1] dag1  ON i.ContractOwner = dag1.AG1Level1
INNER JOIN cc.SupplierContract sc ON sc.Originator = i.Originator
                                             AND sc.ContractNumber = i.ContractNumber
WHERE   dag1.Converted = 0
        AND i.BatchKey = @BatchKey
        AND i.ContractNumber <> 'NEW'


			

---- LPF-3952 cc.importcontract should set claimType (jed)
--    UPDATE  i
--    SET     ErrorMessage = ISNULL(ErrorMessage, '')
--            + 'Update Record, Must provide ClaimAmount, ClaimPercent or ClaimCost; '
--    FROM    cc.SupplierContractImport i
--    WHERE   i.BatchKey = @BatchKey
--            AND i.ContractNumber <> 'NEW'
--            AND i.ClaimAmount IS NULL
--            AND i.ClaimPercent IS NULL
--            AND i.NetCost IS NULL

---- LPF-3944 - cc.ImportContract should error if row sets multiple claim values or no claim values (jed)
--    UPDATE  i
--    SET     ErrorMessage = ISNULL(ErrorMessage, '')
--            + 'Update Record, Must only provide one of ClaimAmount, ClaimPercent or ClaimCost; '
--    FROM    cc.SupplierContractImport i
--    WHERE   i.BatchKey = @BatchKey
--            AND i.ContractNumber <> 'NEW'
--            AND ( CASE WHEN i.claimPercent IS NULL THEN 0
--                       ELSE 1
--                  END + CASE WHEN i.claimAmount IS NULL THEN 0
--                             ELSE 1
--                        END + CASE WHEN i.NetCost IS NULL THEN 0
--                                   ELSE 1
--                              END ) > 1

--    UPDATE  i
--    SET     ErrorMessage = ISNULL(ErrorMessage, '')
--            + 'Update Record, Invalid QuantityType ' + i.QuantityType + '; '
--    FROM    cc.SupplierContractImport i
--    WHERE   i.BatchKey = @BatchKey
--            AND i.ContractNumber <> 'NEW'
--            AND ISNULL(QuantityType, ' ') NOT IN ( ' ', 'U', 'S', 'C' )

--    UPDATE  i
--    SET     ErrorMessage = ISNULL(ErrorMessage, '')
--            + 'Update Record, Invalid QuantityLimit '
--            + CAST(i.QuantityLimit AS VARCHAR(20)) + '; '
--    FROM    cc.SupplierContractImport i
--    WHERE   i.BatchKey = @BatchKey
--            AND i.ContractNumber <> 'NEW'
--            AND ISNULL(QuantityLimit, 0) < 0

----LPF-4468 Import Contract - Enforce rule to only add/amend products for the supplier
    UPDATE  i
    SET     ErrorMessage = ISNULL(ErrorMessage, '')
            + 'Amend Record, Not valid product for vendor ' + i.LevelCode
            + '; '
    FROM    cc.SupplierContractImport i
            INNER JOIN dbo.DimVendor v ON v.VendorNumber = i.VendorNumber
            LEFT JOIN dbo.DimItem di ON v.VendorKey = di.ItemVendorKey
                                        AND di.ItemUDVarChar20 = i.LevelCode
                                        AND di.ItemUDVarchar1 = i.PyramidCode
    WHERE   di.ItemUDVarChar20 IS NULL
            AND i.BatchKey = @Batchkey
            AND i.LevelType = 'Product'
            AND i.ContractNumber <> 'NEW'

--LPF-4468 Import Contract - Enforce rule to only add/amend products for the supplier kbs (08.17.2017)
	--must have a levelType if Level code provided  
		UPDATE  i
		SET     ErrorMessage = ISNULL(ErrorMessage, '')
				+ 'Amend Record, If levelcode or leveltype not null, must have BOTH fields populated ; '
		FROM    cc.SupplierContractImport i
				LEFT JOIN dbo.DimItem di ON di.ItemUDVarchar20 = i.LevelCode   -- LPF-3910 CC-1033 Contract Import should use Product Code not Product ID (jed)
											AND di.ItemUDVarchar1 = i.PyramidCode  -- LPF-3954 cc.importcontract issue with change to product code (jed)
		WHERE ((ISNULL(levelType,'')='' AND ISNULL(levelCode,'') <> '') OR
			(ISNULL(levelType,'')<>'' AND ISNULL(levelCode,'') = '')) 
				AND i.BatchKey = @BatchKey
				AND i.ContractNumber <> 'NEW'

	 --must be a valid product for the vendor of contract
		UPDATE  i
		SET     ErrorMessage = ISNULL(ErrorMessage, '')
				+ 'Amend Record, Invalid Product ' + i.LevelCode + ' for contract supplier; '
		FROM    cc.SupplierContractImport i
				LEFT JOIN dbo.DimItem di ON di.ItemUDVarchar20 = i.LevelCode   -- LPF-3910 CC-1033 Contract Import should use Product Code not Product ID (jed)
											AND di.ItemUDVarchar1 = i.PyramidCode  -- LPF-3954 cc.importcontract issue with change to product code (jed)
				LEFT JOIN cc.SupplierContract sc ON i.ContractNumber = sc.ContractNumber
                                                     AND sc.Originator = i.Originator
													 AND sc.VendorKey = di.ItemVendorKey
													 
		WHERE   i.LevelType = 'Product'
				AND i.BatchKey = @BatchKey
				AND i.ContractNumber <> 'NEW'
				AND sc.VendorKey IS NULL
		

	
		UPDATE  i
		SET     ErrorMessage = ISNULL(ErrorMessage, '') + 'Amend Record, Invalid MPG '
				+ i.LevelCode + '; '
		FROM    cc.SupplierContractImport i
				LEFT JOIN dbo.DimItemGroup1 dig1 ON dig1.IG1Level1 = i.LevelCode
													AND dig1.IG1Level2 = i.PyramidCode
		WHERE   i.LevelType = 'MPG'
				AND i.BatchKey = @BatchKey
				AND dig1.ItemGroup1Key IS NULL
				AND i.ContractNumber <> 'NEW'
			

		UPDATE  i
		SET     ErrorMessage = ISNULL(ErrorMessage, '') + 'Amend Record, Invalid SPG '
				+ i.LevelCode + '; '
		FROM    cc.SupplierContractImport i
				LEFT JOIN dbo.DimItemGroup3 dig3 ON dig3.IG3Level1 = i.LevelCode
													AND dig3.IG3Level4 = i.PyramidCode
		WHERE   i.LevelType = 'SPG'
				AND i.BatchKey = @BatchKey
				AND dig3.ItemGroup3Key IS NULL
				AND i.ContractNumber <> 'NEW'


 --below starts the update of data  

IF ( SELECT COUNT(*)
     FROM   cc.SupplierContractImport i
     WHERE  BatchKey = @BatchKey
            AND ContractNumber <> 'NEW'
            AND ErrorMessage IS NOT NULL
   ) = 0
    BEGIN TRY
        BEGIN TRANSACTION tranUpdateRecord
		
        UPDATE  cc.SupplierContractImport
        SET     StartDate = NULL
        WHERE   ( BatchKey = @BatchKey )
                AND ( StartDate = '' )

        UPDATE  cc.SupplierContractImport
        SET     ExpirationDate = NULL
        WHERE   ( BatchKey = @BatchKey )
                AND ( ExpirationDate = '' )
         

		  DECLARE @UpdatedRowData33 TABLE
            (
              ContractKey KEY_NORMAL_TYPE ,
              vendorkey INT ,
              contractID VARCHAR(9) ,
              oldvalue VARCHAR(50) ,
              newvalue VARCHAR(50)
            )	

        UPDATE  sc
        SET     sc.ContractReference = i.ContractReference
		 OUTPUT INSERTED.ContractKey ,
                INSERTED.vendorKey ,
                INSERTED.contractID ,
                DELETED.ContractReference,
                INSERTED.ContractReference
                INTO @UpdatedRowData33
        FROM    cc.SupplierContractImport i
                INNER JOIN cc.SupplierContract sc ON i.ContractNumber = sc.ContractNumber
                                                     AND sc.Originator = i.Originator
        WHERE   ( i.BatchKey = @BatchKey )
                AND ( sc.ContractReference <> i.ContractReference )

				  INSERT  INTO cc.SupplierContractLog
                ( WebUserKey ,
                  ContractKey ,
                  contractID ,
                  ContractAction ,
                  Notes ,
                  vendorKey ,
                  CreationDate
			    )
                SELECT  @WebUserKey ,
                        u.ContractKey ,
                        u.ContractID ,
                        'Import Contract - AMEND  ' ,
                        'From ContractReference:<'
                        + u.oldvalue
                        + '> To:<'
                        + u.newvalue + '>' ,
                        u.vendorKey ,
                        GETDATE()
                FROM    @UpdatedRowData33 u

				UPDATE sc
				SET ContractSourceKey = 3
				FROM  @UpdatedRowData33 u INNER JOIN cc.SupplierContract sc ON u.contractKey = sc.ContractKey
                WHERE   (sc.ContractSourceKey = 1)
               
	    DECLARE @UpdatedRowData TABLE
            (
              ContractKey KEY_NORMAL_TYPE ,
              vendorkey INT ,
              contractID VARCHAR(9) ,
              olddate DATETIME ,
              newdate DATETIME
            )	
		  
        UPDATE  sc
        SET     sc.StartDate = i.StartDate ,
                sc.ExpirationDate = CASE WHEN sc.expirationDate < i.StartDate
                                         THEN i.StartDate
                                         ELSE sc.ExpirationDate
                                    END
        OUTPUT  INSERTED.ContractKey ,
                INSERTED.vendorKey ,
                INSERTED.contractID ,
                DELETED.StartDate ,
                INSERTED.StartDate
                INTO @UpdatedRowData
        FROM    cc.SupplierContractImport i
                INNER JOIN cc.SupplierContract sc ON i.ContractNumber = sc.ContractNumber
                                                     AND sc.Originator = i.Originator
        WHERE   ( i.BatchKey = @BatchKey )
                AND ( sc.StartDate <> i.StartDate )
                AND ( i.StartDate IS NOT NULL )


        INSERT  INTO cc.SupplierContractLog
                ( WebUserKey ,
                  ContractKey ,
                  contractID ,
                  ContractAction ,
                  Notes ,
                  vendorKey ,
                  CreationDate
			    )
                SELECT  @WebUserKey ,
                        u.ContractKey ,
                        u.ContractID ,
                        'Import Contract - AMEND  ' ,
                        'From StartDate:<'
                        + CAST(CAST(u.olddate AS DATE) AS VARCHAR(20))
                        + '> To:<'
                        + CAST(CAST(u.newdate AS DATE) AS VARCHAR(20)) + '>' ,
                        u.vendorKey ,
                        GETDATE()
                FROM    @UpdatedRowData u
           
				UPDATE sc
				SET ContractSourceKey = 3
				FROM  @UpdatedRowData u INNER JOIN cc.SupplierContract sc ON u.contractKey = sc.ContractKey
                WHERE   (sc.ContractSourceKey = 1)

		/* SPIN OFF ON START DATE*/
		
		--1) IS START DATE > EXPIRY DATA @itemLevel
        DECLARE @UpdatedRowData1 TABLE
            (
              ContractKey KEY_NORMAL_TYPE ,
              olddate DATETIME ,
              newdate DATETIME
            )
					
        UPDATE  cc.SupplierContractItem
        SET     StartDT = sc.StartDate ,
                ExpiryDT = CASE WHEN ExpiryDT < sc.StartDate THEN sc.StartDate
                                ELSE ExpiryDT
                           END
        OUTPUT  INSERTED.ContractKey ,
                INSERTED.StartDT ,
                DELETED.StartDT
                INTO @UpdatedRowData1
        FROM    cc.SupplierContractItem sci
                INNER JOIN cc.SupplierContract sc ON sci.contractKey = sc.ContractKey
                INNER JOIN @UpdatedRowData u ON sc.ContractKey = u.ContractKey
        WHERE   ( sci.StartDT IS NOT NULL )
                AND CAST(sci.StartDT AS DATE) < CAST(sc.StartDate AS DATE)

        INSERT  INTO cc.SupplierContractLog
                ( WebUserKey ,
                  ContractKey ,
                --      contractID ,
                  ContractAction ,
                  Notes ,
                --      vendorKey ,
                  CreationDate
			    )
                SELECT  @WebUserKey ,
                        u.ContractKey ,
                         --   u.ContractID ,
                        'Import Contract - AMEND  ' ,
                        'From  ITEM StartDate:<'
                        + CAST(CAST(u.olddate AS DATE) AS VARCHAR(20))
                        + '> To:<'
                        + CAST(CAST(u.newdate AS DATE) AS VARCHAR(20)) + '>' ,
                      --      u.vendorKey ,
                        GETDATE()
                FROM    @UpdatedRowData1 u
			
        UPDATE  l
        SET     ContractID = sc.ContractID ,
                vendorKey = sc.VendorKey
        FROM    cc.SupplierContractLog l
                INNER JOIN cc.SupplierContract sc ON l.ContractKey = sc.ContractKey
        WHERE   l.ContractLogKey = SCOPE_IDENTITY()

				UPDATE sc
				SET ContractSourceKey = 3
				FROM  @UpdatedRowData1 u INNER JOIN cc.SupplierContract sc ON u.contractKey = sc.ContractKey
                WHERE   (sc.ContractSourceKey = 1)
		
		--1a) IS START DATE > EXPIRY DATA @itemLevel SPG (cc.SupplierContractItemGroupX)
        DECLARE @UpdatedRowData2 TABLE
            (
              ContractKey KEY_NORMAL_TYPE ,
              olddate DATETIME ,
              newdate DATETIME
            )
			
        UPDATE  cc.SupplierContractItemGroupx
        SET     StartDT = sc.StartDate ,
                ExpiryDT = CASE WHEN ExpiryDT < sc.StartDate THEN sc.StartDate
                                ELSE ExpiryDT
                           END
        OUTPUT  INSERTED.ContractKey ,
                INSERTED.StartDT ,
                DELETED.StartDT
                INTO @UpdatedRowData2
        FROM    cc.SupplierContractItemGroupx sci
                INNER JOIN cc.SupplierContract sc ON sci.contractKey = sc.ContractKey
                INNER JOIN @UpdatedRowData u ON sc.ContractKey = u.ContractKey
        WHERE   ( sci.StartDT IS NOT NULL )
                AND CAST(sci.StartDT AS DATE) < CAST(sc.StartDate AS DATE)
            
        INSERT  INTO cc.SupplierContractLog
                ( WebUserKey ,
                  ContractKey ,
                --      contractID ,
                  ContractAction ,
                  Notes ,
                --      vendorKey ,
                  CreationDate
			    )
                SELECT  @WebUserKey ,
                        u.ContractKey ,
                         --   u.ContractID ,
                        'Import Contract - AMEND  ' ,
                        'From  SPG StartDate:<'
                        + CAST(CAST(u.olddate AS DATE) AS VARCHAR(20))
                        + '> To:<'
                        + CAST(CAST(u.newdate AS DATE) AS VARCHAR(20)) + '>' ,
                      --      u.vendorKey ,
                        GETDATE()
                FROM    @UpdatedRowData2 u
			
        UPDATE  l
        SET     ContractID = sc.ContractID ,
                vendorKey = sc.VendorKey
        FROM    cc.SupplierContractLog l
                INNER JOIN cc.SupplierContract sc ON l.ContractKey = sc.ContractKey
        WHERE   l.ContractLogKey = SCOPE_IDENTITY()
		      
				UPDATE sc
				SET ContractSourceKey = 3
				FROM @UpdatedRowData2 u INNER JOIN cc.SupplierContract sc ON u.contractKey = sc.ContractKey
                WHERE   (sc.ContractSourceKey = 1)

		--2) IS START DATE > EXPIRY DATA @Location
          
        DECLARE @UpdatedRowData3 TABLE
            (
              ContractKey KEY_NORMAL_TYPE ,
              olddate DATETIME ,
              newdate DATETIME
            )
			
        UPDATE  sci
        SET     sci.StartDate = sc.StartDate ,
                sci.ExpirationDate = CASE WHEN sci.ExpirationDate < sc.StartDate
                                          THEN sc.StartDate
                                          ELSE sci.ExpirationDate
                                     END
        OUTPUT  INSERTED.ContractKey ,
                INSERTED.StartDate ,
                DELETED.StartDate
                INTO @UpdatedRowData3
        FROM    cc.SupplierContractLocation sci
                INNER JOIN cc.SupplierContract sc ON sci.contractKey = sc.ContractKey
                INNER JOIN @UpdatedRowData u ON sc.ContractKey = u.ContractKey
        WHERE   ( sci.StartDate IS NOT NULL )
                AND CAST(sci.StartDate AS DATE) < CAST(sc.StartDate AS DATE)

        INSERT  INTO cc.SupplierContractLog
                ( WebUserKey ,
                  ContractKey ,
                --      contractID ,
                  ContractAction ,
                  Notes ,
                --      vendorKey ,
                  CreationDate
			    )
                SELECT  @WebUserKey ,
                        u.ContractKey ,
                         --   u.ContractID ,
                        'Import Contract - AMEND  ' ,
                        'From  LOCATION StartDate:<'
                        + CAST(CAST(u.olddate AS DATE) AS VARCHAR(20))
                        + '> To:<'
                        + CAST(CAST(u.newdate AS DATE) AS VARCHAR(20)) + '>' ,
                      --      u.vendorKey ,
                        GETDATE()
                FROM    @UpdatedRowData3 u
			
        UPDATE  l
        SET     ContractID = sc.ContractID ,
                vendorKey = sc.VendorKey
        FROM    cc.SupplierContractLog l
                INNER JOIN cc.SupplierContract sc ON l.ContractKey = sc.ContractKey
        WHERE   l.ContractLogKey = SCOPE_IDENTITY()
		
				UPDATE sc
				SET ContractSourceKey = 3
				FROM @UpdatedRowData3 u INNER JOIN cc.SupplierContract sc ON u.contractKey = sc.ContractKey
                WHERE   (sc.ContractSourceKey = 1)

		--3) Account
        DECLARE @UpdatedRowData4 TABLE
            (
              ContractKey KEY_NORMAL_TYPE ,
              olddate DATETIME ,
              newdate DATETIME
            )

        UPDATE  sca
        SET     sca.StartDate = sc.StartDate ,
                sca.ExpirationDate = CASE WHEN sca.ExpirationDate < sc.StartDate
                                          THEN sc.StartDate
                                          ELSE sca.ExpirationDate
                                     END
        OUTPUT  INSERTED.ContractKey ,
                INSERTED.StartDate ,
                DELETED.StartDate
                INTO @UpdatedRowData4
        FROM    cc.SupplierContractAccount sca
                INNER JOIN cc.SupplierContract sc ON sca.contractKey = sc.ContractKey
                INNER JOIN @UpdatedRowData u ON sc.ContractKey = u.ContractKey
        WHERE   ( sca.StartDate IS NOT NULL )
                AND CAST(sca.StartDate AS DATE) < CAST(sc.StartDate AS DATE)

        INSERT  INTO cc.SupplierContractLog
                ( WebUserKey ,
                  ContractKey ,
                --      contractID ,
                  ContractAction ,
                  Notes ,
                --      vendorKey ,
                  CreationDate
			    )
                SELECT  @WebUserKey ,
                        u.ContractKey ,
                         --   u.ContractID ,
                        'Import Contract - AMEND  ' ,
                        'From  ACCOUNT StartDate:<'
                        + CAST(CAST(u.olddate AS DATE) AS VARCHAR(20))
                        + '> To:<'
                        + CAST(CAST(u.newdate AS DATE) AS VARCHAR(20)) + '>' ,
                      --      u.vendorKey ,
                        GETDATE()
                FROM    @UpdatedRowData4 u
			
        UPDATE  l
        SET     ContractID = sc.ContractID ,
                vendorKey = sc.VendorKey
        FROM    cc.SupplierContractLog l
                INNER JOIN cc.SupplierContract sc ON l.ContractKey = sc.ContractKey
        WHERE   l.ContractLogKey = SCOPE_IDENTITY()

				UPDATE sc
				SET ContractSourceKey = 3
				FROM @UpdatedRowData4 u INNER JOIN cc.SupplierContract sc ON u.contractKey = sc.ContractKey
                WHERE   (sc.ContractSourceKey = 1)
	
		/* END  SPIN OFF ON START DATE*/
        DECLARE @UpdatedRowDataExpryDate TABLE
            (
              ContractKey KEY_NORMAL_TYPE ,
              vendorkey INT ,
              contractID VARCHAR(9) ,
              olddate DATETIME ,
              newdate DATETIME
            )	

        UPDATE  sc
        SET     sc.ExpirationDate = i.ExpirationDate ,
                sc.StartDate = CASE WHEN sc.StartDate > i.ExpirationDate
                                    THEN i.ExpirationDate
                                    ELSE sc.StartDate
                               END
        OUTPUT  INSERTED.ContractKey ,
                INSERTED.vendorKey ,
                INSERTED.contractID ,
                DELETED.ExpirationDate ,
                INSERTED.ExpirationDate
                INTO @UpdatedRowDataExpryDate
        FROM    cc.SupplierContractImport i
                INNER JOIN cc.SupplierContract sc ON i.ContractNumber = sc.ContractNumber
                                                     AND sc.Originator = i.Originator
        WHERE   ( i.BatchKey = @BatchKey )
                AND ( sc.ExpirationDate <> i.ExpirationDate )
                AND ( i.ExpirationDate IS NOT NULL )

        INSERT  INTO cc.SupplierContractLog
                ( WebUserKey ,
                  ContractKey ,
                  contractID ,
                  ContractAction ,
                  Notes ,
                  vendorKey ,
                  CreationDate
			    )
                SELECT  @WebUserKey ,
                        u.ContractKey ,
                        u.ContractID ,
                        'Import Contract - AMEND  ' ,
                        'From ExpiryDate:<'
                        + CAST(CAST(u.olddate AS DATE) AS VARCHAR(20))
                        + '> To:<'
                        + CAST(CAST(u.newdate AS DATE) AS VARCHAR(20)) + '>' ,
                        u.vendorKey ,
                        GETDATE()
                FROM    @UpdatedRowDataExpryDate u
                 
				UPDATE sc
				SET ContractSourceKey = 3
				FROM @UpdatedRowDataExpryDate u INNER JOIN cc.SupplierContract sc ON u.contractKey = sc.ContractKey
                WHERE   (sc.ContractSourceKey = 1)     

		/* SPIN OFF ON START DATE*/
		/*START the SPIN for Expiry Date update*/
		--4) IS START DATE > EXPIRY DATA @itemLevel
        DECLARE @UpdatedRowData5 TABLE
            (
              ContractKey KEY_NORMAL_TYPE ,
              olddate DATETIME ,
              newdate DATETIME
            )

        UPDATE  cc.SupplierContractItem
        SET     ExpiryDT = sc.ExpirationDate ,
                StartDT = CASE WHEN StartDT > sc.ExpirationDate
                               THEN sc.ExpirationDate
                               ELSE StartDT
                          END
        OUTPUT  INSERTED.ContractKey ,
                INSERTED.ExpiryDT ,
                DELETED.ExpiryDT
                INTO @UpdatedRowData5
        FROM    cc.SupplierContractItem sci
                INNER JOIN cc.SupplierContract sc ON sci.contractKey = sc.ContractKey
                INNER JOIN @UpdatedRowDataExpryDate u ON sc.ContractKey = u.ContractKey
        WHERE   ( sci.ExpiryDT IS NOT NULL )
                AND CAST(sci.ExpiryDT AS DATE) > CAST(sc.ExpirationDate AS DATE)
		
        INSERT  INTO cc.SupplierContractLog
                ( WebUserKey ,
                  ContractKey ,
                --      contractID ,
                  ContractAction ,
                  Notes ,
                --      vendorKey ,
                  CreationDate
			    )
                SELECT  @WebUserKey ,
                        u.ContractKey ,
                         --   u.ContractID ,
                        'Import Contract - AMEND  ' ,
                        'From Item ExpiryDate:<'
                        + CAST(CAST(u.olddate AS DATE) AS VARCHAR(20))
                        + '> To:<'
                        + CAST(CAST(u.newdate AS DATE) AS VARCHAR(20)) + '>' ,
                      --      u.vendorKey ,
                        GETDATE()
                FROM    @UpdatedRowData5 u
			
        UPDATE  l
        SET     ContractID = sc.ContractID ,
                vendorKey = sc.VendorKey
        FROM    cc.SupplierContractLog l
                INNER JOIN cc.SupplierContract sc ON l.ContractKey = sc.ContractKey
        WHERE   l.ContractLogKey = SCOPE_IDENTITY()
				
				UPDATE sc
				SET ContractSourceKey = 3
				FROM @UpdatedRowData5 u INNER JOIN cc.SupplierContract sc ON u.contractKey = sc.ContractKey
                WHERE   (sc.ContractSourceKey = 1)     

		--4a) IS EXPIRY DATE > EXPIRY DATA @itemLevel SPG (cc.SupplierContractItemGroupX)
        DECLARE @UpdatedRowData6 TABLE
            (
              ContractKey KEY_NORMAL_TYPE ,
              olddate DATETIME ,
              newdate DATETIME
            )

        UPDATE  cc.SupplierContractItemGroupX
        SET     ExpiryDT = sc.ExpirationDate ,
                StartDT = CASE WHEN StartDT > sc.ExpirationDate
                               THEN sc.ExpirationDate
                               ELSE StartDT
                          END
        OUTPUT  INSERTED.ContractKey ,
                INSERTED.ExpiryDT ,
                DELETED.ExpiryDT
                INTO @UpdatedRowData6
        FROM    cc.SupplierContractItemGroupX sci
                INNER JOIN cc.SupplierContract sc ON sci.contractKey = sc.ContractKey
                INNER JOIN @UpdatedRowDataExpryDate u ON sc.ContractKey = u.ContractKey
        WHERE   ( sci.ExpiryDT IS NOT NULL )
                AND CAST(sci.ExpiryDT AS DATE) > CAST(sc.ExpirationDate AS DATE)
                  
        INSERT  INTO cc.SupplierContractLog
                ( WebUserKey ,
                  ContractKey ,
                --      contractID ,
                  ContractAction ,
                  Notes ,
                --      vendorKey ,
                  CreationDate
			    )
                SELECT  @WebUserKey ,
                        u.ContractKey ,
                         --   u.ContractID ,
                        'Import Contract - AMEND  ' ,
                        'From Item SPG ExpiryDate:<'
                        + CAST(CAST(u.olddate AS DATE) AS VARCHAR(20))
                        + '> To:<'
                        + CAST(CAST(u.newdate AS DATE) AS VARCHAR(20)) + '>' ,
                      --      u.vendorKey ,
                        GETDATE()
                FROM    @UpdatedRowData6 u
			
        UPDATE  l
        SET     ContractID = sc.ContractID ,
                vendorKey = sc.VendorKey
        FROM    cc.SupplierContractLog l
                INNER JOIN cc.SupplierContract sc ON l.ContractKey = sc.ContractKey
        WHERE   l.ContractLogKey = SCOPE_IDENTITY()
				
				UPDATE sc
				SET ContractSourceKey = 3
				FROM @UpdatedRowData6 u INNER JOIN cc.SupplierContract sc ON u.contractKey = sc.ContractKey
                WHERE   (sc.ContractSourceKey = 1)     


		--5) IS EXPIRY DATE > EXPIRY DATA @Location
        DECLARE @UpdatedRowData7 TABLE
            (
              ContractKey KEY_NORMAL_TYPE ,
              olddate DATETIME ,
              newdate DATETIME
            )

        UPDATE  sci
        SET     sci.ExpirationDate = sc.ExpirationDate ,
                sci.StartDate = CASE WHEN sci.StartDate > sc.ExpirationDate
                                     THEN sc.ExpirationDate
                                     ELSE sci.StartDate
                                END
        OUTPUT  INSERTED.ContractKey ,
                INSERTED.ExpirationDate ,
                DELETED.ExpirationDate
                INTO @UpdatedRowData7
        FROM    cc.SupplierContractLocation sci
                INNER JOIN cc.SupplierContract sc ON sci.contractKey = sc.ContractKey
                INNER JOIN @UpdatedRowDataExpryDate u ON sc.ContractKey = u.ContractKey
        WHERE   ( sci.ExpirationDate IS NOT NULL )
                AND CAST(sci.ExpirationDate AS DATE) > CAST(sc.ExpirationDate AS DATE)

        INSERT  INTO cc.SupplierContractLog
                ( WebUserKey ,
                  ContractKey ,
                --      contractID ,
                  ContractAction ,
                  Notes ,
                --      vendorKey ,
                  CreationDate
			    )
                SELECT  @WebUserKey ,
                        u.ContractKey ,
                         --   u.ContractID ,
                        'Import Contract - AMEND  ' ,
                        'From Item Location ExpiryDate:<'
                        + CAST(CAST(u.olddate AS DATE) AS VARCHAR(20))
                        + '> To:<'
                        + CAST(CAST(u.newdate AS DATE) AS VARCHAR(20)) + '>' ,
                      --      u.vendorKey ,
                        GETDATE()
                FROM    @UpdatedRowData7 u
			
        UPDATE  l
        SET     ContractID = sc.ContractID ,
                vendorKey = sc.VendorKey
        FROM    cc.SupplierContractLog l
                INNER JOIN cc.SupplierContract sc ON l.ContractKey = sc.ContractKey
        WHERE   l.ContractLogKey = SCOPE_IDENTITY()

				UPDATE sc
				SET ContractSourceKey = 3
				FROM @UpdatedRowData7 u INNER JOIN cc.SupplierContract sc ON u.contractKey = sc.ContractKey
                WHERE   (sc.ContractSourceKey = 1)     

		--6) Account
        DECLARE @UpdatedRowData8 TABLE
            (
              ContractKey KEY_NORMAL_TYPE ,
              olddate DATETIME ,
              newdate DATETIME
            )

        UPDATE  sca
        SET     sca.ExpirationDate = sc.ExpirationDate ,
                sca.StartDate = CASE WHEN sca.StartDate > sc.ExpirationDate
                                     THEN sc.ExpirationDate
                                     ELSE sca.StartDate
                                END
        OUTPUT  INSERTED.ContractKey ,
                INSERTED.ExpirationDate ,
                DELETED.ExpirationDate
                INTO @UpdatedRowData8
        FROM    cc.SupplierContractAccount sca
                INNER JOIN cc.SupplierContract sc ON sca.contractKey = sc.ContractKey
                INNER JOIN @UpdatedRowDataExpryDate u ON sc.ContractKey = u.ContractKey
        WHERE   ( sca.ExpirationDate IS NOT NULL )
                AND CAST(sca.ExpirationDate AS DATE) > CAST(sc.ExpirationDate AS DATE)

        INSERT  INTO cc.SupplierContractLog
                ( WebUserKey ,
                  ContractKey ,
                --      contractID ,
                  ContractAction ,
                  Notes ,
                --      vendorKey ,
                  CreationDate
			    )
                SELECT  @WebUserKey ,
                        u.ContractKey ,
                         --   u.ContractID ,
                        'Import Contract - AMEND  ' ,
                        'From Item Account ExpiryDate:<'
                        + CAST(CAST(u.olddate AS DATE) AS VARCHAR(20))
                        + '> To:<'
                        + CAST(CAST(u.newdate AS DATE) AS VARCHAR(20)) + '>' ,
                      --      u.vendorKey ,
                        GETDATE()
                FROM    @UpdatedRowData8 u
			
        UPDATE  l
        SET     ContractID = sc.ContractID ,
                vendorKey = sc.VendorKey
        FROM    cc.SupplierContractLog l
                INNER JOIN cc.SupplierContract sc ON l.ContractKey = sc.ContractKey
        WHERE   l.ContractLogKey = SCOPE_IDENTITY()
				
				UPDATE sc
				SET ContractSourceKey = 3
				FROM @UpdatedRowData8 u INNER JOIN cc.SupplierContract sc ON u.contractKey = sc.ContractKey
                WHERE   (sc.ContractSourceKey = 1)     

		/* END  SPIN OFF ON EXPIRY DATE*/

        DECLARE @UpdatedRowData10 TABLE
            (
              ContractKey KEY_NORMAL_TYPE ,
              vendorkey INT ,
              contractID VARCHAR(9) ,
              oldValue VARCHAR(20) ,
              newValue VARCHAR(20)
            )	
  
        UPDATE  sc
        SET     sc.AuthorizedIndicator = i.AuthorizedIndicator
        OUTPUT  INSERTED.ContractKey ,
                INSERTED.vendorKey ,
                INSERTED.contractID ,
                DELETED.AuthorizedIndicator ,
                INSERTED.AuthorizedIndicator
                INTO @UpdatedRowData10
        FROM    cc.SupplierContractImport i
                INNER JOIN cc.SupplierContract sc ON i.ContractNumber = sc.ContractNumber
                                                     AND sc.Originator = i.Originator
        WHERE   ( i.BatchKey = @BatchKey )
                AND ( ISNULL(sc.AuthorizedIndicator, '') <> i.AuthorizedIndicator )
                AND ( i.AuthorizedIndicator IN ( 'Y', 'N' ) )

        INSERT  INTO cc.SupplierContractLog
                ( WebUserKey ,
                  ContractKey ,
                  contractID ,
                  ContractAction ,
                  Notes ,
                  vendorKey ,
                  CreationDate
			    )
                SELECT  @WebUserKey ,
                        u.ContractKey ,
                        u.ContractID ,
                        'Import Contract - AMEND  ' ,
                        'From Authorizatation Indicator:<'
                        + CAST(u.oldvalue AS VARCHAR(20)) + '> To:<'
                        + CAST(u.newvalue AS VARCHAR(20)) + '>' ,
                        u.vendorKey ,
                        GETDATE()
                FROM    @UpdatedRowData10 u

				UPDATE sc
				SET ContractSourceKey = 3
				FROM @UpdatedRowData10 u INNER JOIN cc.SupplierContract sc ON u.contractKey = sc.ContractKey
                WHERE   (sc.ContractSourceKey = 1)


        DECLARE @UpdatedRowData11 TABLE
            (
              ContractKey KEY_NORMAL_TYPE ,
              vendorkey INT ,
              contractID VARCHAR(9) ,
              oldValue VARCHAR(20) ,
              newValue VARCHAR(20)
            )	


        UPDATE  sc
        SET     sc.PricingContractIndicator = i.PricingContractType
        OUTPUT  INSERTED.ContractKey ,
                INSERTED.vendorKey ,
                INSERTED.contractID ,
                DELETED.PricingContractIndicator ,
                INSERTED.PricingContractIndicator
                INTO @UpdatedRowData11
        FROM    cc.SupplierContractImport i
                INNER JOIN cc.SupplierContract sc ON i.ContractNumber = sc.ContractNumber
                                                     AND sc.Originator = i.Originator
        WHERE   ( i.BatchKey = @BatchKey )
                AND ( ISNULL(sc.PricingContractIndicator, '') <> i.PricingContractType )
                AND ( i.PricingContractType IN ( ' ', 'B', 'N' ) )

        INSERT  INTO cc.SupplierContractLog
                ( WebUserKey ,
                  ContractKey ,
                  contractID ,
                  ContractAction ,
                  Notes ,
                  vendorKey ,
                  CreationDate
			    )
                SELECT  @WebUserKey ,
                        u.ContractKey ,
                        u.ContractID ,
                        'Import Contract - AMEND  ' ,
                        'From PricingContract Indicator:<'
                        + CAST(u.oldvalue AS VARCHAR(20)) + '> To:<'
                        + CAST(u.newvalue AS VARCHAR(20)) + '>' ,
                        u.vendorKey ,
                        GETDATE()
                FROM    @UpdatedRowData11 u

				UPDATE sc
				SET ContractSourceKey = 3
				FROM @UpdatedRowData11 u INNER JOIN cc.SupplierContract sc ON u.contractKey = sc.ContractKey
                WHERE   (sc.ContractSourceKey = 1)

        DECLARE @UpdatedRowData12 TABLE
            (
              ContractKey KEY_NORMAL_TYPE ,
              vendorkey INT ,
              contractID VARCHAR(9) ,
              oldValue VARCHAR(20) ,
              newValue VARCHAR(20)
            )
					
        UPDATE  sc
        SET     sc.PricingContractID = i.PricingContractID
        OUTPUT  INSERTED.ContractKey ,
                INSERTED.vendorKey ,
                INSERTED.contractID ,
                DELETED.PricingContractID ,
                INSERTED.PricingContractID
                INTO @UpdatedRowData12
        FROM    cc.SupplierContractImport i
                INNER JOIN cc.SupplierContract sc ON i.ContractNumber = sc.ContractNumber
                                                     AND sc.Originator = i.Originator
        WHERE   ( i.BatchKey = @BatchKey )
                AND ( ISNULL(sc.PricingContractID, '') <> i.PricingContractID )
                AND ( i.PricingContractID IS NOT NULL )
                AND ( i.PricingContractID IN ( SELECT   PricingContractName
                                               FROM     Lion.PricingContract ) )

        INSERT  INTO cc.SupplierContractLog
                ( WebUserKey ,
                  ContractKey ,
                  contractID ,
                  ContractAction ,
                  Notes ,
                  vendorKey ,
                  CreationDate
			    )
                SELECT  @WebUserKey ,
                        u.ContractKey ,
                        u.ContractID ,
                        'Import Contract - AMEND  ' ,
                        'From PricingContractID Indicator:<'
                        + CAST(u.oldvalue AS VARCHAR(20)) + '> To:<'
                        + CAST(u.newvalue AS VARCHAR(20)) + '>' ,
                        u.vendorKey ,
                        GETDATE()
                FROM    @UpdatedRowData12 u

				UPDATE sc
				SET ContractSourceKey = 3
				FROM @UpdatedRowData12 u INNER JOIN cc.SupplierContract sc ON u.contractKey = sc.ContractKey
                WHERE   (sc.ContractSourceKey = 1)


        DECLARE @UpdatedRowData13 TABLE
            (
              ContractKey KEY_NORMAL_TYPE ,
              vendorkey INT ,
              contractID VARCHAR(9) ,
              oldValue VARCHAR(100) ,
              newValue VARCHAR(100)
            )
	

        UPDATE  sc
        SET     sc.JobSite = i.Jobsite
        OUTPUT  INSERTED.ContractKey ,
                INSERTED.vendorKey ,
                INSERTED.contractID ,
                DELETED.JobSite ,
                INSERTED.Jobsite
                INTO @UpdatedRowData13
        FROM    cc.SupplierContractImport i
                INNER JOIN cc.SupplierContract sc ON i.ContractNumber = sc.ContractNumber
                                                     AND sc.Originator = i.Originator
        WHERE   ( i.BatchKey = @BatchKey )
                AND ( ISNULL(sc.JobSite, '') <> i.JobSite )
                AND ( i.JobSite IS NOT NULL )
                AND ( i.JobSite <> '' )
	
        INSERT  INTO cc.SupplierContractLog
                ( WebUserKey ,
                  ContractKey ,
                  contractID ,
                  ContractAction ,
                  Notes ,
                  vendorKey ,
                  CreationDate
			    )
                SELECT  @WebUserKey ,
                        u.ContractKey ,
                        u.ContractID ,
                        'Import Contract - AMEND  ' ,
                        'From Jobsite:<' + CAST(u.oldvalue AS VARCHAR(20))
                        + '> To:<' + CAST(u.newvalue AS VARCHAR(20)) + '>' ,
                        u.vendorKey ,
                        GETDATE()
                FROM    @UpdatedRowData13 u

				UPDATE sc
				SET ContractSourceKey = 3
				FROM @UpdatedRowData13 u INNER JOIN cc.SupplierContract sc ON u.contractKey = sc.ContractKey
                WHERE   (sc.ContractSourceKey = 1)

        DECLARE @UpdatedRowData14 TABLE
            (
              ContractKey KEY_NORMAL_TYPE ,
              vendorkey INT ,
              contractID VARCHAR(9) ,
              oldValue VARCHAR(100) ,
              newValue VARCHAR(100)
            )

        UPDATE  sc
        SET     sc.Developer = i.Developer
        OUTPUT  INSERTED.ContractKey ,
                INSERTED.vendorKey ,
                INSERTED.contractID ,
                DELETED.Developer ,
                INSERTED.Developer
                INTO @UpdatedRowData14
        FROM    cc.SupplierContractImport i
                INNER JOIN cc.SupplierContract sc ON i.ContractNumber = sc.ContractNumber
                                                     AND sc.Originator = i.Originator
        WHERE   ( i.BatchKey = @BatchKey )
                AND ( ISNULL(sc.Developer, '') <> i.Developer )
                AND ( i.Developer IS NOT NULL )
                AND ( i.Developer <> '' )

        INSERT  INTO cc.SupplierContractLog
                ( WebUserKey ,
                  ContractKey ,
                  contractID ,
                  ContractAction ,
                  Notes ,
                  vendorKey ,
                  CreationDate
			    )
                SELECT  @WebUserKey ,
                        u.ContractKey ,
                        u.ContractID ,
                        'Import Contract - AMEND  ' ,
                        'From Developer:<' + ISNULL(u.oldvalue,'NULL') 
                        + '> To:<' + ISNULL(u.newvalue,'NULL') + '>' ,
                        u.vendorKey ,
                        GETDATE()
                FROM    @UpdatedRowData14 u
				
				UPDATE sc
				SET ContractSourceKey = 3
				FROM @UpdatedRowData14 u INNER JOIN cc.SupplierContract sc ON u.contractKey = sc.ContractKey
                WHERE   (sc.ContractSourceKey = 1)


        DECLARE @UpdatedRowData15 TABLE
            (
              ContractKey KEY_NORMAL_TYPE ,
              vendorkey INT ,
              contractID VARCHAR(9) ,
              oldValue VARCHAR(100) ,
              newValue VARCHAR(100)
            )



        UPDATE  sc
        SET     sc.ContactName = i.ContactName
        OUTPUT  INSERTED.ContractKey ,
                INSERTED.vendorKey ,
                INSERTED.contractID ,
                DELETED.ContactName ,
                INSERTED.ContactName
                INTO @UpdatedRowData15
        FROM    cc.SupplierContractImport i
                INNER JOIN cc.SupplierContract sc ON i.ContractNumber = sc.ContractNumber
                                                     AND sc.Originator = i.Originator
        WHERE   ( i.BatchKey = @BatchKey )
                AND ( ISNULL(sc.ContactName, '') <> i.ContactName )
                AND ( i.ContactName IS NOT NULL )
                AND ( i.ContactName <> '' )
			
        INSERT  INTO cc.SupplierContractLog
                ( WebUserKey ,
                  ContractKey ,
                  contractID ,
                  ContractAction ,
                  Notes ,
                  vendorKey ,
                  CreationDate
			    )
                SELECT  @WebUserKey ,
                        u.ContractKey ,
                        u.ContractID ,
                        'Import Contract - AMEND  ' ,
                        'From Contact Name:<'
                        + CAST(u.oldvalue AS VARCHAR(20)) + '> To:<'
                        + CAST(u.newvalue AS VARCHAR(20)) + '>' ,
                        u.vendorKey ,
                        GETDATE()
                FROM    @UpdatedRowData15 u

				UPDATE sc
				SET ContractSourceKey = 3
				FROM @UpdatedRowData15 u INNER JOIN cc.SupplierContract sc ON u.contractKey = sc.ContractKey
                WHERE   (sc.ContractSourceKey = 1)

          
		  
        DECLARE @UpdatedRowData16 TABLE
            (
              ContractKey KEY_NORMAL_TYPE ,
              vendorkey INT ,
              contractID VARCHAR(9) ,
              oldValue VARCHAR(100) ,
              newValue VARCHAR(100)
            )

        UPDATE  sc
        SET     sc.ContactEmail = i.ContactEmail
        OUTPUT  INSERTED.ContractKey ,
                INSERTED.vendorKey ,
                INSERTED.contractID ,
                DELETED.ContactEmail ,
                INSERTED.ContactEmail
                INTO @UpdatedRowData16
        FROM    cc.SupplierContractImport i
                INNER JOIN cc.SupplierContract sc ON i.ContractNumber = sc.ContractNumber
                                                     AND sc.Originator = i.Originator
        WHERE   ( i.BatchKey = @BatchKey )
                AND ( ISNULL(sc.ContactEmail, '') <> i.ContactEmail )
                AND ( i.ContactEmail IS NOT NULL )
                AND ( i.ContactEmail <> '' )

        INSERT  INTO cc.SupplierContractLog
                ( WebUserKey ,
                  ContractKey ,
                  contractID ,
                  ContractAction ,
                  Notes ,
                  vendorKey ,
                  CreationDate
			    )
                SELECT  @WebUserKey ,
                        u.ContractKey ,
                        u.ContractID ,
                        'Import Contract - AMEND  ' ,
                        'From Contact Email:<'
                        + CAST(u.oldvalue AS VARCHAR(20)) + '> To:<'
                        + CAST(u.newvalue AS VARCHAR(20)) + '>' ,
                        u.vendorKey ,
                        GETDATE()
                FROM    @UpdatedRowData16 u

				UPDATE sc
				SET ContractSourceKey = 3
				FROM @UpdatedRowData16 u INNER JOIN cc.SupplierContract sc ON u.contractKey = sc.ContractKey
                WHERE   (sc.ContractSourceKey = 1)



/*
									Level = Product and NOT already in the SupplierContractItem
*/
        DECLARE @UpdatedRowData17 TABLE
            (
              ContractKey KEY_NORMAL_TYPE ,
              itemkey INT
            )
		   
        INSERT  INTO cc.SupplierContractItem
                ( ContractKey ,
                  ItemKey ,
                  ClaimType ,
                  ClaimBackAmount ,
                  ClaimBackPerc ,
                  ClaimCost ,
                  LimitType ,
                  LimitValue ,
                  CreatedByWebUserKey ,
                  CreatedOnDT
                )
        OUTPUT  Inserted.contractKey ,
                Inserted.ItemKey
                INTO @UpdatedRowData17
                SELECT DISTINCT
                        sc.ContractKey ,
                        di.ItemKey ,
                        CASE WHEN i.ClaimPercent IS NOT NULL THEN 'P'
                             WHEN i.ClaimAmount IS NOT NULL THEN 'A'
                             WHEN i.NetCost IS NOT NULL THEN 'C'
                             ELSE ''
                        END AS ClaimType ,
                        i.ClaimAmount ,
                        i.ClaimPercent ,
                        i.NetCost ,
                        i.QuantityType ,
                        i.QuantityLimit ,
                        @WebUserKey ,
                        GETDATE()
                FROM    cc.SupplierContractImport i
                        INNER JOIN cc.SupplierContract sc ON i.ContractNumber = sc.ContractNumber
                                                             AND sc.Originator = i.Originator
                        INNER JOIN dbo.DimItem di ON di.ItemUDVarChar20 = i.LevelCode
                                                     AND di.ItemUDVarchar1 = i.PyramidCode
                WHERE   ( i.LevelType = 'Product' )
                        AND ( i.BatchKey = @BatchKey )
                        AND ( i.ContractNumber <> 'NEW' )
                        AND i.SupplierContractImportKey IN (
                        SELECT  imp.SupplierContractImportKey
                        FROM    cc.SupplierContractImport imp
                                INNER JOIN cc.SupplierContract sc ON imp.ContractNumber = sc.ContractNumber
                                                              AND sc.Originator = imp.Originator
                        WHERE   imp.BatchKey = @BatchKey
                                AND imp.ContractNumber <> 'NEW'
                                AND imp.LevelType = 'Product'
                                AND imp.LevelCode IN (
                                SELECT  di.ItemUDVarChar20
                                FROM    cc.SupplierContractImport i
                                        INNER JOIN dbo.DimItem di ON di.ItemUDVarChar20 = i.LevelCode
                                                              AND di.ItemUDVarchar1 = i.PyramidCode
                                                              AND i.BatchKey = @BatchKey ) )
                        AND CAST(sc.ContractKey AS VARCHAR(20)) + '|'
                        + CAST(di.ItemKey AS VARCHAR(20)) NOT IN (
                        SELECT  CAST(ContractKey AS VARCHAR(20)) + '|'
                                + CAST(ItemKey AS VARCHAR(20))
                        FROM    cc.SupplierContractItem )

						
        INSERT  INTO cc.SupplierContractLog
                ( WebUserKey ,
                  ContractKey ,
                  ContractAction ,
                  Notes ,
                  CreationDate
								
                )
                SELECT  @WebUserKey ,
                        u.ContractKey ,
                        'Import Contract - AMEND  ' ,
                        'Insert New Product :<'
                        + CAST(di.ProductCode AS VARCHAR(20)) + '>' ,
                        GETDATE()
                FROM    @UpdatedRowData17 u
				INNER JOIN lion.vwItem di ON u.itemkey = di.ItemKey 
						
        UPDATE  l
        SET     ContractID = sc.ContractID ,
                vendorKey = sc.VendorKey
        FROM    cc.SupplierContractLog l
                INNER JOIN cc.SupplierContract sc ON l.ContractKey = sc.ContractKey
        WHERE   l.ContractLogKey = SCOPE_IDENTITY()

				UPDATE sc
				SET ContractSourceKey = 3
				FROM @UpdatedRowData17 u INNER JOIN cc.SupplierContract sc ON u.contractKey = sc.ContractKey
                WHERE   (sc.ContractSourceKey = 1)

/*
							
							
							--update logging
							UPDATE l
							SET ContractID = sc.ContractID,
							vendorKey = sc.VendorKey
							FROM cc.SupplierContractLog l INNER JOIN cc.SupplierContract sc ON
							l.ContractKey = sc.ContractKey 
							WHERE  l.ContractLogKey = SCOPE_IDENTITY()
							
							
							
		Level = Product and ALREADY in the SupplierContractItem, UPDATE
*/
        DECLARE @UpdatedRowData18 TABLE
            (
              ContractKey KEY_NORMAL_TYPE ,
              itemkey INT ,
              claimTypeNew VARCHAR(1) ,
              ClaimTypeOld VARCHAR(1) ,
              ClaimBackAmountNew MONEY ,
              ClaimBackAmountOld MONEY ,
              ClaimBackPerNew DECIMAL(9,4) ,
              ClaimBackPerOld DECIMAL(9,4) ,
              ClaimCostNew MONEY ,
              ClaimCostOld MONEY ,
              LimitTypeNew VARCHAR(1) ,
              LimitTypeOld VARCHAR(1) ,
              LimitValueNew DECIMAL(36,4) ,
              LimitValueOld DECIMAL(36,4)
            )

        UPDATE  cc.SupplierContractItem
        SET     claimType = CASE WHEN i.ClaimPercent IS NOT NULL THEN 'P'
                                 WHEN i.ClaimAmount IS NOT NULL THEN 'A'
                                 WHEN i.NetCost IS NOT NULL THEN 'C'
                                 ELSE ''
                            END ,
                ClaimBackAmount = i.ClaimAmount ,
                ClaimBackPerc = i.ClaimPercent ,
                ClaimCost = i.netcost ,
                LimitType = i.QuantityType ,
                LimitValue = i.QuantityLimit ,
                ModifiedByWebUserKey = @WebUserKey ,
                ModifiedOnDT = GETDATE()
        OUTPUT  Inserted.ContractKey ,
                Inserted.ItemKey ,
                Inserted.ClaimType ,
                Deleted.ClaimType ,
                Inserted.ClaimBackAmount ,
                Deleted.ClaimBackAmount ,
                Inserted.ClaimBackPerc ,
                Deleted.ClaimBackPerc ,
                Inserted.ClaimCost ,
                Deleted.ClaimCost ,
                Inserted.LimitType ,
                Deleted.LimitType ,
                Inserted.LimitValue ,
                Deleted.LimitValue
                INTO @UpdatedRowData18
        FROM    cc.SupplierContractImport i
                INNER JOIN cc.SupplierContract sc ON i.ContractNumber = sc.ContractNumber
                                                     AND sc.Originator = i.Originator
                INNER JOIN dbo.DimItem di ON di.ItemUDVarChar20 = i.LevelCode
                                             AND di.ItemUDVarchar1 = i.PyramidCode
                INNER JOIN cc.SupplierContractItem sci ON sc.ContractKey = sci.ContractKey
                                                          AND sci.ItemKey = di.ItemKey
        WHERE   ( i.LevelType = 'Product' )
                AND ( i.BatchKey = @BatchKey )
                AND ( i.ContractNumber <> 'NEW' )
                AND i.SupplierContractImportKey IN (
                SELECT  imp.SupplierContractImportKey
                FROM    cc.SupplierContractImport imp
                        INNER JOIN cc.SupplierContract sc ON imp.ContractNumber = sc.ContractNumber
                                                             AND sc.Originator = imp.Originator
                WHERE   imp.BatchKey = @BatchKey
                        AND imp.ContractNumber <> 'NEW'
                        AND imp.LevelType = 'Product'
                        AND imp.LevelCode IN (
                        SELECT  di.ItemUDVarChar20
                        FROM    cc.SupplierContractImport i
                                INNER JOIN dbo.DimItem di ON di.ItemUDVarChar20 = i.LevelCode
                                                             AND di.ItemUDVarchar1 = i.PyramidCode
                                                             AND i.BatchKey = @BatchKey ) )
                AND CAST(sc.ContractKey AS VARCHAR(20)) + '|'
                + CAST(di.ItemKey AS VARCHAR(20)) IN (
                SELECT  CAST(ContractKey AS VARCHAR(20)) + '|'
                        + CAST(ItemKey AS VARCHAR(20))
                FROM    cc.SupplierContractItem )

        SET @rowCheck = 0
					/*LOG SECTION FOR UPDATE PRODUCT*/
					--ClaimType
        INSERT  INTO cc.SupplierContractLog
                ( WebUserKey ,
                  ContractKey ,
                  ContractAction ,
                  Notes ,
                  CreationDate
								
                )
                SELECT  @WebUserKey ,
                        u.ContractKey ,
                        'Import Contract - AMEND  ' ,
                        'Update Product :<' + CAST(u.itemkey AS VARCHAR(20))
                        + '>; claimType <' + u.ClaimTypeOld + '> To <'
                        + u.claimTypeNew + '>' ,
                        GETDATE()
                FROM    @UpdatedRowData18 u
                WHERE   ISNULL(u.ClaimTypeOld, '') <> ISNULL(u.claimTypeNew,
                                                             '')
        SELECT  @rowCheck = @@ROWCOUNT
         IF @rowCheck > 0
            BEGIN
                UPDATE  l
                SET     ContractID = sc.ContractID ,
                        vendorKey = sc.VendorKey
                FROM    cc.SupplierContractLog l
                        INNER JOIN cc.SupplierContract sc ON l.ContractKey = sc.ContractKey
                WHERE   l.ContractLogKey = SCOPE_IDENTITY()
            END
        SET @rowCheck = 0

				UPDATE sc
				SET ContractSourceKey = 3
				FROM @UpdatedRowData18 u INNER JOIN cc.SupplierContract sc ON u.contractKey = sc.ContractKey
                WHERE   (sc.ContractSourceKey = 1)
							--ClaimBackAmount
        INSERT  INTO cc.SupplierContractLog
                ( WebUserKey ,
                  ContractKey ,
                  ContractAction ,
                  Notes ,
                  CreationDate
								
                )
                SELECT  @WebUserKey ,
                        u.ContractKey ,
                        'Import Contract - AMEND  ' ,
                        'Update Product :<' + CAST(u.itemkey AS VARCHAR(20))
                        + '>; ClaimBackAmount <'
                        + CAST(u.ClaimBackAmountOld AS VARCHAR(20)) + '> To <'
                        + CAST(u.ClaimBackAmountNew AS VARCHAR(20)) + '>' ,
                        GETDATE()
                FROM    @UpdatedRowData18 u
                WHERE   u.ClaimBackAmountOld <> u.ClaimBackAmountNew
        SELECT  @rowCheck = @@ROWCOUNT
        IF @rowCheck > 0
            BEGIN
                UPDATE  l
                SET     ContractID = sc.ContractID ,
                        vendorKey = sc.VendorKey
                FROM    cc.SupplierContractLog l
                        INNER JOIN cc.SupplierContract sc ON l.ContractKey = sc.ContractKey
                WHERE   l.ContractLogKey = SCOPE_IDENTITY()
            END
        SET @rowCheck = 0
							--ClaimBackPerc
        INSERT  INTO cc.SupplierContractLog
                ( WebUserKey ,
                  ContractKey ,
                  ContractAction ,
                  Notes ,
                  CreationDate
								
                )
                SELECT  @WebUserKey ,
                        u.ContractKey ,
                        'Import Contract - AMEND  ' ,
                        'Update Product :<' + CAST(u.itemkey AS VARCHAR(20))
                        + '>; ClaimBackPerc <'
                        + CAST(u.ClaimBackPerOld AS VARCHAR(20)) + '> To <'
                        + CAST(u.ClaimBackPerNew AS VARCHAR(20)) + '>' ,
                        GETDATE()
                FROM    @UpdatedRowData18 u
                WHERE   u.ClaimBackPerOld <> u.ClaimBackPerNew

        SELECT  @rowCheck = @@ROWCOUNT
        IF @rowCheck > 0
            BEGIN
                UPDATE  l
                SET     ContractID = sc.ContractID ,
                        vendorKey = sc.VendorKey
                FROM    cc.SupplierContractLog l
                        INNER JOIN cc.SupplierContract sc ON l.ContractKey = sc.ContractKey
                WHERE   l.ContractLogKey = SCOPE_IDENTITY()
            END
        SET @rowCheck = 0
							--ClaimCost
        INSERT  INTO cc.SupplierContractLog
                ( WebUserKey ,
                  ContractKey ,
                  ContractAction ,
                  Notes ,
                  CreationDate
								
                )
                SELECT  @WebUserKey ,
                        u.ContractKey ,
                        'Import Contract - AMEND  ' ,
                        'Update Product :<' + CAST(u.itemkey AS VARCHAR(20))
                        + '>; ClaimCost <'
                        + CAST(u.ClaimCostOld AS VARCHAR(20)) + '> To <'
                        + CAST(u.ClaimCostNew AS VARCHAR(20)) + '>' ,
                        GETDATE()
                FROM    @UpdatedRowData18 u
                WHERE   u.ClaimCostOld <> u.ClaimCostNew

        SELECT  @rowCheck = @@ROWCOUNT
        IF @rowCheck > 0
            BEGIN
                UPDATE  l
                SET     ContractID = sc.ContractID ,
                        vendorKey = sc.VendorKey
                FROM    cc.SupplierContractLog l
                        INNER JOIN cc.SupplierContract sc ON l.ContractKey = sc.ContractKey
                WHERE   l.ContractLogKey = SCOPE_IDENTITY()
            END
				UPDATE sc
				SET ContractSourceKey = 3
				FROM @UpdatedRowData18 u INNER JOIN cc.SupplierContract sc ON u.contractKey = sc.ContractKey
                WHERE   (sc.ContractSourceKey = 1)

        SET @rowCheck = 0
							--LimitType
        INSERT  INTO cc.SupplierContractLog
                ( WebUserKey ,
                  ContractKey ,
                  ContractAction ,
                  Notes ,
                  CreationDate
								
                )
                SELECT  @WebUserKey ,
                        u.ContractKey ,
                        'Import Contract - AMEND  ' ,
                        'Update Product :<' + CAST(u.itemkey AS VARCHAR(20))
                        + '>; LimitType <' + u.LimitTypeOld + '> To <'
                        + u.LimitTypeNew + '>' ,
                        GETDATE()
                FROM    @UpdatedRowData18 u
                WHERE   ISNULL(u.LimitTypeOld, '') <> ISNULL(u.LimitTypeNew,
                                                             '')
        SELECT  @rowCheck = @@ROWCOUNT
        IF @rowCheck > 0
            BEGIN
                UPDATE  l
                SET     ContractID = sc.ContractID ,
                        vendorKey = sc.VendorKey
                FROM    cc.SupplierContractLog l
                        INNER JOIN cc.SupplierContract sc ON l.ContractKey = sc.ContractKey
                WHERE   l.ContractLogKey = SCOPE_IDENTITY()
            END
        SET @rowCheck = 0
							--LimitValue
        INSERT  INTO cc.SupplierContractLog
                ( WebUserKey ,
                  ContractKey ,
                  ContractAction ,
                  Notes ,
                  CreationDate
								
                )
                SELECT  @WebUserKey ,
                        u.ContractKey ,
                        'Import Contract - AMEND  ' ,
                        'Update Product :<' + CAST(u.itemkey AS VARCHAR(20))
                        + '>; LimitValue <'
                        + CAST(u.LimitValueOld AS VARCHAR(20)) + '> To <'
                        + CAST(u.LimitValueNew AS VARCHAR(20)) + '>' ,
                        GETDATE()
                FROM    @UpdatedRowData18 u
                WHERE   u.LimitValueOld <> u.LimitValueNew

        SELECT  @rowCheck = @@ROWCOUNT
        IF @rowCheck > 0
            BEGIN
                UPDATE  l
                SET     ContractID = sc.ContractID ,
                        vendorKey = sc.VendorKey
                FROM    cc.SupplierContractLog l
                        INNER JOIN cc.SupplierContract sc ON l.ContractKey = sc.ContractKey
                WHERE   l.ContractLogKey = SCOPE_IDENTITY()
            END
				UPDATE sc
				SET ContractSourceKey = 3
				FROM @UpdatedRowData18 u INNER JOIN cc.SupplierContract sc ON u.contractKey = sc.ContractKey
                WHERE   (sc.ContractSourceKey = 1)

        SET @rowCheck = 0

/*
				Level = MPG and NOT already in the SupplierContractItem
*/
        DECLARE @UpdatedRowData21 TABLE
            (
              ContractKey KEY_NORMAL_TYPE ,
              itemGroupXkey INT
            )

        INSERT  INTO cc.SupplierContractItemGroupX
                ( ContractKey ,
                  ItemGroupLevel ,
                  ItemGroupXKey ,
                  ClaimType ,
                  ClaimBackAmount ,
                  ClaimBackPerc ,
                  ClaimCost ,
                  LimitType ,
                  LimitValue ,
                  CreatedByWebUserKey ,
                  CreatedOnDT
                )
        OUTPUT  Inserted.contractKey ,
                Inserted.ItemGroupXKey
                INTO @UpdatedRowData21
                SELECT DISTINCT
                        sc.ContractKey ,
                        1 ,
                        dig1.ItemGroup1Key ,
                        CASE WHEN i.ClaimPercent IS NOT NULL THEN 'P'
                             WHEN i.ClaimAmount IS NOT NULL THEN 'A'
                             WHEN i.NetCost IS NOT NULL THEN 'C'
                             ELSE ''
                        END AS ClaimType ,
                        i.ClaimAmount ,
                        i.ClaimPercent ,
                        i.NetCost ,
                        i.QuantityType ,
                        i.QuantityLimit ,
                        @WebUserKey ,
                        GETDATE()
                FROM    cc.SupplierContractImport i
                        INNER JOIN cc.SupplierContract sc ON i.ContractNumber = sc.ContractNumber
                                                             AND sc.Originator = i.Originator
                        INNER JOIN dbo.DimItemGroup1 dig1 ON dig1.IG1Level1 = i.LevelCode
                                                             AND dig1.IG1Level2 = i.PyramidCode
                WHERE   ( i.LevelType = 'MPG' )
                        AND ( i.BatchKey = @BatchKey )
                        AND ( i.ContractNumber <> 'NEW' )
                        AND i.SupplierContractImportKey IN (
                        SELECT  imp.SupplierContractImportKey
                        FROM    cc.SupplierContractImport imp
                                INNER JOIN cc.SupplierContract sc ON imp.ContractNumber = sc.ContractNumber
                                                              AND sc.Originator = imp.Originator
                        WHERE   imp.BatchKey = @BatchKey
                                AND imp.ContractNumber <> 'NEW'
                                AND imp.LevelType = 'MPG'
                                AND i.LevelCode IN (
                                SELECT  dig1.IG1Level1
                                FROM    cc.SupplierContractImport i
                                        INNER JOIN dbo.DimItemGroup1 dig1 ON dig1.IG1Level1 = i.LevelCode
                                                              AND dig1.IG1Level2 = i.PyramidCode
                                WHERE   ( i.LevelType = 'MPG' )
                                        AND ( i.BatchKey = @BatchKey )
                                        AND ( i.ContractNumber <> 'NEW' ) ) )
                        AND CAST(sc.ContractKey AS VARCHAR(20)) + '|'
                        + CAST(dig1.ItemGroup1Key AS VARCHAR(20)) NOT IN (
                        SELECT  CAST(ContractKey AS VARCHAR(20)) + '|'
                                + CAST(ItemGroupXKey AS VARCHAR(20))
                        FROM    [cc].[SupplierContractItemGroupX] )

        INSERT  INTO cc.SupplierContractLog
                ( WebUserKey ,
                  ContractKey ,
                  ContractAction ,
                  Notes ,
                  CreationDate
								
                )
                SELECT  @WebUserKey ,
                        u.ContractKey ,
                        'Import Contract - AMEND  ' ,
                        'Insert New Product MPG :<'
                        + CAST(u.itemGroupXkey AS VARCHAR(20)) + '>' ,
                        GETDATE()
                FROM    @UpdatedRowData21 u
						
        UPDATE  l
        SET     ContractID = sc.ContractID ,
                vendorKey = sc.VendorKey
        FROM    cc.SupplierContractLog l
                INNER JOIN cc.SupplierContract sc ON l.ContractKey = sc.ContractKey
        WHERE   l.ContractLogKey = SCOPE_IDENTITY()

				UPDATE sc
				SET ContractSourceKey = 3
				FROM @UpdatedRowData21 u INNER JOIN cc.SupplierContract sc ON u.contractKey = sc.ContractKey
                WHERE   (sc.ContractSourceKey = 1)



/*
				Level = MPG and ALREADY in the SupplierContractItem, UPDATE
*/
        DECLARE @UpdatedRowData19 TABLE
            (
              ContractKey KEY_NORMAL_TYPE ,
              itemGroupXkey INT ,
              claimTypeNew VARCHAR(1) ,
              ClaimTypeOld VARCHAR(1) ,
              ClaimBackAmountNew MONEY ,
              ClaimBackAmountOld MONEY ,
              ClaimBackPerNew DECIMAL(9,4) ,
              ClaimBackPerOld DECIMAL(9,4) ,
              ClaimCostNew MONEY ,
              ClaimCostOld MONEY ,
              LimitTypeNew VARCHAR(1) ,
              LimitTypeOld VARCHAR(1) ,
              LimitValueNew DECIMAL(36,4) ,
              LimitValueOld DECIMAL(36,4)
            )
        UPDATE  cc.SupplierContractItemGroupX
        SET     claimType = CASE WHEN i.ClaimPercent IS NOT NULL THEN 'P'
                                 WHEN i.ClaimAmount IS NOT NULL THEN 'A'
                                 WHEN i.NetCost IS NOT NULL THEN 'C'
                                 ELSE ''
                            END ,
                ClaimBackAmount = i.ClaimAmount ,
                ClaimBackPerc = i.ClaimPercent ,
                ClaimCost = i.netcost ,
                LimitType = i.QuantityType ,
                LimitValue = i.QuantityLimit ,
                ModifiedByWebUserKey = @WebUserKey ,
                ModifiedOnDT = GETDATE()
        OUTPUT  Inserted.ContractKey ,
                Inserted.ItemGroupXKey ,
                Inserted.ClaimType ,
                Deleted.ClaimType ,
                Inserted.ClaimBackAmount ,
                Deleted.ClaimBackAmount ,
                Inserted.ClaimBackPerc ,
                Deleted.ClaimBackPerc ,
                Inserted.ClaimCost ,
                Deleted.ClaimCost ,
                Inserted.LimitType ,
                Deleted.LimitType ,
                Inserted.LimitValue ,
                Deleted.LimitValue
                INTO @UpdatedRowData19
        FROM    cc.SupplierContractImport i
                INNER JOIN cc.SupplierContract sc ON i.ContractNumber = sc.ContractNumber
                                                     AND sc.Originator = i.Originator
                INNER JOIN dbo.DimItemGroup1 dig1 ON dig1.IG1Level1 = i.LevelCode
                                                     AND dig1.IG1Level2 = i.PyramidCode
                INNER JOIN cc.SupplierContractItemGroupX scig ON sc.ContractKey = scig.ContractKey
                                                              AND scig.ItemGroupXKey = dig1.ItemGroup1Key
        WHERE   ( i.LevelType = 'MPG' )
                AND ( i.BatchKey = @BatchKey )
                AND ( i.ContractNumber <> 'NEW' )
                AND i.SupplierContractImportKey IN (
                SELECT  imp.SupplierContractImportKey
                FROM    cc.SupplierContractImport imp
                        INNER JOIN cc.SupplierContract sc ON imp.ContractNumber = sc.ContractNumber
                                                             AND sc.Originator = imp.Originator
                WHERE   imp.BatchKey = @BatchKey
                        AND imp.ContractNumber <> 'NEW'
                        AND imp.LevelType = 'MPG'
                        AND i.LevelCode IN (
                        SELECT  dig1.IG1Level1
                        FROM    cc.SupplierContractImport i
                                INNER JOIN dbo.DimItemGroup1 dig1 ON dig1.IG1Level1 = i.LevelCode
                                                              AND dig1.IG1Level2 = i.PyramidCode
                        WHERE   ( i.LevelType = 'MPG' )
                                AND ( i.BatchKey = @BatchKey )
                                AND ( i.ContractNumber <> 'NEW' ) ) )
                AND CAST(sc.ContractKey AS VARCHAR(20)) + '|'
                + CAST(dig1.ItemGroup1Key AS VARCHAR(20)) IN (
                SELECT  CAST(ContractKey AS VARCHAR(20)) + '|'
                        + CAST(ItemGroupXKey AS VARCHAR(20))
                FROM    [cc].[SupplierContractItemGroupX] )
	--		LOG SECTION FOR MPG UPDATE!!
			--ClaimType
        INSERT  INTO cc.SupplierContractLog
                ( WebUserKey ,
                  ContractKey ,
                  ContractAction ,
                  Notes ,
                  CreationDate
								
                )
                SELECT  @WebUserKey ,
                        u.ContractKey ,
                        'Import Contract - AMEND  ' ,
                        'Update Product MPG :<'
                        + CAST(u.itemGroupXkey AS VARCHAR(20))
                        + '>; claimType <' + u.ClaimTypeOld + '> To <'
                        + u.claimTypeNew + '>' ,
                        GETDATE()
                FROM    @UpdatedRowData19 u
                WHERE   ISNULL(u.ClaimTypeOld, '') <> ISNULL(u.claimTypeNew,'')

				UPDATE sc
				SET ContractSourceKey = 3
				FROM @UpdatedRowData19 u INNER JOIN cc.SupplierContract sc ON u.contractKey = sc.ContractKey
                WHERE   (sc.ContractSourceKey = 1)

        SELECT  @rowCheck = @@ROWCOUNT
        IF @rowCheck > 0
            BEGIN
                UPDATE  l
                SET     ContractID = sc.ContractID ,
                        vendorKey = sc.VendorKey
                FROM    cc.SupplierContractLog l
                        INNER JOIN cc.SupplierContract sc ON l.ContractKey = sc.ContractKey
                WHERE   l.ContractLogKey = SCOPE_IDENTITY()
            END
        SET @rowCheck = 0
							--ClaimBackAmount
        INSERT  INTO cc.SupplierContractLog
                ( WebUserKey ,
                  ContractKey ,
                  ContractAction ,
                  Notes ,
                  CreationDate
								
                )
                SELECT  @WebUserKey ,
                        u.ContractKey ,
                        'Import Contract - AMEND  ' ,
                        'Update Product MPG:<'
                        + CAST(u.itemGroupXkey AS VARCHAR(20))
                        + '>; ClaimBackAmount <'
                        + CAST(u.ClaimBackAmountOld AS VARCHAR(20)) + '> To <'
                        + CAST(u.ClaimBackAmountNew AS VARCHAR(20)) + '>' ,
                        GETDATE()
                FROM    @UpdatedRowData19 u
                WHERE   u.ClaimBackAmountOld <> u.ClaimBackAmountNew

        SELECT  @rowCheck = @@ROWCOUNT
        IF @rowCheck > 0
            BEGIN
                UPDATE  l
                SET     ContractID = sc.ContractID ,
                        vendorKey = sc.VendorKey
                FROM    cc.SupplierContractLog l
                        INNER JOIN cc.SupplierContract sc ON l.ContractKey = sc.ContractKey
                WHERE   l.ContractLogKey = SCOPE_IDENTITY()
            END
        SET @rowCheck = 0
							--ClaimBackPerc
        INSERT  INTO cc.SupplierContractLog
                ( WebUserKey ,
                  ContractKey ,
                  ContractAction ,
                  Notes ,
                  CreationDate
								
                )
                SELECT  @WebUserKey ,
                        u.ContractKey ,
                        'Import Contract - AMEND  ' ,
                        'Update Product MPG:<'
                        + CAST(u.itemGroupXkey AS VARCHAR(20))
                        + '>; ClaimBackPerc <'
                        + CAST(u.ClaimBackPerOld AS VARCHAR(20)) + '> To <'
                        + CAST(u.ClaimBackPerNew AS VARCHAR(20)) + '>' ,
                        GETDATE()
                FROM    @UpdatedRowData19 u
                WHERE   u.ClaimBackPerOld <> u.ClaimBackPerNew

        SELECT  @rowCheck = @@ROWCOUNT
        IF @rowCheck > 0
            BEGIN
                UPDATE  l
                SET     ContractID = sc.ContractID ,
                        vendorKey = sc.VendorKey
                FROM    cc.SupplierContractLog l
                        INNER JOIN cc.SupplierContract sc ON l.ContractKey = sc.ContractKey
                WHERE   l.ContractLogKey = SCOPE_IDENTITY()
            END
        SET @rowCheck = 0
							--ClaimCost
        INSERT  INTO cc.SupplierContractLog
                ( WebUserKey ,
                  ContractKey ,
                  ContractAction ,
                  Notes ,
                  CreationDate
								
                )
                SELECT  @WebUserKey ,
                        u.ContractKey ,
                        'Import Contract - AMEND  ' ,
                        'Update Product MPG :<'
                        + CAST(u.itemGroupXkey AS VARCHAR(20))
                        + '>; ClaimCost <'
                        + CAST(u.ClaimCostOld AS VARCHAR(20)) + '> To <'
                        + CAST(u.ClaimCostNew AS VARCHAR(20)) + '>' ,
                        GETDATE()
                FROM    @UpdatedRowData19 u
                WHERE   u.ClaimCostOld <> u.ClaimCostNew

        SELECT  @rowCheck = @@ROWCOUNT
        IF @rowCheck > 0
            BEGIN
                UPDATE  l
                SET     ContractID = sc.ContractID ,
                        vendorKey = sc.VendorKey
                FROM    cc.SupplierContractLog l
                        INNER JOIN cc.SupplierContract sc ON l.ContractKey = sc.ContractKey
                WHERE   l.ContractLogKey = SCOPE_IDENTITY()
            END
        SET @rowCheck = 0
							--LimitType
        INSERT  INTO cc.SupplierContractLog
                ( WebUserKey ,
                  ContractKey ,
                  ContractAction ,
                  Notes ,
                  CreationDate
								
                )
                SELECT  @WebUserKey ,
                        u.ContractKey ,
                        'Import Contract - AMEND  ' ,
                        'Update Product MPG :<'
                        + CAST(u.itemGroupXkey AS VARCHAR(20))
                        + '>; LimitType <' + u.LimitTypeOld + '> To <'
                        + u.LimitTypeNew + '>' ,
                        GETDATE()
                FROM    @UpdatedRowData19 u
                WHERE   ISNULL(u.LimitTypeOld, '') <> ISNULL(u.LimitTypeNew,
                                                             '')
        SELECT  @rowCheck = @@ROWCOUNT
        IF @rowCheck > 0
            BEGIN
                UPDATE  l
                SET     ContractID = sc.ContractID ,
                        vendorKey = sc.VendorKey
                FROM    cc.SupplierContractLog l
                        INNER JOIN cc.SupplierContract sc ON l.ContractKey = sc.ContractKey
                WHERE   l.ContractLogKey = SCOPE_IDENTITY()
            END
        SET @rowCheck = 0
							--LimitValue
        INSERT  INTO cc.SupplierContractLog
                ( WebUserKey ,
                  ContractKey ,
                  ContractAction ,
                  Notes ,
                  CreationDate
								
                )
                SELECT  @WebUserKey ,
                        u.ContractKey ,
                        'Import Contract - AMEND  ' ,
                        'Update Product MPG :<'
                        + CAST(u.itemGroupXkey AS VARCHAR(20))
                        + '>; LimitValue <'
                        + CAST(u.LimitValueOld AS VARCHAR(20)) + '> To <'
                        + CAST(u.LimitValueNew AS VARCHAR(20)) + '>' ,
                        GETDATE()
                FROM    @UpdatedRowData19 u
                WHERE   u.LimitValueOld <> u.LimitValueNew

        SELECT  @rowCheck = @@ROWCOUNT
        IF @rowCheck > 0
            BEGIN
                UPDATE  l
                SET     ContractID = sc.ContractID ,
                        vendorKey = sc.VendorKey
                FROM    cc.SupplierContractLog l
                        INNER JOIN cc.SupplierContract sc ON l.ContractKey = sc.ContractKey
                WHERE   l.ContractLogKey = SCOPE_IDENTITY()
            END
        SET @rowCheck = 0


/*
				Level = SPG and NOT already in the SupplierContractItem
*/
        DECLARE @UpdatedRowData22 TABLE
            (
              ContractKey KEY_NORMAL_TYPE ,
              itemGroupXkey INT
            )

        INSERT  INTO cc.SupplierContractItemGroupX
                ( ContractKey ,
                  ItemGroupLevel ,
                  ItemGroupXKey ,
                  ClaimType ,
                  ClaimBackAmount ,
                  ClaimBackPerc ,
                  ClaimCost ,
                  LimitType ,
                  LimitValue ,
                  CreatedByWebUserKey ,
                  CreatedOnDT
                )
        OUTPUT  Inserted.contractKey ,
                Inserted.ItemGroupXKey
                INTO @UpdatedRowData22
                SELECT DISTINCT
                        sc.ContractKey ,
                        3 ,
                        dig3.ItemGroup3Key ,
                        CASE WHEN i.ClaimPercent IS NOT NULL THEN 'P'
                             WHEN i.ClaimAmount IS NOT NULL THEN 'A'
                             WHEN i.NetCost IS NOT NULL THEN 'C'
                             ELSE ''
                        END AS ClaimType ,
                        i.ClaimAmount ,
                        i.ClaimPercent ,
                        i.NetCost ,
                        i.QuantityType ,
                        i.QuantityLimit ,
                        @WebUserKey ,
                        GETDATE()
                FROM    cc.SupplierContractImport i
                        INNER JOIN cc.SupplierContract sc ON i.ContractNumber = sc.ContractNumber
                                                             AND sc.Originator = i.Originator
                        INNER JOIN dbo.DimItemGroup3 dig3 ON dig3.IG3Level1 = i.LevelCode
                                                             AND dig3.IG3Level4 = i.PyramidCode
                WHERE   ( i.LevelType = 'SPG' )
                        AND ( i.BatchKey = @BatchKey )
                        AND ( i.ContractNumber <> 'NEW' )
                        AND i.SupplierContractImportKey IN (
                        SELECT  imp.SupplierContractImportKey
                        FROM    cc.SupplierContractImport imp
                                INNER JOIN cc.SupplierContract sc ON imp.ContractNumber = sc.ContractNumber
                                                              AND sc.Originator = imp.Originator
                        WHERE   imp.BatchKey = @BatchKey
                                AND imp.ContractNumber <> 'NEW'
                                AND imp.LevelType = 'SPG'
                                AND i.LevelCode IN (
                                SELECT  dig3.IG3Level1
                                FROM    cc.SupplierContractImport i
                                        INNER JOIN dbo.DimItemGroup3 dig3 ON dig3.IG3Level1 = i.LevelCode
                                                              AND dig3.IG3Level4 = i.PyramidCode
                                WHERE   ( i.LevelType = 'SPG' )
                                        AND ( i.BatchKey = @BatchKey )
                                        AND ( i.ContractNumber <> 'NEW' ) ) )
                        AND CAST(sc.ContractKey AS VARCHAR(20)) + '|'
                        + CAST(dig3.ItemGroup3Key AS VARCHAR(20)) NOT IN (
                        SELECT  CAST(ContractKey AS VARCHAR(20)) + '|'
                                + CAST(ItemGroupXKey AS VARCHAR(20))
                        FROM    [cc].[SupplierContractItemGroupX] )

        INSERT  INTO cc.SupplierContractLog
                ( WebUserKey ,
                  ContractKey ,
                  ContractAction ,
                  Notes ,
                  CreationDate
								
                )
                SELECT  @WebUserKey ,
                        u.ContractKey ,
                        'Import Contract - AMEND  ' ,
                        'Insert New Product SPG :<'
                        + CAST(u.itemGroupXkey AS VARCHAR(20)) + '>' ,
                        GETDATE()
                FROM    @UpdatedRowData22 u
						
        UPDATE  l
        SET     ContractID = sc.ContractID ,
                vendorKey = sc.VendorKey
        FROM    cc.SupplierContractLog l
                INNER JOIN cc.SupplierContract sc ON l.ContractKey = sc.ContractKey
        WHERE   l.ContractLogKey = SCOPE_IDENTITY()

				UPDATE sc
				SET ContractSourceKey = 3
				FROM @UpdatedRowData22 u INNER JOIN cc.SupplierContract sc ON u.contractKey = sc.ContractKey
                WHERE   (sc.ContractSourceKey = 1)

--
/*
				Level = SPG and already in the SupplierContractItem
*/
        DECLARE @UpdatedRowData20 TABLE
            (
              ContractKey KEY_NORMAL_TYPE ,
              itemGroupXkey INT ,
              claimTypeNew VARCHAR(1) ,
              ClaimTypeOld VARCHAR(1) ,
              ClaimBackAmountNew MONEY ,
              ClaimBackAmountOld MONEY ,
              ClaimBackPerNew DECIMAL(9,4) ,
              ClaimBackPerOld DECIMAL(9,4) ,
              ClaimCostNew MONEY ,
              ClaimCostOld MONEY ,
              LimitTypeNew VARCHAR(1) ,
              LimitTypeOld VARCHAR(1) ,
              LimitValueNew DECIMAL(36,4) ,
              LimitValueOld DECIMAL(36,4)
            )
        UPDATE  cc.SupplierContractItemGroupX
        SET     claimType = CASE WHEN i.ClaimPercent IS NOT NULL THEN 'P'
                                 WHEN i.ClaimAmount IS NOT NULL THEN 'A'
                                 WHEN i.NetCost IS NOT NULL THEN 'C'
                                 ELSE ''
                            END ,
                ClaimBackAmount = i.ClaimAmount ,
                ClaimBackPerc = i.ClaimPercent ,
                ClaimCost = i.netcost ,
                LimitType = i.QuantityType ,
                LimitValue = i.QuantityLimit ,
                ModifiedByWebUserKey = @WebUserKey ,
                ModifiedOnDT = GETDATE()
        OUTPUT  Inserted.ContractKey ,
                Inserted.ItemGroupXKey ,
                Inserted.ClaimType ,
                Deleted.ClaimType ,
                Inserted.ClaimBackAmount ,
                Deleted.ClaimBackAmount ,
                Inserted.ClaimBackPerc ,
                Deleted.ClaimBackPerc ,
                Inserted.ClaimCost ,
                Deleted.ClaimCost ,
                Inserted.LimitType ,
                Deleted.LimitType ,
                Inserted.LimitValue ,
                Deleted.LimitValue
                INTO @UpdatedRowData20
        FROM    cc.SupplierContractImport i
                INNER JOIN cc.SupplierContract sc ON i.ContractNumber = sc.ContractNumber
                                                     AND sc.Originator = i.Originator
                INNER JOIN dbo.DimItemGroup3 dig3 ON dig3.IG3Level1 = i.LevelCode
                                                     AND dig3.IG3Level4 = i.PyramidCode
                INNER JOIN cc.SupplierContractItemGroupX scig ON sc.ContractKey = scig.ContractKey
                                                              AND scig.ItemGroupXKey = dig3.ItemGroup3Key
        WHERE   ( i.LevelType = 'SPG' )
                AND ( i.BatchKey = @BatchKey )
                AND ( i.ContractNumber <> 'NEW' )
                AND i.SupplierContractImportKey IN (
                SELECT  imp.SupplierContractImportKey
                FROM    cc.SupplierContractImport imp
                        INNER JOIN cc.SupplierContract sc ON imp.ContractNumber = sc.ContractNumber
                                                             AND sc.Originator = imp.Originator
                WHERE   imp.BatchKey = @BatchKey
                        AND imp.ContractNumber <> 'NEW'
                        AND imp.LevelType = 'SPG'
                        AND i.LevelCode IN (
                        SELECT  dig3.IG3Level1
                        FROM    cc.SupplierContractImport i
                                INNER JOIN dbo.DimItemGroup3 dig3 ON dig3.IG3Level1 = i.LevelCode
                                                              AND dig3.IG3Level4 = i.PyramidCode
                        WHERE   ( i.LevelType = 'SPG' )
                                AND ( i.BatchKey = @BatchKey )
                                AND ( i.ContractNumber <> 'NEW' ) ) )
                AND CAST(sc.ContractKey AS VARCHAR(20)) + '|'
                + CAST(dig3.ItemGroup3Key AS VARCHAR(20)) IN (
                SELECT  CAST(ContractKey AS VARCHAR(20)) + '|'
                        + CAST(ItemGroupXKey AS VARCHAR(20))
                FROM    [cc].[SupplierContractItemGroupX] )
		/* Loging section for Update SPG*/
	--ClaimType
        INSERT  INTO cc.SupplierContractLog
                ( WebUserKey ,
                  ContractKey ,
                  ContractAction ,
                  Notes ,
                  CreationDate
								
                )
                SELECT  @WebUserKey ,
                        u.ContractKey ,
                        'Import Contract - AMEND  ' ,
                        'Update Product SPG :<'
                        + CAST(u.itemGroupXkey AS VARCHAR(20))
                        + '>; claimType <' + u.ClaimTypeOld + '> To <'
                        + u.claimTypeNew + '>' ,
                        GETDATE()
                FROM    @UpdatedRowData20 u
                WHERE   ISNULL(u.ClaimTypeOld, '') <> ISNULL(u.claimTypeNew, '')
                    
				UPDATE sc
				SET ContractSourceKey = 3
				FROM @UpdatedRowData20 u INNER JOIN cc.SupplierContract sc ON u.contractKey = sc.ContractKey
                WHERE   (sc.ContractSourceKey = 1)	 
					                                        
        SELECT  @rowCheck = @@ROWCOUNT
        IF @rowCheck > 0
            BEGIN
                UPDATE  l
                SET     ContractID = sc.ContractID ,
                        vendorKey = sc.VendorKey
                FROM    cc.SupplierContractLog l
                        INNER JOIN cc.SupplierContract sc ON l.ContractKey = sc.ContractKey
                WHERE   l.ContractLogKey = SCOPE_IDENTITY()
            END
        SET @rowCheck = 0
							--ClaimBackAmount
        INSERT  INTO cc.SupplierContractLog
                ( WebUserKey ,
                  ContractKey ,
                  ContractAction ,
                  Notes ,
                  CreationDate
								
                )
                SELECT  @WebUserKey ,
                        u.ContractKey ,
                        'Import Contract - AMEND  ' ,
                        'Update Product SPG:<'
                        + CAST(u.itemGroupXkey AS VARCHAR(20))
                        + '>; ClaimBackAmount <'
                        + CAST(u.ClaimBackAmountOld AS VARCHAR(20)) + '> To <'
                        + CAST(u.ClaimBackAmountNew AS VARCHAR(20)) + '>' ,
                        GETDATE()
                FROM    @UpdatedRowData20 u
                WHERE   u.ClaimBackAmountOld <> u.ClaimBackAmountNew

        SELECT  @rowCheck = @@ROWCOUNT
        IF @rowCheck > 0
            BEGIN
                UPDATE  l
                SET     ContractID = sc.ContractID ,
                        vendorKey = sc.VendorKey
                FROM    cc.SupplierContractLog l
                        INNER JOIN cc.SupplierContract sc ON l.ContractKey = sc.ContractKey
                WHERE   l.ContractLogKey = SCOPE_IDENTITY()
            END
        SET @rowCheck = 0
							--ClaimBackPerc
        INSERT  INTO cc.SupplierContractLog
                ( WebUserKey ,
                  ContractKey ,
                  ContractAction ,
                  Notes ,
                  CreationDate
								
                )
                SELECT  @WebUserKey ,
                        u.ContractKey ,
                        'Import Contract - AMEND  ' ,
                        'Update Product SPG:<'
                        + CAST(u.itemGroupXkey AS VARCHAR(20))
                        + '>; ClaimBackPerc <'
                        + CAST(u.ClaimBackPerOld AS VARCHAR(20)) + '> To <'
                        + CAST(u.ClaimBackPerNew AS VARCHAR(20)) + '>' ,
                        GETDATE()
                FROM    @UpdatedRowData20 u
                WHERE   u.ClaimBackPerOld <> u.ClaimBackPerNew

        SELECT  @rowCheck = @@ROWCOUNT
        IF @rowCheck > 0
            BEGIN
                UPDATE  l
                SET     ContractID = sc.ContractID ,
                        vendorKey = sc.VendorKey
                FROM    cc.SupplierContractLog l
                        INNER JOIN cc.SupplierContract sc ON l.ContractKey = sc.ContractKey
                WHERE   l.ContractLogKey = SCOPE_IDENTITY()
            END
        SET @rowCheck = 0
							--ClaimCost
        INSERT  INTO cc.SupplierContractLog
                ( WebUserKey ,
                  ContractKey ,
                  ContractAction ,
                  Notes ,
                  CreationDate
								
                )
                SELECT  @WebUserKey ,
                        u.ContractKey ,
                        'Import Contract - AMEND  ' ,
                        'Update Product SPG :<'
                        + CAST(u.itemGroupXkey AS VARCHAR(20))
                        + '>; ClaimCost <'
                        + CAST(u.ClaimCostOld AS VARCHAR(20)) + '> To <'
                        + CAST(u.ClaimCostNew AS VARCHAR(20)) + '>' ,
                        GETDATE()
                FROM    @UpdatedRowData20 u
                WHERE   u.ClaimCostOld <> u.ClaimCostNew

        SELECT  @rowCheck = @@ROWCOUNT
        IF @rowCheck > 0
            BEGIN
                UPDATE  l
                SET     ContractID = sc.ContractID ,
                        vendorKey = sc.VendorKey
                FROM    cc.SupplierContractLog l
                        INNER JOIN cc.SupplierContract sc ON l.ContractKey = sc.ContractKey
                WHERE   l.ContractLogKey = SCOPE_IDENTITY()
            END
        SET @rowCheck = 0
							--LimitType
        INSERT  INTO cc.SupplierContractLog
                ( WebUserKey ,
                  ContractKey ,
                  ContractAction ,
                  Notes ,
                  CreationDate
								
                )
                SELECT  @WebUserKey ,
                        u.ContractKey ,
                        'Import Contract - AMEND  ' ,
                        'Update Product SPG :<'
                        + CAST(u.itemGroupXkey AS VARCHAR(20))
                        + '>; LimitType <' + u.LimitTypeOld + '> To <'
                        + u.LimitTypeNew + '>' ,
                        GETDATE()
                FROM    @UpdatedRowData20 u
                WHERE   ISNULL(u.LimitTypeOld, '') <> ISNULL(u.LimitTypeNew,
                                                             '')
        SELECT  @rowCheck = @@ROWCOUNT
        IF @rowCheck > 0
            BEGIN
                UPDATE  l
                SET     ContractID = sc.ContractID ,
                        vendorKey = sc.VendorKey
                FROM    cc.SupplierContractLog l
                        INNER JOIN cc.SupplierContract sc ON l.ContractKey = sc.ContractKey
                WHERE   l.ContractLogKey = SCOPE_IDENTITY()
            END
        SET @rowCheck = 0
							--LimitValue
        INSERT  INTO cc.SupplierContractLog
                ( WebUserKey ,
                  ContractKey ,
                  ContractAction ,
                  Notes ,
                  CreationDate
								
                )
                SELECT  @WebUserKey ,
                        u.ContractKey ,
                        'Import Contract - AMEND  ' ,
                        'Update Product SPG :<'
                        + CAST(u.itemGroupXkey AS VARCHAR(20))
                        + '>; LimitValue <'
                        + CAST(u.LimitValueOld AS VARCHAR(20)) + '> To <'
                        + CAST(u.LimitValueNew AS VARCHAR(20)) + '>' ,
                        GETDATE()
                FROM    @UpdatedRowData20 u
                WHERE   u.LimitValueOld <> u.LimitValueNew

        SELECT  @rowCheck = @@ROWCOUNT
        IF @rowCheck > 0
            BEGIN
                UPDATE  l
                SET     ContractID = sc.ContractID ,
                        vendorKey = sc.VendorKey
                FROM    cc.SupplierContractLog l
                        INNER JOIN cc.SupplierContract sc ON l.ContractKey = sc.ContractKey
                WHERE   l.ContractLogKey = SCOPE_IDENTITY()
            END
        SET @rowCheck = 0

--INSERT  INTO cc.SupplierContractLog
--            ( WebUserKey ,
--              ContractKey ,
--              contractID ,
--              ContractAction ,
--              Notes ,
--              vendorKey ,
--              CreationDate
			  
--            )
--            SELECT  CreatedByWebUserKey ,
--                    sc.ContractKey ,
--                    sc.ContractID ,
--                    'Import Contract' ,
--                    'From:<NULL> To:<'
--                    + CAST(scs.StatusDescription AS VARCHAR(20)) + '>' ,
--                    sc.VendorKey ,
--                    GETDATE()
--            FROM    cc.SupplierContract sc
--                    INNER JOIN @NewContractKey c ON c.BatchKey = sc.BatchKey
--                                                    AND c.SequenceNumber = sc.SequenceNumber
--                    INNER JOIN cc.SupplierContractStatus scs ON sc.StatusKey = scs.StatusKey
--            WHERE   sc.BatchKey = @BatchKey





        COMMIT TRANSACTION tranUpdateRecord
    END TRY
    BEGIN CATCH
        SELECT  ERROR_NUMBER() AS ErrorNumber ,
                ERROR_SEVERITY() AS ErrorSeverity ,
                ERROR_STATE() AS ErrorState ,
                ERROR_PROCEDURE() AS ErrorProcedure ,
                ERROR_LINE() AS ErrorLine ,
                ERROR_MESSAGE() AS ErrorMessage;

        ROLLBACK TRANSACTION tranUpdateRecord
        PRINT 'ROLLBACK TRANS'
    END CATCH
  


IF OBJECT_ID('tempdb.dbo.#LocationLevel', 'U') IS NOT NULL
    DROP TABLE #LocationLevel; 
IF OBJECT_ID('tempdb.dbo.#ContractSequence', 'U') IS NOT NULL
    DROP TABLE #ContractSequence; 
IF OBJECT_ID('tempdb.dbo.#TempContractNumber', 'U') IS NOT NULL
    DROP TABLE #TempContractNumber; 


SELECT  SupplierContractImportKey ,
        RowNumber ,
        ErrorMessage
FROM    cc.SupplierContractImport
WHERE   BatchKey = @BatchKey
        AND ErrorMessage IS NOT NULL


GO
