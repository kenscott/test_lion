SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE PROCEDURE [lion].[InsertTerms] 
			@Filename nvarchar(4000)
           ,@RowType nvarchar(255) = null
           ,@RecID nvarchar(255) = null
           ,@AccountKey int = null
           ,@ItemKey int = null
           ,@InvoiceLineGroup1Key int = null
           ,@AccountNumber nvarchar(255) = null
           ,@AccountName nvarchar(255) = null
           ,@Area nvarchar(255) = null
           ,@LLSPGDescription nvarchar(255) = null
           ,@ProductDescription nvarchar(255) = null
           ,@LLSPG nvarchar(255) = null
           ,@ProductCode nvarchar(255) = null
           ,@ProdKVI nvarchar(255) = null
           ,@KVIOverride nvarchar(255) = null
           ,@DefaultDiscount decimal(19,8) = null
           ,@LastTermsChange nvarchar(255) = null
           ,@SPGDisc1 decimal(19,8) = null
           ,@SPGDisc2 decimal(19,8) = null
           ,@SPGCompoundedDiscount decimal(19,8) = null
           ,@ExceptionDiscount decimal(19,8) = null
           ,@ExceptionFixedPrice nvarchar(255) = null
           ,@FixedPriceStartDate nvarchar(255) = null
           ,@FixedPriceExpiryDate nvarchar(255) = null
           ,@TotalQuantity decimal(19,8) = null
           ,@ProductSales decimal(19,8) = null
           ,@LLSPGSales decimal(19,8) = null
           ,@LastSaleDate nvarchar(255) = null
           ,@NewSPGDiscount1 decimal(19,8) = null
           ,@NewSPGDiscount2 decimal(19,8) = null
           ,@NewCompoundedDiscount nvarchar(255) = null
           ,@NewExceptionFixedPrice decimal(19,8) = null
           ,@NewExceptionDiscount decimal(19,8) = null
           ,@NewExceptionPriceExpiryDate nvarchar(255) = null
           ,@Brand nvarchar(255) = null
		   ,@RecommendedImpact decimal(19,8) = null
		   ,@AcceptedImpact decimal(19,8) = null
AS

SET NOCOUNT ON
SET ANSI_WARNINGS OFF 

if @Filename <> ''
begin
	INSERT INTO [lion].[IncomingTerms]
			   ([Filename]
			   ,[RowType]
			   ,[RecID]
			   ,[AccountKey]
			   ,[ItemKey]
			   ,[InvoiceLineGroup1Key]
			   ,[AccountNumber]
			   ,[AccountName]
			   ,[Area]
			   ,[LLSPG Description]
			   ,[Product Description]
			   ,[LLSPG]
			   ,[Product Code]
			   ,[Prod KVI]
			   ,[KVI Override]
			   ,[Default Discount]
			   ,[Last Terms Change]
			   ,[SPG Disc 1]
			   ,[SPG Disc 2]
			   ,[SPG Compounded Discount]
			   ,[Exception Discount]
			   ,[Exception Fixed Price]
			   ,[Fixed Price Start Date]
			   ,[Fixed Price Expiry Date]
			   ,[Total Quantity]
			   ,[Product Sales]
			   ,[LLSPG Sales]
			   ,[Last Sale Date]
			   ,[New SPG Discount 1]
			   ,[New SPG Discount 2]
			   ,[New Compounded Discount]
			   ,[New Exception Fixed Price]
			   ,[New Exception Discount]
			   ,[New Exception Price Expiry Date]
			   ,[Brand]
			   ,[Recommended Impact]
			   ,[Accepted Impact])
		 VALUES
			   (@Filename 
			   ,IIF(@RowType='Null',null,@RowType)
			   ,IIF(@RecID='Null',null,@RecID)
			   ,@AccountKey
			   ,@ItemKey 
			   ,@InvoiceLineGroup1Key 
			   ,IIF(@AccountNumber ='Null',null,@AccountNumber )
			   ,IIF(@AccountName ='Null',null,@AccountName )
			   ,IIF(@Area ='Null',null,@Area )
			   ,IIF(@LLSPGDescription ='Null',null,@LLSPGDescription )
			   ,IIF(@ProductDescription='Null',null,@ProductDescription)
			   ,IIF(@LLSPG ='Null',null,@LLSPG )
			   ,IIF(@ProductCode ='Null',null,@ProductCode )
			   ,IIF(@ProdKVI ='Null',null,@ProdKVI )
			   ,IIF(@KVIOverride ='Null',null,@KVIOverride )
			   ,@DefaultDiscount 
			   ,IIF(@LastTermsChange ='Null',null,convert(datetime,@LastTermsChange) )
			   ,@SPGDisc1 
			   ,@SPGDisc2 
			   ,@SPGCompoundedDiscount 
			   ,@ExceptionDiscount 
			   ,IIF(@ExceptionFixedPrice ='Null',null,@ExceptionFixedPrice )
			   ,IIF(@FixedPriceStartDate ='Null',null,@FixedPriceStartDate )
			   ,IIF(@FixedPriceExpiryDate ='Null',null,@FixedPriceExpiryDate )
			   ,@TotalQuantity 
			   ,@ProductSales 
			   ,@LLSPGSales 
			   ,IIF(@LastSaleDate ='Null',null,convert(datetime,@LastSaleDate) ) 
			   ,@NewSPGDiscount1 
			   ,@NewSPGDiscount2 
			   ,IIF(@NewCompoundedDiscount ='Null',null,@NewCompoundedDiscount ) 
			   ,@NewExceptionFixedPrice 
			   ,@NewExceptionDiscount 
			   ,IIF(@NewExceptionPriceExpiryDate ='Null',null,convert(datetime,@NewExceptionPriceExpiryDate) )
			   ,IIF(@Brand ='Null',null,@Brand )
			   ,@RecommendedImpact
			   ,@AcceptedImpact)
end

SELECT @@ROWCOUNT as NumRowsAffected




GO
