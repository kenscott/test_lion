SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- Stored Procedure

CREATE PROCEDURE [cm].[Load_StagingClaimJournalIncoming] (@BatchId INT, @RecordCount INT)

AS

/*

EXEC cm.Load_StagingClaimJournalIncomingNew @BatchId = 1, @RecordCount = 0

*/


SET NOCOUNT ON 


DECLARE
	@SQLString NVARCHAR(4000) = '',
	@ODSPrefix NVARCHAR(4000) = ''


-- INSERT INTO dbo.Param (ParamName, ParamValue) SELECT 'ClaimsManagerODSPrefix', '[ODSD]..' WHERE NOT EXISTS (SELECT 1 FROM dbo.Param WHERE ParamName = 'ClaimsManagerODSPrefix')

SELECT 
	@ODSPrefix = ParamValue
FROM 
	dbo.[Param]
WHERE 
	ParamName = 'ClaimsManagerODSPrefix'


SELECT @SQLString = '

INSERT INTO [cm].[StagingClaimJournalIncoming]
           ([BatchID]
           ,[ODSClaimJournalKey]
           --,[ADVClaimJournalKey]
           ,[ClaimMonth]
           ,[TransactionID]
           ,[PyramidCode]
           ,[SupplierID]
           ,[SupplierName]
           ,[POSContractID]
           ,[ARPContractID]
           ,[ClaimType]
           ,[ClaimLineValue]
		   ,[TradingMargin]
		   ,[FinancialMargin]
		   ,[ConfidentialTermsGPAdjustAmt]
           ,[PriceDeferralGPAdjustAmt]
		   ,[SpecialDealGPAdjustAmt]
		   ,[ClearanceDealGPAdjustAmt]
           ,[Status]
           ,[StatusDate]
           ,[ARPResult]
           ,[ARPResultDate]
           ,[ARPReason]
		   ,[ARPRecommendation]
		   ,[SupplierReferenceNumber]
		   ,[OriginalTransactionID]
		   )

SELECT
		BATCH_ID,
		ODS_CC_SK,
		--ADV_SK,
		YRMTH,
		SRC_TXN_SK,
		PYRAMID_CODE,
		SUPPLIER_CODE,
		SUPPLIER_NAME,
		POS_CONTRACT_ID,
		ARP_CONTRACT_ID,
		CLAIM_TYPE,
		VALUE,
		TRADING_MARGIN,
		FINANCIAL_MARGIN,
		CONF_TERMS_GP_ADJ_AMT,
		PRICE_DEFER_GP_ADJ_AMT,
		SPECIAL_DEAL_GP_ADJ_AMT,
		CLEAR_DEAL_GP_ADJ_AMT,
		[STATUS],
		STATUS_DATE,
		ARP_RESULT,
		ARP_RESULT_DATE,
		ARP_REASON,
		REVIEW_REQUIRED,
		SUPPLIER_REFERENCE_ID,
		SALE_SRC_TXN_SK

FROM ' + @ODSPrefix + '..[ODS_DATA_OUT].[ODS_CONTRACT_CLAIMS_JOURNAL]

WHERE BATCH_ID = @BatchId
'

--select @SQLString
EXECUTE sp_executesql @SQLString, N'@BatchId INT', @BatchId

IF
(
SELECT COUNT(*)
FROM cm.StagingClaimJournalIncoming
WHERE BatchID = @BatchId
)
= @RecordCount

BEGIN
	
	SELECT @SQLString = '
		UPDATE ' + @ODSPrefix + '..ODS_DATA_OUT.ADV_INTERFACE_CONTROL
		SET COLLECT_DATETIME = GETDATE()
		WHERE TABLE_NAME = ''ODS_CONTRACT_CLAIMS_JOURNAL''
			AND BATCH_ID = @BatchId
		'

	--select @SQLString
	
	EXECUTE sp_executesql @SQLString, N'@BatchId INT', @BatchId

	/*
	EXECUTE ('BEGIN ODS_API.adv_to_ods_notify.ack_advanous_jnl_out_success(?,?); END;', @BatchId,@RecordCount) AT ODST
	*/
	SELECT @SQLString = 'EXECUTE (''BEGIN ODS_API.adv_to_ods_notify.ack_advanous_jnl_out_success(?,?,?,?); COMMIT; END;'', @BatchId,@RecordCount,'''','''') AT ' + @ODSPrefix
	EXECUTE sp_executesql @SQLString, N'@BatchId INT, @RecordCount INT', @BatchId, @RecordCount

END

GO
