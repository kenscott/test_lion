SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO









CREATE      PROCEDURE 
[dbo].[RBP_QuantityBandRecomputeCheck]
@PlaybookDataPointGroupKey INT
AS
SET QUOTED_IDENTIFIER ON 
SET ANSI_NULLS ON 
SET ARITHABORT ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
SET CONCAT_NULL_YIELDS_NULL ON
SET NUMERIC_ROUNDABORT OFF
DECLARE @RowsI Quantity_Normal_type, @RowsU Quantity_Normal_type, @RowsD Quantity_Normal_type, @PricingRuleWhereClause NVARCHAR(2000), @tmpValuesAssigned NVARCHAR(2000)
SET @RowsI = 0
SET @RowsU = 0
SET @RowsD = 0

DECLARE @ProjectKey key_normal_type
SELECT @ProjectKey = ProjectKey FROM PlaybookDataPointGroup WHERE PlaybookDataPointGroupKey = @PlaybookDataPointGroupKey

EXEC LogDCPEvent 'DCP --- RBP_QuantityBandRecomputeCheck', 'B',0,0,0,@ProjectKey


DECLARE @ScenarioKey INT

--Now that all of the price groups have been created, lets determine how the quantity bands for them should be set!
--If we need to prevent quantity bands from being re-calculated if they have been calculated already in the last X months, this code will do it:
--This gets the ScenarioKey of the scenario we're dealing with
SELECT @ScenarioKey = ScenarioKey FROM PlaybookDataPointGroup_View WHERE PlaybookDataPointGroupKey = @PlaybookDataPointGroupKey


DECLARE @MinDaysAutomaticRecompute INT
DECLARE @MinDaysManualRecompute INT

SELECT
@MinDaysAutomaticRecompute = MinDaysToRecomputeQuantityBandFromAutomaticRecompute,
@MinDaysManualRecompute = MinDaysToRecomputeQuantityBandFromManualChange
FROM ATKScenario 
WHERE ScenarioKey = @ScenarioKey


--This will list all data points groups that are part of this scenario but were created BEFORE this group was.  We do not care about groups Created AFTER it.
--Using this list, we can see if quantity bands have already been calculated for any of the groups
SELECT PlaybookDataPointGroupKey 
INTO #OtherPDPGs
FROM PlaybookDataPointGroup_View 
WHERE ScenarioKey = @ScenarioKey
	AND PlaybookDataPointGroupKey < @PlaybookDataPointGroupKey



--These are all of the bands that we DO NOT want to recompute.
SELECT PlaybookPricingGroupKey, GroupingColumns, GroupValues, CreationDate
INTO #Temp
FROM PlaybookPricingGroup
WHERE 
	PlaybookDataPointGroupKey IN (SELECT PlaybookDataPointGroupKey FROM #OtherPDPGs)
	AND 
	(
		(QuantityBandCreationMethod = 'Recomputed' AND DATEDIFF(Day, CreationDate, GETDATE()) < @MinDaysAutomaticRecompute)
		  OR	
		(QuantityBandCreationMethod = 'Manually Entered' AND DATEDIFF(Day, CreationDate, GETDATE()) < @MinDaysManualRecompute)
	)



--Now, find the most recently recomputed value (in case there are multiple), we'll try to use that one if we find one
SELECT 
	A.GroupingColumns, 
	A.GroupValues, 
	(SELECT TOP 1 PlaybookPricingGroupKey FROM #Temp B WHERE A.GroupingColumns = B.GroupingColumns AND A.GroupValues = B.GroupValues ORDER BY CreationDate DESC) PlaybookPricingGroupKey
INTO #DoNotRecomputeThesePricingGroups
FROM #Temp A
GROUP BY GroupingColumns, GroupValues


UPDATE PlaybookPricingGroup
SET QuantityBandCreationMethod = 'Used Previous Value',
	PreviousValuesPlaybookPricingGroupKey = B.PlaybookPricingGroupKey
FROM PlaybookPricingGroup A
JOIN #DoNotRecomputeThesePricingGroups B
	ON A.GroupingColumns = B.GroupingColumns AND A.GroupValues = B.GroupValues	--If these columns match, we should not re-compute it
WHERE A.PlaybookDataPointGroupKey = @PlaybookDataPointGroupKey
SET @RowsU = @RowsU + @@RowCount

UPDATE PlaybookPricingGroup
SET QuantityBandCreationMethod = 'Recomputed'
WHERE PlaybookDataPointGroupKey = @PlaybookDataPointGroupKey
	AND QuantityBandCreationMethod IS NULL
SET @RowsU = @RowsU + @@RowCount


EXEC LogDCPEvent 'DCP --- RBP_QuantityBandRecomputeCheck', 'E', @RowsI, @RowsU, @RowsD








GO
