SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO










CREATE PROCEDURE [lion].[Load_FactRecommendedPriceBand]
AS

SET NOCOUNT ON
SET QUOTED_IDENTIFIER ON 
SET ANSI_NULLS ON 
SET ARITHABORT ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
SET CONCAT_NULL_YIELDS_NULL ON
SET NUMERIC_ROUNDABORT OFF

EXEC LogDCPEvent 'ETL - lion.Load_FactRecommendedPriceBand', 'B'

DECLARE 
	@RowsI Quantity_Normal_type, 
	@RowsU Quantity_Normal_type, 
	@RowsD Quantity_Normal_type
	
	
SET @RowsI = 0
SET @RowsU = 0
SET @RowsD = 0


EXEC LogDCPEvent 'ETL - lion.Load_FactRecommendedPriceBand: insert FactRecommendedPriceBand', 'B'

SET @RowsI = 0
SET @RowsU = 0
SET @RowsD = 0

EXEC DBA_IndexRebuild 'FactRecommendedPriceBand', 'Drop'

TRUNCATE TABLE dbo.FactRecommendedPriceBand


; WITH flpac AS (
	SELECT --DISTINCT
		flpac.AccountKey,
		flpac.ItemKey,
		flpac.InvoiceLineGroup1Key,

		FloorPercentile,
		TargetPercentile,
		StretchPercentile,
		Offset,

		RPBPlaybookPricingGroupKey,
		Total12MonthQuantity,
		IG3Level4,						-- ItemPyramidCode,
		IG3Level1,						-- LLSPGCode,
		di.ItemNumber,					-- ItemNumber,
		flpac.UDVarchar10,				-- PriceApproach,
		InvoiceLineGroup1UDVarchar2,	-- DeviatedIndicator, -- = @ContractClaimsIndicator,
		AG1Level4,						-- Region,
		AG1Level3,						-- Area,
		AG1Level2,						-- Network
		AG1Level1						-- Branch

	FROM dbo.FactLastPriceAndCost flpac
	INNER JOIN dbo.DimAccount da
		ON da.AccountKey = flpac.AccountKey
	INNER JOIN dbo.DimAccountGroup1 dag1
		ON dag1.AccountGroup1Key = da.AccountGroup1Key
	INNER JOIN dbo.DimItem di
		ON di.ItemKey = flpac.ItemKey
	INNER JOIN dbo.DimItemGroup3 dag3
		ON dag3.ItemGroup3Key = di.ItemGroup3Key
	INNER JOIN dbo.DimInvoiceLineGroup1 dilg1
		ON dilg1.InvoiceLineGroup1Key = flpac.InvoiceLineGroup1Key
	OUTER APPLY lion.fn_GetPercentile (
			di.ItemUDVarchar2,				-- product brand
			dag1.AG1Level4,					-- Region,
			dag1.AG1Level3,					-- Area,
			dag1.AG1Level2,					-- Network
			dag1.AG1Level1,					-- Branch,
			flpac.UDVarchar4,				-- Sales603010Bucket
			ItemUDVarChar1,					-- ItemPyramidCode
			IG3Level1						-- LLSPGCode
		)
)
INSERT dbo.FactRecommendedPriceBand
(
	AccountKey,
	ItemKey,
	InvoiceLineGroup1Key,
	FloorGPP,
	TargetGPP,
	StretchGPP,
	--FloorPercentile,
	--TargetPercentile,
	--StretchPercentile,
	--Offset,
	PlaybookPricingGroupKey,
	PricingRuleSequenceNumber
	--,
	--CoveringBandGroupValues,
	--CoveringBandGroupingColumns,

	--PBFloorGPP,
	--PBTargetGPP,
	--PBStretchGPP,
	--MPBFloorGPP,
	--MPBTargetGPP,
	--MPBStretchGPP,

	--FloorDiscount,
	--TargetDiscount,
	--StretchDiscount
)
SELECT DISTINCT
	flpac.AccountKey,
	flpac.ItemKey,
	flpac.InvoiceLineGroup1Key,

	--ISNULL(MPBFloorGPP, TheFloor.PDPGPP) + ISNULL(Offset, 0.) AS FloorGPP,
	--ISNULL(MPBTargetGPP, TheTarget.PDPGPP) + ISNULL(Offset, 0.) AS TargetGPP,
	--ISNULL(MPBStretchGPP, TheStretch.PDPGPP) + ISNULL(Offset, 0.) AS StretchGPP,

	--ISNULL(mpb.FloorGPP, TheFloor.PDPGPP) + ISNULL(Offset, 0.) AS FloorGPP,
	--ISNULL(mpb.TargetGPP, TheTarget.PDPGPP) + ISNULL(Offset, 0.) AS TargetGPP,
	--ISNULL(mpb.StretchGPP, TheStretch.PDPGPP) + ISNULL(Offset, 0.) AS StretchGPP,

	--ISNULL(mpb.FloorGPP, a.FloorGPP) + ISNULL(Offset, 0.) AS FloorGPP,
	--ISNULL(mpb.TargetGPP, a.TargetGPP) + ISNULL(Offset, 0.) AS TargetGPP,
	--ISNULL(mpb.StretchGPP, a.StretchGPP) + ISNULL(Offset, 0.) AS StretchGPP,

	ISNULL(mpb.MPBFloorGPP, a.FloorGPP) + ISNULL(Offset, 0.) AS FloorGPP,
	ISNULL(mpb.MPBTargetGPP, a.TargetGPP) + ISNULL(Offset, 0.) AS TargetGPP,
	ISNULL(mpb.MPBStretchGPP, a.StretchGPP) + ISNULL(Offset, 0.) AS StretchGPP,

	--FloorPercentile,
	--TargetPercentile,
	--StretchPercentile,
	--Offset,

	RPBPlaybookPricingGroupKey AS PlaybookPricingGroupKey,

	--TheFloor.PricingRuleSequenceNumber AS PricingRuleSequenceNumber,
	--TheFloor.GroupValues AS CoveringBandGroupValues,
	--TheFloor.PDPGPP AS PBFloorGPP,
	--TheTarget.PDPGPP AS PBTargetGPP,	
	--TheStretch.PDPGPP AS PBStretchGPP,

	a.PricingRuleSequenceNumber AS PricingRuleSequenceNumber
	--,
	--a.GroupValues AS CoveringBandGroupValues,
	--a.GroupingColumns AS CoveringBandGroupingColumns,
	--a.FloorGPP AS PBFloorGPP,
	--a.TargetGPP AS PBTargetGPP,	
	--a.StretchGPP AS PBStretchGPP,

	--MPBFloorGPP,
	--MPBTargetGPP,
	--MPBStretchGPP,

	--mpb.FloorGPP AS MPBFloorGPP,
	--mpb.TargetGPP AS MPBTargetGPP,
	--mpb.StretchGPP AS MPBStretchGPP,

	--a.FloorGPP AS PBFloorGPP,
	--a.TargetGPP AS PBTargetGPP,	
	--a.StretchGPP AS PBStretchGPP,


	--TheFloor.PDPDiscountPercent AS FloorDiscount,
	--TheTarget.PDPDiscountPercent AS TargetDiscount,
	--TheStretch.PDPDiscountPercent AS StretchDiscount

	--a.FloorDiscount,
	--a.TargetDiscount,
	--a.StretchDiscount

FROM flpac
--LEFT JOIN lion.PlaybookPlaybookPricingGroupPriceBandActive TheFloor (NOLOCK)
--	ON TheFloor.PlaybookPricingGroupKey = flpac.RPBPlaybookPricingGroupKey
--	AND flpac.Total12MonthQuantity BETWEEN TheFloor.LowerBandQuantity AND TheFloor.UpperBandQuantity 
--	AND TheFloor.MemberRank = ROUND ( TheFloor.RulingMemberCount * FloorPercentile, 0)
--LEFT JOIN lion.PlaybookPlaybookPricingGroupPriceBandActive TheTarget (NOLOCK)
--	ON TheTarget.PlaybookPricingGroupKey = flpac.RPBPlaybookPricingGroupKey
--	AND flpac.Total12MonthQuantity BETWEEN TheTarget.LowerBandQuantity AND TheTarget.UpperBandQuantity 
--	AND TheTarget.MemberRank = ROUND ( TheTarget.RulingMemberCount * TargetPercentile, 0)
--LEFT JOIN lion.PlaybookPlaybookPricingGroupPriceBandActive TheStretch (NOLOCK)
--	ON TheStretch.PlaybookPricingGroupKey = flpac.RPBPlaybookPricingGroupKey
--	AND flpac.Total12MonthQuantity BETWEEN TheStretch.LowerBandQuantity AND TheStretch.UpperBandQuantity 
--	AND TheStretch.MemberRank = ROUND ( TheStretch.RulingMemberCount * StretchPercentile, 0)

OUTER APPLY lion.fn_GetRPBPercentages (
	flpac.RPBPlaybookPricingGroupKey,
	flpac.Total12MonthQuantity,
	FloorPercentile,
	TargetPercentile,
	StretchPercentile) a

--LEFT JOIN lion.ATK ManualPriceBandExpansion mpb
--	ON mpb.ItemKey = flpac.ItemKey
--	AND mpb.PriceApproach = flpac.UDVarchar10
--	and mpb.ContractClaimsIndicator = InvoiceLineGroup1UDVarchar2
--	and mpb.Region = AG1Level4
--	and mpb.Area = AG1Level3
--	and mpb.Network = AG1Level2

-- could also try using lion.ATK ManualPriceBandExpansion if it is being populated
OUTER APPLY lion.fn_GetManualPriceBand (
	IG3Level4,						-- ItemPyramidCode,
	IG3Level1,						-- LLSPGCode,
	ItemNumber,						-- ItemNumber,
	flpac.UDVarchar10,				-- PriceApproach,
	InvoiceLineGroup1UDVarchar2,	-- DeviatedIndicator, -- = @ContractClaimsIndicator,
	AG1Level4,						-- Region,
	AG1Level3,						-- Area,
	AG1Level2,						-- Network
	AG1Level1						-- Branch
	) mpb

SET @RowsI = @RowsI + @@RowCount
EXEC LogDCPEvent 'ETL - lion.Load_FactRecommendedPriceBand: insert FactRecommendedPriceBand', 'E', @RowsI, @RowsU, @RowsD


EXEC LogDCPEvent 'ETL - lion.Load_FactRecommendedPriceBand: index rebuild', 'B'

EXEC DBA_IndexRebuild 'FactRecommendedPriceBand', 'Create'

EXEC LogDCPEvent 'ETL - lion.Load_FactRecommendedPriceBand: index rebuild', 'E'




--DROP TABLE #ppgpbtemp

EXEC LogDCPEvent 'ETL - lion.Load_FactRecommendedPriceBand', 'E',  @RowsI, @RowsU, @RowsD










GO
