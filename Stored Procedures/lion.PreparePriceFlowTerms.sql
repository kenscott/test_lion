SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




CREATE PROCEDURE [lion].[PreparePriceFlowTerms]
AS

TRUNCATE TABLE lion.PFCustomerTerms

; WITH LLSPGTerms AS (
SELECT 
       ROW_NUMBER() OVER (PARTITION BY AccountKey, ISNULL(ig3.[ItemGroup3Key], 1) ORDER BY Accountkey, ISNULL(ig3.[ItemGroup3Key], 1), ItemKey) AS RowNumber
      ,[CustomerTermsKey]
      ,[AccountKey]
      ,1 AS [ItemKey]
      ,ISNULL(ig3.[ItemGroup3Key], 1) AS ItemGroup3Key
      ,[AccountId]
      ,[PyramidCode]
      ,[SPGCode]
      ,[OwningBrandCode]
      ,[Usualdiscount1]
      ,[Usualdiscount2]
      ,[DeleteFlag]
      ,[LastChangeInitials]
      ,[LastChangeDate]
      ,[LastChangeTime]
      ,[LastChangePLID]
      ,[LastChangeBranch]
      ,NULL AS [ExceptionProduct]
      ,NULL AS [ExceptionDiscount]
      ,NULL AS [ExceptionFixedPrice]
      ,NULL AS [ExceptionFromDate]
      ,NULL AS [ExceptionToDate]
      ,NULL AS [ExceptionPCF]
      ,[FallbackMarginFlag]
  FROM [lion].[CustomerTerms] ct LEFT JOIN
       [dbo].[DimItemGroup3] ig3 ON ct.PyramidCode = ig3.IG3Level4 AND ct.SPGCode = ig3.IG3Level1
)

INSERT INTO [lion].[PFCustomerTerms]
           ([CustomerTermsKey]
           ,[AccountKey]
           ,[ItemKey]
           ,[ItemGroup3Key]
           ,[AccountId]
           ,[PyramidCode]
           ,[SPGCode]
           ,[OwningBrandCode]
           ,[Usualdiscount1]
           ,[Usualdiscount2]
           ,[DeleteFlag]
           ,[LastChangeInitials]
           ,[LastChangeDate]
           ,[LastChangeTime]
           ,[LastChangePLID]
           ,[LastChangeBranch]
           ,[FallbackMarginFlag])
SELECT [CustomerTermsKey]
      ,[AccountKey]
      ,[ItemKey]
      ,[ItemGroup3Key]
      ,[AccountId]
      ,[PyramidCode]
      ,[SPGCode]
      ,[OwningBrandCode]
      ,[Usualdiscount1]
      ,[Usualdiscount2]
      ,[DeleteFlag]
      ,[LastChangeInitials]
      ,[LastChangeDate]
      ,[LastChangeTime]
      ,[LastChangePLID]
      ,[LastChangeBranch]
      ,[FallbackMarginFlag]
FROM LLSPGTerms
WHERE LLSPGTerms.RowNumber = 1

INSERT INTO [lion].[PFCustomerTerms]
           ([CustomerTermsKey]
           ,[AccountKey]
           ,[ItemKey]
           ,[ItemGroup3Key]
           ,[AccountId]
           ,[PyramidCode]
           ,[SPGCode]
           ,[OwningBrandCode]
           ,[Usualdiscount1]
           ,[Usualdiscount2]
           ,[DeleteFlag]
           ,[LastChangeInitials]
           ,[LastChangeDate]
           ,[LastChangeTime]
           ,[LastChangePLID]
           ,[LastChangeBranch]
           ,[ExceptionProduct]
           ,[ExceptionDiscount]
           ,[ExceptionFixedPrice]
           ,[ExceptionFromDate]
           ,[ExceptionToDate]
           ,[ExceptionPCF]
           ,[FallbackMarginFlag])
SELECT 
       [CustomerTermsKey]
      ,[AccountKey]
      ,[ItemKey]
      ,ISNULL(ig3.[ItemGroup3Key], 1) AS ItemGroup3Key
      ,[AccountId]
      ,[PyramidCode]
      ,[SPGCode]
      ,[OwningBrandCode]
      ,[Usualdiscount1]
      ,[Usualdiscount2]
      ,[DeleteFlag]
      ,[LastChangeInitials]
      ,[LastChangeDate]
      ,[LastChangeTime]
      ,[LastChangePLID]
      ,[LastChangeBranch]
      ,[ExceptionProduct]
      ,[ExceptionDiscount]
      ,[ExceptionFixedPrice]
      ,[ExceptionFromDate]
      ,[ExceptionToDate]
      ,[ExceptionPCF]
      ,[FallbackMarginFlag]
  FROM [lion].[CustomerTerms] ct LEFT JOIN
       [dbo].[DimItemGroup3] ig3 ON ct.PyramidCode = ig3.IG3Level4 AND ct.SPGCode = ig3.IG3Level1 
 WHERE ItemKey <> 1

DECLARE @MarginChangeThreshold UDDecimal_Type
SET @MarginChangeThreshold = 0.05

INSERT INTO lion.PriceFlowTracker (RequestKey, AccountKey, ItemGroup3Key, ItemKey, OriginalDiscount, OriginalDate, NewDiscount, NewWolcenDate,
                               [Total12MonthQuantity], [Sales @ Original], [Sales @ New], NewException, NewSPG, TradePrice, UnitCost)
select r.RequestKey, 
       r.AccountKey, 
	   r.ItemGroup3Key, 
	   r.ItemKey, 
       COALESCE(csd.Discount, r.LastDiscount) as OriginalDiscount,
	   cast(r.CreationDate as date) as OriginalDate,
	   nsd.Discount as NewDiscount,
	   cast(NULL as date) as NewWolcenDate, 
	   cast(0 as decimal(19,8)) as [Total12MonthQuantity],
	   cast(0 as decimal(19,8)) as [Sales @ Original],
	   cast(0 as decimal(19,8)) as [Sales @ New],
	   CASE WHEN r.ItemKey = 1
	        THEN 0
	        ELSE IIF(r.CurrentExceptionDiscount IS NULL AND r.CurrentExceptionFixedPrice IS NULL, 1, 0)
	   END as NewException,
	   CASE WHEN r.ItemKey = 1
	        THEN IIF(r.CurrentSPGDiscount1 IS NULL AND r.CurrentSPGDiscount2 IS NULL, 1, 0)
	        ELSE 0
	   END as NewSPG,
	   IIF(r.ItemKey = 1, NULL, di.ItemUDDecimal1) as TradePrice,
	   IIF(r.ItemKey = 1, NULL, di.ItemUDDecimal3 - di.ItemUDDecimal4 - di.ItemUDDecimal5) AS UnitCost -- i.e., CurrentBranchCost - Padding - CurrNotSettVal
  from ppe.RequestMain r inner join
       dbo.DimItem di on r.ItemKey = di.Itemkey left outer join
       lion.PriceFlowTracker pft on r.RequestKey = pft.RequestKey
		CROSS APPLY ppe.fn_GetSimpleDiscount(r.CurrentExceptionFixedPrice, di.ItemUDDecimal1, r.CurrentExceptionDiscount, ISNULL(r.CurrentCompoundDiscount, 0)) csd
		CROSS APPLY ppe.fn_GetSimpleDiscount(r.NewExceptionFixedPrice, di.ItemUDDecimal1, r.NewExceptionDiscount, ISNULL(r.NewCompoundDiscount, 0)) nsd
 where pft.RequestKey is null and r.StatusKey = (SELECT StatusKey FROM ppe.RequestStatus WHERE StatusRollup = 'Exported')


update pft
   set pft.CurrentIndicator = 0
  from lion.PriceFlowTracker pft
 where pft.CurrentIndicator = 1

;with CurrPFT as (
select AccountKey, ItemGroup3Key, ItemKey, max(RequestKey) as RequestKey
  from lion.PriceFlowTracker
group by AccountKey, ItemGroup3Key, ItemKey
)

update pft
   set pft.CurrentIndicator = 1
  from lion.PriceFlowTracker pft inner join 
       CurrPFT on pft.RequestKey = CurrPFT.RequestKey

update pft
   set pft.NewWolcenDate = ISNULL(ct.LastChangeDate, DATEADD(dd, 1, pft.OriginalDate)),
       pft.Processed = 0
  from lion.PriceFlowTracker pft (nolock)
       JOIN dbo.DimItem di on pft.ItemKey = di.ItemKey
       LEFT JOIN lion.PFCustomerTerms ct ON ct.AccountKey = pft.AccountKey AND ct.ItemGroup3Key = pft.ItemGroup3Key AND
	                                   ct.ItemKey = pft.ItemKey
	CROSS APPLY ppe.fn_GetCompoundDiscount(ct.UsualDiscount1, ct.UsualDiscount2) cd 
	CROSS APPLY ppe.fn_GetSimpleDiscount(CASE WHEN pft.ItemKey = 1 THEN NULL ELSE ct.ExceptionFixedPrice END, di.ItemUDDecimal1, CASE WHEN pft.ItemKey = 1 THEN NULL ELSE ct.ExceptionDiscount/100 END,  ISNULL(cd.Discount/100, 0)) ctd
 where (pft.Processed = 0 or pft.NewWolcenDate is null) and 
       ABS(ctd.Discount - pft.NewDiscount) <= 0.002 -- Stuck

update pft
   set pft.NewWolcenDate = DATEADD(dd, 1, pft.OriginalDate)
  from lion.PriceFlowTracker pft
 where pft.NewWolcenDate <= pft.OriginalDate

update pft
   set OriginalDiscount = ISNULL(ROUND( 
                            ISNULL(
							    (SELECT sum(f.TradingPrice * ISNULL(f.TotalNetQuantity, f.TotalQuantity) - ISNULL(f.TotalNetPrice, f.TotalActualPrice))/nullif(sum(f.TradingPrice * ISNULL(f.TotalNetQuantity, ISNULL(f.TotalNetQuantity, f.TotalQuantity))), 0)
								   FROM lion.vwFactInvoiceLine f inner join
								        dbo.dimday dd on f.InvoiceDateDayKey = dd.DayKey inner join
								        lion.vwFactInvoiceLineGroup1 ilg1 on f.InvoiceLineGroup1Key = ilg1.InvoiceLineGroup1Key
								  WHERE f.Accountkey = pft.AccountKey and
								        f.ItemKey = pft.ItemKey and
								        dd.SQLDate < pft.NewWolcenDate and ilg1.ShipmentType = 'S'),
                                (SELECT OriginalDiscount 
								   FROM lion.PriceFlowTracker ipft 
								  WHERE ipft.AccountKey = pft.AccountKey and
								        ipft.ItemGroup3Key = pft.ItemGroup3Key and
										ipft.ItemKey is null and
										ipft.CurrentIndicator = 1))
				   , 4), pft.OriginalDiscount)
 from lion.PriceFlowTracker pft
 where pft.Processed = 0 and pft.NewException = 1 and pft.NewWolcenDate is not null

update pft
   set OriginalDiscount = ISNULL(ROUND( 
                            ISNULL(
							    (SELECT sum(f.TradingPrice * ISNULL(f.TotalNetQuantity, ISNULL(f.TotalNetQuantity, f.TotalQuantity)) - ISNULL(f.TotalNetPrice, f.TotalActualPrice))/nullif(sum(f.TradingPrice * ISNULL(f.TotalNetQuantity, ISNULL(f.TotalNetQuantity, f.TotalQuantity))), 0)
								   from lion.vwFactInvoiceLine f INNER JOIN
										lion.vwFactInvoiceLineGroup1 ilg1 on f.InvoiceLineGroup1Key = ilg1.InvoiceLineGroup1Key INNER JOIN
										dbo.DimDay dd on f.InvoiceDateDayKey = dd.DayKey
								  where f.AccountKey = pft.AccountKey and f.ItemGroup3Key = pft.ItemGroup3Key and
								        f.PriceDerivation in ('DP','DDC','ADJ', 'MP')  and dd.SQLDate < pft.NewWolcenDate and
										ilg1.ShipmentType = 'S' 
								),
                                (SELECT OriginalDiscount 
								   FROM lion.PriceFlowTracker ipft 
								  WHERE ipft.AccountKey = pft.AccountKey and
								        ipft.ItemGroup3Key = pft.ItemGroup3Key and
										ipft.ItemKey is null and
										ipft.CurrentIndicator = 1))
				   , 4), pft.OriginalDiscount)
 from lion.PriceFlowTracker pft
 where pft.Processed = 0 and 
      pft.NewSPG = 1 and pft.NewWolcenDate is not null
 	   
 ;with exceptions as (
  select *
    from lion.PriceFlowTracker
   where Processed = 0
     and ItemKey <> 1
     and NewWolcenDate is not null
)

, benefits as (
 select f.AccountKey,
        f.ItemKey,
		sum(IIF(ISNULL(f.TotalNetQuantity, f.TotalQuantity) < 0, 0, ISNULL(f.TotalNetQuantity, f.TotalQuantity))) AS Total12MonthQuantity, 
        sum(IIF(ISNULL(f.TotalNetQuantity, f.TotalQuantity) < 0, 0, ISNULL(f.TotalNetQuantity, f.TotalQuantity))) * (1 - e.OriginalDiscount) * i.CurrBranchTradePrice AS [Sales @ Original],
        sum(IIF(ISNULL(f.TotalNetQuantity, f.TotalQuantity) < 0, 0, ISNULL(f.TotalNetQuantity, f.TotalQuantity))) * (1 - e.NewDiscount) * i.CurrBranchTradePrice AS [Sales @ New]
   from lion.vwFactInvoiceLine f inner join
        exceptions e on f.AccountKey = e.AccountKey and
		                f.ItemKey = e.ItemKey inner join
		lion.vwItem i on f.ItemKey = i.ItemKey inner join
		dbo.DimDay dd on f.InvoiceDateDayKey = dd.DayKey
  where f.PriceDerivation IN ('GT', 'OTG') and
        dd.SQLDate between DATEADD(yy, -1, e.NewWolcenDate) and e.NewWolcenDate
 group by f.AccountKey, f.ItemKey, e.OriginalDiscount, e.NewDiscount, i.CurrBranchTradePrice
)

update pft
   set pft.Total12MonthQuantity = b.Total12MonthQuantity,
       pft.[Sales @ Original] = b.[Sales @ Original],
	   pft.[Sales @ New] = b.[Sales @ New]
  from lion.PriceFlowTracker pft inner join
       benefits b on pft.AccountKey = b.AccountKey and
	                 pft.ItemKey = b.ItemKey
 where pft.Processed = 0

;with spgterms as (
  select *
    from lion.PriceFlowTracker
   where ItemKey = 1
     and NewWolcenDate is not null
	 and Processed = 0
)

, exceptions as (
  select *
    from lion.PriceFlowTracker
   where ItemKey <> 1
     and NewWolcenDate is not null
	 and Processed = 0
)

, benefits as (
 select f.AccountKey,
        f.ItemGroup3Key,
		sum(ISNULL(f.TotalNetQuantity, f.TotalQuantity)) as Total12MonthQuantity, 
        sum(ISNULL(f.TotalNetQuantity, f.TotalQuantity) * (1 - s.OriginalDiscount) * i.CurrBranchTradePrice) as [Sales @ Original],
        sum(ISNULL(f.TotalNetQuantity, f.TotalQuantity) * (1 - s.NewDiscount) * i.CurrBranchTradePrice) as [Sales @ New]
   from lion.vwFactInvoiceLine f inner join
		lion.vwItem i on f.ItemKey = i.ItemKey inner join
        spgterms s on f.AccountKey = s.AccountKey and
		              f.ItemGroup3Key = s.ItemGroup3Key inner join
		dbo.DimDay dd on f.InvoiceDateDayKey = dd.DayKey left outer join
		exceptions e on f.AccountKey = e.AccountKey and f.ItemKey = e.ItemKey
  where f.PriceDerivation IN ('GT', 'OTG') and
        dd.SQLDate between DATEADD(yy, -1, s.NewWolcenDate) and s.NewWolcenDate and
		e.AccountKey is null -- i.e., not exists (select * from exceptions e where e.AccountKey = f.AccountKey and e.ItemKey = f.ItemKey)
 group by f.AccountKey, f.ItemGroup3Key
)

update pft
   set pft.Total12MonthQuantity = b.Total12MonthQuantity,
       pft.[Sales @ Original] = b.[Sales @ Original],
	   pft.[Sales @ New] = b.[Sales @ New]
  from lion.PriceFlowTracker pft inner join
       benefits b on pft.AccountKey = b.AccountKey and
	                 pft.ItemGroup3Key = b.ItemGroup3Key and
					 pft.ItemKey = 1
 where pft.Processed = 0

 update opft
    set opft.LastInvoiceDateDayKey = dd.DayKey
   from lion.PriceFlowTracker opft inner join
        lion.PriceFlowTracker npft on opft.AccountKey = npft.AccountKey and
		                         opft.ItemGroup3Key = npft.ItemGroup3Key and
								 opft.ItemKey = npft.ItemKey and
								 opft.Processed = 1 and
								 npft.Processed = 0 inner join
		dbo.DimDay dd on dd.SQLDate = npft.NewWolcenDate

update lion.PriceFlowTracker
   set Processed = 1
 where Processed = 0

delete from lion.PriceFlowBenefits

INSERT INTO lion.PriceFlowBenefits (ClientInvoicelineUniqueID, RequestKey, [RuleCode], [Actual Benefit],  [Potential Benefit])
SELECT f.ClientInvoicelineUniqueID,
       pft.RequestKey,

	   /*
			Excluded - Transaction not impacted by PriceFlow				
					Exclude transaction from benefit calculations (Test sequence is first)		
				E1	Price Derivation code = DC (Default contracts are not impacted by PriceFlow/Terms)		
				E2	Price Derivation code = DP (Discount Price is from converting a Quote,  is not impacted by PriceFlow/Terms)		
				E3	Big change in trade price (Data may not be reliable)		
				E4	Big change in cost price (Data may not be reliable)		
				E5	We could not determine what the prior price would have been, so we don’t know if the new price is better or worse
				E6  Price derivations other than GT, OTG, ADJ		
				
			Excluded - Local Business Decision to meet competition				
				D1	PriceFlow recommended a benefit, the benefit was accepted, (terms were updated), but the transaction was billed at a loss		
				D2	PriceFlow recommended a benefit, a loss was accepted (terms were updated to be worse than before), and the transaction was billed at a loss		
				
			No Change		No Change as a result of PriceFlow (test sequence is 2nd); (+/- 0.2%)		
				Z1	No change made: Accepted Discount (20%)  = Original Discount (20%) = Actual Discount (20%) 		
				Z2	PriceFlow Change Ignored:  Original Discount (25%) = Actual Discount (25%) , Accepted Discount (not 25%) 		
				
			Benefit		Price Flow recommended a Benefit: i.e.:  Accepted change is positive (Accepted discount is better than Original in PriceFlow)(ex: Original=36%, Accepted = 32%)  AND …		
				B1	Accepted Change Stuck --> Actual Discount  (32%) = Accepted Discount (32%) ;   (+/- 0.2%)		
				B2	Actual was better than Accepted --> Actual Discount (30%) <  Accepted Discount (32%) 		
				B3	Actual was less then Accepted,  but still positive --> Actual Discount (35%) > Accepted Discount (32%) & Actual Discount (35%) < Original Discount (36%)		
				
			Loss		Price Flow recommended a Loss		
				L1	Accepted Change Stuck -->  Actual Discount (72%) = Accepted discount (72%); (+/- 0.2%)		
				L2	Actual was better, but still a loss  --> Actual Discount (60%) < Accepted Discount (72%) & Actual Discount (60%) > Original Discount (50%)		
				L3	Actual was worse --> Actual Discount (80%) > Accepted Discount (72%)		
				L4	Actual was better than Original --> Actual Discount (40%) < Original Discount (50%) (Transaction benefit is positive, but terms are still on file as a loss)		
					We expect this to be unusual, and likely indicate that the terms are incorrect		

			Anything else
			    UNK Catch all bucket for anything else
	   */
	   CASE WHEN f.PriceDerivation = 'DC' 
	        THEN 'E1'
	        WHEN f.PriceDerivation = 'DP' 
			THEN 'E2'
			WHEN f.PriceDerivation IN ('GT', 'OTG', 'ADJ') AND pft.ItemKey <> 1 AND 
			     ABS(tm.GPP - pft.TradeMargin) >= @MarginChangeThreshold AND 
			     ((f.TradingPrice - pft.TradePrice) / NULLIF(pft.TradePrice, 0)) > 
				   ((ISNULL(f.TotalNetCost, f.TotalActualCost)/NULLIF(ISNULL(f.TotalNetQuantity, f.TotalQuantity),0) - pft.UnitCost) / 
					     NULLIF(pft.UnitCost, 0))
			THEN 'E3'
			WHEN f.PriceDerivation IN ('GT', 'OTG', 'ADJ') AND pft.ItemKey <> 1 AND 
			     ABS(tm.GPP - pft.TradeMargin) >= @MarginChangeThreshold AND 
			     ((f.TradingPrice - pft.TradePrice) / NULLIF(pft.TradePrice, 0)) <
				   ((ISNULL(f.TotalNetCost, f.TotalActualCost)/NULLIF(ISNULL(f.TotalNetQuantity, f.TotalQuantity),0) - pft.UnitCost) / 
					     NULLIF(pft.UnitCost, 0))
			THEN 'E4'
			WHEN pft.OriginalDiscount IS NULL
			THEN 'E5'
			WHEN f.PriceDerivation NOT IN ('GT', 'OTG', 'ADJ') 
			THEN 'E6'
			WHEN (f.TradingPrice IS NULL OR f.TradingPrice <= 0) AND SpecialInd = 'Y'
            THEN 'E7'                                                                                                                                            --Special & No Trade Price
            WHEN (f.TradingPrice IS NULL OR f.TradingPrice <= 0) AND SpecialInd = 'N'
            THEN 'E8'
			WHEN ROUND(pft.OriginalDiscount, 3) > ROUND(ISNULL(r.ProposedExceptionDiscount, r.ProposedCompoundDiscount), 3) AND									-- PriceFlow recommended a benefit
			     ABS(ROUND(ISNULL(r.ProposedExceptionDiscount, r.ProposedCompoundDiscount), 3) - ROUND(pft.NewDiscount, 3)) < 0.002 AND							-- Benefit was accepted	
				 ( (ROUND(pft.OriginalDiscount, 4) - ROUND((ISNULL(f.TotalNetQuantity, f.TotalQuantity) * f.TradingPrice -			
				    ISNULL(f.TotalNetPrice, f.TotalActualPrice)) / NULLIF((ISNULL(f.TotalNetQuantity, f.TotalQuantity) * f.TradingPrice), 0), 4)) < -0.002)		-- Actual was a loss
	        THEN 'D1'
			WHEN ROUND(pft.OriginalDiscount, 3) > ROUND(ISNULL(r.ProposedExceptionDiscount, r.ProposedCompoundDiscount), 3) AND									-- PriceFlow recommended a benefit
			     ROUND(pft.NewDiscount, 3) > ROUND(pft.OriginalDiscount, 3) AND																					-- A loss was accepted instead	
				 ( (ROUND(pft.OriginalDiscount, 4) - ROUND((ISNULL(f.TotalNetQuantity, f.TotalQuantity) * f.TradingPrice - 
				    ISNULL(f.TotalNetPrice, f.TotalActualPrice)) / NULLIF((ISNULL(f.TotalNetQuantity, f.TotalQuantity) * f.TradingPrice), 0), 4)) < -0.002)		-- Actual was a loss
	        THEN 'D2'
			WHEN ROUND(pft.OriginalDiscount, 3) = ROUND(pft.NewDiscount, 3) AND																					-- No change made in PriceFlow
				 ABS( (ROUND(pft.OriginalDiscount, 4) - ROUND((ISNULL(f.TotalNetQuantity, f.TotalQuantity) * f.TradingPrice -			
				    ISNULL(f.TotalNetPrice, f.TotalActualPrice)) / NULLIF((ISNULL(f.TotalNetQuantity, f.TotalQuantity) * f.TradingPrice), 0), 4))) <= 0.002		-- Actual discount = original
	        THEN 'Z1'
			WHEN ROUND(pft.OriginalDiscount, 3) <> ROUND(pft.NewDiscount, 3) AND																				-- Accepted different discount...
				 ABS( (ROUND(pft.OriginalDiscount, 4) - ROUND((ISNULL(f.TotalNetQuantity, f.TotalQuantity) * f.TradingPrice -			
				    ISNULL(f.TotalNetPrice, f.TotalActualPrice)) / NULLIF((ISNULL(f.TotalNetQuantity, f.TotalQuantity) * f.TradingPrice), 0), 4))) <= 0.002		-- ...but sold at original discount
	        THEN 'Z2'
			
			WHEN ROUND(pft.OriginalDiscount, 3) > ROUND(pft.NewDiscount, 3) AND																					-- Accepted a benefit
				 ABS( (ROUND(pft.NewDiscount, 4) - ROUND((ISNULL(f.TotalNetQuantity, f.TotalQuantity) * f.TradingPrice -			
				    ISNULL(f.TotalNetPrice, f.TotalActualPrice)) / NULLIF((ISNULL(f.TotalNetQuantity, f.TotalQuantity) * f.TradingPrice), 0), 4))) <= 0.002		-- Sold at accepted discount
	        THEN 'B1'
			WHEN ROUND(pft.OriginalDiscount, 3) > ROUND(pft.NewDiscount, 3) AND																					-- Accepted a benefit
				 (ROUND(pft.NewDiscount, 4) - ROUND((ISNULL(f.TotalNetQuantity, f.TotalQuantity) * f.TradingPrice -			
				    ISNULL(f.TotalNetPrice, f.TotalActualPrice)) / NULLIF((ISNULL(f.TotalNetQuantity, f.TotalQuantity) * f.TradingPrice), 0), 4)) > 0.002		-- Sold at better than accepted discount
	        THEN 'B2'
			WHEN ROUND(pft.OriginalDiscount, 3) > ROUND(pft.NewDiscount, 3) AND																					-- Accepted a benefit
				 (ROUND(pft.OriginalDiscount, 4) - ROUND((ISNULL(f.TotalNetQuantity, f.TotalQuantity) * f.TradingPrice -			
				    ISNULL(f.TotalNetPrice, f.TotalActualPrice)) / NULLIF((ISNULL(f.TotalNetQuantity, f.TotalQuantity) * f.TradingPrice), 0), 4)) > 0.002		-- Sold at better than original discount
	        THEN 'B3'
			WHEN ROUND(pft.OriginalDiscount, 3) < ROUND(ISNULL(r.ProposedExceptionDiscount, r.ProposedCompoundDiscount), 3) AND									-- PriceFlow recommended a loss
				 ABS( (ROUND(pft.NewDiscount, 4) - ROUND((ISNULL(f.TotalNetQuantity, f.TotalQuantity) * f.TradingPrice -			
				    ISNULL(f.TotalNetPrice, f.TotalActualPrice)) / NULLIF((ISNULL(f.TotalNetQuantity, f.TotalQuantity) * f.TradingPrice), 0), 4))) <= 0.002		-- Sold at accepted discount
	        THEN 'L1'
			WHEN ROUND(pft.OriginalDiscount, 3) < ROUND(ISNULL(r.ProposedExceptionDiscount, r.ProposedCompoundDiscount), 3) AND									-- PriceFlow recommended a loss
				 (ROUND(pft.NewDiscount, 4) - ROUND((ISNULL(f.TotalNetQuantity, f.TotalQuantity) * f.TradingPrice -			
				    ISNULL(f.TotalNetPrice, f.TotalActualPrice)) / NULLIF((ISNULL(f.TotalNetQuantity, f.TotalQuantity) * f.TradingPrice), 0), 4)) > 0.002 AND	-- Sold at better than accepted discount
				 ( (ROUND(pft.OriginalDiscount, 4) - ROUND((ISNULL(f.TotalNetQuantity, f.TotalQuantity) * f.TradingPrice - 
				    ISNULL(f.TotalNetPrice, f.TotalActualPrice)) / NULLIF((ISNULL(f.TotalNetQuantity, f.TotalQuantity) * f.TradingPrice), 0), 4)) < -0.002)		-- Actual was a loss
	        THEN 'L2'
			WHEN ROUND(pft.OriginalDiscount, 3) < ROUND(ISNULL(r.ProposedExceptionDiscount, r.ProposedCompoundDiscount), 3) AND									-- PriceFlow recommended a loss
				 (ROUND(pft.NewDiscount, 4) - ROUND((ISNULL(f.TotalNetQuantity, f.TotalQuantity) * f.TradingPrice -			
				    ISNULL(f.TotalNetPrice, f.TotalActualPrice)) / NULLIF((ISNULL(f.TotalNetQuantity, f.TotalQuantity) * f.TradingPrice), 0), 4)) < 0.002		-- Sold at worse than accepted discount
	        THEN 'L3'
			WHEN ROUND(pft.OriginalDiscount, 3) < ROUND(ISNULL(r.ProposedExceptionDiscount, r.ProposedCompoundDiscount), 3) AND									-- PriceFlow recommended a loss
				 (ROUND(pft.OriginalDiscount, 4) - ROUND((ISNULL(f.TotalNetQuantity, f.TotalQuantity) * f.TradingPrice -			
				    ISNULL(f.TotalNetPrice, f.TotalActualPrice)) / NULLIF((ISNULL(f.TotalNetQuantity, f.TotalQuantity) * f.TradingPrice), 0), 4)) > 0.002		-- Sold at better than original discount
	        THEN 'L4'
			ELSE 'Unk'
	   END AS [RuleCode],
	   CASE WHEN f.PriceDerivation IN ('GT', 'OTG', 'ADJ')
	        THEN ISNULL(f.TotalNetQuantity, f.TotalQuantity) * f.TradingPrice * (pft.OriginalDiscount - ((ISNULL(f.TotalNetQuantity, f.TotalQuantity) * f.TradingPrice - ISNULL(f.TotalNetPrice, f.TotalActualPrice)) / NULLIF((ISNULL(f.TotalNetQuantity, f.TotalQuantity) * f.TradingPrice), 0)))
			ELSE 0
	   END AS [Actual Benefit],
	   ISNULL(f.TotalNetQuantity, f.TotalQuantity) * f.TradingPrice * (pft.OriginalDiscount - pft.NewDiscount) AS [Potential Benefit]
  FROM lion.vwFactInvoiceLine f (NOLOCK) INNER JOIN
	   lion.vwItem i (NOLOCK) ON f.ItemKey = i.ItemKey INNER JOIN
       lion.PriceFlowTracker pft ON (pft.AccountKey = f.AccountKey AND
		                        ISNULL(IIF(pft.ItemKey=1,NULL,pft.ItemKey), f.ItemKey) = f.ItemKey AND
								CASE WHEN EXISTS (SELECT * 
								              FROM lion.PriceFlowTracker ipft
											 WHERE ipft.AccountKey = f.AccountKey AND
											       ipft.ItemKey = f.ItemKey) AND
											       pft.ItemKey = 1
									 THEN 0
									 ELSE pft.ItemGroup3Key
								 END = f.ItemGroup3Key
					           ) INNER JOIN
	   dbo.DimDay dd ON f.InvoiceDateDayKey = dd.DayKey INNER JOIN
	   ppe.RequestMain r ON pft.RequestKey = r.RequestKey
	   CROSS APPLY lion.fn_GetGPP(i.CurrBranchTradePrice, i.CurrentBranchCost - i.Padding - i.CurrNotSettVal) tm
 WHERE dd.SQLDate >= pft.NewWolcenDate



GO
