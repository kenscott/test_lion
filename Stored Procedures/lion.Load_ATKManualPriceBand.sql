SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE [lion].[Load_ATKManualPriceBand]
AS
/*

BEGIN TRAN
EXEC lion.Load_ATKManualPriceBand
EXEC DCPLOG
ROLLBACK TRAN

SELECT TOP 1000 * FROM lion.ATKManualPriceBand
SELECT TOP 10 * FROM lion.ATK ManualPriceBandExpansion

*/


/*
*/

SET NOCOUNT ON

EXEC LogDCPEvent 'DCP - Load_ATKManualPriceBand', 'B'


DECLARE 
	@RowsI Quantity_Normal_type, 
	@RowsU Quantity_Normal_type, 
	@RowsD Quantity_Normal_type,
	@TodayDayKey Key_Small_Type

SET @RowsI = 0
SET @RowsU = 0
SET @RowsD = 0



EXEC LogDCPEvent 'DCP - Load_ATKManualPriceBand: Archiving Expiring MPBs', 'B'

INSERT lion.ATKManualPriceBandArchive (
	ManualPriceBandKey,
	ItemPyramidCode,
	LLSPGCode,
	ProductCode,
	PriceApproach,
	ContractClaimsIndicator,
	Region,
	Area,
	Network,
	Branch,
	FloorGPP,
	TargetGPP,
	StretchGPP,
	ExpirationDate,
	CreationDate,
	CreatedByWebUserKey,
	ModificationDate,
	ModifiedByWebUserKey
)
SELECT DISTINCT
	ManualPriceBandKey,
	ItemPyramidCode,
	LLSPGCode,
	ProductCode,
	PriceApproach,
	ContractClaimsIndicator,
	Region,
	Area,
	Network,
	Branch,
	FloorGPP,
	TargetGPP,
	StretchGPP,
	ExpirationDate,
	CreationDate,
	CreatedByWebUserKey,
	ModificationDate,
	ModifiedByWebUserKey
FROM Lion.ATKManualPriceBand mpb
WHERE 
	ExpirationDate IS NOT NULL 
	AND ExpirationDate <= GETDATE()
	AND NOT EXISTS (SELECT 1 FROM lion.ATKManualPriceBandArchive a where a.ManualPriceBandKey = mpb.ManualPriceBandKey)
SET @RowsI = @RowsI + @@RowCount


DELETE
FROM Lion.ATKManualPriceBand
WHERE 
	ExpirationDate IS NOT NULL 
	AND ExpirationDate <= GETDATE()
SET @RowsD = @RowsD + @@RowCount


EXEC LogDCPEvent 'DCP - Load_ATKManualPriceBand: Archiving Expiring MPBs', 'E', @RowsI, @RowsU, @RowsD


-- UPDATE Lion.ATKManualPriceBand SET EXPIRATIONDATE = GETDATE() + 1 WHERE ManualPriceBandKey = 24
-- UPDATE Lion.ATKManualPriceBand SET EXPIRATIONDATE = GETDATE() + 120 WHERE ManualPriceBandKey = 24



--EXEC LogDCPEvent 'DCP - Load_ATKManualPriceBand: Inserting ATK ManualPriceBandExpansion', 'B'

SET @RowsI = 0
SET @RowsU = 0
SET @RowsD = 0

--EXEC DBA_IndexRebuild 'ATK ManualPriceBandExpansion', 'Drop', null, 'lion'

--TRUNCATE TABLE lion.ATK ManualPriceBandExpansion


--; WITH PriceApproachData AS (
--	SELECT DISTINCT 
--		UDVarchar10 AS PriceApproach
--	FROM dbo.FactLastPriceAndCost
--), ContractClaimsIndicatorData AS (
--	SELECT DISTINCT 
--		InvoiceLineGroup1UDVarchar2 AS ContractClaimsIndicator
--	FROM dbo.DimInvoiceLineGroup1
--	WHERE InvoiceLineGroup1UDVarchar2 <> N'Unknown'
--), GeographicData AS (
--	SELECT DISTINCT
--		AG1Level4 AS Region,
--		AG1Level3 AS Area,
--		AG1Level2 AS Network,
--		AG1Level1 AS Branch
--	FROM dbo.DimAccount da
--	INNER JOIN dbo.DimAccountGroup1 dag1
--		ON dag1.AccountGroup1Key = da.AccountGroup1Key
--	WHERE AG1Level2 <> N'Unknown'
--), ExpansionData AS (
--	SELECT DISTINCT
--		di.ItemKey,
--		PriceApproachData.PriceApproach,
--		ContractClaimsIndicatorData.ContractClaimsIndicator,
--		GeographicData.Region,
--		GeographicData.Area,
--		GeographicData.Network,
--		GeographicData.Branch,
--		mpb.FloorGPP,
--		mpb.TargetGPP,
--		mpb.StretchGPP,
--		mpb.ModificationDate AS ExpirationDate,
--		ROW_NUMBER() OVER (
--			PARTITION BY
--				di.ItemKey,
--				PriceApproachData.PriceApproach,
--				ContractClaimsIndicatorData.ContractClaimsIndicator,
--				GeographicData.Region,
--				GeographicData.Area,
--				GeographicData.Network,
--				GeographicData.Branch
--			ORDER BY 
--				CASE
--					WHEN mpb.ProductCode <> N'ALL' AND mpb.PriceApproach <> N'ALL' AND mpb.ContractClaimsIndicator <> N'ALL' AND mpb.Region <> N'ALL' AND mpb.Area <> N'ALL' AND mpb.Network <> N'ALL' THEN 1
--					WHEN mpb.ProductCode <> N'ALL' AND mpb.PriceApproach <> N'ALL' AND mpb.ContractClaimsIndicator <> N'ALL' AND mpb.Region = N'ALL' AND mpb.Area <> N'ALL' AND mpb.Network <> N'ALL' THEN 2
--					WHEN mpb.ProductCode <> N'ALL' AND mpb.PriceApproach <> N'ALL' AND mpb.ContractClaimsIndicator <> N'ALL' AND mpb.Region = N'ALL' AND mpb.Area = N'ALL' AND mpb.Network <> N'ALL' THEN 3
--					WHEN mpb.ProductCode <> N'ALL' AND mpb.PriceApproach <> N'ALL' AND mpb.ContractClaimsIndicator <> N'ALL' AND mpb.Region = N'ALL' AND mpb.Area = N'ALL' AND mpb.Network = N'ALL' THEN 4
--					WHEN mpb.ProductCode <> N'ALL' AND mpb.PriceApproach <> N'ALL' AND mpb.ContractClaimsIndicator = N'ALL' AND mpb.Region <> N'ALL' AND mpb.Area <> N'ALL' AND mpb.Network <> N'ALL' THEN 5
--					WHEN mpb.ProductCode <> N'ALL' AND mpb.PriceApproach <> N'ALL' AND mpb.ContractClaimsIndicator = N'ALL' AND mpb.Region = N'ALL' AND mpb.Area <> N'ALL' AND mpb.Network <> N'ALL' THEN 6
--					WHEN mpb.ProductCode <> N'ALL' AND mpb.PriceApproach <> N'ALL' AND mpb.ContractClaimsIndicator = N'ALL' AND mpb.Region = N'ALL' AND mpb.Area = N'ALL' AND mpb.Network <> N'ALL' THEN 7
--					WHEN mpb.ProductCode <> N'ALL' AND mpb.PriceApproach <> N'ALL' AND mpb.ContractClaimsIndicator = N'ALL' AND mpb.Region = N'ALL' AND mpb.Area = N'ALL' AND mpb.Network = N'ALL' THEN 8
--					WHEN mpb.ProductCode <> N'ALL' AND mpb.PriceApproach = N'ALL' AND mpb.ContractClaimsIndicator <> N'ALL' AND mpb.Region <> N'ALL' AND mpb.Area <> N'ALL' AND mpb.Network <> N'ALL' THEN 9
--					WHEN mpb.ProductCode <> N'ALL' AND mpb.PriceApproach = N'ALL' AND mpb.ContractClaimsIndicator <> N'ALL' AND mpb.Region = N'ALL' AND mpb.Area <> N'ALL' AND mpb.Network <> N'ALL' THEN 10
--					WHEN mpb.ProductCode <> N'ALL' AND mpb.PriceApproach = N'ALL' AND mpb.ContractClaimsIndicator <> N'ALL' AND mpb.Region = N'ALL' AND mpb.Area = N'ALL' AND mpb.Network <> N'ALL' THEN 11
--					WHEN mpb.ProductCode <> N'ALL' AND mpb.PriceApproach = N'ALL' AND mpb.ContractClaimsIndicator <> N'ALL' AND mpb.Region = N'ALL' AND mpb.Area = N'ALL' AND mpb.Network = N'ALL' THEN 12
--					WHEN mpb.ProductCode <> N'ALL' AND mpb.PriceApproach = N'ALL' AND mpb.ContractClaimsIndicator = N'ALL' AND mpb.Region <> N'ALL' AND mpb.Area <> N'ALL' AND mpb.Network <> N'ALL' THEN 13
--					WHEN mpb.ProductCode <> N'ALL' AND mpb.PriceApproach = N'ALL' AND mpb.ContractClaimsIndicator = N'ALL' AND mpb.Region = N'ALL' AND mpb.Area <> N'ALL' AND mpb.Network <> N'ALL' THEN 14
--					WHEN mpb.ProductCode <> N'ALL' AND mpb.PriceApproach = N'ALL' AND mpb.ContractClaimsIndicator = N'ALL' AND mpb.Region = N'ALL' AND mpb.Area = N'ALL' AND mpb.Network <> N'ALL' THEN 15
--					WHEN mpb.ProductCode <> N'ALL' AND mpb.PriceApproach = N'ALL' AND mpb.ContractClaimsIndicator = N'ALL' AND mpb.Region = N'ALL' AND mpb.Area = N'ALL' AND mpb.Network = N'ALL' THEN 16
--					WHEN mpb.ProductCode = N'ALL' AND mpb.PriceApproach <> N'ALL' AND mpb.ContractClaimsIndicator <> N'ALL' AND mpb.Region <> N'ALL' AND mpb.Area <> N'ALL' AND mpb.Network <> N'ALL' THEN 17
--					WHEN mpb.ProductCode = N'ALL' AND mpb.PriceApproach <> N'ALL' AND mpb.ContractClaimsIndicator <> N'ALL' AND mpb.Region = N'ALL' AND mpb.Area <> N'ALL' AND mpb.Network <> N'ALL' THEN 18
--					WHEN mpb.ProductCode = N'ALL' AND mpb.PriceApproach <> N'ALL' AND mpb.ContractClaimsIndicator <> N'ALL' AND mpb.Region = N'ALL' AND mpb.Area = N'ALL' AND mpb.Network <> N'ALL' THEN 19
--					WHEN mpb.ProductCode = N'ALL' AND mpb.PriceApproach <> N'ALL' AND mpb.ContractClaimsIndicator <> N'ALL' AND mpb.Region = N'ALL' AND mpb.Area = N'ALL' AND mpb.Network = N'ALL' THEN 20
--					WHEN mpb.ProductCode = N'ALL' AND mpb.PriceApproach <> N'ALL' AND mpb.ContractClaimsIndicator = N'ALL' AND mpb.Region <> N'ALL' AND mpb.Area <> N'ALL' AND mpb.Network <> N'ALL' THEN 21
--					WHEN mpb.ProductCode = N'ALL' AND mpb.PriceApproach <> N'ALL' AND mpb.ContractClaimsIndicator = N'ALL' AND mpb.Region = N'ALL' AND mpb.Area <> N'ALL' AND mpb.Network <> N'ALL' THEN 22
--					WHEN mpb.ProductCode = N'ALL' AND mpb.PriceApproach <> N'ALL' AND mpb.ContractClaimsIndicator = N'ALL' AND mpb.Region = N'ALL' AND mpb.Area = N'ALL' AND mpb.Network <> N'ALL' THEN 23
--					WHEN mpb.ProductCode = N'ALL' AND mpb.PriceApproach <> N'ALL' AND mpb.ContractClaimsIndicator = N'ALL' AND mpb.Region = N'ALL' AND mpb.Area = N'ALL' AND mpb.Network = N'ALL' THEN 24
--					WHEN mpb.ProductCode = N'ALL' AND mpb.PriceApproach = N'ALL' AND mpb.ContractClaimsIndicator <> N'ALL' AND mpb.Region <> N'ALL' AND mpb.Area <> N'ALL' AND mpb.Network <> N'ALL' THEN 25
--					WHEN mpb.ProductCode = N'ALL' AND mpb.PriceApproach = N'ALL' AND mpb.ContractClaimsIndicator <> N'ALL' AND mpb.Region = N'ALL' AND mpb.Area <> N'ALL' AND mpb.Network <> N'ALL' THEN 26
--					WHEN mpb.ProductCode = N'ALL' AND mpb.PriceApproach = N'ALL' AND mpb.ContractClaimsIndicator <> N'ALL' AND mpb.Region = N'ALL' AND mpb.Area = N'ALL' AND mpb.Network <> N'ALL' THEN 27
--					WHEN mpb.ProductCode = N'ALL' AND mpb.PriceApproach = N'ALL' AND mpb.ContractClaimsIndicator <> N'ALL' AND mpb.Region = N'ALL' AND mpb.Area = N'ALL' AND mpb.Network = N'ALL' THEN 28
--					WHEN mpb.ProductCode = N'ALL' AND mpb.PriceApproach = N'ALL' AND mpb.ContractClaimsIndicator = N'ALL' AND mpb.Region <> N'ALL' AND mpb.Area <> N'ALL' AND mpb.Network <> N'ALL' THEN 29
--					WHEN mpb.ProductCode = N'ALL' AND mpb.PriceApproach = N'ALL' AND mpb.ContractClaimsIndicator = N'ALL' AND mpb.Region = N'ALL' AND mpb.Area <> N'ALL' AND mpb.Network <> N'ALL' THEN 30
--					WHEN mpb.ProductCode = N'ALL' AND mpb.PriceApproach = N'ALL' AND mpb.ContractClaimsIndicator = N'ALL' AND mpb.Region = N'ALL' AND mpb.Area = N'ALL' AND mpb.Network <> N'ALL' THEN 31
--					WHEN mpb.ProductCode = N'ALL' AND mpb.PriceApproach = N'ALL' AND mpb.ContractClaimsIndicator = N'ALL' AND mpb.Region = N'ALL' AND mpb.Area = N'ALL' AND mpb.Network = N'ALL' THEN 32
--				END ASC,
--				mpb.ManualPriceBandKey DESC
--			) AS RowRank
--	FROM lion.ATKManualPriceBand mpb
--	INNER JOIN dbo.DimItemGroup3 dig3
--		ON dig3.IG3Level4 = mpb.ItemPyramidCode
--		AND dig3.IG3Level1 = mpb.LLSPGCode
--	INNER JOIN dbo.DimItem di
--		ON dig3.ItemGroup3Key = di.ItemGroup3Key
--		AND (di.ItemNumber = mpb.ItemPyramidCode + mpb.ProductCode OR mpb.ProductCode = N'ALL')
--	INNER JOIN PriceApproachData
--		ON (PriceApproachData.PriceApproach = mpb.PriceApproach OR mpb.PriceApproach = N'ALL')
--	INNER JOIN ContractClaimsIndicatorData
--		ON (ContractClaimsIndicatorData.ContractClaimsIndicator = mpb.ContractClaimsIndicator OR mpb.ContractClaimsIndicator = N'ALL')
--	INNER JOIN GeographicData
--		ON  (GeographicData.Region = mpb.Region OR mpb.Region = N'ALL')
--		AND (GeographicData.Area = mpb.Area OR mpb.Area = N'ALL')
--		AND (GeographicData.Network = mpb.Network OR mpb.Network = N'ALL')
--		AND (GeographicData.Branch = mpb.Branch OR mpb.Branch = N'ALL')
--)
--INSERT INTO lion.ATK ManualPriceBandExpansion (
--	ItemKey,
--	PriceApproach,
--	ContractClaimsIndicator,
--	Region,
--	Area,
--	Network,
--	Branch,
--	FloorGPP,
--	TargetGPP,
--	StretchGPP
--)
--SELECT
--	ItemKey,
--	PriceApproach,
--	ContractClaimsIndicator,
--	Region,
--	Area,
--	Network,
--	Branch,
--	FloorGPP,
--	TargetGPP,
--	StretchGPP
--FROM
--	ExpansionData
--WHERE
--	RowRank = 1
--SET @RowsI = @RowsI + @@RowCount


--EXEC DBA_IndexRebuild 'ATK ManualPriceBandExpansion', 'Create', null, 'lion'

--EXEC LogDCPEvent 'DCP - Load_ATKManualPriceBand: Inserting ATK ManualPriceBandExpansion', 'E', @RowsI, @RowsU, @RowsD



EXEC LogDCPEvent 'DCP - Load_ATKManualPriceBand', 'E'


GO
