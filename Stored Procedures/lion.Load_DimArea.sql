SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [lion].[Load_DimArea]
WITH EXECUTE AS CALLER
AS
SET NOCOUNT ON


EXEC LogDCPEvent 'ETL - Load_DimArea', 'B'


DECLARE 
	@RowsI INT, 
	@RowsU INT, 
	@RowsD INT,
	@BeginRowCount INT,
	@TodayDayKey Key_Small_type

SET @RowsI = 0
SET @RowsU = 0
SET @RowsD = 0

SET @TodayDayKey = (SELECT DayKey FROM DimDay WHERE CONVERT(VARCHAR(10), SQLDate, 120) = CONVERT(VARCHAR(10), GETDATE(), 120))


 --EXEC DBA_IndexRebuild 'DimArea', 'Drop'
 --TRUNCATE TABLE dbo.DimArea
 --EXEC DBA_IndexRebuild 'DimArea', 'Create'
--EXEC lion.Load_DimArea

IF NOT EXISTS (SELECT 1 FROM dbo.DimArea WHERE Area = 'Unknown')
BEGIN
	INSERT dbo.DimArea (
		Area,
		AreaName,
		CreationDayKey, 
		ModificationDayKey)
	VALUES (
		'Unknown',
		'Unknown', 
		@TodayDayKey, 
		@TodayDayKey)
	SET @RowsI = @RowsI + @@RowCount
END


SELECT @BeginRowCount = COUNT(*) FROM dbo.DimArea

; WITH StagingData AS (
	SELECT DISTINCT 
	b.Area AS Area, 
	u.BranchName AS AreaName
	FROM Lion_Staging.dbo.BusUnit b
	INNER JOIN Lion_Staging.dbo.BusUnit u
	ON b.Area = u.BranchCode AND u.BusUnitTypeDesc = 'WOLCEN_AREA'
	WHERE b.Area <> ''

)
MERGE dbo.DimArea AS Tgt
USING 
	(
	SELECT 
		Area,
		AreaName
	FROM StagingData
) AS Src
ON Tgt.Area = Src.Area
WHEN MATCHED AND Tgt.AreaName <> Src.AreaName
THEN UPDATE 
	SET
		Tgt.ModificationDayKey = @TodayDayKey,
		Tgt.AreaName = Src.AreaName
WHEN NOT MATCHED 
	THEN INSERT (
		Area,
		AreaName,
		CreationDayKey, 
		ModificationDayKey
	)
	VALUES (
		Area,
		AreaName,
		@TodayDayKey, 
		@TodayDayKey
	)
;

SELECT 
	@RowsU = @RowsU + (@@ROWCOUNT - (COUNT(*) - @BeginRowCount)),
	@RowsI = @RowsI + (COUNT(*) - @BeginRowCount)
FROM dbo.DimArea


SELECT TOP 10 * FROM dbo.DimArea

EXEC LogDCPEvent 'ETL - Load_DimArea', 'E', @RowsI, @RowsU, @RowsD









GO
