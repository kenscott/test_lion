SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO








--MVIEWS_ProposalPrice_Drop
CREATE          procedure
[dbo].[MVIEWS_ProposalPrice_Create]
as

exec LogDCPEvent 'ETL - dbo.MVIEWS_ProposalPrice_Create', 'B'

declare @Cmd nvarchar(4000)

set @cmd = '
CREATE view ProposalPrice_Summary_Account
With SchemaBinding
as
Select 
	ProjectKey,
	PlaybookDataPointGroupKey,
	AccountKey,
	AccountManagerKey,
	AcceptanceCodeKey,
	UserAcceptanceCodeKey,
	DefaultAcceptanceCodeKey,
	Sum(TotalActual12MonthCost) as TotalActual12MonthCost,
	Sum(TotalActual12MonthPrice) as TotalActual12MonthPrice,
	Sum(TotalActual12MonthQuantity) as TotalActual12MonthQuantity,
	Sum(TotalActual12MonthFrequency) as TotalActual12MonthFrequency,
	Sum(ISNULL(Forecasted12MonthQuantity, 0)) as Forecasted12MonthQuantity,
	Sum(ISNULL(TotalForecasted12MonthCost_LastCost, 0)) as TotalForecasted12MonthCost_LastCost,
	Sum(ISNULL(TotalForecasted12MonthPrice_LastPrice, 0)) as TotalForecasted12MonthPrice_LastPrice,
	Sum(ISNULL(TotalForecasted12MonthPrice_ProposedPrice, 0)) as TotalForecasted12MonthPrice_ProposedPrice,
	Sum(ISNULL(TotalForecasted12MonthPrice_NewPrice, 0)) as TotalForecasted12MonthPrice_NewPrice,
	Sum(ISNULL(TotalForecasted12MonthCost_RelevantCost, 0)) as TotalForecasted12MonthCost_RelevantCost,
	Count_Big(*) as cnt
From dbo.ProposalPrice
Group by 
	ProjectKey, 
	PlaybookDataPointGroupKey,
	AccountKey,
	AccountManagerKey,
	AcceptanceCodeKey,
	UserAcceptanceCodeKey,
	DefaultAcceptanceCodeKey'

exec sp_executesql @cmd

Create unique clustered index UI_ProposalPrice_Summary_Account_1 on ProposalPrice_Summary_Account(ProjectKey, 	PlaybookDataPointGroupKey, AccountKey,	AccountManagerKey, AcceptanceCodeKey, 	UserAcceptanceCodeKey,	DefaultAcceptanceCodeKey)

Create index I_ProposalPrice_Summary_Account_1 on ProposalPrice_Summary_Account(
ProjectKey,
AccountKey,
AccountManagerKey,
TotalForecasted12MonthPrice_ProposedPrice,
TotalForecasted12MonthPrice_LastPrice)
---------------------------------------------------------------------------
set @cmd = '
CREATE view ProposalPrice_Summary_Item
With SchemaBinding
as
Select 
	ProjectKey,
	PlaybookDataPointGroupKey,
	ItemKey,
	AcceptanceCodeKey,
	UserAcceptanceCodeKey,
	DefaultAcceptanceCodeKey,
	Sum(TotalActual12MonthCost) as TotalActual12MonthCost,
	Sum(TotalActual12MonthPrice) as TotalActual12MonthPrice,
	Sum(TotalActual12MonthQuantity) as TotalActual12MonthQuantity,
	Sum(TotalActual12MonthFrequency) as TotalActual12MonthFrequency,
	Sum(ISNULL(Forecasted12MonthQuantity, 0)) as Forecasted12MonthQuantity,
	Sum(ISNULL(TotalForecasted12MonthCost_LastCost, 0)) as TotalForecasted12MonthCost_LastCost,
	Sum(ISNULL(TotalForecasted12MonthPrice_LastPrice, 0)) as TotalForecasted12MonthPrice_LastPrice,
	Sum(ISNULL(TotalForecasted12MonthPrice_ProposedPrice, 0)) as TotalForecasted12MonthPrice_ProposedPrice,
	Sum(ISNULL(TotalForecasted12MonthPrice_NewPrice, 0)) as TotalForecasted12MonthPrice_NewPrice,
	Sum(ISNULL(TotalForecasted12MonthCost_RelevantCost, 0)) as TotalForecasted12MonthCost_RelevantCost,
	Count_Big(*) as cnt
From dbo.ProposalPrice
Group by 
	ProjectKey, 
	PlaybookDataPointGroupKey,
	ItemKey,
	AcceptanceCodeKey,
	UserAcceptanceCodeKey,
	DefaultAcceptanceCodeKey'

exec sp_executesql @cmd

Create unique clustered index UI_ProposalPrice_Summary_Item_1 on ProposalPrice_Summary_Item(ProjectKey, 	PlaybookDataPointGroupKey, 	ItemKey,AcceptanceCodeKey, 	UserAcceptanceCodeKey,	DefaultAcceptanceCodeKey)
---------------------------------------------------------------------------


set @cmd = '
CREATE view ProposalPrice_Summary_AccountManagerKey
With SchemaBinding
as
Select 
	ProjectKey,
	PlaybookDataPointGroupKey,
	AccountManagerKey,
	AcceptanceCodeKey,
	UserAcceptanceCodeKey,
	DefaultAcceptanceCodeKey,
	Sum(TotalActual12MonthCost) as TotalActual12MonthCost,
	Sum(TotalActual12MonthPrice) as TotalActual12MonthPrice,
	Sum(TotalActual12MonthQuantity) as TotalActual12MonthQuantity,
	Sum(TotalActual12MonthFrequency) as TotalActual12MonthFrequency,
	Sum(ISNULL(Forecasted12MonthQuantity, 0)) as Forecasted12MonthQuantity,
	Sum(ISNULL(TotalForecasted12MonthCost_LastCost, 0)) as TotalForecasted12MonthCost_LastCost,
	Sum(ISNULL(TotalForecasted12MonthPrice_LastPrice, 0)) as TotalForecasted12MonthPrice_LastPrice,
	Sum(ISNULL(TotalForecasted12MonthPrice_ProposedPrice, 0)) as TotalForecasted12MonthPrice_ProposedPrice,
	Sum(ISNULL(TotalForecasted12MonthPrice_NewPrice, 0)) as TotalForecasted12MonthPrice_NewPrice,
	Sum(ISNULL(TotalForecasted12MonthCost_RelevantCost, 0)) as TotalForecasted12MonthCost_RelevantCost,
	Count_Big(*) as cnt
From dbo.ProposalPrice
Group by 
	ProjectKey, 
	PlaybookDataPointGroupKey,
	AccountManagerKey,
	AcceptanceCodeKey,
	UserAcceptanceCodeKey,
	DefaultAcceptanceCodeKey'

exec sp_executesql @cmd

Create unique clustered index UI_ProposalPrice_Summary_AccountManagerKey_1 on ProposalPrice_Summary_AccountManagerKey(ProjectKey, 	PlaybookDataPointGroupKey,	AccountManagerKey,AcceptanceCodeKey,	UserAcceptanceCodeKey,	DefaultAcceptanceCodeKey)



----------------------------

--set @cmd = '
--Create view ProposalPrice_AccountStatus
--WITH SchemaBinding
--as
--Select
--WebUserKey,
--ProposalPrice.AccountManagerKey,
--ProposalPrice.ProjectKey,
--PlaybookProjectAccountStatus.ProjectAccountStatus, 
--PlaybookProjectAccountManagerStatus.ProjectAccountManagerStatus,
--sum(ISNULL(
--	(TotalForecasted12MonthPrice_ProposedPrice-TotalForecasted12MonthCost_RelevantCost)
--	-
--	(TotalForecasted12MonthPrice_LastPrice-TotalForecasted12MonthCost_LastCost)
--	, 0)
--)
--IncrementalGrossProfit,
--Sum(ISNULL(TotalForecasted12MonthPrice_ProposedPrice, 0)) TotalForecasted12MonthPrice_ProposedPrice,
--Sum(ISNULL(TotalForecasted12MonthCost_RelevantCost, 0)) TotalForecasted12MonthCost_RelevantCost,
--Sum(ISNULL(TotalForecasted12MonthPrice_LastPrice, 0)) TotalForecasted12MonthPrice_LastPrice,
--Sum(ISNULL(TotalForecasted12MonthCost_LastCost, 0)) TotalForecasted12MonthCost_LastCost,
--count_big(*) cnt
--from dbo.ProposalPrice 
--join dbo.PlaybookProjectAccountStatus on 
--	PlaybookProjectAccountStatus.AccountKey = ProposalPrice.AccountKey and ProposalPrice.ProjectKey = PlaybookProjectAccountStatus.ProjectKey
--join dbo.PlaybookProjectAccountManagerStatus on 
--	PlaybookProjectAccountManagerStatus.AccountManagerKey = ProposalPrice.AccountManagerKey and ProposalPrice.ProjectKey = PlaybookProjectAccountManagerStatus.ProjectKey
--join dbo.WebUserAccountManagerAccessType AMAT on AMAT.AccountManagerKey = ProposalPrice.AccountManagerKey
----	and ProposalPrice.AccountManagerKey in (Select AccountManagerKey from @AMs)
--Group by WebUserKey, ProposalPrice.AccountManagerKey, ProposalPrice.ProjectKey, PlaybookProjectAccountStatus.ProjectAccountStatus, PlaybookProjectAccountManagerStatus.ProjectAccountManagerStatus'

--exec sp_executesql @cmd

--Create unique clustered index I_PPAS_1 on ProposalPrice_AccountStatus(WebUserKey, AccountManagerKey, ProjectKey, ProjectAccountStatus, ProjectAccountManagerStatus)
--Create index I_ProposalPrice_AccountStatus_2 on ProposalPrice_AccountStatus(WebUserKey, ProjectKey)

exec LogDCPEvent 'ETL - dbo.MVIEWS_ProposalPrice_Create', 'E', 0, 0, 0









GO
