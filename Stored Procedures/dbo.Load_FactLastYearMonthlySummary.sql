SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




CREATE 
PROCEDURE [dbo].[Load_FactLastYearMonthlySummary]
AS

SET NOCOUNT ON

/*

EXEC dbo.Load_FactLastYearMonthlySummary

SELECT TOP 300 * FROM dbo.FactLastYearMonthlySummary

*/

EXEC LogDCPEvent 'ETL - dbo.Load_FactLastYearMonthlySummary', 'B'


DECLARE @RowsI Int_type, 
	@RowsU Int_type, 
	@RowsD Int_type,
	@MaxInvoiceDateDayKey DateKey_type

SET @RowsI = 0
SET @RowsU = 0
SET @RowsD = 0


TRUNCATE TABLE dbo.FactLastYearMonthlySummary


--  DBA_IndexRebuild may not handle dbo owned tables
--  EXEC DBA_IndexRebuild 'FactLastYearMonthlySummary', 'Drop_NoClustered'
ALTER TABLE [dbo].[FactLastYearMonthlySummary] DROP CONSTRAINT [PK_dbo_FactLastYearMonthlySummary]
--DROP INDEX  [dbo].[FactLastYearMonthlySummary].[I_dbo_FactLastYearMonthlySummary_DaysSinceLastSale]
--DROP INDEX  [dbo].[FactLastYearMonthlySummary].[I_dbo_FactLastYearMonthlySummary_ItemKey]

SET @MaxInvoiceDateDayKey = (SELECT MAX(InvoiceDateDayKey) FROM dbo.FactInvoiceLine)

INSERT dbo.FactLastYearMonthlySummary
(
	AccountKey,
	ItemKey,
	InvoiceLineGroup1Key,

	MonthSequenceNumber0Sales,
	MonthSequenceNumber1Sales,
	MonthSequenceNumber2Sales,
	MonthSequenceNumber3Sales,
	MonthSequenceNumber4Sales,
	MonthSequenceNumber5Sales,
	MonthSequenceNumber6Sales,
	MonthSequenceNumber7Sales,
	MonthSequenceNumber8Sales,
	MonthSequenceNumber9Sales,
	MonthSequenceNumber10Sales,
	MonthSequenceNumber11Sales,
	MonthSequenceNumber12Sales,

	MonthSequenceNumber0Cost,
	MonthSequenceNumber1Cost,
	MonthSequenceNumber2Cost,
	MonthSequenceNumber3Cost,
	MonthSequenceNumber4Cost,
	MonthSequenceNumber5Cost,
	MonthSequenceNumber6Cost,
	MonthSequenceNumber7Cost,
	MonthSequenceNumber8Cost,
	MonthSequenceNumber9Cost,
	MonthSequenceNumber10Cost,
	MonthSequenceNumber11Cost,
	MonthSequenceNumber12Cost,

	MonthSequenceNumber0Quantity,
	MonthSequenceNumber1Quantity,
	MonthSequenceNumber2Quantity,
	MonthSequenceNumber3Quantity,
	MonthSequenceNumber4Quantity,
	MonthSequenceNumber5Quantity,
	MonthSequenceNumber6Quantity,
	MonthSequenceNumber7Quantity,
	MonthSequenceNumber8Quantity,
	MonthSequenceNumber9Quantity,
	MonthSequenceNumber10Quantity,
	MonthSequenceNumber11Quantity,
	MonthSequenceNumber12Quantity,

	MonthSequenceNumber0InvoiceLineCount,
	MonthSequenceNumber1InvoiceLineCount,
	MonthSequenceNumber2InvoiceLineCount,
	MonthSequenceNumber3InvoiceLineCount,
	MonthSequenceNumber4InvoiceLineCount,
	MonthSequenceNumber5InvoiceLineCount,
	MonthSequenceNumber6InvoiceLineCount,
	MonthSequenceNumber7InvoiceLineCount,
	MonthSequenceNumber8InvoiceLineCount,
	MonthSequenceNumber9InvoiceLineCount,
	MonthSequenceNumber10InvoiceLineCount,
	MonthSequenceNumber11InvoiceLineCount,
	MonthSequenceNumber12InvoiceLineCount,

	Total12MonthSales,
	Total12MonthCost,
	Total12MonthQuantity,
	InvoiceLineCount,

	FirstSaleMonthSequenceNumber,
	LastSaleDayKey,
	DaysSinceLastSale
)
SELECT
	s.AccountKey,
	s.ItemKey,
	s.InvoiceLineGroup1Key,

	MonthSequenceNumber0Sales,
	MonthSequenceNumber1Sales,
	MonthSequenceNumber2Sales,
	MonthSequenceNumber3Sales,
	MonthSequenceNumber4Sales,
	MonthSequenceNumber5Sales,
	MonthSequenceNumber6Sales,
	MonthSequenceNumber7Sales,
	MonthSequenceNumber8Sales,
	MonthSequenceNumber9Sales,
	MonthSequenceNumber10Sales,
	MonthSequenceNumber11Sales,
	MonthSequenceNumber12Sales,

	MonthSequenceNumber0Cost,
	MonthSequenceNumber1Cost,
	MonthSequenceNumber2Cost,
	MonthSequenceNumber3Cost,
	MonthSequenceNumber4Cost,
	MonthSequenceNumber5Cost,
	MonthSequenceNumber6Cost,
	MonthSequenceNumber7Cost,
	MonthSequenceNumber8Cost,
	MonthSequenceNumber9Cost,
	MonthSequenceNumber10Cost,
	MonthSequenceNumber11Cost,
	MonthSequenceNumber12Cost,

	MonthSequenceNumber0Quantity,
	MonthSequenceNumber1Quantity,
	MonthSequenceNumber2Quantity,
	MonthSequenceNumber3Quantity,
	MonthSequenceNumber4Quantity,
	MonthSequenceNumber5Quantity,
	MonthSequenceNumber6Quantity,
	MonthSequenceNumber7Quantity,
	MonthSequenceNumber8Quantity,
	MonthSequenceNumber9Quantity,
	MonthSequenceNumber10Quantity,
	MonthSequenceNumber11Quantity,
	MonthSequenceNumber12Quantity,

	MonthSequenceNumber0InvoiceLineCount,
	MonthSequenceNumber1InvoiceLineCount,
	MonthSequenceNumber2InvoiceLineCount,
	MonthSequenceNumber3InvoiceLineCount,
	MonthSequenceNumber4InvoiceLineCount,
	MonthSequenceNumber5InvoiceLineCount,
	MonthSequenceNumber6InvoiceLineCount,
	MonthSequenceNumber7InvoiceLineCount,
	MonthSequenceNumber8InvoiceLineCount,
	MonthSequenceNumber9InvoiceLineCount,
	MonthSequenceNumber10InvoiceLineCount,
	MonthSequenceNumber11InvoiceLineCount,
	MonthSequenceNumber12InvoiceLineCount,

	s.Total12MonthSales,
	s.Total12MonthCost,
	s.Total12MonthQuantity,
	s.InvoiceLineCount,

	FirstSaleMonthSequenceNumber,
 	LastSaleDayKey,
 	@MaxInvoiceDateDayKey - LastSaleDayKey AS DaysSinceLastSale
FROM
	(
	SELECT
		fil.AccountKey,
		fil.ItemKey,
		fil.InvoiceLineGroup1Key,
	
			/* To completely denormalize like in Margin Generator, uncomment the following lines.  Then uncomment around the joins.
			AccountNumber,
			AccountName,
			da.AccountGroup1Key,
			AG1Level1,
			AccountManagerCode,
			FullName,
			ItemNumber,
			ItemDescription,
			ItemUDVarChar1,
			IG1Level1, 		-- product type
			InvoiceLineGroup1UDVarchar1,
			InvoiceLineGroup1UDVarchar2,
			VendorNumber,
			VendorDescription
			*/
	
		SUM(CASE MonthSequenceNumber WHEN 0 THEN TotalActualPrice ELSE 0.0 END) AS MonthSequenceNumber0Sales,
		SUM(CASE MonthSequenceNumber WHEN 1 THEN TotalActualPrice ELSE 0.0 END) AS MonthSequenceNumber1Sales,
		SUM(CASE MonthSequenceNumber WHEN 2 THEN TotalActualPrice ELSE 0.0 END) AS MonthSequenceNumber2Sales,
		SUM(CASE MonthSequenceNumber WHEN 3 THEN TotalActualPrice ELSE 0.0 END) AS MonthSequenceNumber3Sales,
		SUM(CASE MonthSequenceNumber WHEN 4 THEN TotalActualPrice ELSE 0.0 END) AS MonthSequenceNumber4Sales,
		SUM(CASE MonthSequenceNumber WHEN 5 THEN TotalActualPrice ELSE 0.0 END) AS MonthSequenceNumber5Sales,
		SUM(CASE MonthSequenceNumber WHEN 6 THEN TotalActualPrice ELSE 0.0 END) AS MonthSequenceNumber6Sales,
		SUM(CASE MonthSequenceNumber WHEN 7 THEN TotalActualPrice ELSE 0.0 END) AS MonthSequenceNumber7Sales,
		SUM(CASE MonthSequenceNumber WHEN 8 THEN TotalActualPrice ELSE 0.0 END) AS MonthSequenceNumber8Sales,
		SUM(CASE MonthSequenceNumber WHEN 9 THEN TotalActualPrice ELSE 0.0 END) AS MonthSequenceNumber9Sales,
		SUM(CASE MonthSequenceNumber WHEN 10 THEN TotalActualPrice ELSE 0.0 END) AS MonthSequenceNumber10Sales,
		SUM(CASE MonthSequenceNumber WHEN 11 THEN TotalActualPrice ELSE 0.0 END) AS MonthSequenceNumber11Sales,
		SUM(CASE MonthSequenceNumber WHEN 12 THEN TotalActualPrice ELSE 0.0 END) AS MonthSequenceNumber12Sales,
	
		SUM(CASE MonthSequenceNumber WHEN 0 THEN TotalActualCost ELSE 0.0 END) AS MonthSequenceNumber0Cost,
		SUM(CASE MonthSequenceNumber WHEN 1 THEN TotalActualCost ELSE 0.0 END) AS MonthSequenceNumber1Cost,
		SUM(CASE MonthSequenceNumber WHEN 2 THEN TotalActualCost ELSE 0.0 END) AS MonthSequenceNumber2Cost,
		SUM(CASE MonthSequenceNumber WHEN 3 THEN TotalActualCost ELSE 0.0 END) AS MonthSequenceNumber3Cost,
		SUM(CASE MonthSequenceNumber WHEN 4 THEN TotalActualCost ELSE 0.0 END) AS MonthSequenceNumber4Cost,
		SUM(CASE MonthSequenceNumber WHEN 5 THEN TotalActualCost ELSE 0.0 END) AS MonthSequenceNumber5Cost,
		SUM(CASE MonthSequenceNumber WHEN 6 THEN TotalActualCost ELSE 0.0 END) AS MonthSequenceNumber6Cost,
		SUM(CASE MonthSequenceNumber WHEN 7 THEN TotalActualCost ELSE 0.0 END) AS MonthSequenceNumber7Cost,
		SUM(CASE MonthSequenceNumber WHEN 8 THEN TotalActualCost ELSE 0.0 END) AS MonthSequenceNumber8Cost,
		SUM(CASE MonthSequenceNumber WHEN 9 THEN TotalActualCost ELSE 0.0 END) AS MonthSequenceNumber9Cost,
		SUM(CASE MonthSequenceNumber WHEN 10 THEN TotalActualCost ELSE 0.0 END) AS MonthSequenceNumber10Cost,
		SUM(CASE MonthSequenceNumber WHEN 11 THEN TotalActualCost ELSE 0.0 END) AS MonthSequenceNumber11Cost,
		SUM(CASE MonthSequenceNumber WHEN 12 THEN TotalActualCost ELSE 0.0 END) AS MonthSequenceNumber12Cost,
	
		SUM(CASE MonthSequenceNumber WHEN 0 THEN TotalQuantity ELSE 0.0 END) AS MonthSequenceNumber0Quantity,
		SUM(CASE MonthSequenceNumber WHEN 1 THEN TotalQuantity ELSE 0.0 END) AS MonthSequenceNumber1Quantity,
		SUM(CASE MonthSequenceNumber WHEN 2 THEN TotalQuantity ELSE 0.0 END) AS MonthSequenceNumber2Quantity,
		SUM(CASE MonthSequenceNumber WHEN 3 THEN TotalQuantity ELSE 0.0 END) AS MonthSequenceNumber3Quantity,
		SUM(CASE MonthSequenceNumber WHEN 4 THEN TotalQuantity ELSE 0.0 END) AS MonthSequenceNumber4Quantity,
		SUM(CASE MonthSequenceNumber WHEN 5 THEN TotalQuantity ELSE 0.0 END) AS MonthSequenceNumber5Quantity,
		SUM(CASE MonthSequenceNumber WHEN 6 THEN TotalQuantity ELSE 0.0 END) AS MonthSequenceNumber6Quantity,
		SUM(CASE MonthSequenceNumber WHEN 7 THEN TotalQuantity ELSE 0.0 END) AS MonthSequenceNumber7Quantity,
		SUM(CASE MonthSequenceNumber WHEN 8 THEN TotalQuantity ELSE 0.0 END) AS MonthSequenceNumber8Quantity,
		SUM(CASE MonthSequenceNumber WHEN 9 THEN TotalQuantity ELSE 0.0 END) AS MonthSequenceNumber9Quantity,
		SUM(CASE MonthSequenceNumber WHEN 10 THEN TotalQuantity ELSE 0.0 END) AS MonthSequenceNumber10Quantity,
		SUM(CASE MonthSequenceNumber WHEN 11 THEN TotalQuantity ELSE 0.0 END) AS MonthSequenceNumber11Quantity,
		SUM(CASE MonthSequenceNumber WHEN 12 THEN TotalQuantity ELSE 0.0 END) AS MonthSequenceNumber12Quantity,
	
		SUM(CASE MonthSequenceNumber WHEN 0 THEN 1 ELSE 0 END) AS MonthSequenceNumber0InvoiceLineCount,
		SUM(CASE MonthSequenceNumber WHEN 1 THEN 1 ELSE 0 END) AS MonthSequenceNumber1InvoiceLineCount,
		SUM(CASE MonthSequenceNumber WHEN 2 THEN 1 ELSE 0 END) AS MonthSequenceNumber2InvoiceLineCount,
		SUM(CASE MonthSequenceNumber WHEN 3 THEN 1 ELSE 0 END) AS MonthSequenceNumber3InvoiceLineCount,
		SUM(CASE MonthSequenceNumber WHEN 4 THEN 1 ELSE 0 END) AS MonthSequenceNumber4InvoiceLineCount,
		SUM(CASE MonthSequenceNumber WHEN 5 THEN 1 ELSE 0 END) AS MonthSequenceNumber5InvoiceLineCount,
		SUM(CASE MonthSequenceNumber WHEN 6 THEN 1 ELSE 0 END) AS MonthSequenceNumber6InvoiceLineCount,
		SUM(CASE MonthSequenceNumber WHEN 7 THEN 1 ELSE 0 END) AS MonthSequenceNumber7InvoiceLineCount,
		SUM(CASE MonthSequenceNumber WHEN 8 THEN 1 ELSE 0 END) AS MonthSequenceNumber8InvoiceLineCount,
		SUM(CASE MonthSequenceNumber WHEN 9 THEN 1 ELSE 0 END) AS MonthSequenceNumber9InvoiceLineCount,
		SUM(CASE MonthSequenceNumber WHEN 10 THEN 1 ELSE 0 END) AS MonthSequenceNumber10InvoiceLineCount,
		SUM(CASE MonthSequenceNumber WHEN 11 THEN 1 ELSE 0 END) AS MonthSequenceNumber11InvoiceLineCount,
		SUM(CASE MonthSequenceNumber WHEN 12 THEN 1 ELSE 0 END) AS MonthSequenceNumber12InvoiceLineCount,
	
		SUM(TotalActualPrice) AS Total12MonthSales,
		SUM(TotalActualCost) AS Total12MonthCost,
		SUM(TotalQuantity) AS Total12MonthQuantity,
		COUNT(*) AS InvoiceLineCount,
	
		MIN(MonthSequenceNumber) AS FirstSaleMonthSequenceNumber
	FROM dbo.FactInvoiceLine fil
	INNER JOIN DimMonth dm ON
		dm.MonthKey = fil.InvoiceDateMonthKey
		/* To completely denormalize like in Margin Generator, uncomment the following lines.  Then uncomment around the group by.
		INNER JOIN DimAccount da ON 
			da.AccountKey = fil.AccountKey
		INNER JOIN DimAccountGroup1 dag1 ON
			dag1.AccountGroup1Key = da.AccountGroup1Key
		INNER JOIN DimAccountManager dam ON
			dam.AccountManagerKey = da.AccountManagerKey
		INNER JOIN DimPerson DP ON
			dp.DimPersonKey = dam.DimPersonKey
		INNER JOIN DimItem di ON
			di.ItemKey = fil.ItemKey
		INNER JOIN DimVendor dv ON
			dv.VendorKey = di.ItemVendorKey
		INNER JOIN DimItemGroup1 dig1 ON
			dig1.ItemGroup1Key = di.ItemGroup1Key
		INNER JOIN DimInvoiceLineGroup1 dilg1 ON
			dilg1.InvoiceLineGroup1Key = fil.InvoiceLineGroup1Key
		*/
	WHERE 
		dm.MonthSequenceNumber BETWEEN 0 AND 12
		--Last12MonthsIndicator = 'Y'
	GROUP BY
		fil.AccountKey,
		fil.ItemKey,
		fil.InvoiceLineGroup1Key
		/* To completely denormalize like in Margin Generator, uncomment the following lines.  Then see example query below.
			,
			AccountNumber,
			AccountName,
			da.AccountGroup1Key,
			AG1Level1,
			AccountManagerCode,
			FullName,
			ItemNumber,
			ItemDescription,
			ItemUDVarChar1,
			IG1Level1, 		-- product type
			InvoiceLineGroup1UDVarchar1,
			InvoiceLineGroup1UDVarchar2,
			VendorNumber,
			VendorDescription
		*/
	) s
INNER JOIN dbo.FactLastPriceAndCost flpac ON
 		flpac.AccountKey = s.AccountKey AND
 		flpac.ItemKey = s.ItemKey AND
 		flpac.InvoiceLineGroup1Key = s.InvoiceLineGroup1Key
SET @RowsI = @RowsI + @@RowCount

/* MG based example... (not tested)
SELECT
	*
	'...'
FROM dbo.FactLastYearMonthlySummary,
(
	SELECT
		(SELECT FirstSQLDate FROM DimMonth WHERE MonthSequenceNumber = 1) AS Month1,
		(SELECT FirstSQLDate FROM DimMonth WHERE MonthSequenceNumber = 2) AS Month2,
		(SELECT FirstSQLDate FROM DimMonth WHERE MonthSequenceNumber = 3) AS Month3,
		(SELECT FirstSQLDate FROM DimMonth WHERE MonthSequenceNumber = 4) AS Month4,
		(SELECT FirstSQLDate FROM DimMonth WHERE MonthSequenceNumber = 5) AS Month5,
		(SELECT FirstSQLDate FROM DimMonth WHERE MonthSequenceNumber = 6) AS Month6,
		(SELECT FirstSQLDate FROM DimMonth WHERE MonthSequenceNumber = 7) AS Month7,
		(SELECT FirstSQLDate FROM DimMonth WHERE MonthSequenceNumber = 8) AS Month8,
		(SELECT FirstSQLDate FROM DimMonth WHERE MonthSequenceNumber = 9) AS Month9,
		(SELECT FirstSQLDate FROM DimMonth WHERE MonthSequenceNumber = 10) AS Month10,
		(SELECT FirstSQLDate FROM DimMonth WHERE MonthSequenceNumber = 11) AS Month11,
		(SELECT FirstSQLDate FROM DimMonth WHERE MonthSequenceNumber = 12) AS Month12
) Months
WHERE  	
	AccountGroup1Key IN (SELECT Value FROM dbo.fn_ListToTableInt(@strBranchKeys, ','))
	AND AccountKey IN (SELECT AccountKey FROM DimAccount WHERE AccountManagerKey IN (SELECT AccountManagerKey FROM WebUserAccountManagerAccessType WHERE WebUserKey = @WebUserKey))	--Make sure this webuser has access to see data for the account manager
	AND (@strVendorKey = '' OR ItemVendorKey = @VendorKey)
	AND TotalActual12GrossProfit >= @GPDollar
	AND InvoiceLineCount >= @Frequency
	AND (@ProductType = '' OR IG1Level1 = @ProductType)
	AND (@ItemNumber = '' OR ItemNumber = @ItemNumber)
	AND (@AccountManagerCode = '' OR AccountManagerCode = @AccountManagerCode)
	AND (@AccountNumber = '' OR AccountNumber = @AccountNumber)
	--AND @MinMonthThreshold < MinMonthSequenceNumber 	--This is based on the # of full months that have passed since the last sale.  Does not count days from partial months.
	AND DaysSinceLastSale >= @strDaysWOSales 		--This is based on the # of days including partial months.  This is how it used to work under the old version but it is incorrect since the rest of the report only operates on sales from full months
	AND @SalesStartingInMonth >= MinMonthSequenceNumber 	--It must have been sold since this month
	ORDER BY 
		FullName,
		AccountName,
		REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(@DescriptionTokenExpression, '%itemNumber%', ISNULL(ItemNumber, '')), '%catalogNumber%', ISNULL(ItemUDVarChar1, '')), '%vendorNumber%', ISNULL(VendorNumber,'')), '%productDescription%', ISNULL(ItemDescription,'')), '%vendorName%', ISNULL(VendorDescription, ''))
*/

--  DBA_IndexRebuild may not handle client owned tables
--  EXEC DBA_IndexRebuild 'FactLastYearMonthlySummary', 'Create'
ALTER TABLE [dbo].[FactLastYearMonthlySummary] ADD CONSTRAINT [PK_dbo_FactLastYearMonthlySummary] PRIMARY KEY  CLUSTERED 
	(
		[AccountKey],
		[ItemKey],
		[InvoiceLineGroup1Key]
	) WITH  FILLFACTOR = 90  ON [PRIMARY] 

--CREATE INDEX [I_dbo_FactLastYearMonthlySummary_DaysSinceLastSale] ON [dbo].[FactLastYearMonthlySummary]([DaysSinceLastSale]) ON [PRIMARY]
--CREATE INDEX [I_dbo_FactLastYearMonthlySummary_ItemKey] ON [dbo].[FactLastYearMonthlySummary]([ItemKey]) ON [PRIMARY]


EXEC LogDCPEvent 'ETL - dbo.Load_FactLastYearMonthlySummary', 'E', @RowsI, @RowsU, @RowsD







GO
