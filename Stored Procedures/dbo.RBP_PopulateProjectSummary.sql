SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




CREATE  
PROCEDURE [dbo].[RBP_PopulateProjectSummary]
	@ProjectKey Key_Normal_Type
AS

SET QUOTED_IDENTIFIER ON 
SET ANSI_NULLS ON 
SET ARITHABORT ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
SET CONCAT_NULL_YIELDS_NULL ON
SET NUMERIC_ROUNDABORT OFF
SET NOCOUNT ON 


DECLARE 
	@RowsI Quantity_Normal_type, 
	@RowsU Quantity_Normal_type, 
	@RowsD Quantity_Normal_type

SET @RowsI = 0
SET @RowsU = 0
SET @RowsD = 0


EXEC LogDCPEvent 'DCP --- RBP_PopulateProjectSummary: Insert PlaybookProjectFullScopeSummary', 'B', 0, 0, 0, @ProjectKey

DELETE FROM PlaybookProjectFullScopeSummary WHERE ProjectKey = @ProjectKey
SET @RowsD = @RowsD + @@RowCount

SET QUOTED_IDENTIFIER ON 
SET ANSI_NULLS ON 
SET ARITHABORT ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
SET CONCAT_NULL_YIELDS_NULL ON
SET NUMERIC_ROUNDABORT OFF


SELECT DISTINCT 
	pdpgs.AccountKey, 
	da.AccountManagerKey
INTO #Accounts
FROM PlaybookDataPointGroupScope pdpgs
INNER JOIN DimAccount da ON 
	pdpgs.AccountKey = da.AccountKey
INNER JOIN 
	(
	SELECT 
		PlaybookDataPointGroupKey 
	FROM PlaybookDataPointGroup 
	WHERE ProjectKey = @ProjectKey
	) GroupKeys ON
	GroupKeys.PlaybookDataPointGroupKey = pdpgs.PlaybookDataPointGroupKey

CREATE UNIQUE CLUSTERED INDEX I_A_1 ON #Accounts(AccountKey)


INSERT PlaybookProjectFullScopeSummary (
	ProjectKey,
	AccountManagerKey,
	AccountKey,
	ItemKey,
	InvoiceLineGroup1Key,
	LastInvoiceLineGroup2Key,
	TotalActual12MonthPrice,
	TotalActual12MonthCost,
	TotalActual12MonthQuantity,
	TotalInvoiceLineCount,
	LastItemPrice,
	LastItemCost)
SELECT 
	@ProjectKey,
	A.AccountManagerKey,
	fil.AccountKey, 
	fil.ItemKey,
	fil.InvoiceLineGroup1Key,
	flpc.LastInvoiceLineGroup2Key,
	SUM(fil.TotalActualPrice) AS TotalActualPrice,
	SUM(fil.TotalActualCost) AS TotalActualCost,
	SUM(fil.TotalQuantity) AS TotalQuantity,
	COUNT(*) AS TotalInvoiceLineCount,
	flpc.LastItemPrice,
	flpc.LastItemCost
	FROM dbo.FactInvoiceLine fil --dbo.FactInvoiceLine_Summary1 fil
	LEFT JOIN FactLastPriceAndCost flpc ON 
		flpc.AccountKey = fil.AccountKey AND 
		flpc.ItemKey = fil.ItemKey AND 
		flpc.InvoiceLineGroup1Key = fil.InvoiceLineGroup1Key
	INNER JOIN #Accounts A ON 
		A.AccountKey = fil.AccountKey
	WHERE Last12MonthsIndicator = 'Y'
	GROUP BY 
		A.AccountManagerKey,
		fil.AccountKey,
		fil.ItemKey,
		fil.InvoiceLineGroup1Key,
		flpc.LastInvoiceLineGroup2Key,
		flpc.LastItemPrice,
		flpc.LastItemCost
	-- the following having was added in to 1) conform to RBP_PopulatePlaybookDataPointGroupScope, 2) correct div by zero errors that could occur and 3) Renegade did something similar
 	HAVING	SUM(fil.TotalActualPrice) <> 0.0
 		AND SUM(fil.TotalActualCost) <> 0.0
 		AND SUM(fil.TotalQuantity) <> 0.0
SET @RowsI = @RowsI + @@RowCount


DECLARE @USql NVARCHAR(MAX)
SET @USql = 'Update Statistics PlaybookProjectFullScopeSummary'
EXEC (@USql)


EXEC LogDCPEvent 'DCP --- RBP_PopulateProjectSummary: Insert PlaybookProjectFullScopeSummary', 'E', @RowsI, @RowsU, @RowsD
















GO
