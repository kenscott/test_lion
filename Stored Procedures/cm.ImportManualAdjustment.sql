SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




CREATE PROCEDURE [cm].[ImportManualAdjustment]
		@BatchKey INT,
		@WebUserKey INT,
	    @AdjustmentType VARCHAR(1)

AS

SET NOCOUNT ON

IF @AdjustmentType = 'I'  --Import
BEGIN

--Data Validation
UPDATE i SET ErrorMessage = ISNULL(ErrorMessage, '') + 'Invalid ContractID ' + i.ContractID + '; '
	FROM cm.AdjustmentStaging i
	LEFT JOIN cc.SupplierContract sc
		ON sc.ContractID = i.ContractID
		AND sc.StatusKey IN (4, 6)  --Pending or Approved
	WHERE i.BatchKey = @BatchKey
		AND sc.ContractKey IS NULL
		
UPDATE i SET ErrorMessage = ISNULL(ErrorMessage, '') + 'Invalid SupplierID ' + i.SupplierID + '; '
	FROM cm.AdjustmentStaging i
	LEFT JOIN dbo.DimVendor dv
		ON dv.VendorNumber = i.SupplierId
	LEFT JOIN cc.SupplierContract sc
		ON sc.VendorKey = dv.VendorKey
		AND sc.ContractID = i.ContractId
	WHERE i.BatchKey = @BatchKey
		AND (dv.VendorKey IS NULL OR sc.ContractKey IS NULL)

UPDATE i SET ErrorMessage = ISNULL(ErrorMessage, '') + 'Invalid SupplierRef ' + i.SupplierRef + '; '
	FROM cm.AdjustmentStaging i
	LEFT JOIN cc.SupplierContract sc
		ON sc.ContractID = i.ContractId
	LEFT JOIN cc.SupplierContractReference r
		ON r.ContractKey = sc.ContractKey
		AND r.ContractReference = i.SupplierRef
	WHERE i.BatchKey = @BatchKey
		AND r.ContractKey IS NULL

UPDATE i SET ErrorMessage = ISNULL(ErrorMessage, '') + 'Invalid ChargeNoteID ' + i.ChargeNoteID + '; '
	FROM cm.AdjustmentStaging i
	LEFT JOIN cm.FactInvoiceLine fil
		ON fil.ChargeNoteID = i.ChargeNoteId
	WHERE i.BatchKey = @BatchKey
		AND i.ChargeNoteID IS NOT NULL
		AND fil.ClientInvoiceLineUniqueID IS NULL

UPDATE i SET ErrorMessage = ISNULL(ErrorMessage, '') + 'Invalid AccountId ' + i.AccountId + '; '
	FROM cm.AdjustmentStaging i
	LEFT JOIN dbo.DimAccount da
		ON da.AccountNumber = i.AccountId
		--AND da.Inactive = 0
	LEFT JOIN cc.SupplierContract sc
		ON sc.ContractID = i.ContractID
	LEFT JOIN cc.SupplierContractAccount sca
		ON sca.ContractKey = sc.ContractKey
		AND sca.AccountKey = da.AccountKey
	WHERE i.BatchKey = @BatchKey
		AND i.AccountId IS NOT NULL 
		AND (sca.ContractAccountKey IS NULL OR da.AccountKey IS NULL)
		AND sc.IncludeAllAccounts <> 1

UPDATE i SET ErrorMessage = ISNULL(ErrorMessage, '') + 'AccountId is excluded from this contract ' + i.AccountId + '; '
	FROM cm.AdjustmentStaging i
	LEFT JOIN dbo.DimAccount da
		ON da.AccountNumber = i.AccountId
	LEFT JOIN cc.SupplierContract sc
		ON sc.ContractID = i.ContractID
	LEFT JOIN cc.SupplierContractAccount sca
		ON sca.ContractKey = sc.ContractKey
		AND sca.AccountKey = da.AccountKey
	WHERE i.BatchKey = @BatchKey
		AND i.AccountId NOT IN ('VARIOUS', 'ALL') 
		AND i.AccountId IS NOT NULL 
		AND sc.IncludeAllAccounts = 1
		AND sca.ContractAccountKey IS NOT NULL

UPDATE i SET ErrorMessage = ISNULL(ErrorMessage, '') + 'Invalid LocationId ' + i.LocationId + '; '
	FROM cm.AdjustmentStaging i
	LEFT JOIN dbo.DimAccountGroup1 dag1
		ON dag1.AG1Level1 = i.LocationId
		--AND dag1.Inactive = 0
	LEFT JOIN cc.SupplierContract sc
		ON sc.ContractID = i.ContractID
	LEFT JOIN cc.SupplierContractLocation scl
		ON scl.ContractKey = sc.ContractKey
		AND scl.LocationName = i.LocationId
	WHERE i.BatchKey = @BatchKey
		AND i.LocationId IS NOT NULL 
		AND (scl.ContractLocationKey IS NULL OR dag1.AccountGroup1Key IS NULL)
		AND sc.IncludeAllLocations <> 1

UPDATE i SET ErrorMessage = ISNULL(ErrorMessage, '') + 'LocationId is excluded from this contract ' + i.LocationId + '; '
	FROM cm.AdjustmentStaging i
	LEFT JOIN dbo.DimAccountGroup1 dag1
		ON dag1.AG1Level1 = i.LocationId
	LEFT JOIN cc.SupplierContract sc
		ON sc.ContractID = i.ContractID
	LEFT JOIN cc.SupplierContractLocation scl
		ON scl.ContractKey = sc.ContractKey
		AND scl.LocationName = i.LocationId
	WHERE i.BatchKey = @BatchKey
		AND i.LocationId NOT IN ('VARIOUS', 'ALL') 
		AND i.LocationId IS NOT NULL 
		AND sc.IncludeAllLocations = 1
		AND scl.ContractLocationKey IS NOT NULL

UPDATE i SET ErrorMessage = ISNULL(ErrorMessage, '') + 'Invalid ProductId ' + i.ProductId + '; '
	FROM cm.AdjustmentStaging i
	LEFT JOIN dbo.DimItem di
		ON di.ItemNumber = '1' + i.ProductId
		--AND di.Inactive = 0
	LEFT JOIN cc.SupplierContract sc
		ON sc.ContractID = i.ContractID
	LEFT JOIN cc.SupplierContractItem sci
		ON sci.ContractKey = sc.ContractKey
		AND sci.ItemKey = di.ItemKey
	LEFT JOIN cc.SupplierContractItemGroupX mpg
		ON mpg.ContractKey = sc.ContractKey
		AND mpg.ItemGroupLevel = 1
		AND mpg.ItemGroupXKey = di.ItemGroup1Key
	LEFT JOIN cc.SupplierContractItemGroupX spg
		ON spg.ContractKey = sc.ContractKey
		AND spg.ItemGroupLevel = 3
		AND spg.ItemGroupXKey = di.ItemGroup3Key
	WHERE i.BatchKey = @BatchKey
		AND i.ProductId IS NOT NULL 
		AND (sci.ContractItemKey IS NULL AND mpg.ContractItemGroupXKey IS NULL AND spg.ContractItemGroupXKey IS NULL)

UPDATE i SET ErrorMessage = ISNULL(ErrorMessage, '') + 'Distribute must be Y, N or Blank; '
	FROM cm.AdjustmentStaging i
	WHERE i.BatchKey = @BatchKey
		AND i.Distribute IS NOT NULL
		AND i.Distribute NOT IN ('Y', 'N')

UPDATE i SET ErrorMessage = ISNULL(ErrorMessage, '') + 'Immediate must be Y, N or Blank; '
	FROM cm.AdjustmentStaging i
	WHERE i.BatchKey = @BatchKey
		AND i.[Immediate] IS NOT NULL
		AND i.[Immediate] NOT IN ('Y', 'N')

UPDATE i SET ErrorMessage = ISNULL(ErrorMessage, '') + 'Reason Code does not exist; '
	FROM cm.AdjustmentStaging i
	LEFT JOIN cm.AdjustmentReason ar
		ON ar.AdjustmentReasonCode = i.AdjustmentReasonCode 
	WHERE i.BatchKey = @BatchKey
		AND ar.AdjustmentReasonKey IS NULL
  
--LPF-5582, Claims Manager - Import Claim Adjustments - Blank Import Template uploads successfully 
UPDATE i SET ErrorMessage = ISNULL(ErrorMessage, '') + 'Period and SupplierID are required; '
	FROM cm.AdjustmentStaging i
	WHERE i.BatchKey = @BatchKey
	AND Period IS NULL AND SupplierId IS NULL


END

UPDATE i SET ErrorMessage = ISNULL(ErrorMessage, '') + 'Adjustment must be between -999999.99 and 999999.99; '
	FROM cm.AdjustmentStaging i
	WHERE i.BatchKey = @BatchKey
		AND (i.Adjustment < -999999.99 OR i.Adjustment > 999999.99)

--If NO Errors...
IF (SELECT COUNT(*) 
		FROM cm.AdjustmentStaging i
		WHERE BatchKey = @BatchKey AND ErrorMessage IS NOT NULL) = 0
BEGIN
-------------------------------------------------------------------------------------
--Account Adjustments
DECLARE @AdjustmentTotal TABLE (AdjustmentStagingKey KEY_NORMAL_TYPE, Adjustment DECIMAL(19,3), TotalClaimValue DECIMAL(19,8))

--Get total claim value for matching claims
INSERT INTO @AdjustmentTotal
SELECT s.AdjustmentStagingKey, s.Adjustment, SUM(cj.ClaimLineValue) AS TotalClaimValue
FROM cm.AdjustmentStaging s
INNER JOIN cm.ClaimJournal cj
	ON cj.VendorNumber = s.SupplierId
	--AND cj.ClaimMonth = s.Period
	AND cj.CurrentContractID = s.ContractId
INNER JOIN cm.FactInvoiceLine fil
	ON fil.ClientInvoiceLineUniqueID = cj.ClientInvoiceLineUniqueID
	AND fil.AccountNumber = s.AccountId
WHERE cj.ClaimType = 'C'
	AND cj.ClaimJournalStatus = 'C'
	AND s.AdjustmentType = 'A'
	AND s.BatchKey = @BatchKey
GROUP BY s.AdjustmentStagingKey, s.Adjustment

--Create adjustment claim lines
INSERT INTO [cm].[ClaimJournal]
           ([ClaimMonth]
		   ,[ClientInvoiceLineUniqueID]
		   ,[PyramidCode]
           ,[VendorNumber]
           ,[VendorDescription]
           ,[CurrentContractID]
           ,[ClaimType]
           ,[ClaimLineValue]
           ,[ClaimJournalStatus]
           ,[ClaimJournalStatusDate]
		   ,[VendorKey]
           ,[CreationDate]
           ,[CreatedByWebUserKey]
           ,[ImportBatchKey]
           ,[SupplierRef]
		   ,[ChargeNoteId]
		   ,[CustomerId]
		   ,[BranchCode]
		   ,[ProductCode]
		   ,[AdjustmentReasonKey]
		   ,[OriginalInvoiceClaimKey]
		   )
     SELECT
            CAST(CONVERT(VARCHAR(4), DATEPART(yy, GETDATE())) + CONVERT(VARCHAR(2), DATEPART(mm, GETDATE())) AS INT)
		   ,fil.ClientInvoiceLineUniqueID
		   ,'1'
           ,ad.SupplierID
           ,dv.VendorDescription
           ,ad.ContractID
		   ,CASE
				WHEN ar.CostCenter IS NOT NULL
				THEN 'W'  --write off
				ELSE 'A'  --adjustment
			END
           ,at.Adjustment * (cj.ClaimLineValue / NULLIF(at.TotalClaimValue, 0)) AS AdjustmentAmt
		   ,CASE
				WHEN ABS(at.Adjustment) > 1000 THEN 'R'
				ELSE 'P'
			END
           ,GETDATE()
           ,dv.VendorKey
		   ,GETDATE()
           ,@WebUserKey 
           ,ad.BatchKey 
           ,ad.SupplierRef
		   ,ad.ChargeNoteID
		   ,ad.AccountId
		   ,ad.LocationId
		   ,ad.ProductId
		   ,ar.AdjustmentReasonKey
		   ,cj.InvoiceClaimKey
FROM cm.AdjustmentStaging ad
INNER JOIN cm.ClaimJournal cj
	ON cj.VendorNumber = ad.SupplierId
	--AND cj.ClaimMonth = ad.Period
	AND cj.CurrentContractID = ad.ContractId
INNER JOIN cm.FactInvoiceLine fil
	ON fil.ClientInvoiceLineUniqueID = cj.ClientInvoiceLineUniqueID
	AND fil.AccountNumber = ad.AccountId
INNER JOIN @AdjustmentTotal at
	ON at.AdjustmentStagingKey = ad.AdjustmentStagingKey
INNER JOIN dbo.DimVendor dv
	ON dv.VendorNumber = ad.SupplierID
INNER JOIN cm.AdjustmentReason ar
	ON ar.AdjustmentReasonCode = ad.AdjustmentReasonCode
WHERE cj.ClaimType = 'C'
	AND cj.ClaimJournalStatus = 'C'
	AND ad.AdjustmentType = 'A'
	AND ad.BatchKey = @BatchKey

--Find the difference between the total adjustment amount and the total calculated values
SELECT ad.AdjustmentStagingKey, ad.Adjustment, SUM(cj.ClaimLineValue) as AppliedClaimValue, ABS(ad.Adjustment - SUM(cj.ClaimLineValue)) AS AdjDiff
INTO #TotalAdjAccount
FROM cm.AdjustmentStaging ad
INNER JOIN cm.ClaimJournal cj
	ON cj.VendorNumber = ad.SupplierId
	--AND cj.ClaimMonth = ad.Period
	AND cj.CurrentContractID = ad.ContractId
INNER JOIN cm.FactInvoiceLine fil
	ON fil.ClientInvoiceLineUniqueID = cj.ClientInvoiceLineUniqueID
	AND fil.AccountNumber = ad.AccountId
INNER JOIN @AdjustmentTotal at
	ON at.AdjustmentStagingKey = ad.AdjustmentStagingKey
INNER JOIN dbo.DimVendor dv
	ON dv.VendorNumber = ad.SupplierID
INNER JOIN cm.AdjustmentReason ar
	ON ar.AdjustmentReasonCode = ad.AdjustmentReasonCode
WHERE cj.ClaimType = 'A'
	AND cj.ClaimJournalStatus = 'P'
	AND ad.AdjustmentType = 'A'
  	AND ad.BatchKey = @BatchKey
GROUP BY ad.AdjustmentStagingKey, ad.Adjustment

--Find largest adjustment value
SELECT ad.AdjustmentStagingKey, MAX(cj.ClaimLineValue) AS MaxAdj
INTO #MaxAdjAccount
FROM cm.AdjustmentStaging ad
INNER JOIN cm.ClaimJournal cj
	ON cj.VendorNumber = ad.SupplierId
	--AND cj.ClaimMonth = ad.Period
	AND cj.CurrentContractID = ad.ContractId
INNER JOIN cm.FactInvoiceLine fil
	ON fil.ClientInvoiceLineUniqueID = cj.ClientInvoiceLineUniqueID
	AND fil.AccountNumber = ad.AccountId
INNER JOIN @AdjustmentTotal at
	ON at.AdjustmentStagingKey = ad.AdjustmentStagingKey
INNER JOIN dbo.DimVendor dv
	ON dv.VendorNumber = ad.SupplierID
INNER JOIN cm.AdjustmentReason ar
	ON ar.AdjustmentReasonCode = ad.AdjustmentReasonCode
WHERE cj.ClaimType = 'A'
	AND cj.ClaimJournalStatus = 'P'
	AND ad.AdjustmentType = 'A'
  	AND ad.BatchKey = @BatchKey
GROUP BY ad.AdjustmentStagingKey

--Add the difference to the largest adjustment value
UPDATE cj
SET ClaimLineValue = ClaimLineValue + t.AdjDiff
--select ClaimLineValue + t.AdjDiff
FROM cm.AdjustmentStaging ad
INNER JOIN cm.ClaimJournal cj
	ON cj.VendorNumber = ad.SupplierId
	--AND cj.ClaimMonth = ad.Period
	AND cj.CurrentContractID = ad.ContractId
INNER JOIN #MaxAdjAccount a
	ON a.AdjustmentStagingKey = ad.AdjustmentStagingKey
	AND a.MaxAdj = cj.ClaimLineValue
INNER JOIN #TotalAdjAccount t
	ON t.AdjustmentStagingKey = ad.AdjustmentStagingKey

DELETE FROM @AdjustmentTotal
----------------------------------------------------------------------------------------
--Claim Adjustments
--Get total claim value for matching claims
INSERT INTO @AdjustmentTotal
SELECT s.AdjustmentStagingKey, s.Adjustment, SUM(cj.ClaimLineValue) AS TotalClaimValue
FROM cm.AdjustmentStaging s
INNER JOIN cm.ClaimJournal cj
	ON cj.VendorNumber = s.SupplierId
	--AND cj.ClaimMonth = s.Period
	AND cj.CurrentContractID = s.ContractId
WHERE cj.ClaimType = 'C'
	AND cj.ClaimJournalStatus = 'C'
	AND s.AdjustmentType = 'C'
	AND s.BatchKey = @BatchKey
GROUP BY s.AdjustmentStagingKey, s.Adjustment

--Create adjustment claim lines
INSERT INTO [cm].[ClaimJournal]
           ([ClaimMonth]
		   ,[ClientInvoiceLineUniqueID]
		   ,[PyramidCode]
           ,[VendorNumber]
           ,[VendorDescription]
           ,[CurrentContractID]
           ,[ClaimType]
           ,[ClaimLineValue]
           ,[ClaimJournalStatus]
           ,[ClaimJournalStatusDate]
		   ,[VendorKey]
           ,[CreationDate]
           ,[CreatedByWebUserKey]
           ,[ImportBatchKey]
           ,[SupplierRef]
		   ,[ChargeNoteId]
		   ,[CustomerId]
		   ,[BranchCode]
		   ,[ProductCode]
		   ,[AdjustmentReasonKey]
		   ,[OriginalInvoiceClaimKey]
		   )
     SELECT
            CAST(CONVERT(VARCHAR(4), DATEPART(yy, GETDATE())) + CONVERT(VARCHAR(2), DATEPART(mm, GETDATE())) AS INT)
		   ,fil.ClientInvoiceLineUniqueID
		   ,'1'
           ,ad.SupplierID
           ,dv.VendorDescription
           ,ad.ContractID
           ,CASE
				WHEN ar.CostCenter IS NOT NULL
				THEN 'W'  --write off
				ELSE 'A'  --adjustment
			END
           ,at.Adjustment * (cj.ClaimLineValue / NULLIF(at.TotalClaimValue, 0)) AS AdjustmentAmt
		   ,CASE
				WHEN ABS(at.Adjustment) > 1000 THEN 'R'
				ELSE 'P'
			END
           ,GETDATE()
           ,dv.VendorKey
		   ,GETDATE()
           ,@WebUserKey 
           ,ad.BatchKey 
           ,ad.SupplierRef
		   ,ad.ChargeNoteID
		   ,ad.AccountId
		   ,ad.LocationId
		   ,ad.ProductId
		   ,ar.AdjustmentReasonKey
		   ,cj.InvoiceClaimKey
FROM cm.AdjustmentStaging ad
INNER JOIN cm.ClaimJournal cj
	ON cj.VendorNumber = ad.SupplierId
	--AND cj.ClaimMonth = ad.Period
	AND cj.CurrentContractID = ad.ContractId
INNER JOIN cm.FactInvoiceLine fil
	ON fil.ClientInvoiceLineUniqueID = cj.ClientInvoiceLineUniqueID
INNER JOIN @AdjustmentTotal at
	ON at.AdjustmentStagingKey = ad.AdjustmentStagingKey
INNER JOIN dbo.DimVendor dv
	ON dv.VendorNumber = ad.SupplierID
INNER JOIN cm.AdjustmentReason ar
	ON ar.AdjustmentReasonCode = ad.AdjustmentReasonCode
WHERE cj.ClaimType = 'C'
	AND cj.ClaimJournalStatus = 'C'
	AND ad.AdjustmentType = 'C'
	AND ad.BatchKey = @BatchKey

--Find the difference between the total adjustment amount and the total calculated values
SELECT ad.AdjustmentStagingKey, ad.Adjustment, SUM(cj.ClaimLineValue) as AppliedClaimValue, ABS(ad.Adjustment - SUM(cj.ClaimLineValue)) AS AdjDiff
INTO #TotalAdjClaim
FROM cm.AdjustmentStaging ad
INNER JOIN cm.ClaimJournal cj
	ON cj.VendorNumber = ad.SupplierId
	--AND cj.ClaimMonth = ad.Period
	AND cj.CurrentContractID = ad.ContractId
INNER JOIN @AdjustmentTotal at
	ON at.AdjustmentStagingKey = ad.AdjustmentStagingKey
INNER JOIN dbo.DimVendor dv
	ON dv.VendorNumber = ad.SupplierID
INNER JOIN cm.AdjustmentReason ar
	ON ar.AdjustmentReasonCode = ad.AdjustmentReasonCode
WHERE cj.ClaimType = 'A'
	AND cj.ClaimJournalStatus = 'P'
	AND ad.AdjustmentType = 'A'
  	AND ad.BatchKey = @BatchKey
GROUP BY ad.AdjustmentStagingKey, ad.Adjustment

--Find largest adjustment value
SELECT ad.AdjustmentStagingKey, MAX(cj.ClaimLineValue) AS MaxAdj
INTO #MaxAdjClaim
FROM cm.AdjustmentStaging ad
INNER JOIN cm.ClaimJournal cj
	ON cj.VendorNumber = ad.SupplierId
	--AND cj.ClaimMonth = ad.Period
	AND cj.CurrentContractID = ad.ContractId
INNER JOIN @AdjustmentTotal at
	ON at.AdjustmentStagingKey = ad.AdjustmentStagingKey
INNER JOIN dbo.DimVendor dv
	ON dv.VendorNumber = ad.SupplierID
INNER JOIN cm.AdjustmentReason ar
	ON ar.AdjustmentReasonCode = ad.AdjustmentReasonCode
WHERE cj.ClaimType = 'A'
	AND cj.ClaimJournalStatus = 'P'
	AND ad.AdjustmentType = 'C'
  	AND ad.BatchKey = @BatchKey
GROUP BY ad.AdjustmentStagingKey

--Add the difference to the largest adjustment value
UPDATE cj
SET ClaimLineValue = ClaimLineValue + t.AdjDiff
--select ClaimLineValue + t.AdjDiff
FROM cm.AdjustmentStaging ad
INNER JOIN cm.ClaimJournal cj
	ON cj.VendorNumber = ad.SupplierId
	--AND cj.ClaimMonth = ad.Period
	AND cj.CurrentContractID = ad.ContractId
INNER JOIN #MaxAdjClaim a
	ON a.AdjustmentStagingKey = ad.AdjustmentStagingKey
	AND a.MaxAdj = cj.ClaimLineValue
INNER JOIN #TotalAdjClaim t
	ON t.AdjustmentStagingKey = ad.AdjustmentStagingKey

DELETE FROM @AdjustmentTotal
----------------------------------------------------------------------------------------
--Product Adjustment
--Get total claim value for matching claims
INSERT INTO @AdjustmentTotal
SELECT s.AdjustmentStagingKey, s.Adjustment, SUM(cj.ClaimLineValue) AS TotalClaimValue
FROM cm.AdjustmentStaging s
INNER JOIN cm.ClaimJournal cj
	ON cj.VendorNumber = s.SupplierId
	--AND cj.ClaimMonth = s.Period
	AND cj.CurrentContractID = s.ContractId
INNER JOIN cm.FactInvoiceLine fil
	ON fil.ClientInvoiceLineUniqueID = cj.ClientInvoiceLineUniqueID
	AND fil.ProductCode = s.ProductId
WHERE cj.ClaimType = 'C'
	AND cj.ClaimJournalStatus = 'C'
	AND s.AdjustmentType = 'P'
	AND s.BatchKey = @BatchKey
GROUP BY s.AdjustmentStagingKey, s.Adjustment

--Create adjustment claim lines
INSERT INTO [cm].[ClaimJournal]
           ([ClaimMonth]
		   ,[ClientInvoiceLineUniqueID]
		   ,[PyramidCode]
           ,[VendorNumber]
           ,[VendorDescription]
           ,[CurrentContractID]
           ,[ClaimType]
           ,[ClaimLineValue]
           ,[ClaimJournalStatus]
           ,[ClaimJournalStatusDate]
		   ,[VendorKey]
           ,[CreationDate]
           ,[CreatedByWebUserKey]
           ,[ImportBatchKey]
           ,[SupplierRef]
		   ,[ChargeNoteId]
		   ,[CustomerId]
		   ,[BranchCode]
		   ,[ProductCode]
		   ,[AdjustmentReasonKey]
		   ,[OriginalInvoiceClaimKey]
		   )
     SELECT
            CAST(CONVERT(VARCHAR(4), DATEPART(yy, GETDATE())) + CONVERT(VARCHAR(2), DATEPART(mm, GETDATE())) AS INT)
		   ,fil.ClientInvoiceLineUniqueID
		   ,'1'
           ,ad.SupplierID
           ,dv.VendorDescription
           ,ad.ContractID
           ,CASE
				WHEN ar.CostCenter IS NOT NULL
				THEN 'W'  --write off
				ELSE 'A'  --adjustment
			END
           ,at.Adjustment * (cj.ClaimLineValue / NULLIF(at.TotalClaimValue, 0)) AS AdjustmentAmt
		   ,CASE
				WHEN ABS(at.Adjustment) > 1000 THEN 'R'
				ELSE 'P'
			END
           ,GETDATE()
           ,dv.VendorKey
		   ,GETDATE()
           ,@WebUserKey 
           ,ad.BatchKey 
           ,ad.SupplierRef
		   ,ad.ChargeNoteID
		   ,ad.AccountId
		   ,ad.LocationId
		   ,ad.ProductId
		   ,ar.AdjustmentReasonKey
		   ,cj.InvoiceClaimKey
FROM cm.AdjustmentStaging ad
INNER JOIN cm.ClaimJournal cj
	ON cj.VendorNumber = ad.SupplierId
	--AND cj.ClaimMonth = ad.Period
	AND cj.CurrentContractID = ad.ContractId
INNER JOIN cm.FactInvoiceLine fil
	ON fil.ClientInvoiceLineUniqueID = cj.ClientInvoiceLineUniqueID
	AND fil.ProductCode = ad.ProductId
INNER JOIN @AdjustmentTotal at
	ON at.AdjustmentStagingKey = ad.AdjustmentStagingKey
INNER JOIN dbo.DimVendor dv
	ON dv.VendorNumber = ad.SupplierID
INNER JOIN cm.AdjustmentReason ar
	ON ar.AdjustmentReasonCode = ad.AdjustmentReasonCode
WHERE cj.ClaimType = 'C'
	AND cj.ClaimJournalStatus = 'C'
	AND ad.AdjustmentType = 'P'
	AND ad.BatchKey = @BatchKey

--Find the difference between the total adjustment amount and the total calculated values
SELECT ad.AdjustmentStagingKey, ad.Adjustment, SUM(cj.ClaimLineValue) as AppliedClaimValue, ABS(ad.Adjustment - SUM(cj.ClaimLineValue)) AS AdjDiff
INTO #TotalAdjProduct
FROM cm.AdjustmentStaging ad
INNER JOIN cm.ClaimJournal cj
	ON cj.VendorNumber = ad.SupplierId
	--AND cj.ClaimMonth = ad.Period
	AND cj.CurrentContractID = ad.ContractId
INNER JOIN cm.FactInvoiceLine fil
	ON fil.ClientInvoiceLineUniqueID = cj.ClientInvoiceLineUniqueID
	AND fil.ProductCode = ad.ProductId
INNER JOIN @AdjustmentTotal at
	ON at.AdjustmentStagingKey = ad.AdjustmentStagingKey
INNER JOIN dbo.DimVendor dv
	ON dv.VendorNumber = ad.SupplierID
INNER JOIN cm.AdjustmentReason ar
	ON ar.AdjustmentReasonCode = ad.AdjustmentReasonCode
WHERE cj.ClaimType = 'A'
	AND cj.ClaimJournalStatus = 'P'
	AND ad.AdjustmentType = 'P'
  	AND ad.BatchKey = @BatchKey
GROUP BY ad.AdjustmentStagingKey, ad.Adjustment

--Find largest adjustment value
SELECT ad.AdjustmentStagingKey, MAX(cj.ClaimLineValue) AS MaxAdj
INTO #MaxAdjProduct
FROM cm.AdjustmentStaging ad
INNER JOIN cm.ClaimJournal cj
	ON cj.VendorNumber = ad.SupplierId
	--AND cj.ClaimMonth = ad.Period
	AND cj.CurrentContractID = ad.ContractId
INNER JOIN cm.FactInvoiceLine fil
	ON fil.ClientInvoiceLineUniqueID = cj.ClientInvoiceLineUniqueID
	AND fil.ProductCode = ad.ProductId
INNER JOIN @AdjustmentTotal at
	ON at.AdjustmentStagingKey = ad.AdjustmentStagingKey
INNER JOIN dbo.DimVendor dv
	ON dv.VendorNumber = ad.SupplierID
INNER JOIN cm.AdjustmentReason ar
	ON ar.AdjustmentReasonCode = ad.AdjustmentReasonCode
WHERE cj.ClaimType = 'A'
	AND cj.ClaimJournalStatus = 'P'
	AND ad.AdjustmentType = 'P'
  	AND ad.BatchKey = @BatchKey
GROUP BY ad.AdjustmentStagingKey

--Add the difference to the largest adjustment value
UPDATE cj
SET ClaimLineValue = ClaimLineValue + t.AdjDiff
--select ClaimLineValue + t.AdjDiff
FROM cm.AdjustmentStaging ad
INNER JOIN cm.ClaimJournal cj
	ON cj.VendorNumber = ad.SupplierId
	--AND cj.ClaimMonth = ad.Period
	AND cj.CurrentContractID = ad.ContractId
INNER JOIN #MaxAdjProduct a
	ON a.AdjustmentStagingKey = ad.AdjustmentStagingKey
	AND a.MaxAdj = cj.ClaimLineValue
INNER JOIN #TotalAdjProduct t
	ON t.AdjustmentStagingKey = ad.AdjustmentStagingKey

DELETE FROM @AdjustmentTotal
----------------------------------------------------------------------------------------
--Location Adjustment
--Get total claim value for matching claims
INSERT INTO @AdjustmentTotal
SELECT s.AdjustmentStagingKey, s.Adjustment, SUM(cj.ClaimLineValue) AS TotalClaimValue
FROM cm.AdjustmentStaging s
INNER JOIN cm.ClaimJournal cj
	ON cj.VendorNumber = s.SupplierId
	--AND cj.ClaimMonth = s.Period
	AND cj.CurrentContractID = s.ContractId
INNER JOIN cm.FactInvoiceLine fil
	ON fil.ClientInvoiceLineUniqueID = cj.ClientInvoiceLineUniqueID
	AND fil.BranchCode = s.LocationId
WHERE cj.ClaimType = 'C'
	AND cj.ClaimJournalStatus = 'C'
	AND s.AdjustmentType = 'L'
	AND s.BatchKey = @BatchKey
GROUP BY s.AdjustmentStagingKey, s.Adjustment

--Create adjustment claim lines
INSERT INTO [cm].[ClaimJournal]
           ([ClaimMonth]
		   ,[ClientInvoiceLineUniqueID]
		   ,[PyramidCode]
           ,[VendorNumber]
           ,[VendorDescription]
           ,[CurrentContractID]
           ,[ClaimType]
           ,[ClaimLineValue]
           ,[ClaimJournalStatus]
           ,[ClaimJournalStatusDate]
		   ,[VendorKey]
           ,[CreationDate]
           ,[CreatedByWebUserKey]
           ,[ImportBatchKey]
           ,[SupplierRef]
		   ,[ChargeNoteId]
		   ,[CustomerId]
		   ,[BranchCode]
		   ,[ProductCode]
		   ,[AdjustmentReasonKey]
		   ,[OriginalInvoiceClaimKey]
		   )
     SELECT
            CAST(CONVERT(VARCHAR(4), DATEPART(yy, GETDATE())) + CONVERT(VARCHAR(2), DATEPART(mm, GETDATE())) AS INT)
		   ,fil.ClientInvoiceLineUniqueID
		   ,'1'
           ,ad.SupplierID
           ,dv.VendorDescription
           ,ad.ContractID
           ,CASE
				WHEN ar.CostCenter IS NOT NULL
				THEN 'W'  --write off
				ELSE 'A'  --adjustment
			END
           ,at.Adjustment * (cj.ClaimLineValue / NULLIF(at.TotalClaimValue, 0)) AS AdjustmentAmt
		   ,CASE
				WHEN ABS(at.Adjustment) > 1000 THEN 'R'
				ELSE 'P'
			END
           ,GETDATE()
           ,dv.VendorKey
		   ,GETDATE()
           ,@WebUserKey 
           ,ad.BatchKey 
           ,ad.SupplierRef
		   ,ad.ChargeNoteID
		   ,ad.AccountId
		   ,ad.LocationId
		   ,ad.ProductId
		   ,ar.AdjustmentReasonKey
		   ,cj.InvoiceClaimKey
FROM cm.AdjustmentStaging ad
INNER JOIN cm.ClaimJournal cj
	ON cj.VendorNumber = ad.SupplierId
	--AND cj.ClaimMonth = ad.Period
	AND cj.CurrentContractID = ad.ContractId
INNER JOIN cm.FactInvoiceLine fil
	ON fil.ClientInvoiceLineUniqueID = cj.ClientInvoiceLineUniqueID
	AND fil.BranchCode = ad.LocationId
INNER JOIN @AdjustmentTotal at
	ON at.AdjustmentStagingKey = ad.AdjustmentStagingKey
INNER JOIN dbo.DimVendor dv
	ON dv.VendorNumber = ad.SupplierID
INNER JOIN cm.AdjustmentReason ar
	ON ar.AdjustmentReasonCode = ad.AdjustmentReasonCode
WHERE cj.ClaimType = 'C'
	AND cj.ClaimJournalStatus = 'C'
	AND ad.AdjustmentType = 'L'
	AND ad.BatchKey = @BatchKey

--Find the difference between the total adjustment amount and the total calculated values
SELECT ad.AdjustmentStagingKey, ad.Adjustment, SUM(cj.ClaimLineValue) as AppliedClaimValue, ABS(ad.Adjustment - SUM(cj.ClaimLineValue)) AS AdjDiff
INTO #TotalAdjLocation
FROM cm.AdjustmentStaging ad
INNER JOIN cm.ClaimJournal cj
	ON cj.VendorNumber = ad.SupplierId
	--AND cj.ClaimMonth = ad.Period
	AND cj.CurrentContractID = ad.ContractId
INNER JOIN cm.FactInvoiceLine fil
	ON fil.ClientInvoiceLineUniqueID = cj.ClientInvoiceLineUniqueID
	AND fil.BranchCode = ad.LocationId
INNER JOIN @AdjustmentTotal at
	ON at.AdjustmentStagingKey = ad.AdjustmentStagingKey
INNER JOIN dbo.DimVendor dv
	ON dv.VendorNumber = ad.SupplierID
INNER JOIN cm.AdjustmentReason ar
	ON ar.AdjustmentReasonCode = ad.AdjustmentReasonCode
WHERE cj.ClaimType = 'A'
	AND cj.ClaimJournalStatus = 'P'
	AND ad.AdjustmentType = 'L'
  	AND ad.BatchKey = @BatchKey
GROUP BY ad.AdjustmentStagingKey, ad.Adjustment

--Find largest adjustment value
SELECT ad.AdjustmentStagingKey, MAX(cj.ClaimLineValue) AS MaxAdj
INTO #MaxAdjLocation
FROM cm.AdjustmentStaging ad
INNER JOIN cm.ClaimJournal cj
	ON cj.VendorNumber = ad.SupplierId
	--AND cj.ClaimMonth = ad.Period
	AND cj.CurrentContractID = ad.ContractId
INNER JOIN cm.FactInvoiceLine fil
	ON fil.ClientInvoiceLineUniqueID = cj.ClientInvoiceLineUniqueID
	AND fil.BranchCode = ad.LocationId
INNER JOIN @AdjustmentTotal at
	ON at.AdjustmentStagingKey = ad.AdjustmentStagingKey
INNER JOIN dbo.DimVendor dv
	ON dv.VendorNumber = ad.SupplierID
INNER JOIN cm.AdjustmentReason ar
	ON ar.AdjustmentReasonCode = ad.AdjustmentReasonCode
WHERE cj.ClaimType = 'A'
	AND cj.ClaimJournalStatus = 'P'
	AND ad.AdjustmentType = 'L'
  	AND ad.BatchKey = @BatchKey
GROUP BY ad.AdjustmentStagingKey

--Add the difference to the largest adjustment value
UPDATE cj
SET ClaimLineValue = ClaimLineValue + t.AdjDiff
--select ClaimLineValue + t.AdjDiff
FROM cm.AdjustmentStaging ad
INNER JOIN cm.ClaimJournal cj
	ON cj.VendorNumber = ad.SupplierId
	--AND cj.ClaimMonth = ad.Period
	AND cj.CurrentContractID = ad.ContractId
INNER JOIN #MaxAdjLocation a
	ON a.AdjustmentStagingKey = ad.AdjustmentStagingKey
	AND a.MaxAdj = cj.ClaimLineValue
INNER JOIN #TotalAdjLocation t
	ON t.AdjustmentStagingKey = ad.AdjustmentStagingKey

DELETE FROM @AdjustmentTotal
----------------------------------------------------------------------------------------
--ClaimLine (transaction) Adjustment
--Get total claim value for matching claims
INSERT INTO @AdjustmentTotal
SELECT s.AdjustmentStagingKey, s.Adjustment, SUM(cj.ClaimLineValue) AS TotalClaimValue
FROM cm.AdjustmentStaging s
INNER JOIN cm.ClaimJournal cj
	ON cj.VendorNumber = s.SupplierId
	--AND cj.ClaimMonth = s.Period
	AND cj.CurrentContractID = s.ContractId
INNER JOIN cm.FactInvoiceLine fil
	ON fil.ClientInvoiceLineUniqueID = cj.ClientInvoiceLineUniqueID
	AND fil.ClientInvoiceLineUniqueID = s.TransactionId
WHERE cj.ClaimType = 'C'
	AND cj.ClaimJournalStatus = 'C'
	AND s.AdjustmentType = 'T'
	AND s.BatchKey = @BatchKey
GROUP BY s.AdjustmentStagingKey, s.Adjustment

--Create adjustment claim lines
INSERT INTO [cm].[ClaimJournal]
           ([ClaimMonth]
		   ,[ClientInvoiceLineUniqueID]
		   ,[PyramidCode]
           ,[VendorNumber]
           ,[VendorDescription]
           ,[CurrentContractID]
           ,[ClaimType]
           ,[ClaimLineValue]
           ,[ClaimJournalStatus]
           ,[ClaimJournalStatusDate]
		   ,[VendorKey]
           ,[CreationDate]
           ,[CreatedByWebUserKey]
           ,[ImportBatchKey]
           ,[SupplierRef]
		   ,[ChargeNoteId]
		   ,[CustomerId]
		   ,[BranchCode]
		   ,[ProductCode]
		   ,[AdjustmentReasonKey]
		   ,[OriginalInvoiceClaimKey]
		   )
     SELECT
            CAST(CONVERT(VARCHAR(4), DATEPART(yy, GETDATE())) + CONVERT(VARCHAR(2), DATEPART(mm, GETDATE())) AS INT)
		   ,fil.ClientInvoiceLineUniqueID
		   ,'1'
           ,ad.SupplierID
           ,dv.VendorDescription
           ,ad.ContractID
           ,CASE
				WHEN ar.CostCenter IS NOT NULL
				THEN 'W'  --write off
				ELSE 'A'  --adjustment
			END
			,CASE
				WHEN cj.ClaimLineValue = at.TotalClaimValue  --if only one claim line included use the AdjAmt (in case of a zero claim value)
				THEN at.Adjustment
				ELSE at.Adjustment * (cj.ClaimLineValue / NULLIF(at.TotalClaimValue,0)) 
			END AS AdjustmentAmt
		   ,CASE
				WHEN ABS(at.Adjustment) > 1000 THEN 'R'
				ELSE 'P'
			END
           ,GETDATE()
           ,dv.VendorKey
		   ,GETDATE()
           ,@WebUserKey 
           ,ad.BatchKey 
           ,ad.SupplierRef
		   ,ad.ChargeNoteID
		   ,ad.AccountId
		   ,ad.LocationId
		   ,ad.ProductId
		   ,ar.AdjustmentReasonKey
		   ,cj.InvoiceClaimKey
FROM cm.AdjustmentStaging ad
INNER JOIN cm.ClaimJournal cj
	ON cj.VendorNumber = ad.SupplierId
	--AND cj.ClaimMonth = ad.Period
	AND cj.CurrentContractID = ad.ContractId
INNER JOIN cm.FactInvoiceLine fil
	ON fil.ClientInvoiceLineUniqueID = cj.ClientInvoiceLineUniqueID
	AND fil.ClientInvoiceLineUniqueID = ad.TransactionId
INNER JOIN @AdjustmentTotal at
	ON at.AdjustmentStagingKey = ad.AdjustmentStagingKey
INNER JOIN dbo.DimVendor dv
	ON dv.VendorNumber = ad.SupplierID
INNER JOIN cm.AdjustmentReason ar
	ON ar.AdjustmentReasonCode = ad.AdjustmentReasonCode
WHERE cj.ClaimType = 'C'
	AND cj.ClaimJournalStatus = 'C'
	AND ad.AdjustmentType = 'T'
	AND ad.BatchKey = @BatchKey

--Find the difference between the total adjustment amount and the total calculated values
SELECT ad.AdjustmentStagingKey, ad.Adjustment, SUM(cj.ClaimLineValue) as AppliedClaimValue, ABS(ad.Adjustment - SUM(cj.ClaimLineValue)) AS AdjDiff
INTO #TotalAdjClaimLine
FROM cm.AdjustmentStaging ad
INNER JOIN cm.ClaimJournal cj
	ON cj.VendorNumber = ad.SupplierId
	--AND cj.ClaimMonth = ad.Period
	AND cj.CurrentContractID = ad.ContractId
INNER JOIN cm.FactInvoiceLine fil
	ON fil.ClientInvoiceLineUniqueID = cj.ClientInvoiceLineUniqueID
	AND fil.ClientInvoiceLineUniqueID = ad.TransactionId
INNER JOIN @AdjustmentTotal at
	ON at.AdjustmentStagingKey = ad.AdjustmentStagingKey
INNER JOIN dbo.DimVendor dv
	ON dv.VendorNumber = ad.SupplierID
INNER JOIN cm.AdjustmentReason ar
	ON ar.AdjustmentReasonCode = ad.AdjustmentReasonCode
WHERE cj.ClaimType = 'A'
	AND cj.ClaimJournalStatus = 'P'
	AND ad.AdjustmentType = 'T'
  	AND ad.BatchKey = @BatchKey
GROUP BY ad.AdjustmentStagingKey, ad.Adjustment

--Find largest adjustment value
SELECT ad.AdjustmentStagingKey, MAX(cj.ClaimLineValue) AS MaxAdj
INTO #MaxAdjClaimLine
FROM cm.AdjustmentStaging ad
INNER JOIN cm.ClaimJournal cj
	ON cj.VendorNumber = ad.SupplierId
	--AND cj.ClaimMonth = ad.Period
	AND cj.CurrentContractID = ad.ContractId
INNER JOIN cm.FactInvoiceLine fil
	ON fil.ClientInvoiceLineUniqueID = cj.ClientInvoiceLineUniqueID
	AND fil.ClientInvoiceLineUniqueID = ad.TransactionId
INNER JOIN @AdjustmentTotal at
	ON at.AdjustmentStagingKey = ad.AdjustmentStagingKey
INNER JOIN dbo.DimVendor dv
	ON dv.VendorNumber = ad.SupplierID
INNER JOIN cm.AdjustmentReason ar
	ON ar.AdjustmentReasonCode = ad.AdjustmentReasonCode
WHERE cj.ClaimType = 'A'
	AND cj.ClaimJournalStatus = 'P'
	AND ad.AdjustmentType = 'T'
  	AND ad.BatchKey = @BatchKey
GROUP BY ad.AdjustmentStagingKey

--Add the difference to the largest adjustment value
UPDATE cj
SET ClaimLineValue = ClaimLineValue + t.AdjDiff
--select ClaimLineValue + t.AdjDiff
FROM cm.AdjustmentStaging ad
INNER JOIN cm.ClaimJournal cj
	ON cj.VendorNumber = ad.SupplierId
	--AND cj.ClaimMonth = ad.Period
	AND cj.CurrentContractID = ad.ContractId
INNER JOIN #MaxAdjClaimLine a
	ON a.AdjustmentStagingKey = ad.AdjustmentStagingKey
	AND a.MaxAdj = cj.ClaimLineValue
INNER JOIN #TotalAdjClaimLine t
	ON t.AdjustmentStagingKey = ad.AdjustmentStagingKey

DELETE FROM @AdjustmentTotal
----------------------------------------------------------------------------------------


END

--Create supplier claim if one does not exist for the current month
DECLARE @NewSupplierClaimRowData TABLE (
		SupplierClaimKey KEY_NORMAL_TYPE,
		ClaimStatus varchar(1))

INSERT INTO [cm].[SupplierClaim]
           ([VendorKey]
           ,[ClaimMonth]
           ,[CreationDate]
           ,[CreatedByWebUserKey]
           ,[ClaimStatus])

	OUTPUT 
			INSERTED.SupplierClaimKey,
			INSERTED.ClaimStatus
			INTO @NewSupplierClaimRowData
    
	SELECT DISTINCT 
		cj.VendorKey,
		CAST(CONVERT(VARCHAR(4), DATEPART(yy, GETDATE())) + CONVERT(VARCHAR(2), DATEPART(mm, GETDATE())) AS INT), 
		GETDATE(),
		1,
		'P'
	FROM cm.ClaimJournal cj
	LEFT JOIN cm.SupplierClaim c
		ON c.VendorKey = cj.VendorKey
		AND c.ClaimMonth = cj.ClaimMonth
		AND c.ClaimStatus IN ('P', 'R')  --don't create new if these exist 
	WHERE cj.ImportBatchKey = @BatchKey
		AND c.SupplierClaimKey IS NULL
		
--Log new Supplier Claim
INSERT INTO cm.ClaimLog
	(
	[WebUserKey],
	[SupplierClaimKey],
	[Notes],
	[CreationDate],
	[ClaimAction]
	)
SELECT 
	1,
	d.SupplierClaimKey,
	'From: blank To: ' + s.SupplierClaimStatusDescription,
	GETDATE(),
	'Create Supplier Claim'
FROM @NewSupplierClaimRowData d
INNER JOIN cm.SupplierClaimStatus s
	ON s.SupplierClaimStatus = d.ClaimStatus

--If the supplier claims status is R (ready for claim) and an associated supplier claim line has been created, the supplier claim should change to P (pending)
	DECLARE @SupplierClaimUpdateRowData TABLE (
		SupplierClaimKey KEY_NORMAL_TYPE,
		ClaimStatus varchar(1))
	
	UPDATE sc
	SET ClaimStatus = 'P'
	OUTPUT INSERTED.SupplierClaimKey,
			INSERTED.ClaimStatus
		INTO @SupplierClaimUpdateRowData
	FROM cm.SupplierClaim sc
	INNER JOIN cm.ClaimJournal cj
		ON cj.VendorKey = sc.VendorKey
		AND cj.ClaimMonth = sc.ClaimMonth
	WHERE cj.ImportBatchKey = @BatchKey
		AND sc.ClaimStatus = 'R'

--Log Supplier Claim status change
INSERT INTO cm.ClaimLog
	(
	[WebUserKey],
	[SupplierClaimKey],
	[Notes],
	[CreationDate],
	[ClaimAction]
	)
SELECT 
	1,
	d.SupplierClaimKey,
	'From: Ready to Claim  To: ' + s.SupplierClaimStatusDescription,
	GETDATE(),
	'Supplier Claim System Status Change'
FROM @SupplierClaimUpdateRowData d
INNER JOIN cm.SupplierClaimStatus s
	ON s.SupplierClaimStatus = d.ClaimStatus
		
--Create Adjustment Log entry
INSERT INTO cm.ClaimJournalLog (WebUserKey, ClaimJournalKey, Notes, CreationDate, ClaimAction)
SELECT
	cj.CreatedByWebUserKey,
	ClaimJournalKey,
	'From: blank To: ' + s.ClaimJournalStatusDescription +'; Adjustment Reason = ' + ar.AdjustmentReasonValue,
	GETDATE(),
	'Create Adjustment Claim Line'
FROM cm.ClaimJournal cj
INNER JOIN cm.ClaimJournalStatus s
	ON s.ClaimJournalStatus = cj.ClaimJournalStatus
LEFT JOIN cm.AdjustmentReason ar ON cj.AdjustmentReasonKey = ar.AdjustmentReasonKey
WHERE ImportBatchKey = @BatchKey

SELECT
	AdjustmentStagingKey,
	RowNumber,
	ErrorMessage,
	ContractID,
	SupplierID
FROM cm.AdjustmentStaging
WHERE BatchKey = @BatchKey
	AND ErrorMessage IS NOT NULL
GO
