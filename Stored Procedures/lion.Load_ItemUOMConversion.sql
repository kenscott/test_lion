SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO










CREATE PROCEDURE [lion].[Load_ItemUOMConversion]
AS

/*

EXEC lion.Load_ItemUOMConversion

*/


SET ANSI_WARNINGS OFF
SET ARITHABORT OFF
SET ARITHIGNORE ON

SET NOCOUNT ON

EXEC LogDCPEvent 'ETL - Load_ItemUOMConversion', 'B'


DECLARE 
	@RowsI Quantity_Normal_type, 
	@RowsU Quantity_Normal_type, 
	@RowsD Quantity_Normal_type

SET @RowsI = 0
SET @RowsU = 0
SET @RowsD = 0



TRUNCATE TABLE dbo.ItemUOMConversion


INSERT INTO dbo.ItemUOMConversion
(
	ItemKey,
	BuyingConvFact,
	CostingConvFactor,
	PricingConvFact,
	SellingConvFactor
)
SELECT DISTINCT
	ItemKey,
	ISNULL(CAST(BuyingConvFact AS Decimal(19,3)) / 1000, 1),
	ISNULL(CAST(CostingConvFactor AS Decimal(19,3)) / 1000, 1),
	ISNULL(CAST(PricingConvFact AS Decimal(19,3)) / 1000, 1),
	ISNULL(CAST(SellingConvFactor AS Decimal(19,3)) / 1000, 1)
FROM dbo.DimItem di
INNER JOIN Lion_Staging.dbo.UnitsOfMeasure uc 
	ON REPLICATE('0', 3 - LEN(UnitCode)) + UnitCode = di.ItemUDVarChar18



SET @RowsI = @RowsI + @@RowCount

--   SELECT DISTINCT BuyingConvFact FROM Lion_Staging.dbo.UnitCode ORDER BY 1



SELECT TOP 10 * FROM dbo.ItemUOMConversion


EXEC LogDCPEvent 'ETL - Load_ItemUOMConversion', 'E', @RowsI, @RowsU, @RowsD















GO
