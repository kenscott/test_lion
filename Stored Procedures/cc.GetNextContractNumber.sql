SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
	exec cc.GetNextContractNumber '22'
*/

CREATE PROCEDURE [cc].[GetNextContractNumber](@Originator VARCHAR(20)) AS
BEGIN
	BEGIN TRANSACTION
	SET TRANSACTION ISOLATION LEVEL REPEATABLE READ

	IF EXISTS (SELECT * FROM cc.SupplierContractNumberCounter WHERE Originator = @Originator)
		UPDATE cc.SupplierContractNumberCounter SET ContractNumberCounter = ContractNumberCounter + 1 WHERE Originator = @Originator
	ELSE IF EXISTS( SELECT * FROM cc.SupplierContract WHERE Originator = @Originator )
		INSERT INTO cc.SupplierContractNumberCounter(Originator, ContractNumberCounter) 
			SELECT Originator, Max(ContractNumber) + 1
			FROM cc.SupplierContract
			WHERE Originator = @Originator
			GROUP BY Originator
	ELSE
		INSERT INTO cc.SupplierContractNumberCounter( Originator, ContractNumberCounter ) VALUES ( @Originator, 1 )

	SELECT REPLICATE('0',6-LEN(CAST(ContractNumberCounter AS VARCHAR(10)))) + CAST(ContractNumberCounter AS VARCHAR(10)) AS ContractNumberCounter
	FROM cc.SupplierContractNumberCounter WHERE Originator = @Originator

	COMMIT TRANSACTION

END

GO
