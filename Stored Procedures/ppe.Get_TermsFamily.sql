SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [ppe].[Get_TermsFamily] (@RequestKey INT) AS
BEGIN
	
	DECLARE @AccountKey INT, @ItemGroup3Key INT 

	--SELECT @AccountKey =247075, @ItemGroup3Key =13589
	SELECT  @AccountKey =AccountKey, @ItemGroup3Key=ItemGroup3Key FROM ppe.RequestMain WHERE RequestKey = @RequestKey

	DECLARE @SPGSales MONEY

	SELECT @SPGSales = SUM(TotalActualPrice)
	FROM dbo.FactInvoiceLine fil
	INNER JOIN dbo.DimItem di ON di.ItemKey = fil.ItemKey
	WHERE fil.Last12MonthsIndicator = N'Y'
	AND fil.AccountKey = @AccountKey
	AND di.ItemGroup3Key = @ItemGroup3Key

	SELECT 
		NEWID() AS GUID,
		a.AccountNumber,
		a.AccountName,
		a.LLSPG,
		a.LLSPGDescription,
		a.ItemNumber,
		a.ItemDescription,
		a.GreenDiscount,
		a.AmberDiscount,
		a.RedDiscount,
		a.CurrTermDisc,
		a.CurrTermPrice,
		r.Discount AS PendingDiscount,
		r.Price AS PendingPrice,
		r.StatusName AS PendingStatus,
		a.Sales, -- sales unknown.  question asked via LIO-455
		a.ContractClaimsPercent,
		a.LastChangeDate
	 FROM
	(
		SELECT 
			da.AccountClientUniqueIdentifier AS AccountNumber,
			da.AccountName,
			dig3.IG3Level1 AS LLSPG,
			dig3.IG3Level1UDVarchar1 AS LLSPGDescription,
			NULL AS ItemNumber,
			NULL AS ItemDescription,
			ct.AccountKey,
			ct.ItemGroup3Key,
			ct.ItemKey,
			(SELECT Discount FROM ppe.fn_GetDiscount(@AccountKey,@ItemGroup3Key, 1, NULL, NULL, 'G')) AS GreenDiscount,
			(SELECT Discount FROM ppe.fn_GetDiscount(@AccountKey,@ItemGroup3Key, 1, NULL, NULL, 'A')) AS AmberDiscount,
			(SELECT Discount FROM ppe.fn_GetDiscount(@AccountKey,@ItemGroup3Key, 1, NULL, NULL, 'R')) AS RedDiscount,
			1.0 - (1.0 - CAST(ISNULL(Usualdiscount1,'0.0') AS DEC(38,8))/100.0) * (1.0 - CAST(ISNULL(Usualdiscount2,'0.0') AS DEC(38,8))/100.0) AS CurrTermDisc,
			NULL AS CurrTermPrice,
			@SPGSales AS Sales,
			ISNULL(v.ContractClaimsPercent,0) ContractClaimsPercent,
			ct.LastChangeDate
		FROM lion.CustomerTerms ct 
		JOIN dbo.DimAccount da ON da.AccountKey = ct.AccountKey
		JOIN dbo.DimItemGroup3 dig3 ON dig3.ItemGroup3Key = ct.ItemGroup3Key
		LEFT JOIN lion.vwAccountLLSPG v ON v.AccountKey = ct.AccountKey AND v.ItemGroup3Key = ct.ItemGroup3Key
		WHERE ct.AccountKey = @AccountKey
		AND ct.ItemGroup3Key = @ItemGroup3Key
		AND ct.ItemKey = 1

		UNION

		SELECT 
			da.AccountClientUniqueIdentifier AS AccountNumber,
			da.AccountName,
			dig3.IG3Level1 AS LLSPG,
			dig3.IG3Level1UDVarchar1 AS LLSPGDescription,
			di.ItemClientUniqueIdentifier AS ItemNumber, 
			di.ItemDescription,
			ct.AccountKey,
			di.ItemGroup3Key,
			ct.ItemKey,
			gd.Discount GreenDiscount, 
			ad.Discount AS AmberDiscount, 
			rd.Discount AS RedDiscount,
			CAST(ct.ExceptionDiscount AS DEC(38,8))/100.0 AS CurrTermDisc,
			CAST(ct.ExceptionFixedPrice AS MONEY) AS CurrTermPrice,
			s.Sales,
			NULL AS ContractClaimsPercent,
			ct.LastChangeDate
		FROM lion.CustomerTerms ct 
		LEFT JOIN ( SELECT filpbs1.* FROM 
					[lion].[vwFactInvoiceLinePricebandScore] filpbs1
					JOIN (SELECT AccountKey, ItemGroup3Key, ItemKey, MAX(InvoiceDateDayKey) MaxDateKey
					FROM [lion].[vwFactInvoiceLinePricebandScore] 
					GROUP BY AccountKey, ItemGroup3Key, ItemKey) mx 
						ON mx.AccountKey = filpbs1.AccountKey
						AND mx.ItemGroup3Key = filpbs1.ItemGroup3Key
						AND mx.ItemKey = filpbs1.ItemKey
						AND mx.MaxDateKey = filpbs1.InvoiceDateDayKey) filpbs
			ON ct.AccountKey = filpbs.AccountKey
			AND ct.ItemKey = filpbs.ItemKey
		JOIN dbo.DimAccount da ON da.AccountKey = ct.AccountKey
		JOIN dbo.DimItem di ON di.ItemKey = ct.ItemKey
		JOIN dbo.DimItemGroup3 dig3 ON dig3.ItemGroup3Key = di.ItemGroup3Key
		CROSS APPLY ppe.fn_GetDiscount(filpbs.AccountKey, filpbs.ItemGroup3Key, filpbs.ItemKey, filpbs.GreenPrice, filpbs.UnitListPrice, 'G') gd
		CROSS APPLY ppe.fn_GetDiscount(filpbs.AccountKey, filpbs.ItemGroup3Key, filpbs.ItemKey, filpbs.GreenPrice, filpbs.UnitListPrice, 'A') ad
		CROSS APPLY ppe.fn_GetDiscount(filpbs.AccountKey, filpbs.ItemGroup3Key, filpbs.ItemKey, filpbs.GreenPrice, filpbs.UnitListPrice, 'R') rd
		LEFT JOIN (SELECT fil.AccountKey, fil.ItemKey, SUM(TotalActualPrice) Sales
					FROM dbo.FactInvoiceLine fil
					INNER JOIN dbo.DimItem di ON di.ItemKey = fil.ItemKey
					WHERE fil.Last12MonthsIndicator = N'Y'
					AND fil.AccountKey = @AccountKey
					AND di.ItemGroup3Key = @ItemGroup3Key
					GROUP BY fil.AccountKey, fil.ItemKey) s
			ON s.ItemKey = filpbs.ItemKey
			AND s.AccountKey = filpbs.AccountKey
		WHERE di.ItemGroup3Key=@ItemGroup3Key
		AND ct.AccountKey=@AccountKey
	) a
	LEFT JOIN 
		(SELECT 
			rm.AccountKey,
			rm.ItemGroup3Key,
			rm.ItemKey,
			np.Price,
			rsd.Discount,
			rs.StatusName
		FROM ppe.RequestMain rm
		JOIN (SELECT AccountKey, ItemGroup3Key, ItemKey, MAX(RequestKey) mxRequestKey
				FROM ppe.RequestMain
				GROUP BY  AccountKey, ItemGroup3Key, ItemKey) mx
			ON mx.AccountKey = rm.AccountKey
			AND mx.ItemGroup3Key = rm.ItemGroup3Key
			AND mx.ItemKey = rm.ItemKey
			AND mx.mxRequestKey = rm.RequestKey
		JOIN ppe.RequestStatus rs ON rs.StatusKey = rm.StatusKey
		CROSS APPLY ppe.fn_GetSimpleDiscount(rm.ProposedExceptionFixedPrice, rm.TradePrice, rm.ProposedExceptionDiscount, rm.ProposedCompoundDiscount) rsd
		CROSS APPLY ppe.fn_GetPrice(rm.NewExceptionFixedPrice, rm.TradePrice, rm.NewExceptionDiscount, rm.NewCompoundDiscount) np
		WHERE rm.AccountKey = @AccountKey
		AND rm.ItemGroup3Key = @ItemGroup3Key) r
		ON r.AccountKey = a.AccountKey
		AND r.ItemGroup3Key = a.ItemGroup3Key
		AND r.ItemKey = a.ItemKey

END
GO
