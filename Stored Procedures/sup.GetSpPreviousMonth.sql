SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [sup].[GetSpPreviousMonth]
    @claimMonth INT,
    @vendorKey INT
AS

SET NOCOUNT ON;
SET ANSI_WARNINGS OFF ;

SELECT NEWID() GUID,
	   cj.ClaimMonth,
       COUNT(DISTINCT cj.InvoiceClaimKey) AS Invoices,
       COUNT(*) AS ClaimLines,
       SUM(cj.ClaimLineValue) AS InvoiceAmount
FROM cm.ClaimJournal cj
WHERE cj.VendorKey = @vendorKey
      AND ClaimMonth = @claimMonth
      AND cj.ClaimJournalStatus = 'C'
GROUP BY cj.ClaimMonth,
         cj.VendorKey;
GO
