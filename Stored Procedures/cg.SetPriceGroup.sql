SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE PROCEDURE [cg].[SetPriceGroup]
	@PlaybookDataPointGroupKey [int],
	@PricingRulePlaybookKey [int],
	@ScenarioKey [int]
WITH EXECUTE AS CALLER
AS
/*
EXEC cg.SetPriceGroup 1, 1, 1
*/

SET QUOTED_IDENTIFIER ON 
SET ANSI_NULLS ON 
SET ARITHABORT ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
SET CONCAT_NULL_YIELDS_NULL ON
SET NUMERIC_ROUNDABORT OFF
SET NOCOUNT ON

DECLARE 
	@RowsI INT, 
	@RowsU INT, 
	@RowsD INT, 
	@ProjectKey Key_Normal_type,
	@PricingRuleWhereClause Description_Huge_type, 
	@PricingRuleJoinClause Description_Huge_type, 
	@PricingRuleAttributeSourceSchema NVARCHAR(256), 
	@PricingRuleAttributeSourceTable Description_Normal_type, 
	@tmpValuesAssigned NVARCHAR(MAX),
	@DataPointReUseJoinClause NVARCHAR(MAX), 
	@DataPointReUseWhereClause NVARCHAR(MAX), 
	@DataPointReUseIndicator CHAR(1),
	@PricingRuleKey Key_Normal_type,
	@AttributeType Description_Normal_type,
	@First INT,
	@TempFieldList NVARCHAR(MAX),
	@TempFieldVarList NVARCHAR(MAX),
	@TempFieldVarListWithTypesQualified NVARCHAR(MAX),
	@TempJoinList NVARCHAR(MAX),
	@PricingRuleAttributeJoinClause1 Description_Normal_type, 
	@PricingRuleAttributeJoinClause2 Description_Normal_type,
	@MinimumPoints INT,
	@HavingClause Description_Huge_type,
	@SectionNumber SMALLINT = 1,
	@MinimumSales DEC(38,8),
	@RulingPointThreshold INT,
	@SQLStmt NVARCHAR(MAX)

SET @RowsI = 0
SET @RowsU = 0
SET @RowsD = 0

 

SELECT @ProjectKey = ProjectKey FROM dbo.PlaybookDataPointGroup WHERE PlaybookDataPointGroupKey = @PlaybookDataPointGroupKey


PRINT '/*'
SELECT '@PlaybookDataPointGroupKey: ', @PlaybookDataPointGroupKey
SELECT '@PricingRulePlaybookKey: ', @PricingRulePlaybookKey 
SELECT '@ScenarioKey: ', @ScenarioKey
SELECT '@ProjectKey: ', @ProjectKey
PRINT '*/'





EXEC LogDCPEvent 'DCP --- cg.SetPriceGroup: Delete', 'B',0,0,0,@ProjectKey




EXEC DBA_IndexRebuild 'PlaybookDatapointPricingGroup', 'Drop_NoClustered', NULL, 'cg'

--First, delete all data that already exists for this data point group.  This includes EVERYTHING that falls under Price groups, including proposals
DELETE FROM cg.PlaybookPricingGroupPriceBand WHERE PlaybookPricingGroupKey IN (SELECT PlaybookPricingGroupKey FROM dbo.PlaybookPricingGroup WHERE PlaybookDataPointGroupKey = @PlaybookDataPointGroupKey)
SET @RowsD = @RowsD + @@RowCount
DELETE FROM cg.PlaybookDataPointPricingGroup WHERE PlaybookDataPointPricingGroupKey IN (SELECT PlaybookDataPointPricingGroupKey FROM cg.PlaybookDatapointPricingGroup a JOIN dbo.PlaybookPricingGroup B ON a.PlaybookPricingGroupKey = B.PlaybookPricingGroupKey WHERE PlaybookDataPointGroupKey = @PlaybookDataPointGroupKey)
SET @RowsD = @RowsD + @@RowCount
DELETE FROM dbo.PlaybookPricingGroup WHERE PlaybookDataPointGroupKey = @PlaybookDataPointGroupKey
SET @RowsD = @RowsD + @@RowCount

--SET @SQLStmt = 'UPDATE STATISTICS dbo.PlaybookPricingGroupPriceBand'
--EXEC SP_EXECUTESQL @SQLStmt

--SET @SQLStmt = 'UPDATE STATISTICS cg.PlaybookDataPointPricingGroup'
--EXEC SP_EXECUTESQL @SQLStmt

--SET @SQLStmt = 'UPDATE STATISTICS dbo.PlaybookPricingGroup'
--EXEC SP_EXECUTESQL @SQLStmt

EXEC LogDCPEvent 'DCP --- cg.SetPriceGroup: Delete', 'E', @RowsI, @RowsU, @RowsD


SET @RowsI = 0
SET @RowsU = 0
SET @RowsD = 0


--EXEC DBA_IndexRebuild 'PlaybookDataPoint', 'Drop'


DECLARE CurPriceGroup CURSOR FAST_FORWARD FOR
SELECT 
	PricingRuleKey, 
	MinimumPoints, 
	MinimumSales, 
	ISNULL(PricingRuleWhereClause, ''), 
	ISNULL(PricingRuleJoinClause, ''), 
	RulingPointThreshold, 
	DataPointReUseIndicator,
	ISNULL(HavingClause, N''),
	ISNULL(SectionNumber, 1) AS SectionNumber
FROM dbo.ATKPricingRule
WHERE PricingRulePlaybookKey = @PricingRulePlaybookKey
ORDER BY PricingRuleSequenceNumber

OPEN CurPriceGroup
FETCH NEXT FROM CurPriceGroup INTO @PricingRuleKey, @MinimumPoints, @MinimumSales, @PricingRuleWhereClause, @PricingRuleJoinClause, @RulingPointThreshold, @DataPointReUseIndicator, @HavingClause, @SectionNumber

WHILE (@@FETCH_STATUS <> -1)
BEGIN

	--Load the fields...............................................................
	SET @TempJoinList = @PricingRuleJoinClause + '
	'
	SET @TempFieldList = ''
	SET @tmpValuesAssigned = ''
	SET @TempFieldVarList = ''
	SET @TempFieldVarListWithTypesQualified = ''

	DECLARE CurRuleField CURSOR FAST_FORWARD FOR
	SELECT 
		aprat.PricingRuleAttributeType, 
		PricingRuleAttributeSourceSchema, 
		PricingRuleAttributeSourceTable, 
		aprat.PricingRuleAttributeJoinClause1, 
		aprat.PricingRuleAttributeJoinClause2
	FROM dbo.ATKPricingRuleAttribute apra
	JOIN dbo.ATKPricingRuleAttributeType  aprat 
		ON apra.PricingRuleAttributeTypeKey = aprat.PricingRuleAttributeTypeKey
	WHERE 
		apra.PricingRuleKey = @PricingRuleKey
	ORDER BY 
		apra.PricingRuleAttributeSequenceNumber ASC

	OPEN CurRuleField
	FETCH NEXT FROM CurRuleField INTO @AttributeType, @PricingRuleAttributeSourceSchema, @PricingRuleAttributeSourceTable, @PricingRuleAttributeJoinClause1, @PricingRuleAttributeJoinClause2

	SET @First = 1
	WHILE(@@FETCH_STATUS <> -1)
	BEGIN

		IF @First = 1
		BEGIN
			SET @TempFieldList = @PricingRuleAttributeSourceTable + '.' + @AttributeType
			SET @TempFieldVarList = '@'+@AttributeType
			SET @tmpValuesAssigned = 'CAST(@'+@AttributeType+' AS NVARCHAR(MAX))'
			SET @TempFieldVarListWithTypesQualified =  'CAST(' + @PricingRuleAttributeSourceTable + '.'+@AttributeType+' AS NVARCHAR(MAX))'
			SET @First = 0
		END
		ELSE
		BEGIN
			SET @TempFieldList = @TempFieldList+','+ @PricingRuleAttributeSourceTable + '.' + @AttributeType
			SET @TempFieldVarList = @TempFieldVarList+',@'+@AttributeType
			SET @tmpValuesAssigned = @tmpValuesAssigned+'+ '',''+ CAST(@'+@AttributeType+' AS NVARCHAR(MAX))'
			SET @TempFieldVarListWithTypesQualified = @TempFieldVarListWithTypesQualified+'+ '','' + CAST('+@PricingRuleAttributeSourceTable + '.' + @AttributeType+' AS NVARCHAR(MAX))'
		END

		PRINT '/*'
		PRINT '@AttributeType: ' + ISNULL(@AttributeType, '')
		PRINT '@PricingRuleAttributeSourceSchema: ' + ISNULL(@PricingRuleAttributeSourceSchema, '')
		PRINT '@PricingRuleAttributeSourceTable: ' + ISNULL(@PricingRuleAttributeSourceTable, '')
		PRINT '*/'

		--Set the first join, if it's needed
		IF @TempJoinList NOT LIKE '%'+@PricingRuleAttributeJoinClause1+'%'
		BEGIN
			SET @TempJoinList = @TempJoinList + ' 
			' +	@PricingRuleAttributeJoinClause1 + ' ' 
		END

		--Set the second join, if it's needed
		IF @TempJoinList NOT LIKE '%'+@PricingRuleAttributeJoinClause2+'%' AND ISNULL(@PricingRuleAttributeJoinClause2, '') <> ''
		BEGIN
			SET @TempJoinList = @TempJoinList + '
			'+@PricingRuleAttributeJoinClause2 + ' ' 
		END

		FETCH NEXT FROM CurRuleField INTO @AttributeType, @PricingRuleAttributeSourceSchema, @PricingRuleAttributeSourceTable, @PricingRuleAttributeJoinClause1, @PricingRuleAttributeJoinClause2
	END


	CLOSE CurRuleField
	DEALLOCATE CurRuleField


	IF @PricingRuleWhereClause IS NOT NULL AND RTRIM(@PricingRuleWhereClause) <> ''
		SET @PricingRuleWhereClause = 'AND '+@PricingRuleWhereClause

	--------------------------------------------------------
	---------------Handle Data Point Re-Use-----------------
	--------------------------------------------------------
	IF @DataPointReUseIndicator = 'Y' or 1=1	--If we'd like to re-use data points
	BEGIN
		SET @DataPointReUseJoinClause = 'LEFT JOIN cg.PlaybookDataPointPricingGroup PDPPG ON PDPPG.PlaybookDataPointKey = PlaybookDataPoint.PlaybookDataPointKey AND PDPPG.SectionNumber = ' + CAST(@SectionNumber AS NVARCHAR(25))
		SET @DataPointReUseWhereClause = 'PDPPG.PlaybookPricingGroupKey IS NULL AND'
	END
	ELSE		--Else, do not re-use data points
	BEGIN
		SET @DataPointReUseJoinClause = 'LEFT JOIN cg.PlaybookDataPointPricingGroup PDPPG on PDPPG.PlaybookDataPointKey = PlaybookDataPoint.PlaybookDataPointKey'
		SET @DataPointReUseWhereClause = 'PDPPG.PlaybookPricingGroupKey IS NULL AND'
	END


	PRINT '/*'
	PRINT '@PricingRuleKey: '+ISNULL(CAST(@PricingRuleKey AS VARCHAR(25)), '')
	PRINT '@DataPointReUseIndicator: '+ISNULL(@DataPointReUseIndicator, '')
	PRINT '@DataPointReUseJoinClause: '+@DataPointReUseJoinClause
	PRINT '@DataPointReUseWhereClause: '+@DataPointReUseWhereClause
	PRINT '@TempJoinList: '+@TempJoinList
	PRINT '@TempFieldList: ' + @TempFieldList
	PRINT '@TempFieldVarList: ' + @TempFieldVarList
	PRINT '@tmpValuesAssigned: ' + @tmpValuesAssigned
	PRINT '@TempFieldVarListWithTypesQualified: ' + @TempFieldVarListWithTypesQualified
	PRINT '*/'


	--------------------------------------------------------
	--------------------------------------------------------
	--------------------------------------------------------


	SET @SQLStmt = N'

    DECLARE 
		@RowsI INT, 
		@RowsU INT, 
		@RowsD INT, 
		@MaxDayKey INT

 	SET @RowsI = 0
 	SET @RowsU = 0
 	SET @RowsD = 0

    --SELECT @MaxDayKey = MAX(InvoiceDateDayKey) FROM FactInvoiceLine 

	EXEC LogDCPEvent ''DCP --- cg.SetPriceGroup: Insert PDPPG and PPG for PricingRuleKey '+CAST(@PricingRuleKey AS NVARCHAR(25))+' '', ''B'',0,0,0,'+CAST(@ProjectKey AS NVARCHAR(25))+' 

	; WITH TempA AS (
	SELECT ' + @TempFieldList +'
		--@MaxDayKey - PlaybookDataPoint.LastSaleDayKey AS ''DaysSinceSale''
		,PlaybookDataPoint.ContractKey
		
	--Into #TempDaysSince
	FROM cg.PlaybookDataPoint
		' +@DataPointReUseJoinClause +'
		' + @TempJoinList + '		
	WHERE	'+@DataPointReUseWhereClause+'
			PlaybookDataPoint.PlaybookDataPointGroupKey = ' + CAST(@PlaybookDataPointGroupKey AS NVARCHAR(100)) + '
			' + @PricingRuleWhereClause + ' ' + '
	)
		SELECT ' + REPLACE(@TempFieldVarList, '@', '') +',
		''' + CAST(@TempFieldList AS NVARCHAR(MAX)) + ''' AS GroupingColumns, 
		' + REPLACE(@tmpValuesAssigned, '@', '') + ' AS GroupValues
		INTO #Temp
		FROM TempA
		WHERE 
			' + REPLACE(@tmpValuesAssigned, '@', '') + ' IS NOT NULL
		GROUP BY ' + REPLACE(@TempFieldVarList, '@', '') +'
		HAVING COUNT(*) >= '+CAST(@MinimumPoints AS NVARCHAR(500))+'
		' + @HavingClause + '
		
		DECLARE @PlaybookPricingGroupData TABLE ( 
			PlaybookPricingGroupKey INT,
			GroupingColumns NVARCHAR(MAX),
			GroupValues NVARCHAR(MAX)
		);

		INSERT dbo.PlaybookPricingGroup (PlaybookDataPointGroupKey, GroupingColumns, GroupValues, PricingRuleKey) 
		OUTPUT INSERTED.PlaybookPricingGroupKey, INSERTED.GroupingColumns , INSERTED.GroupValues
		INTO @PlaybookPricingGroupData
		SELECT
			' + CAST(@PlaybookDataPointGroupKey AS NVARCHAR(25)) +',
			 GroupingColumns,
			 GroupValues, 
			 '+CAST(@PricingRuleKey AS NVARCHAR(500))+'
		FROM #Temp
		Set @RowsI = @RowsI + @@RowCount

		
		INSERT INTO cg.PlaybookDataPointPricingGroup (PlaybookDataPointKey,PlaybookPricingGroupKey,PricingRuleKey,RulingMemberFlag,ClaimBackPercent, SectionNumber)
		SELECT
			PlaybookDataPoint.PlaybookDataPointKey,
			Grp.PlaybookPricingGroupKey,
			'+CAST(@PricingRuleKey AS NVARCHAR(500))+',
			''Y'' as PricingGroupRulingMemberFlag,
			PlaybookDataPoint.ClaimBackPercent,
			'+CAST(@SectionNumber AS NVARCHAR(25))+'
		FROM cg.PlaybookDataPoint 
		'+@DataPointReUseJoinClause+'
				' + @TempJoinList + ' 
		JOIN @PlaybookPricingGroupData Grp
			ON 	Grp.GroupingColumns = ''' + CAST(@TempFieldList AS NVARCHAR(1000)) + '''
			AND Grp.GroupValues = ' + @TempFieldVarListWithTypesQualified + '
		WHERE ' +@DataPointReUseWhereClause+'
			PlaybookDataPoint.PlaybookDataPointGroupKey = ' + CAST(@PlaybookDataPointGroupKey AS NVARCHAR(100))+ '
			' + @PricingRuleWhereClause + ' ' + '
		SET @RowsI = @RowsI + @@RowCount

	EXEC LogDCPEvent ''DCP --- cg.SetPriceGroup: Insert PDPPG and PPG for PricingRuleKey '+CAST(@PricingRuleKey AS NVARCHAR(100))+' '', ''E'', @RowsI, @RowsU, @RowsD

	'


	EXEC dbo.PrintNVarcharMax @SQLStmt

	EXEC SP_EXECUTESQL @SQLStmt

	SET @SQLStmt = N'UPDATE STATISTICS dbo.PlaybookPricingGroup'
	EXEC SP_EXECUTESQL @SQLStmt
	
	SET @SQLStmt = N'UPDATE STATISTICS cg.PlaybookDataPointPricingGroup'
	EXEC SP_EXECUTESQL @SQLStmt


	FETCH NEXT FROM CurPriceGroup INTO @PricingRuleKey,@MinimumPoints, @MinimumSales, @PricingRuleWhereClause, @PricingRuleJoinClause, @RulingPointThreshold, @DataPointReUseIndicator, @HavingClause, @SectionNumber
END


CLOSE CurPriceGroup
DEALLOCATE CurPriceGroup


EXEC DBA_IndexRebuild 'PlaybookDatapointPricingGroup', 'Create', NULL, 'cg'

SET @SQLStmt = 'UPDATE STATISTICS cg.PlaybookDatapointPricingGroup'
EXEC SP_EXECUTESQL @SQLStmt


















GO
