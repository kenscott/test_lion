SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE PROCEDURE [dbo].[procArchiveLogs]
AS
    DECLARE @ArchiveMonths INT ,
        @ArchiveDate DATETIME ,
        @RowsI INT ,
        @RowsU INT ,
        @RowsD INT

    SET @RowsI = 0
    SET @RowsU = 0
    SET @RowsD = 0
    SELECT  @ArchiveMonths = CAST(ParamValue AS INT)
    FROM    dbo.Param
    WHERE   ParamName = 'ArchiveMonths'	
    SELECT  @ArchiveDate = DATEADD(MONTH, @ArchiveMonths, GETDATE())

    EXEC LogDCPEvent 'dbo.ArchiveLogs - AuditLog', 'B'
    BEGIN TRY
        BEGIN TRANSACTION
        INSERT  INTO AuditLogArchive
                ( AuditLogKey ,
                  AuditSubject ,
                  AuditAction ,
                  AuditActor ,
                  AuditActorName ,
                  AuditModule ,
                  AuditDetails ,
                  CreationDate ,
                  WebUserKey
                )
                SELECT  AuditLogKey ,
                        AuditSubject ,
                        AuditAction ,
                        AuditActor ,
                        AuditActorName ,
                        AuditModule ,
                        AuditDetails ,
                        CreationDate ,
                        WebUserKey
                FROM    AuditLog
                WHERE   ( CreationDate <= @archiveDate )
        SELECT  @RowsI = @@ROWCOUNT

        DELETE  FROM AuditLog
        WHERE   ( CreationDate <= @archiveDate )
        SELECT  @RowsD = @@ROWCOUNT
	
	
        COMMIT TRAN
    END TRY
    BEGIN CATCH
        IF @@TRANCOUNT > 0
            ROLLBACK TRAN
        EXEC dbo.LogDCPError
    END CATCH

    EXEC LogDCPEvent 'dbo.ArchiveLogs - AuditLog', 'E', @RowsI, @RowsU, @RowsD

    



GO
