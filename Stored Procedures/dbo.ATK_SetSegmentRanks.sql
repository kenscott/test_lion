SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





CREATE         
PROCEDURE [dbo].[ATK_SetSegmentRanks] 
	@SegmentFactorPlaybookKey INT
AS

--Author:  Tom Werz
--Date:  03/31/2005
--Purpose:  To set the rankings for each segment/scenario/type.  This should really be done by the application (it can call this SP if necessary)
SET NOCOUNT ON
DECLARE @ProjectKey INT

SELECT @ProjectKey = PP.ProjectKey
FROM ATKScenario S
JOIN ATKProjectSpecificationScenario P ON S.ScenarioKey = P.ScenarioKey
JOIN PlaybookProject PP ON PP.ProjectSpecificationKey = P.ProjectSpecificationKey
WHERE SegmentFactorPlaybookKey = @SegmentFactorPlaybookKey


EXEC LogDCPEvent 'ATK_SetSegmentRanks', 'B',0,0,0,@ProjectKey

--Highest upper band gets ranked #1
UPDATE ATKSegmentCustomer SET SegmentSequenceNumber =
(SELECT COUNT(*) FROM ATKSegmentCustomer A WHERE A.SegmentFactorPlaybookKey = B.SegmentFactorPlaybookKey AND A.SegmentType = B.SegmentType AND A.SegmentUpperBound > B.SegmentUpperBound)+1
FROM ATKSegmentCustomer B
WHERE B.SegmentFactorPlaybookKey = @SegmentFactorPlaybookKey

EXEC LogDCPEvent 'ATK_SetSegmentRanks', 'E',0,0,0,@ProjectKey





GO
