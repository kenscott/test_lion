SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO






--select * From playbookdatapointgroup
--RBP_CalculateFullScopeAccountBands 129,1
--RBP_CalculateFullScopeAccountBands 130,1

CREATE           
PROCEDURE [dbo].[RBP_CalculateFullScopeAccountBands] 
(
	@PlaybookDataPointGroupKey INT, 
	@SegmentFactorPlaybookKey INT
)

AS

SET QUOTED_IDENTIFIER ON 
SET ANSI_NULLS ON 
SET ARITHABORT ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
SET CONCAT_NULL_YIELDS_NULL ON
SET NUMERIC_ROUNDABORT OFF
DECLARE @RowsI Quantity_Normal_type, @RowsU Quantity_Normal_type, @RowsD Quantity_Normal_type

DECLARE @ProjectKey key_normal_type
SELECT @ProjectKey = ProjectKey FROM PlaybookDataPointGroup WHERE PlaybookDataPointGroupKey = @PlaybookDataPointGroupKey

EXEC LogDCPEvent 'DCP --- RBP_CalculateFullScopeAccountBands', 'B',0,0,0,@ProjectKey
SET @RowsI = 0
SET @RowsU = 0
SET @RowsD = 0

DELETE FROM PlaybookDataPointGroupFullScopeAccountBands
WHERE PlaybookDataPointGroupKey = @PlaybookDataPointGroupKey
SET @RowsD = @RowsD + @@RowCOunt


INSERT INTO PlaybookDataPointGroupFullScopeAccountBands
(PlaybookDataPointGroupKey, AccountKey, TotalActualPrice, SalesBand, SalesRank, AvgGPPerOrder, AvgLinesPerOrder, GPBand, GPRank, BlendedRank, TotalActual12MonthInvoiceLines, TotalActual12MonthInvoices)
	SELECT @PlaybookDataPointGroupKey, AccountKey, TotalActualPrice, SalesBand, SalesRank, AvgGPPerOrder, AvgLinesPerOrder, GPBand, GPRank, 
	CASE (SELECT BlendedRankRule FROM ATKSegmentFactorPlaybook WHERE ATKSegmentFactorPlaybook.SegmentFactorPlaybookKey = @SegmentFactorPlaybookKey)
		WHEN 1 THEN 
					CASE
						WHEN SalesRank = 1 OR GPRank = 1 THEN 'A'
						WHEN SalesRank = 2 OR GPRank = 2 THEN 'B'
						WHEN SalesRank = 3 OR GPRank = 3 THEN 'C'
-- 						WHEN SalesRank = 4 OR GPRank = 4 THEN 'B'
-- 						WHEN SalesRank = 5 OR GPRank = 5 THEN 'C'
-- 						WHEN SalesRank = 6 OR GPRank = 6 THEN 'C'
-- 						WHEN SalesRank = 7 OR GPRank = 7 THEN 'C'
					ELSE NULL END
		ELSE
					CASE
-- 						WHEN SalesRank = 7 OR GPRank = 7 THEN 'C'
-- 						WHEN SalesRank = 6 OR GPRank = 6 THEN 'B'
-- 						WHEN SalesRank = 5 OR GPRank = 5 THEN 'A'
-- 						WHEN SalesRank = 4 OR GPRank = 4 THEN 'B'
						WHEN SalesRank = 3 OR GPRank = 3 THEN 'C'
						WHEN SalesRank = 2 OR GPRank = 2 THEN 'B'
						WHEN SalesRank = 1 OR GPRank = 1 THEN 'A'
					ELSE NULL END
	END AS 'BlendedRank',
	TotalActual12MonthInvoiceLines, 
	TotalActual12MonthInvoices
	FROM
		(SELECT 
		A.AccountKey, A.TotalActualPrice, SalesBand.SegmentLabel AS 'SalesBand', SalesBand.SegmentSequenceNumber AS 'SalesRank', A.AvgGPPerOrder, AvgLinesPerOrder, GPBand.SegmentLabel AS 'GPBand', GPBand.SegmentSequenceNumber AS 'GPRank', TotalActual12MonthInvoiceLines, TotalActual12MonthInvoices
		FROM
		(SELECT 
			FIL.AccountKey, 
			SUM(FIL.TotalActualPrice) TotalActualPrice,
			COUNT(*) AS 'TotalActual12MonthInvoiceLines',
			COUNT(DISTINCT ClientInvoiceID) AS 'TotalActual12MonthInvoices',
			(SUM(FIL.TotalActualPrice)-SUM(FIL.TotalActualCost)) / COUNT(*) 'AvgGPPerOrder',
			COUNT(*) / COUNT(DISTINCT ClientInvoiceID) AS 'AvgLinesPerOrder'
			FROM dbo.FactInvoiceLine FIL
			WHERE Last12MonthsIndicator = 'Y'
			GROUP BY FIL.AccountKey) A
		JOIN ATKSegmentCustomer SalesBand ON 
			A.TotalActualPrice >= SalesBand.SegmentLowerBound 
			AND A.TotalActualPrice < SalesBand.SegmentUpperBound 
			AND SalesBand.SegmentType = 'SalesRank' 
			AND SalesBand.SegmentFactorPlaybookKey = @SegmentFactorPlaybookKey
		JOIN ATKSegmentCustomer GPBand ON 
			A.AvgGPPerOrder >= GPBand.SegmentLowerBound 
			AND A.AvgGPPerOrder < GPBand.SegmentUpperBound 
			AND GPBand.SegmentType = 'GPPerOrder' 
			AND GPBand.SegmentFactorPlaybookKey = @SegmentFactorPlaybookKey) C
--)	
SET @RowsI = @RowsI + @@RowCOunt

DECLARE @USql NVARCHAR(MAX)
SET @USql = 'Update Statistics PlaybookDataPointGroupFullScopeAccountBands'
EXEC (@USql)

EXEC LogDCPEvent 'DCP --- RBP_CalculateFullScopeAccountBands', 'E', @RowsI, @RowsU, @RowsD






GO
