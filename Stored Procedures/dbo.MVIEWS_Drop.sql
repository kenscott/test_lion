SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE   PROCEDURE [dbo].[MVIEWS_Drop]
AS


EXEC LogDCPEvent 'ETL - dbo.MVIEWS_Drop', 'B'

--Drop the view if the FactInvoiceLine table is empty or if the view already exists
IF --(Select count(*) from FactInvoiceLine) = 0
	EXISTS (SELECT 1 FROM SysObjects WHERE name = 'FactInvoiceLine_Summary1' AND xtype = 'V')
BEGIN	
	DROP VIEW dbo.FactInvoiceLine_Summary1
END
--go
IF --(Select count(*) from FactInvoiceLine) = 0
	EXISTS (SELECT 1 FROM SysObjects WHERE name = 'FactInvoiceLine_Summary2' AND xtype = 'V')
BEGIN	
	DROP VIEW dbo.FactInvoiceLine_Summary2
END
--go
IF --(Select count(*) from FactInvoiceLine) = 0
	EXISTS (SELECT 1 FROM SysObjects WHERE name = 'FactInvoiceLine_Summary3' AND xtype = 'V')
BEGIN	
	DROP VIEW dbo.FactInvoiceLine_Summary3
END


--go

--Drop the view if the FactInvoiceLine table is empty or if the view already exists
-- If --(Select count(*) from FactInvoiceLine) = 0
-- 	Exists (select 1 from SysObjects where name = 'FactInvoiceLine_Summary_Item1' and xtype = 'V')
-- Begin	
-- 	Drop VIEW dbo.FactInvoiceLine_Summary_Item1
-- end
--go
--Drop the view if the FactInvoiceLine table is empty or if the view already exists
IF --(Select count(*) from FactInvoiceLine) = 0
	EXISTS (SELECT 1 FROM SysObjects WHERE name = 'FactInvoiceLine_Summary_AccountGroupAll1' AND xtype = 'V')
BEGIN	
	DROP VIEW dbo.FactInvoiceLine_Summary_AccountGroupAll1
END
--go
--Drop the view if the FactInvoiceLine table is empty or if the view already exists
IF --(Select count(*) from FactInvoiceLine) = 0
	EXISTS (SELECT 1 FROM SysObjects WHERE name = 'FactInvoiceLine_Summary_ItemGroupAll1' AND xtype = 'V')
BEGIN	
	DROP VIEW dbo.FactInvoiceLine_Summary_ItemGroupAll1
END

--go
--Drop the view if the FactInvoiceLine table is empty or if the view already exists
IF --(Select count(*) from FactInvoiceLine) = 0
	EXISTS (SELECT 1 FROM SysObjects WHERE name = 'FactInvoiceLine_Summary_InvoiceLineGroupAll1' AND xtype = 'V')
BEGIN	
	DROP VIEW dbo.FactInvoiceLine_Summary_InvoiceLineGroupAll1
END
--go
--Drop the view if the FactInvoiceLine table is empty or if the view already exists
IF --(Select count(*) from FactInvoiceLine) = 0
	EXISTS (SELECT 1 FROM SysObjects WHERE name = 'FactInvoiceLine_Summary_Item1' AND xtype = 'V')
BEGIN	
	DROP VIEW dbo.FactInvoiceLine_Summary_Item1
END

--go
--Drop the view if the FactInvoiceLine table is empty or if the view already exists
IF --(Select count(*) from FactInvoiceLine) = 0
	EXISTS (SELECT 1 FROM SysObjects WHERE name = 'FactInvoiceLine_Summary_Account1' AND xtype = 'V')
BEGIN	
	DROP VIEW dbo.FactInvoiceLine_Summary_Account1
END
--go
--Drop the view if the FactInvoiceLine table is empty or if the view already exists
IF --(Select count(*) from FactInvoiceLine) = 0
	EXISTS (SELECT 1 FROM SysObjects WHERE name = 'FactInvoiceLine_Summary_Account2' AND xtype = 'V')
BEGIN	
	DROP VIEW dbo.FactInvoiceLine_Summary_Account2
END
--go
--Drop the view if the FactInvoiceLine table is empty or if the view already exists
IF --(Select count(*) from FactInvoiceLine) = 0
	EXISTS (SELECT 1 FROM SysObjects WHERE name = 'FactInvoiceLine_Summary_Account3' AND xtype = 'V')
BEGIN	
	DROP VIEW dbo.FactInvoiceLine_Summary_Account3
END
--go
--Drop the view if the FactInvoiceLine table is empty or if the view already exists
IF --(Select count(*) from FactInvoiceLine) = 0
	EXISTS (SELECT 1 FROM SysObjects WHERE name = 'FactInvoiceLine_Summary_Account4' AND xtype = 'V')
BEGIN	
	DROP VIEW dbo.FactInvoiceLine_Summary_Account4

END
--go

--Drop the view if the FactInvoiceLine table is empty or if the view already exists
IF --(Select count(*) from FactInvoiceLine) = 0
	EXISTS (SELECT 1 FROM SysObjects WHERE name = 'FactInvoiceLine_Summary_AccountManager1' AND xtype = 'V')
BEGIN	
	DROP VIEW dbo.FactInvoiceLine_Summary_AccountManager1
END


--Drop the view if the FactInvoiceLine table is empty or if the view already exists
IF --(Select count(*) from FactInvoiceLine) = 0
	EXISTS (SELECT 1 FROM SysObjects WHERE name = 'FactInvoiceLine_Summary_AccountCount' AND xtype = 'V')
BEGIN	
	DROP VIEW dbo.FactInvoiceLine_Summary_AccountCount
END


--Drop the view if the FactInvoiceLine table is empty or if the view already exists
IF --(Select count(*) from FactInvoiceLine) = 0
	EXISTS (SELECT 1 FROM SysObjects WHERE name = 'FactInvoiceLine_Summary_ItemCount' AND xtype = 'V')
BEGIN	
	DROP VIEW dbo.FactInvoiceLine_Summary_ItemCount
END

-----------------------------------------------------------------------------------------------------------

-----------------------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------------

-- WITHOUT Last12MonthsIndicator!!

-----------------------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------------

--Drop the view if the FactInvoiceLine table is empty or if the view already exists
IF --(Select count(*) from FactInvoiceLine) = 0
	EXISTS (SELECT 1 FROM SysObjects WHERE name = 'FactInvoiceLine_SummaryLast12Months1' AND xtype = 'V')
BEGIN	
	DROP VIEW dbo.FactInvoiceLine_SummaryLast12Months1
END


--Drop the view if the FactInvoiceLine table is empty or if the view already exists
IF --(Select count(*) from FactInvoiceLine) = 0
	EXISTS (SELECT 1 FROM SysObjects WHERE name = 'FactInvoiceLine_SummaryLast12Months2' AND xtype = 'V')
BEGIN	
	DROP VIEW dbo.FactInvoiceLine_SummaryLast12Months2
END


--Drop the view if the FactInvoiceLine table is empty or if the view already exists
IF --(Select count(*) from FactInvoiceLine) = 0
	EXISTS (SELECT 1 FROM SysObjects WHERE name = 'FactInvoiceLine_SummaryLast12Months3' AND xtype = 'V')
BEGIN	
	DROP VIEW dbo.FactInvoiceLine_SummaryLast12Months3
END


--Drop the view if the FactInvoiceLine table is empty or if the view already exists
IF --(Select count(*) from FactInvoiceLine) = 0
	EXISTS (SELECT 1 FROM SysObjects WHERE name = 'FactInvoiceLine_SummaryLast12Months_AccountGroupAll1' AND xtype = 'V')
BEGIN	
	DROP VIEW dbo.FactInvoiceLine_SummaryLast12Months_AccountGroupAll1
END


--Drop the view if the FactInvoiceLine table is empty or if the view already exists
IF --(Select count(*) from FactInvoiceLine) = 0
	EXISTS (SELECT 1 FROM SysObjects WHERE name = 'FactInvoiceLine_SummaryLast12Months_ItemGroupAll1' AND xtype = 'V')
BEGIN	
	DROP VIEW dbo.FactInvoiceLine_SummaryLast12Months_ItemGroupAll1
END


--Drop the view if the FactInvoiceLine table is empty or if the view already exists
IF --(Select count(*) from FactInvoiceLine) = 0
	EXISTS (SELECT 1 FROM SysObjects WHERE name = 'FactInvoiceLine_SummaryLast12Months_InvoiceLineGroupAll1' AND xtype = 'V')
BEGIN	
	DROP VIEW dbo.FactInvoiceLine_SummaryLast12Months_InvoiceLineGroupAll1
END


--Drop the view if the FactInvoiceLine table is empty or if the view already exists
IF --(Select count(*) from FactInvoiceLine) = 0
	EXISTS (SELECT 1 FROM SysObjects WHERE name = 'FactInvoiceLine_SummaryLast12Months_Item1' AND xtype = 'V')
BEGIN	
	DROP VIEW dbo.FactInvoiceLine_SummaryLast12Months_Item1
END


--Drop the view if the FactInvoiceLine table is empty or if the view already exists
IF --(Select count(*) from FactInvoiceLine) = 0
	EXISTS (SELECT 1 FROM SysObjects WHERE name = 'FactInvoiceLine_SummaryLast12Months_Account1' AND xtype = 'V')
BEGIN	
	DROP VIEW dbo.FactInvoiceLine_SummaryLast12Months_Account1
END


--Drop the view if the FactInvoiceLine table is empty or if the view already exists
IF --(Select count(*) from FactInvoiceLine) = 0
	EXISTS (SELECT 1 FROM SysObjects WHERE name = 'FactInvoiceLine_SummaryLast12Months_Account2' AND xtype = 'V')
BEGIN	
	DROP VIEW dbo.FactInvoiceLine_SummaryLast12Months_Account2
END


--Drop the view if the FactInvoiceLine table is empty or if the view already exists
IF --(Select count(*) from FactInvoiceLine) = 0
	EXISTS (SELECT 1 FROM SysObjects WHERE name = 'FactInvoiceLine_SummaryLast12Months_Account3' AND xtype = 'V')
BEGIN	
	DROP VIEW dbo.FactInvoiceLine_SummaryLast12Months_Account3
END


--Drop the view if the FactInvoiceLine table is empty or if the view already exists
IF --(Select count(*) from FactInvoiceLine) = 0
	EXISTS (SELECT 1 FROM SysObjects WHERE name = 'FactInvoiceLine_SummaryLast12Months_AccountManager1' AND xtype = 'V')
BEGIN	
	DROP VIEW dbo.FactInvoiceLine_SummaryLast12Months_AccountManager1
END


--Drop the view if the FactInvoiceLine table is empty or if the view already exists
IF --(Select count(*) from FactInvoiceLine) = 0
	EXISTS (SELECT 1 FROM SysObjects WHERE name = 'FactInvoiceLine_SummaryLast12Months_AccountCount' AND xtype = 'V')
BEGIN	
	DROP VIEW dbo.FactInvoiceLine_SummaryLast12Months_AccountCount
END


--Drop the view if the FactInvoiceLine table is empty or if the view already exists
IF --(Select count(*) from FactInvoiceLine) = 0
	EXISTS (SELECT 1 FROM SysObjects WHERE name = 'FactInvoiceLine_SummaryLast12Months_ItemCount' AND xtype = 'V')
BEGIN	
	DROP VIEW dbo.FactInvoiceLine_SummaryLast12Months_ItemCount
END


--Drop the view if the FactInvoiceLine table is empty or if the view already exists
IF --(Select count(*) from FactInvoiceLine) = 0
	EXISTS (SELECT 1 FROM SysObjects WHERE name = 'FactInvoiceLine_Vendor3' AND xtype = 'V')
BEGIN	
	DROP VIEW dbo.FactInvoiceLine_Vendor3
END


--Drop the view if the FactInvoiceLine table is empty or if the view already exists
IF --(Select count(*) from FactInvoiceLine) = 0
	EXISTS (SELECT 1 FROM SysObjects WHERE name = 'FactInvoiceLine_Vendor2' AND xtype = 'V')
BEGIN	
	DROP VIEW dbo.FactInvoiceLine_Vendor2
END

--Drop the view if the FactInvoiceLine table is empty or if the view already exists
IF --(Select count(*) from FactInvoiceLine) = 0
	EXISTS (SELECT 1 FROM SysObjects WHERE name = 'FactInvoiceLine_Vendor1' AND xtype = 'V')
BEGIN	
	DROP VIEW dbo.FactInvoiceLine_Vendor1
END


--Drop the view if the FactInvoiceLine table is empty or if the view already exists
IF --(Select count(*) from FactInvoiceLine) = 0
	EXISTS (SELECT 1 FROM SysObjects WHERE name = 'FactInvoiceLine_Vendor4' AND xtype = 'V')
BEGIN	
	DROP VIEW dbo.FactInvoiceLine_Vendor4
END


--go
EXEC LogDCPEvent 'ETL - dbo.MVIEWS_Drop', 'E'

























GO
