SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [cm].[Load_FactInvoiceLine]

AS

INSERT INTO [cm].[FactInvoiceLine]
           ([ClientInvoiceLineUniqueID]
		   ,[DelNo]
           ,[DeliveryDate]
           ,[TransNo]
		   ,[TransactionDate]
           ,[SLedgerNo]
		   ,[SLedgerDate]
		   ,[BranchCode]
		   ,[BranchDescription]
		   ,[BranchBrand]
           ,[GpAllocBranchCode]
           ,[PriceOrderType]
           ,[TransType]
           ,[PriceDerivation]
           ,[LineNumber]
           ,[ChargeNoteID]
           ,[CustCntNo]
           ,[AccountNumber]
		   ,[AccountName]
           ,[CustomerOrderReference]
           ,[PyramidCode]
           ,[ProductCode]
		   ,[ProductDescription]
		   ,[VendorItemNumber]
           ,[LLSPGCode]
		   ,[LLSPGDescription]
           ,[VendorNumber]
           ,[KnownValueItem]
           ,[VariablePackInd]
           ,[SupplyType]
           ,[Qty]
           ,[UnitPrice]
           ,[NettPrice]
           ,[TradingPrice]
           ,[SupplierNettCost]
		   ,[TotalActualCost]
		   ,[ODSBatchKey]
           ,[ItemKey]
		   ,[AccountKey]
		   ,[CTSReleaseCode]
		   ,[CTSReleaseUser]
		   ,[CTSReleaseDate]
		   ,[TransactionSource]
		   ,[DeliveryAddressLine1]
		   ,[DeliveryAddressLine2]
		   ,[DeliveryAddressLine3]
		   ,[DeliveryAddressLine4]
		   ,[DeliveryAddressLine5]
		   ,[DeliveryPostCode]
		   ,[TradingMargin]
		   ,[FinancialMargin]
		   ,[CreditReasonCode]
		   )
    
	SELECT DISTINCT 
			CAST(st.[TransactionID] AS INT)
           ,CAST(st.[DelNo] AS VARCHAR(1))
           ,st.[DeliveryDate]
           ,st.[TransNo]
		   ,st.TransactionDate
		   --,CAST(st.SLedgerNo AS INT)
		   ,st.SLedgerNo
		   ,st.SLedgerDate
		   ,CAST(st.[BranchCode] AS VARCHAR(50))
		   ,st.BranchDescription
		   ,st.BranchBrand
		   ,st.[GpAllocBranchCode]
           ,st.[PriceOrderType]
           ,st.[TransactionType]
           ,CAST(st.[PriceDerivation] AS VARCHAR(50))
           ,CAST(st.[LineNumber] AS INT)
           ,st.[ChargeNoteID]
           ,st.[CustCntNo]
           ,st.[AccountID]
		   ,st.AccountName
           ,st.[CustomerOrderReference]
           ,CAST(st.[PyramidCode] AS VARCHAR(1))
           ,CAST(st.[ProductCode] AS VARCHAR(25))
		   ,st.ProductDescription
		   ,st.SupplierProductCode 
           ,CAST(st.[LLSPG] AS VARCHAR(50))
		   ,dig3.IG3Level1UDVarchar1  --LLSPGDescription
           ,st.[Supplier]
           ,CAST(st.[KnownValueItem] AS VARCHAR(1))
           ,st.[VariablePackInd]
           ,st.[SupplyType]
           ,CAST(st.[Qty] AS DECIMAL(18,9))
           ,st.[UnitPrice]
           ,st.NettPrice
           ,st.[TradingPrice]
           ,st.[SupplierNettCost]
		   ,st.[TotalActualCost]
           ,CAST(st.[ODSBatchKey] AS INT)
           ,di.ItemKey
		   ,da.AccountKey
		   ,st.[CTSReleaseCode]
		   ,st.[CTSReleaseUser]
		   ,st.[CTSReleaseDate]
		   ,st.TransactionSource
		   ,st.[DeliveryAddressLine1]
		   ,st.[DeliveryAddressLine2]
		   ,st.[DeliveryAddressLine3]
		   ,st.[DeliveryAddressLine4]
		   ,st.[DeliveryAddressLine5]
		   ,st.[DeliveryPostCode]
		   ,st.[TradingMargin]
		   ,st.[FinancialMargin]
		   ,st.[CreditReasonCode]

	FROM cm.StagingTransaction st (NOLOCK)
	--LEFT JOIN DimDay dd (NOLOCK)
	--	ON dd.SQLDate = st.TransactionDate
	LEFT JOIN DimItem di (NOLOCK)
		ON Convert(varchar(2), st.PyramidCode) + st.ProductCode = di.ItemNumber
	LEFT JOIN DimAccount da (NOLOCK)
		ON da.AccountNumber = st.AccountID
	LEFT JOIN dbo.DimItemGroup3 dig3 (NOLOCK)
		ON dig3.ItemGroup3Key = di.ItemGroup3Key
	LEFT JOIN cm.FactInvoiceLine fil (NOLOCK)
		ON fil.ClientInvoiceLineUniqueID = st.TransactionID
	WHERE fil.ClientInvoiceLineUniqueID IS NULL

--Log Import of Fact Invoice Line
INSERT INTO cm.FactInvoiceLineLog
	(
	[WebUserKey],
	[ClientInvoiceLineUniqueID],
	[CreationDate],
	[ClaimAction]
	)
SELECT 
	1,
	fil.ClientInvoiceLineUniqueID,
	GETDATE(),
	'Import Transaction'
FROM cm.FactInvoiceLine fil (NOLOCK)
INNER JOIN cm.StagingTransaction st (NOLOCK)
	ON fil.ClientInvoiceLineUniqueID = st.TransactionID

--Send email notification
DECLARE @Message varchar(250)
DECLARE @RecordCount int
DECLARE @BatchId int

SET @RecordCount = (SELECT COUNT(fil.ClientInvoiceLineUniqueID) 
					FROM cm.FactInvoiceLine fil (NOLOCK)
					INNER JOIN cm.StagingTransaction st (NOLOCK)
					ON fil.ClientInvoiceLineUniqueID = st.TransactionID)
SET @BatchId = (SELECT TOP 1 fil.ODSBatchKey
					FROM cm.FactInvoiceLine fil (NOLOCK)
					INNER JOIN cm.StagingTransaction st (NOLOCK)
					ON fil.ClientInvoiceLineUniqueID = st.TransactionID)
SET @Message = CAST(@BatchId AS VARCHAR(20)) + ' Transaction Batch; ' + CAST(@RecordCount AS VARCHAR(20)) + ' # of Records Loaded'

EXEC msdb.dbo.sp_send_dbmail  
@recipients = 'melanie.zaremba@enterbridge.com; scott.toolson@enterbridge.com; devin.huch@enterbridge.com; kevin.kennedy@enterbridge.com',
@subject = 'Transaction Data Loaded',
@body = @Message, 
@body_format = 'HTML'
GO
