SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



/*
DECLARE @ProcName [sysname]; SELECT @ProcName = NEWID(); -- SELECT @ProcName [@ProcName];
EXEC [dbo].[LogDCPEvent] @NewStepDesc = @ProcName, @Cmd = 'B'
*/
CREATE PROCEDURE [dbo].[LogDCPEvent] (
      @NewStepDesc Description_Normal_Type
    , @Cmd Description_Normal_Type
    , @RowsI Quantity_Normal_type = 0
    , @RowsU Quantity_Normal_type = 0
    , @RowsD Quantity_Normal_type = 0
    , @ProjectKey Key_Normal_Type = NULL
)
AS

/*
	EXEC LogDCPEventNew 'This is a test', 'E', 0, 0, 0
	EXEC DCPLOG
*/

SET NOCOUNT ON

BEGIN
	IF @Cmd = 'B' -- If we are beginning a step 
	BEGIN
		DELETE FROM dbo.LogDCP WHERE StepDesc = @NewStepDesc AND EndTime IS NULL	-- delete any old entries which were never completed
		INSERT INTO dbo.LogDCP (ProjectKey, StepDesc, BeginTime, RowsInserted, RowsUpdated, RowsDeleted) VALUES (@ProjectKey, @NewStepDesc, GETDATE(), @RowsI, @RowsU, @RowsD)	-- insert a new one
	END
	ELSE	-- if we are ending a step
	BEGIN
	
		UPDATE dbo.LogDCP SET EndTime = GETDATE(), RowsInserted = @RowsI, RowsUpdated = @RowsU, RowsDeleted = @RowsD WHERE StepDesc = @NewStepDesc AND EndTime IS NULL AND (ProjectKey = @ProjectKey OR @ProjectKey IS NULL)
		IF @@ROWCOUNT = 0 	-- row count must be checked immediately after the UPDATE
		BEGIN
			-- insert if not there
			DECLARE @CurrentDateTime DATETIME
			SELECT @CurrentDateTime = GETDATE()
			DELETE FROM dbo.LogDCP WHERE StepDesc = @NewStepDesc AND EndTime IS NULL	-- delete any old entries which were never completed
			INSERT INTO dbo.LogDCP (ProjectKey, StepDesc, BeginTime, EndTime, RowsInserted, RowsUpdated, RowsDeleted) VALUES (@ProjectKey, @NewStepDesc, @CurrentDateTime, @CurrentDateTime, @RowsI, @RowsU, @RowsD)	-- insert a new one
		END

    END
END


BEGIN TRY

	DECLARE
		@EmailRecipients NVARCHAR(MAX),
  		@EmailSubject NVARCHAR (255), 
		@EmailBody NVARCHAR (MAX), 	
		@SendEmailNotificationIndicator NCHAR(1),
		@EmailProfileName NVARCHAR(MAX)

	SELECT @SendEmailNotificationIndicator = ParamValue FROM dbo.Param WHERE ParamName = 'SendEmailNotificationIndicator'
	SELECT @EmailRecipients = ParamValue FROM dbo.Param WHERE ParamName = 'EmailRecipients'
	SELECT @EmailProfileName = ParamValue FROM dbo.Param WHERE ParamName = 'EmailProfileName'
	
	IF	ISNULL(@SendEmailNotificationIndicator, 'N') = 'Y' AND
		@EmailRecipients IS NOT NULL
	BEGIN

		SET @EmailSubject = N'DCP Event Logged (' + ISNULL(@@SERVERNAME, '') + '.' + ISNULL(DB_NAME(), '') + ')'
	    
		SET @EmailBody = '@NewStepDesc:' + CHAR(13) + CONVERT([NVARCHAR] (MAX), @NewStepDesc) + REPLICATE(CHAR(13), 2)
				  + '@Cmd:' + CHAR(13) + CONVERT([NVARCHAR] (MAX), @Cmd) + REPLICATE(CHAR(13), 2)
				  + ISNULL('@ProjectKey:' + CHAR(13) + CONVERT([NVARCHAR] (32), @ProjectKey) + REPLICATE(CHAR(13), 2), '')

		EXEC [msdb].[dbo].[sp_send_dbmail]
			@profile_name = @EmailProfileName
			, @recipients = @EmailRecipients
			, @subject    = @EmailSubject
			, @body	      = @EmailBody
			, @exclude_query_output = 1

	END

END TRY
BEGIN CATCH
    PRINT 'Error encountered while trying to send e-mail.'
END CATCH

GO
