SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [cg].[GetCGPriceEnvelope]
	@ParamPyramidCode NVARCHAR(2) = N'1',
	@ParamInstructionLevelType NVARCHAR(50),
	@ParamInstructionLevelCode Description_Small_type,
	@ParamAccountKey Key_Normal_type,
	@ParamVendorKey Key_Normal_type,
	@ParamJobFlag BIT,
	@ParamSales603010Bucket NVARCHAR(50) = N'Large'
WITH EXECUTE AS CALLER
AS

/*

EXEC cg.GetCGPriceEnvelope N'1', N'Item', N'1124688', 309836, 3514, 1, N'Sales Team'

EXEC cg.GetCGPriceEnvelope N'1', N'Item', N'2124688', 309836, 3514, 1, N'Sales Team'

*/

SET NOCOUNT ON



SELECT
	PlaybookPricingGroupKey,
	ReturnCode,
	NEWID() AS GUID,
	RedClaimBackPercent,
	AmberClaimBackPercent,
	GreenClaimBackPercent,
	RedClaimBack,
	AmberClaimBack,
	GreenClaimBack			
FROM cg.fn_GetCGPriceEnvelope ( 
	@ParamPyramidCode,
	@ParamInstructionLevelType,
	@ParamInstructionLevelCode,
	@ParamAccountKey,
	@ParamVendorKey,
	@ParamJobFlag,
	@ParamSales603010Bucket,
	N'Y'									-- Debug flag; set to N for no debugging
	)



GO
