SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [cg].[PopulatePlaybookDataPoint]
	@PlaybookDataPointGroupKey INT
AS

SET QUOTED_IDENTIFIER ON 
SET ANSI_NULLS ON 
SET ARITHABORT ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
SET CONCAT_NULL_YIELDS_NULL ON
SET NUMERIC_ROUNDABORT OFF

DECLARE 
	@RowsI INT, 
	@RowsU INT, 
	@RowsD INT,
	@ProjectKey INT,
	@SQLString NVARCHAR(MAX)


SET @RowsI = 0
SET @RowsU = 0
SET @RowsD = 0


SELECT @ProjectKey = ProjectKey FROM PlaybookDataPointGroup WHERE PlaybookDataPointGroupKey = @PlaybookDataPointGroupKey


EXEC LogDCPEvent 'DCP --- cg.PopulatePlaybookDataPoint: Delete', 'B',0,0,0,@ProjectKey

DELETE a
FROM cg.PlaybookDataPointPricingGroup a
JOIN dbo.PlaybookPricingGroup B 
		ON a.PlaybookPricingGroupKey = B.PlaybookPricingGroupKey
        WHERE   PlaybookDataPointGroupKey = @PlaybookDataPointGroupKey
SET @RowsD = @RowsD + @@RowCount

DELETE FROM cg.PlaybookDataPoint WHERE PlaybookDataPointGroupKey = @PlaybookDataPointGroupKey
SET @RowsD = @RowsD + @@RowCount

EXEC LogDCPEvent 'DCP --- cg.PopulatePlaybookDataPoint: Delete', 'E',@RowsI, @RowsU, @RowsD,@ProjectKey


EXEC LogDCPEvent 'DCP --- cg.PopulatePlaybookDataPoint: Insert cg.PlaybookDataPoint', 'B',0,0,0,@ProjectKey

SELECT DISTINCT
	AccountKey,
	UDVarchar4 AS Sales603010Bucket
INTO #AccountBucket
FROM dbo.FactLastPriceAndCost flpac

CREATE UNIQUE CLUSTERED INDEX I_1 ON #AccountBucket (AccountKey)



EXEC LogDCPEvent 'DCP --- cg.PopulatePlaybookDataPoint: Insert PlaybookDataPoint - Item level', 'B',0,0,0,@ProjectKey

SET @RowsI = 0
SET @RowsU = 0
SET @RowsD = 0

; WITH ContractData AS (
SELECT DISTINCT
	dc.ContractKey,
	ISNULL(da.AccountKey, 1) AS AccountKey,
	'Item' AS InstructionLevelType,
	dig3.IG3Level1 AS LLSPGCode,
	dig1.IG1Level1 AS MPGCode,
	di.ItemNumber,
	di.ItemNumber AS InstructionLevelCode,
	CASE
		WHEN ClaimBackAmount IS NOT NULL THEN 1. - ( (CurrentInvoiceCost - ClaimBackAmount) / NULLIF(CurrentInvoiceCost, 0) )
		ELSE ClaimBackPerc
	END AS ClaimBackPercentCalc,
	ISNULL(ab.Sales603010Bucket, abParent.Sales603010Bucket) AS Sales603010Bucket
FROM cc.SupplierContract dc
INNER JOIN dbo.DimVendor dv 
	ON dv.VendorKey = dc.VendorKey
INNER JOIN cc.SupplierContractItem dci
	ON dci.ContractKey = dc.ContractKey
	AND (dci.StartDT <= GETDATE() OR dci.StartDT IS NULL)
	AND (dci.ExpiryDT > GETDATE() OR dci.ExpiryDT IS NULL)
INNER JOIN dbo.DimItem di 
	ON di.ItemKey = dci.ItemKey
	AND di.InActive = 0
INNER JOIN dbo.DimItemGroup1 dig1
	ON dig1.ItemGroup1Key = di.ItemGroup1Key
INNER JOIN dbo.DimItemGroup3 dig3
	ON dig3.ItemGroup3Key = di.ItemGroup3Key
LEFT JOIN cc.SupplierContractAccount dca
	ON dca.ContractKey = dc.ContractKey
	AND (dca.StartDate <= GETDATE() OR dca.StartDate IS NULL)
	AND (dca.ExpirationDate > GETDATE() OR dca.ExpirationDate IS NULL)
LEFT JOIN dbo.DimAccount da
	ON da.AccountKey = dca.AccountKey
	AND da.InActive = 0
LEFT JOIN dbo.DimAccountGroup2 dag2
	ON dag2.AccountGroup2Key = da.AccountGroup2Key
LEFT JOIN dbo.DimAccount daParent
	ON daParent.AccountNumber = da.AccountNumberParent
LEFT JOIN #AccountBucket ab
	ON ab.AccountKey = da.AccountKey
LEFT JOIN #AccountBucket abParent
	ON abParent.AccountKey = daParent.AccountKey

WHERE
	(ClaimBackAmount IS NOT NULL OR ClaimBackPerc IS NOT NULL)
	--AND GETDATE() BETWEEN dc.RecordStartDT AND dc.RecordEndDT 
	AND dc.[ExpirationDate] >= CAST('2015-09-01' AS DATE)
	-- AND dc.ContractSourceKey = 1		LPF-5292 Remove Contract Source exclusion rule from Contract Guidance
	AND NOT ( ISNULL(dc.IncludeAllAccounts, 0) = 1 AND da.AccountKey IS NOT NULL)
	AND ( ISNULL(dc.ExcludeCashAccounts, 0) = 0 OR (ISNULL(dc.ExcludeCashAccounts, 0) = 1 AND AG2Level1 <> N'CS') )
)
INSERT INTO cg.PlaybookDataPoint
(
	PlaybookDataPointGroupKey,
	ContractKey,
	AccountKey,
	InstructionLevelType,
	LLSPGCode,
	MPGCode,
	ItemNumber,
	InstructionLevelCode,
	ClaimBackPercent,
	Sales603010Bucket
)
SELECT DISTINCT
	@PlaybookDataPointGroupKey,
	ContractKey,
	AccountKey,
	InstructionLevelType,
	LLSPGCode,
	MPGCode,
	ItemNumber,
	InstructionLevelCode,
	ClaimBackPercentCalc,
	Sales603010Bucket
FROM ContractData
WHERE
	ClaimBackPercentCalc >= 0.00
	AND ClaimBackPercentCalc <= 0.99
SET @RowsI = @RowsI + @@RowCount

EXEC LogDCPEvent 'DCP --- cg.PopulatePlaybookDataPoint: Insert PlaybookDataPoint - Item level', 'E', @RowsI, @RowsU, @RowsD, @ProjectKey


EXEC LogDCPEvent 'DCP --- cg.PopulatePlaybookDataPoint: Insert PlaybookDataPoint - LLSPG level', 'B',0,0,0,@ProjectKey

SET @RowsI = 0
SET @RowsU = 0
SET @RowsD = 0

; WITH ContractData AS (
SELECT DISTINCT
	dc.ContractKey,
	ISNULL(da.AccountKey, 1) AS AccountKey,
	'LLSPG' AS InstructionLevelType,
	dig3.IG3Level1 AS LLSPGCode,
	'n/a' AS MPGCode,
	'n/a' AS ItemNumber,
	dig3.IG3Level1 AS InstructionLevelCode,
	x.ClaimBackPerc,
	ISNULL(ab.Sales603010Bucket, abParent.Sales603010Bucket) AS Sales603010Bucket
FROM cc.SupplierContract dc
INNER JOIN dbo.DimVendor dv 
	ON dv.VendorKey = dc.VendorKey
INNER JOIN cc.SupplierContractItemGroupX x
	ON x.ContractKey = dc.ContractKey
	AND (x.StartDT <= GETDATE() OR x.StartDT IS NULL)
	AND (x.ExpiryDT > GETDATE() OR x.ExpiryDT IS NULL)
	AND x.ItemGroupLevel = 3
INNER JOIN dbo.DimItemGroup3 dig3
	ON dig3.ItemGroup3Key = x.ItemGroupXKey
LEFT JOIN cc.SupplierContractAccount dca
	ON dca.ContractKey = dc.ContractKey
	AND (dca.StartDate <= GETDATE() OR dca.StartDate IS NULL)
	AND (dca.ExpirationDate > GETDATE() OR dca.ExpirationDate IS NULL)
LEFT JOIN dbo.DimAccount da
	ON da.AccountKey = dca.AccountKey
	AND da.InActive = 0
LEFT JOIN dbo.DimAccountGroup2 dag2
	ON dag2.AccountGroup2Key = da.AccountGroup2Key
LEFT JOIN dbo.DimAccount daParent
	ON daParent.AccountNumber = da.AccountNumberParent
LEFT JOIN #AccountBucket ab
	ON ab.AccountKey = da.AccountKey
LEFT JOIN #AccountBucket abParent
	ON abParent.AccountKey = daParent.AccountKey

WHERE
	(ClaimBackAmount IS NOT NULL OR ClaimBackPerc IS NOT NULL)
	--AND GETDATE() BETWEEN dc.RecordStartDT AND dc.RecordEndDT 
	AND dc.[ExpirationDate] >= CAST('2015-09-01' AS DATE)
	-- AND dc.ContractSourceKey = 1		LPF-5292 Remove Contract Source exclusion rule from Contract Guidance
	AND NOT ( ISNULL(dc.IncludeAllAccounts, 0) = 1 AND da.AccountKey IS NOT NULL)
	AND ( ISNULL(dc.ExcludeCashAccounts, 0) = 0 OR (ISNULL(dc.ExcludeCashAccounts, 0) = 1 AND AG2Level1 <> N'CS') )
)
INSERT INTO cg.PlaybookDataPoint
(
	PlaybookDataPointGroupKey,
	ContractKey,
	AccountKey,
	InstructionLevelType,
	LLSPGCode,
	MPGCode,
	ItemNumber,
	InstructionLevelCode,
	ClaimBackPercent,
	Sales603010Bucket
)
SELECT DISTINCT
	@PlaybookDataPointGroupKey,
	ContractKey,
	AccountKey,
	InstructionLevelType,
	LLSPGCode,
	MPGCode,
	ItemNumber,
	InstructionLevelCode,
	ClaimBackPerc,
	Sales603010Bucket
FROM ContractData
WHERE
	ClaimBackPerc >= 0.00
	AND ClaimBackPerc <= 0.99
SET @RowsI = @RowsI + @@RowCount

EXEC LogDCPEvent 'DCP --- cg.PopulatePlaybookDataPoint: Insert PlaybookDataPoint - LLSPG level', 'E', @RowsI, @RowsU, @RowsD, @ProjectKey










SET @SQLString = 'UPDATE STATISTICS cg.PlaybookDataPoint'
EXEC SP_EXECUTESQL @SQLString

DROP TABLE #AccountBucket


SELECT TOP 10 * FROM cg.PlaybookDataPoint


EXEC LogDCPEvent 'DCP --- cg.PopulatePlaybookDataPoint: Insert cg.PlaybookDataPoint', 'E', 0, 0, 0, @ProjectKey

















GO
