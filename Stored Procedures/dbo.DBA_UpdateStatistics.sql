SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO







CREATE PROCEDURE [dbo].[DBA_UpdateStatistics]
AS
--Author: Tom Werz
--Date: 03/29/2005
--Purpose:  This SP will manually update statistics on all tables in a database.
--Use: This should be run at the end of every DCP and also could be scheduled to run weekly or nightly.

EXEC LogDCPEvent 'dbo.DBA_UpdateStatistics', 'B'


DECLARE @table NVARCHAR(500), @cmd NVARCHAR(4000)
DECLARE a CURSOR 
FOR 

	SELECT DISTINCT
		  '[' + SCHEMA_NAME (o.schema_id) + '].[' + O.name + ']'
	FROM sys.objects o
	WHERE 
		  o.type_desc = 'USER_TABLE'
	UNION
	SELECT DISTINCT
		  '[' + SCHEMA_NAME (o.schema_id) + '].[' + O.name + ']'
	FROM sys.objects o
	JOIN sys.indexes i
		  ON o.object_id = i.object_id
	WHERE 
		  o.type_desc = 'VIEW'
		  AND i.type_desc = 'CLUSTERED'
	ORDER BY 1

OPEN a
FETCH NEXT FROM a INTO @table
WHILE @@fetch_STatus = 0
BEGIN
	PRINT 'UPDATE STATISTICS ' + @table
	SET @cmd = 'UPDATE STATISTICS ' + @table
	EXEC sp_executesql @cmd
	FETCH NEXT FROM a INTO @table
END
CLOSE a
DEALLOCATE a


EXEC LogDCPEvent 'dbo.DBA_UpdateStatistics', 'E'







GO
