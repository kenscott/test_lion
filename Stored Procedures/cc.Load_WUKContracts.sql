SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [cc].[Load_WUKContracts] AS
BEGIN

	SET NOCOUNT ON

	EXEC cc.Load_SupplierContract
	EXEC cc.Load_SupplierContractItem
	EXEC cc.Load_SupplierContractItemGroupX
	EXEC cc.Load_SupplierContractAccount
	EXEC cc.Load_SupplierContractLocation
	EXEC cc.Load_SupplierContractNote

	
END
GO
