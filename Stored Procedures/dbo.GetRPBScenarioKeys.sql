SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO







CREATE PROCEDURE [dbo].[GetRPBScenarioKeys]
	@ScenarioKey Key_Normal_type OUTPUT,
	@PlaybookDataPointGroupKey Key_Normal_type OUTPUT,
	@ProjectKey Key_Normal_type OUTPUT
AS

/*

DECLARE @ScenarioKey INT, @PlaybookDataPointGroupKey INT, @ProjectKey INT
EXEC GetRPBScenarioKeys @ScenarioKey OUTPUT, @PlaybookDataPointGroupKey OUTPUT, @ProjectKey OUTPUT
SELECT @ScenarioKey, @PlaybookDataPointGroupKey, @ProjectKey


DECLARE @ScenarioKey2 INT, @PlaybookDataPointGroupKey2 INT, @ProjectKey2 INT
EXEC GetRPBScenarioKeys @ScenarioKey2 OUTPUT, @PlaybookDataPointGroupKey2 OUTPUT, @ProjectKey2 OUTPUT
SELECT @ScenarioKey2, @PlaybookDataPointGroupKey2, @ProjectKey2

*/

SET NOCOUNT ON


-- get @ScenarioKey and @PlaybookDataPointGroupKey keys
; WITH Keys AS (
	SELECT DISTINCT
		a.ScenarioKey,
		PlaybookDataPointGroupKey,
		pp.ProjectKey,
		ROW_NUMBER() OVER (ORDER BY a.ScenarioKey DESC, PlaybookDataPointGroupKey DESC, pp.ProjectKey) AS RowRank
	FROM dbo.ATKScenario A (NOLOCK)
	INNER JOIN dbo.ATKProjectSpecificationScenario C (NOLOCK)
		ON C.ScenarioKey = A.ScenarioKey
	INNER JOIN dbo.PlaybookProject pp (NOLOCK)
		ON pp.ProjectSpecificationKey = c.ProjectSpecificationKey
	INNER JOIN dbo.PlaybookDataPointGroup pdpg (NOLOCK)
		ON pdpg.ProjectSpecificationScenarioKey = c.ProjectSpecificationScenarioKey
		AND pdpg.ProjectKey = pp.ProjectKey
	--WHERE ScenarioName LIKE 'RPB Paper - I3%'
	WHERE 
		a.ApproachTypeKey = 7
		AND pp.ProductionProjectIndicator = 'Y'
		--AND A.UDVarchar1 = '1'
)
SELECT
	@ScenarioKey = ScenarioKey,
	@PlaybookDataPointGroupKey = PlaybookDataPointGroupKey,
	@ProjectKey = ProjectKey
FROM Keys
WHERE RowRank = 1









GO
