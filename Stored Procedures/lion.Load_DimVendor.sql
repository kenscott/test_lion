SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE [lion].[Load_DimVendor]
AS

EXEC LogDCPEvent 'ETL - Load_DimVendor', 'B'

--This table will contaIN a row for every row IN the dimension table that has been changed IN the staging table (type 1 update will be done)
CREATE TABLE #UpdateKeys (KeyID INTEGER)
CREATE TABLE #UpdateKeys2 (KeyID INTEGER)

DECLARE @RowsI Quantity_Normal_type, @RowsU Quantity_Normal_type, @RowsD Quantity_Normal_type
SET @RowsI = 0
SET @RowsU = 0
SET @RowsD = 0

DECLARE @TodayDayKey Key_Small_Type
SET @TodayDayKey = (SELECT DayKey FROM DimDay WHERE CONVERT(VARCHAR(10), SQLDate, 120) = CONVERT(VARCHAR(10), GETDATE(), 120))

--Exec DBA_IndexRebuild 'DimVendor', 'Drop'

--Truncate table DimVendor
--Select top 10 * from DimVendor   

IF NOT EXISTS (SELECT 1 FROM DimVendor WHERE VendorClientUniqueIdentifier = 'Unknown')
BEGIN
	INSERT DimVendor (
		VendorClientUniqueIdentifier,
		VendorDescription,
		VendorNumber,
		CreationDayKey,
		ModificationDayKey)
	VALUES (
		'Unknown',
		'Unknown',
		'Unknown', 
		@TodayDayKey,
		@TodayDayKey
	)
	SET @RowsI = @RowsI + @@RowCount
END

--Load Vendor data from the Product staging table
;
WITH StagingData AS (
	SELECT DISTINCT
		CAST(ISNULL(SupplierName , N'Unknown') AS NVARCHAR(50)) AS VendorName, 
		CAST(ISNULL(SupplierCode, N'Unknown') AS NVARCHAR(50)) AS VendorNumber, 
		CAST(N'Unknown' AS NVARCHAR(50)) AS ParentVendorNumber,
		 ROW_NUMBER() OVER (PARTITION BY COALESCE(SupplierCode, SupplierName, N'Unknown') ORDER BY COALESCE(SupplierCode, SupplierName, N'Unknown') ASC) AS RowRank
	FROM Lion_Staging.dbo.Product
)
SELECT
	VendorName,
	VendorNumber,
	ParentVendorNumber
INTO
	#staging
FROM StagingData
WHERE RowRank = 1
ORDER BY 2, 1


--Compare the checksum of the current dimension data to the checksum of the staging data
--IF we find any checksums that do not match, we will update those rows
INSERT INTO #UpdateKeys (KeyID)
SELECT DimVendor.VendorKey
FROM 
	#staging Staging
JOIN DimVendor ON DimVendor.VendorClientUniqueIdentifier = Staging.VendorNumber
WHERE 
	CHECKSUM(--CheckSum for dimension table.  This needs to check all rows that we will update IF a find a change
		DimVendor.VendorDescription, DimVendor.VendorNumber, DimVendor.VendorUDVarchar1)
<>
	CHECKSUM(--CheckSum for staging table
		Staging.VendorName, Staging.VendorNumber, ParentVendorNumber)


--This will perform the update ON all Vendors that changed. 
--All columns that we used IN the checksum above need the be updated
UPDATE DimVendor SET 
	DimVendor.VendorDescription=Staging.VendorName,
	DimVendor.VendorNumber=Staging.VendorNumber,
	DimVendor.VendorUDVarchar1=Staging.ParentVendorNumber
FROM #staging Staging
WHERE Staging.VendorNumber = DimVendor.VendorClientUniqueIdentifier
AND DimVendor.VendorKey IN (SELECT KeyID FROM #UpdateKeys)
SET @RowsU = @RowsU + @@RowCount


INSERT DimVendor (
	VendorClientUniqueIdentifier,
	VendorDescription,
	VendorNumber,
	VendorUDVarchar1,
	CreationDayKey,
	ModificationDayKey
	)
SELECT DISTINCT
	v.VendorNumber, 
	v.VendorName, 
	v.VendorNumber, 
	ParentVendorNumber,
	@TodayDayKey,
	@TodayDayKey 
FROM #staging V
LEFT JOIN DimVendor DV ON DV.VendorClientUniqueIdentifier = v.VendorNumber
WHERE DV.VendorClientUniqueIdentifier IS NULL
SET @RowsI = @RowsI + @@RowCount


--Load Vendor data from the Supplier staging table
;
WITH StagingData2 AS (
	SELECT DISTINCT
		CAST(ISNULL(s.SupplierName , N'Unknown') AS NVARCHAR(50)) AS VendorName, 
		CAST(ISNULL(s.SupplierCode, N'Unknown') AS NVARCHAR(50)) AS VendorNumber, 
		CAST(N'Unknown' AS NVARCHAR(50)) AS ParentVendorNumber,
		CAST(o.OracleCode AS NVARCHAR(50)) AS VendorOracleCode,
		CAST(s.Address1 AS NVARCHAR(250)) AS VendorAddress1,
		CAST(s.Address2 AS NVARCHAR(250)) AS VendorAddress2,
		CAST(s.Address3 AS NVARCHAR(250)) AS VendorAddress3,
		CAST(s.Address4 AS NVARCHAR(250)) AS VendorAddress4,
		CAST(s.Postcode AS NVARCHAR(50)) AS VendorZipCode,
		CAST(LEFT(s.ContractClaimsNarrative1, 50) + ' ' + LEFT(s.ContractClaimsNarrative2, 50) + ' ' + LEFT(s.ContractClaimsNarrative3, 50) + ' ' + LEFT(s.ContractClaimsNarrative4, 50) + ' ' + LEFT(s.ContractClaimsNarrative5, 50) AS NVARCHAR(250)) AS VendorComments,
		 ROW_NUMBER() OVER (PARTITION BY COALESCE(s.SupplierCode, s.SupplierName, N'Unknown') ORDER BY COALESCE(s.SupplierCode, s.SupplierName, N'Unknown') ASC) AS RowRank
	FROM Lion_Staging.dbo.Supplier s
	LEFT JOIN Lion_Staging.dbo.SupplierOraCode o
		ON o.SupplierCode = s.SupplierCode
)
SELECT
	VendorName,
	VendorNumber,
	ParentVendorNumber,
	VendorOracleCode,
	VendorAddress1,
	VendorAddress2,
	VendorAddress3,
	VendorAddress4,
	VendorZipCode,
	VendorComments
INTO
	#staging2
FROM StagingData2
WHERE RowRank = 1
ORDER BY 2, 1

INSERT INTO #UpdateKeys2 (KeyID)
SELECT DimVendor.VendorKey
FROM 
	#staging2 Staging2
JOIN DimVendor ON DimVendor.VendorClientUniqueIdentifier = Staging2.VendorNumber
WHERE 
	CHECKSUM(--CheckSum for dimension table.  This needs to check all rows that we will update IF a find a change
		DimVendor.VendorDescription, DimVendor.VendorNumber, DimVendor.VendorUDVarchar1, DimVendor.VendorOracleCode, DimVendor.VendorAddress1, DimVendor.VendorAddress2, 
		DimVendor.VendorAddress3, DimVendor.VendorAddress4, DimVendor.VendorZipCode, DimVendor.VendorComments)
<>
	CHECKSUM(--CheckSum for staging table
		Staging2.VendorName, Staging2.VendorNumber, Staging2.ParentVendorNumber, Staging2.VendorOracleCode, Staging2.VendorAddress1, Staging2.VendorAddress2, 
		Staging2.VendorAddress3, Staging2.VendorAddress4, Staging2.VendorZipCode, Staging2.VendorComments)

UPDATE DimVendor SET 
	DimVendor.VendorDescription = Staging2.VendorName,
	DimVendor.VendorNumber = Staging2.VendorNumber,
	DimVendor.VendorUDVarchar1 = Staging2.ParentVendorNumber,
	DimVendor.VendorOracleCode = Staging2.VendorOracleCode,
	DimVendor.VendorAddress1 = Staging2.VendorAddress1,
	DimVendor.VendorAddress2 = Staging2.VendorAddress2,
	DimVendor.VendorAddress3 = Staging2.VendorAddress3,
	DimVendor.VendorAddress4 = Staging2.VendorAddress4,
	DimVendor.VendorZipCode = Staging2.VendorZipCode,
	DimVendor.VendorComments = Staging2.VendorComments
FROM #staging2 Staging2
WHERE Staging2.VendorNumber = DimVendor.VendorClientUniqueIdentifier
AND DimVendor.VendorKey IN (SELECT KeyID FROM #UpdateKeys2)
SET @RowsU = @RowsU + @@RowCount


INSERT DimVendor (
	VendorClientUniqueIdentifier,
	VendorDescription,
	VendorNumber,
	VendorUDVarchar1,
	CreationDayKey,
	ModificationDayKey,
	VendorOracleCode,
	VendorAddress1,
	VendorAddress2,
	VendorAddress3,
	VendorAddress4,
	VendorZipCode,
	VendorComments,
	AllowAllLocations,
	AllowAllAccounts,
	AllowClaimPercent,
	AllowClaimAmount,
	AllowNetCost,
	AllowUnitLimits,
	AllowSalesLimits,
	AllowClaimsLimits,
	AllowClaimsOnPending,
	AllowClaimReturnCredits,
	PaymentMethod,
	HistoricalClaimsPeriod
	)
SELECT DISTINCT
	v.VendorNumber, 
	v.VendorName, 
	v.VendorNumber, 
	ParentVendorNumber,
	@TodayDayKey,
	@TodayDayKey,
	v.VendorOracleCode,
	v.VendorAddress1,
	v.VendorAddress2,
	v.VendorAddress3,
	v.VendorAddress4,
	v.VendorZipCode,
	v.VendorComments,
	'Optional',
	'Optional',
	1,
	1,
	1,
	1,
	1,
	1,
	1,
	1,
	'Credit Note',
	3
FROM #staging2 v
LEFT JOIN DimVendor DV ON DV.VendorClientUniqueIdentifier = v.VendorNumber
WHERE DV.VendorClientUniqueIdentifier IS NULL
SET @RowsI = @RowsI + @@RowCount

--Update Inactive Flag
UPDATE DimVendor 
SET Inactive =
	CASE
		WHEN s.SupplierCode IS NULL THEN 1
		WHEN CONVERT(DATE, s.DateForDeletion) < CONVERT(DATE, GETDATE()) THEN 1
		ELSE 0
	END
FROM DimVendor v
LEFT JOIN Lion_Staging.dbo.Supplier s
ON s.SupplierCode = v.VendorClientUniqueIdentifier


--Exec DBA_IndexRebuild 'DimVendor', 'Create'

SELECT TOP 10 * FROM DimVendor

DROP TABLE #staging
DROP TABLE #staging2

EXEC LogDCPEvent 'ETL - Load_DimVendor', 'E', @RowsI, @RowsU, @RowsD



GO
