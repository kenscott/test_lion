SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE [ppe].[Get_Delegates] (@WebUserKey INT)
AS 
BEGIN

--DECLARE @WebUserKey INT
--SELECT @WebUserKey = 12730

	SELECT wu.WebUserKey, wu.UserName, op.FullName
	FROM (
			SELECT SubsidiaryAccountManagerKey AS AccountManagerKey /*all subordinates */
			FROM dbo.BridgeAccountManager bam
			JOIN dbo.WebUser wu ON bam.ParentAccountManagerKey = wu.AccountManagerKey
			WHERE wu.WebUserKey = @WebUserKey
			UNION
			SELECT ParentAccountManagerKey AS AccountManagerKey /*all managers */
			FROM dbo.BridgeAccountManager bam
			JOIN dbo.WebUser wu ON bam.SubsidiaryAccountManagerKey = wu.AccountManagerKey
			WHERE wu.WebUserKey = @WebUserKey
			UNION
			SELECT SubsidiaryAccountManagerKey AS AccountManagerKey /*all peers (ie we have the same manager) */
			FROM dbo.BridgeAccountManager
			WHERE ParentAccountManagerKey=
				(SELECT ParentAccountManagerKey 
				FROM dbo.BridgeAccountManager bam
				JOIN dbo.WebUser wu ON bam.SubsidiaryAccountManagerKey = wu.AccountManagerKey
				WHERE wu.WebUserKey = @WebUserKey AND NumberOfLevels = 1)
			UNION
			SELECT wu.AccountManagerKey /* all users not in the hierarchy */
			FROM dbo.WebUser wu
			LEFT JOIN dbo.BridgeAccountManager bam ON bam.ParentAccountManagerKey = wu.AccountManagerKey
			LEFT JOIN dbo.BridgeAccountManager bam2 ON bam2.SubsidiaryAccountManagerKey = wu.AccountManagerKey
			WHERE bam.ParentAccountManagerKey IS NULL
			AND bam2.SubsidiaryAccountManagerKey IS NULL

			EXCEPT 
			(--SELECT dam.AccountManagerKey /*except any current delegates */ 
				--FROM WebUserDelegate wud
				--JOIN dbo.WebUser wu ON wu.WebUserKey = wud.DelegateWebUserKey
				--JOIN dbo.DimAccountManager dam ON dam.AccountManagerKey = wu.AccountManagerKey
				--WHERE DelegateWebUserKey <> DelegatorWebUserKey
				--UNION
				SELECT dam.AccountManagerKey /*except any current delegators */
				FROM WebUserDelegate wud
				JOIN dbo.WebUser wu ON wu.WebUserKey = wud.DelegatorWebUserKey
				JOIN dbo.DimAccountManager dam ON dam.AccountManagerKey = wu.AccountManagerKey
				WHERE DelegateWebUserKey <> DelegatorWebUserKey
				AND GETDATE() BETWEEN wud.StartDate AND ISNULL(wud.EndDate,'12-31-9999') )
		) w
	JOIN dbo.WebUser wu ON wu.AccountManagerKey = w.AccountManagerKey
	JOIN dbo.ODSPerson op ON op.ODSPersonKey = wu.ODSPersonKey
	WHERE wu.WebUserKey <> @WebUserKey

END

GO
