SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO







--   Select * from BridgeAccountManager

CREATE PROCEDURE [lion].[Load_BridgeAccountManager]
AS

SET NOCOUNT ON

EXEC LogDCPEvent 'ETL - Load_BridgeAccountManager', 'B'

DECLARE 
	@CoordinatorAM Key_Normal_Type, 
	@UnknownAM Key_Normal_Type,
	@DefaultAM Key_Normal_Type,
	@AS_MANAGES_IS_MANAGED_BY INT,
	@AS_VIEWS_IS_VIEWED_BY INT,
	@Levels INT,
	@RowsAffected INT,
	@RowsI Int_type, 
	@RowsU Int_type, 
	@RowsD Int_type

SET @Levels = 0
SET @AS_MANAGES_IS_MANAGED_BY = 1
SET @AS_VIEWS_IS_VIEWED_BY = 2
SET @RowsI = 0
SET @RowsU = 0
SET @RowsD = 0


CREATE TABLE #Downline (
	ParentID INT,
	ChildID INT,
	Levels INT,
	TopLevelFlag VARCHAR(10) DEFAULT 'N',
	BottomLevelFlag VARCHAR(10) DEFAULT 'N'
)

CREATE INDEX i_downline_1 ON #Downline (ParentID)

CREATE CLUSTERED INDEX i_downline_2 ON #Downline (ChildID)


--First, get everyone to manage themselves who has a manage relationship
INSERT INTO #Downline (ParentID, ChildID, Levels) 
SELECT DISTINCT UserID, UserID, @Levels 
FROM
	(
	SELECT DISTINCT UserID1 UserID 
	FROM Lion_Staging..UserAssoc
	WHERE UserID1 <> UserID2 AND Type = 1
	UNION 
	SELECT DISTINCT UserID2 UserID
	FROM Lion_Staging..UserAssoc
	WHERE UserID1 <> UserID2 AND Type = 1
	) A


-- Insert the active managers
SELECT @RowsAffected = 1  -- init in such a way to get the loop going
WHILE @RowsAffected > 0
BEGIN
	SET @Levels = @Levels + 1
	INSERT INTO #Downline (ParentID, ChildID, Levels)
	SELECT d.ParentID, ua.UserID2, @Levels
	FROM #Downline d
	JOIN Lion_Staging..UserAssoc ua ON
		d.ChildID = ua.UserID1 AND ua.Type = @AS_MANAGES_IS_MANAGED_BY
	WHERE 
	   ua.UserID2 NOT IN
		(SELECT ChildID 
		FROM #Downline
		WHERE ParentID = d.ParentID)

	SELECT @RowsAffected = @@Rowcount
END


--Top level users (works)
UPDATE #Downline SET TopLevelFlag = 'Y' 
WHERE 
	ParentID IN (SELECT DISTINCT ParentID FROM #Downline WHERE ParentID NOT IN (SELECT DISTINCT ChildID FROM #Downline WHERE Levels <> 0))
	AND Levels = 0

--Bottom level users (Works)
UPDATE #Downline SET BottomLevelFlag = 'Y' 
WHERE 
	ParentID IN (SELECT DISTINCT ParentID FROM #Downline WHERE ParentID NOT IN (SELECT DISTINCT ParentID FROM #Downline WHERE ParentID <> ChildID))



DELETE FROM BridgeAccountManager
SET @RowsD = @RowsD + @@RowCount

--Select * from #Downline

INSERT INTO BridgeAccountManager(ParentAccountManagerKey, SubsidiaryAccountManagerKey, NumberOfLevels, BottomMostFlag, TopMostFlag, WeightFactor)
SELECT DISTINCT ParentAM.AccountManagerKey, ChildAM.AccountManagerKey, Levels, BottomLevelFlag, TopLevelFlag, 1
FROM #Downline D
JOIN Lion_Staging..[User] ParentUser ON ParentUser.UserID = D.ParentID
JOIN Lion_Staging..[User] ChildUser ON ChildUser.UserID = D.ChildID

JOIN ODSPerson ODSPParent ON ODSPParent.ExtID = ParentUser.ExtID
JOIN ODSPerson ODSPChild ON ODSPChild.ExtID = ChildUser.ExtID

JOIN DimPerson DimPParent ON DimPParent.ODSPersonKey = ODSPParent.ODSPersonKey
JOIN DimPerson DimPChild ON DimPChild.ODSPersonKey = ODSPChild.ODSPersonKey

JOIN DimAccountManager ParentAM ON ParentAM.DimPersonKey = DimPParent.DimPersonKey
JOIN DimAccountManager ChildAM ON ChildAM.DimPersonKey = DimPChild.DimPersonKey
ORDER BY ParentAM.AccountManagerKey
SET @RowsI = @RowsI + @@RowCount


SET @CoordinatorAM = (SELECT MIN(AccountManagerKey) FROM DimAccountManager WHERE DimPersonKey IN (SELECT DimPersonKey FROM DimPerson dp INNER JOIN dbo.ODSPerson odsp ON odsp.ODSPersonKey = dp.ODSPersonKey WHERE dp.FullName =  'Coordinator' OR odsp.ExtID = '0'))
SET @UnknownAM = (SELECT AccountManagerKey FROM DimAccountManager WHERE DimPersonKey = (SELECT DimPersonKey FROM DimPerson WHERE FullName =  'Unknown'))
SET @DefaultAM = (SELECT AccountManagerKey FROM DimAccountManager WHERE DimPersonKey = (SELECT DimPersonKey FROM dbo.DimPerson WHERE Email LIKE 'David.Boothman%'))


--Now, we need to put the "unknown" account manager into the bridge as being managed by CoordinatorUser or default manager
INSERT BridgeAccountManager(ParentAccountManagerKey, SubsidiaryAccountManagerKey, NumberOfLevels, BottomMostFlag, TopMostFlag, WeightFactor)
VALUES (@UnknownAM, @UnknownAM, 0, 'Y', 'N', 1)
SET @RowsI = @RowsI + @@RowCount

INSERT BridgeAccountManager(ParentAccountManagerKey, SubsidiaryAccountManagerKey, NumberOfLevels, BottomMostFlag, TopMostFlag, WeightFactor)
SELECT ISNULL(@DefaultAM, @CoordinatorAM), @UnknownAM, 1, 'N', 'N', 1
SET @RowsI = @RowsI + @@RowCount

INSERT BridgeAccountManager(ParentAccountManagerKey, SubsidiaryAccountManagerKey, NumberOfLevels, BottomMostFlag, TopMostFlag, WeightFactor)
SELECT @CoordinatorAM, @UnknownAM, 2, 'N', 'N', 1
WHERE @DefaultAM <> @CoordinatorAM
AND @CoordinatorAM IS NOT NULL  --! sould not be the case 
SET @RowsI = @RowsI + @@RowCount


UPDATE BridgeAccountManager SET BottomMostFlag = 'Y' 
WHERE SubsidiaryAccountMAnagerKey IN
(SELECT SubsidiaryAccountManagerKey FROM BridgeAccountManager
	WHERE ParentAccountManagerKey NOT IN (
		SELECT DISTINCT ParentAccountMAnagerKey FROM BridgeAccountManager WHERE NumberOfLevels <> 0)
)

DROP TABLE #Downline

EXEC LogDCPEvent 'ETL - Load_BridgeAccountManager', 'E', @RowsI, @RowsU, @RowsD










GO
