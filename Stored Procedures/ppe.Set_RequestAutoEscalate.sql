SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [ppe].[Set_RequestAutoEscalate]
WITH EXECUTE AS CALLER
AS
BEGIN
	/* ppe.Set_RequestAutoEscalate implemented per LIO-466 */

	SET NOCOUNT ON

	EXEC LogDCPEvent 'ETL - ppe.Set_RequestAutoEscalate', 'B'

	DECLARE 
		@RowsI Quantity_Normal_type, 
		@RowsU Quantity_Normal_type, 
		@RowsD Quantity_Normal_type,
		@DayCount INT

	SET @RowsI = 0
	SET @RowsU = 0
	SET @RowsD = 0

	IF NOT EXISTS (SELECT * FROM dbo.Param WHERE ParamName='TermsRequestEscalationDays') -- per LPF-1465
		INSERT INTO Param(ParamName,ParamValue) VALUES ('TermsRequestEscalationDays','5')

	SELECT @DayCount = CAST(ParamValue AS INT) FROM [Param] WHERE  ParamName='TermsRequestEscalationDays' -- per LPF-1465

	INSERT INTO ppe.RequestLog
			( WebUserKey ,
			  RequestKey ,
			  OldStatusKey ,
			  NewStatusKey ,
			  RequestType ,
			  LogTime ,
			  RequestAction,
			  EmailSent,
			  OldOwnerWebUserKey,
			  NewOwnerWebUserKey,
			  Notes
			)
	SELECT 1, RequestKey, rm.StatusKey, rs2.StatusKey, '',GETDATE(), 'AUTO-ESCALATE',0,
	rm.OwnerWebUserKey, -- on acceptance, current becomes prior 
	CASE WHEN rs2.StatusName ='Approved' THEN ISNULL(wu0.WebUserKey,wu3.WebUserKey) ELSE wu2.WebUserKey END, -- managers webuserkey
	CASE
		WHEN rm.ItemKey = 1 THEN
			ISNULL('New Discount: ' + CAST(CAST(NewCompoundDiscount*100 AS DEC(24,2)) AS VARCHAR(100)) + '%', 'New Disc: NULL' )
		ELSE
			ISNULL('New Discount: ' + CAST(CAST(NewExceptionDiscount*100  AS DEC(24,2)) AS VARCHAR(100)) + '% ', 'New Disc: NULL ' ) + 
			ISNULL('New Price: £' + CAST(CAST(NewExceptionFixedPrice AS DEC(24,3)) AS VARCHAR(100)), 'New Price: NULL')
	END AS Note
	FROM ppe.RequestMain rm
	JOIN ppe.RequestStatus rs1 ON rs1.StatusKey = rm.StatusKey
	JOIN ppe.RequestStatus rs2 ON rs2.StatusPosition = rs1.StatusPosition + 1 
	JOIN dbo.DimAccount da ON da.AccountKey = rm.AccountKey
	JOIN WebUser wu ON wu.WebUserKey = rm.OwnerWebUserKey 
	JOIN BridgeAccountManager bam1 ON bam1.SubsidiaryAccountManagerKey = wu.AccountManagerKey  AND bam1.NumberOfLevels =1
	JOIN WebUser wu0 ON wu0.AccountManagerKey = da.AccountManagerKey
	JOIN WebUser wu2 ON wu2.AccountManagerKey = bam1.ParentAccountManagerKey
	/*LPF-2303 below*/
	LEFT JOIN (SELECT di.ItemGroup3Key, MAX(di.ItemUDVarChar2 ) ProdBrandAtLLSPG FROM dbo.DimItem di GROUP BY di.ItemGroup3Key) pb 
		ON pb.ItemGroup3Key = rm.ItemGroup3Key
	LEFT JOIN dbo.DimBrand db ON pb.ProdBrandAtLLSPG = db.Brand
	LEFT JOIN dbo.DimAccountBrandManager dabm ON dabm.AccountKey = da.AccountKey AND dabm.BrandKey = db.BrandKey
	LEFT JOIN dbo.WebUser wu3 ON wu3.AccountManagerKey = dabm.AccountManagerKey

	CROSS APPLY ppe.fn_GetPrice(rm.NewExceptionFixedPrice, rm.TradePrice, rm.NewExceptionDiscount, rm.NewCompoundDiscount) np
	WHERE 
	--rs1.StatusName IN ('Branch Manager Pending','Network Manager Pending','Central Pending')
	rs1.StatusName IN ('Level 1 Pending','Level 2 Pending' ,'Level 3 Pending' )
	AND (SELECT COUNT(DISTINCT DayKey) 
			FROM DimDay 
			WHERE SQLDate >= ISNULL(rm.ModificationDate,rm.CreationDate)
			AND SQLDate <= GETDATE()
			AND CalendarDay NOT IN ('Saturday','Sunday') ) > @DayCount
	SET @RowsI = @RowsI + @@RowCount


	UPDATE rm SET 
		StatusKey = rs2.StatusKey,
		[NewSPGDiscount1]	= [ProposedSPGDiscount1] ,
		[NewSPGDiscount2]	= [ProposedSPGDiscount2] ,
		--[NewSPGFromDate]	=  ,
		[NewExceptionDiscount] = [ProposedExceptionDiscount] ,
		[NewExceptionFixedPrice] = [ProposedExceptionFixedPrice] ,
		[NewExceptionStartDate]	= [ProposedExceptionStartDate] ,
		[NewExceptionExpiryDate] =  [ProposedExceptionExpiryDate] ,
		ModifiedByWebUserKey = 1,
		ModificationDate = GETDATE(),
		LastActionKey = (SELECT ActionKey FROM ppe.RequestAction WHERE ActionName = 'Auto-Escalated'),
		PriorOwnerWebUserKey = rm.OwnerWebUserKey,
		OwnerWebUserKey = CASE WHEN rs2.StatusName ='Approved' THEN ISNULL(wu0.WebUserKey,wu3.WebUserKey) ELSE wu2.WebUserKey END -- owner's manager's webuserkey
	FROM ppe.RequestMain rm
	JOIN ppe.RequestStatus rs1 ON rs1.StatusKey = rm.StatusKey
	JOIN ppe.RequestStatus rs2 ON rs2.StatusPosition = rs1.StatusPosition + 1 
	JOIN dbo.DimAccount da ON da.AccountKey = rm.AccountKey
	JOIN WebUser wu0 ON wu0.AccountManagerKey = da.AccountManagerKey
	JOIN WebUser wu ON wu.WebUserKey = rm.OwnerWebUserKey 
	JOIN BridgeAccountManager bam1 ON bam1.SubsidiaryAccountManagerKey = wu.AccountManagerKey  AND bam1.NumberOfLevels =1
	JOIN WebUser wu2 ON wu2.AccountManagerKey = bam1.ParentAccountManagerKey
	/*LPF-2303 below*/
	LEFT JOIN (SELECT di.ItemGroup3Key, MAX(di.ItemUDVarChar2 ) ProdBrandAtLLSPG FROM dbo.DimItem di GROUP BY di.ItemGroup3Key) pb 
		ON pb.ItemGroup3Key = rm.ItemGroup3Key
	LEFT JOIN dbo.DimBrand db ON pb.ProdBrandAtLLSPG = db.Brand
	LEFT JOIN dbo.DimAccountBrandManager dabm ON dabm.AccountKey = da.AccountKey AND dabm.BrandKey = db.BrandKey
	LEFT JOIN dbo.WebUser wu3 ON wu3.AccountManagerKey = dabm.AccountManagerKey

	WHERE 
	--rs1.StatusName IN ('Branch Manager Pending','Network Manager Pending','Central Pending')
	rs1.StatusName IN ('Level 1 Pending','Level 2 Pending' ,'Level 3 Pending')
	AND (SELECT COUNT(DISTINCT DayKey) 
			FROM DimDay 
			WHERE SQLDate >= ISNULL(rm.ModificationDate,rm.CreationDate)
			AND SQLDate <= GETDATE()
			AND CalendarDay NOT IN ('Saturday','Sunday') ) > @DayCount

	SET @RowsU = @RowsU + @@RowCount

	EXEC LogDCPEvent 'ETL - ppe.Set_RequestAutoEscalate', 'E', @RowsI, @RowsU, @RowsD


END

GO
