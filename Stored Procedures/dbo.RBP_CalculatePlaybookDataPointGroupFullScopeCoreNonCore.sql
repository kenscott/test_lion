SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE          PROCEDURE [dbo].[RBP_CalculatePlaybookDataPointGroupFullScopeCoreNonCore]

@PlaybookDataPointGroupKey INT, @SegmentFactorPlaybookKey INT
AS
SET QUOTED_IDENTIFIER ON 
SET ANSI_NULLS ON 
SET ARITHABORT ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
SET CONCAT_NULL_YIELDS_NULL ON
SET NUMERIC_ROUNDABORT OFF
DECLARE @RowsI Quantity_Normal_type, @RowsU Quantity_Normal_type, @RowsD Quantity_Normal_type


DECLARE @ProjectKey key_normal_type
SELECT @ProjectKey = ProjectKey FROM PlaybookDataPointGroup WHERE PlaybookDataPointGroupKey = @PlaybookDataPointGroupKey


EXEC LogDCPEvent 'DCP --- RBP_CalculatePlaybookDataPointGroupFullScopeCoreNonCore', 'B',0,0,0,@ProjectKey
SET @RowsI = 0
SET @RowsU = 0
SET @RowsD = 0

DELETE FROM PlaybookDataPointGroupFullScopeCoreNonCore
WHERE PlaybookDataPointGroupKey = @PlaybookDataPointGroupKey
SET @RowsD = @RowsD + @@rowcount



INSERT INTO PlaybookDataPointGroupFullScopeCoreNonCore(
PlaybookDataPointGroupKey, 
AccountKey, 
ItemGroup1Key, 
PercentItemGroup1SalesToTotalAccountSales,
CoreNonCoreIndicator)
SELECT
@PlaybookDataPointGroupKey,
AccountKey,
ItemGroup1Key,
PercentItemGroup1SalesToTotalAccountSales,
CASE
	WHEN PercentItemGroup1SalesToTotalAccountSales > ATKSegmentFactorPlaybook.CoreNonCoreThreshold THEN 'Y'
	ELSE 'N'
END AS 'CoreNonCoreIndicator'
FROM
FactCoreNonCore
JOIN ATKSegmentFactorPlaybook ON ATKSegmentFactorPlaybook.SegmentFactorPlaybookKey = @SegmentFactorPlaybookKey

SET @RowsI = @RowsI + @@Rowcount

DECLARE @USql NVARCHAR(MAX)
SET @USql = 'Update Statistics PlaybookDataPointGroupFullScopeCoreNonCore'
EXEC (@USql)

EXEC LogDCPEvent 'DCP --- RBP_CalculatePlaybookDataPointGroupFullScopeCoreNonCore', 'E', @RowsI, @RowsU, @RowsD




GO
