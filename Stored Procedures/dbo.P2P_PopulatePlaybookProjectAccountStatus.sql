SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO













CREATE                                            
PROCEDURE [dbo].[P2P_PopulatePlaybookProjectAccountStatus]
	@ProjectKey Key_Normal_type, 
	@PlaybookDataPointGroupKey Key_Normal_type
AS

SET NOCOUNT ON




DECLARE
	@RowsI Quantity_Normal_type, 
	@RowsU Quantity_Normal_type, 
	@RowsD Quantity_Normal_type	


SET @RowsI = 0
SET @RowsU = 0
SET @RowsD = 0


-- this can be made its own sp that could be used by rbp as well and maybe others

EXEC LogDCPEvent 'DCP - P2P_PopulatePlaybookProjectAccountStatus: Insert PlaybookProjectAccountStatus', 'B', 0, 0, 0,@ProjectKey

SET @RowsI = 0
SET @RowsU = 0
SET @RowsD = 0

INSERT PlaybookProjectAccountStatus (
	ProjectKey, 
	AccountKey, 
	ProjectAccountStatus, 
	ProjectAccountManagerStatus, 
	AccountManagerKey
)
SELECT DISTINCT 
	@ProjectKey, 
	ProposalPrice.AccountKey, 
	'Not Reviewed',  
	'Not Finalized', 
	DimAccount.AccountManagerKey
FROM ProposalPrice
INNER JOIN DimAccount ON 
	DimAccount.AccountKey = ProposalPrice.AccountKey
WHERE 
	PlaybookDataPointGroupKey = @PlaybookDataPointGroupKey
SET @RowsI = @RowsI + @@ROWCOUNT

EXEC LogDCPEvent 'DCP - P2P_PopulatePlaybookProjectAccountStatus: Insert PlaybookProjectAccountStatus', 'E', @RowsI, @RowsU, @RowsD, @ProjectKey























GO
