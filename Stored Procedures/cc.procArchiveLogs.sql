SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE [cc].[procArchiveLogs]
AS
    DECLARE @ArchiveMonths INT ,
        @ArchiveDate DATETIME ,
        @RowsI INT ,
        @RowsU INT ,
        @RowsD INT

    SET @RowsI = 0
    SET @RowsU = 0
    SET @RowsD = 0
    SELECT  @ArchiveMonths = CAST(ParamValue AS INT)
    FROM    dbo.Param
    WHERE   ParamName = 'ArchiveMonths'	
    SELECT  @ArchiveDate = DATEADD(MONTH, @ArchiveMonths, GETDATE())


    EXEC LogDCPEvent 'cc.ArchiveLogs - SupplierContractLog', 'B'


    BEGIN TRY
        BEGIN TRANSACTION;

       INSERT   INTO cc.SupplierContractLogArchive
                ( ContractLogKey ,
                  WebUserKey ,
                  ContractKey ,
                  VendorKey ,
                  ContractID ,
                  ContractAction ,
                  Notes ,
                  CreationDate
                )
                SELECT  ContractLogKey ,
                        WebUserKey ,
                        ContractKey ,
                        vendorKey ,
                        ContractID ,
                        ContractAction ,
                        Notes ,
                        CreationDate
                FROM    cc.SupplierContractLog
                WHERE   ( CreationDate <= @ArchiveDate )
		
        SELECT  @RowsI = @@ROWCOUNT

	

        DELETE  FROM cc.SupplierContractLog
        WHERE   ( CreationDate <= @ArchiveDate )

        SELECT  @RowsD = @@ROWCOUNT

        COMMIT TRAN
    END TRY
    BEGIN CATCH
        IF @@TRANCOUNT > 0
            ROLLBACK TRAN
        EXEC dbo.LogDCPError
    END CATCH


    EXEC LogDCPEvent 'cc.ArchiveLogs - SupplierContractLog', 'E', @RowsI,
        @RowsU, @RowsD



GO
