SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [cc].[VerifyContractLocation] AS
BEGIN

	SET NOCOUNT ON

--Branch
UPDATE cc.SupplierContractLocation
SET LocationName = AG1Level1,
	LocationDescription = AG1Level1UDVarchar1
FROM dbo.DimAccountGroup1
WHERE ContractLocationKey IN (
	SELECT scl.ContractLocationKey
	FROM cc.SupplierContractLocation scl
	INNER JOIN dbo.DimAccountGroup1 dag1
		ON dag1.AccountGroup1Key = scl.LocationKey
	WHERE scl.ContractLocationLevelKey = 1
		AND (scl.LocationName <> dag1.AG1Level1 OR
		scl.LocationDescription <> dag1.AG1Level1UDVarchar1)
	)
AND AccountGroup1Key = LocationKey

--Region
UPDATE cc.SupplierContractLocation
SET LocationName = Region,
	LocationDescription = RegionName
FROM dbo.DimRegion
WHERE ContractLocationKey IN (
	SELECT scl.ContractLocationKey
	FROM cc.SupplierContractLocation scl
	INNER JOIN dbo.DimRegion dr
		ON dr.RegionKey = scl.LocationKey
	WHERE scl.ContractLocationLevelKey = 4
		AND (scl.LocationName <> dr.Region OR
		scl.LocationDescription <> dr.RegionName)
	)
AND RegionKey = LocationKey

--Area
UPDATE cc.SupplierContractLocation
SET LocationName = Area,
	LocationDescription = AreaName
FROM dbo.DimArea
WHERE ContractLocationKey IN (
	SELECT scl.ContractLocationKey
	FROM cc.SupplierContractLocation scl
	INNER JOIN dbo.DimArea da
		ON da.AreaKey = scl.LocationKey
	WHERE scl.ContractLocationLevelKey = 3
		AND (scl.LocationName <> da.Area OR
		scl.LocationDescription <> da.AreaName)
	)
AND AreaKey = LocationKey

--Network
UPDATE cc.SupplierContractLocation
SET LocationName = NetworkId,
	LocationDescription = NetworkDescription
FROM dbo.DimNetwork
WHERE ContractLocationKey IN (
	SELECT scl.ContractLocationKey
	FROM cc.SupplierContractLocation scl
	INNER JOIN dbo.DimNetwork dn
		ON dn.NetworkKey = scl.LocationKey
	WHERE scl.ContractLocationLevelKey = 2
		AND (scl.LocationName <> dn.NetworkId OR
		scl.LocationDescription <> dn.NetworkDescription)
	)
AND NetworkKey = LocationKey

--Brand
UPDATE cc.SupplierContractLocation
SET LocationName = Brand,
	LocationDescription = Brand
FROM dbo.DimBrand
WHERE ContractLocationKey IN (
	SELECT scl.ContractLocationKey
	FROM cc.SupplierContractLocation scl
	INNER JOIN dbo.DimBrand db
		ON db.BrandKey = scl.LocationKey
	WHERE scl.ContractLocationLevelKey = 5
		AND (scl.LocationName <> db.Brand OR
		scl.LocationDescription <> db.Brand)
	)
AND BrandKey = LocationKey

END
GO
