SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [cm].[GetMatchingTransactions] (@ClientInvoiceLineUniqueId INT)

AS

-- EXEC cm.GetMatchingTransactions 625326665
-- select top 100 * from cm.FactInvoiceLine

DECLARE @VendorNumber VARCHAR(50)
SET @VendorNumber = (SELECT VendorNumber FROM cm.FactInvoiceLine
	WHERE ClientInvoiceLineUniqueID = @ClientInvoiceLineUniqueId)

SELECT
	ChargeNoteId,
	SLedgerDate AS InvoiceDate,
	SLedgerNo AS InvoiceNumber,
	BranchCode AS Branch,
	BranchDescription,
	AccountNumber AS Account,
	AccountName,
	ProductCode AS Product,
	ProductDescription,
	TransType AS TransactionType,
	SupplyType,
	CASE 
		WHEN CTSReleaseCode IS NOT NULL THEN 'Y'
		ELSE 'N'
	END AS CTSFlag,
	Qty AS Quantity,
	NettPrice AS Price,
	NettPrice / NULLIF(Qty, 0) AS UnitPrice,
	SupplierNettCost AS Cost,
	SupplierNettCost / NULLIF(Qty, 0) AS UnitCost,
	TradingMargin / NULLIF(NettPrice,0) AS BeforeMarginPerc,
	TradingMargin AS BeforeMarginValue

FROM cm.FactInvoiceLine

WHERE ChargeNoteID IN (
	SELECT DISTINCT ChargeNoteID
	FROM cm.FactInvoiceLine
	WHERE ClientInvoiceLineUniqueID = @ClientInvoiceLineUniqueId
	)
	AND VendorNumber = @VendorNumber
GO
