SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO







--RBP_PopulatePlaybookPriceProposal 22, 9, 'N'


--Select * from PlaybookDataPointGroup
--Select * from PlaybookPriceProposal
--Select count(*) from PlaybookPriceProposal
--Select count(*) from ProposalPrice where PlaybookPriceProposalKey is not null
--Select count(*) from ProposalPrice
--exec RBP_PopulatePlaybookPriceProposal 167, 5, 'N'

--exec RBP_PopulatePlaybookPriceProposal 7, 17, 'N'

--clone_scenario 20, 'y'


--select * from atkscenario
CREATE
PROCEDURE [dbo].[RBP_PopulatePlaybookPriceProposal] (@PlaybookDataPointGroupKey INT, @ProposalRecommendationPlaybookKey INT, @Execute CHAR(1) = 'Y')
AS



--RBP_PopulatePlaybookPriceProposal 21, 8, 'N'

---------------------------------------------------------------
--Customizations:
--TBW: 07/16/2005	The acceptance code has been set to rejected for items with a weighted account/item band of A, and accepted for everything else
--TBW: 07/16/2005	Made the proposalPrice and ProposalCost statements use dynamic SQL and added where clause for them.
---------------------------------------------------------------
SET QUOTED_IDENTIFIER ON 
SET ANSI_NULLS ON 
SET ARITHABORT ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
SET CONCAT_NULL_YIELDS_NULL ON
SET NUMERIC_ROUNDABORT OFF
SET NOCOUNT ON
SET xact_abort ON

DECLARE @DynamicDeleteWhere1 NVARCHAR(MAX), @DynamicDeleteJoin1 NVARCHAR(MAX), @DynamicDeleteGroupBy1 NVARCHAR(MAX), @DynamicDeleteHaving1  NVARCHAR(MAX), @txt NVARCHAR(MAX)
DECLARE @Str NVARCHAR(2000), @ApproachTypeKey INT
DECLARE @DynamicUDVarchar1 NVARCHAR(MAX)
DECLARE @DynamicUDVarchar2 NVARCHAR(MAX)
DECLARE @ProjectKey Key_Normal_Type
DECLARE @MinPriceIncreaseThresholdPercent Percent_Type
DECLARE @FieldList NVARCHAR(4000)
DECLARE @SQLStmt NVARCHAR(MAX)
DECLARE @WhereClause1 NVARCHAR(MAX), @WhereClause2 NVARCHAR(MAX), @WhereClause3 NVARCHAR(MAX)
DECLARE @JoinClause1 NVARCHAR(MAX), @JoinClause2 NVARCHAR(MAX), @JoinClause3 NVARCHAR(MAX)
DECLARE @PriceOrCostFiltering NVARCHAR(4000)
DECLARE @MinCostDecreaseThresholdPercent Percent_Type, @CostDecreaseCapPercent Percent_Type
DECLARE @ForecastType NVARCHAR(50)
DECLARE @ProposedPriceOverride NVARCHAR(MAX), @ProposedCostOverride NVARCHAR(MAX)
--ALTER  table #Temp1 (SqlStmt ntext)

SET @ProjectKey = (SELECT ProjectKey FROM PlaybookDataPointGroup WHERE PlaybookDataPointGroupKey = @PlaybookDataPointGroupKey)
SELECT @ApproachTypeKey = ApproachTypeKey FROM ATKProjectSpecification WHERE ProjectSpecificationKey = (SELECT ProjectSpecificationKey FROM PlaybookProject WHERE ProjectKey = @ProjectKey)

DECLARE @ApproachTypeDesc NVARCHAR(500)
SELECT @ApproachTypeDesc = ApproachTypeDesc FROM ATKApproachType WHERE ApproachTypeKey = @ApproachTypeKey

DECLARE @DeferredCode INT
SELECT @DeferredCode = AcceptanceCodeKey FROM atkacceptancecode WHERE acceptanceCodeDesc = 'Deferred'

DECLARE @ScenarioKey INT
SELECT @ScenarioKey = ScenarioKey
FROM PlaybookDataPointGroup A
JOIN ATKProjectSpecificationScenario B ON A.ProjectSpecificationScenarioKey = B.ProjectSpecificationScenarioKey
WHERE A.PlaybookDataPointGroupKey = @PlaybookDataPointGroupKey

DECLARE @SuppressionTypeKey Key_Small_Type
SELECT @SuppressionTypeKey = SuppressionTypeKey FROM ATKScenario WHERE ScenarioKey = @ScenarioKey

DECLARE @SuppressionJoinClause NVARCHAR(MAX)

DECLARE @ProposedCost NVARCHAR(MAX), @ProposedPrice NVARCHAR(MAX)

DECLARE
	@Pass INT,
	@PriceIncreaseCapPercent Percent_Type,
	@ProposalDayKey Key_Small_Type,
	@ProposalRecommendationKey INT

DECLARE @RowsI Quantity_Normal_type, @RowsU Quantity_Normal_type, @RowsD Quantity_Normal_type, @PricingRuleWhereClause NVARCHAR(2000), @tmpValuesAssigned NVARCHAR(2000)
SET @RowsI = 0
SET @RowsU = 0
SET @RowsD = 0

IF @Execute = 'Y' 
BEGIN
	EXEC LogDCPEvent 'DCP --- RBP_PopulatePlaybookPriceProposal: Delete', 'B',0,0,0,@ProjectKey


	DELETE FROM ProposalPrice WHERE PlaybookDataPointGroupKey = @PlaybookDataPointGroupKey
	SET @RowsD = @RowsD + @@RowCOunt
	DELETE FROM PlaybookPriceProposal WHERE PlaybookDataPointGroupKey = @PlaybookDataPointGroupKey
	SET @RowsD = @RowsD + @@RowCOunt

	DELETE FROM PlaybookProjectAccountStatus WHERE ProjectKey = @ProjectKey
	SET @RowsD = @RowsD + @@RowCOunt
	DELETE FROM PlaybookProjectAccountManagerStatus WHERE ProjectKey = @ProjectKey
	SET @RowsD = @RowsD + @@RowCOunt
	
	EXEC LogDCPEvent 'DCP --- RBP_PopulatePlaybookPriceProposal: Delete', 'E', @RowsI, @RowsU, @RowsD 
END

SET @RowsI = 0
SET @RowsU = 0
SET @RowsD = 0

-------------------------------------------
SET @RowsI = 0
SET @RowsU = 0
SET @RowsD = 0

SET @FieldList = '@ProposalRecommendationKey INT, @PriceIncreaseCapPercent Percent_Type, @PlaybookDataPointGroupKey INT,  @MinPriceIncreaseThresholdPercent Percent_Type, @ApproachTypeDesc nvarchar(500), @RowsI INT OUT'

--Sets the forecat type (n month, 3 month, 12 month, etc)
SELECT @ForecastType =  ''''+ForecastType+'''',
		@DynamicDeleteWhere1 = DynamicDeleteWhere1,
		@DynamicDeleteJoin1 = DynamicDeleteJoin1,
		@DynamicDeleteGroupBy1 = DynamicDeleteGroupBy1,
		@DynamicDeleteHaving1 = DynamicDeleteHaving1
FROM ATKProposalRecommendationPlaybook
WHERE ProposalRecommendationPlaybookKey =  @ProposalRecommendationPlaybookKey





DECLARE CurAutoSagePass CURSOR FOR
SELECT ProposalRecommendationKey, ProposalRecommendationPassSequenceNumber, ISNULL(PriceIncreaseCapPercent,9999999), ISNULL(MinPriceIncreaseThresholdPercent,0), ISNULL(MinCostDecreaseThresholdPercent,0), ISNULL(CostDecreaseCapPercent,9999999), ProposedPriceOverride, ProposedCostOverride, DynamicUDVarchar1, DynamicUDVarchar2
FROM ATKProposalRecommendation
WHERE ProposalRecommendationPlaybookKey =  @ProposalRecommendationPlaybookKey
ORDER BY ProposalRecommendationPassSequenceNumber

OPEN CurAutoSagePass
FETCH NEXT FROM CurAutoSagePass INTO
	@ProposalRecommendationKey, @Pass, @PriceIncreaseCapPercent, @MinPriceIncreaseThresholdPercent, @MinCostDecreaseThresholdPercent, @CostDecreaseCapPercent, @ProposedPriceOverride, @ProposedCostOverride, @DynamicUDVarchar1, @DynamicUDVarchar2


WHILE (@@fetch_status <> -1)
BEGIN

	SET @RowsI = 0
	SET @RowsU = 0
	SET @RowsD = 0
	SET @Str = 'DCP --- RBP_PopulatePlaybookPriceProposal: AutoSage Pass: '+CAST(@Pass AS NVARCHAR(100))
	IF @Execute = 'Y'
	BEGIN
		EXEC LogDCPEvent @Str, 'B',0,0,0,@ProjectKey
	END

	DECLARE @PriceCostFiltering NVARCHAR(2000)

	--If we're not overriding the optimal price then we need to make sure we dont propose on anything where proposed < last
	IF @ProposedPriceOverride = ''		
	BEGIN
		SET @ProposedPrice = '
			CASE
				WHEN '''+CAST(@ApproachTypeDesc AS NVARCHAR(500))+''' = ''Reality Based Pricing'' and PlaybookDataPointOptimalPrice.OptimalPrice < PlaybookDataPoint.LastItemPrice * (1.0 + '+CAST(@PriceIncreaseCapPercent AS NVARCHAR(500))+') THEN PlaybookDataPointOptimalPrice.OptimalPrice
				WHEN '''+CAST(@ApproachTypeDesc AS NVARCHAR(500))+''' = ''Reality Based Pricing'' THEN PlaybookDataPoint.LastItemPrice * (1.0 + '+CAST(@PriceIncreaseCapPercent AS NVARCHAR(500))+')
				ELSE NULL
			END
		'
	END
	ELSE
	BEGIN
		SET @ProposedPrice = '
		'+@ProposedPriceOverride+'
		'

	END

	IF @ProposedCostOverride = ''
	BEGIN
		SET @ProposedCost = '
			CASE
				WHEN '''+CAST(@ApproachTypeDesc AS NVARCHAR(500))+''' = ''Deviated Cost (Reality Based Costing)'' and PlaybookDataPointOptimalPrice.OptimalCost between PlaybookDataPoint.LastItemCost * (1.0 - '+CAST(@CostDecreaseCapPercent AS NVARCHAR(500))+') and PlaybookDataPoint.LastItemCost THEN PlaybookDataPointOptimalPrice.OptimalCost
				WHEN '''+CAST(@ApproachTypeDesc AS NVARCHAR(500))+''' = ''Deviated Cost (Reality Based Costing)'' and PlaybookDataPointOptimalPrice.OptimalCost < PlaybookDataPoint.LastItemCost * (1.0 - '+CAST(@CostDecreaseCapPercent AS NVARCHAR(500))+') THEN PlaybookDataPoint.LastItemCost * (1.0 - '+CAST(@CostDecreaseCapPercent AS NVARCHAR(500))+')
				WHEN '''+CAST(@ApproachTypeDesc AS NVARCHAR(500))+''' = ''Deviated Cost (Reality Based Costing)'' and PlaybookDataPointOptimalPrice.OptimalCost > PlaybookDataPoint.LastItemCost THEN PlaybookDataPointOptimalPrice.OptimalCost
				Else NULL
			END
		'
	END
	ELSE
	BEGIN
		SET @ProposedCost = '
		'+@ProposedCostOverride+'
		'
	END

	IF @ProposedCostOverride <> '' OR @ProposedPriceOverride <> ''
	BEGIN
		SET @PriceCostFiltering = 'where (ProposedCost <> 0.0 or ProposedCost is null) and (ProposedPrice <> 0.0 or ProposedPrice is null)'
	END
	ELSE
	BEGIN
		SET @PriceCostFiltering = '
	WHERE (ProposedCost < LastItemCost or ProposedCost is NULL)
	and (ProposedPrice > LastItemPrice or ProposedPrice is NULL)
		'
	END

	SELECT @WhereClause1 = WhereClause1,
			@JoinClause1 = JoinClause1,
			@WhereClause2 = WhereClause2,
			@JoinClause2= JoinClause2,
			@WhereClause3 = WhereClause3,
			@JoinClause3 = JoinClause3
	FROM ATKProposalRecommendation
	WHERE ProposalRecommendationKey = @ProposalRecommendationKey

	IF @WhereClause1 <> '' AND @WhereClause1 NOT LIKE 'and %'
		SET @WhereClause1 = 'and ' + @WhereClause1
	IF @WhereClause2 <> '' AND @WhereClause2 NOT LIKE 'and %'
		SET @WhereClause2 = 'and ' + @WhereClause2
	IF @WhereClause3 <> '' AND @WhereClause3 NOT LIKE 'and %'
		SET @WhereClause3 = 'and ' + @WhereClause3

	--Now, if we're going to suppress optimals from being generated, add the dynamic SQL here:
	IF (SELECT SuppressionTypeCode 
		FROM ATKSuppressionType 
		WHERE SuppressionTypeKey = @SuppressionTypeKey) = 'Proposal'
	BEGIN
		SET @SuppressionJoinClause = 
		'left join (SELECT Distinct AccountKey, ItemKey, InvoiceLineGroup1Key 
				FROM ProposalPrice
				WHERE SuppressionDate >= GetDate()) SuppressionList
			on SuppressionList.AccountKey = PlaybookDataPoint.AccountKey
				and SuppressionList.ItemKey = PlaybookDataPoint.ItemKey
				and SuppressionList.InvoiceLineGroup1Key = PlaybookDataPoint.InvoiceLineGroup1Key'
		SET @WhereClause1 = @WhereClause1 + '
			and SuppressionList.AccountKey is null
			'
	END
	ELSE
		SET @SuppressionJoinClause = ''



	SET @SQLStmt = '


	declare @RowsI int, @RowsU int, @RowsD int
	select @RowsI = 0, @RowsU = 0, @RowsD = 0

	INSERT INTO PlaybookPriceProposal
		(
		PlaybookDataPointOptimalPriceKey,
		ProposalRecommendationKey, 
		ProposedPrice,
		ProposedCost,
		PlaybookDataPointGroupKey
		)
	SELECT 
		PlaybookDataPointOptimalPriceKey,
		'+CAST(@ProposalRecommendationKey AS NVARCHAR(500))+',
		ProposedPrice,
		ProposedCost,
		'+CAST(@PlaybookDataPointGroupKey AS NVARCHAR(500))+'
	FROM
		(
		SELECT
			PlaybookDataPointOptimalPrice.PlaybookDataPointOptimalPriceKey,
			'+@ProposedPrice+'
			ProposedPrice,
			'+@ProposedCost+'
			ProposedCost,
			PlaybookDataPoint.LastItemCost,
			PlaybookDataPoint.LastItemPrice
		FROM PlaybookDataPointOptimalPrice
			join PlaybookDataPointPricingGroup on 
				PlaybookDataPointOptimalPrice.PlaybookDataPointPricingGroupKey = PlaybookDataPointPricingGroup.PlaybookDataPointPricingGroupKey
			join PlaybookDataPoint on 
				PlaybookDataPoint.PlaybookDataPointKey = PlaybookDataPointPricingGroup.PlaybookDataPointKey
			' + @JoinClause1 + '
			' + @SuppressionJoinClause + '
		WHERE
			not exists (Select 1 from PlaybookPriceProposal where PlaybookPriceProposal.PlaybookDataPointOptimalPriceKey = PlaybookDataPointOptimalPrice.PlaybookDataPointOptimalPriceKey)--To see if a proposal has already been created for this data point.  We only want 1 proposal per account/item/shiptype
			And PlaybookDataPoint.PlaybookDataPointGroupKey = '+CAST(@PlaybookDataPointGroupKey AS NVARCHAR(500))+'
			And ISNULL(PlaybookDataPoint.LastItemPrice, 0.0) <> 0.0 -- Valid value for LastItemPrice
			And ISNULL(PlaybookDataPoint.LastItemCost, 0.0) <> 0.0 -- Valid value for LastItemCost
			'+ @WhereClause1 + '
		) Results '
	+ @PriceCostFiltering +'
	set @RowsI = @@RowCount
	exec LogDCPEvent '''+@Str+''', ''E'', @RowsI, 0, 0'
	
	--PRINT @SQLStmt
	EXEC dbo.PrintNVarcharMax @SQLStmt

	IF @Execute = 'Y'
	BEGIN
		EXECUTE(@SQLStmt)
	END



	--------------------------------------------------
	--------------------------------------------------
	--------------------------------------------------
	--------------------------------------------------
	----------NOW START THE INSERT TO PROPOSAL TABLES
	SET @txt = ''
	IF @ApproachTypeDesc = 'Reality Based Pricing'
	BEGIN
		IF @Execute = 'Y'
		BEGIN
			SET @txt = 'DCP --- RBP_PopulatePlaybookPriceProposal:  Insert ProposalPrice for AutoSage Pass: '+CAST(@Pass AS NVARCHAR(100))
			EXEC LogDCPEvent @txt, 'B',0,0,0,@ProjectKey
		END
	
		SET @SQLStmt = '
		
		declare @RowsI int, @RowsU int, @RowsD int
		select @RowsI = 0, @RowsU = 0, @RowsD = 0
	
		INSERT INTO ProposalPrice
			(
			ProjectKey,
			PlaybookDataPointGroupKey,
			AccountKey,
			AccountManagerKey,
			ItemKey,
			InvoiceLineGroup1Key,
			PlaybookPriceProposalKey, 
			PlaybookMarginErosionProposalKey, 
			ProposedPrice,
			NewPrice, 
		
			AcceptanceCodeKey,
			UserAcceptanceCodeKey,
			DefaultAcceptanceCodeKey,
		
			TotalActual12MonthQuantity,
			TotalActual12MonthFrequency,
			TotalActual12MonthCost,
			TotalActual12MonthPrice,
			LastItemCost,
			LastItemPrice,
			CurrentItemAISCost,
			CurrentItemStandardCost,
			FutureItemCost,
			Forecasted12MonthQuantity,
			TotalForecasted12MonthCost_LastCost,
		
			TotalForecasted12MonthPrice_LastPrice,
			TotalForecasted12MonthPrice_ProposedPrice,
			TotalForecasted12MonthPrice_NewPrice,
			TotalForecasted12MonthCost_RelevantCost,
		
			AcceptanceStatusDate,
			LastSaleDayKey,
			LastInvoiceLineGroup2Key,
			ReasonCodeKey,
			SuppressionDate,
			EffectiveDate,
			UDVarchar1,
			UDVarchar2,
			OptimalPrice
			--,
			--RelevantCost
			)
		SELECT
			PlaybookDataPointGroup.ProjectKey,
			PlaybookPriceProposal.PlaybookDataPointGroupKey,
			PlaybookDataPoint.AccountKey,
			DimAccount.AccountManagerKey,
			PlaybookDataPoint.ItemKey,
			PlaybookDataPoint.InvoiceLineGroup1Key,
			PlaybookPriceProposal.PlaybookPriceProposalKey,
			NULL,
			PlaybookPriceProposal.ProposedPrice,
			PlaybookPriceProposal.ProposedPrice,
			ATKPR.AcceptRejectCode_PriceProposal,
			'+CAST(@DeferredCode AS NVARCHAR(500))+',
			ATKPR.AcceptRejectCode_PriceProposal,
			PlaybookDataPoint.Total12MonthQuantity  as ''TotalActual12MonthQuantity'',
			FactLastPriceAndCost.Total12MonthInvoiceLines as ''TotalActual12MonthFrequency'',
			PlaybookDataPoint.TotalActual12MonthCost as ''TotalActual12MonthCost'',
			PlaybookDataPoint.TotalActual12MonthPrice as ''TotalActual12MonthPrice'',
			PlaybookDataPoint.LastItemCost,
			PlaybookDataPoint.LastItemPrice,
	
			FactCurrentPriceAndCost.CurrentItemCost, 
			DimItem.CurrentCost,
			DimItem.FutureCost,
	
			(FactSalesForecast.ForecastedMonthlyQty*12) ,
			PlaybookDataPoint.LastItemCost * FactSalesForecast.ForecastedMonthlyQty*12 as ''TotalForecasted12MonthCost_LastCost'',
			PlaybookDataPoint.LastItemPrice * FactSalesForecast.ForecastedMonthlyQty*12 as ''TotalForecasted12MonthPrice_LastPrice'',
			PlaybookPriceProposal.ProposedPrice * FactSalesForecast.ForecastedMonthlyQty*12 as ''TotalForecasted12MonthPrice_ProposedPrice'',
			PlaybookPriceProposal.ProposedPrice * FactSalesForecast.ForecastedMonthlyQty*12 as ''TotalForecasted12MonthPrice_NewPrice'',
			IsNull(DimItem.FutureCost,IsNull(FactCurrentPriceAndCost.CurrentItemCost, PlaybookDataPoint.LastItemCost)) * FactSalesForecast.ForecastedMonthlyQty*12  as ''TotalForecasted12MonthCost_RelevantCost'',
	
			GetDate(),
			FactLastPriceAndCost.LastSaleDayKey,
			FactLastPriceAndCost.LastInvoiceLineGroup2Key,
		
			IsNull(ATKPR.DynamicReasonCode, NULL),
			IsNull(ATKPR.DynamicSuppressionDate, getdate()),
			IsNull(ATKPR.DynamicEffectiveDate, getdate()),
			'''+@DynamicUDVarchar1+''',
			'''+@DynamicUDVarchar2+''',
			PlaybookDataPointOptimalPrice.OptimalPrice
			--,
			--IsNull(DimItem.FutureCost,IsNull(FactCurrentPriceAndCost.CurrentItemCost, PlaybookDataPoint.LastItemCost))
		FROM
			PlaybookPriceProposal
		 	join PlaybookDataPointOptimalPrice on 
				PlaybookDataPointOptimalPrice.PlaybookDataPointOptimalPriceKey = PlaybookPriceProposal.PlaybookDataPointOptimalPriceKey
		 	join PlaybookDataPointPricingGroup on 
				PlaybookDataPointPricingGroup.PlaybookDataPointPricingGroupKey = PlaybookDataPointOptimalPrice.PlaybookDataPointPricingGroupKey
			join PlaybookDataPoint on 
				PlaybookDataPoint.PlaybookDataPointKey = PlaybookDataPointPricingGroup.PlaybookDataPointKey 
				and PlaybookDataPoint.PlaybookDataPointGroupKey = PlaybookPriceProposal.PlaybookDataPointGroupKey
		 	join DimAccount on 
				DimAccount.AccountKey = PlaybookDataPoint.AccountKey
		 	join DimItem on 
				DimItem.ItemKey = PlaybookDataPoint.ItemKey
		 	join FactSalesForecast on 
		 		FactSalesForecast.AccountKey = PlaybookDataPoint.AccountKey 
		 		and FactSalesForecast.ItemKey = PlaybookDataPoint.ItemKey 
		 		and FactSalesForecast.InvoiceLineGroup1Key = PlaybookDataPoint.InvoiceLineGroup1Key 
		 		and FactSalesForecast.ForecastType = '+@ForecastType+'
		 	join PlaybookDataPointGroup on 
				PlaybookDataPointGroup.PlaybookDataPointGroupKey = PlaybookPriceProposal.PlaybookDataPointGroupKey
		 	join FactLastPriceAndCost on 
		 		FactLastPriceAndCost.AccountKey = PlaybookDataPoint.AccountKey 
		 		and FactLastPriceAndCost.ItemKey = PlaybookDataPoint.ItemKey 
		 		and FactLastPriceAndCost.InvoiceLineGroup1Key = PlaybookDataPoint.InvoiceLineGroup1Key
		 	join ATKProposalRecommendation ATKPR on 
				ATKPR.ProposalRecommendationKey = PlaybookPriceProposal.ProposalRecommendationKey
			left join FactCurrentPriceAndCost on
				FactCurrentPriceAndCost.AccountKey = PlaybookDataPoint.AccountKey
				and FactCurrentPriceAndCost.itemkey = PlaybookDataPoint.itemkey
				and FactCurrentPriceAndCost.invoicelinegroup1key = PlaybookDataPoint.invoicelinegroup1key
			'+@JoinClause2+'
		WHERE PlaybookPriceProposal.PlaybookDataPointGroupKey = '+CAST(@PlaybookDataPointGroupKey AS NVARCHAR(100))
			+'
			and PlaybookPriceProposal.ProposalRecommendationKey = '+CAST(@ProposalRecommendationKey AS NVARCHAR(100))+'
			and not exists (Select 1 from ProposalPrice where ProposalPrice.PlaybookPriceProposalKey = PlaybookPriceProposal.PlaybookPriceProposalKey)--To see if a proposal has already been created for this AIS
			'+@WhereClause2
		+ ' 
		Set @RowsI = @@RowCount
	
		exec LogDCPEvent '''+@txt+''', ''E'', @RowsI, @RowsU, @RowsD'
	
	
		--PRINT @SQLStmt
		EXEC dbo.PrintNVarcharMax @SQLStmt
	
		IF @Execute = 'Y'
		BEGIN
			EXEC (@SQLStmt)	
		END

	END
	ELSE
	BEGIN
		IF @Execute = 'Y'
		BEGIN
			SET @txt = 'DCP --- RBP_PopulatePlaybookPriceProposal:  Insert ProposalCost for AutoSage Pass: '+CAST(@Pass AS NVARCHAR(100))
			EXEC LogDCPEvent @txt, 'B',0,0,0,@ProjectKey

		END
	
		SET @SQLStmt = '

	
		declare @RowsI int, @RowsU int, @RowsD int
		select @RowsI = 0, @RowsU = 0, @RowsD = 0
	
		INSERT INTO ProposalCost
			(
			ProjectKey,
			PlaybookDataPointGroupKey,
			AccountKey,
			AccountManagerKey,
			ItemKey,
			InvoiceLineGroup1Key,
			PlaybookPriceProposalKey, 
			ProposedCost,
			NewCost, 
			AcceptanceCodeKey,
			UserAcceptanceCodeKey,
			DefaultAcceptanceCodeKey,
			TotalActual12MonthQuantity,
			TotalActual12MonthFrequency,
			TotalActual12MonthCost,
			TotalActual12MonthPrice,
			LastItemCost,
			LastItemPrice,
			CurrentItemAISCost,
			CurrentItemStandardCost,
			FutureItemCost,
			Forecasted12MonthQuantity,
			TotalForecasted12MonthPrice_LastPrice,
			TotalForecasted12MonthCost_LastCost,
			TotalForecasted12MonthCost_ProposedCost,
			TotalForecasted12MonthCost_NewCost,
			TotalForecasted12MonthCost_RelevantCost,
			AcceptanceStatusDate,
			LastSaleDayKey,
			LastInvoiceLineGroup2Key,
			ReasonCodeKey,
			SuppressionDate,
			EffectiveDate,
			UDVarchar1,
			UDVarchar2,
			--RelevantCost,
			UoMConversionFactor,
			UoM,
			ListPricingUoMConvFactor,
			ListPricingUoM
		)
		SELECT
			PlaybookDataPointGroup.ProjectKey,
			PlaybookPriceProposal.PlaybookDataPointGroupKey,
			PlaybookDataPoint.AccountKey,
			DimAccount.AccountManagerKey,
			PlaybookDataPoint.ItemKey,
			PlaybookDataPoint.InvoiceLineGroup1Key,
			PlaybookPriceProposal.PlaybookPriceProposalKey,
			PlaybookPriceProposal.ProposedCost,
			PlaybookPriceProposal.ProposedCost,
			ATKPR.AcceptRejectCode_PriceProposal,
			'+CAST(@DeferredCode AS NVARCHAR(500))+',
			ATKPR.AcceptRejectCode_PriceProposal,
			PlaybookDataPoint.Total12MonthQuantity  as ''TotalActual12MonthQuantity'',
			FactLastPriceAndCost.Total12MonthInvoiceLines as ''TotalActual12MonthFrequency'',
			PlaybookDataPoint.TotalActual12MonthCost as ''TotalActual12MonthCost'',
			PlaybookDataPoint.TotalActual12MonthPrice as ''TotalActual12MonthPrice'',
		
			PlaybookDataPoint.LastItemCost,
			PlaybookDataPoint.LastItemPrice,
			FactCurrentPriceAndCost.CurrentItemCost, 
			DimItem.CurrentCost,
			DimItem.FutureCost,
		
			(FactSalesForecast.ForecastedMonthlyQty*12) ,
			PlaybookDataPoint.LastItemPrice * FactSalesForecast.ForecastedMonthlyQty*12 as ''TotalForecasted12MonthPrice_LastPrice'',
			PlaybookDataPoint.LastItemCost * FactSalesForecast.ForecastedMonthlyQty*12 as ''TotalForecasted12MonthCost_LastCost'',
			PlaybookPriceProposal.ProposedCost * FactSalesForecast.ForecastedMonthlyQty*12 as ''TotalForecasted12MonthCost_ProposedCost'',
			PlaybookPriceProposal.ProposedCost * FactSalesForecast.ForecastedMonthlyQty*12 as ''TotalForecasted12MonthCost_NewCost'',
			IsNull(DimItem.FutureCost,IsNull(FactCurrentPriceAndCost.CurrentItemCost, PlaybookDataPoint.LastItemCost)) * FactSalesForecast.ForecastedMonthlyQty*12  as ''TotalForecasted12MonthCost_RelevantCost'',
		
			GetDate(),
			FactLastPriceAndCost.LastSaleDayKey,
			FactLastPriceAndCost.LastInvoiceLineGroup2Key,
			IsNull(ATKPR.DynamicReasonCode, ''NULL''),
			IsNull(ATKPR.DynamicSuppressionDate, getdate()),
			IsNull(ATKPR.DynamicEffectiveDate, getdate()),
			'''+@DynamicUDVarchar1+''',
			'''+@DynamicUDVarchar2+''',
			--IsNull(DimItem.FutureCost,IsNull(FactCurrentPriceAndCost.CurrentItemCost, PlaybookDataPoint.LastItemCost)),
			1.,
			DimItem.BaseUnitOfMeasure,
			1.,
			DimItem.BaseUnitOfMeasure
		FROM
			PlaybookPriceProposal
		 	join PlaybookDataPointOptimalPrice on 
				PlaybookDataPointOptimalPrice.PlaybookDataPointOptimalPriceKey = PlaybookPriceProposal.PlaybookDataPointOptimalPriceKey
		 	join PlaybookDataPointPricingGroup on 
				PlaybookDataPointPricingGroup.PlaybookDataPointPricingGroupKey = PlaybookDataPointOptimalPrice.PlaybookDataPointPricingGroupKey
			join PlaybookDataPoint on 
				PlaybookDataPoint.PlaybookDataPointKey = PlaybookDataPointPricingGroup.PlaybookDataPointKey 
				and PlaybookDataPoint.PlaybookDataPointGroupKey = PlaybookPriceProposal.PlaybookDataPointGroupKey
		 	join DimAccount on 
				DimAccount.AccountKey = PlaybookDataPoint.AccountKey
		 	join DimItem on 
				DimItem.ItemKey = PlaybookDataPoint.ItemKey
		 	join FactSalesForecast on 
		 		FactSalesForecast.AccountKey = PlaybookDataPoint.AccountKey 
		 		and FactSalesForecast.ItemKey = PlaybookDataPoint.ItemKey 
		 		and FactSalesForecast.InvoiceLineGroup1Key = PlaybookDataPoint.InvoiceLineGroup1Key 
		 		and FactSalesForecast.ForecastType = '+@ForecastType+'
		 	join PlaybookDataPointGroup on 
				PlaybookDataPointGroup.PlaybookDataPointGroupKey = PlaybookPriceProposal.PlaybookDataPointGroupKey
		 	join FactLastPriceAndCost on 
		 		FactLastPriceAndCost.AccountKey = PlaybookDataPoint.AccountKey 
		 		and FactLastPriceAndCost.ItemKey = PlaybookDataPoint.ItemKey 
		 		and FactLastPriceAndCost.InvoiceLineGroup1Key = PlaybookDataPoint.InvoiceLineGroup1Key
		 	join ATKProposalRecommendation ATKPR on 
				ATKPR.ProposalRecommendationKey = PlaybookPriceProposal.ProposalRecommendationKey
			left join FactCurrentPriceAndCost on
				FactCurrentPriceAndCost.AccountKey = PlaybookDataPoint.AccountKey
				and FactCurrentPriceAndCost.itemkey = PlaybookDataPoint.itemkey
				and FactCurrentPriceAndCost.invoicelinegroup1key = PlaybookDataPoint.invoicelinegroup1key
			'+@JoinClause3+'
		WHERE PlaybookPriceProposal.PlaybookDataPointGroupKey = '+CAST(@PlaybookDataPointGroupKey AS NVARCHAR(100))
			+'
			and not exists (Select 1 from ProposalCost where ProposalCost.PlaybookPriceProposalKey = PlaybookPriceProposal.PlaybookPriceProposalKey)--To see if a proposal has already been created for this AIS
			'+@WhereClause3
		+ ' 
		Set @RowsI = @@RowCount
	
		exec LogDCPEvent '''+@txt+''', ''E'', @RowsI, @RowsU, @RowsD'

	
		--PRINT @SQLStmt
		EXEC dbo.PrintNVarcharMax @SQLStmt
	
		IF @Execute = 'Y'
		BEGIN
			EXEC (@SQLStmt)
		END
	
	END
	
	--------------------------------------------------

	FETCH NEXT FROM CurAutoSagePass INTO @ProposalRecommendationKey, @Pass, @PriceIncreaseCapPercent, @MinPriceIncreaseThresholdPercent, @MinCostDecreaseThresholdPercent, @CostDecreaseCapPercent, @ProposedPriceOverride, @ProposedCostOverride, @DynamicUDVarchar1, @DynamicUDVarchar2
END

CLOSE CurAutoSagePass
DEALLOCATE CurAutoSagePass

DECLARE @USql NVARCHAR(MAX)
SET @USql = 'Update Statistics PlaybookPriceProposal'
EXEC (@USql)

SET @RowsI = 0
SET @RowsU = 0
SET @RowsD = 0



----------------------------------------------------
----------------------------------------------------
----------------------------------------------------
---------------NOW START THE DYNAMIC DELETE---------
----------------------------------------------------
----------------------------------------------------
----------------------------------------------------
IF @ApproachTypeDesc = 'Reality Based Pricing'
BEGIN
	--Now, we need to run the dynamic delete if one exists.
	--This can be used for things like deleting all proposals for account with <$X incr. benefit
	IF @DynamicDeleteWhere1 <> '' OR @DynamicDeleteJoin1 <> '' OR @DynamicDeleteGroupBy1 <> '' OR @DynamicDeleteHaving1 <> ''
	BEGIN
		IF @Execute = 'Y'	
		BEGIN
			SELECT @RowsI = 0, @RowsU = 0, @RowsD = 0
			EXEC LogDCPEvent 'DCP --- RBP_PopulatePlaybookPriceProposal:  Dynamic Delete', 'B', @RowsI, @RowsU, @RowsD
		END

		SET @SQLStmt = ''
		SET @SQLStmt =  'Delete from ProposalPrice '+CHAR(10)

		IF @DynamicDeleteJoin1 <> ''
		BEGIN
			SET @SQLStmt =  @SQLStmt + @DynamicDeleteJoin1 + CHAR(10)
		END
		SET @SQLStmt =  @SQLStmt + 'Where ProjectKey = '+ CAST(@ProjectKey AS NVARCHAR(50)) + CHAR(10)

		IF @DynamicDeleteWhere1 <> ''
		BEGIN
			SET @DynamicDeleteWhere1 = REPLACE(@DynamicDeleteWhere1, '@ProjectKey', @ProjectKey) --Put the project key in place of the variable
			SET @SQLStmt =  @SQLStmt + 'and ' + @DynamicDeleteWhere1 + CHAR(10)
		END

		IF @DynamicDeleteGroupBy1 <> ''
		BEGIN
			SET @SQLStmt =  @SQLStmt + 'Group By '+ @DynamicDeleteGroupBy1 + CHAR(10)
		END


		IF @DynamicDeleteHaving1 <> ''
		BEGIN
			SET @SQLStmt =  @SQLStmt + 'Having '+ @DynamicDeleteHaving1 + CHAR(10)
		END

		PRINT @SQLStmt
		IF @Execute = 'Y'
		BEGIN
			EXEC (@SQLStmt)	
			SET @RowsD = @@RowCount		
			EXEC LogDCPEvent 'DCP --- RBP_PopulatePlaybookPriceProposal:  Dynamic Delete', 'B', @RowsI, @RowsU, @RowsD
		END
	END	

	SET @USql = 'Update Statistics ProposalPrice'
	IF @Execute = 'Y'
		EXEC (@USql)

END
ELSE
BEGIN
	--Now, we need to run the dynamic delete if one exists.
	--This can be used for things like deleting all proposals for account with <$X incr. benefit
	IF @DynamicDeleteWhere1 <> '' OR @DynamicDeleteJoin1 <> '' OR @DynamicDeleteGroupBy1 <> '' OR @DynamicDeleteHaving1 <> ''
	BEGIN
		IF @Execute = 'Y'	
		BEGIN
			SELECT @RowsI = 0, @RowsU = 0, @RowsD = 0
			EXEC LogDCPEvent 'DCP --- RBP_PopulatePlaybookPriceProposal:  Dynamic Delete', 'B', @RowsI, @RowsU, @RowsD
		END
		SET @SQLStmt = ''
		SET @SQLStmt =  'Delete from ProposalCost '+CHAR(10)

		IF @DynamicDeleteJoin1 <> ''
		BEGIN
			SET @SQLStmt =  @SQLStmt + @DynamicDeleteJoin1 + CHAR(10)
		END
		SET @SQLStmt =  @SQLStmt + 'Where ProjectKey = '+CAST(@ProjectKey AS NVARCHAR(50))

		IF @DynamicDeleteWhere1 <> ''
		BEGIN
			SET @SQLStmt =  @SQLStmt + 'and ' + @DynamicDeleteWhere1 + CHAR(10)
		END

		IF @DynamicDeleteGroupBy1 <> ''
		BEGIN
			SET @SQLStmt =  @SQLStmt + 'Group By '+ @DynamicDeleteGroupBy1 + CHAR(10)
		END


		IF @DynamicDeleteHaving1 <> ''
		BEGIN
			SET @SQLStmt =  @SQLStmt + 'Having '+ @DynamicDeleteHaving1 + CHAR(10)
		END

		PRINT @SQLStmt
		IF @Execute = 'Y'
		BEGIN
			EXEC (@SQLStmt)	
			SET @RowsD = @@RowCount		
			EXEC LogDCPEvent 'DCP --- RBP_PopulatePlaybookPriceProposal:  Dynamic Delete', 'B', @RowsI, @RowsU, @RowsD
		END
		
	END	


	SET @USql = 'Update Statistics ProposalCost'
	IF @Execute = 'Y'
		EXEC (@USql)
END




----------------------------------------------------------------------------------------
IF @Execute = 'Y'
BEGIN

	EXEC LogDCPEvent 'DCP --- RBP_PopulatePlaybookPriceProposal: Insert ProjectAcountStatus', 'B',0,0,0,@ProjectKey
	SET @RowsI = 0
	SET @RowsU = 0
	SET @RowsD = 0

	
	IF @ApproachTypeDesc = 'Reality Based Pricing'
	BEGIN
		INSERT PlaybookProjectAccountStatus (ProjectKey, AccountKey, ProjectAccountStatus, ProjectAccountManagerStatus, AccountManagerKey)
		SELECT DISTINCT @ProjectKey, ProposalPrice.AccountKey, 'Not Reviewed', 'Not Finalized', DimAccount.AccountManagerKey
		FROM ProposalPrice
		JOIN DimAccount ON DimAccount.AccountKey = ProposalPrice.AccountKey
		WHERE PlaybookDataPointGroupKey = @PlaybookDataPointGroupKey
	END
	ELSE
	BEGIN
		INSERT PlaybookProjectAccountStatus (ProjectKey, AccountKey, ProjectAccountStatus, ProjectAccountManagerStatus, AccountManagerKey)
		SELECT DISTINCT @ProjectKey, ProposalCost.AccountKey, 'Not Reviewed', 'Not Finalized', DimAccount.AccountManagerKey
		FROM ProposalCost
		JOIN DimAccount ON DimAccount.AccountKey = ProposalCost.AccountKey
		WHERE PlaybookDataPointGroupKey = @PlaybookDataPointGroupKey
	END


	SET @RowsI = @RowsI + @@RowCount
	EXEC LogDCPEvent 'DCP --- RBP_PopulatePlaybookPriceProposal: Insert ProjectAcountStatus', 'E', @RowsI, @RowsU, @RowsD
	
	-----------------------------
	EXEC LogDCPEvent 'DCP --- RBP_PopulatePlaybookPriceProposal: Insert PlaybookProjectAccountManagerStatus ', 'B',0,0,0,@ProjectKey
	SET @RowsI = 0
	SET @RowsU = 0
	SET @RowsD = 0
	
	IF @ApproachTypeDesc = 'Reality Based Pricing'
	BEGIN
	
		INSERT PlaybookProjectAccountManagerStatus (ProjectKey, AccountManagerKey, ProjectAccountManagerStatus)
		SELECT @ProjectKey, ParentAccountManagerKey, 'Not Finalized' 
		FROM
		(SELECT DISTINCT ParentACcountManagerKey FROM BridgeACcountManager WHERE SubsidiaryACcountManagerKey IN 
		(
			SELECT DISTINCT AccountManagerKey 
			FROM ProposalPrice 
			WHERE ProjectKey = @ProjectKey
		)
		) A
		WHERE (SELECT COUNT(*) FROM ProposalPrice PP WHERE ProjectKey = @ProjectKey AND PP.AccountManagerKey IN (SELECT SubsidiaryAccountManagerKey FROM BridgeAccountManager WHERE ParentAccountManagerKey = A.ParentACcountManagerKey)) > 0--as TotalProposals
	END
	ELSE
	BEGIN
		INSERT PlaybookProjectAccountManagerStatus (ProjectKey, AccountManagerKey, ProjectAccountManagerStatus)
		SELECT @ProjectKey, ParentAccountManagerKey, 'Not Finalized' FROM
		(SELECT DISTINCT ParentACcountManagerKey FROM BridgeACcountManager WHERE SubsidiaryACcountManagerKey IN 
		(
			SELECT DISTINCT AccountManagerKey 
			FROM ProposalCost
			WHERE ProjectKey = @ProjectKey
		)
		) A
		WHERE (SELECT COUNT(*) FROM ProposalCost PP WHERE ProjectKey = @ProjectKey AND PP.AccountManagerKey IN (SELECT SubsidiaryAccountManagerKey FROM BridgeAccountManager WHERE ParentAccountManagerKey = A.ParentACcountManagerKey)) > 0--as TotalProposals

	END	
	SET @RowsI = @RowsI + @@RowCount
	EXEC LogDCPEvent 'DCP --- RBP_PopulatePlaybookPriceProposal: Insert PlaybookProjectAccountManagerStatus ', 'E', @RowsI, @RowsU, @RowsD
END














GO
