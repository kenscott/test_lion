SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO






CREATE     
PROCEDURE [dbo].[DCPLOG]
AS

SELECT 
	ProjectKey, 
	Minutes, 
	StepDesc, 
	BeginTime, 
	RowsInserted, 
	RowsUpdated, 
	RowsDeleted
FROM 
	logdcp_view WITH (NOLOCK)
ORDER BY 
	begintime DESC, 
	LogDCPID DESC







GO
