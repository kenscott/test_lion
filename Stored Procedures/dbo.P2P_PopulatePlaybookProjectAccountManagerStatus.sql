SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO












CREATE                                            
PROCEDURE [dbo].[P2P_PopulatePlaybookProjectAccountManagerStatus]
	@ProjectKey Key_Normal_type, 
	@PlaybookDataPointGroupKey Key_Normal_type
AS

SET NOCOUNT ON


DECLARE
	@RowsI Quantity_Normal_type, 
	@RowsU Quantity_Normal_type, 
	@RowsD Quantity_Normal_type


SET @RowsI = 0
SET @RowsU = 0
SET @RowsD = 0




EXEC LogDCPEvent 'DCP - P2P_PopulatePlaybookProjectAccountManagerStatus: Insert PlaybookProjectAccountManagerStatus ', 'B', 0, 0, 0, @ProjectKey

SET @RowsI = 0
SET @RowsU = 0
SET @RowsD = 0

INSERT PlaybookProjectAccountManagerStatus 
	(
	ProjectKey, 
	AccountManagerKey, 
	ProjectAccountManagerStatus
	) 
SELECT DISTINCT 
	@ProjectKey AS ProjectKey,
	ParentAccountManagerKey,
	'Not Finalized' AS ProjectAccountManagerStatus
FROM dbo.BridgeAccountManager bam 
INNER JOIN dbo.ProposalPrice pp ON
	pp.ProjectKey = @ProjectKey AND
	pp.AccountManagerKey = bam.SubsidiaryAccountManagerKey
SET @RowsI = @RowsI + @@ROWCOUNT

-- 
-- 	SELECT @ProjectKey, 
-- 		ParentAccountManagerKey, 
-- 		'Not Finalized' 
-- 	FROM
-- 		(
-- 		SELECT DISTINCT ParentAccountManagerKey 
-- 		FROM   BridgeAccountManager 
-- 		WHERE  SubsidiaryAccountManagerKey IN 
-- 			(
-- 			SELECT DISTINCT AccountManagerKey 
-- 			FROM   ProposalPrice 
-- 			WHERE  ProjectKey = @ProjectKey
-- 			)
-- 		) A 
-- 	WHERE 
-- 		(
-- 		SELECT COUNT(* ) 
-- 		FROM   ProposalPrice PP 
-- 		WHERE  
-- 			ProjectKey = @ProjectKey AND 
-- 			PP.AccountManagerKey IN 
-- 				(
-- 				SELECT SubsidiaryAccountManagerKey 
-- 				FROM   BridgeAccountManager 
-- 				WHERE  ParentAccountManagerKey = A.ParentAccountManagerKey
-- 				)
-- 		) > 0 											-- as TotalProposals
-- 	SET @RowsI = @RowsI + @@ROWCOUNT

	
EXEC LogDCPEvent 'DCP - P2P_PopulatePlaybookProjectAccountManagerStatus: Insert PlaybookProjectAccountManagerStatus ', 'E', @RowsI, @RowsU, @RowsD, @ProjectKey






















GO
