SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE PROCEDURE [lion].[Load_DimAccountGroup2]
AS

--This is used to look up the date key in the DimDay table
EXEC LogDCPEvent 'ETL - DimAccountGroup2', 'B'

EXEC DBA_IndexRebuild 'DimAccountGroup2', 'Drop'

--This table will contain a row for every row in the dimension table that has been changed in the staging table (type 1 update will be done)
CREATE TABLE #UpdateKeys (KeyID INTEGER)


DECLARE @RowsI Quantity_Normal_type, @RowsU Quantity_Normal_type, @RowsD Quantity_Normal_type
SET @RowsI = 0
SET @RowsU = 0
SET @RowsD = 0

DECLARE @TodayDayKey Key_Small_Type
SET @TodayDayKey = (SELECT DayKey FROM DimDay WHERE CONVERT(VARCHAR(10), SQLDate, 120) = CONVERT(VARCHAR(10), GETDATE(), 120))

-- TRUNCATE TABLE DimAccountGroup2

IF NOT EXISTS (SELECT 1 FROM DimAccountGroup2 WHERE AG2Level1 = 'Unknown')
BEGIN
	INSERT DimAccountGroup2 (
		AG2Level1, 
		AG2Level2, 
		AG2Level3, 
		AG2Level4, 
		AG2Level5, 
		AG2Level1UDVarchar1, 
		CreationDayKey, 
		ModificationDayKey)
	VALUES (
		'Unknown', 
		'Unknown',
		'Unknown',
		'Unknown',
		'Unknown', 
		'Unknown Customer Type', 
		@TodayDayKey, 
		@TodayDayKey)
	SET @RowsI = @RowsI + @@RowCount
END




INSERT DimAccountGroup2 (
	AG2Level1, 
	AG2Level2, 
	AG2Level3, 
	AG2Level4, 
	AG2Level5, 
	AG2Level1UDVarchar1, 
	CreationDayKey, 
	ModificationDayKey)
SELECT DISTINCT 
	c.CustomerType, 
	'None',
	'None',
	'None',
	'None',
	--c.CustomerTypeDescription,
	CASE c.CustomerType
		WHEN 'BD' THEN 'BAD DEBTS RECOVERED'
		WHEN 'BM' THEN 'BRANCH MANAGERS'
		WHEN 'CS' THEN 'CASH SALES'
		WHEN 'HQ' THEN 'HEADQUARTERS'
		WHEN 'IN' THEN 'INTEREST'
		WHEN 'LC' THEN 'LEGAL COSTS RECOVERED'
		WHEN 'PC' THEN 'Proclub'
		WHEN 'ST' THEN 'STANDARD TRADE'
		ELSE c.CustomerType
	END,
	@TodayDayKey, 
	@TodayDayKey
FROM Lion_Staging.dbo.Customer c
WHERE 
	c.CustomerType IS NOT NULL 
	AND c.CustomerType <> ''
	AND c.CustomerType NOT IN (SELECT AG2Level1 FROM DimAccountGroup2)	--INSERTs all new dimension rows








----Compare the checksum of the current dimension data to the checksum of the staging data
----IF we find any checksums that do not match, we will update those rows
--INSERT INTO #UpdateKeys (KeyID)
--SELECT DimAccountGroup2.AccountGroup2Key
--FROM 
--	(SELECT DISTINCT 
--		GroupLevel5 AS AccountProductType,
--		CASE LTRIM(RTRIM(GroupLevel5))
--			WHEN '1' THEN 'Printing'
--			WHEN '2' THEN 'Facility Supplies'
--			WHEN '3' THEN 'Packaging'
--			WHEN '4' THEN 'Graphics'
--			ELSE 'Unknown Account Product Type (' + ISNULL(GroupLevel5, 'NIL') + ')'
--		END AS AccountProductTypeDescription
--	FROM lion_Staging..Account 
--	WHERE GroupLevel5 IS NOT NULL AND GroupLevel5 <> '') StagingAG2
--JOIN DimAccountGroup2 ON DimAccountGroup2.AG2Level1 = StagingAG2.AccountProductType
--WHERE 
--	CheckSum(--CheckSum for dimension table.  This needs to check all rows that we will update IF a find a change
--	DimAccountGroup2.AG2Level1, DimAccountGroup2.AG2Level1UDVarchar1)
--<>
--	CheckSum(--CheckSum for staging table
--	StagingAG2.AccountProductType, StagingAG2.AccountProductTypeDescription)


----This will perform the update on all Account Managers that changed
--UPDATE DimAccountGroup2 SET 
--	AG2Level1 = AccountProductType,
--	AG2Level1UDVarchar1 = AccountProductTypeDescription,
--	ModificationDayKey= @TodayDayKey
--FROM (
--	SELECT DISTINCT
--		GroupLevel5 AS AccountProductType,
--		CASE LTRIM(RTRIM(GroupLevel5))
--			WHEN '1' THEN 'Printing'
--			WHEN '2' THEN 'Facility Supplies'
--			WHEN '3' THEN 'Packaging'
--			WHEN '4' THEN 'Graphics'
--			ELSE 'Unknown Account Product Type (' + ISNULL(GroupLevel5, 'NIL') + ')'
--		END AS AccountProductTypeDescription
--	FROM lion_Staging..Account 
--	WHERE GroupLevel5 IS NOT NULL AND GroupLevel5 <> '') StagingAG2
--WHERE StagingAG2.AccountProductType = DimAccountGroup2.AG2Level1
--AND DimAccountGroup2.AccountGroup2Key in (SELECT KeyID FROM #UpdateKeys)

--SET @RowsU = @@RowCOunt + @RowsU


--INSERT DimAccountGroup2 (
--	AG2Level1, 
--	AG2Level2, 
--	AG2Level3, 
--	AG2Level4, 
--	AG2Level5, 
--	AG2Level1UDVarchar1, 
--	CreationDayKey, 
--	ModificationDayKey)
--SELECT DISTINCT 
--	GroupLevel5 AS AccountProductType, 
--	'None',
--	'None',
--	'None',
--	'None',
--	CASE LTRIM(RTRIM(GroupLevel5))
--		WHEN '1' THEN 'Printing'
--		WHEN '2' THEN 'Facility Supplies'
--		WHEN '3' THEN 'Packaging'
--		WHEN '4' THEN 'Graphics'
--		ELSE 'Unknown Account Product Type (' + ISNULL(GroupLevel5, 'NIL') + ')'
--	END AS AccountProductTypeDescription,
--	@TodayDayKey, 
--	@TodayDayKey
--FROM lion_Staging..Account
--WHERE GroupLevel5 IS NOT NULL AND GroupLevel5 <> ''
--AND GroupLevel5 NOT IN (SELECT AG2Level1 FROM DimAccountGroup2)	--INSERTs all new dimension rows

--SET @RowsI = @RowsI + @@RowCount


SELECT TOP 10 * FROM DimAccountGroup2

--DROP TABLE #UpdateKeys

EXEC DBA_IndexRebuild 'DimAccountGroup2', 'Create'
EXEC LogDCPEvent 'ETL - DimAccountGroup2', 'E', @RowsI, @RowsU, @RowsD


GO
