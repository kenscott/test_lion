SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [lion].[Load_DimAccount]
WITH EXECUTE AS CALLER
AS
SET NOCOUNT ON
	
EXEC LogDCPEvent 'ETL - Load_DimAccount', 'B'


DECLARE
	@NullAccountManagerKey Key_Normal_type,
	@NullAccountGroup1Key Key_Normal_type,
	@NullAccountGroup2Key Key_Normal_type,
	@NullAccountGroup3Key Key_Normal_type,
	@NullAccountGroup4Key Key_Normal_type,
	@NullBrandKey Key_Small_type,
	@TodayDayKey Key_Small_Type,

	@RowsI Quantity_Normal_type, 
	@RowsU Quantity_Normal_type, 
	@RowsD Quantity_Normal_type,
	@BeginRowCount INT,

	@MinDayKey INT,
	@MaxDayKey INT,
	@MinDate DATE,
	@MaxDate DATE

	
SET @RowsI = 0
SET @RowsU = 0
SET @RowsD = 0

--This is used to look up the date key in the DimDay table
SET @TodayDayKey = (SELECT DayKey FROM dbo.DimDay WHERE CONVERT(VARCHAR(10), SQLDate, 120) = CONVERT(VARCHAR(10), GETDATE(), 120))
SET @NullAccountGroup1Key = (SELECT AccountGroup1Key FROM dbo.DimAccountGroup1 WHERE AG1Level1 = N'Unknown')
SET @NullAccountGroup2Key = (SELECT AccountGroup2Key FROM dbo.DimAccountGroup2 WHERE AG2Level1 = N'Unknown')
SET @NullAccountGroup3Key = (SELECT AccountGroup3Key FROM dbo.DimAccountGroup3 WHERE AG3Level1 = N'Unknown')
SELECT @NullAccountGroup4Key = ISNULL( (SELECT AccountGroup4Key FROM dbo.DimAccountGroup4 WHERE AG4Level1 = N'Unknown') , 1)
SELECT @NullBrandKey = ISNULL( (SELECT BrandKey FROM dbo.DimBrand WHERE Brand = N'Unknown') , 1)


SET @NullAccountManagerKey = (SELECT AccountManagerKey FROM dbo.DimAccountManager WHERE AccountManagerCode = 'David.Boothman')
IF @NullAccountManagerKey IS NULL
BEGIN
	SET @NullAccountManagerKey = (SELECT MIN(AccountManagerKey) FROM dbo.DimAccountManager WHERE AccountManagerCode IN ('0', 'scott.toolson'))	-- should be coordinator
END
IF @NullAccountManagerKey IS NULL
BEGIN
	SET @NullAccountManagerKey = (SELECT AccountManagerKey FROM dbo.DimAccountManager WHERE DimPersonKey = (SELECT DimPersonKey FROM DimPerson WHERE FullName = N'Unknown'))
END



SET @MaxDayKey = (
	SELECT dd.DayKey
	FROM   
	(
		SELECT 
			MAX(CAST(TransactionDate AS DATETIME)) AS InvoiceDate
		FROM Lion_Staging.dbo.Transactions
		WHERE ETLRowStatusCodeKey = 0
		--WHERE ISDATE(DeliveryDate) = 1
	) il
	INNER JOIN DimDay dd 
		ON CONVERT(VARCHAR(10), dd.SQLDate, 120) = CONVERT(VARCHAR(10), il.InvoiceDate, 120)
	)

SET @MinDayKey = @MaxDayKey - 364 + 1

SET @MinDate = (SELECT SQLDate FROM dbo.DimDay WHERE DayKey = @MinDayKey)
SET @MaxDate = (SELECT SQLDate FROM dbo.DimDay WHERE DayKey = @MaxDayKey)


SELECT @BeginRowCount = COUNT(*) FROM dbo.DimAccount


--EXEC DBA_IndexRebuild 'DimAccount', 'Drop_NoClustered'


--TRUNCATE TABLE DimAccount



IF NOT EXISTS (SELECT 1 FROM dbo.DimAccount WHERE AccountClientUniqueIdentifier = N'Unknown')
BEGIN
	INSERT dbo.DimAccount (
		AccountManagerKey,
		AccountClientUniqueIdentifier,
		AccountNumber,
		AccountName,
		AccountGroup1Key,
		AccountGroup2Key,
		AccountGroup3Key,
		AccountGroup4Key,
		AccountCity,
		AccountState,
		AccountAddress1,
		AccountZipCode,
		CreationDayKey,
		ModificationDayKey,
		AccountUDVarChar1,
		AccountUDVarChar2,
		AccountUDVarChar3
	)
	VALUES (
		@NullAccountManagerKey,
		N'Unknown',
		N'Unknown',
		N'Unknown',
		@NullAccountGroup1Key,
		@NullAccountGroup2Key,
		@NullAccountGroup3Key,
		@NullAccountGroup4Key,
		N'Unknown',
		N'Unknown',
		N'Unknown',
		N'Unknown',
		@TodayDayKey,
		@TodayDayKey,
		N'Unknown',
		N'Unknown',
		N'Unknown'
	)
	SET @RowsI = @RowsI + @@RowCount
END



/*** Create list of account brand owners ***/
/* DROP TABLE #AccountBrandManager */
/* DROP TABLE #AccountManager */
CREATE TABLE #AccountBrandManager (
	AccountNumber NVARCHAR(4000) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	Brand NVARCHAR(4000) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	BrandOwner NVARCHAR(4000) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	BrandKey SMALLINT NOT NULL,
	CONSTRAINT [PK_TempAccountBrandManager] PRIMARY KEY CLUSTERED 
	(
		AccountNumber,
		Brand
	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

; WITH AccountOwnerData AS (
	SELECT
		AccountNumber,
		LTRIM(RTRIM(ISNULL(PlumbOwnerEmail, NationalOwnerEmail))) AS PlumbOwnerEmail, 
		LTRIM(RTRIM(PartsOwnerEmail)) AS PartsOwnerEmail, 
		LTRIM(RTRIM(DrainOwnerEmail)) AS DrainOwnerEmail, 
		--[P&CBrandOwnerEmail], 
		LTRIM(RTRIM(PipeOwnerEmail)) AS PipeOwnerEmail,
		LTRIM(RTRIM(ClimateOwnerEmail)) AS ClimateOwnerEmail,
		LTRIM(RTRIM(BrandOwnerBurdensEmail)) AS BrandOwnerBurdensEmail,
		LTRIM(RTRIM(FusionOwnerEmail)) AS FusionOwnerEmail,
		LTRIM(RTRIM(UPSOwnerEmail)) AS UPSOwnerEmail
	FROM Lion_Staging.dbo.AccountOwner
), AccountOwnerDataPivot AS (
	SELECT 
		AccountNumber, 
		BrandType, 
		OwnerValue
	FROM AccountOwnerData
	UNPIVOT (OwnerValue FOR BrandType IN ( PlumbOwnerEmail, PartsOwnerEmail, DrainOwnerEmail, PipeOwnerEmail, ClimateOwnerEmail, BrandOwnerBurdensEmail, FusionOwnerEmail, UPSOwnerEmail)) AS UnPivotTable
)
, TransformedData AS (
	SELECT DISTINCT
		AccountNumber, 
		CASE BrandType
			WHEN 'NationalOwnerEmail' THEN 'PLUMB'
			WHEN 'PlumbOwnerEmail' THEN 'PLUMB'
			WHEN 'PartsOwnerEmail' THEN 'PARTS'
			WHEN 'DrainOwnerEmail' THEN 'DRAIN'
			WHEN 'PipeOwnerEmail' THEN 'PIPE'
			WHEN 'ClimateOwnerEmail' THEN 'CLIMT'
			--WHEN 'P&CBrandOwnerEmail' THEN 'PIPE'
			WHEN 'BrandOwnerBurdensEmail'  THEN 'BURDN'
			WHEN 'FusionOwnerEmail'  THEN 'FUSN'
			WHEN 'UPSOwnerEmail'  THEN 'UPS'
			ELSE NULL
		END AS Brand,
		OwnerValue
	FROM AccountOwnerDataPivot
	WHERE 
		ISNULL(OwnerValue, N'') <> N''
), TransformedDataRanked AS (
	SELECT
		AccountNumber, 
		Brand, 
		OwnerValue,
		ROW_NUMBER() OVER (
			PARTITION BY 
				AccountNumber,
				Brand
			ORDER BY 
				OwnerValue DESC
		) AS RowRank
	FROM TransformedData
)
INSERT #AccountBrandManager (
	AccountNumber,
	Brand,
	BrandOwner,
	BrandKey
)
SELECT
	d.AccountNumber, 
	d.Brand, 
	d.OwnerValue,
	db.BrandKey
FROM TransformedDataRanked d
INNER JOIN dbo.DimBrand db 
	ON db.Brand = d.Brand
WHERE RowRank = 1


/* with the account brand owner file having taken precedence, now look at terms owner and user hierarchy files */
/* first, we need to get some sales numbers for the accounts */
SELECT
	t.AccountId,
	t.BranchCode,
	SUM(LinePrice) AS TotalSales,
	COUNT(*) AS TransactionCount,
	SUM(SUM(LinePrice)) OVER ( PARTITION BY t.AccountId ) AS AccountSales
INTO #AccountBranchSales
FROM Lion_Staging.dbo.Transactions t
WHERE
	t.ETLRowStatusCodeKey = 0
	AND t.TransactionDate BETWEEN @MinDate AND @MaxDate
GROUP BY
	t.AccountId,
	t.BranchCode

;
WITH TermsOwnerData AS (
SELECT
	ROW_NUMBER() OVER (
		PARTITION BY 
			termO.AccountId, 
			uh.[Level0 Brand] 
		ORDER BY 
			uh.[Level0 Branch] ASC,							-- Destination vs. Standard branch
			BranchSales.TotalSales DESC,
			BranchSales.TransactionCount DESC,
			termO.TermsOwner DESC,							-- matches TermsOwnerBranchSelected below
			CASE 
				WHEN [Level0 Branch] = 'Branch Manager' THEN 1
				ELSE 2
			END,
			[Level0 Email Address] COLLATE SQL_Latin1_General_CP1_CS_AS DESC
			) AS RowRank,
	termO.AccountId AS AccountNumber,
	uh.[Level0 Brand] AS Brand,
	LTRIM(RTRIM(uh.[Level0 Email Address])) AS BrandOwner,
	db.BrandKey
FROM Lion_Staging.dbo.TermsOwner termO
INNER JOIN Lion_Staging.dbo.UserHierarchy uh
	ON uh.[Level1 Assignment] = termO.TermsOwner
INNER JOIN dbo.DimBrand db 
	ON db.Brand = uh.[Level0 Brand]
LEFT JOIN #AccountBranchSales BranchSales 
	ON BranchSales.AccountId = termO.AccountID 
	AND BranchSales.BranchCode = termO.TermsOwner
WHERE
	uh.[Level0 Email Address] IS NOT NULL
)
INSERT #AccountBrandManager (
	AccountNumber,
	Brand,
	BrandOwner,
	BrandKey
)
SELECT DISTINCT
	AccountNumber, 
	Brand, 
	BrandOwner,
	BrandKey
FROM TermsOwnerData
WHERE 
	RowRank = 1
	AND NOT EXISTS (SELECT 1 FROM #AccountBrandManager dabm WHERE dabm.AccountNumber = TermsOwnerData.AccountNumber AND dabm.Brand = TermsOwnerData.Brand)


-- LPF-2573 PF-476, 477 Modify Account Brand Owner Logic to Route Parts/Drain SPGs to Plumb
INSERT #AccountBrandManager (
	AccountNumber,
	Brand,
	BrandOwner,
	BrandKey
)
SELECT DISTINCT
	abm.AccountNumber,
	'PARTS' AS Brand,
	abm.BrandOwner,
	db.BrandKey
FROM #AccountBrandManager abm
LEFT JOIN #AccountBrandManager abmParts
	ON abm.AccountNumber = abmParts.AccountNumber
	AND abmParts.Brand = 'PARTS'
INNER JOIN dbo.DimBrand db 
	ON db.Brand = 'PARTS'
WHERE 
	abm.Brand = 'PLUMB'
	AND abmParts.AccountNumber IS NULL
	AND NOT EXISTS (SELECT 1 FROM #AccountBrandManager dabm WHERE dabm.AccountNumber = abm.AccountNumber AND dabm.Brand = 'PARTS')

INSERT #AccountBrandManager (
	AccountNumber,
	Brand,
	BrandOwner,
	BrandKey
)
SELECT DISTINCT
	abm.AccountNumber,
	'DRAIN' AS Brand,
	abm.BrandOwner,
	db.BrandKey
FROM #AccountBrandManager abm
LEFT JOIN #AccountBrandManager abmParts
	ON abm.AccountNumber = abmParts.AccountNumber
	AND abmParts.Brand = 'DRAIN'
INNER JOIN dbo.DimBrand db 
	ON db.Brand = 'DRAIN'
WHERE 
	abm.Brand = 'PLUMB'
	AND abmParts.AccountNumber IS NULL
	AND NOT EXISTS (SELECT 1 FROM #AccountBrandManager dabm WHERE dabm.AccountNumber = abm.AccountNumber AND dabm.Brand = 'DRAIN')

-- LPF-3353 Modify Account Brand Owner Logic to Route Climate(CLIMT) SPGs to Pipe (PIPE)
INSERT #AccountBrandManager (
	AccountNumber,
	Brand,
	BrandOwner,
	BrandKey
)
SELECT DISTINCT
	abm.AccountNumber,
	'CLIMT' AS Brand,
	abm.BrandOwner,
	db.BrandKey
FROM #AccountBrandManager abm
INNER JOIN dbo.DimBrand db 
	ON db.Brand = 'CLIMT'
WHERE 
	abm.Brand = 'PIPE'
	AND NOT EXISTS (SELECT 1 FROM #AccountBrandManager dabm WHERE dabm.AccountNumber = abm.AccountNumber AND dabm.Brand = 'CLIMT')

-- LPF-4938 Modify Account Brand Owner Logic to Route Burdens SPGs to Fusion
INSERT #AccountBrandManager (
	AccountNumber,
	Brand,
	BrandOwner,
	BrandKey
)
SELECT DISTINCT
	abm.AccountNumber,
	'BURDN' AS Brand,
	abm.BrandOwner,
	db.BrandKey
FROM #AccountBrandManager abm
INNER JOIN dbo.DimBrand db 
	ON db.Brand = 'BURDN'
WHERE 
	abm.Brand = 'FUSN'
	AND NOT EXISTS (SELECT 1 FROM #AccountBrandManager dabm WHERE dabm.AccountNumber = abm.AccountNumber AND dabm.Brand = 'BURDN')

-- LPF-5154 Route UPS to Burdn also
INSERT #AccountBrandManager (
	AccountNumber,
	Brand,
	BrandOwner,
	BrandKey
)
SELECT DISTINCT
	abm.AccountNumber,
	'BURDN' AS Brand,
	abm.BrandOwner,
	db.BrandKey
FROM #AccountBrandManager abm
INNER JOIN dbo.DimBrand db 
	ON db.Brand = 'BURDN'
WHERE 
	abm.Brand = 'UPS'
	AND NOT EXISTS (SELECT 1 FROM #AccountBrandManager dabm WHERE dabm.AccountNumber = abm.AccountNumber AND dabm.Brand = 'BURDN')


; WITH AccountBrandSales AS (
	SELECT
		t.AccountId,
		p.ProdBrandAtLLSPG AS Brand,		
		SUM(LinePrice) AS TotalSales,
		COUNT(*) AS TransactionCount
	FROM Lion_Staging.dbo.Transactions t
	INNER JOIN Lion_Staging.dbo.Product p
		ON p.PYRCode = t.PyramidCode
		AND p.ProductCode = t.ProductCode
	WHERE
		t.ETLRowStatusCodeKey = 0
		AND t.TransactionDate BETWEEN @MinDate AND @MaxDate
	GROUP BY
		t.AccountId,
		p.ProdBrandAtLLSPG
), AccountBrandSalesRanked AS (
	SELECT
		AccountId,
		Brand,
		ROW_NUMBER() OVER (
			PARTITION BY 
				AccountId 
			ORDER BY 
				TotalSales DESC, 
				TransactionCount DESC, 
				Brand ASC
		) AS RowRank
	FROM AccountBrandSales
), AccountBrandSalesTop AS (
	SELECT 
		AccountId,
		Brand
	FROM AccountBrandSalesRanked
	WHERE AccountBrandSalesRanked.RowRank = 1
)
SELECT
	abm.AccountNumber,
	abm.Brand,
	abm.BrandOwner
INTO #AccountManager
FROM #AccountBrandManager abm
INNER JOIN AccountBrandSalesTop sales
	ON sales.AccountID = abm.AccountNumber
	AND sales.Brand = abm.Brand

CREATE  UNIQUE CLUSTERED INDEX I_1 ON #AccountManager(AccountNumber)


--drop table #TermsOwnerBranchSelected
; WITH AccountBranchBrandList AS (
	SELECT DISTINCT	
		tOwner.AccountId, 
		TermsOwner AS TermsOwnerBranch,
		dag1TOwner.AG1Level1UDVarchar6 AS TermsOwnerBranchBrand,
		dag1TOwner.AccountGroup1Key
	FROM Lion_Staging.dbo.TermsOwner tOwner
	INNER JOIN dbo.DimAccountGroup1 dag1TOwner
		ON dag1TOwner.AG1Level1 = tOwner.TermsOwner
), AccountSalesByBranchBrand AS (
	SELECT
		t.AccountId,
		dag1.AG1Level1UDVarchar6 AS SalesBranchBrand,
		COUNT(*) AS TransactionCount,
		SUM(LinePrice) AS TotalSales
	FROM Lion_Staging.dbo.Transactions t
	INNER JOIN dbo.DimAccountGroup1 dag1
		ON dag1.AG1Level1 = t.BranchCode 
	WHERE
		t.ETLRowStatusCodeKey = 0
		AND t.TransactionDate BETWEEN @MinDate AND @MaxDate
	GROUP BY
		t.AccountId,
		dag1.AG1Level1UDVarchar6
), AccountBranchWithRank AS (
	SELECT
		abbl.TermsOwnerBranch,
		abbl.AccountGroup1Key,
		sales.*,
		ROW_NUMBER() OVER (
			PARTITION BY 
				sales.AccountId 
			ORDER BY 
				TotalSales DESC, 
				TransactionCount DESC, 
				TermsOwnerBranch DESC
	) AS RowRank
	FROM AccountSalesByBranchBrand sales
	INNER JOIN AccountBranchBrandList abbl
		ON abbl.AccountId = sales.AccountId
		AND abbl.TermsOwnerBranchBrand = sales.SalesBranchBrand
)
SELECT
	*
INTO #TermsOwnerBranchSelected
FROM AccountBranchWithRank
WHERE RowRank = 1

CREATE  UNIQUE CLUSTERED INDEX I_1 ON #TermsOwnerBranchSelected(AccountId)


; WITH a AS (
	SELECT DISTINCT
		AccountId,
		TermsParent,
		PricingCategory,
		ROW_NUMBER() OVER (
			PARTITION BY 
				AccountId 
			ORDER BY 
				TermsParent, 
				PricingCategory
		) AS RowRank
	FROM Lion_Staging.dbo.TermsOwner
)
SELECT
	AccountId,
	TermsParent,
	PricingCategory
INTO #TermsOwner
FROM a
WHERE
	RowRank = 1

CREATE  UNIQUE CLUSTERED INDEX I_1 ON #TermsOwner(AccountId)


;
WITH cs AS (
	SELECT DISTINCT 
		AccountNumberParent,
		cp.Specialism AS CustomerSpecialism,
		ROW_NUMBER() OVER (
			PARTITION BY 
				AccountNumberParent 
			ORDER BY 
				specialism
		) AS RowRank
	FROM 
		Lion_Staging.dbo.CustomerSpecialism cp
	INNER JOIN dbo.DimAccount da WITH (NOLOCK)
		ON da.AccountNumber = cp.account_number
)
SELECT
	AccountNumberParent,
	CustomerSpecialism
INTO #CustomerSpecialism
FROM cs
WHERE 
	RowRank = 1

CREATE UNIQUE CLUSTERED INDEX I_1 ON #CustomerSpecialism (AccountNumberParent)




; WITH StagingData AS (
	SELECT
		c.AccountID AS AccountClientUniqueIdentifier,
		c.AccountID AS AccountNumber,
		ISNULL(NULLIF(tOwn.TermsParent, ''), c.AccountID) AS AccountNumberParent,		
		c.CustomerName AS AccountName,
		COALESCE(
			CASE
				WHEN TermsOwnerCount.TOCount IS NULL OR TermsOwnerCount.TOCount <= 0 THEN DAG1SB.AccountGroup1Key
				WHEN TermsOwnerCount.TOCount = 1 THEN DAG1TOB.AccountGroup1Key
				WHEN TermsOwnerCount.TOCount > 1 THEN TermsOwnerBranchSelected.AccountGroup1Key
				ELSE NULL
			END, 
			DAG1TOB.AccountGroup1Key, 
			DAG1SB.AccountGroup1Key, 
			@NullAccountGroup1Key) AS AccountGroup1Key,
		ISNULL(DAG2.AccountGroup2Key, @NullAccountGroup2Key) AS AccountGroup2Key,
		ISNULL(DAG3.AccountGroup3Key, @NullAccountGroup3Key) AS AccountGroup3Key,
		@NullAccountGroup4Key AS AccountGroup4Key,

		LTRIM(RTRIM(ISNULL(c.AddressLine1, N''))) AS AccountAddress1,
		LTRIM(RTRIM(ISNULL(c.AddressLine2, N''))) AS AccountAddress2,
		LTRIM(RTRIM(ISNULL(c.AddressLine3, N''))) AS AccountAddress3,
		LTRIM(RTRIM(ISNULL(c.AddressLine4, N''))) AS AccountAddress4,
		CASE	
			WHEN ISNULL(c.AddressLine4, N'') IN (N'', N'.')  AND ISNULL(c.AddressLine3, N'') IN (N'', N'.') THEN N''
			WHEN ISNULL(c.AddressLine4, N'') IN (N'', N'.') THEN ISNULL(c.AddressLine2, N'')
			ELSE ISNULL(c.AddressLine3, N'')
		END AS AccountCity,
		CASE	
			WHEN ISNULL(c.AddressLine4, N'') IN (N'', '.')  AND ISNULL(c.AddressLine3, N'') IN (N'', N'.') THEN ISNULL(c.AddressLine2, N'') 
			WHEN ISNULL(c.AddressLine4, N'') IN (N'', '.') THEN ISNULL(c.AddressLine3, N'')
			ELSE ISNULL(c.AddressLine4, N'')
		END AS AccountState,
		c.PostCode AS AccountZipCode,
		
		c.CreditLimit         AS AccountUDVarChar1,						-- CreditLimit
		c.PaymentTermName     AS AccountUDVarChar2,						-- PaymentTermName
		c.IntercompanyAccount AS AccountUDVarChar3,						-- IntercompanyAccount
		
		c.OverLimitStatus	  AS AccountUDVarChar4,					-- OverLimitStatus
		c.OverdueStatus		  AS AccountUDVarChar5,					-- OverdueStatus

		tOwn.TermsParent AS AccountUDVarChar6,
		tOwn.PricingCategory AS AccountUDVarChar7,
		
		CASE
			WHEN LTRIM(RTRIM(ISNULL(AccountOwner.[NationalOwner], N''))) <> N'' 
				--OR LTRIM(RTRIM(ISNULL(PCowner.[Brand Owner P & C: Title], N''))) LIKE N'%National%'
				THEN N'National'
			WHEN 
				LTRIM(RTRIM(ISNULL(AccountOwner.[PlumbOwner], N''))) <> N'' 
				OR LTRIM(RTRIM(ISNULL(AccountOwner.[PartsOwner], N''))) <> N'' 
				OR LTRIM(RTRIM(ISNULL(AccountOwner.[PipeOwner], N''))) <> N'' 
				OR LTRIM(RTRIM(ISNULL(AccountOwner.[ClimateOwner], N''))) <> N'' 
				--OR LTRIM(RTRIM(ISNULL(AccountOwner.[P&CBrandOwner], N''))) <> N'' 
				OR LTRIM(RTRIM(ISNULL(AccountOwner.[DrainOwner], N''))) <> N'' 
				OR LTRIM(RTRIM(ISNULL(AccountOwner.[BrandOwnerBurdens], N''))) <> N'' 
				--OR LTRIM(RTRIM(ISNULL(PCowner.[Brand Owner P & C: Title], N''))) <> N'' 
				THEN N'Sales Team'
			ELSE N''
		END AS AccountUDVarChar8,					-- OwnerType

		c.CustomerType,
		CASE 
			WHEN c.CustomerType = N'CS' THEN N'Cash'
			WHEN LTRIM(RTRIM(ISNULL(AccountOwner.[NationalOwner], N''))) <> N'' 
				--OR LTRIM(RTRIM(ISNULL(PCowner.[Brand Owner P & C: Title], N''))) LIKE N'%National%'
				THEN N'National'
			ELSE N'Local'
		END AS AccountUDVarChar9,					-- OwnerBucket - New as of 5/22/2014
		
		ISNULL(NULLIF(cs.CustomerSegment, N''), N'Unknown') AS AccountUDVarChar10,						-- Customer Segment

		CASE
			WHEN rc.[Account Number] IS NOT NULL THEN N'Y'
			ELSE N'N'
		END AS AccountUDVarChar11,						-- CustomerRebateIndicator https://enterbridge.atlassian.net/browse/LIO-125 

		CASE ISNULL(c.FixedPrice, N'N')
			WHEN N'Y' THEN N'Y'
			ELSE N'N'
		END AS AccountUDVarChar12,

		c.ContractName AS AccountUDVarChar13,

		CASE
			WHEN ISNULL(CustomerSpecialism.CustomerSpecialism, N'Other') = N'Other' THEN 'Heat'
			WHEN CustomerSpecialism.CustomerSpecialism = 'noncore' THEN 'Cash'---
			ELSE CustomerSpecialism.CustomerSpecialism
		END AS CustomerSpecialism,

		CASE
			WHEN ISDATE(c.DateOpened) = 1 THEN CAST(c.DateOpened AS DATETIME)
			ELSE NULL
		END AS AccountUDDateTime1,
		CASE
			WHEN ISDATE(c.DateClosed) = 1 THEN CAST(c.DateClosed AS DATETIME)
			ELSE NULL
		END AS AccountUDDateTime2,

		ROW_NUMBER() OVER (PARTITION BY c.AccountID ORDER BY c.AccountID COLLATE SQL_Latin1_General_CP1_CS_AS DESC) AS RowRank
	FROM Lion_Staging.dbo.Customer c
	LEFT JOIN dbo.DimAccountGroup1 DAG1SB
		ON DAG1SB.AG1Level1 = c.StatementBranch
	LEFT JOIN (
			SELECT AccountID, MAX(TermsOwner) AS Branch
			FROM Lion_Staging.dbo.TermsOwner
			GROUP BY AccountID
			HAVING COUNT(*) = 1
			) TermsOwner
		ON TermsOwner.AccountID = c.AccountID
	LEFT JOIN dbo.DimAccountGroup1 DAG1TOB
		ON DAG1TOB.AG1Level1 = TermsOwner.Branch
	LEFT JOIN (
			SELECT AccountID, COUNT(*) AS TOCount
			FROM Lion_Staging.dbo.TermsOwner
			GROUP BY AccountID
			) TermsOwnerCount
		ON TermsOwnerCount.AccountID = c.AccountID
	LEFT JOIN #TermsOwnerBranchSelected TermsOwnerBranchSelected
		ON TermsOwnerBranchSelected.AccountID = c.AccountID
	LEFT JOIN dbo.DimAccountGroup2 DAG2 
		ON DAG2.AG2Level1 = c.CustomerType
	LEFT JOIN dbo.DimAccountGroup3 DAG3
		ON DAG3.AG3Level1 =
			CASE LTRIM(RTRIM(c.TradingStatus))
				WHEN N'' THEN ''
				WHEN N'0' THEN '0'
				WHEN N'011' THEN '1'
				WHEN N'012' THEN '3'
				WHEN N'013' THEN '4'
				WHEN N'014' THEN '6'
				WHEN N'031' THEN '2'
				WHEN N'041' THEN '5'
				WHEN N'114' THEN '9'
				WHEN N'141' THEN '8'
				WHEN N'241' THEN '10'
				WHEN N'341' THEN '7'
				ELSE c.TradingStatus
			END 
	LEFT JOIN Lion_Staging.dbo.CustomerSegment cs
		ON cs.AccountNumber = c.AccountID
		
	LEFT JOIN Lion_Staging.dbo.AccountOwner  -- "Full" sales force data - not sure why there is subsequent P&C specific data but there is
		ON RIGHT(AccountOwner.AccountNumber, 7) = c.AccountID

	LEFT JOIN #TermsOwner tOwn
		ON tOwn.AccountID = c.AccountID
	LEFT JOIN Lion_Staging.dbo.RebateCustomer rc
		ON rc.[Account Number] = c.AccountID
	LEFT JOIN #CustomerSpecialism CustomerSpecialism
		ON CustomerSpecialism.AccountNumberParent = ISNULL(NULLIF(tOwn.TermsParent, ''), c.AccountID)
	WHERE
		c.ETLRowStatusCodeKey = 0

), AccountStagingData AS (
		SELECT
			AccountClientUniqueIdentifier,
			s.AccountNumber,
			AccountNumberParent,
			s.AccountName,
			s.AccountGroup1Key,
			AccountGroup2Key,
			AccountGroup3Key,
			AccountGroup4Key,
			--ISNULL(DAG4.AccountGroup4Key, @NullAccountGroup4Key) AS AccountGroup4Key,
			COALESCE(dam1.AccountManagerKey, dam2.AccountManagerKey, dam3.AccountManagerKey, @NullAccountManagerKey) AS AccountManagerKey,
			
			COALESCE(db.BrandKey, @NullBrandKey) AS BrandKey,
			
			AccountAddress1,
			AccountAddress2,
			AccountAddress3,
			AccountAddress4,
			AccountCity,
			AccountState,
			AccountZipCode,
			AccountUDVarChar1,
			AccountUDVarChar2,
			AccountUDVarChar3,
			AccountUDVarChar4,
			AccountUDVarChar5,
			AccountUDVarChar6,
			AccountUDVarChar7,
			AccountUDVarChar8,
			AccountUDVarChar9,
			AccountUDVarChar10,
			AccountUDVarChar11,
			AccountUDVarChar12,
			AccountUDVarChar13,
			CustomerSpecialism,
			AccountUDDateTime1,
			AccountUDDateTime2,
			CASE
				WHEN AccountUDDateTime2 IS NULL 
					AND ISNULL(AccountUDVarChar3, N'') <> N'Y' -- intercompany
					AND ISNULL(CustomerType, N'') NOT IN (N'', N'BD', N'BM', N'IN', N'LC') 
					THEN 0
				ELSE 1
			END AS InActive,
			--CASE
			--	-- ((closed date is null or closed date > today) AND Opened date <= today)
			--	WHEN (AccountUDDateTime2 IS NULL OR AccountUDDateTime2 > GETDATE()) AND AccountUDDateTime1 <= DATEADD(DAY, 1, GETDATE()) then 0
			--	ELSE 1
			--END AS InActive,
			ROW_NUMBER() OVER (PARTITION BY AccountClientUniqueIdentifier ORDER BY AccountClientUniqueIdentifier COLLATE SQL_Latin1_General_CP1_CS_AS DESC) AS RowRank
		FROM StagingData s
		LEFT JOIN dbo.DimAccountGroup1 dag1  -- get to the branch in so that we know the branch's brand
			ON dag1.AccountGroup1Key = s.AccountGroup1Key

		LEFT JOIN #AccountManager am1							-- account manager/owner based on the brand with the highest sales
			ON am1.AccountNumber = s.AccountNumber

		LEFT JOIN dbo.DimBrand db
			ON db.Brand = am1.Brand

		LEFT JOIN #AccountBrandManager am2						-- account manager/owner based on the brand with the highest sales
			ON am2.AccountNumber = s.AccountNumber
			AND am2.Brand = 'National'

		LEFT JOIN dbo.DimPerson dp1
			ON dp1.Email = am1.BrandOwner
		LEFT JOIN dbo.DimAccountManager dam1
			ON dam1.DimPersonKey = dp1.DimPersonKey		 

		LEFT JOIN dbo.DimPerson dp2
			ON dp2.Email = am2.BrandOwner
		LEFT JOIN dbo.DimAccountManager dam2
			ON dam2.DimPersonKey = dp2.DimPersonKey

		LEFT JOIN dbo.DimPerson dp3
			ON dp3.Email = AG1Level1UDVarchar24
		LEFT JOIN dbo.DimAccountManager dam3
			ON dam3.DimPersonKey = dp3.DimPersonKey

		WHERE 
			s.RowRank = 1
)
MERGE dbo.DimAccount AS Tgt
USING 
	(
		SELECT
			Src.AccountClientUniqueIdentifier,
			Src.AccountManagerKey,
			Src.AccountNumber,
			Src.AccountNumberParent,
			Src.AccountName,
			Src.AccountGroup1Key,
			Src.AccountGroup2Key,
			Src.AccountGroup3Key,
			Src.AccountGroup4Key,
			Src.AccountAddress1,
			Src.AccountAddress2,
			Src.AccountAddress3,
			Src.AccountAddress4,
			Src.AccountCity,
			Src.AccountState,
			Src.AccountZipCode,
			Src.AccountUDVarChar1,
			Src.AccountUDVarChar2,
			Src.AccountUDVarChar3,
			Src.AccountUDVarChar4,
			Src.AccountUDVarChar5,
			Src.AccountUDVarChar6,
			Src.AccountUDVarChar7,
			Src.AccountUDVarChar8,
			Src.AccountUDVarChar9,
			Src.AccountUDVarChar10,
			Src.AccountUDVarChar11,
			Src.AccountUDVarChar12,
			Src.AccountUDVarChar13,
			Src.CustomerSpecialism,
			Src.AccountUDDateTime1,
			Src.AccountUDDateTime2,
			Src.InActive,
			Src.BrandKey
		FROM AccountStagingData Src
		WHERE
			RowRank = 1
	) AS Src
ON Tgt.AccountClientUniqueIdentifier = Src.AccountClientUniqueIdentifier
WHEN MATCHED AND
	CHECKSUM(--CheckSum for target DimAccount table.  This needs to check all rows that we will UPDATE IF a find a change
		Tgt.AccountManagerKey,
		Tgt.AccountNumber,
		Tgt.AccountNumberParent,
		Tgt.AccountName,
		Tgt.AccountGroup1Key,
		Tgt.AccountGroup2Key,
		Tgt.AccountGroup3Key,
		Tgt.AccountGroup4Key,
		Tgt.AccountAddress1,
		Tgt.AccountAddress2,
		Tgt.AccountAddress3,
		Tgt.AccountAddress4,
		Tgt.AccountCity,
		Tgt.AccountState,
		Tgt.AccountZipCode,
		Tgt.AccountUDVarChar1,
		Tgt.AccountUDVarChar2,
		Tgt.AccountUDVarChar3,
		Tgt.AccountUDVarChar4,
		Tgt.AccountUDVarChar5,
		Tgt.AccountUDVarChar6,
		Tgt.AccountUDVarChar7,
		Tgt.AccountUDVarChar8,
		Tgt.AccountUDVarChar9,
		Tgt.AccountUDVarChar10,
		Tgt.AccountUDVarChar11,
		Tgt.AccountUDVarChar12,
		Tgt.AccountUDVarChar13,
		Tgt.CustomerSpecialism,
		Tgt.AccountUDDateTime1,
		Tgt.AccountUDDateTime2,
		Tgt.InActive,
		Tgt.BrandKey
		)
	<>
	CHECKSUM(--CheckSum for staging table
		COALESCE(Src.AccountManagerKey, Tgt.AccountManagerKey, @NullAccountManagerKey),
		Src.AccountNumber,
		Src.AccountNumberParent,
		Src.AccountName,
		Src.AccountGroup1Key,
		Src.AccountGroup2Key,
		Src.AccountGroup3Key,
		Src.AccountGroup4Key,
		Src.AccountAddress1,
		Src.AccountAddress2,
		Src.AccountAddress3,
		Src.AccountAddress4,
		Src.AccountCity,
		Src.AccountState,
		Src.AccountZipCode,
		Src.AccountUDVarChar1,
		Src.AccountUDVarChar2,
		Src.AccountUDVarChar3,
		Src.AccountUDVarChar4,
		Src.AccountUDVarChar5,
		Src.AccountUDVarChar6,
		Src.AccountUDVarChar7,
		Src.AccountUDVarChar8,
		Src.AccountUDVarChar9,
		Src.AccountUDVarChar10,
		Src.AccountUDVarChar11,
		Src.AccountUDVarChar12,
		Src.AccountUDVarChar13,
		Src.CustomerSpecialism,
		Src.AccountUDDateTime1,
		Src.AccountUDDateTime2,
		Src.InActive,
		Src.BrandKey
		)
	THEN UPDATE 
		SET
			Tgt.ModificationDayKey = @TodayDayKey,
			Tgt.AccountManagerKey = COALESCE(Src.AccountManagerKey, Tgt.AccountManagerKey, @NullAccountManagerKey),
			Tgt.AccountNumber = Src.AccountNumber,
			Tgt.AccountNumberParent = Src.AccountNumberParent,
			Tgt.AccountName = Src.AccountName,
			Tgt.AccountGroup1Key = Src.AccountGroup1Key,
			Tgt.AccountGroup2Key = Src.AccountGroup2Key,
			Tgt.AccountGroup3Key = Src.AccountGroup3Key,
			Tgt.AccountGroup4Key = Src.AccountGroup4Key,
			Tgt.AccountAddress1 = Src.AccountAddress1,
			Tgt.AccountAddress2 = Src.AccountAddress2,
			Tgt.AccountAddress3 = Src.AccountAddress3,
			Tgt.AccountAddress4 = Src.AccountAddress4,
			Tgt.AccountCity = Src.AccountCity,
			Tgt.AccountState = Src.AccountState,
			Tgt.AccountZipCode = Src.AccountZipCode,
			Tgt.AccountUDVarChar1 = Src.AccountUDVarChar1,
			Tgt.AccountUDVarChar2 = Src.AccountUDVarChar2,
			Tgt.AccountUDVarChar3 = Src.AccountUDVarChar3,
			Tgt.AccountUDVarChar4 = Src.AccountUDVarChar4,
			Tgt.AccountUDVarChar5 = Src.AccountUDVarChar5,
			Tgt.AccountUDVarChar6 = Src.AccountUDVarChar6,
			Tgt.AccountUDVarChar7 = Src.AccountUDVarChar7,
			Tgt.AccountUDVarChar8 = Src.AccountUDVarChar8,
			Tgt.AccountUDVarChar9 = Src.AccountUDVarChar9,
			Tgt.AccountUDVarChar10 = Src.AccountUDVarChar10,
			Tgt.AccountUDVarChar11 = Src.AccountUDVarChar11,
			Tgt.AccountUDVarChar12 = Src.AccountUDVarChar12,
			Tgt.AccountUDVarChar13 = Src.AccountUDVarChar13,
			Tgt.CustomerSpecialism = Src.CustomerSpecialism,
			Tgt.AccountUDDateTime1 = Src.AccountUDDateTime1,
			Tgt.AccountUDDateTime2 = Src.AccountUDDateTime2,
			Tgt.InActive = Src.InActive,
			Tgt.BrandKey = Src.BrandKey
WHEN NOT MATCHED 
	THEN INSERT (
			CreationDayKey,
			ModificationDayKey,
			AccountManagerKey,
			AccountClientUniqueIdentifier,
			AccountNumber,
			AccountNumberParent,
			AccountName,
			AccountGroup1Key,
			AccountGroup2Key,
			AccountGroup3Key,
			AccountGroup4Key,
			AccountAddress1,
			AccountAddress2,
			AccountAddress3,
			AccountAddress4,
			AccountCity,
			AccountState,
			AccountZipCode,
			AccountUDVarChar1,
			AccountUDVarChar2,
			AccountUDVarChar3,
			AccountUDVarChar4,
			AccountUDVarChar5,
			AccountUDVarChar6,
			AccountUDVarChar7,
			AccountUDVarChar8,
			AccountUDVarChar9,
			AccountUDVarChar10,
			AccountUDVarChar11,
			AccountUDVarChar12,
			AccountUDVarChar13,
			CustomerSpecialism,
			AccountUDDateTime1,
			AccountUDDateTime2,
			InActive,
			BrandKey
			)
		VALUES (
			@TodayDayKey,
			@TodayDayKey,		
			ISNULL(Src.AccountManagerKey, @NullAccountManagerKey),
			Src.AccountClientUniqueIdentifier,
			Src.AccountNumber,
			Src.AccountNumberParent,
			Src.AccountName,
			Src.AccountGroup1Key,
			Src.AccountGroup2Key,
			Src.AccountGroup3Key,
			Src.AccountGroup4Key,
			Src.AccountAddress1,
			Src.AccountAddress2,
			Src.AccountAddress3,
			Src.AccountAddress4,
			Src.AccountCity,
			Src.AccountState,
			Src.AccountZipCode,
			Src.AccountUDVarChar1,
			Src.AccountUDVarChar2,
			Src.AccountUDVarChar3,
			Src.AccountUDVarChar4,
			Src.AccountUDVarChar5,
			Src.AccountUDVarChar6,
			Src.AccountUDVarChar7,
			Src.AccountUDVarChar8,
			Src.AccountUDVarChar9,
			Src.AccountUDVarChar10,
			Src.AccountUDVarChar11,
			Src.AccountUDVarChar12,
			Src.AccountUDVarChar13,
			Src.CustomerSpecialism,
			Src.AccountUDDateTime1,
			Src.AccountUDDateTime2,
			Src.InActive,
			Src.BrandKey
			)
--OUTPUT deleted.*, $action, inserted.* INTO #MyTempTable
--OUTPUT $action, inserted.*, deleted.*;
;
--SET @RowsU = @@RowCOunt + @RowsU


SELECT 
	@RowsU = @RowsU + (@@RowCOunt - (COUNT(*) - @BeginRowCount)),
	@RowsI =  COUNT(*) - @BeginRowCount
FROM dbo.DimAccount

----EXEC DBA_IndexRebuild 'DimAccount', 'Create'



/**************************************************************************************************************************************************/
EXEC LogDCPEvent 'ETL - Load_DimAccount: DimAccountBrandManager', 'B'

DECLARE
	@RowsI2 INT, 
	@RowsU2 INT, 
	@RowsD2 INT
	
SET @RowsI2 = 0
SET @RowsU2 = 0
SET @RowsD2 = 0


EXEC DBA_IndexRebuild 'Load_DimAccount', 'Drop'

SET @RowsD2 = (SELECT COUNT(*) FROM dbo.DimAccountBrandManager)

TRUNCATE TABLE dbo.DimAccountBrandManager

INSERT dbo.DimAccountBrandManager (
	AccountKey,
	BrandKey,
	AccountManagerKey,
	CreationDayKey,
	ModificationDayKey
)
SELECT
	da.AccountKey,
	abm.BrandKey,
	dam.AccountManagerKey,
	@TodayDayKey,
	@TodayDayKey
FROM #AccountBrandManager abm
INNER JOIN dbo.DimAccount da
	ON da.AccountNumber = abm.AccountNumber
--INNER JOIN dbo.DimBrand db 
--	ON db.Brand = abm.Brand
INNER JOIN dbo.DimPerson dp
	ON dp.Email = abm.BrandOwner
INNER JOIN dbo.DimAccountManager dam
	ON dam.DimPersonKey = dp.DimPersonKey
SET @RowsI2 = @RowsI2 + @@RowCount

EXEC DBA_IndexRebuild 'DimAccountBrandManager', 'Create'

EXEC LogDCPEvent 'ETL - Load_DimAccount: DimAccountBrandManager', 'E', @RowsI2, @RowsU2, @RowsD2



DROP TABLE #AccountBrandManager
DROP TABLE #AccountBranchSales
DROP TABLE #AccountManager
DROP TABLE #TermsOwnerBranchSelected
DROP TABLE #TermsOwner
DROP TABLE #CustomerSpecialism


EXEC LogDCPEvent 'ETL - Load_DimAccount', 'E', @RowsI, @RowsU, @RowsD 


SELECT TOP 10 * FROM dbo.DimAccount
SELECT TOP 10 * FROM dbo.DimAccountBrandManager




SET QUOTED_IDENTIFIER OFF 





GO
