SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [cm].[GetExpiredContracts] (@WebUserKey INT)

AS

-- exec [cm].[GetExpiredContracts] 1


--Get list of contract branches for this WebUserKey
CREATE TABLE #ContractBranchList (SortOrder INT, PrimaryAccountGroup1Key INT, Branch VARCHAR(50), BranchName VARCHAR(250), Converted BIT)

INSERT INTO #ContractBranchList EXEC [cc].[Get_WebUserContractBranches] @WebUserKey;


SELECT  
		dv.VendorNumber AS SupplierNumber,
		sc.ContractID AS ContractID,
		sc.ExpirationDate
	
FROM cc.SupplierContract sc WITH (NOLOCK)
INNER JOIN dbo.DimVendor dv
	ON dv.VendorKey = sc.VendorKey
INNER JOIN #ContractBranchList cbl
	ON cbl.Branch COLLATE DATABASE_DEFAULT = sc.ContractOwner COLLATE DATABASE_DEFAULT 
WHERE DATEDIFF(dd, CAST(GETDATE() AS DATE), ExpirationDate) BETWEEN -30 AND 60

ORDER BY sc.ExpirationDate


DROP TABLE #ContractBranchList
GO
