SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




CREATE PROCEDURE [lion].[GetRPBPriceAttributes]
	@ParamPlaybookPricingGroupKey Key_Normal_type
AS

/*

EXEC lion.GetRPBPriceAttributes 651225

*/

SET NOCOUNT ON

DECLARE
	@GroupValues VARCHAR(MAX),
	@PricingRuleKey Key_Normal_type


SELECT
	@GroupValues = GroupValues,
	@PricingRuleKey = PricingRuleKey
FROM dbo.PlaybookPricingGroup ppg
WHERE 
	PlaybookPricingGroupKey = @ParamPlaybookPricingGroupKey


--SELECT '@PricingRuleKey', @PricingRuleKey
--SELECT * FROM dbo.fn_ListToTableMax(@GroupValues, ',')


SELECT
	NEWID() AS GUID,
	DisplayName,
	vals.Value
FROM dbo.ATKPricingRuleAttribute apra
INNER JOIN dbo.ATKPricingRuleAttributeType aprat
	ON aprat.PricingRuleAttributeTypeKey = apra.PricingRuleAttributeTypeKey
LEFT JOIN dbo.fn_ListToTableMax(@GroupValues, ',') vals
	ON vals.ListID = PricingRuleAttributeSequenceNumber
WHERE PricingRuleKey = @PricingRuleKey


GO
