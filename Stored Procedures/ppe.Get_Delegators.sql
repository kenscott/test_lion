SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [ppe].[Get_Delegators](@WebUserKey INT)
AS 

	--/* delegators */
	--SELECT wu.WebUserKey, wu.UserName , op.FullName
	--FROM dbo.DimAccountManager dam
	--JOIN dbo.WebUser wu ON wu.AccountManagerKey = dam.AccountManagerKey
	--JOIN dbo.ODSPerson op ON op.ODSPersonKey = wu.ODSPersonKey
	--LEFT JOIN dbo.WebUserDelegate wud ON wud.DelegatorWebUserKey = wu.WebUserKey AND wud.DelegateWebUserKey <> wud.DelegatorWebUserKey
	--WHERE wud.WebUserDelegateKey IS NULL

	SELECT wu.WebUserKey, wu.UserName, op.FullName
	FROM (
			SELECT AccountManagerKey from WebUser where WebUserKey =  @WebUserKey /* me */
			UNION

			SELECT SubsidiaryAccountManagerKey AS AccountManagerKey /*all subordinates */
			FROM dbo.BridgeAccountManager bam
			JOIN dbo.WebUser wu ON bam.ParentAccountManagerKey = wu.AccountManagerKey
			WHERE wu.WebUserKey = @WebUserKey
			UNION

			SELECT dam.AccountManagerKey /*my delegators */
			FROM WebUserDelegate wud
			JOIN dbo.WebUser wu ON wu.WebUserKey = wud.DelegatorWebUserKey
			JOIN dbo.DimAccountManager dam ON dam.AccountManagerKey = wu.AccountManagerKey
			WHERE DelegateWebUserKey <> DelegatorWebUserKey
			AND DelegateWebUserKey  = @WebUserKey
			AND GETDATE() BETWEEN wud.StartDate AND ISNULL(wud.EndDate,'12-31-9999')

			UNION

			SELECT bam.SubsidiaryAccountManagerKey /*my delegators downline */
			FROM WebUserDelegate wud
			JOIN dbo.WebUser wu ON wu.WebUserKey = wud.DelegatorWebUserKey
			JOIN dbo.BridgeAccountManager bam ON bam.ParentAccountManagerKey = wu.AccountManagerKey
			WHERE DelegateWebUserKey <> DelegatorWebUserKey
			AND DelegateWebUserKey  = @WebUserKey
			AND GETDATE() BETWEEN wud.StartDate AND ISNULL(wud.EndDate,'12-31-9999')

			EXCEPT 
			(   SELECT dam.AccountManagerKey /*except any current delegates */ 
				FROM WebUserDelegate wud
				JOIN dbo.WebUser wu ON wu.WebUserKey = wud.DelegateWebUserKey
				JOIN dbo.DimAccountManager dam ON dam.AccountManagerKey = wu.AccountManagerKey
				WHERE DelegateWebUserKey <> DelegatorWebUserKey
				AND GETDATE() BETWEEN wud.StartDate AND ISNULL(wud.EndDate,'12-31-9999')
				--UNION
				--SELECT dam.AccountManagerKey /*except any current delegators */
				--FROM WebUserDelegate wud
				--JOIN dbo.WebUser wu ON wu.WebUserKey = wud.DelegatorWebUserKey
				--JOIN dbo.DimAccountManager dam ON dam.AccountManagerKey = wu.AccountManagerKey
				--WHERE DelegateWebUserKey <> DelegatorWebUserKey
				--AND GETDATE() BETWEEN wud.StartDate AND ISNULL(wud.EndDate,'12-31-9999')
				 )
		) w
	JOIN dbo.WebUser wu ON wu.AccountManagerKey = w.AccountManagerKey
	JOIN dbo.ODSPerson op ON op.ODSPersonKey = wu.ODSPersonKey
GO
