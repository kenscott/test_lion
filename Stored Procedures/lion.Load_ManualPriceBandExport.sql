SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO






CREATE PROCEDURE [lion].[Load_ManualPriceBandExport]  (
	@LLSPGCode UDVarchar_type
)
AS

/*

Only include rows where 1) there's data, or 2) a single summary row for every product. 

EXEC lion.Load_ManualPriceBandExportByNetwork N'AJ01'


EXEC lion.Load_ManualPriceBandExport N'AJ01'

SELECT * FROM lion.ATKManualPriceBandExport
ORDER BY 2,3,4,5,6,7,8,9,10

*/

SET NOCOUNT ON

DECLARE
	@RowsI INT,
	@RowsU INT,
	@RowsD INT


SET @RowsI = 0
SET @RowsU = 0
SET @RowsD = 0


TRUNCATE TABLE lion.ATKManualPriceBandExport



/*** Do the LLSPG Code Rollup ***/

/** Get Item "all" summary row **/
; WITH ItemSalesRollup AS (
	SELECT
		IG3Level4 AS ItemPyramidCode,
		IG3Level1 AS LLSPGCode,
		ItemUDVarChar20 AS ProductCode,

		SUM(TotalActualPrice) AS TotalSales,
		SUM(TotalActualCost) AS TotalCost,
		SUM(TotalQuantity) AS TotalQuantity,
		SUM(TotalQuantity * UnitListPrice) AS TotalTradePrice,
		--SUM(TotalActualPrice * il.FloorGPP) AS GTMFloor,
		--SUM(TotalActualPrice * il.TargetGPP) AS GTMTarget,
		--SUM(TotalActualPrice * il.StretchGPP) AS GTMStretch,
		SUM(TotalActualPrice * il.FloorGPP)/NULLIF(SUM(TotalActualPrice), 0) AS CurrentFloorGPP,
		SUM(TotalActualPrice * il.TargetGPP)/NULLIF(SUM(TotalActualPrice), 0) AS CurrentTargetGPP,
		SUM(TotalActualPrice * il.StretchGPP)/NULLIF(SUM(TotalActualPrice), 0) AS CurrentStretchGPP
	FROM FactInvoiceLinePricebandScore il
	--INNER JOIN dbo.DimAccountGroup1 dag1
	--	ON dag1.AccountGroup1Key = il.AccountGroup1Key
	INNER JOIN dbo.DimItem di
		ON di.ItemKey = il.ItemKey
	INNER JOIN dbo.DimItemGroup3 dig3
		ON dig3.ItemGroup3Key = di.ItemGroup3Key
	--INNER JOIN dbo.DimInvoiceLineGroup1 dilg1
	--	ON dilg1.InvoiceLineGroup1Key = il.InvoiceLineGroup1Key
	INNER JOIN dbo.FactLastPriceAndCost flpac
		ON flpac.AccountKey = il.AccountKey
		AND flpac.ItemKey = il.ItemKey
		AND flpac.InvoiceLineGroup1Key = il.InvoiceLineGroup1Key
	WHERE 
		IG3Level4 = N'1'
		--AND IG3Level1 = N'AJ01'	--@LLSPGCode
		AND IG3Level1 = @LLSPGCode
	GROUP BY
		IG3Level4,
		IG3Level1,
		ItemUDVarChar20
)
INSERT lion.ATKManualPriceBandExport (
	ItemPyramidCode,
	LLSPGCode,
	ProductCode,
	PriceApproach,
	ContractClaimsIndicator,
	Region,
	Area,
	Network,
	Branch,
	
	CurrentFloorGPP,
	CurrentTargetGPP,
	CurrentStretchGPP,

	CurrentFloorDiscount,
	CurrentTargetDiscount,
	CurrentStretchDiscount,

	CurrentFloorPrice, 
	CurrentTargetPrice, 
	CurrentStretchPrice, 

	ProductCost, 
	TradePrice
)
SELECT
	IG3Level4 AS ItemPyramidCode,
	IG3Level1 AS LLSPGCode,
	ItemUDVarChar20 AS ProductCode,
	N'ALL' AS PriceApproach,
	N'ALL' AS ContractClaimsIndicator,
	N'ALL' AS Region,
	N'ALL' AS Area,
	N'ALL' AS Network,
	N'ALL' AS Branch,

	CurrentFloorGPP,
	CurrentTargetGPP,
	CurrentStretchGPP,
	
	1 - (rp.Price / NULLIF(TotalTradePrice/TotalQuantity, 0)) AS CurrentFloorDiscount,
	1 - (ap.Price / NULLIF(TotalTradePrice/TotalQuantity, 0)) AS CurrentTargetDiscount,
	1 - (gp.Price / NULLIF(TotalTradePrice/TotalQuantity, 0)) AS CurrentGreeDiscount,

	rp.Price AS CurrentFloorPrice,
	ap.Price AS CurrentTargetPrice,
	gp.Price AS CurrentStretchPrice,

	(di.ItemUDDecimal3 - di.ItemUDDecimal4 - di.ItemUDDecimal5) AS ProductCost,
	ItemUDDecimal1 AS TradePrice
FROM dbo.DimItem di
INNER JOIN dbo.DimItemGroup3 dig3
	ON dig3.ItemGroup3Key = di.ItemGroup3Key
LEFT JOIN ItemSalesRollup r
	ON 
		IG3Level4 = r.ItemPyramidCode
		AND IG3Level1 = r.LLSPGCode
		AND ItemUDVarChar20 = r.ProductCode
OUTER APPLY lion.fn_GetPriceWeighted(TotalCost, TotalQuantity, CurrentFloorGPP) rp
OUTER APPLY lion.fn_GetPriceWeighted(TotalCost, TotalQuantity, CurrentTargetGPP) ap
OUTER APPLY lion.fn_GetPriceWeighted(TotalCost, TotalQuantity, CurrentStretchGPP) gp
WHERE 
	IG3Level4 = N'1'
	--AND IG3Level1 = N'AJ01'	--@LLSPGCode
	AND IG3Level1 = @LLSPGCode
SELECT @RowsI = @RowsI + @@ROWCOUNT


/** Get attribute summary rows **/
; WITH LLSPGSalesRollup AS (
	SELECT
		IG3Level4 AS ItemPyramidCode,
		IG3Level1 AS LLSPGCode,
		ItemUDVarChar20 AS ProductCode,
		UDVarchar10 AS PriceApproach,
		InvoiceLineGroup1UDVarchar2 AS ContractClaimsIndicator,

		SUM(TotalActualPrice) AS TotalSales,
		SUM(TotalActualCost) AS TotalCost,
		SUM(TotalQuantity) AS TotalQuantity,
		SUM(TotalQuantity * UnitListPrice) AS TotalTradePrice,
		--SUM(TotalActualPrice * il.FloorGPP) AS GTMFloor,
		--SUM(TotalActualPrice * il.TargetGPP) AS GTMTarget,
		--SUM(TotalActualPrice * il.StretchGPP) AS GTMStretch,
		SUM(TotalActualPrice * il.FloorGPP)/NULLIF(SUM(TotalActualPrice), 0) AS CurrentFloorGPP,
		SUM(TotalActualPrice * il.TargetGPP)/NULLIF(SUM(TotalActualPrice), 0) AS CurrentTargetGPP,
		SUM(TotalActualPrice * il.StretchGPP)/NULLIF(SUM(TotalActualPrice), 0) AS CurrentStretchGPP
	FROM FactInvoiceLinePricebandScore il
	INNER JOIN dbo.DimAccountGroup1 dag1
		ON dag1.AccountGroup1Key = il.AccountGroup1Key
	INNER JOIN dbo.DimItem di
		ON di.ItemKey = il.ItemKey
	INNER JOIN dbo.DimItemGroup3 dig3
		ON dig3.ItemGroup3Key = di.ItemGroup3Key
	INNER JOIN dbo.DimInvoiceLineGroup1 dilg1
		ON dilg1.InvoiceLineGroup1Key = il.InvoiceLineGroup1Key
	INNER JOIN dbo.FactLastPriceAndCost flpac
		ON flpac.AccountKey = il.AccountKey
		AND flpac.ItemKey = il.ItemKey
		AND flpac.InvoiceLineGroup1Key = il.InvoiceLineGroup1Key
	WHERE 
		IG3Level4 = N'1'
		--AND IG3Level1 = N'AJ01'	--@LLSPGCode
		AND IG3Level1 = @LLSPGCode
	GROUP BY
		IG3Level4,
		IG3Level1,
		ItemUDVarChar20,
		UDVarchar10,
		InvoiceLineGroup1UDVarchar2
)
INSERT lion.ATKManualPriceBandExport (
	ItemPyramidCode,
	LLSPGCode,
	ProductCode,
	PriceApproach,
	ContractClaimsIndicator,
	Region,
	Area,
	Network,
	Branch,
	
	CurrentFloorGPP,
	CurrentTargetGPP,
	CurrentStretchGPP,

	CurrentFloorDiscount,
	CurrentTargetDiscount,
	CurrentStretchDiscount,

	CurrentFloorPrice, 
	CurrentTargetPrice, 
	CurrentStretchPrice, 

	ProductCost, 
	TradePrice
)
SELECT
	ItemPyramidCode,
	LLSPGCode,
	ProductCode,
	PriceApproach,
	ContractClaimsIndicator,
	N'ALL' AS Region,
	N'ALL' AS Area,
	N'ALL' AS Network,
	N'ALL' AS Branch,

	CurrentFloorGPP,
	CurrentTargetGPP,
	CurrentStretchGPP,
	
	1 - (rp.Price / NULLIF(TotalTradePrice/TotalQuantity, 0)) AS CurrentFloorDiscount,
	1 - (ap.Price / NULLIF(TotalTradePrice/TotalQuantity, 0)) AS CurrentTargetDiscount,
	1 - (gp.Price / NULLIF(TotalTradePrice/TotalQuantity, 0)) AS CurrentGreeDiscount,

	rp.Price AS CurrentFloorPrice,
	ap.Price AS CurrentTargetPrice,
	gp.Price AS CurrentStretchPrice,
		
	(di.ItemUDDecimal3 - di.ItemUDDecimal4 - di.ItemUDDecimal5) AS ProductCost,
	ItemUDDecimal1 AS TradePrice
FROM 
	LLSPGSalesRollup r
INNER JOIN dbo.DimItem di
	ON di.ItemNumber = r.ItemPyramidCode + r.ProductCode
OUTER APPLY lion.fn_GetPriceWeighted(TotalCost, TotalQuantity, CurrentFloorGPP) rp
OUTER APPLY lion.fn_GetPriceWeighted(TotalCost, TotalQuantity, CurrentTargetGPP) ap
OUTER APPLY lion.fn_GetPriceWeighted(TotalCost, TotalQuantity, CurrentStretchGPP) gp
SELECT @RowsI = @RowsI + @@ROWCOUNT




SELECT @RowsI AS NumberOfRows
GO
