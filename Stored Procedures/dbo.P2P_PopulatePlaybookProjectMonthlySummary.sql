SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE  
PROCEDURE [dbo].[P2P_PopulatePlaybookProjectMonthlySummary]
	@ProjectKey Key_Normal_Type
AS

/*
	EXEC dbo.PM_PopulatePlaybookProjectMonthlySummary 172
*/

SET QUOTED_IDENTIFIER ON 
SET ANSI_NULLS ON 
SET ARITHABORT ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
SET CONCAT_NULL_YIELDS_NULL ON
SET NUMERIC_ROUNDABORT OFF
SET NOCOUNT ON 


DECLARE 
	@RowsI Quantity_Normal_type, 
	@RowsU Quantity_Normal_type, 
	@RowsD Quantity_Normal_type

SET @RowsI = 0
SET @RowsU = 0
SET @RowsD = 0


EXEC LogDCPEvent 'DCP --- dbo.PM_PopulatePlaybookProjectMonthlySummary: Insert dbo.PlaybookProjectMonthlySummary', 'B', 0, 0, 0, @ProjectKey


DELETE FROM dbo.PlaybookProjectMonthlySummary 
WHERE ProjectKey = @ProjectKey
SET @RowsD = @RowsD + @@RowCount


SET QUOTED_IDENTIFIER ON 
SET ANSI_NULLS ON 
SET ARITHABORT ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
SET CONCAT_NULL_YIELDS_NULL ON
SET NUMERIC_ROUNDABORT OFF


INSERT INTO dbo.PlaybookProjectMonthlySummary (
	ProjectKey,
	fil.AccountKey,
	MonthKey,
	TotalActualPrice,
	TotalActualCost
) 
SELECT
	@ProjectKey,
	fil.AccountKey,
	InvoiceDateMonthKey,
	SUM(TotalActualPrice) AS TotalActualPrice,
	SUM(TotalActualCost) AS TotalActualCost
FROM dbo.FactInvoiceLine fil
INNER JOIN PlaybookProjectFullScopeSummary s
	ON s.ProjectKey = @ProjectKey
	AND s.AccountManagerKey = fil.AccountManagerKey
	AND s.AccountKey = fil.AccountKey
	AND s.ItemKey = fil.ItemKey
	AND s.InvoiceLineGroup1Key = fil.InvoiceLineGroup1Key
WHERE Last12MonthsIndicator = 'Y'
GROUP BY
	fil.AccountKey,
	InvoiceDateMonthKey
SET @RowsI = @RowsI + @@RowCount


DECLARE @USql NVARCHAR(4000)
SET @USql = 'UPDATE STATISTICS dbo.PlaybookProjectMonthlySummary'
EXEC (@USql)


EXEC LogDCPEvent 'DCP --- dbo.PM_PopulatePlaybookProjectMonthlySummary: Insert bearcat.PlaybookProjectMonthlySummary', 'E', @RowsI, @RowsU, @RowsD




SET QUOTED_IDENTIFIER OFF 
GO
