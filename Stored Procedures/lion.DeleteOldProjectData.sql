SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




CREATE
PROCEDURE [lion].[DeleteOldProjectData]

AS

SET QUOTED_IDENTIFIER ON 
SET ANSI_NULLS ON 
SET ARITHABORT ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
SET CONCAT_NULL_YIELDS_NULL ON
SET NUMERIC_ROUNDABORT OFF
SET XACT_ABORT ON
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
SET NOCOUNT ON

EXEC LogDCPEvent N'ETL - lion.DeleteOldProjectData', N'B'

DECLARE 
	@ProjectKey INT,
	@MaxProjectKey INT,
	@MsgStr VARCHAR (4000)


EXEC dbo.MVIEWS_Drop
EXEC dbo.MVIEWS_ProposalPrice_Drop

EXEC DBA_IndexRebuild N'PlaybookPricingGroupQuantityBand', N'Drop_NoClustered'
EXEC DBA_IndexRebuild N'PlaybookPricingGroup', N'Drop_NoClustered'


DECLARE ProjectCursor CURSOR FOR
	-- get list of projects
	SELECT DISTINCT 
		pdpg.ProjectKey,
		MAX(pdpg.ProjectKey) OVER() AS MaxProjectKey
	FROM dbo.PlaybookPricingGroup ppg
	INNER JOIN PlaybookDataPointGroup pdpg
		ON pdpg.PlaybookDataPointGroupKey = ppg.PlaybookDataPointGroupKey
	INNER JOIN PlaybookProject pp
		ON pp.ProjectKey = pdpg.ProjectKey
	WHERE 
		pp.ApproachTypeKey = 7
		AND NOT EXISTS (SELECT 1 FROM FactLastPriceAndCost l        WHERE l.RPBPlaybookPricingGroupKey = ppg.PlaybookPricingGroupKey)
		AND EXISTS     (SELECT 1 FROM PlaybookDataPointGroupScope s WHERE s.PlaybookDataPointGroupKey  = ppg.PlaybookDataPointGroupKey)
	ORDER BY
		pdpg.ProjectKey

OPEN ProjectCursor

FETCH NEXT FROM ProjectCursor INTO @ProjectKey, @MaxProjectKey

WHILE @@FETCH_STATUS = 0 AND @ProjectKey <> @MaxProjectKey
BEGIN
	RAISERROR ('', 0, 1) WITH NOWAIT	
	SET @MsgStr = (SELECT 'lion.DeleteOldProjectData: About to purge project key: ' + CAST(@ProjectKey AS VARCHAR(25)) + '.')
	RAISERROR (@MsgStr, 0, 1) WITH NOWAIT

	EXEC lion.RBP_DeleteProject @ProjectKey, 'N'

	FETCH NEXT FROM ProjectCursor INTO @ProjectKey, @MaxProjectKey
END
CLOSE ProjectCursor
DEALLOCATE ProjectCursor


EXEC DBA_IndexRebuild N'PlaybookPricingGroupQuantityBand', N'Create'
EXEC DBA_IndexRebuild N'PlaybookPricingGroup', N'Create'

EXEC dbo.DBA_RemainingFKRebuild

EXEC LogDCPEvent N'ETL - lion.DeleteOldProjectData', N'E', 0, 0, 0







GO
