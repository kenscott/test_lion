SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [lion].[Load_DimBrand]
WITH EXECUTE AS CALLER
AS
EXEC LogDCPEvent 'ETL - Load_DimBrand', 'B'

DECLARE 
	@RowsI INT, 
	@RowsU INT, 
	@RowsD INT,
	@BeginRowCount INT,
	@TodayDayKey Key_Small_Type

SET @RowsI = 0
SET @RowsU = 0
SET @RowsD = 0

SET @TodayDayKey = (SELECT DayKey FROM DimDay WHERE CONVERT(VARCHAR(10), SQLDate, 120) = CONVERT(VARCHAR(10), GETDATE(), 120))


-- EXEC DBA_IndexRebuild 'DimBrand', 'Drop'

-- TRUNCATE TABLE dbo.DimBrand


IF NOT EXISTS (SELECT 1 FROM DimBrand WHERE Brand = N'Unknown')
BEGIN

	INSERT dbo.DimBrand (
		Brand, 
		SegmentBrand,
		SegmentBrandDetached,
		AccountingBrand,
		PlaybookBrand,
		CreationDayKey,
		ModificationDayKey
	)
	VALUES (
		'Unknown',
		'Unknown',
		'Unknown',
		'Unknown',
		'',  -- intentially blank
		@TodayDayKey,
		@TodayDayKey)
	SET @RowsI = @RowsI + @@ROWCOUNT

END


SELECT @BeginRowCount = COUNT(*) FROM dbo.DimBrand

MERGE dbo.DimBrand AS Tgt
USING	(
	SELECT 'Unknown' AS Brand, 'Unknown' AS SegmentBrand, 'Unknown' AS SegmentBrandDetached, 'Unknown' AS AccountingBrand, '' AS PlaybookBrand
	UNION SELECT '', '', '', '', ''
	UNION SELECT 'BCGM', 'BCG', 'BCG', '', 'BCGM'
	UNION SELECT 'BRONI', '', '', '', ''
	UNION SELECT 'BROOK', '', '', '', ''
	UNION SELECT 'BUILD', '', '', '', 'Build'
	UNION SELECT 'BURDN', 'INFRA-ALL', 'BURDENS', 'Burdens', 'Infrastructure'
	UNION SELECT 'CLIMT', 'PIPE/CLIMATE', 'CLIMATE', 'Pipe & Climate', 'Pipe&Climate'
	UNION SELECT 'DRAIN', 'DRAIN', 'DRAIN', 'Drain', 'Drain'
	UNION SELECT 'ELEC', '', '', '', ''
	UNION SELECT 'FUSN', 'INFRA-ALL', '', 'Fusion', 'Infrastructure'
	UNION SELECT 'HEAT', '', '', '', ''
	UNION SELECT 'HIRE', '', '', '', 'Hire'
	UNION SELECT 'INFRA', 'INFRA-ALL', '', '', 'Infrastructure'
	UNION SELECT 'IS', 'IS', 'IS', 'Integrated Services', 'IS'
	UNION SELECT 'MPS', 'INFRA-ALL', '', '', 'Infrastructure'
	UNION SELECT 'NONE', '', '', '', ''
	UNION SELECT 'PARTS', 'PLUMB/PARTS', 'PARTS', 'Plumb & Parts', 'Plumb&Parts'
	UNION SELECT 'PIPE', 'PIPE/CLIMATE', 'PIPE', 'Pipe & Climate', 'Pipe&Climate'
	UNION SELECT 'PLUMB', 'PLUMB/PARTS', 'PLUMB', 'Plumb & Parts', 'Plumb&Parts'
	UNION SELECT 'UNFIX', '', '', '', ''
	UNION SELECT 'UPS', 'INFRA-ALL', '', 'Fusion', 'Infrastructure'
) AS Src
ON Tgt.Brand = Src.Brand
WHEN MATCHED AND
   (Tgt.SegmentBrand         <> Src.SegmentBrand         OR (Src.SegmentBrand         IS NULL AND Tgt.SegmentBrand         IS NOT NULL) OR (Src.SegmentBrand         IS NOT NULL AND Tgt.SegmentBrand         IS NULL))
OR (Tgt.SegmentBrandDetached <> Src.SegmentBrandDetached OR (Src.SegmentBrandDetached IS NULL AND Tgt.SegmentBrandDetached IS NOT NULL) OR (Src.SegmentBrandDetached IS NOT NULL AND Tgt.SegmentBrandDetached IS NULL))
OR (Tgt.AccountingBrand      <> Src.AccountingBrand      OR (Src.AccountingBrand      IS NULL AND Tgt.AccountingBrand      IS NOT NULL) OR (Src.AccountingBrand      IS NOT NULL AND Tgt.AccountingBrand      IS NULL))
OR (Tgt.PlaybookBrand        <> Src.PlaybookBrand        OR (Src.PlaybookBrand        IS NULL AND Tgt.PlaybookBrand        IS NOT NULL) OR (Src.PlaybookBrand        IS NOT NULL AND Tgt.PlaybookBrand        IS NULL))
THEN UPDATE SET
	Tgt.SegmentBrand         = Src.SegmentBrand,
	Tgt.SegmentBrandDetached = Src.SegmentBrandDetached,
	Tgt.AccountingBrand      = Src.AccountingBrand,
	Tgt.PlaybookBrand        = Src.PlaybookBrand,
	Tgt.ModificationDayKey   = @TodayDayKey
WHEN NOT MATCHED THEN 
	INSERT (
		Brand,
		SegmentBrand,
		SegmentBrandDetached,
		AccountingBrand,
		PlaybookBrand,
		ModificationDayKey,
		CreationDayKey
	)
	VALUES (
		Src.Brand,
		Src.SegmentBrand,
		Src.SegmentBrandDetached,
		Src.AccountingBrand,
		Src.PlaybookBrand,
		@TodayDayKey,
		@TodayDayKey
	)
;

SELECT 
	@RowsU = @RowsU + (@@ROWCOUNT - (COUNT(*) - @BeginRowCount)),
	@RowsI = @RowsI + (COUNT(*) - @BeginRowCount)
FROM dbo.DimBrand


/* Now, insert anything new.  Hopefully this will not happen. */
; WITH StagingData AS (
	--SELECT DISTINCT BranchBrand AS Brand FROM Lion_Staging.dbo.BrandMatrix --WHERE BranchBrand IS NOT NULL
	--UNION SELECT DISTINCT ProductBrand FROM Lion_Staging.dbo.BrandMatrix --WHERE ProductBrand IS NOT NULL
	--UNION 
	SELECT DISTINCT OwningBrandCode AS Brand FROM Lion_Staging.dbo.CustomerTerms --WHERE OwningBrandCode IS NOT NULL
	--UNION SELECT DISTINCT PrimaryBrand FROM Lion_Staging.dbo.BusUnit --WHERE PrimaryBrand IS NOT NULL
	UNION SELECT DISTINCT ProdBrandAtLLSPG FROM Lion_Staging.dbo.Product --WHERE ProdBrandAtLLSPG IS NOT NULL
)
INSERT dbo.DimBrand (
	Brand,
	SegmentBrand,
	SegmentBrandDetached,
	AccountingBrand,
	PlaybookBrand,
	ModificationDayKey,
	CreationDayKey
)
SELECT
	Brand,
	'',
	'',
	'',
	'',  -- intentially blank
	@TodayDayKey,
	@TodayDayKey
FROM StagingData
WHERE Brand IS NOT NULL
AND NOT EXISTS (SELECT 1 FROM dbo.DimBrand db WHERE db.Brand = StagingData.Brand)
SET @RowsI = @RowsI + @@ROWCOUNT

-- EXEC DBA_IndexRebuild 'DimBrand', 'Create'

SELECT TOP 10 * FROM dbo.DimBrand


EXEC LogDCPEvent 'ETL - Load_DimBrand', 'E', @RowsI, @RowsU, @RowsD


GO
