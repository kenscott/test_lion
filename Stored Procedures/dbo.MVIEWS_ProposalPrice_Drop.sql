SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

























CREATE                Procedure [dbo].[MVIEWS_ProposalPrice_Drop]
as


exec LogDCPEvent 'ETL - dbo.MVIEWS_ProposalPrice_Drop', 'B'


If Exists (selecT 1 from SysObjects where name = 'ProposalPrice_Summary_Account' and xtype = 'V')
Begin	
	Drop VIEW dbo.ProposalPrice_Summary_Account
end
If Exists (selecT 1 from SysObjects where name = 'ProposalPrice_Summary_AccountManagerKey' and xtype = 'V')
Begin	
	Drop VIEW dbo.ProposalPrice_Summary_AccountManagerKey
end
If Exists (selecT 1 from SysObjects where name = 'ProposalPrice_Summary_Item' and xtype = 'V')
Begin	
	Drop VIEW dbo.ProposalPrice_Summary_Item
end
If Exists (selecT 1 from SysObjects where name = 'ProposalPrice_AccountStatus' and xtype = 'V')
Begin	
	Drop VIEW dbo.ProposalPrice_AccountStatus
end


exec LogDCPEvent 'ETL - dbo.MVIEWS_ProposalPrice_Drop', 'E'

























GO
