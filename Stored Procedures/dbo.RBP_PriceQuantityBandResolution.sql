SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO















CREATE          PROCEDURE [dbo].[RBP_PriceQuantityBandResolution]
 (@PlaybookPricingGroupKey INT,
--  @ProjectKey int,
--  @ScenarioKey int,
  @Band1LowerPctile NUMERIC(19,8),
  @Band1UpperPctile NUMERIC(19,8),
  @Band2LowerPctile NUMERIC(19,8),
  @Band2UpperPctile NUMERIC(19,8),
  @QuantityToMatch NUMERIC(19,8),
  @RulingMemberFlag NVARCHAR(100),
  @Result NVARCHAR(100) OUT) --Pass in NULL to look at ALL data points.  Pass in 'Y' to look at only ruling data points.  Pass in 'N' to look at only non-ruling data points)
AS
BEGIN
SET QUOTED_IDENTIFIER ON 
SET ANSI_NULLS ON 
SET ARITHABORT ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
SET CONCAT_NULL_YIELDS_NULL ON
SET NUMERIC_ROUNDABORT OFF
SET NoCount ON
-- declare @Result nvarchar(50)
 DECLARE @sqlstmt NVARCHAR(500)
 DECLARE @midCount INT
 DECLARE @Band1Quantity DECIMAL(19,8)
 DECLARE @Band2Quantity DECIMAL(19,8)
 DECLARE @ExactDP1 DECIMAL(19,8), @ExactDP2 DECIMAL(19,8),@ExactDP3 DECIMAL(19,8),@ExactDP4 DECIMAL(19,8), @logging NVARCHAR(10)

 SET @Logging = 'N'

 DECLARE @tempPriceQuantityBand TABLE (RowID INT IDENTITY(1,1), col DECIMAL(19,8))

 INSERT @tempPriceQuantityBand
 SELECT Total12MonthQuantity
 FROM PlaybookDataPointPricingGroup
 WHERE PlaybookPricingGroupKey = @PlaybookPricingGroupKey
	AND RulingMemberFlag = 'Y'
 ORDER BY Total12MonthQuantity

-- declare c_med cursor SCROLL for select col from @tempPriceQuantityBand
 SELECT @ExactDP1 = COUNT(*) * @Band1LowerPctile FROM @tempPriceQuantityBand
 SELECT @ExactDP2 = COUNT(*) * @Band1UpperPctile FROM @tempPriceQuantityBand
 SELECT @ExactDP3 = COUNT(*) * @Band2LowerPctile FROM @tempPriceQuantityBand
 SELECT @ExactDP4 = COUNT(*) * @Band2UpperPctile FROM @tempPriceQuantityBand

 SET @Band1Quantity = (SELECT COUNT(*) FROM @tempPriceQuantityBand WHERE RowID BETWEEN @ExactDP1 AND @ExactDP2 AND Col = @QuantityToMatch)
 SET @Band2Quantity = (SELECT COUNT(*) FROM @tempPriceQuantityBand WHERE RowID BETWEEN @ExactDP3 AND @ExactDP4 AND Col = @QuantityToMatch)

 IF @Logging = 'Y'
 BEGIN
	 PRINT 'Q: ' + CAST(@QuantityToMatch AS NVARCHAR(100))
	 PRINT 'ExactDP1: ' + CAST(@ExactDP1 AS NVARCHAR(100))
	 PRINT 'ExactDP2: ' + CAST(@ExactDP2 AS NVARCHAR(100))
	 PRINT 'ExactDP3: ' + CAST(@ExactDP3 AS NVARCHAR(100))
	 PRINT 'ExactDP4: ' + CAST(@ExactDP4 AS NVARCHAR(100))
	
	 PRINT 'Band1Q: ' + CAST(@Band1Quantity AS NVARCHAR(100))
	 PRINT 'Band2Q: ' + CAST(@Band2Quantity AS NVARCHAR(100))
 END

 IF @Band1Quantity >= @Band2Quantity
	SET @Result = 'QuantityGoesInBand1'
 ELSE
	SET @Result = 'QuantityGoesInBand2'

END





















GO
