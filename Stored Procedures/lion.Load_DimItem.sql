SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [lion].[Load_DimItem]
    WITH EXECUTE AS CALLER
AS 
/*


UPDATE dbo.DimItem SET itemudvarchar20 = right(ItemNumber, LEN(itemnumber) - 1) WHERE ItemNumber <> N'' AND ItemNumber <> N'Unknown'
UPDATE dbo.DimItem SET itemudvarchar20 =  N'Unknown' WHERE ItemNumber = N'' OR ItemNumber = N'Unknown'

*/
    SET NOCOUNT ON

    EXEC LogDCPEvent 'ETL - Load_DimItem', 'B'


DECLARE @RowsI Quantity_Normal_type, @RowsU Quantity_Normal_type, @RowsD Quantity_Normal_type

SET @RowsI = 0
SET @RowsU = 0
SET @RowsD = 0

DECLARE 
	@TodayDayKey Key_Small_Type ,
	@UnknownItemGroup1Key Key_Small_Type ,
	@UnknownItemGroup2Key Key_Small_Type ,
	@UnknownItemGroup3Key Key_Small_Type ,
	@UnknownVendorKey Key_Normal_Type ,
	@BeginRowCount INT

SET @TodayDayKey = (SELECT DayKey FROM DimDay WHERE CONVERT(VARCHAR(10), SQLDate, 120) = CONVERT(VARCHAR(10), GETDATE(), 120))

SET @UnknownItemGroup1Key = (SELECT ItemGroup1Key FROM DimItemGroup1 WHERE IG1Level1 = 'Unknown')
SET @UnknownItemGroup2Key = (SELECT ItemGroup2Key FROM DimItemGroup2 WHERE IG2Level1 = 'Unknown')
SET @UnknownItemGroup3Key = (SELECT ItemGroup3Key FROM DimItemGroup3 WHERE IG3Level1 = 'Unknown')

SET @UnknownVendorKey = (SELECT VendorKey FROM DimVendor WHERE VendorClientUniqueIdentifier = 'Unknown')

SELECT @BeginRowCount = COUNT(*) FROM dbo.DimItem


--EXEC DBA_IndexRebuild 'DimItem', 'Drop'

--Truncate table DimItem

IF NOT EXISTS (SELECT 1 FROM DimItem WHERE ItemClientUniqueIdentifier = 'Unknown')
BEGIN
	INSERT DimItem (
		ItemVendorKey,
		ItemGroup1Key,
		ItemGroup2Key,
		ItemGroup3Key,
		ItemClientUniqueIdentifier,
		ItemNumber,
		ItemDescription,
		ItemUDDecimal1,
		ItemUDDecimal2,
		ItemUDDecimal3,
		ItemUDDecimal4,
		ItemUDDecimal5,
		CreationDayKey,
		ModificationDayKey)
	SELECT
		@UnknownVendorKey,
		@UnknownItemGroup1Key,
		@UnknownItemGroup2Key,
		@UnknownItemGroup3Key,
		'Unknown',
		'Unknown',
		'Unknown',
		0.,
		0.,
		0.,
		0.,
		0.,
		@TodayDayKey,
		@TodayDayKey
	SET @RowsI = @RowsI + @@RowCount
END

; WITH StagingData AS (
	SELECT DISTINCT
			ISNULL(V.VendorKey, @UnknownVendorKey) AS ItemVendorKey ,
			ISNULL(IG1.ItemGroup1Key, @UnknownItemGroup1Key) AS ItemGroup1Key,
			ISNULL(IG2.ItemGroup2Key, @UnknownItemGroup2Key) AS ItemGroup2Key,
			ISNULL(IG3.ItemGroup3Key, @UnknownItemGroup3Key) AS ItemGroup3Key,
			PYRCode + ProductCode AS ItemClientUniqueIdentifier ,
			PYRCode + ProductCode AS ItemNumber ,
			ISNULL(NULLIF(ProdDescription, N''), ProductCode) AS ItemDescription,	
			N'EA' AS BaseUnitOfMeasure ,
			SuppliersProdCode AS VendorStockNumber ,
			PYRCode AS ItemUDVarChar1 ,
			ProdBrandAtLLSPG AS ItemUDVarChar2 ,
			ISNULL(MPGCode, N'') AS ItemUDVarChar3 ,
			ISNULL(MPGDescription, N'') AS ItemUDVarChar4 ,
			ISNULL(SuperCategory, N'') AS ItemUDVarChar5 ,
			ISNULL(Category, N'') AS ItemUDVarChar6 ,
			ISNULL(SubCategory, N'') AS ItemUDVarChar7 ,
			ISNULL(Obsolete, N'') AS ItemUDVarChar8 ,
			ISNULL(HireInd, N'') AS ItemUDVarChar9 ,
			ISNULL(SpecialInd, N'') AS ItemUDVarChar10 ,
			ProductLifecycle AS ItemUDVarChar11 ,
			OwnBrandInd AS ItemUDVarChar12 ,
			KnownValueInd AS ItemUDVarChar13 ,
			NDPInd AS ItemUDVarChar14 ,
			MultiSourcedProductInd AS ItemUDVarChar15 ,
			VariablePackInd AS ItemUDVarChar16 ,
			ZonedInd AS ItemUDVarChar17 ,
			p.UnitCode AS ItemUDVarChar18 ,
			GenericProductID AS ItemUDVarChar19 ,
			ProductCode AS ItemUDVarChar20 ,
		
			--CASE
			--	WHEN p.ProdBrandAtLLSPG = 'BURDN' THEN 'Burdens'
			--	WHEN p.ProdBrandAtLLSPG IN ('CLIMT', 'PIPE') THEN 'Pipe&Climate'
			--	WHEN p.ProdBrandAtLLSPG = 'DRAIN' THEN 'Drain'
			--	WHEN p.ProdBrandAtLLSPG IN ('PLUMB', 'PARTS') THEN 'Plumb&Parts'
			--	ELSE ''
			--END AS ItemUDVarChar21,				-- PlaybookProductBrand/PlaybookItemBrand
			ISNULL(brand.PlaybookBrand, '') AS ItemUDVarChar21 ,				-- PlaybookProductBrand/PlaybookItemBrand

			CASE
				WHEN ISDATE(LTRIM(RTRIM(CurrentSncDate))) = 1 THEN LTRIM(RTRIM(CurrentSncDate))
				ELSE NULL
			END AS ItemUDVarChar22,

			--CAST(FutureDateApplicable AS DATE) AS ItemUDDate2,
			--CAST(CurrentSncDate AS DATE) AS ItemUDDate3,
			--CAST(CurrBranchCostDate AS DATE) AS ItemUDDate4,
			--CAST(CurrBranchTradePriceDate AS DATE) AS ItemUDDate5,

			CAST(ISNULL(CurrBranchTradePrice, 0.) AS DEC(38,8)) 
				* ( ISNULL(CAST(SellingConvFactor AS Decimal(19,3)) / 1000, 1) / ISNULL(CAST(PricingConvFact AS Decimal(19,3)) / 1000, 1) ) AS ItemUDDecimal1 ,
			
			CAST(ISNULL(RetailPrice, 0.) AS DEC(38,8)) AS ItemUDDecimal2 ,
			
			CAST(ISNULL(CurrentBranchCost, 0.) AS DEC(38,8))
				* (ISNULL(CAST(SellingConvFactor AS Decimal(19,3)) / 1000, 1) / ISNULL(CAST(CostingConvFactor AS Decimal(19,3)) / 1000, 1) ) AS ItemUDDecimal3,

			CAST(ISNULL(Padding, 0.) AS DEC(38,8))
				* (ISNULL(CAST(SellingConvFactor AS Decimal(19,3)) / 1000, 1) / ISNULL(CAST(CostingConvFactor AS Decimal(19,3)) / 1000, 1) ) AS ItemUDDecimal4,

			CAST(ISNULL(CurrNotSettVal, 0.) AS DEC(38,8))
				* (ISNULL(CAST(SellingConvFactor AS Decimal(19,3)) / 1000, 1) / ISNULL(CAST(CostingConvFactor AS Decimal(19,3)) / 1000, 1) ) AS ItemUDDecimal5,

			CAST(p.DateForDeletion AS DATE) AS ItemUDDate1,
			CAST(NULLIF(CurrBranchCostDate, N'') AS DATE) AS ItemUDDate2,
			CAST(NULLIF(CurrBranchTradePriceDate, N'') AS DATE) AS ItemUDDate3,

			CASE
				WHEN p.DeletedProduct = '' AND p.DateForDeletion = '' THEN 0
				ELSE 1
			END InActive,

			( CAST(ISNULL(CurrentBranchCost, 0.) AS DEC(38,8)) - CAST(ISNULL(Padding, 0.) AS DEC(38, 8)) - CAST(ISNULL(CurrNotSettVal, 0.) AS DEC(38,8)) )
				* (ISNULL(CAST(SellingConvFactor AS Decimal(19,3)) / 1000, 1) / ISNULL(CAST(CostingConvFactor AS Decimal(19,3)) / 1000, 1) ) AS CurrentCost ,

			CAST(ISNULL(CurrentInvoiceCost, 0.) AS DEC(38,8))
				* (ISNULL(CAST(SellingConvFactor AS Decimal(19,3)) / 1000, 1) / ISNULL(CAST(CostingConvFactor AS Decimal(19,3)) / 1000, 1) ) AS CurrentInvoiceCost ,

			ROW_NUMBER() OVER ( PARTITION BY PYRCode+ ProductCode ORDER BY PYRCode + ProductCode COLLATE SQL_Latin1_General_CP1_CS_AS DESC ) AS RowRank
		FROM Lion_Staging.dbo.Product p 
		--Below two lines replaced due to LP2P-818
		--LEFT JOIN Lion_Staging.dbo.UnitCode uc
		--	ON REPLICATE('0', 3 - LEN(UnitCodeNumber)) + UnitCodeNumber = p.UnitCode
		LEFT JOIN Lion_Staging.dbo.UnitsOfMeasure uc --new code, kbs Lp2p-818 03.16.2016
			ON uc.UnitCode = p.UnitCode                  --new code, kbs Lp2p-818 03.16.2016
		LEFT JOIN DimVendor V 
			ON V.VendorClientUniqueIdentifier = p.SupplierCode
		LEFT JOIN DimItemGroup1 IG1 
			ON IG1.IG1Level1 = p.MPGCode
			AND IG1.IG1Level2 = p.PYRCode
		LEFT JOIN DimItemGroup2 IG2 
			ON IG2.IG2Level1 = p.ProdBrandAtLLSPG
		LEFT JOIN DimItemGroup3 IG3 
			ON IG3.IG3Level1 = p.LLSPGCode
			AND IG3.IG3Level4 = p.PYRCode
		LEFT JOIN dbo.DimBrand brand 
			ON brand.Brand = p.ProdBrandAtLLSPG
		WHERE    
			ProductCode IS NOT NULL
			AND p.ETLRowStatusCodeKey = 0
	)
MERGE dbo.DimItem AS Tgt
USING
		( 
		SELECT
				ItemVendorKey ,
				ItemGroup1Key ,
				ItemGroup2Key ,
				ItemGroup3Key ,
				ItemClientUniqueIdentifier ,
				ItemNumber ,
				ItemDescription ,
				BaseUnitOfMeasure ,
				VendorStockNumber ,
				ItemUDVarChar1 ,
				ItemUDVarChar2 ,
				ItemUDVarChar3 ,
				ItemUDVarChar4 ,
				ItemUDVarChar5 ,
				ItemUDVarChar6 ,
				ItemUDVarChar7 ,
				ItemUDVarChar8 ,
				ItemUDVarChar9 ,
				ItemUDVarChar10 ,
				ItemUDVarChar11 ,
				ItemUDVarChar12 ,
				ItemUDVarChar13 ,
				ItemUDVarChar14 ,
				ItemUDVarChar15 ,
				ItemUDVarChar16 ,
				ItemUDVarChar17 ,
				ItemUDVarChar18 ,
				ItemUDVarChar19 ,
				ItemUDVarChar20 ,
				ItemUDVarChar21 ,
				ItemUDVarChar22,
				ItemUDDecimal1 ,
				ItemUDDecimal2 ,
				ItemUDDecimal3 ,
				ItemUDDecimal4 ,
				ItemUDDecimal5 ,
				ItemUDDate1 ,
				ItemUDDate2,
				ItemUDDate3,
				InActive ,
				CurrentCost ,
				CurrentInvoiceCost
		FROM StagingData Src
		WHERE
			RowRank = 1
	) AS Src
ON Tgt.ItemClientUniqueIdentifier = Src.ItemClientUniqueIdentifier
WHEN MATCHED AND
	CHECKSUM(--CheckSum for target table.  This needs to check all rows that we will UPDATE IF a find a change
		Tgt.ItemVendorKey,
		Tgt.ItemGroup1Key,
		Tgt.ItemGroup2Key,
		Tgt.ItemGroup3Key,
		Tgt.ItemClientUniqueIdentifier,
		Tgt.ItemNumber,
		Tgt.ItemDescription,	
		Tgt.BaseUnitOfMeasure,
		Tgt.VendorStockNumber,
		Tgt.ItemUDVarChar1,
		Tgt.ItemUDVarChar2,
		Tgt.ItemUDVarChar3,
		Tgt.ItemUDVarChar4,
		Tgt.ItemUDVarChar5,
		Tgt.ItemUDVarChar6,
		Tgt.ItemUDVarChar7,
		Tgt.ItemUDVarChar8,
		Tgt.ItemUDVarChar9,
		Tgt.ItemUDVarChar10,
		Tgt.ItemUDVarChar11,
		Tgt.ItemUDVarChar12,
		Tgt.ItemUDVarChar13,
		Tgt.ItemUDVarChar14,
		Tgt.ItemUDVarChar15,
		Tgt.ItemUDVarChar16,
		Tgt.ItemUDVarChar17,
		Tgt.ItemUDVarChar18,
		Tgt.ItemUDVarChar19,
		Tgt.ItemUDVarChar20,
		Tgt.ItemUDVarChar21,
		Tgt.ItemUDVarChar22,
		Tgt.ItemUDDecimal1,
		Tgt.ItemUDDecimal2,
		Tgt.ItemUDDecimal3,
		Tgt.ItemUDDecimal4,
		Tgt.ItemUDDecimal5,
		Tgt.ItemUDDate1,
		Tgt.ItemUDDate2,
		Tgt.ItemUDDate3,
		Tgt.InActive,
		Tgt.CurrentCost,
		Tgt.CurrentInvoiceCost
	)				
	<>
	CHECKSUM(--CheckSum for staging table
		Src.ItemVendorKey,
		Src.ItemGroup1Key,
		Src.ItemGroup2Key,
		Src.ItemGroup3Key,
		Src.ItemClientUniqueIdentifier,
		Src.ItemNumber,
		Src.ItemDescription,	
		Src.BaseUnitOfMeasure,
		Src.VendorStockNumber,
		Src.ItemUDVarChar1,
		Src.ItemUDVarChar2,
		Src.ItemUDVarChar3,
		Src.ItemUDVarChar4,
		Src.ItemUDVarChar5,
		Src.ItemUDVarChar6,
		Src.ItemUDVarChar7,
		Src.ItemUDVarChar8,
		Src.ItemUDVarChar9,
		Src.ItemUDVarChar10,
		Src.ItemUDVarChar11,
		Src.ItemUDVarChar12,
		Src.ItemUDVarChar13,
		Src.ItemUDVarChar14,
		Src.ItemUDVarChar15,
		Src.ItemUDVarChar16,
		Src.ItemUDVarChar17,
		Src.ItemUDVarChar18,
		Src.ItemUDVarChar19,
		Src.ItemUDVarChar20,
		Src.ItemUDVarChar21,
		Src.ItemUDVarChar22,
		Src.ItemUDDecimal1,
		Src.ItemUDDecimal2,
		Src.ItemUDDecimal3,
		Src.ItemUDDecimal4,
		Src.ItemUDDecimal5,
		Src.ItemUDDate1,
		Src.ItemUDDate2,
		Src.ItemUDDate3,
		Src.InActive,
		Src.CurrentCost,
		Src.CurrentInvoiceCost
	)
	OR Tgt.ItemUDDecimal1 <> Src.ItemUDDecimal1
	OR Tgt.ItemUDDecimal3 <> Src.ItemUDDecimal3
	OR Tgt.ItemUDDecimal4 <> Src.ItemUDDecimal4
	OR Tgt.ItemUDDecimal5 <> Src.ItemUDDecimal5
	THEN UPDATE SET
		Tgt.ItemVendorKey = Src.ItemVendorKey,
		Tgt.ItemGroup1Key = Src.ItemGroup1Key, 
		Tgt.ItemGroup2Key = Src.ItemGroup2Key, 
		Tgt.ItemGroup3Key = Src.ItemGroup3Key, 
		Tgt.ItemClientUniqueIdentifier = Src.ItemClientUniqueIdentifier, 
		Tgt.ItemNumber = Src.ItemNumber, 
		Tgt.ItemDescription = Src.ItemDescription,	 	
		Tgt.BaseUnitOfMeasure = Src.BaseUnitOfMeasure, 
		Tgt.VendorStockNumber = Src.VendorStockNumber, 
		Tgt.ItemUDVarChar1 = Src.ItemUDVarChar1, 
		Tgt.ItemUDVarChar2 = Src.ItemUDVarChar2, 
		Tgt.ItemUDVarChar3 = Src.ItemUDVarChar3, 
		Tgt.ItemUDVarChar4 = Src.ItemUDVarChar4, 
		Tgt.ItemUDVarChar5 = Src.ItemUDVarChar5, 
		Tgt.ItemUDVarChar6 = Src.ItemUDVarChar6, 
		Tgt.ItemUDVarChar7 = Src.ItemUDVarChar7, 
		Tgt.ItemUDVarChar8 = Src.ItemUDVarChar8, 
		Tgt.ItemUDVarChar9 = Src.ItemUDVarChar9, 
		Tgt.ItemUDVarChar10 = Src.ItemUDVarChar10, 
		Tgt.ItemUDVarChar11 = Src.ItemUDVarChar11, 
		Tgt.ItemUDVarChar12 = Src.ItemUDVarChar12, 
		Tgt.ItemUDVarChar13 = Src.ItemUDVarChar13, 
		Tgt.ItemUDVarChar14 = Src.ItemUDVarChar14, 
		Tgt.ItemUDVarChar15 = Src.ItemUDVarChar15, 
		Tgt.ItemUDVarChar16 = Src.ItemUDVarChar16, 
		Tgt.ItemUDVarChar17 = Src.ItemUDVarChar17, 
		Tgt.ItemUDVarChar18 = Src.ItemUDVarChar18, 
		Tgt.ItemUDVarChar19 = Src.ItemUDVarChar19,
		Tgt.ItemUDVarChar20 = Src.ItemUDVarChar20,
		Tgt.ItemUDVarChar21 = Src.ItemUDVarChar21,
		Tgt.ItemUDVarChar22 = Src.ItemUDVarChar22,
		Tgt.ItemUDDecimal1 = Src.ItemUDDecimal1,
		Tgt.ItemUDDecimal2 = Src.ItemUDDecimal2,
		Tgt.ItemUDDecimal3 = Src.ItemUDDecimal3,
		Tgt.ItemUDDecimal4 = Src.ItemUDDecimal4,
		Tgt.ItemUDDecimal5 = Src.ItemUDDecimal5,
		Tgt.ItemUDDate1 = Src.ItemUDDate1, 
		Tgt.ItemUDDate2 = Src.ItemUDDate2,
		Tgt.ItemUDDate3 = Src.ItemUDDate3,
		Tgt.InActive = Src.InActive,
		Tgt.CurrentCost = Src.CurrentCost,
		Tgt.CurrentInvoiceCost = Src.CurrentInvoiceCost,
		Tgt.ModificationDayKey = @TodayDayKey
WHEN NOT MATCHED BY Target
	THEN INSERT (
			ItemVendorKey,
			ItemGroup1Key,
			ItemGroup2Key,
			ItemGroup3Key,
			ItemClientUniqueIdentifier,
			ItemNumber,
			ItemDescription,	
			BaseUnitOfMeasure,
			VendorStockNumber,
			ItemUDVarChar1,
			ItemUDVarChar2,
			ItemUDVarChar3,
			ItemUDVarChar4,
			ItemUDVarChar5,
			ItemUDVarChar6,
			ItemUDVarChar7,
			ItemUDVarChar8,
			ItemUDVarChar9,
			ItemUDVarChar10,
			ItemUDVarChar11,
			ItemUDVarChar12,
			ItemUDVarChar13,
			ItemUDVarChar14,
			ItemUDVarChar15,
			ItemUDVarChar16,
			ItemUDVarChar17,
			ItemUDVarChar18,
			ItemUDVarChar19,
			ItemUDVarChar20,
			ItemUDVarChar21,
			ItemUDVarChar22,
			ItemUDDecimal1,
			ItemUDDecimal2,
			ItemUDDecimal3,
			ItemUDDecimal4,
			ItemUDDecimal5,
			ItemUDDate1,
			ItemUDDate2,
			ItemUDDate3,
			InActive,
			CurrentCost,
			CurrentInvoiceCost,
			CreationDayKey,
			ModificationDayKey)
		VALUES (
			Src.ItemVendorKey,
			Src.ItemGroup1Key,
			Src.ItemGroup2Key,
			Src.ItemGroup3Key,
			Src.ItemClientUniqueIdentifier,
			Src.ItemNumber,
			Src.ItemDescription,	
			Src.BaseUnitOfMeasure,
			Src.VendorStockNumber,
			Src.ItemUDVarChar1,
			Src.ItemUDVarChar2,
			Src.ItemUDVarChar3,
			Src.ItemUDVarChar4,
			Src.ItemUDVarChar5,
			Src.ItemUDVarChar6,
			Src.ItemUDVarChar7,
			Src.ItemUDVarChar8,
			Src.ItemUDVarChar9,
			Src.ItemUDVarChar10,
			Src.ItemUDVarChar11,
			Src.ItemUDVarChar12,
			Src.ItemUDVarChar13,
			Src.ItemUDVarChar14,
			Src.ItemUDVarChar15,
			Src.ItemUDVarChar16,
			Src.ItemUDVarChar17,
			Src.ItemUDVarChar18,
			Src.ItemUDVarChar19,
			Src.ItemUDVarChar20,
			Src.ItemUDVarChar21,
			Src.ItemUDVarChar22,
			Src.ItemUDDecimal1,
			Src.ItemUDDecimal2,
			Src.ItemUDDecimal3,
			Src.ItemUDDecimal4,
			Src.ItemUDDecimal5,
			Src.ItemUDDate1,
			Src.ItemUDDate2,
			Src.ItemUDDate3,
			Src.InActive,
			Src.CurrentCost,
			Src.CurrentInvoiceCost,
			@TodayDayKey,
			@TodayDayKey
			)
WHEN NOT MATCHED BY Source
THEN UPDATE SET
		Tgt.ModificationDayKey = @TodayDayKey,
		Tgt.InActive = 1


;


SELECT 
	@RowsU = @@RowCOunt - (COUNT(*) - @BeginRowCount),
	@RowsI =  COUNT(*) - @BeginRowCount
FROM dbo.DimItem




--EXEC DBA_IndexRebuild 'DimItem', 'Create'




SELECT TOP 10 * FROM DimItem

EXEC LogDCPEvent 'ETL - Load_DimItem', 'E', @RowsI, @RowsU, @RowsD


SET QUOTED_IDENTIFIER ON




GO
