SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO










CREATE         PROCEDURE [dbo].[RBP_CalculateItemQuantityBands]
@PlaybookDataPointGroupKey Key_Normal_Type,
@B_ItemPercentage Quantity_Normal_Type
AS

SET NoCount ON

DECLARE @TotalRemainingQuantity DECIMAL(38,8), @40PercentQuantity DECIMAL(38,8), @CurrentQuantity DECIMAL(38,8)
DECLARE @ItemKey Key_Normal_Type, @ItemQuantity Quantity_Normal_Type

DECLARE @RowsI Quantity_Normal_type, @RowsU Quantity_Normal_type, @RowsD Quantity_Normal_type

DECLARE @ProjectKey key_normal_type
SELECT @ProjectKey = ProjectKey FROM PlaybookDataPointGroup WHERE PlaybookDataPointGroupKey = @PlaybookDataPointGroupKey

EXEC LogDCPEvent 'DCP --- RBP_CalculateItemQuantityBands', 'B',0,0,0,@ProjectKey
SET @RowsI = 0
SET @RowsU = 0
SET @RowsD = 0


--First, set all QuantityBands to NULL
--Update DimItem set ItemQuantityBand = NULL

--This sets the top 200 items to A items
	UPDATE PlaybookDataPointGroupItemBands
	SET ItemQuantityBand = 'A'
	WHERE 	ItemKey IN (
					SELECT TOP 200 ItemKey
					FROM FactInvoiceLine FIL
					WHERE Last12MonthsIndicator = 'Y'
					GROUP BY ItemKey ORDER BY SUM(TotalQuantity) DESC)
			AND PlaybookDataPointGroupKey = @PlaybookDataPointGroupKey
	SET @RowsU = @RowsU + @@RowCount


--Now, find the remaining total quantity (excluding A items)
SELECT @TotalRemainingQuantity = SUM(TotalQuantity)
FROM FactInvoiceLine FIL
WHERE FIL.ItemKey NOT IN (SELECT ItemKey FROM PlaybookDataPointGroupItemBands WHERE ItemQuantityBand IS NOT NULL AND PlaybookDataPointGroupKey = @PlaybookDataPointGroupKey)
AND Last12MonthsIndicator = 'Y'

SET @40PercentQuantity = @TotalRemainingQuantity * @B_ItemPercentage

--This will set B items to items representing 80% of the total quantity remaining
SET @CurrentQuantity = 0
DECLARE A CURSOR FOR
SELECT ItemKey, SUM(TotalQuantity)
FROM FactInvoiceLine FIL
WHERE ItemKey NOT IN (SELECT ItemKEy FROM PlaybookDataPointGroupItemBands WHERE ItemQuantityBand IS NOT NULL  AND PlaybookDataPointGroupKey = @PlaybookDataPointGroupKey)
	AND Last12MonthsIndicator = 'Y'
GROUP BY ItemKey ORDER BY SUM(TotalQuantity) DESC
OPEN A
FETCH NEXT FROM A INTO @ItemKey, @ItemQuantity
WHILE @@fetch_STatus = 0 AND @CurrentQuantity <= @40PercentQuantity
BEGIN

-- 	Update DimItem set ItemQuantityBand = 'B'
-- 	Where ItemKey = @ItemKey
	UPDATE PlaybookDataPointGroupItemBands
	SET ItemQuantityBand = 'B'
	WHERE ItemKey = @ItemKey
	AND PlaybookDataPointGroupKey = @PlaybookDataPointGroupKey
	SET @RowsU = @RowsU + @@RowCount


	--Increase the current quantity until we get to 80% of the remaining total
	SET @CurrentQuantity = @CurrentQuantity + @ItemQuantity
	
	FETCH NEXT FROM A INTO @ItemKey, @ItemQuantity
END
CLOSE A
DEALLOCATE A

--Everything else is a C item
-- Update DimItem set ItemQuantityBand = 'C'
-- Where ItemQuantityBand is null
UPDATE PlaybookDataPointGroupItemBands
SET ItemQuantityBand = 'C'
WHERE ItemQuantityBand IS NULL
AND PlaybookDataPointGroupKey = @PlaybookDataPointGroupKey
SET @RowsU = @RowsU + @@RowCount



EXEC LogDCPEvent 'DCP --- RBP_CalculateItemQuantityBands', 'E', @RowsI, @RowsU, @RowsD










GO
