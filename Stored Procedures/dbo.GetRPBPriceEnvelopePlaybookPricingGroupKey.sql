SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO






CREATE PROCEDURE [dbo].[GetRPBPriceEnvelopePlaybookPricingGroupKey]
	@ParamAccountKey Key_Normal_type,
	@ParamItemKey Key_Normal_type,
	@ParamInvoiceLineGroup1Key Key_Normal_type = 4,		-- shipment type key; default to stock
	@ScenarioKey Key_Normal_type,
	@PlaybookDataPointGroupKey Key_Normal_type,
	@ParamPlaybookPricingGroupKey Key_Normal_type OUTPUT,
	@ParamDebug CHAR(1) = 'N'
AS

SET NOCOUNT ON


DECLARE
	@GroupingColumnsSearchStr NVARCHAR(MAX),
	@GroupValuesSearchStr NVARCHAR(MAX),
	@SQLString NVARCHAR(MAX),
	@PricingRuleSequenceNumber INT,
	@PricingRuleAttributeSequenceNumber INT,
	@PricingRuleAttributeType NVARCHAR(MAX),
	@PricingRuleAttributeSourceTable NVARCHAR(MAX),
	@DefaultValueForMatch Description_Normal_type,
	@StrValue NVARCHAR(255)


SET @GroupValuesSearchStr = N''
SET @GroupingColumnsSearchStr = N''
SET @SQLString = N''


DECLARE PricingRuleSequenceCursor 
CURSOR 
FAST_FORWARD 
FOR
	SELECT 
		--TOP 1
		PricingRuleSequenceNumber,
		PricingRuleAttributeSequenceNumber,
		PricingRuleAttributeType,
		PricingRuleAttributeSourceTable,
		DefaultValueForMatch
	FROM dbo.ATKPricingRuleAttribute pra (NOLOCK)
	INNER JOIN dbo.ATKPricingRuleAttributeType prat  (NOLOCK)
		ON prat.PricingRuleAttributeTypeKey = pra.PricingRuleAttributeTypeKey
	INNER JOIN dbo.ATKPricingRule pr (NOLOCK)
		ON pr.PricingRuleKey = pra.PricingRuleKey
	INNER JOIN dbo.ATKScenario s (NOLOCK)
		ON s.PricingRulePlaybookKey = pr.PricingRulePlaybookKey
	WHERE ScenarioKey = @ScenarioKey
	ORDER BY
		pr.PricingRuleSequenceNumber,
		pra.PricingRuleAttributeSequenceNumber

OPEN PricingRuleSequenceCursor

FETCH NEXT FROM PricingRuleSequenceCursor 
INTO 	
	@PricingRuleSequenceNumber,
	@PricingRuleAttributeSequenceNumber,
	@PricingRuleAttributeType,
	@PricingRuleAttributeSourceTable,
	@DefaultValueForMatch


SET @ParamPlaybookPricingGroupKey = NULL 


WHILE (@@FETCH_STATUS = 0) AND (@ParamPlaybookPricingGroupKey IS NULL)
BEGIN

	--/** override(s) **/
	--IF @PricingRuleAttributeSourceTable = 'DimAccount' AND @PricingRuleAttributeType = 'AccountUDVarChar7' AND @ParamNationalAccountIndicator IS NOT NULL  -- then we want to use a specific, passed in value; thus no need to query for one
	--BEGIN

	--	SET @StrValue = @ParamNationalAccountIndicator

	--END
	--ELSE IF @PricingRuleAttributeSourceTable IN ('FactLastPriceAndCost') AND @PricingRuleAttributeType = 'UDVarchar5' -- StrategicItemIndicator INDUSTRIAL
	--BEGIN
		
	--	IF @StrategicItemIndicator IS NULL	-- then we have not yet determined it yet, go looking
	--	BEGIN
		
	--		-- if there was a previous sale, the value will be in FactLastPriceAndCost
	--		SELECT
	--			@StrategicItemIndicator = UDVarchar5
	--		FROM dbo.FactLastPriceAndCost
	--		WHERE 
	--				AccountKey = @ParamAccountKey
	--				AND ItemKey = @ParamItemKey
	--				AND InvoiceLineGroup1Key = @ParamInvoiceLineGroup1Key

	--		-- if not in FacatLastPriceAndCost then determine if products with this items attributes are considered strategic; query FactInviceLine
	--		IF @StrategicItemIndicator IS NULL
	--		BEGIN
			
	--			SELECT
	--				@StrategicItemIndicator = 
	--					CASE
	--						WHEN SUM(TotalActualPrice) / (SELECT SUM(TotalActualPrice) AS TotalAccountSales FROM dbo.FactInvoiceLine WHERE Last12MonthsIndicator = 'Y' AND AccountKey = @ParamAccountKey) > 0.10 THEN 'Y'
	--						ELSE 'N'
	--					END
	--			FROM dbo.FactInvoiceLine fil
	--			INNER JOIN dbo.DimItem di
	--				ON di.ItemKey = fil.ItemKey
	--			INNER JOIN dbo.DimItem di2	-- item quoting
	--				ON 	di2.ItemVendorKey = di.ItemVendorKey
	--				AND di2.ItemUDVarchar10 = di.ItemUDVarchar10
	--				AND di2.ItemUDVarchar11 = di.ItemUDVarchar11
	--				AND di2.ItemUDVarchar13 = di.ItemUDVarchar13
	--			WHERE Last12MonthsIndicator = 'Y'
	--				AND AccountKey = @ParamAccountKey
	--				AND di2.ItemKey = @ParamItemKey
	--			GROUP BY 
	--				AccountKey,
	--				VendorKey,
	--				di.ItemUDVarchar10,
	--				di.ItemUDVarchar11,
	--				di.ItemUDVarchar13			

	--		END
	
	--	END
			
	--	SELECT @StrategicItemIndicator = ISNULL(@StrategicItemIndicator, @DefaultValueForMatch)
	--	SELECT @StrValue = @StrategicItemIndicator
		
	--END

	--ELSE IF @PricingRuleAttributeSourceTable IN ('FactLastPriceAndCost') AND @PricingRuleAttributeType = 'UDVarchar6' -- StrategicItemIndicator PRINT
	--BEGIN
		
	--	IF @StrategicItemIndicator IS NULL	-- then we have not yet determined it yet, go looking
	--	BEGIN
		
	--		-- if there was a previous sale, the value will be in FactLastPriceAndCost
	--		SELECT
	--			@StrategicItemIndicator = UDVarchar6
	--		FROM dbo.FactLastPriceAndCost
	--		WHERE 
	--				AccountKey = @ParamAccountKey
	--				AND ItemKey = @ParamItemKey
	--				AND InvoiceLineGroup1Key = @ParamInvoiceLineGroup1Key

	--		-- if not in FacatLastPriceAndCost then determine if products with this items attributes are considered strategic; query FactInviceLine
	--		IF @StrategicItemIndicator IS NULL
	--		BEGIN
			
	--			SELECT
	--				@StrategicItemIndicator = 
	--					CASE
	--						WHEN SUM(TotalActualPrice) / (SELECT SUM(TotalActualPrice) AS TotalAccountSales FROM dbo.FactInvoiceLine WHERE Last12MonthsIndicator = 'Y' AND AccountKey = @ParamAccountKey) > 0.10 THEN 'Y'
	--						ELSE 'N'
	--					END
	--			FROM dbo.FactInvoiceLine fil
	--			INNER JOIN dbo.DimItem di
	--				ON di.ItemKey = fil.ItemKey
	--			INNER JOIN dbo.DimItem di2	-- item quoting
	--				ON 	di2.ItemVendorKey = di.ItemVendorKey
	--				AND di2.ItemUDVarchar10 = di.ItemUDVarchar10
	--				AND di2.ItemUDVarchar11 = di.ItemUDVarchar11
	--				AND di2.ItemUDVarchar13 = di.ItemUDVarchar13
	--				AND di2.ItemUDVarchar15 = di.ItemUDVarchar15
	--			WHERE Last12MonthsIndicator = 'Y'
	--				AND AccountKey = @ParamAccountKey
	--				AND di2.ItemKey = @ParamItemKey
	--			GROUP BY 
	--				AccountKey,
	--				VendorKey,
	--				di.ItemUDVarchar10,
	--				di.ItemUDVarchar11,
	--				di.ItemUDVarchar13,
	--				di.ItemUDVarchar15			

	--		END
	
	--	END
			
	--	SELECT @StrategicItemIndicator = ISNULL(@StrategicItemIndicator, @DefaultValueForMatch)
	--	SELECT @StrValue = @StrategicItemIndicator
		
	--END

	--ELSE	
	--BEGIN
	
		IF @PricingRuleAttributeSourceTable IN (
			'DimAccount', 'DimAccountGroup1', 'DimAccountGroup2', 'DimAccountGroup3',
			'DimItem', 'DimItemGroup1', 'DimItemGroup2', 'DimItemGroup3')
		BEGIN
			SET @SQLString = N'
				SELECT
					 @StrValueOut = CAST(' + @PricingRuleAttributeType + ' AS NVARCHAR(255))
				FROM (
					SELECT 
						@AccountKey AS AccountKey,
						@ItemKey AS ItemKey,
						@InvoiceLineGroup1Key AS InvoiceLineGroup1Key
					) data
				INNER JOIN dbo.DimAccount da (NOLOCK)
					ON da.AccountKey = data.AccountKey
				INNER JOIN dbo.DimAccountGroup1 dag1 (NOLOCK)
					ON dag1.AccountGroup1key = da.AccountGroup1Key
				INNER JOIN dbo.DimAccountGroup2 dag2 (NOLOCK)
					ON dag2.AccountGroup2key = da.AccountGroup2Key
				INNER JOIN dbo.DimAccountGroup3 dag3 (NOLOCK)
					ON dag3.AccountGroup3key = da.AccountGroup3Key

				INNER JOIN dbo.DimItem di (NOLOCK)
					ON di.ItemKey = data.ItemKey
				INNER JOIN dbo.DimItemGroup1 dig1 (NOLOCK)
					ON dig1.ItemGroup1key = di.ItemGroup1Key
				INNER JOIN dbo.DimItemGroup2 dig2 (NOLOCK)
					ON dig2.ItemGroup2key = di.ItemGroup2Key
				INNER JOIN dbo.DimItemGroup3 dig3 (NOLOCK)
					ON dig3.ItemGroup3key = di.ItemGroup3Key'
		END
		ELSE IF @PricingRuleAttributeSourceTable IN ('DimInvoiceLineGroup2') AND @PricingRuleAttributeType LIKE 'InvoiceLineGroup2UDVarchar1%'
		BEGIN
			/*** OVERRIDE ***/
			-- switch from the last rebate indicator to a current rebate status
			
			--SET @SQLString = N'
			--	SELECT
			--		@StrValueOut = 
			--			CASE
			--				WHEN FactRebate.AccountKey IS NULL THEN ''N''
			--				ELSE ''Y''
			--			END	 
			--	FROM (
			--		SELECT 
			--			@AccountKey AS AccountKey,
			--			@ItemKey AS ItemKey,
			--			@InvoiceLineGroup1Key AS InvoiceLineGroup1Key
			--		) data
			--	LEFT JOIN dbo.FactRebate
			--		ON FactRebate.AccountKey = data.AccountKey
			--		AND FactRebate.ItemKey = data.ItemKey'
			SET @SQLString = N'
				SELECT
					@StrValueOut = CAST(' + @PricingRuleAttributeType + ' AS NVARCHAR(255))
				FROM dbo.FactLastPriceAndCost (NOLOCK) 
				JOIN DimInvoiceLineGroup2  (NOLOCK)
					ON DimInvoiceLineGroup2.InvoiceLineGroup2Key = FactLastPriceAndCost.LastInvoiceLineGroup2Key
				WHERE 
					AccountKey = @AccountKey
					AND ItemKey = @ItemKey
					AND InvoiceLineGroup1Key = @InvoiceLineGroup1Key'
		END	
		ELSE IF @PricingRuleAttributeSourceTable IN ('DimInvoiceLineGroup3') AND @PricingRuleAttributeType LIKE 'InvoiceLineGroup3UDVarchar1%'
		BEGIN
			SET @SQLString = N'
				SELECT
					@StrValueOut = CAST(' + @PricingRuleAttributeType + ' AS NVARCHAR(255))
				FROM dbo.FactLastPriceAndCost (NOLOCK) 
				JOIN DimInvoiceLineGroup3  (NOLOCK)
					ON DimInvoiceLineGroup3.InvoiceLineGroup3Key = FactLastPriceAndCost.LastInvoiceLineGroup3Key
				WHERE 
					AccountKey = @AccountKey
					AND ItemKey = @ItemKey
					AND InvoiceLineGroup1Key = @InvoiceLineGroup1Key'
		END		
		--ELSE IF @PricingRuleAttributeSourceTable IN ('FactLastPriceAndCost') AND @PricingRuleAttributeType = 'UDVarchar7'
		--BEGIN
		--	/*** OVERRIDE: CDI ***/
		--	SELECT @SQLString = N'
		--		SELECT
		--			@StrValueOut = 
		--				CASE
		--					WHEN LEFT(di.ItemDescription, 1) = ''@'' THEN ''Y''
		--					WHEN StockStatus IS NULL OR CDIFlag IS NULL THEN ''N''
		--					WHEN StockStatus = ''CS'' OR StockStatus = ''CA''
		--					  OR CDIFlag = ''B'' OR CDIFlag = ''C'' OR CDIFlag = ''P'' THEN ''Y''
		--					ELSE ' + 
		--						CASE 
		--							WHEN @DefaultValueForMatch IS NULL THEN '''N'''
		--							ELSE '''' + @DefaultValueForMatch + ''''
		--						END + ' 
		--				END	 
		--		FROM (
		--			SELECT 
		--				@AccountKey AS AccountKey,
		--				@ItemKey AS ItemKey,
		--				@InvoiceLineGroup1Key AS InvoiceLineGroup1Key
		--			) data
		--		INNER JOIN dbo.DimAccount da 
		--			ON da.AccountKey = data.AccountKey
		--		INNER JOIN dbo.DimItem di
		--			ON di.ItemKey = data.ItemKey
		--		LEFT JOIN bearcat.PlaybookAccountGroup3Item PlaybookAccountGroup3Item
		--			ON PlaybookAccountGroup3Item.AccountGroup3Key = da.FirstAlternateAccountGroup3Key
		--			AND PlaybookAccountGroup3Item.ItemKey = di.ItemKey
		--		'
		--END
		ELSE IF @PricingRuleAttributeSourceTable IN ('FactLastPriceAndCost') AND @PricingRuleAttributeType LIKE 'UDVarchar%' --	IN ('UDVarchar1', 'UDVarchar5', 'UDVarchar6', 'UDVarchar7')
		BEGIN
			SET @SQLString = N'
				SELECT
					@StrValueOut = CAST(' + @PricingRuleAttributeType + ' AS NVARCHAR(255))
				FROM dbo.FactLastPriceAndCost (NOLOCK) 
				WHERE 
					AccountKey = @AccountKey
					AND ItemKey = @ItemKey
					AND InvoiceLineGroup1Key = @InvoiceLineGroup1Key'
		END
		ELSE IF @PricingRuleAttributeSourceTable IN ('DimInvoiceLineGroup1') AND @PricingRuleAttributeType = 'InvoiceLineGroup1UDVarchar1'
		BEGIN
			SET @SQLString = N'
				SELECT
					@StrValueOut = InvoiceLineGroup1UDVarchar1
				FROM (
					SELECT 
						@AccountKey AS AccountKey,
						@ItemKey AS ItemKey,
						@InvoiceLineGroup1Key AS InvoiceLineGroup1Key
					) data
				JOIN dbo.DimInvoiceLineGroup1 dilg1last (NOLOCK) 
					ON dilg1last.InvoiceLineGroup1Key = data.InvoiceLineGroup1Key'
		END

		ELSE IF @PricingRuleAttributeSourceTable IN ('DimInvoiceLineGroup1') AND @PricingRuleAttributeType = 'InvoiceLineGroup1UDVarchar2'
		BEGIN
			SET @SQLString = N'
				SELECT
					@StrValueOut = InvoiceLineGroup1UDVarchar2
				FROM (
					SELECT 
						@AccountKey AS AccountKey,
						@ItemKey AS ItemKey,
						@InvoiceLineGroup1Key AS InvoiceLineGroup1Key
					) data
				JOIN dbo.DimInvoiceLineGroup1 dilg1last (NOLOCK) 
					ON dilg1last.InvoiceLineGroup1Key = data.InvoiceLineGroup1Key'
		END
		ELSE IF @PricingRuleAttributeSourceTable IN ('DimInvoiceLineGroup1') AND @PricingRuleAttributeType = 'InvoiceLineGroup1UDVarchar3'
		BEGIN
			SET @SQLString = N'
				SELECT
					@StrValueOut = InvoiceLineGroup1UDVarchar3
				FROM (
					SELECT 
						@AccountKey AS AccountKey,
						@ItemKey AS ItemKey,
						@InvoiceLineGroup1Key AS InvoiceLineGroup1Key
					) data
				JOIN dbo.DimInvoiceLineGroup1 dilg1last (NOLOCK) 
					ON dilg1last.InvoiceLineGroup1Key = data.InvoiceLineGroup1Key'
		END
		ELSE IF @PricingRuleAttributeSourceTable IN ('DimInvoiceLineGroup1') AND @PricingRuleAttributeType = 'InvoiceLineGroup1UDVarchar4'
		BEGIN
			SET @SQLString = N'
				SELECT
					@StrValueOut = InvoiceLineGroup1UDVarchar4
				FROM (
					SELECT 
						@AccountKey AS AccountKey,
						@ItemKey AS ItemKey,
						@InvoiceLineGroup1Key AS InvoiceLineGroup1Key
					) data
				JOIN dbo.DimInvoiceLineGroup1 dilg1last (NOLOCK) 
					ON dilg1last.InvoiceLineGroup1Key = data.InvoiceLineGroup1Key'
		END
		ELSE IF @PricingRuleAttributeSourceTable IN ('DimInvoiceLineGroup1') AND @PricingRuleAttributeType = 'InvoiceLineGroup1UDVarchar5'
		BEGIN
			SET @SQLString = N'
				SELECT
					@StrValueOut = InvoiceLineGroup1UDVarchar5
				FROM (
					SELECT 
						@AccountKey AS AccountKey,
						@ItemKey AS ItemKey,
						@InvoiceLineGroup1Key AS InvoiceLineGroup1Key
					) data
				JOIN dbo.DimInvoiceLineGroup1 dilg1last (NOLOCK) 
					ON dilg1last.InvoiceLineGroup1Key = data.InvoiceLineGroup1Key'
		END


		ELSE IF @PricingRuleAttributeSourceTable IN ('DimVendor') AND @PricingRuleAttributeType IN ('VendorNumber')
		BEGIN
			SET @SQLString = N'
				SELECT
					@StrValueOut = VendorNumber
				FROM (
					SELECT 
						@AccountKey AS AccountKey,
						@ItemKey AS ItemKey,
						@InvoiceLineGroup1Key AS InvoiceLineGroup1Key
					) data
				JOIN dbo.DimItem di (NOLOCK) 
					ON di.ItemKey = data.ItemKey
				INNER JOIN dbo.DimVendor dv (NOLOCK)
					ON dv.VendorKey = di.ItemVendorKey'
		END		
		ELSE
		BEGIN
			-- something is bad/wrong if we get here
			SET @SQLString = N''
			
			SELECT 'ERROR - Unexpected playbook attribute encountered'   -- !!! do this select to cause the app to present an error (the raiserror does not seem to work)
			
			--SELECT @PricingRuleSequenceNumber
			RAISERROR ('ERROR - Unexpected playbook attribute encountered - PricingRuleSequenceNumber: %d, PricingRuleAttributeSequenceNumber: %d, PricingRuleAttributeType: %s, PricingRuleAttributeSourceTable: %s', 
				0, 1, @PricingRuleSequenceNumber, @PricingRuleAttributeSequenceNumber, @PricingRuleAttributeType, @PricingRuleAttributeSourceTable) WITH NOWAIT 
		END
	
		IF @ParamDebug = 'Y'-- or 1=1
		BEGIN
			SELECT '@SQLString: ', @SQLString
		END

		SET @StrValue = NULL	-- needs to be null for the isnull check right after the call to sp_executesql
		
		IF @SQLString <> ''
		BEGIN
			EXECUTE sp_executesql
				@SQLString,
				N'@StrValueOut NVARCHAR(255) OUTPUT, @AccountKey INT, @ItemKey INT, @InvoiceLineGroup1Key INT',
				@AccountKey = @ParamAccountKey,
				@ItemKey = @ParamItemKey,
				@InvoiceLineGroup1Key = @ParamInvoiceLineGroup1Key,
				@StrValueOut = @StrValue OUTPUT;
			SELECT @StrValue = COALESCE(@StrValue, @DefaultValueForMatch, '')	-- made the default value table driven - from dbo.ATKPricingRuleAttributeType - so that data with no sales could have an appropriate value
		END
		
	IF @PricingRuleAttributeSequenceNumber = 1 
	BEGIN
		SET @GroupValuesSearchStr = ISNULL(@StrValue, N'')				-- @StrValue could be null if there was no @SQLString build (as would be the case if this could received an attribute that it did not know how to handle)
		SET @GroupingColumnsSearchStr = @PricingRuleAttributeType
	END
	ELSE
	BEGIN 
		SET @GroupValuesSearchStr = @GroupValuesSearchStr + N',' + ISNULL(@StrValue, N'')				-- @StrValue could be null if there was no @SQLString build (as would be the case if this could received an attribute that it did not know how to handle)
		SET @GroupingColumnsSearchStr = @GroupingColumnsSearchStr + N',' + @PricingRuleAttributeType
	END

	--SELECT @GroupValuesSearchStr

	FETCH NEXT FROM PricingRuleSequenceCursor INTO 	
		@PricingRuleSequenceNumber,
		@PricingRuleAttributeSequenceNumber,
		@PricingRuleAttributeType,
		@PricingRuleAttributeSourceTable,
		@DefaultValueForMatch


	IF @PricingRuleAttributeSequenceNumber = 1 AND @GroupValuesSearchStr <> '' -- just rolled over to a new rule; stop and search
	BEGIN
		
		IF @ParamDebug = 'Y'
		BEGIN 
			SELECT 'Searching for values: ', @GroupValuesSearchStr
			SELECT 'Searching for columns: ', @GroupingColumnsSearchStr
		END
		
		SELECT @ParamPlaybookPricingGroupKey = PlaybookPricingGroupKey
		FROM dbo.PlaybookPricingGroup (NOLOCK)
		WHERE 
			PlaybookDataPointGroupKey = @PlaybookDataPointGroupKey
			AND GroupValues = @GroupValuesSearchStr
		
		SET @GroupValuesSearchStr = N''
		SET @GroupingColumnsSearchStr = N''

	END

END

CLOSE PricingRuleSequenceCursor
DEALLOCATE PricingRuleSequenceCursor


-- search last one
IF @ParamPlaybookPricingGroupKey IS NULL AND @GroupValuesSearchStr <> '' -- just rolled over to a new rule; stop and search
BEGIN
	
	IF @ParamDebug = 'Y'
	BEGIN 
		SELECT 'Searching for values: ', @GroupValuesSearchStr
		SELECT 'Searching for columns: ', @GroupingColumnsSearchStr
	END
	
	SELECT @ParamPlaybookPricingGroupKey = PlaybookPricingGroupKey
	FROM dbo.PlaybookPricingGroup (NOLOCK)
	WHERE 
		PlaybookDataPointGroupKey = @PlaybookDataPointGroupKey
		AND GroupValues = @GroupValuesSearchStr
	
	--SET @GroupValuesSearchStr = ''
	--SET @GroupingColumnsSearchStr = ''

END

IF @ParamDebug = 'Y'
BEGIN 
	SELECT '@ParamPlaybookPricingGroupKey', @ParamPlaybookPricingGroupKey
END







GO
