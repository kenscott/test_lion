SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [cm].[GetLimitList] (@WebUserKey INT)

AS

-- exec [cm].[GetLimitList] 1

--Get list of contract branches for this WebUserKey
CREATE TABLE #ContractBranchList (SortOrder INT, PrimaryAccountGroup1Key INT, Branch VARCHAR(50), BranchName VARCHAR(250), Converted BIT)

INSERT INTO #ContractBranchList EXEC [cc].[Get_WebUserContractBranches] @WebUserKey;


SELECT  
		dv.VendorNumber AS SupplierNumber,
		sc.ContractID AS ContractID,
		--di.ItemNumber AS ItemNumberOrGroup,
		di.ItemUDVarChar20 AS ItemNumberOrGroup,
		sci.LimitCurrentRatio AS LimitReachedPercent
	
FROM cc.SupplierContractItem sci
INNER JOIN cc.SupplierContract sc 
	ON sc.ContractKey = sci.ContractKey
INNER JOIN dbo.DimItem di
	ON di.ItemKey = sci.ItemKey
INNER JOIN dbo.DimVendor dv
	ON dv.VendorKey = sc.VendorKey
INNER JOIN #ContractBranchList cbl
	ON cbl.Branch COLLATE DATABASE_DEFAULT = sc.ContractOwner COLLATE DATABASE_DEFAULT 
WHERE LimitCurrentRatio >= 90.0


UNION

SELECT  
		dv.VendorNumber AS SupplierNumber,
		sc.ContractID AS ContractID,
		ISNULL(dig1.IG1Level1, dig3.IG3Level1) AS ItemNumberOrGroup,
		scig.LimitCurrentRatio AS LimitReachedPercent
	
FROM cc.SupplierContractItemGroupX scig
INNER JOIN cc.SupplierContract sc 
	ON sc.ContractKey = scig.ContractKey
INNER JOIN dbo.DimVendor dv
	ON dv.VendorKey = sc.VendorKey
INNER JOIN #ContractBranchList cbl
	ON cbl.Branch COLLATE DATABASE_DEFAULT = sc.ContractOwner COLLATE DATABASE_DEFAULT 
LEFT JOIN dbo.DimItemGroup1 dig1
	ON dig1.ItemGroup1Key = scig.ItemGroupXKey
	AND scig.ItemGroupLevel = 1
LEFT JOIN dbo.DimItemGroup3 dig3
	ON dig3.ItemGroup3Key = scig.ItemGroupXKey
	AND scig.ItemGroupLevel = 3
WHERE scig.LimitCurrentRatio >= 90.0

ORDER BY LimitCurrentRatio DESC


DROP TABLE #ContractBranchList
GO
