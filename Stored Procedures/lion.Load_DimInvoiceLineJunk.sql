SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE PROCEDURE [lion].[Load_DimInvoiceLineJunk] @FullTransactionReprocessFlag VARCHAR(1) = 'N'
AS

SET NOCOUNT ON 

EXEC LogDCPEvent 'DimInvoiceLineJunk - Insert', 'B'

--This table will contain a row for every row in the dimension table that has been changed in the staging table (type 1 update will be done)
CREATE TABLE #UpDayKeys (KeyID INTEGER)

DECLARE @RowsI Quantity_Normal_type, @RowsU Quantity_Normal_type, @RowsD Quantity_Normal_type
SET @RowsI = 0
SET @RowsU = 0
SET @RowsD = 0


DECLARE @TodayDayKey Key_Small_Type

SET @TodayDayKey = (SELECT DayKey FROM DimDay WHERE CONVERT(VARCHAR(10), SQLDate, 120) = CONVERT(VARCHAR(10), GETDATE(), 120))

--This finds every combination of ShipmentType, ConType, and LineType, assigning a Key value to each combination.  
--There are a limited # of combinations so this allows all three to be stored as one key value in the InvoiceLine table (Saving space and processing time)


IF @FullTransactionReprocessFlag = 'Y'
BEGIN
	EXEC DBA_IndexRebuild 'DimInvoiceLineJunk', 'Drop'
	TRUNCATE TABLE dbo.DimInvoiceLineJunk
END


IF NOT EXISTS (SELECT 1 FROM DimInvoiceLineJunk WHERE InvoiceLineJunkUDVarchar1 = 'Unknown')
BEGIN
	INSERT DimInvoiceLineJunk (
		InvoiceLineUniqueIdentifier,
		InvoiceLineJunkUDVarchar1,
		InvoiceLineJunkUDVarchar2,
		InvoiceLineJunkUDVarchar3,
		InvoiceLineJunkUDVarchar4,
		InvoiceLineJunkUDVarchar5,
		CreationDayKey,
		ModificationDayKey)
	VALUES (
		0,
		'Unknown', 
		'Unknown',
		'Unknown',
		'Unknown',
		'Unknown',
		@TodayDayKey,
		@TodayDayKey
	)
	SET @RowsI = @RowsI + @@ROWCOUNT
END


CREATE TABLE #UpdateKeys (KeyID INTEGER)

--Compare the checksum of the current dimension data to the checksum of the staging data
--IF we find any checksums that do not match, we will update those rows
INSERT INTO #UpdateKeys (KeyID)
SELECT junk.InvoiceLineUniqueIdentifier
FROM (
	SELECT
		AdvanousInvoiceLineId AS InvoiceLineUniqueIdentifier,

		BranchCode AS InvoiceLineJunkUDVarchar1, 
		TransNo AS InvoiceLineJunkUDVarchar2, 
		CAST(DelNo AS NVARCHAR(50)) AS InvoiceLineJunkUDVarchar3, 
		CAST(LineNumber AS NVARCHAR(50)) AS InvoiceLineJunkUDVarchar4, 
		CAST(EffectiveQty AS NVARCHAR(50)) AS InvoiceLineJunkUDVarchar5, 
		CAST(EffectiveLinePrice AS NVARCHAR(50)) AS InvoiceLineJunkUDVarchar6, 
		CAST(EffectiveApTradingMarginPadRelease AS NVARCHAR(50)) AS InvoiceLineJunkUDVarchar7, 
		DeliveryDate AS InvoiceLineJunkUDVarchar8, 
		SledgerNo AS InvoiceLineJunkUDVarchar9, 
		SledgerDate AS InvoiceLineJunkUDVarchar10, 
		YMTH AS InvoiceLineJunkUDVarchar11, 
		SupplyType AS InvoiceLineJunkUDVarchar12, 
		DocInd AS InvoiceLineJunkUDVarchar13, 
		CreditReason AS InvoiceLineJunkUDVarchar14, 
		KnownValueItem AS InvoiceLineJunkUDVarchar15, 
		CtsRelCode AS InvoiceLineJunkUDVarchar16, 
		CustCntNo AS InvoiceLineJunkUDVarchar17, 
		MthdOfDespatch AS InvoiceLineJunkUDVarchar18, 
		TransType AS InvoiceLineJunkUDVarchar19, 
		SalesOrigin AS InvoiceLineJunkUDVarchar20, 
		TakenBy AS InvoiceLineJunkUDVarchar21, 
		MethodOfPayment AS InvoiceLineJunkUDVarchar22, 
		HeaderID AS InvoiceLineJunkUDVarchar23, 
		CrdOrigInvNo AS InvoiceLineJunkUDVarchar24, 
		CrdOrigInvDate AS InvoiceLineJunkUDVarchar25, 
		CAST(OriginalOrdNo AS NVARCHAR(50)) AS InvoiceLineJunkUDVarchar26, 
		CAST(CarriagePack AS NVARCHAR(50)) AS InvoiceLineJunkUDVarchar27, 
		DelAddPhone AS InvoiceLineJunkUDVarchar28, 
		GpAllocBranchCode AS InvoiceLineJunkUDVarchar29, 
		SalesOriginDesc AS InvoiceLineJunkUDVarchar30, 
		PriceOrderType AS InvoiceLineJunkUDVarchar31, 
		CAST(TransactionDate AS NVARCHAR(50)) AS InvoiceLineJunkUDVarchar32, 
		OrderDate  AS InvoiceLineJunkUDVarchar33,
		t.AccountingBrand AS InvoiceLineJunkUDVarchar34,
		t.TradingBranchPrimaryBrand AS InvoiceLineJunkUDVarchar35,
		t.BranchBrand AS InvoiceLineJunkUDVarchar36,
		t.CustOrdRef AS InvoiceLineJunkUDVarchar37,
		t.SuppliersNettCost AS InvoiceLineJunkUDDecimal3,
		t.SuppliersNettCostMNTHDay1 AS InvoiceLineJunkUDDecimal4
	FROM Lion_Staging.dbo.Transactions t
	LEFT JOIN dbo.DimItem di
		ON di.ItemClientUniqueIdentifier = ISNULL(t.PyramidCode, N'') + t.ProductCode
	LEFT JOIN dbo.DimAccountGroup1 dag1
		ON dag1.AG1Level1 = t.BranchCode
	) Staging
JOIN dbo.DimInvoiceLineJunk junk
	ON junk.InvoiceLineUniqueIdentifier = Staging.InvoiceLineUniqueIdentifier

WHERE 
	CHECKSUM(--CheckSum for dimension table.  This needs to check all rows that we will update IF a find a change
		junk.InvoiceLineJunkUDVarchar1,
		junk.InvoiceLineJunkUDVarchar2,
		junk.InvoiceLineJunkUDVarchar3,
		junk.InvoiceLineJunkUDVarchar4,
		junk.InvoiceLineJunkUDVarchar5,
		junk.InvoiceLineJunkUDVarchar6,
		junk.InvoiceLineJunkUDVarchar7,
		junk.InvoiceLineJunkUDVarchar8,
		junk.InvoiceLineJunkUDVarchar9,
		junk.InvoiceLineJunkUDVarchar10,
		junk.InvoiceLineJunkUDVarchar11,
		junk.InvoiceLineJunkUDVarchar12,
		junk.InvoiceLineJunkUDVarchar13,
		junk.InvoiceLineJunkUDVarchar14,
		junk.InvoiceLineJunkUDVarchar15,
		junk.InvoiceLineJunkUDVarchar16,
		junk.InvoiceLineJunkUDVarchar17,
		junk.InvoiceLineJunkUDVarchar18,
		junk.InvoiceLineJunkUDVarchar19,
		junk.InvoiceLineJunkUDVarchar20,
		junk.InvoiceLineJunkUDVarchar21,
		junk.InvoiceLineJunkUDVarchar22,
		junk.InvoiceLineJunkUDVarchar23,
		junk.InvoiceLineJunkUDVarchar24,
		junk.InvoiceLineJunkUDVarchar25,
		junk.InvoiceLineJunkUDVarchar26,
		junk.InvoiceLineJunkUDVarchar27,
		junk.InvoiceLineJunkUDVarchar28,
		junk.InvoiceLineJunkUDVarchar29,
		junk.InvoiceLineJunkUDVarchar30,
		junk.InvoiceLineJunkUDVarchar31,
		junk.InvoiceLineJunkUDVarchar32,
		junk.InvoiceLineJunkUDVarchar33,
		junk.InvoiceLineJunkUDVarchar34,
		junk.InvoiceLineJunkUDVarchar35,
		junk.InvoiceLineJunkUDVarchar36,
		junk.InvoiceLineJunkUDVarchar37,
		junk.InvoiceLineJunkUDDecimal3,
		junk.InvoiceLineJunkUDDecimal4
	)
	<>
	CHECKSUM(--CheckSum for staging table
		staging.InvoiceLineJunkUDVarchar1,
		staging.InvoiceLineJunkUDVarchar2,
		staging.InvoiceLineJunkUDVarchar3,
		staging.InvoiceLineJunkUDVarchar4,
		staging.InvoiceLineJunkUDVarchar5,
		staging.InvoiceLineJunkUDVarchar6,
		staging.InvoiceLineJunkUDVarchar7,
		staging.InvoiceLineJunkUDVarchar8,
		staging.InvoiceLineJunkUDVarchar9,
		staging.InvoiceLineJunkUDVarchar10,
		staging.InvoiceLineJunkUDVarchar11,
		staging.InvoiceLineJunkUDVarchar12,
		staging.InvoiceLineJunkUDVarchar13,
		staging.InvoiceLineJunkUDVarchar14,
		staging.InvoiceLineJunkUDVarchar15,
		staging.InvoiceLineJunkUDVarchar16,
		staging.InvoiceLineJunkUDVarchar17,
		staging.InvoiceLineJunkUDVarchar18,
		staging.InvoiceLineJunkUDVarchar19,
		staging.InvoiceLineJunkUDVarchar20,
		staging.InvoiceLineJunkUDVarchar21,
		staging.InvoiceLineJunkUDVarchar22,
		staging.InvoiceLineJunkUDVarchar23,
		staging.InvoiceLineJunkUDVarchar24,
		staging.InvoiceLineJunkUDVarchar25,
		staging.InvoiceLineJunkUDVarchar26,
		staging.InvoiceLineJunkUDVarchar27,
		staging.InvoiceLineJunkUDVarchar28,
		staging.InvoiceLineJunkUDVarchar29,
		staging.InvoiceLineJunkUDVarchar30,
		staging.InvoiceLineJunkUDVarchar31,
		staging.InvoiceLineJunkUDVarchar32,
		staging.InvoiceLineJunkUDVarchar33,
		staging.InvoiceLineJunkUDVarchar34,
		staging.InvoiceLineJunkUDVarchar35,
		staging.InvoiceLineJunkUDVarchar36,
		staging.InvoiceLineJunkUDVarchar37,
		staging.InvoiceLineJunkUDDecimal3,
		staging.InvoiceLineJunkUDDecimal4
	)



--This will perform the update ON all data that changed. 
--All columns that we used IN the checksum above need the be updated
UPDATE dbo.DimInvoiceLineJunk SET 
	InvoiceLineJunkUDVarchar1 = staging.InvoiceLineJunkUDVarchar1,
	InvoiceLineJunkUDVarchar2 = staging.InvoiceLineJunkUDVarchar2,
	InvoiceLineJunkUDVarchar3 = staging.InvoiceLineJunkUDVarchar3,
	InvoiceLineJunkUDVarchar4 = staging.InvoiceLineJunkUDVarchar4,
	InvoiceLineJunkUDVarchar5 = staging.InvoiceLineJunkUDVarchar5,
	InvoiceLineJunkUDVarchar6 = staging.InvoiceLineJunkUDVarchar6,
	InvoiceLineJunkUDVarchar7 = staging.InvoiceLineJunkUDVarchar7,
	InvoiceLineJunkUDVarchar8 = staging.InvoiceLineJunkUDVarchar8,
	InvoiceLineJunkUDVarchar9 = staging.InvoiceLineJunkUDVarchar9,
	InvoiceLineJunkUDVarchar10 = staging.InvoiceLineJunkUDVarchar10,
	InvoiceLineJunkUDVarchar11 = staging.InvoiceLineJunkUDVarchar11,
	InvoiceLineJunkUDVarchar12 = staging.InvoiceLineJunkUDVarchar12,
	InvoiceLineJunkUDVarchar13 = staging.InvoiceLineJunkUDVarchar13,
	InvoiceLineJunkUDVarchar14 = staging.InvoiceLineJunkUDVarchar14,
	InvoiceLineJunkUDVarchar15 = staging.InvoiceLineJunkUDVarchar15,
	InvoiceLineJunkUDVarchar16 = staging.InvoiceLineJunkUDVarchar16,
	InvoiceLineJunkUDVarchar17 = staging.InvoiceLineJunkUDVarchar17,
	InvoiceLineJunkUDVarchar18 = staging.InvoiceLineJunkUDVarchar18,
	InvoiceLineJunkUDVarchar19 = staging.InvoiceLineJunkUDVarchar19,
	InvoiceLineJunkUDVarchar20 = staging.InvoiceLineJunkUDVarchar20,
	InvoiceLineJunkUDVarchar21 = staging.InvoiceLineJunkUDVarchar21,
	InvoiceLineJunkUDVarchar22 = staging.InvoiceLineJunkUDVarchar22,
	InvoiceLineJunkUDVarchar23 = staging.InvoiceLineJunkUDVarchar23,
	InvoiceLineJunkUDVarchar24 = staging.InvoiceLineJunkUDVarchar24,
	InvoiceLineJunkUDVarchar25 = staging.InvoiceLineJunkUDVarchar25,
	InvoiceLineJunkUDVarchar26 = staging.InvoiceLineJunkUDVarchar26,
	InvoiceLineJunkUDVarchar27 = staging.InvoiceLineJunkUDVarchar27,
	InvoiceLineJunkUDVarchar28 = staging.InvoiceLineJunkUDVarchar28,
	InvoiceLineJunkUDVarchar29 = staging.InvoiceLineJunkUDVarchar29,
	InvoiceLineJunkUDVarchar30 = staging.InvoiceLineJunkUDVarchar30,
	InvoiceLineJunkUDVarchar31 = staging.InvoiceLineJunkUDVarchar31,
	InvoiceLineJunkUDVarchar32 = staging.InvoiceLineJunkUDVarchar32,
	InvoiceLineJunkUDVarchar33 = staging.InvoiceLineJunkUDVarchar33,
	InvoiceLineJunkUDVarchar34 = staging.InvoiceLineJunkUDVarchar34,
	InvoiceLineJunkUDVarchar35 = staging.InvoiceLineJunkUDVarchar35,
	InvoiceLineJunkUDVarchar36 = staging.InvoiceLineJunkUDVarchar36,
	InvoiceLineJunkUDVarchar37 = staging.InvoiceLineJunkUDVarchar37,
	InvoiceLineJunkUDDecimal3 = staging.InvoiceLineJunkUDDecimal3,
	InvoiceLineJunkUDDecimal4 = staging.InvoiceLineJunkUDDecimal4
FROM (
	SELECT
		AdvanousInvoiceLineId AS InvoiceLineUniqueIdentifier,

		BranchCode AS InvoiceLineJunkUDVarchar1, 
		TransNo AS InvoiceLineJunkUDVarchar2, 
		CAST(DelNo AS NVARCHAR(25)) AS InvoiceLineJunkUDVarchar3, 
		LineNumber AS InvoiceLineJunkUDVarchar4, 
		EffectiveQty AS InvoiceLineJunkUDVarchar5, 
		EffectiveLinePrice AS InvoiceLineJunkUDVarchar6, 
		EffectiveApTradingMarginPadRelease AS InvoiceLineJunkUDVarchar7, 
		DeliveryDate AS InvoiceLineJunkUDVarchar8, 
		SledgerNo AS InvoiceLineJunkUDVarchar9, 
		SledgerDate AS InvoiceLineJunkUDVarchar10, 
		YMTH AS InvoiceLineJunkUDVarchar11, 
		SupplyType AS InvoiceLineJunkUDVarchar12, 
		DocInd AS InvoiceLineJunkUDVarchar13, 
		CreditReason AS InvoiceLineJunkUDVarchar14, 
		KnownValueItem AS InvoiceLineJunkUDVarchar15, 
		CtsRelCode AS InvoiceLineJunkUDVarchar16, 
		CustCntNo AS InvoiceLineJunkUDVarchar17, 
		MthdOfDespatch AS InvoiceLineJunkUDVarchar18, 
		TransType AS InvoiceLineJunkUDVarchar19, 
		SalesOrigin AS InvoiceLineJunkUDVarchar20, 
		TakenBy AS InvoiceLineJunkUDVarchar21, 
		MethodOfPayment AS InvoiceLineJunkUDVarchar22, 
		HeaderID AS InvoiceLineJunkUDVarchar23, 
		CrdOrigInvNo AS InvoiceLineJunkUDVarchar24, 
		CrdOrigInvDate AS InvoiceLineJunkUDVarchar25, 
		OriginalOrdNo AS InvoiceLineJunkUDVarchar26, 
		CarriagePack AS InvoiceLineJunkUDVarchar27, 
		DelAddPhone AS InvoiceLineJunkUDVarchar28, 
		GpAllocBranchCode AS InvoiceLineJunkUDVarchar29, 
		SalesOriginDesc AS InvoiceLineJunkUDVarchar30, 
		PriceOrderType AS InvoiceLineJunkUDVarchar31, 
		TransactionDate AS InvoiceLineJunkUDVarchar32, 
		OrderDate  AS InvoiceLineJunkUDVarchar33,
		t.AccountingBrand AS InvoiceLineJunkUDVarchar34,
		t.TradingBranchPrimaryBrand AS InvoiceLineJunkUDVarchar35,
		t.BranchBrand AS InvoiceLineJunkUDVarchar36,
		t.CustOrdRef AS InvoiceLineJunkUDVarchar37,
		t.SuppliersNettCost AS InvoiceLineJunkUDDecimal3,
		t.SuppliersNettCostMNTHDay1 AS InvoiceLineJunkUDDecimal4
FROM Lion_Staging.dbo.Transactions t
LEFT JOIN dbo.DimItem di
	ON di.ItemClientUniqueIdentifier = ISNULL(t.PyramidCode, N'') + t.ProductCode
LEFT JOIN dbo.DimAccountGroup1 dag1
	ON dag1.AG1Level1 = t.BranchCode
) Staging

WHERE 
	Staging.InvoiceLineUniqueIdentifier = dbo.DimInvoiceLineJunk.InvoiceLineUniqueIdentifier
	AND EXISTS (SELECT 1 FROM #UpdateKeys u where u.KeyID = dbo.DimInvoiceLineJunk.InvoiceLineUniqueIdentifier)
	AND dbo.DimInvoiceLineJunk.InvoiceLineUniqueIdentifier IN (SELECT KeyID FROM #UpdateKeys)
SET @RowsU = @RowsU + @@RowCount


INSERT INTO dbo.DimInvoiceLineJunk (
	InvoiceLineUniqueIdentifier,
	InvoiceLineJunkUDVarchar1,
	InvoiceLineJunkUDVarchar2,
	InvoiceLineJunkUDVarchar3,
	InvoiceLineJunkUDVarchar4,
	InvoiceLineJunkUDVarchar5,
	InvoiceLineJunkUDVarchar6,
	InvoiceLineJunkUDVarchar7,
	InvoiceLineJunkUDVarchar8,
	InvoiceLineJunkUDVarchar9,
	InvoiceLineJunkUDVarchar10,
	InvoiceLineJunkUDVarchar11,
	InvoiceLineJunkUDVarchar12,
	InvoiceLineJunkUDVarchar13,
	InvoiceLineJunkUDVarchar14,
	InvoiceLineJunkUDVarchar15,
	InvoiceLineJunkUDVarchar16,
	InvoiceLineJunkUDVarchar17,
	InvoiceLineJunkUDVarchar18,
	InvoiceLineJunkUDVarchar19,
	InvoiceLineJunkUDVarchar20,
	InvoiceLineJunkUDVarchar21,
	InvoiceLineJunkUDVarchar22,
	InvoiceLineJunkUDVarchar23,
	InvoiceLineJunkUDVarchar24,
	InvoiceLineJunkUDVarchar25,
	InvoiceLineJunkUDVarchar26,
	InvoiceLineJunkUDVarchar27,
	InvoiceLineJunkUDVarchar28,
	InvoiceLineJunkUDVarchar29,
	InvoiceLineJunkUDVarchar30,
	InvoiceLineJunkUDVarchar31,
	InvoiceLineJunkUDVarchar32,
	InvoiceLineJunkUDVarchar33,
	InvoiceLineJunkUDVarchar34,
	InvoiceLineJunkUDVarchar35,
	InvoiceLineJunkUDVarchar36,
	InvoiceLineJunkUDVarchar37,
	InvoiceLineJunkUDDecimal3,
	InvoiceLineJunkUDDecimal4,
	CreationDayKey,
	ModificationDayKey
)
SELECT DISTINCT
	AdvanousInvoiceLineId AS InvoiceLineUniqueIdentifier,
	
	BranchCode,
	TransNo,
	CAST(DelNo AS NVARCHAR(25)),
	LineNumber AS InvoiceLineJunkUDVarchar4,

	t.EffectiveQty, -- change to net
	t.EffectiveLinePrice, -- change to net
	t.EffectiveApTradingMarginPadRelease,  -- change to net
	
	DeliveryDate,
	SledgerNo,
	SledgerDate,
	YMTH,
	SupplyType,
	DocInd,
	CreditReason,
	KnownValueItem,
	CtsRelCode,
	CustCntNo,
	MthdOfDespatch,
	TransType,
	SalesOrigin,
	TakenBy,
	MethodOfPayment,
	HeaderID,
	CrdOrigInvNo,
	CrdOrigInvDate,
	OriginalOrdNo,
	CarriagePack,
	DelAddPhone,
	GpAllocBranchCode,
	SalesOriginDesc,
	PriceOrderType,
	TransactionDate,
	OrderDate,
	t.AccountingBrand AS InvoiceLineJunkUDVarchar34,
	t.TradingBranchPrimaryBrand AS InvoiceLineJunkUDVarchar35,
	t.BranchBrand AS InvoiceLineJunkUDVarchar36,
	t.CustOrdRef AS InvoiceLineJunkUDVarchar37,
	t.SuppliersNettCost AS InvoiceLineJunkUDDecimal3,
	t.SuppliersNettCostMNTHDay1 AS InvoiceLineJunkUDDecimal4,
	/* CreationDayKey */ @TodayDayKey,
	/* ModificationDayKey */ @TodayDayKey
FROM Lion_Staging.dbo.Transactions t
LEFT JOIN dbo.DimItem di
	ON di.ItemClientUniqueIdentifier = ISNULL(t.PyramidCode, N'') + t.ProductCode
LEFT JOIN dbo.DimAccountGroup1 dag1
	ON dag1.AG1Level1 = t.BranchCode
WHERE 
	t.ETLRowStatusCodeKey = 0
	AND NOT EXISTS (SELECT 1 FROM dbo.DimInvoiceLineJunk j where j.InvoiceLineUniqueIdentifier = t.AdvanousInvoiceLineId)
SET @RowsI = @RowsI + @@RowCount


IF @FullTransactionReprocessFlag = 'Y' --or (SELECT count(*) FROM FactInvoiceLine) = 0
BEGIN
	EXEC DBA_IndexRebuild 'DimInvoiceLineJunk', 'Create'
END


SELECT TOP 10 * FROM DimInvoiceLineJunk

EXEC LogDCPEvent 'DimInvoiceLineJunk - Insert', 'E', @RowsI, @RowsU, @RowsD


GO
