SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [lion].[Load_ResetAccountManagerKey]
WITH EXECUTE AS CALLER
AS

/*
EXEC lion.Load_ResetAccountManagerKey

EXEc dcplog
*/

SET NOCOUNT ON


EXEC LogDCPEvent 'DCP --- lion.Load_ResetAccountManagerKey', 'B'

DECLARE
	@RowsI Quantity_Normal_type, 
	@RowsU Quantity_Normal_type, 
	@RowsD Quantity_Normal_type

SET @RowsI = 0
SET @RowsU = 0
SET @RowsD = 0



EXEC LogDCPEvent 'DCP --- lion.Load_ResetAccountManagerKey: FactInvoiceLine', 'B'

UPDATE fil
SET
	fil.AccountManagerKey = da.AccountManagerKey
FROM dbo.FactInvoiceLine fil
INNER JOIN dbo.DimAccount da
	ON da.AccountKey = fil.AccountKey
WHERE 
	fil.AccountManagerKey <> da.AccountManagerKey
SET @RowsU = @@ROWCOUNT

EXEC LogDCPEvent 'DCP --- lion.Load_ResetAccountManagerKey: FactInvoiceLine', 'E', @RowsI, @RowsU, @RowsD


SET @RowsU = 0


EXEC LogDCPEvent 'DCP --- lion.Load_ResetAccountManagerKey: FactInvoiceLinePricebandScore', 'B'

UPDATE filpbs
SET
	filpbs.AccountManagerKey = da.AccountManagerKey
FROM dbo.FactInvoiceLinePricebandScore filpbs
INNER JOIN dbo.DimAccount da
	ON da.AccountKey = filpbs.AccountKey
WHERE 
	filpbs.AccountManagerKey <> da.AccountManagerKey
SET @RowsU = @@ROWCOUNT

EXEC LogDCPEvent 'DCP --- lion.Load_ResetAccountManagerKey: FactInvoiceLinePricebandScore', 'E', @RowsI, @RowsU, @RowsD



EXEC LogDCPEvent 'DCP --- lion.Load_ResetAccountManagerKey', 'E'






GO
