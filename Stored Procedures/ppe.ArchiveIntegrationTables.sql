SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE [ppe].[ArchiveIntegrationTables]
WITH EXECUTE AS CALLER
AS
BEGIN
	/*
	LPF-1632 Archive IntegrationEmail and IntegrationMessage
	*/

	/*
	EXEC ppe.ArchiveIntegrationTables
	*/


	SET NOCOUNT ON

	EXEC LogDCPEvent 'ETL - ppe.ArchiveIntegrationTables - IntegrationEmailArchive', 'B'

	DECLARE 
		@RowsI Quantity_Normal_type, 
		@RowsU Quantity_Normal_type, 
		@RowsD Quantity_Normal_type,
		@DayCount INT

	SET @RowsI = 0
	SET @RowsU = 0
	SET @RowsD = 0

	INSERT INTO ppe.IntegrationEmailArchive
			( FromAddress ,
			  ToAddress ,
			  IsHTML ,
			  EmailSubject ,
			  EmailBody ,
			  Retries ,
			  EmailStatus ,
			  StatusComment ,
			  CreatedDT ,
			  IntegrationEmailKey ,
			  SentDT ,
			  FromName ,
			  ToName
			)
	SELECT FromAddress ,
		   ToAddress ,
		   IsHTML ,
		   EmailSubject ,
		   EmailBody ,
		   Retries ,
		   EmailStatus ,
		   StatusComment ,
		   CreatedDT ,
		   IntegrationEmailKey ,
		   SentDT ,
		   FromName ,
		   ToName
	FROM ppe.IntegrationEmail
	WHERE EmailStatus='sent' AND CreatedDT < DATEADD(d,-30,GETDATE())
	SET @RowsI = @RowsI + @@RowCount

	DELETE FROM ppe.IntegrationEmail
	WHERE EmailStatus='sent' AND CreatedDT < DATEADD(d,-30,GETDATE())
	SET @RowsD = @RowsD + @@RowCount

	EXEC LogDCPEvent 'ETL - ppe.ArchiveIntegrationTables - IntegrationEmailArchive', 'E', @RowsI, @RowsU, @RowsD



	EXEC LogDCPEvent 'ETL - ppe.ArchiveIntegrationTables - IntegrationMessageArchive', 'B'
	INSERT INTO ppe.IntegrationMessageArchive
			( IntegrationMessageKey ,
			  [Type] ,
			  [Status] ,
			  [Message] ,
			  DateCreated ,
			  DeliveryAttempts ,
			  LastErrorTimestamp
			)
	SELECT IntegrationMessageKey ,
		   [Type] ,
		   [Status] ,
		   [Message] ,
		   DateCreated ,
		   DeliveryAttempts ,
		   LastErrorTimestamp
	FROM ppe.IntegrationMessage
	WHERE [Status]=3 AND DateCreated < DATEADD(d,-30,GETDATE())
	SET @RowsI = @RowsI + @@RowCount

	DELETE FROM ppe.IntegrationMessage
	WHERE [Status]=3 AND DateCreated < DATEADD(d,-30,GETDATE())
	SET @RowsD = @RowsD + @@RowCount

	EXEC LogDCPEvent 'ETL - ppe.ArchiveIntegrationTables - IntegrationMessageArchive', 'E', @RowsI, @RowsU, @RowsD


END


GO
