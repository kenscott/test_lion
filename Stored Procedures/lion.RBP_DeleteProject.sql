SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




CREATE
PROCEDURE [lion].[RBP_DeleteProject]
	@ProjectKey INT,
	@FullDeleteIndicator VARCHAR(1) = 'N'
AS


/*

BEGIN TRAN

--EXEC lion.DeleteOldProjectData
EXEC lion.RBP_DeleteProject 1

ROLLBACK TRAN

SELECT * FROM dbo.PlaybookProject
SELECT * FROM dbo.ATKScenario_View

*/


SET QUOTED_IDENTIFIER ON 
SET ANSI_NULLS ON 
SET ARITHABORT ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
SET CONCAT_NULL_YIELDS_NULL ON
SET NUMERIC_ROUNDABORT OFF
SET XACT_ABORT ON
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
SET NOCOUNT ON



EXEC LogDCPEvent 'lion.RBP_DeleteProject', 'B', 0, 0, 0, @ProjectKey


DECLARE
	@PlaybookDataPointGroupKey INT = NULL,
	@RowsD INT = 0

SET @RowsD = 0


PRINT '@ProjectKey: '+CAST(@ProjectKey AS VARCHAR(500))


DECLARE a CURSOR FOR
	SELECT PlaybookDataPointGroupKey 
	FROM PlaybookDataPointGroup 
	WHERE ProjectKey = @ProjectKey

OPEN a

FETCH NEXT FROM a INTO @PlaybookDataPointGroupKey
WHILE @@FETCH_STATUS = 0
BEGIN


	EXEC LogDCPEvent 'lion.RBP_DeleteProject - Deleting from PlaybookPricingGroupOptimalPrice', 'B', 0, 0, 0, @ProjectKey
	DELETE FROM PlaybookPricingGroupOptimalPrice WHERE PlaybookPricingGroupOptimalPriceKey IN (SELECT PlaybookPricingGroupOptimalPriceKey FROM PlaybookPricingGroupOptimalPrice_View WHERE PlaybookDataPointGroupKey = @PlaybookDataPointGroupKey)
	EXEC LogDCPEvent 'lion.RBP_DeleteProject - Deleting from PlaybookPricingGroupOptimalPrice', 'E', 0, 0, @@RowCount, @ProjectKey 


	IF @FullDeleteIndicator = 'Y'
	BEGIN


			EXEC LogDCPEvent 'lion.RBP_DeleteProject - Deleting from PlaybookPricingGroupPriceBand', 'B', 0, 0, 0, @ProjectKey
			DELETE FROM PlaybookPricingGroupPriceBand WHERE PlaybookPricingGroupKey IN (SELECT PlaybookPricingGroupKey FROM PlaybookPricingGroupQuantityBand_View WHERE PlaybookDataPointGroupKey = @PlaybookDataPointGroupKey)
			EXEC LogDCPEvent 'lion.RBP_DeleteProject - Deleting from PlaybookPricingGroupPriceBand', 'E', 0, 0, @@RowCount, @ProjectKey 

		
			EXEC LogDCPEvent 'lion.RBP_DeleteProject - Deleting from PlaybookPricingGroupQuantityBand A', 'B', 0, 0, 0, @ProjectKey
			DELETE FROM PlaybookPricingGroupQuantityBand WHERE PlaybookPricingGroupQuantityBandKey IN (SELECT PlaybookPricingGroupQuantityBandKey FROM PlaybookPricingGroupQuantityBand_View WHERE PlaybookDataPointGroupKey = @PlaybookDataPointGroupKey)
			EXEC LogDCPEvent 'lion.RBP_DeleteProject - Deleting from PlaybookPricingGroupQuantityBand A', 'E', 0, 0, @@RowCount, @ProjectKey 


			EXEC LogDCPEvent 'lion.RBP_DeleteProject - Deleting from PlaybookPricingGroupQuantityBand B', 'B', 0, 0, 0, @ProjectKey
			DELETE FROM PlaybookPricingGroupQuantityBand WHERE PlaybookPricingGroupKey IN (SELECT PlaybookPricingGroupKey FROM PlaybookPricingGroupQuantityBand_View WHERE PlaybookDataPointGroupKey = @PlaybookDataPointGroupKey)
			EXEC LogDCPEvent 'lion.RBP_DeleteProject - Deleting from PlaybookPricingGroupQuantityBand B', 'E', 0, 0, @@RowCount, @ProjectKey 


	END


	EXEC LogDCPEvent 'lion.RBP_DeleteProject - Deleting from PlaybookDatapointPricingGroup', 'B', 0, 0, 0, @ProjectKey
	DELETE FROM PlaybookDatapointPricingGroup WHERE PlaybookDatapointPricingGroupKey IN (SELECT PlaybookDatapointPricingGroupKey FROM PlaybookDatapointPricingGroup_View WHERE PlaybookDataPointGroupKey = @PlaybookDataPointGroupKey)
	EXEC LogDCPEvent 'lion.RBP_DeleteProject - Deleting from PlaybookDatapointPricingGroup', 'E', 0, 0, @@RowCount, @ProjectKey 


	IF @FullDeleteIndicator = 'Y'
	BEGIN
		EXEC LogDCPEvent 'lion.RBP_DeleteProject - Deleting from PlaybookPricingGroup', 'B', 0, 0, 0, @ProjectKey
		DELETE FROM PlaybookPricingGroup WHERE PlaybookDataPointGroupKey = @PlaybookDataPointGroupKey
		EXEC LogDCPEvent 'lion.RBP_DeleteProject - Deleting from PlaybookPricingGroup', 'E', 0, 0, @@RowCount, @ProjectKey 
	END


	EXEC LogDCPEvent 'lion.RBP_DeleteProject - Deleting from PlaybookProjectFullScopeSummary', 'B', 0, 0, 0, @ProjectKey
	DELETE FROM PlaybookProjectFullScopeSummary WHERE ProjectKey = @ProjectKey
	EXEC LogDCPEvent 'lion.RBP_DeleteProject - Deleting from PlaybookProjectFullScopeSummary', 'E', 0, 0, @@RowCount, @ProjectKey 

	
	EXEC LogDCPEvent 'lion.RBP_DeleteProject - Deleting from lion.PlaybookProjectMonthlySummary', 'B', 0, 0, 0, @ProjectKey
	DELETE FROM dbo.PlaybookProjectMonthlySummary WHERE ProjectKey = @ProjectKey
	EXEC LogDCPEvent 'lion.RBP_DeleteProject - Deleting from lion.PlaybookProjectMonthlySummary', 'E', 0, 0, @@RowCount, @ProjectKey 


	EXEC LogDCPEvent 'lion.RBP_DeleteProject - Deleting from PlaybookDataPoint', 'B', 0, 0, 0, @ProjectKey
	DELETE FROM PlaybookDataPoint WHERE PlaybookDataPointGroupKey = @PlaybookDataPointGroupKey
	EXEC LogDCPEvent 'lion.RBP_DeleteProject - Deleting from PlaybookDataPoint', 'E', 0, 0, @@RowCount, @ProjectKey 


	EXEC LogDCPEvent 'lion.RBP_DeleteProject - Deleting from PlaybookDataPointGroupFullScopeCoreNonCore', 'B', 0, 0, 0, @ProjectKey
	DELETE FROM PlaybookDataPointGroupFullScopeCoreNonCore WHERE PlaybookDataPointGroupKey = @PlaybookDataPointGroupKey
	EXEC LogDCPEvent 'lion.RBP_DeleteProject - Deleting from PlaybookDataPointGroupFullScopeCoreNonCore', 'E', 0, 0, @@RowCount, @ProjectKey


	EXEC LogDCPEvent 'lion.RBP_DeleteProject - Deleting from PlaybookDataPointGroupFullScopeAccountBands', 'B', 0, 0, 0, @ProjectKey
	DELETE FROM PlaybookDataPointGroupFullScopeAccountBands WHERE PlaybookDataPointGroupKey = @PlaybookDataPointGroupKey
	EXEC LogDCPEvent 'lion.RBP_DeleteProject - Deleting from PlaybookDataPointGroupFullScopeAccountBands', 'E', 0, 0, @@RowCount, @ProjectKey


	EXEC LogDCPEvent 'lion.RBP_DeleteProject - Deleting from PlaybookDataPointGroupAccountItemBands', 'B', 0, 0, 0, @ProjectKey
	DELETE FROM PlaybookDataPointGroupAccountItemBands WHERE PlaybookDataPointGroupKey = @PlaybookDataPointGroupKey
	EXEC LogDCPEvent 'lion.RBP_DeleteProject - Deleting from PlaybookDataPointGroupAccountItemBands', 'E', 0, 0, @@RowCount, @ProjectKey


	EXEC LogDCPEvent 'lion.RBP_DeleteProject - Deleting from PlaybookDataPointGroupItemBands', 'B', 0, 0, 0, @ProjectKey
	DELETE FROM PlaybookDataPointGroupItemBands WHERE PlaybookDataPointGroupKey = @PlaybookDataPointGroupKey
	EXEC LogDCPEvent 'lion.RBP_DeleteProject - Deleting from PlaybookDataPointGroupItemBands', 'E', 0, 0, @@RowCount, @ProjectKey


	EXEC LogDCPEvent 'lion.RBP_DeleteProject - Deleting from PlaybookDataPointGroupScope', 'B', 0, 0, 0, @ProjectKey
	DELETE FROM PlaybookDataPointGroupScope WHERE PlaybookDataPointGroupKey = @PlaybookDataPointGroupKey
	EXEC LogDCPEvent 'lion.RBP_DeleteProject - Deleting from PlaybookDataPointGroupScope', 'E', 0, 0, @@RowCount, @ProjectKey


	EXEC LogDCPEvent 'lion.RBP_DeleteProject - Deleting from PlaybookProjectAccountExclusion', 'B', 0, 0, 0, @ProjectKey
	DELETE FROM PlaybookProjectAccountExclusion WHERE ProjectKey = @ProjectKey
	EXEC LogDCPEvent 'lion.RBP_DeleteProject - Deleting from PlaybookProjectAccountExclusion', 'E', 0, 0, @@RowCount, @ProjectKey

	
	IF @FullDeleteIndicator = 'Y'
	BEGIN
		EXEC LogDCPEvent 'lion.RBP_DeleteProject - Deleting from PlaybookDataPointGroup', 'B', 0, 0, 0, @ProjectKey
		DELETE FROM dbo.PlaybookDataPointGroup WHERE PlaybookDataPointGroupKey = @PlaybookDataPointGroupKey
		EXEC LogDCPEvent 'lion.RBP_DeleteProject - Deleting from PlaybookDataPointGroup', 'E', 0, 0, @@RowCount, @ProjectKey
	END


	FETCH NEXT FROM a INTO @PlaybookDataPointGroupKey


END
CLOSE a
DEALLOCATE a




EXEC LogDCPEvent 'lion.RBP_DeleteProject', 'E', 0, 0, 0, @ProjectKey






GO
