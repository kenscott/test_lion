SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [cm].[BulkEditClaimLines](@WebUserKey INT, @BatchKey INT, @Status VARCHAR(1) = NULL, @RejectReasonKey INT = NULL, @Comment VARCHAR(4000) = NULL)

AS 

SET NOCOUNT ON

DECLARE @ContractBatchCount INT
DECLARE @UpdatedClaimLineCount INT
DECLARE @ErrorMessage VARCHAR(250)
DECLARE @CommentLineCount INT
DECLARE @MultipleClaimLines TABLE (ClientInvoiceLineUniqueID KEY_NORMAL_TYPE, BatchKey INT)
DECLARE @ClaimedMultiples TABLE (ClientInvoiceLineUniqueID KEY_NORMAL_TYPE)


-------- Check for errors -------------------
IF @Status IS NULL AND @Comment IS NULL
BEGIN
	SET @ErrorMessage = 'ERROR: Must include Status or Comment'
END

IF @Status = 'X' AND @RejectReasonKey IS NULL
BEGIN
	SET @ErrorMessage = 'ERROR: Reject Reason Key missing'
END
---------------------------------------------

IF @ErrorMessage IS NULL
BEGIN


SELECT @ContractBatchCount = COUNT(*) FROM cm.ClaimJournal WHERE BatchKey = @BatchKey
SET @UpdatedClaimLineCount = 0

--Get multiple claim lines (claim lines with the same transaction id)
	INSERT INTO @ClaimedMultiples  
	SELECT DISTINCT ClientInvoiceLineUniqueID 
	FROM cm.ClaimJournal
	WHERE ClaimType = 'C'  
		AND ClaimJournalStatus = 'C' 

	INSERT INTO @MultipleClaimLines (ClientInvoiceLineUniqueID, BatchKey)
	SELECT DISTINCT cj.ClientInvoiceLineUniqueID, BatchKey
	FROM cm.ClaimJournal cj
	LEFT JOIN @ClaimedMultiples cm
		ON cm.ClientInvoiceLineUniqueID = cj.ClientInvoiceLineUniqueID
	WHERE cj.ClientInvoiceLineUniqueID IN (
		SELECT DISTINCT ClientInvoiceLineUniqueID 
		FROM cm.ClaimJournal
		WHERE ClaimType = 'C'   --do not group Adjustment or Manual claims
			AND ClaimJournalStatus <> 'D'  --do not include claims with a Deleted status 
		GROUP BY ClientInvoiceLineUniqueID
		HAVING COUNT(*) > 1)
	AND cm.ClientInvoiceLineUniqueID IS NULL  --Do not include in the updates


IF @Status = 'P'  --Pending
BEGIN
	BEGIN TRAN

	DECLARE @UpdatedStatusRowData TABLE (
		ClaimJournalKey KEY_NORMAL_TYPE,
		NewClaimJournalStatus varchar(1),
		OldClaimJournalStatus varchar(1),
		NewADVRecommendation varchar(1),
		OldADVRecommendation varchar(1)
	)

	UPDATE cj
	SET ClaimJournalStatus = 'P',
		ADVRecommendation = 'A',
		ADVBatchKey = NULL,
		--Comment = @Comment,
		RejectionReasonKey = NULL,
		ModificationDate = GETDATE(),
		ModifiedByWebUserKey = @WebUserKey,
		ADVRecommendationDate = GETDATE(),
		ADVRecommendationWebUserKey = @WebUserKey
	OUTPUT 
			INSERTED.ClaimJournalKey,
			INSERTED.ClaimJournalStatus,
			DELETED.ClaimJournalStatus,
			INSERTED.ADVRecommendation,
			DELETED.ADVRecommendation
			INTO @UpdatedStatusRowData
	FROM cm.ClaimJournal cj
	LEFT JOIN @MultipleClaimLines m
		ON m.ClientInvoiceLineUniqueID = cj.ClientInvoiceLineUniqueID
	WHERE cj.BatchKey = @BatchKey
		AND ClaimJournalStatus <> 'P'
		AND m.ClientInvoiceLineUniqueID IS NULL  --do not update if a multiple claim line

	SET @UpdatedClaimLineCount = @@ROWCOUNT

	INSERT INTO cm.ClaimJournalLog (WebUserKey, ClaimJournalKey, Notes, CreationDate, ClaimAction )
		SELECT @WebUserKey, 
			    ClaimJournalKey, 
			    'Status changed from ' + OldClaimJournalStatus + ' to ' + NewClaimJournalStatus, 
			    GETDATE(),
			   'Accept Claim Line'
			FROM @UpdatedStatusRowData

	COMMIT TRAN
END

IF @Status = 'X'  --Rejected
BEGIN
	BEGIN TRAN

	DECLARE @UpdatedStatusRowDataX TABLE (
		ClaimJournalKey KEY_NORMAL_TYPE,
		NewClaimJournalStatus varchar(1),
		OldClaimJournalStatus varchar(1),
		NewADVRecommendation varchar(1),
		OldADVRecommendation varchar(1)

	)

	UPDATE cm.ClaimJournal
	SET ClaimJournalStatus = 'X',
		ADVRecommendation = 'X',
		ADVBatchKey = NULL,
		--Comment = @Comment,
		RejectionReasonKey = @RejectReasonKey,
		ModificationDate = GETDATE(),
		ModifiedByWebUserKey = @WebUserKey,
		ADVRecommendationDate = GETDATE(),
		ADVRecommendationWebUserKey = @WebUserKey
	OUTPUT 
			INSERTED.ClaimJournalKey,
			INSERTED.ClaimJournalStatus,
			DELETED.ClaimJournalStatus,
			INSERTED.ADVRecommendation,
			DELETED.ADVRecommendation
			INTO @UpdatedStatusRowDataX
	FROM cm.ClaimJournal cj
	LEFT JOIN @MultipleClaimLines m
		ON m.ClientInvoiceLineUniqueID = cj.ClientInvoiceLineUniqueID
	WHERE (m.ClientInvoiceLineUniqueID IS NULL AND cj.BatchKey = @BatchKey
			OR m.ClientInvoiceLineUniqueID IS NOT NULL AND m.BatchKey = @BatchKey)  --include multiple claim lines
			AND cj.ClaimJournalStatus <> 'X'

	SET @UpdatedClaimLineCount = @@ROWCOUNT

	INSERT INTO cm.ClaimJournalLog (WebUserKey, ClaimJournalKey, Notes, CreationDate, ClaimAction )
		SELECT @WebUserKey, 
			    ClaimJournalKey, 
			    'Status changed from ' + OldClaimJournalStatus + ' to ' + NewClaimJournalStatus + ' Rejection Reason = ' + (SELECT RejectionReasonValue FROM cm.RejectionReason WHERE RejectionReasonKey = @RejectReasonKey), 
			    GETDATE(),
			   'Reject Claim Line'
		FROM @UpdatedStatusRowDataX  
		

	COMMIT TRAN
END

IF @Status = 'R'  --Review
BEGIN
	BEGIN TRAN
	
	DECLARE @UpdatedStatusRowDataR TABLE (
		ClaimJournalKey KEY_NORMAL_TYPE,
		NewClaimJournalStatus varchar(1),
		OldClaimJournalStatus varchar(1),
		NewADVRecommendation varchar(1),
		OldADVRecommendation varchar(1)
	)

	UPDATE cj
	SET ClaimJournalStatus = 'R',
		ADVRecommendation = NULL,
		ADVBatchKey = NULL,
		--Comment = @Comment,
		RejectionReasonKey = NULL,
		ModificationDate = GETDATE(),
		ModifiedByWebUserKey = @WebUserKey,
		ADVRecommendationDate = GETDATE(),
		ADVRecommendationWebUserKey = @WebUserKey
	OUTPUT 
			INSERTED.ClaimJournalKey,
			INSERTED.ClaimJournalStatus,
			DELETED.ClaimJournalStatus,
			INSERTED.ADVRecommendation,
			DELETED.ADVRecommendation
			INTO @UpdatedStatusRowDataR
	FROM cm.ClaimJournal cj
	LEFT JOIN @MultipleClaimLines m
		ON m.ClientInvoiceLineUniqueID = cj.ClientInvoiceLineUniqueID  
	WHERE (m.ClientInvoiceLineUniqueID IS NULL AND cj.BatchKey = @BatchKey
			OR m.ClientInvoiceLineUniqueID IS NOT NULL AND m.BatchKey = @BatchKey)  --include multiple claim lines
		AND ClaimJournalStatus <> 'R'

	SET @UpdatedClaimLineCount = @@ROWCOUNT

	INSERT INTO cm.ClaimJournalLog (WebUserKey, ClaimJournalKey, Notes, CreationDate, ClaimAction )
		SELECT @WebUserKey, 
			    ClaimJournalKey, 
			    'Status changed from ' + OldClaimJournalStatus + ' to ' + NewClaimJournalStatus, 
			    GETDATE(),
			   'Review Claim Line'
			FROM @UpdatedStatusRowDataR

	COMMIT TRAN
END

--If the supplier claims status is R (ready for claim) and an associated supplier claim line has changed status, the supplier claim should change to P (pending)
IF @Status IS NOT NULL
BEGIN

DECLARE @SupplierClaimUpdateRowData TABLE (
		SupplierClaimKey KEY_NORMAL_TYPE,
		ClaimStatus varchar(1))

	UPDATE sc
	SET ClaimStatus = 'P'
	OUTPUT INSERTED.SupplierClaimKey,
			INSERTED.ClaimStatus
		INTO @SupplierClaimUpdateRowData
	FROM cm.SupplierClaim sc
	INNER JOIN cm.ClaimJournal cj
		ON cj.VendorKey = sc.VendorKey
		AND cj.ClaimMonth = sc.ClaimMonth
	WHERE cj.BatchKey = @BatchKey
		AND sc.ClaimStatus = 'R'

--Log Supplier Claim status change
INSERT INTO cm.ClaimLog
	(
	[WebUserKey],
	[SupplierClaimKey],
	[Notes],
	[CreationDate],
	[ClaimAction]
	)
SELECT 
	1,
	d.SupplierClaimKey,
	'From: Ready to Claim  To: ' + s.SupplierClaimStatusDescription,
	GETDATE(),
	'Supplier Claim System Status Change'
FROM @SupplierClaimUpdateRowData d
INNER JOIN cm.SupplierClaimStatus s
	ON s.SupplierClaimStatus = d.ClaimStatus

END

IF @Comment IS NOT NULL
BEGIN
	UPDATE cj
	SET Comment = @Comment
	FROM cm.ClaimJournal cj
	LEFT JOIN @MultipleClaimLines m
		ON m.ClientInvoiceLineUniqueID = cj.ClientInvoiceLineUniqueID
	WHERE (m.ClientInvoiceLineUniqueID IS NULL AND cj.BatchKey = @BatchKey
			OR m.ClientInvoiceLineUniqueID IS NOT NULL AND m.BatchKey = @BatchKey)  --update multiples

	SET @CommentLineCount = @@ROWCOUNT

	--Log Comment update
	INSERT INTO cm.ClaimJournalLog (WebUserKey, ClaimJournalKey, Notes, CreationDate, ClaimAction)
		SELECT @WebUserKey, 
			    ClaimJournalKey, 
			    Comment, 
			    GETDATE(),
			   'Amend Comment'
		FROM cm.ClaimJournal
		WHERE BatchKey = @BatchKey

	IF @Status IS NULL
	BEGIN
		SET @UpdatedClaimLineCount = @CommentLineCount
	END
END 

END

SELECT @ContractBatchCount AS BatchContractCount, 
	   @UpdatedClaimLineCount AS UpdatedClaimLineCount,
	   @ErrorMessage AS ErrorMessage
GO
