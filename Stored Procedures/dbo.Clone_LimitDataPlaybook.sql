SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE     PROCEDURE [dbo].[Clone_LimitDataPlaybook]
@LimitDataPlaybookKey INT,
@Command CHAR(1) = 'N',
@ApproachTypeKey INT
AS

DECLARE @UpdateFlag CHAR(1)
SET @UpdateFlag = 'N'

DECLARE @SQL NVARCHAR(MAX)

DECLARE
	@MaxDayThreshold NVARCHAR(MAX),
	@WhereClause  NVARCHAR(MAX),
	@JoinClause  NVARCHAR(MAX),
	@HavingClause  NVARCHAR(MAX)

--Get the values for this playbook
SELECT 
@MaxDayThreshold = ISNULL(MaxDayThreshold,''),
@WhereClause = ISNULL(REPLACE(WhereClause,'''',''''''),''),
@JoinClause = ISNULL(REPLACE(JoinClause,'''',''''''),''),
@HavingClause = ISNULL(REPLACE(HavingClause,'''',''''''),'')
FROM ATKLimitDataPlaybook
WHERE LimitDataPlaybookKey = @LimitDataPlaybookKey

IF @Command = 'N'
BEGIN
	INSERT INTO #ScenarioData SELECT  '-------------------------------------'
	INSERT INTO #ScenarioData SELECT  '--Insert new LimitDataPlaybook Row'
	INSERT INTO #ScenarioData SELECT  '-------------------------------------'
	INSERT INTO #ScenarioData SELECT  'Declare @NewLimitDataPlaybookKey int'

	SET @SQL = 
	'
	INSERT INTO ATKLimitDataPlaybook(MaxDayThreshold, WhereClause, JoinClause, HavingClause, Version)
	Values ('''+@MaxDayThreshold+''','''+@WhereClause+''','''+@JoinClause+''','''+@HavingClause+''', 0)'
	INSERT INTO #ScenarioData SELECT  @SQL
	
	INSERT INTO #ScenarioData SELECT  'Set @NewLimitDataPlaybookKey = @@Identity'
	
	INSERT INTO #ScenarioData SELECT  'Print ''Your new Limit Data Playbook Key is: ''+Cast(@NewLimitDataPlaybookKey as nvarchar(10))'

	INSERT INTO #ScenarioData SELECT  '-------------------------------------'
	INSERT INTO #ScenarioData SELECT  '-------------------------------------'
END
ELSE IF @Command = 'U'
BEGIN
	INSERT INTO #ScenarioData SELECT  '------------------------------------------------------'
	INSERT INTO #ScenarioData SELECT  '--Updating ATKLimitDataPlaybookKey: '+CAST(@LimitDataPlaybookKey AS NVARCHAR(100))
	INSERT INTO #ScenarioData SELECT  '------------------------------------------------------'
	INSERT INTO #ScenarioData SELECT  '	
	Update ATKLimitDataPlaybook
	Set MaxDayThreshold = '''+@MaxDayThreshold+''',
	WhereClause = '''+@WhereClause+''', 
	JoinClause = '''+@JoinClause+''',
	HavingClause= '''+@HavingClause+'''
	Where LimitDataPlaybookKey = '+CAST(@LimitDataPlaybookKey AS NVARCHAR(100))
END
ELSE
BEGIN
	INSERT INTO #ScenarioData SELECT  '------------------------------------------------------'
	INSERT INTO #ScenarioData SELECT  'Your current ATKLimitDataPlaybookKey is: '+CAST(@LimitDataPlaybookKey AS NVARCHAR(100))
	INSERT INTO #ScenarioData SELECT  '------------------------------------------------------'
	INSERT INTO #ScenarioData SELECT  '--MaxDayThreshold:   ' +ISNULL(@MaxDayThreshold, 'NULL')
	INSERT INTO #ScenarioData SELECT  '--WhereClause:       '+ ISNULL(@WhereClause, 'NULL')
	INSERT INTO #ScenarioData SELECT  '--JoinClause:        '+ ISNULL(@JoinClause, 'NULL')
	INSERT INTO #ScenarioData SELECT  '--HavingClause:      '+ ISNULL(@HavingClause, 'NULL')
END




GO
