SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO







CREATE PROCEDURE 
[dbo].[Load_RollingPeriods]
AS
--Author:  Tom Werz
--Date:  04/01/2005
--Purpose:  This will update the DimMonth table, setting the most recent 3 months = to period 1, the next 3 oldest months to period 2, etc.

SET NoCount ON
DECLARE @RowsI Quantity_Normal_type, @RowsU Quantity_Normal_type, @RowsD Quantity_Normal_type

SET @RowsI = 0
SET @RowsU = 0
SET @RowsD = 0

EXEC LogDCPEvent 'ETL - dbo.Load_RollingPeriods', 'B'

DECLARE @MaxMonthKey Key_Small_Type, @x INT, @PeriodDesc VARCHAR(500), @PeriodNumber INT, @NumBusinessDaysNotReceivedInCurrentMonth INT

SET @MaxMonthKey = (SELECT MAX(InvoiceDateMonthKey) FROM FactInvoiceLine)

DECLARE @FILMaxDay INT, @MaxSQLDate DATETIME
SELECT @FILMaxDay = MAX(InvoiceDateDayKey) FROM FactInvoiceLine
SELECT @MaxSQLDate = SQLDate FROM DimDay WHERE DayKey = @FILMaxDay
SELECT @NumBusinessDaysNotReceivedInCurrentMonth = COUNT(*) 
FROM DimDay 
WHERE DayKey > @FILMaxDay AND MonthKey = @MaxMonthKey AND CalendarDay NOT IN ('Saturday', 'Sunday') AND ISNULL(HolidayIndicator, 'N') <> 'Y'

PRINT 'NumBusinessDaysNotReceivedInCurrentMonth: '+CAST(@NumBusinessDaysNotReceivedInCurrentMonth AS VARCHAR(50))
----------------------
IF @NumBusinessDaysNotReceivedInCurrentMonth = 0
	SET @x = @MaxMonthKey	--If the curent month should be in period 0
ELSE
	SET @x = @MaxMonthKey-1 --Else if we have all on hte current month, it should be in period 1

SET @PeriodNumber = 1

UPDATE DimMonth SET Period = NULL, PeriodNumber = NULL, MonthSequenceNumber = NULL

WHILE @x >= 1
BEGIN
	SET @PeriodDesc = 
	(SELECT CalendarMonthInYear FROM DimMonth WHERE MonthKey = @x-2)
	+ ' - ' +
	(SELECT CalendarMonthInYear FROM DimMonth WHERE MonthKey = @x)
	UPDATE DimMonth SET Period = @PeriodDesc, PeriodNumber = @PeriodNumber WHERE MonthKey BETWEEN @x-2 AND @x

	SET @x = @x - 3	--Go to the next period
	SET @PeriodNumber = @PeriodNumber + 1
END

DECLARE @SeqNum INT
SET @SeqNum = 1
--Now, set up rolling month sequence numbers
IF @NumBusinessDaysNotReceivedInCurrentMonth = 0
	SET @x = @MaxMonthKey	--If the curent month should be in period 0
ELSE
	SET @x = @MaxMonthKey-1 --Else if we have all on hte current month, it should be in period 1

WHILE @x >= 1
BEGIN
	UPDATE DimMonth SET MonthSequenceNumber = @SeqNum WHERE MonthKey = @x

	SET @x = @x - 1	--Go to the next month
	SET @SeqNum = @SeqNum + 1
END


--Now, if the current month is not a complete month, set it to period 0, month sequence 0
IF @NumBusinessDaysNotReceivedInCurrentMonth > 0
BEGIN
	UPDATE DimMonth 
		SET Period = CalendarMonth + ' 1, ' + CAST(CalendarYear AS VARCHAR(50)) +
					' - ' +
					CalendarMonth + ' ' + CAST(DATEPART(day, @MaxSQLDate) AS VARCHAR(50)) + ', ' + CAST(CalendarYear AS VARCHAR(50)),
		PeriodNumber = 0,
		MonthSequenceNumber = 0
	WHERE MonthKey = @MaxMonthKey
END

EXEC LogDCPEvent 'ETL - dbo.Load_RollingPeriods', 'E', @RowsI, @RowsU, @RowsD








GO
