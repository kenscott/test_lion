SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE PROCEDURE [lion].[Load_DimInvoiceLineGroup2]
AS

EXEC LogDCPEvent 'ETL - Load_DimInvoiceLineGroup2', 'B'

DECLARE 
	@RowsI INT, 
	@RowsU INT, 
	@RowsD INT,
	@BeginRowCount INT,
	@TodayDayKey Key_Small_Type

SET @RowsI = 0
SET @RowsU = 0
SET @RowsD = 0

SELECT @BeginRowCount = COUNT(*) FROM dbo.DimItem


SET @TodayDayKey = (SELECT DayKey FROM DimDay WHERE CONVERT(VARCHAR(10), SQLDate, 120) = CONVERT(VARCHAR(10), GETDATE(), 120))


EXEC DBA_IndexRebuild 'DimInvoiceLineGroup2', 'Drop'


--TRUNCATE TABLE dbo.DimInvoiceLineGroup2

IF NOT EXISTS (SELECT 1 FROM DimInvoiceLineGroup2 WHERE InvoiceLineGroup2UDVarchar1 = N'Unknown')
BEGIN

	INSERT DimInvoiceLineGroup2 (
		InvoiceLineGroup2UDVarchar1,
		InvoiceLineGroup2UDVarchar2,
		InvoiceLineGroup2UDVarchar3,
		InvoiceLineGroup2UDVarchar4,
		InvoiceLineGroup2UDVarchar5,
		CreationDayKey,
		ModificationDayKey
	)
	VALUES (
		'Unknown',
		'Unknown',
		'Unknown',
		'Unknown',
		'Unknown',
		@TodayDayKey,
		@TodayDayKey)
	SET @RowsI = @RowsI + @@RowCount

END

MERGE dbo.DimInvoiceLineGroup2 AS Tgt
USING	(
	SELECT 'ADJ' AS PriceDerivation, 'Market' AS PriceDerivationBucket, 'Adjusted Price' AS PriceDerivationDescription, '02 - Manually Priced' AS ChrisHierarchy, '4 - Manually Priced' AS PriceflowSalesSummary
	UNION SELECT 'CCP', 'Market', 'Call Center Price Used', '00 - Exceptions to the rules', '4 - Manually Priced'
	UNION SELECT 'DC', 'Terms', 'Distributed Contract Used', '03 - Pricing Contract', '2 - Default Contract Used'
	UNION SELECT 'DCD', 'Terms', 'Default Owned Contract (Dist.)', '', '2 - Default Contract Used'
	UNION SELECT 'DCL', 'Terms', 'Default Owned Contract (Local)', '', '2 - Default Contract Used'
	UNION SELECT 'DDC', 'Market', 'Default Contract (Dist.)', '06 - Default Contract - Product Exceptions; 08 - SPG Discount', '2 - Default Contract Used'
	UNION SELECT 'DP', 'Terms', 'Discounted Price Used', '00 - Exceptions to the rules', '4 - Manually Priced'
	UNION SELECT 'DT', 'Terms', 'Distributed Terms Used', '', '1 - Terms Price Used'
	UNION SELECT 'FC', 'Unknown', 'Free Of Charge', '00 - Exceptions to the rules', '4 - Manually Priced'
	UNION SELECT 'FM', 'Terms', 'Fallback Terms', '04 - No Customer Terms', '4 - Manually Priced'
	UNION SELECT 'FOC', 'Unknown', 'Free Of Charge', '00 - Exceptions to the rules', '4 - Manually Priced'
	UNION SELECT 'GT', 'Terms', 'Global Terms', '05 - Customer Terms - Product Exceptions; 07 - SPG Discount', '1 - Terms Price Used'
	UNION SELECT 'IP', 'Terms', 'Internet Priced', '00 - Exceptions to the rules', '4 - Manually Priced'
	UNION SELECT 'KVI', 'Unknown', 'KVI Price Used', '01 - KVI / NDP', '4 - Manually Priced'
	UNION SELECT 'LP', 'Market', 'Starting Price Used', '09 - Quantity Break - Standard Price', '4 - Manually Priced'
	UNION SELECT 'LPC', 'Market', 'Last Price Charged', '11 - New Price Guidance Codes', '3 - Price Guidance Used'
	UNION SELECT 'MP', 'Market', 'Manually Priced', '02 - Manually Priced', '4 - Manually Priced'
	UNION SELECT 'NDM', 'Market', 'NDP Priced Manually', '02 - Manually Priced', '4 - Manually Priced'
	UNION SELECT 'NDP', 'Market', 'Non-Discountable Product Used', '01 - KVI / NDP', '4 - Manually Priced'
	UNION SELECT 'OCD', 'Terms', 'Owned Contract (Dist) Used', '03 - Pricing Contract', '2 - Default Contract Used'
	UNION SELECT 'OCL', 'Terms', 'Owned Contract (Local) Used', '03 - Pricing Contract', '2 - Default Contract Used'
	UNION SELECT 'OTD', 'Terms', 'Owned Terms (Dist) Used', '', '1 - Terms Price Used'
	UNION SELECT 'OTG', 'Terms', 'Owned Global Terms', '05 - Customer Terms - Product Exceptions; 07 - SPG Discount', '1 - Terms Price Used'
	UNION SELECT 'OTL', 'Terms', 'Owned Terms (Local) Used', '', '1 - Terms Price Used'
	UNION SELECT 'PPA', 'Unknown', 'Amber Price Guidance', '11 - New Price Guidance Codes', '3 - Price Guidance Used'
	UNION SELECT 'PPG', 'Unknown', 'Green Price Guidance', '11 - New Price Guidance Codes', '3 - Price Guidance Used'
	UNION SELECT 'PPR', 'Unknown', 'Red Price Guidance', '11 - New Price Guidance Codes', '3 - Price Guidance Used'
	UNION SELECT 'QB', 'Market', 'Quantity Breaks Used', '09 - Quantity Break - Standard Price', '4 - Manually Priced'
	UNION SELECT 'RP', 'Market', 'Retail Price Used', '09 - Quantity Break - Standard Price', '4 - Manually Priced'
	UNION SELECT 'SPG', 'Market', 'Default SPG Discount Used', '', '1 - Terms Price Used'
	UNION SELECT 'SPO', 'Terms', 'Special Offer Price Used', '10 - Special Offer Price', '4 - Manually Priced'
	UNION SELECT 'TP', 'Market', 'Trade Price Used', '09 - Quantity Break - Standard Price', '4 - Manually Priced'
	UNION SELECT 'PGA', 'Market', 'Amber Price Guidance', '11 - New Price Guidance Codes', '3 - Price Guidance Used'
	UNION SELECT 'PGG', 'Market', 'Green Price Guidance', '11 - New Price Guidance Codes', '3 - Price Guidance Used'
	UNION SELECT 'PGR', 'Market', 'Red Price Guidance',   '11 - New Price Guidance Codes', '3 - Price Guidance Used'
	UNION SELECT 'MSUP', 'Market', '', '', '4 - Manually Priced'
) AS Src
ON Tgt.InvoiceLineGroup2UDVarchar1 = Src.PriceDerivation
WHEN MATCHED AND
	Tgt.InvoiceLineGroup2UDVarchar2 <> Src.PriceDerivationBucket
	OR Tgt.InvoiceLineGroup2UDVarchar3 <> Src.PriceDerivationDescription
	OR Tgt.InvoiceLineGroup2UDVarchar4 <> Src.ChrisHierarchy
	OR Tgt.InvoiceLineGroup2UDVarchar5 <> Src.PriceflowSalesSummary
THEN UPDATE SET
	Tgt.InvoiceLineGroup2UDVarchar2 = Src.PriceDerivationBucket,
	Tgt.InvoiceLineGroup2UDVarchar3 = Src.PriceDerivationDescription,
	Tgt.InvoiceLineGroup2UDVarchar4 = Src.ChrisHierarchy,
	Tgt.InvoiceLineGroup2UDVarchar5 = Src.PriceflowSalesSummary,
	Tgt.ModificationDayKey = @TodayDayKey
WHEN NOT MATCHED THEN 
	INSERT (
		InvoiceLineGroup2UDVarchar1, 
		InvoiceLineGroup2UDVarchar2, 
		InvoiceLineGroup2UDVarchar3, 
		InvoiceLineGroup2UDVarchar4, 
		InvoiceLineGroup2UDVarchar5,
		ModificationDayKey,
		CreationDayKey
	)
	VALUES (
		Src.PriceDerivation,
		Src.PriceDerivationBucket,
		Src.PriceDerivationDescription,
		Src.ChrisHierarchy,
		Src.PriceflowSalesSummary,
		@TodayDayKey,
		@TodayDayKey
	)
;

SELECT 
	@RowsU = @@RowCOunt - (COUNT(*) - @BeginRowCount),
	@RowsI = COUNT(*) - @BeginRowCount
FROM dbo.DimItem


EXEC DBA_IndexRebuild 'DimInvoiceLineGroup2', 'Create'

SELECT TOP 10 * FROM DimInvoiceLineGroup2


EXEC LogDCPEvent 'ETL - Load_DimInvoiceLineGroup2', 'E', @RowsI, @RowsU, @RowsD


GO
