SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE [ppe].[Get_GenericCalcs] (@AccountKey INT, @ItemGroup3Key INT, @ItemKey INT,
	 @Price MONEY, @Disc1 AS DEC(38,8), @Disc2 AS DEC(38,8), @TermType AS VARCHAR(2) = 'TD') AS
BEGIN

	--DECLARE @RequestKey INT, @Price MONEY, @Disc1 AS DEC(38,8), @Disc2 AS DEC(38,8), @TermType AS VARCHAR(2)
	--SELECT @Price=2.256, @Disc1 = 0.45, @Disc2 = NULL, @RequestKey = 2167, @TermType = 'TD'

	DECLARE 
	    @DPC AS DEC(38, 8),
		@PPC AS DEC(38,8), 
		@AcceptedImpact MONEY,
		--@RecommendedImpact MONEY, 
		@GPP DEC(38,8), 
		@PriceIndex INT,
		@LTMSalesAtTrade MONEY,
		--@PropDiscount DEC(38,8),
		--@NewDiscount DEC(38,8),
		@NewCompoundDiscount DEC(38,8),
		@CurrCompoundDiscount DEC(38,8),
		@RedDiscount  DEC(38,8),
		@AmberDiscount  DEC(38,8),
		@GreenDiscount  DEC(38,8),
		@RedPrice  DEC(38,8),
		@AmberPrice  DEC(38,8),
		@GreenPrice  DEC(38,8),
		@LastDiscount  DEC(38,8),
		@DefaultDiscount DEC(38,8)

	IF @TermType = 'FP' AND @Disc1 IS NULL
	BEGIN
		SELECT @Disc1 =  1 - (@Price / NULLIF(di.ItemUDDecimal1, 0))
		  FROM dbo.DimItem di 
		 WHERE di.ItemKey = @ItemKey
	END

	SELECT @NewCompoundDiscount = ISNULL(Discount, 0)
	FROM ppe.fn_GetCompoundDiscount(@Disc1, @Disc2)

	SELECT @CurrCompoundDiscount = ISNULL(Discount, 0)
	FROM lion.CustomerTerms ct
	CROSS APPLY ppe.fn_GetCompoundDiscount(Usualdiscount1, Usualdiscount2)
	WHERE ct.AccountKey = @AccountKey
	AND ct.ItemGroup3Key = @ItemGroup3Key
	AND ct.ItemKey = @ItemKey

	SELECT @LastDiscount = MAX((fil.UnitListPrice - flpc.LastItemPrice) / NULLIF(fil.UnitListPrice,0)) 
	FROM FactLastPriceAndCost flpc
	JOIN dbo.DimItem di2 ON di2.ItemKey = flpc.ItemKey
	JOIN dbo.FactInvoiceLine fil ON flpc.LastInvoiceLineUniqueIdentifier = fil.InvoiceLineUniqueIdentifier
	JOIN dbo.DimDay dd ON dd.DayKey = flpc.LastSaleDayKey
	WHERE flpc.InvoiceLineGroup1Key = 4
	AND flpc.AccountKey = @AccountKey
	AND flpc.ItemKey = @ItemKey

	SELECT @DefaultDiscount = MAX(1 - (1 - ISNULL(UsualDiscount1 / 100, 0)) * (1 - ISNULL(UsualDiscount2 / 100, 0)))
	FROM lion.AccountContractTerms
    WHERE SPGExpiryDate > GETDATE() AND ContractName = 'ACCOUNT'
	AND ItemGroup3Key = @ItemGroup3Key

	IF @Price IS NULL
	BEGIN
		SELECT @Price = di.ItemUDDecimal1 * (1 - @NewCompoundDiscount)
		FROM dbo.DimItem di 
		WHERE di.ItemKey = @ItemKey
	END

	SELECT 
	    @PPC = CASE 
					WHEN @ItemKey <> 1 
						THEN (@Price-ISNULL(ct.ExceptionFixedPrice, di.ItemUDDecimal1 * (1.0 - (ct.ExceptionDiscount /100.0))))
							/NULLIF(ISNULL(ct.ExceptionFixedPrice, di.ItemUDDecimal1 * (1.0 - (ct.ExceptionDiscount/100.0))),0)
					ELSE ((1 - ISNULL(nd.Discount,0)) / NULLIF((1 - ISNULL(cd.Discount,0)),0))-1
				END
	 FROM lion.CustomerTerms ct
	 JOIN dbo.DimItem di ON di.ItemKey = ct.ItemKey
	 CROSS APPLY ppe.fn_GetCompoundDiscount(@Disc1, @Disc2) nd 
	 CROSS APPLY ppe.fn_GetCompoundDiscount(ct.Usualdiscount1 / 100.0, ct.Usualdiscount2 / 100.0) cd
	 WHERE AccountKey = @AccountKey
	 AND ct.ItemGroup3Key = @ItemGroup3Key
	 AND ct.ItemKey = @ItemKey


	SELECT
		@RedPrice = p.RedPrice,
		@AmberPrice = p.AmberPrice,
		@GreenPrice = p.GreenPrice
	FROM FactRecommendedPriceBand frpb 
	INNER JOIN dbo.DimItem di 
		ON di.ItemKey = frpb.ItemKey
	OUTER APPLY lion.fn_GetTradePriceMatchedRAGPrices (
	 di.ItemUDDecimal1,  /*CurrBranchTradePrice*/
	 di.CurrentCost,
	 frpb.FloorGPP,
	 frpb.TargetGPP,
	 frpb.StretchGPP,
	 1,
	 1
	) p
	WHERE frpb.AccountKey = @AccountKey
		AND frpb.ItemKey = @ItemKey
		AND frpb.InvoiceLineGroup1Key = 4


	SELECT 
	    @DPC= -- below is Jose's code from LPF-1143
            CASE
                WHEN @TermType = 'FP' 
					THEN (@Price-di.ItemUDDecimal1 * (1 - @LastDiscount))/NULLIF(di.ItemUDDecimal1,0)
                ELSE (ISNULL(@LastDiscount, ISNULL(@CurrCompoundDiscount,@DefaultDiscount)) - @NewCompoundDiscount)
            END,
		@GPP = 	CASE 
					WHEN @Price IS NOT NULL THEN (@Price-di.ItemUDDecimal3 - di.ItemUDDecimal4 - di.ItemUDDecimal5)/NULLIF(@Price,0)
					ELSE NULL
				END ,
		@PriceIndex = ndi.DiscountIndex,
		@Price = CASE 
				WHEN @TermType = 'FP' THEN @Price
				ELSE ROUND(di.ItemUDDecimal1 * ( 1.0 - @NewCompoundDiscount ), 3)
			END,
		@RedDiscount = rd.Discount,
		@AmberDiscount = ad.Discount,
		@GreenDiscount = gd.Discount

	FROM dbo.DimItem di 
    CROSS APPLY ppe.fn_GetDiscount(@AccountKey, @ItemGroup3Key, @ItemKey, @GreenPrice, di.ItemUDDecimal1, 'G') gd
    CROSS APPLY ppe.fn_GetDiscount(@AccountKey, @ItemGroup3Key, @ItemKey, @AmberPrice, di.ItemUDDecimal1, 'A') ad
    CROSS APPLY ppe.fn_GetDiscount(@AccountKey, @ItemGroup3Key, @ItemKey, @RedPrice, di.ItemUDDecimal1, 'R') rd
	CROSS APPLY ppe.fn_GetCompoundDiscount (@Disc1, @Disc2) cd
	CROSS APPLY ppe.fn_GetSimpleDiscount(CASE WHEN @TermType='FP' THEN @Price ELSE NULL END, di.ItemUDDecimal1, NULL, cd.Discount) nsd
	CROSS APPLY ppe.fn_GetDiscountIndex(rd.Discount, ad.Discount, gd.Discount, nsd.Discount) ndi
	WHERE di.ItemKey = @ItemKey

	--IF @ItemKey <> 1
	--	SELECT @LTMSalesAtTrade= SUM(ISNULL(TotalNetQuantity, TotalQuantity) * ItemUDDecimal1 ) 
	--	FROM dbo.FactInvoiceLine fil
	--	JOIN dbo.DimItem di ON di.ItemKey = fil.ItemKey
	--	WHERE fil.Last12MonthsIndicator = N'Y'
	--	AND fil.AccountKey = @AccountKey
	--	AND fil.ItemKey = @ItemKey
	--	AND fil.InvoiceLineGroup1Key = 4
	--	AND fil.UDVarchar1 /*PriceDerivation*/ NOT IN ( 'DC', 'OCD', 'OCL')

	--ELSE	
	--	SELECT @LTMSalesAtTrade= SUM(ISNULL(TotalNetQuantity, TotalQuantity) * ItemUDDecimal1 )
	--	FROM dbo.FactInvoiceLine fil
	--	JOIN dbo.DimItem di ON di.ItemKey = fil.ItemKey
	--	LEFT JOIN lion.CustomerTerms ct
	--		ON ct.AccountKey = fil.AccountKey
	--		AND ct.ItemKey = fil.ItemKey
	--	WHERE fil.Last12MonthsIndicator = N'Y'
	--	AND fil.AccountKey = @AccountKey
	--	AND fil.ItemGroup3Key = @ItemGroup3Key
	--	AND fil.InvoiceLineGroup1Key = 4
	--	AND ct.AccountKey IS NULL
	--	AND fil.UDVarchar1 /*PriceDerivation*/ NOT IN ( 'DC', 'OCD', 'OCL')

	IF @ItemKey <> 1 --LPF-2047
		SELECT @AcceptedImpact= f.SalesAtTrade * ( ALPCD - @NewCompoundDiscount ) 
		FROM ppe.vw_AccountItemSPGImpact f
		WHERE AccountKey = @AccountKey
		AND ItemKey = @ItemKey
	ELSE  --LPF-2047
	BEGIN
		SELECT @LTMSalesAtTrade = SUM(SalesAtTrade)
		FROM ppe.vw_AccountItemSPGImpact f
		WHERE f.AccountKey = @AccountKey
		AND f.ItemGroup3Key = @ItemGroup3Key 
		AND f.TermsKey IS NULL 

		SELECT @AcceptedImpact = (@LTMSalesAtTrade) * ((1.0-a.SPGLastDiscount) - @NewCompoundDiscount )
		FROM  [ppe].[fn_GetDiscountSPG] (@AccountKey,@ItemGroup3Key) a
	END
	
		--SELECT @AcceptedImpact= SUM(CASE WHEN f.KVIIndicator = 'Y' AND f.InCustomerSPG = 'N' THEN 0 ELSE (f.SalesAtTrade) * ( (1.0-a.SPGLastDiscount) - @NewCompoundDiscount ) END )
		--FROM ppe.vw_AccountItemSPGImpact f
		--CROSS APPLY [ppe].[fn_GetDiscountSPG] (@AccountKey,@ItemGroup3Key) a
		--WHERE f.AccountKey = @AccountKey
		--AND f.ItemGroup3Key = @ItemGroup3Key
		----AND f.TermsKey IS NULL

	SELECT @NewCompoundDiscount = ISNULL(Discount, 0)
	  FROM ppe.fn_GetCompoundDiscount(@Disc1, @Disc2)

	SELECT
		NEWID() AS GUID,
		@PPC AS PPC,
		@AcceptedImpact AS AcceptedImpact,
		@GPP AS GPP,
		@PriceIndex AS PricingIndex,
		@NewCompoundDiscount AS TotalDiscount,
		@Price AS FixedPrice,
		@Disc1 AS Discount1,
		@RedDiscount AS RedDiscount,
		@AmberDiscount AS AmberDiscount,
		@GreenDiscount AS GreenDiscount,
		ISNULL(@TermType, CASE WHEN @Price IS NOT NULL THEN 'FP' ELSE 'TD' END) AS TermType
END







GO
