SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [lion].[Load_StagingTransactionArchive]

AS
/*

EXEC lion.Load_StagingTransactionArchive

*/


SET ANSI_WARNINGS OFF
SET ARITHABORT OFF
SET ARITHIGNORE ON
SET NOCOUNT ON

EXEC LogDCPEvent 'ETL - Load_StagingTransactionArchive', 'B'


DECLARE 
	@RowsI INT, 
	@RowsU INT, 
	@RowsD INT,
	@MaxDateTime DATETIME,
	@OldDateTime DATETIME,
	@ArchiveDate DATE,
	@y VARCHAR(50),
	@m VARCHAR(50),
	@d VARCHAR(50)


SELECT @MaxDateTime = MAX(TransactionDate) FROM Lion_Staging.dbo.Transactions

SELECT @OldDateTime = DATEADD(MONTH, -27, @MaxDateTime)

SET @m = DATEPART(MONTH, @OldDateTime)
SET @y = DATEPART(YEAR, @OldDateTime)
SET @d = 1

SELECT @ArchiveDate = CAST(@y + '/ ' + @m + '/' + @d AS DATE)

SELECT @MaxDateTime, @OldDateTime, @ArchiveDate


SET @RowsI = 0
SET @RowsU = 0
SET @RowsD = 0



SET IDENTITY_INSERT Lion_Staging.dbo.TransactionsArchive ON

DECLARE @InsertedRowData TABLE (
	AdvanousInvoiceLineId INT NOT NULL PRIMARY KEY CLUSTERED
	)

EXEC LogDCPEvent 'ETL - Load_StagingTransactionArchive - Insert', 'B'

INSERT INTO Lion_Staging.dbo.TransactionsArchive 
(
	AdvanousInvoiceLineId,
	ETLRowStatusCodeKey,
	RawFileKey,
	BranchCode,
	AccountID,
	TransNo,
	DeliveryDate,
	SLedgerNo,
	SLedgerDate,
	YMTH,
	SupplyType,
	DocInd,
	CreditReason,
	KnownValueItem,
	VariablePackInd,
	CtsRelCode,
	ClaimNo,
	PriceDerivation,
	ProductCode,
	Qty,
	TradingPrice,
	LinePrice,
	BranchMargin,
	PaddingReleaseVal,
	ExtraAddVal,
	MarketingAddVal,
	PromoAddVal,
	RealCost,
	RealCostAllTerms,
	CcProfit,
	ApTradingMarginPadRelease,
	FinMargin,
	CustCntNo,
	MthdOfDespatch,
	TransType,
	SalesOrigin,
	TakenBy,
	MethodOfPayment,
	DelNo,
	HeaderID,
	CrdOrigInvNo,
	CrdOrigInvDate,
	OriginalOrdNo,
	CarriagePack,
	DelAddPhone,
	SrcAddr1,
	SrcAddr2,
	SrcAddr3,
	SrcAddr4,
	SrcAddr5,
	SrcPostCode,
	GpAllocBranchCode,
	PyramidCode,
	SalesOriginDesc,
	LineNumber,
	PriceOrderType,
	TransactionDate,
	OrderDate,
	RetailPrice,
	UniqueId,
	RedPrice,
	AmberPrice,
	GreenPrice,
	BluePrice,
	TerminalEmulator,
	PriceGuidanceIndicator,
	UnitPrice,
	CreditAdvanousInvoiceLineId,
	EffectiveQty,
	EffectiveLinePrice,
	EffectiveApTradingMarginPadRelease,
	CustOrdRef,
	BranchBrand,
	TradingBranchPrimaryBrand,
	AccountingBrand,
	SuppliersNettCost,
	SuppliersNettCostMNTHDay1,
	ArchiveDate
)
OUTPUT 
	INSERTED.AdvanousInvoiceLineId
INTO @InsertedRowData
SELECT
	AdvanousInvoiceLineId,
	ETLRowStatusCodeKey,
	RawFileKey,
	BranchCode,
	AccountID,
	TransNo,
	DeliveryDate,
	SLedgerNo,
	SLedgerDate,
	YMTH,
	SupplyType,
	DocInd,
	CreditReason,
	KnownValueItem,
	VariablePackInd,
	CtsRelCode,
	ClaimNo,
	PriceDerivation,
	ProductCode,
	Qty,
	TradingPrice,
	LinePrice,
	BranchMargin,
	PaddingReleaseVal,
	ExtraAddVal,
	MarketingAddVal,
	PromoAddVal,
	RealCost,
	RealCostAllTerms,
	CcProfit,
	ApTradingMarginPadRelease,
	FinMargin,
	CustCntNo,
	MthdOfDespatch,
	TransType,
	SalesOrigin,
	TakenBy,
	MethodOfPayment,
	DelNo,
	HeaderID,
	CrdOrigInvNo,
	CrdOrigInvDate,
	OriginalOrdNo,
	CarriagePack,
	DelAddPhone,
	SrcAddr1,
	SrcAddr2,
	SrcAddr3,
	SrcAddr4,
	SrcAddr5,
	SrcPostCode,
	GpAllocBranchCode,
	PyramidCode,
	SalesOriginDesc,
	LineNumber,
	PriceOrderType,
	TransactionDate,
	OrderDate,
	RetailPrice,
	UniqueId,
	RedPrice,
	AmberPrice,
	GreenPrice,
	BluePrice,
	TerminalEmulator,
	PriceGuidanceIndicator,
	UnitPrice,
	CreditAdvanousInvoiceLineId,
	EffectiveQty,
	EffectiveLinePrice,
	EffectiveApTradingMarginPadRelease,
	CustOrdRef,
	BranchBrand,
	TradingBranchPrimaryBrand,
	AccountingBrand,
	SuppliersNettCost,
	SuppliersNettCostMNTHDay1,
	GETDATE()
FROM Lion_Staging.dbo.Transactions t
WHERE TransactionDate IS NOT NULL
	AND TransactionDate < @ArchiveDate
SET @RowsI = @RowsI + @@RowCount
EXEC LogDCPEvent 'ETL - Load_StagingTransactionArchive - Insert', 'E', @RowsI, @RowsU, @RowsD



EXEC LogDCPEvent 'ETL - Load_StagingTransactionArchive - Delete', 'B'

DELETE t FROM lion_staging.dbo.transactions t
WHERE EXISTS (SELECT 1 FROM @InsertedRowData i WHERE i.advanousinvoicelineid = t.advanousinvoicelineid)
SET @RowsD = @RowsD + @@RowCount

SET IDENTITY_INSERT Lion_Staging.dbo.TransactionsArchive OFF



EXEC LogDCPEvent 'ETL - Load_StagingTransactionArchive - Delete', 'E', @RowsI, @RowsU, @RowsD





EXEC LogDCPEvent 'ETL - Load_StagingTransactionArchive', 'E', @RowsI, @RowsU, @RowsD


GO
