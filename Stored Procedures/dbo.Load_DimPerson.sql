SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[Load_DimPerson]
	@ParamFullLoad [CHAR](1) = 'Y'
WITH EXECUTE AS CALLER
AS
/*
EXEC [dbo].[Load_DimPerson] 'Y'
*/

SET NOCOUNT ON

DECLARE @RowsI Quantity_Normal_type, @RowsU Quantity_Normal_type, @RowsD Quantity_Normal_type
DECLARE @NullDimPersonKey Key_Normal_Type

SET @RowsI = 0
SET @RowsU = 0
SET @RowsD = 0


EXEC LogDCPEvent 'ETL - Load_DimPerson', 'B'

DECLARE 
	@ExtID Description_Normal_Type, 
	@Email Description_Normal_Type, 
	@FullName Description_Normal_Type, 
	@ODSPersonKey Key_Normal_Type,
	@UserName Description_Normal_Type, 
	@Password Description_Normal_Type, 
	@NumManageRelationships INT, 
	@NumViewRelationships INT,
	@DimAccountManagerKey Key_Normal_Type,
	@DimPersonKey Key_Normal_Type,
	@WebUserKey Key_Normal_Type,
	@UnknownDimAccountGroup1 Key_Normal_Type,
	@TodayDayKey Key_Small_Type,
	@AS_MANAGES_IS_MANAGED_BY INT,
	@AS_VIEWS_IS_VIEWED_BY INT,
	@SupportEmail NVARCHAR(4000)


SET @AS_MANAGES_IS_MANAGED_BY = 1
SET @AS_VIEWS_IS_VIEWED_BY = 2

SET @TodayDayKey = (SELECT DayKey FROM DimDay WHERE CONVERT(VARCHAR(10), SQLDate, 120) = CONVERT(VARCHAR(10), GETDATE(), 120))

SET @UnknownDimAccountGroup1 = (SELECT AccountGroup1Key FROM DimAccountGroup1 WHERE AG1Level1 = 'Unknown')

SELECT @SupportEmail = ParamValue FROM [Param] WHERE ParamName = 'NewUserPasswordEmail'


IF NOT EXISTS (SELECT 1 FROM DimPerson WHERE FullName = 'Unknown')
BEGIN
	INSERT DimPerson (FullName,CreationDayKey, ModificationDayKey)
	VALUES ('Unknown', @TodayDayKey, @TodayDayKey)
	SET @RowsI = @RowsI + @@ROWCOUNT
END

SET @NullDimPersonKey = (SELECT DimPersonKey FROM DimPerson WHERE FullName = 'Unknown')

IF NOT EXISTS (SELECT 1 FROM DimAccountManager WHERE DimPersonKey = @NullDimPersonKey)
BEGIN
	INSERT DimAccountManager (DimPersonKey, CreationDayKey, ModificationDayKey, AccountManagerCode)
	VALUES (@NullDimPersonKey, @TodayDayKey, @TodayDayKey, 'Unknown')
	SET @RowsI = @RowsI + @@ROWCOUNT
END


--------------FIRST, update all changed DimPersons-----------------------------
--This table will contain a row for every row in the dimension table that has been changed in the staging table (type 1 update will be done)
CREATE TABLE #UpdateKeys (KeyID INTEGER)

--Compare the checksum of the current dimension data to the checksum of the staging data
--For all ODSPersons
INSERT INTO #UpdateKeys (KeyID)
SELECT B.ODSPersonKey 
FROM DimPerson A
JOIN ODSPerson B ON A.ODSPersonKey = B.ODSPersonKey
WHERE 
	CHECKSUM( --CheckSum for WebUser table
		A.FullName,
		A.Email,
		A.DimPersonUDVarchar1,
		A.DimPersonUDVarchar2,
		A.DimPersonUDVarchar3,
		A.LevelId)
	<>
	CHECKSUM(--CheckSum for staging table
		B.FullName,
		B.Email,
		B.ODSPersonUDVarchar1,
		B.ODSPersonUDVarchar2,
		B.ODSPersonUDVarchar3,
		B.LevelId)


--This will perform the update on all ODSPersons who changed
UPDATE DimPerson SET 
	DimPerson.FullName = ODSPerson.FullName,
	DimPerson.Email = ODSPerson.Email,
	ModificationDayKey = @TodayDayKey
FROM ODSPerson
WHERE ODSPerson.ODSPersonKey IN (SELECT KeyID FROM #UpdateKeys)
AND DimPerson.ODSPersonKey = ODSPerson.ODSPersonKey
SET @RowsU = @@ROWCOUNT + @RowsU





/*** Now do new user Reconciliation ***/

IF @ParamFullLoad = 'Y'
BEGIN

	-- get at table with all the relevant user info that we will need
	SELECT DISTINCT
		odsp.ODSPersonKey,
		odsp.ExtID,
		odsp.FullName,
		odsp.Email,
		odsp.LevelId,
		odsp.ODSPersonUDVarchar1,
		odsp.ODSPersonUDVarchar2,
		odsp.ODSPersonUDVarchar3,
		CASE
			WHEN dp.ODSPersonKey IS NULL THEN 'Y'
			ELSE 'N'
		END AS NewUserIndicator
	INTO
		#UserInfoTemp
	FROM
		ODSPerson odsp 
	LEFT JOIN
		DimPerson dp ON odsp.ODSPersonKey = dp.ODSPersonKey
	 --  SELECT * FROM #UserInfoTemp
	--   DROP TABLE #UserInfoTemp

	BEGIN TRAN

	INSERT DimPerson (FullName, Email, ODSPersonKey, CreationDayKey, ModificationDayKey) 
	SELECT 
		FullName,
		Email,
		ODSPersonKey,
		@TodayDayKey,
		@TodayDayKey
	FROM 
		#UserInfoTemp
	WHERE
		NewUserIndicator = 'Y'
	SET @RowsI = @RowsI + @@ROWCOUNT

	DECLARE NewUsers 
	CURSOR 
	FOR
	SELECT
		ExtId AS UserName, 
		ODSPersonKey,
		FullName,
		Email
	FROM #UserInfoTemp
	WHERE
		NewUserIndicator = 'Y'
	ORDER BY 1,2

	OPEN NewUsers

	FETCH NEXT FROM NewUsers INTO 
		@UserName, 
		@ODSPersonKey,
		@FullName,
		@Email

	WHILE @@FETCH_STATUS = 0
	BEGIN
	
		SELECT @Password = 'Margin01'
		--EXEC RandomPasswordGenerator @Password OUTPUT
		--SELECT dbo.fn_HashPassword('Margin01')
		-- Reset ALL: UPDATE WebUser SET WebUserPassword = dbo.fn_HashPassword(N'Margin01'), WebUserPasswordExpirationDate = DATEADD(DAY, 90, GETDATE())
			
		--RAISERROR ('Adding new user; full name: %s; user name: %s; p: %s; odspersonkey: %d', 0, 1, @FullName, @UserName, @Password, @ODSPersonKey) WITH NOWAIT
		RAISERROR ('Adding new user; full name: %s; user name: %s; odspersonkey: %d', 0, 1, @FullName, @UserName, @ODSPersonKey) WITH NOWAIT 		

		INSERT WebUser (UserName, WebUserPassword, ODSPersonKey, WebUserPasswordExpirationDate)
		SELECT
			@UserName, 
			dbo.fn_HashPassword(@Password),			--dbo.fn_GetPasswordHash(@Password),
			@ODSPersonKey, 
			GETDATE()
		SET @RowsI = @RowsI + @@ROWCOUNT

		--EXEC EmailSupportOfNewUser @SupportEmail, @FullName, @Email, @UserName, @Password
		
		--EXEC EmailSupportOfNewUser 'kkennedy@enterbridge.com', @FullName, @Email, @UserName, @Password
		
		--EXEC EmailSupportOfNewUser 'kkennedy@enterbridge.com', 'John M. Tuttle', 'jtuttle@mash.com', 'jtuttle', 'margin'

		FETCH NEXT FROM NewUsers INTO 
			@UserName, 
			@ODSPersonKey,
			@FullName,
			@Email

	END

	CLOSE NewUsers
	DEALLOCATE NewUsers


	--INSERT WebUser (UserName, WebUserPassword, ODSPersonKey, WebUserPasswordExpirationDate) 
	--SELECT
	--	u.UserName,
	--	NULL,	-- dbo.fn_HashPassword(@Password),
	--	ODSPersonKey,
	--	NULL
	--FROM #UserInfoTemp
	--INNER JOIN _Staging.dbo.[User] u
	--	ON u.ExtId = #UserInfoTemp.ExtID
	--WHERE
	--	NewUserIndicator = 'Y'
	--SET @RowsI = @RowsI + @@ROWCOUNT


	DECLARE
		@P2PUserWebRoleKey INT,
		@AdministratorWebRoleKey INT,
		@PPEManagerWebRoleKey INT,
		@CentralPricingWebRoleKey INT,
		@CentralBusinessUnitWebRoleKey INT,
		@RegionManagerWebRoleKey INT,
		@AreaManagerWebRoleKey INT,
		@NetworkManagerWebRoleKey INT,
		@BranchManagerWebRoleKey INT,
		@DelegateWebRoleKey INT
		
	SELECT @P2PUserWebRoleKey = WebRoleKey 
	FROM dbo.WebRole 
	WHERE WebRoleName = 'P2PUser'

	SELECT @AdministratorWebRoleKey = WebRoleKey 
	FROM dbo.WebRole 
	WHERE WebRoleName = 'Administrator'

	SELECT @PPEManagerWebRoleKey = WebRoleKey 
	FROM dbo.WebRole 
	WHERE WebRoleName = 'PPEManager'

	SELECT @CentralPricingWebRoleKey = WebRoleKey 
	FROM dbo.WebRole 
	WHERE WebRoleName = 'CentralPricing'

	SELECT @CentralBusinessUnitWebRoleKey = WebRoleKey 
	FROM dbo.WebRole 
	WHERE WebRoleName = 'CentralBusinessUnit'

	SELECT @RegionManagerWebRoleKey = WebRoleKey 
	FROM dbo.WebRole 
	WHERE WebRoleName = 'RegionManager'

	SELECT @AreaManagerWebRoleKey = WebRoleKey 
	FROM dbo.WebRole 
	WHERE WebRoleName = 'AreaManager'

	SELECT @NetworkManagerWebRoleKey = WebRoleKey 
	FROM dbo.WebRole 
	WHERE WebRoleName = 'NetworkManager'

	SELECT @BranchManagerWebRoleKey = WebRoleKey 
	FROM dbo.WebRole 
	WHERE WebRoleName = 'BranchManager'

	SELECT @DelegateWebRoleKey = WebRoleKey 
	FROM dbo.WebRole 
	WHERE WebRoleName = 'Delegate'

	-- check our special users for their roles --
	DECLARE	
		@CoordinatorWebUserKey INT
		
	SELECT TOP 1
		@CoordinatorWebUserKey = MIN(WebUserKey)
	FROM WebUser wu
	INNER JOIN dbo.ODSPerson odsp
		ON odsp.ODSPersonKey = wu.ODSPersonKey
	WHERE 
		odsp.FullName = 'coordinator'
		OR odsp.ExtID = '0'

	IF (SELECT COUNT(*) FROM WebUserRole WHERE WebUserKey = @CoordinatorWebUserKey) <= 0
	BEGIN
		INSERT INTO WebUserRole (WebUserKey, WebRoleKey)
		SELECT @CoordinatorWebUserKey, wr.WebRoleKey
		FROM dbo.WebRole wr 
		WHERE @CoordinatorWebUserKey IS NOT NULL
	END
	

	-- delete any (hierarchy) Permissions/Roles users no longer have
	DELETE FROM dbo.WebUserPermission
	WHERE WebUserPermissionKey IN (
		SELECT wup.WebUserPermissionKey
		FROM dbo.ODSPerson odsp
		INNER JOIN WebUser wu 
			ON wu.ODSPersonKey = odsp.ODSPersonKey
		INNER JOIN dbo.WebUserRole wur
			ON wur.WebUserKey = wu.WebUserKey
		INNER JOIN dbo.WebRole wr
			ON wr.WebRoleKey = wur.WebRoleKey
			AND wr.WebRoleName IN ('Administrator', 'PPEManager', 'CentralPricing', 'CentralBusinessUnit', 'RegionManager', 'AreaManager', 'NetworkManager','BranchManager')
		INNER JOIN dbo.WebRolePermission wrp
			ON wrp.WebRoleKey = wr.WebRoleKey
		INNER JOIN dbo.WebUserPermission wup
			ON wup.WebUserKey = wu.WebUserKey
			AND wup.WebPermissionKey = wrp.WebPermissionKey
		WHERE 
			odsp.LevelId <> WebRoleName
		)

	DELETE from dbo.WebUserRole
	WHERE WebUserRoleKey IN (
		SELECT wur.WebUserRoleKey
		FROM dbo.ODSPerson odsp
		INNER JOIN WebUser wu 
			ON wu.ODSPersonKey = odsp.ODSPersonKey
		INNER JOIN dbo.WebUserRole wur
			ON wur.WebUserKey = wu.WebUserKey
		INNER JOIN dbo.WebRole wr
			ON wr.WebRoleKey = wur.WebRoleKey
			AND wr.WebRoleName IN ('CentralPricing', 'CentralBusinessUnit', 'RegionManager', 'AreaManager', 'NetworkManager','BranchManager')
		WHERE 
			odsp.LevelId <> WebRoleName
		)
	
		
	-- ensure that web users have the latest role assignment (permissions [for the role] first, then the role)
	INSERT dbo.WebUserPermission ( WebUserKey , WebPermissionKey , CreationDate )
	SELECT wu.WebUserKey, wrp.WebPermissionKey, GETDATE()
	FROM dbo.ODSPerson odsp
	INNER JOIN WebUser wu 
		ON wu.ODSPersonKey = odsp.ODSPersonKey
	INNER JOIN dbo.WebRole wr
		ON wr.WebRoleName = odsp.LevelId
	INNER JOIN dbo.WebRolePermission wrp
		ON wrp.WebRoleKey = wr.WebRoleKey
	WHERE
		NOT EXISTS (SELECT 1 FROM WebUserRole wur WHERE wur.WebUserKey = wu.WebUserKey AND wur.WebRoleKey = wr.WebRoleKey)
		AND WebUserKey <> @CoordinatorWebUserKey
		
	INSERT WebUserRole (WebUserKey, WebRoleKey)
	SELECT DISTINCT WebUserKey, wr.WebRoleKey AS WebRoleKey
	FROM 
		ODSPerson odsp 
	INNER JOIN WebUser wu 
		ON wu.ODSPersonKey = odsp.ODSPersonKey
	INNER JOIN dbo.WebRole wr
		ON wr.WebRoleName = odsp.LevelId
	WHERE
		NOT EXISTS (SELECT 1 FROM WebUserRole wur WHERE wur.WebUserKey = wu.WebUserKey AND wur.WebRoleKey = wr.WebRoleKey)
		AND WebUserKey <> @CoordinatorWebUserKey

	-- give new users access to the system by assigning P2PUser role
	INSERT WebUserRole (WebUserKey, WebRoleKey)
	SELECT DISTINCT WebUserKey, @P2PUserWebRoleKey AS WebRoleKey
	FROM 
		#UserInfoTemp t 
	INNER JOIN WebUser wu 
		ON wu.ODSPersonKey = t.ODSPersonKey
	WHERE 
		NewUserIndicator = 'Y'
		AND WebUserKey <> @CoordinatorWebUserKey
		AND NOT EXISTS (SELECT 1 FROM WebUserRole wur WHERE wur.WebUserKey = wu.WebUserKey AND wur.WebRoleKey = @P2PUserWebRoleKey)
	SET @RowsI = @RowsI + @@ROWCOUNT


	--INSERT WebUserRole (WebUserKey, WebRoleKey)
	--SELECT DISTINCT WebUserKey, @NetworkManagerWebRoleKey AS WebRoleKey
	--FROM 
	--	#UserInfoTemp t 
	--INNER JOIN WebUser wu 
	--	ON wu.ODSPersonKey = t.ODSPersonKey
	--WHERE 
	--	NewUserIndicator = 'Y'
	--	AND WebUserKey <> @CoordinatorWebUserKey
	--	AND ODSPersonUDVarchar1 = 'NetworkManager'  -- purpose; level 2/network manager
	--SET @RowsI = @RowsI + @@ROWCOUNT

	--INSERT WebUserRole (WebUserKey, WebRoleKey)
	--SELECT DISTINCT WebUserKey, @BranchManagerWebRoleKey AS WebRoleKey
	--FROM 
	--	#UserInfoTemp t 
	--INNER JOIN WebUser wu 
	--	ON wu.ODSPersonKey = t.ODSPersonKey
	--WHERE 
	--	NewUserIndicator = 'Y'
	--	AND WebUserKey <> @CoordinatorWebUserKey
	--	AND ODSPersonUDVarchar1 = 'BranchManager'  -- purpose; level 1/branch manager
	--SET @RowsI = @RowsI + @@ROWCOUNT

	--INSERT WebUserRole (WebUserKey, WebRoleKey)
	--SELECT DISTINCT WebUserKey, @PPEManagerWebRoleKey AS WebRoleKey
	--FROM 
	--	#UserInfoTemp t 
	--INNER JOIN WebUser wu 
	--	ON wu.ODSPersonKey = t.ODSPersonKey
	--WHERE 
	--	NewUserIndicator = 'Y'
	--	AND WebUserKey <> @CoordinatorWebUserKey
	--	AND ODSPersonUDVarchar1 = 'PPEManager'
	--SET @RowsI = @RowsI + @@ROWCOUNT

	

	/* Now reconcile DimAccountManager inserts and WebUser.AccountManagerKey updates */
	-- INSERT into DimAccountManager if they manage accounts (or manage others that manage accounts - downline)
	INSERT DimAccountManager (DimPersonKey, AccountManagerCode, PrimaryAccountGroup1Key, CreationDayKey, ModificationDayKey) 
	SELECT DISTINCT
		dp.DimPersonKey,
		ExtID,
		@UnknownDimAccountGroup1,
 		@TodayDayKey,
 		@TodayDayKey
	FROM #UserInfoTemp t
	INNER JOIN DimPerson dp 
		ON dp.ODSPersonKey = t.ODSPersonKey
	WHERE
		--NumAccountsManaged + NumMLAAccountsManaged > 0 AND
		dp.DimPersonKey NOT IN (SELECT DimPersonKey FROM DimAccountManager)
	SET @RowsI = @RowsI + @@ROWCOUNT


	-- UPDATE the AccountMangerKey column in the WebUserTable
	UPDATE WebUser SET
		AccountManagerKey = dam.AccountManagerKey
	FROM
		WebUser INNER JOIN 
		ODSPerson odsp ON
			WebUser.ODSPersonKey = odsp.ODSPersonKey INNER JOIN
		DimPerson dp ON
			odsp.ODSPersonKey = dp.ODSPersonKey INNER JOIN
		DimAccountManager dam ON
			dp.DimPersonKey = dam.DimPersonKey
	WHERE
		(WebUser.AccountManagerKey <> dam.AccountManagerKey)
		OR (WebUser.AccountManagerKey IS NULL)
	SET @RowsU = @RowsU + @@ROWCOUNT




	/* set tables for hierarchy */

	-- fully rebuild this table based on what's in UserAssoc
	TRUNCATE TABLE WebUserAccountManager

	INSERT WebUserAccountManager (WebUserKey, AccountManagerKey, AccessType, Version)
	SELECT DISTINCT WebUserKey, AccountManagerKey, 'Modify', 0
	FROM WebUser
	WHERE AccountManagerKey IS NOT NULL




	DROP TABLE #UserInfoTemp

	COMMIT TRAN



END

EXEC LogDCPEvent 'ETL - Load_DimPerson', 'E', @RowsI, @RowsU, @RowsD

















GO
