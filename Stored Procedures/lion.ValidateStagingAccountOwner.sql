SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE PROCEDURE [lion].[ValidateStagingAccountOwner] (
	@ParamDebug CHAR(1) = 'N'
)
AS

/*

EXEC lion.ValidateStagingAccountOwner

TRUNCATE TABLE lion.StagingAccountOwner
EXEC lion.ValidateStagingAccountOwner 'Y'

*/



SET NOCOUNT ON



IF @ParamDebug = 'Y'
BEGIN

	INSERT lion.StagingAccountOwner (
		AccountNumber,
		AccountName,
		PostCode,
		NationalOwner,
		NationalOwnerEmail,
		PlumbOwner,
		PlumbOwnerEmail,
		PartsOwner,
		PartsOwnerEmail,
		DrainOwner,
		DrainOwnerEmail,
		PipeOwner,
		PipeOwnerEmail,
		ClimateOwner,
		ClimateOwnerEmail,
		BrandOwnerBurdens,
		BrandOwnerBurdensEmail,
		FusionOwner,
		FusionOwnerEmail,
		UPSOwner,
		UPSOwnerEmail,
		RowIndex,
		Message
	)
	VALUES (
		'7849J38', -- Account Number
		'URQUHART & CO (PHE) LTD', -- Account Name
		'', -- Post code
		'', -- National owner
		'', -- National owner email
		'Allan Ross', -- Plumb owner
		'allan.ross@wolseley.co.uk', -- Plumb ower email
		'Iain Beaton', -- Parts owner
		'iain.beaton@wolseley.co.uk', -- Parts owner email
		'', -- Drain owner
		'', -- Drain owner email
		'Andy Duguid', -- Pipe owner
		'andy.duguid@wolseley.co.uk', -- Pipe owner email
		'Andy Duguid', -- Climate owner
		'andy.duguid@wolseley.co.uk', -- Climate owner email
		'', -- Burdens owner
		'', -- Burdens owner email,
		'',
		'',
		'',
		'',
		1, -- RowIndex
		'' -- Message
	)

	--INSERT lion.StagingAccountOwner (
	--	AccountNumber,
	--	AccountName,
	--	PostCode,
	--	NationalOwner,
	--	NationalOwnerEmail,
	--	PlumbOwner,
	--	PlumbOwnerEmail,
	--	PartsOwner,
	--	PartsOwnerEmail,
	--	DrainOwner,
	--	DrainOwnerEmail,
	--	PipeOwner,
	--	PipeOwnerEmail,
	--	ClimateOwner,
	--	ClimateOwnerEmail,
	--	BrandOwnerBurdens,
	--	BrandOwnerBurdensEmail,
	--	RowIndex,
	--	Message
	--)
	--VALUES (
	--	'7849J38zzz', -- Account Number
	--	'URQUHART & CO (PHE) LTD', -- Account Name
	--	'', -- Post code
	--	'', -- National owner
	--	'', -- National owner email
	--	'Allan Ross', -- Plumb owner
	--	'allan.ross@wolseley.co.uk', -- Plumb ower email
	--	'Iain Beaton', -- Parts owner
	--	'iain.beaton@wolseley.co.uk', -- Parts owner email
	--	'', -- Drain owner
	--	'', -- Drain owner email
	--	'Andy Duguid', -- Pipe owner
	--	'andy.duguid@wolseley.co.uk', -- Pipe owner email
	--	'Andy Duguid', -- Climate owner
	--	'andy.duguid@wolseley.co.uk', -- Climate owner email
	--	'', -- Burdens owner
	--	'', -- Burdens owner email
	--	2, -- RowIndex
	--	'' -- Message
	--)

	--INSERT lion.StagingAccountOwner (
	--	AccountNumber,
	--	AccountName,
	--	PostCode,
	--	NationalOwner,
	--	NationalOwnerEmail,
	--	PlumbOwner,
	--	PlumbOwnerEmail,
	--	PartsOwner,
	--	PartsOwnerEmail,
	--	DrainOwner,
	--	DrainOwnerEmail,
	--	PipeOwner,
	--	PipeOwnerEmail,
	--	ClimateOwner,
	--	ClimateOwnerEmail,
	--	BrandOwnerBurdens,
	--	BrandOwnerBurdensEmail,
	--	RowIndex,
	--	Message
	--)
	--VALUES (
	--	'7849J32', -- Account Number
	--	'URQUHART & CO (PHE) LTD', -- Account Name
	--	'', -- Post code
	--	'aaa', -- National owner
	--	'aaa', -- National owner email
	--	'Allan Ross', -- Plumb owner
	--	'allan.rosswolseley.co.uk', -- Plumb ower email
	--	'Iain Beaton', -- Parts owner
	--	'iainbeatonwolseleycouk', -- Parts owner email
	--	'bbb', -- Drain owner
	--	'bbb', -- Drain owner email
	--	'Andy Duguid', -- Pipe owner
	--	'andy.duguidwolseleycouk', -- Pipe owner email
	--	'Andy Duguid', -- Climate owner
	--	'andyduguidwolseleycouk', -- Climate owner email
	--	'ccc', -- Burdens owner
	--	'ccc', -- Burdens owner email
	--	3, -- RowIndex
	--	'' -- Message
	--)

	--INSERT lion.StagingAccountOwner (
	--	AccountNumber,
	--	AccountName,
	--	PostCode,
	--	NationalOwner,
	--	NationalOwnerEmail,
	--	PlumbOwner,
	--	PlumbOwnerEmail,
	--	PartsOwner,
	--	PartsOwnerEmail,
	--	DrainOwner,
	--	DrainOwnerEmail,
	--	PipeOwner,
	--	PipeOwnerEmail,
	--	ClimateOwner,
	--	ClimateOwnerEmail,
	--	BrandOwnerBurdens,
	--	BrandOwnerBurdensEmail,
	--	RowIndex,
	--	Message
	--)
	--VALUES (
	--	'7849J38', -- Account Number
	--	'URQUHART & CO (PHE) LTD', -- Account Name
	--	'', -- Post code
	--	'', -- National owner
	--	'', -- National owner email
	--	'Allan Ross', -- Plumb owner
	--	'allan.ross@wolseley.co.uk', -- Plumb ower email
	--	'Iain Beaton', -- Parts owner
	--	'iain.beaton@wolseley.co.uk', -- Parts owner email
	--	'', -- Drain owner
	--	'', -- Drain owner email
	--	'Andy Duguid', -- Pipe owner
	--	'andy.duguid@wolseley.co.uk', -- Pipe owner email
	--	'Andy Duguid', -- Climate owner
	--	'andy.duguid@wolseley.co.uk', -- Climate owner email
	--	'', -- Burdens owner
	--	'', -- Burdens owner email
	--	2, -- RowIndex
	--	'' -- Message
	--)
	
	--   SELECT * FROM lion.StagingAccountOwner

END



/* initial scribbing */
UPDATE lion.StagingAccountOwner 
SET 
	Message = '',
	AccountNumber = RTRIM(LTRIM(AccountNumber)),
	AccountName = RTRIM(LTRIM(AccountName)),
	PostCode = RTRIM(LTRIM(PostCode)),
	NationalOwner = RTRIM(LTRIM(NationalOwner)),
	NationalOwnerEmail = RTRIM(LTRIM(NationalOwnerEmail)),
	PlumbOwner = RTRIM(LTRIM(PlumbOwner)),
	PlumbOwnerEmail = RTRIM(LTRIM(PlumbOwnerEmail)),
	PartsOwner = RTRIM(LTRIM(PartsOwner)),
	PartsOwnerEmail = RTRIM(LTRIM(PartsOwnerEmail)),
	DrainOwner = RTRIM(LTRIM(DrainOwner)),
	DrainOwnerEmail = RTRIM(LTRIM(DrainOwnerEmail)),
	PipeOwner = RTRIM(LTRIM(PipeOwner)),
	PipeOwnerEmail = RTRIM(LTRIM(PipeOwnerEmail)),
	ClimateOwner = RTRIM(LTRIM(ClimateOwner)),
	ClimateOwnerEmail = RTRIM(LTRIM(ClimateOwnerEmail)),
	BrandOwnerBurdens = RTRIM(LTRIM(BrandOwnerBurdens)),
	BrandOwnerBurdensEmail = RTRIM(LTRIM(BrandOwnerBurdensEmail)),
	FusionOwner = RTRIM(LTRIM(FusionOwner)),
	FusionOwnerEmail = RTRIM(LTRIM(FusionOwnerEmail)),
	UPSOwner = RTRIM(LTRIM(UPSOwner)),
	UPSOwnerEmail = RTRIM(LTRIM(UPSOwnerEmail)),
	RowIndex = RTRIM(LTRIM(RowIndex))



UPDATE s
SET 
	s.Message = ISNULL(s.Message, N'') + N'Empty/Null row ' 
FROM lion.StagingAccountOwner s
WHERE 
	ISNULL(NULLIF(AccountNumber,'NULL'),N'') = N''
	AND ISNULL(NULLIF(AccountName,'NULL'),N'') = N''
	AND ISNULL(NULLIF(PostCode,'NULL'),N'') = N''
	AND ISNULL(NULLIF(NationalOwner,'NULL'),N'') = N''
	AND ISNULL(NULLIF(NationalOwnerEmail,'NULL'),N'') = N''
	AND ISNULL(NULLIF(PlumbOwner,'NULL'),N'') = N''
	AND ISNULL(NULLIF(PlumbOwnerEmail,'NULL'),N'') = N''
	AND ISNULL(NULLIF(PartsOwner,'NULL'),N'') = N''
	AND ISNULL(NULLIF(PartsOwnerEmail,'NULL'),N'') = N''
	AND ISNULL(NULLIF(DrainOwner,'NULL'),N'') = N''
	AND ISNULL(NULLIF(DrainOwnerEmail,'NULL'),N'') = N''
	AND ISNULL(NULLIF(PipeOwner,'NULL'),N'') = N''
	AND ISNULL(NULLIF(PipeOwnerEmail,'NULL'),N'') = N''
	AND ISNULL(NULLIF(ClimateOwner,'NULL'),N'') = N''
	AND ISNULL(NULLIF(ClimateOwnerEmail,'NULL'),N'') = N''
	AND ISNULL(NULLIF(BrandOwnerBurdens,'NULL'),N'') = N''
	AND ISNULL(NULLIF(BrandOwnerBurdensEmail,'NULL'),N'') = N''
	AND ISNULL(NULLIF(FusionOwner,'NULL'),N'') = N''
	AND ISNULL(NULLIF(FusionOwnerEmail,'NULL'),N'') = N''
	AND ISNULL(NULLIF(UPSOwner,'NULL'),N'') = N''
	AND ISNULL(NULLIF(UPSOwnerEmail,'NULL'),N'') = N''




/* check file for dups */
; WITH a AS (
	SELECT
		RowIndex,
		MIN(RowIndex) OVER (PARTITION BY 
			AccountNumber,
			AccountName,
			PostCode,
			NationalOwner,
			NationalOwnerEmail,
			PlumbOwner,
			PlumbOwnerEmail,
			PartsOwner,
			PartsOwnerEmail,
			DrainOwner,
			DrainOwnerEmail,
			PipeOwner,
			PipeOwnerEmail,
			ClimateOwner,
			ClimateOwnerEmail,
			BrandOwnerBurdens,
			BrandOwnerBurdensEmail,
			FusionOwner,
			FusionOwnerEmail,
			UPSOwner,
			UPSOwnerEmail
			ORDER BY 
				RowIndex 
			) FirstRowIndex,
		ROW_NUMBER() OVER (PARTITION BY 
			AccountNumber,
			AccountName,
			PostCode,
			NationalOwner,
			NationalOwnerEmail,
			PlumbOwner,
			PlumbOwnerEmail,
			PartsOwner,
			PartsOwnerEmail,
			DrainOwner,
			DrainOwnerEmail,
			PipeOwner,
			PipeOwnerEmail,
			ClimateOwner,
			ClimateOwnerEmail,
			BrandOwnerBurdens,
			BrandOwnerBurdensEmail,
			FusionOwner,
			FusionOwnerEmail,
			UPSOwner,
			UPSOwnerEmail
			ORDER BY 
				RowIndex 
			) 
		AS RowRank
	FROM lion.StagingAccountOwner s 
)
UPDATE s
SET 
	s.Message = ISNULL(s.Message, N'') + N'Row duplicates row ' + CAST(FirstRowIndex AS NVARCHAR(25)) + N' '
FROM lion.StagingAccountOwner s
INNER JOIN a 
	ON a.RowIndex = s.RowIndex
WHERE
	a.RowRank <> 1 



/* check file for dup account numbers */
; WITH a AS (
	SELECT
		RowIndex,
		MIN(RowIndex) OVER (PARTITION BY 
			AccountNumber
			ORDER BY 
				RowIndex 
			) FirstRowIndex,
		ROW_NUMBER() OVER (PARTITION BY 
			AccountNumber
			ORDER BY 
				RowIndex 
			) 
		AS RowRank
	FROM lion.StagingAccountOwner s 
)
UPDATE s
SET 
	s.Message = ISNULL(s.Message, N'') + N'Row''s Account Id/Number duplicates row ' + CAST(FirstRowIndex AS NVARCHAR(25)) + N' '
FROM lion.StagingAccountOwner s
INNER JOIN a 
	ON a.RowIndex = s.RowIndex
WHERE
	a.RowRank <> 1 



/* check for bad account number/id */
UPDATE s
SET 
	s.Message = ISNULL(s.Message, N'') + N'Invalid Account Id ' 
FROM lion.StagingAccountOwner s
WHERE 
	NOT EXISTS (SELECT 1 FROM dbo.DimAccount da WHERE da.AccountNumber = s.AccountNumber)



/* check for valid formatted email addresses */
UPDATE s
SET 
	s.Message = ISNULL(s.Message, N'') + N'Invalid National Email Address ' 
FROM lion.StagingAccountOwner s
WHERE 
	NationalOwnerEmail <> N'' 
	AND (
		CHARINDEX(N'@', NationalOwnerEmail) <= 0
		OR CHARINDEX(N'.', NationalOwnerEmail) <= 0
	)

UPDATE s
SET 
	s.Message = ISNULL(s.Message, N'') + N'Invalid Plumb Address ' 
FROM lion.StagingAccountOwner s
WHERE
	PlumbOwnerEmail <> N'' 
	AND ( 
		CHARINDEX(N'@', PlumbOwnerEmail) <= 0
		OR CHARINDEX(N'.', PlumbOwnerEmail) <= 0
	)

UPDATE s
SET 
	s.Message = ISNULL(s.Message, N'') + N'Invalid Parts Email Address ' 
FROM lion.StagingAccountOwner s
WHERE 
	PartsOwnerEmail <> N'' 
	AND (
		CHARINDEX(N'@', PartsOwnerEmail) <= 0
		OR CHARINDEX(N'.', PartsOwnerEmail) <= 0
	)

UPDATE s
SET 
	s.Message = ISNULL(s.Message, N'') + N'Invalid Drain Email Address ' 
FROM lion.StagingAccountOwner s
WHERE 
	DrainOwnerEmail <> N'' 
	AND (
		CHARINDEX(N'@', DrainOwnerEmail) <= 0
		OR CHARINDEX(N'.', DrainOwnerEmail) <= 0
	)

UPDATE s
SET 
	s.Message = ISNULL(s.Message, N'') + N'Invalid Pipe Email Address ' 
FROM lion.StagingAccountOwner s
WHERE 
	PipeOwnerEmail <> N'' 
	AND (
		CHARINDEX(N'@', PipeOwnerEmail) <= 0
		OR CHARINDEX(N'.', PipeOwnerEmail) <= 0
	)

UPDATE s
SET 
	s.Message = ISNULL(s.Message, N'') + N'Invalid Climate Email Address ' 
FROM lion.StagingAccountOwner s
WHERE 
	ClimateOwnerEmail <> N'' 
	AND (
		CHARINDEX(N'@', ClimateOwnerEmail) <= 0
		OR CHARINDEX(N'.', ClimateOwnerEmail) <= 0
	)

UPDATE s
SET 
	s.Message = ISNULL(s.Message, N'') + N'Invalid Burdens Email Address ' 
FROM lion.StagingAccountOwner s
WHERE 
	BrandOwnerBurdensEmail <> N'' 
	AND (
		CHARINDEX(N'@', BrandOwnerBurdensEmail) <= 0
		OR CHARINDEX(N'.', BrandOwnerBurdensEmail) <= 0
	)

UPDATE s
SET 
	s.Message = ISNULL(s.Message, N'') + N'Invalid Fusion Email Address ' 
FROM lion.StagingAccountOwner s
WHERE 
	FusionOwnerEmail <> N'' 
	AND (
		CHARINDEX(N'@', FusionOwnerEmail) <= 0
		OR CHARINDEX(N'.', FusionOwnerEmail) <= 0
	)

UPDATE s
SET 
	s.Message = ISNULL(s.Message, N'') + N'Invalid UPS Email Address ' 
FROM lion.StagingAccountOwner s
WHERE 
	UPSOwnerEmail <> N'' 
	AND (
		CHARINDEX(N'@', UPSOwnerEmail) <= 0
		OR CHARINDEX(N'.', UPSOwnerEmail) <= 0
	)


DECLARE @ErrorTable TABLE (
	AccountNumber NVARCHAR(4000) NOT NULL,
	AccountName NVARCHAR(4000) NULL,
	PostCode NVARCHAR(4000) NULL,
	NationalOwner NVARCHAR(4000) NULL,
	NationalOwnerEmail NVARCHAR(4000) NULL,
	PlumbOwner NVARCHAR(4000) NULL,
	PlumbOwnerEmail NVARCHAR(4000) NULL,
	PartsOwner NVARCHAR(4000) NULL,
	PartsOwnerEmail NVARCHAR(4000) NULL,
	DrainOwner NVARCHAR(4000) NULL,
	DrainOwnerEmail NVARCHAR(4000) NULL,
	PipeOwner NVARCHAR(4000) NULL,
	PipeOwnerEmail NVARCHAR(4000) NULL,
	ClimateOwner NVARCHAR(4000) NULL,
	ClimateOwnerEmail NVARCHAR(4000) NULL,
	BrandOwnerBurdens NVARCHAR(4000) NULL,
	BrandOwnerBurdensEmail NVARCHAR(4000) NULL,
	FusionOwner NVARCHAR(4000) NULL,
	FusionOwnerEmail NVARCHAR(4000) NULL,
	UPSOwner NVARCHAR(4000) NULL,
	UPSOwnerEmail NVARCHAR(4000) NULL,
	RowIndex INT NOT NULL,
	Message NVARCHAR(255) NOT NULL DEFAULT ('')
)

INSERT @ErrorTable (
	AccountNumber,
	AccountName,
	PostCode,
	NationalOwner,
	NationalOwnerEmail,
	PlumbOwner,
	PlumbOwnerEmail,
	PartsOwner,
	PartsOwnerEmail,
	DrainOwner,
	DrainOwnerEmail,
	PipeOwner,
	PipeOwnerEmail,
	ClimateOwner,
	ClimateOwnerEmail,
	BrandOwnerBurdens,
	BrandOwnerBurdensEmail,
	FusionOwner,
	FusionOwnerEmail,
	UPSOwner,
	UPSOwnerEmail,
	RowIndex,
	Message
	)
SELECT
	AccountNumber,
	AccountName,
	PostCode,
	NationalOwner,
	NationalOwnerEmail,
	PlumbOwner,
	PlumbOwnerEmail,
	PartsOwner,
	PartsOwnerEmail,
	DrainOwner,
	DrainOwnerEmail,
	PipeOwner,
	PipeOwnerEmail,
	ClimateOwner,
	ClimateOwnerEmail,
	BrandOwnerBurdens,
	BrandOwnerBurdensEmail,
	FusionOwner,
	FusionOwnerEmail,
	UPSOwner,
	UPSOwnerEmail,
	RowIndex,
	Message
FROM 
	lion.StagingAccountOwner 
WHERE 
	ISNULL(Message, N'') <> N''



IF (SELECT COUNT(*) FROM @ErrorTable) >= 1
BEGIN
	
	TRUNCATE TABLE lion.StagingAccountOwner

END



SELECT
	--NEWID() AS GUID,
	* 
FROM @ErrorTable



GO
