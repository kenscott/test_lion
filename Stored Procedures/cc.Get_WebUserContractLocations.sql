SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE PROCEDURE [cc].[Get_WebUserContractLocations](@WebUserKey INT, @SearchBy Description_small_type )
/*

Purpose: Provide a list of locations (branch, area, region, network and brand) that can be selected
by the user when creating a contract.  This will define the account coverage for the contract.

Logic: Based on the webuserkey passed in, this procedure will:

	1) Determine if the user is a contract coordinator, branch manager or not (considers delegations)
	2) If so, it will return a list of all branches, areas, regions, networks and brands
	3) If not, it will return a list of branches and areas that the user has visibility to (in the hierarchy)
	
	Either way, the SearchBy parameter will be used to limit the result set based on the LocationName and
	LocationDescription.
	
Application: This procedure is currently used on the Contract Manager Account Tab for selecting
the locations that should be covered by the contract.

Examples:

	exec cc.get_WebUserContractLocations 1, 'D'    -- BRANCH
	exec cc.get_WebUserContractLocations 2082, 'DB'	-- CONTRACT COORD

	select delegatewebuserkey, count(*) from webuserdelegate  group by delegatewebuserkey having count(*) > 1
	select * from dbo.dimaccountgroup1

	EXEC cc.Get_WebUserContractLocations 3811, 'A'  -- CONTRACT COORD
		
*/

AS 

SET NOCOUNT ON
--declare @webUserkey INT
--Declare @SearchBy varchar(50)
--Set @webUserKey = 2082
--SET @SearchBy = 'd'

--Show all locations for a ContractCoordinator, BranchManager, NetworkManager or RegionalCoordinator
IF 
	@WebUserKey IN
		( SELECT WebUserKey
			FROM WebUserRole wur
			JOIN WebRole wr ON wr.WebRoleKey = wur.WebRoleKey
		    WHERE wr.WebRoleName = 'ContractCoordinator'   
				OR wr.WebRoleName = 'BranchManager'
				OR wr.WebRoleName = 'NetworkManager'
				OR wr.WebRoleName = 'RegionalCoordinator'
	)
BEGIN
	SELECT * 
		FROM cc.vw_DimLocation 
		WHERE ( Location LIKE '%' + @SearchBy + '%' OR LocationName LIKE '%' + @SearchBy + '%' ) AND
			 LocationLevelName IN ('Branch', 'Network', 'Area', 'Region', 'Brand') AND
			 BranchPyramidCode IS NOT NULL
			 AND Location IS NOT NULL
			 AND Location <> ''
			 AND Location <> 'NONE'
			AND (LocationLevelName + Location <>'BranchWUK') --LPF-5888, Don't return branch WUK in cc.Get_WebUserContractLocations
		
		ORDER BY Location
END

ELSE
BEGIN
	-- First, find the user's area (might be multiple areas if the user is a delegate across areas.
	
	DECLARE @TempArea TABLE
	(
		AreaKey int,
		Area nvarchar(50)
	);

	INSERT INTO @TempArea	
		SELECT DISTINCT	AreaKey, AG1Level3
			FROM dbo.WebUserDelegate wud
			JOIN dbo.WebUser wu 
				ON wu.WebUserKey = wud.DelegatorWebUserKey
			JOIN dbo.DimPerson dp 
				ON dp.ODSPersonKey = wu.odspersonkey
			JOIN dbo.DimAccountManager dam 
				ON dam.DimPersonKey = dp.DimPersonKey
			JOIN dbo.DimAccountGroup1 dag1 
				ON dag1.AccountGroup1Key = dam.PrimaryAccountGroup1Key
			WHERE wud.DelegateWebUserKey = @WebUserKey 
			and AG1Level3 <> 'Unknown' 
			and dag1.Inactive = 0
			AND AG1Level3 IS NOT NULL
			AND AG1Level3 <> ''
			AND AG1Level3 <> 'NONE'

	--select * from @TempArea

	-- Now, use the temp table of areas to get networks
	SELECT * 
		FROM cc.vw_DimLocation 
		WHERE ContractLocationLevelKey = 2 AND LocationKey in (
			SELECT DISTINCT networkkey 
				FROM dimaccountgroup1 dag1, @TempArea ta 
				WHERE dag1.areakey = ta.AreaKey AND 
					  dag1.Inactive = 0 AND 
					  dag1.AG1Level1UDVarchar19 IS NOT NULL AND
					  ( dag1.AG1Level1 LIKE '%' + @SearchBy + '%' OR dag1.AG1Level1UDVarchar1 LIKE '%' + @SearchBy + '%' )
		) 
		AND Location IS NOT NULL
		AND Location <> ''
		AND Location <> 'NONE'

	union
	
	-- Now, use the temp table of areas to get branches
	SELECT * FROM cc.vw_DimLocation WHERE ContractLocationLevelKey = 1 AND LocationKey in (
		SELECT DISTINCT accountgroup1key 
			FROM dimaccountgroup1 dag1, @TempArea ta 
			WHERE dag1.areakey = ta.AreaKey AND 
				  dag1.Inactive = 0 AND
				  dag1.AG1Level1UDVarchar19 IS NOT NULL AND
				  ( dag1.AG1Level1 LIKE '%' + @SearchBy + '%' OR dag1.AG1Level1UDVarchar1 LIKE '%' + @SearchBy + '%' )
	) 
	AND Location IS NOT NULL
	AND Location <> ''
	AND Location <> 'NONE'
	 AND (LocationLevelName='Branch' AND Location <>'WUK') --LPF-5888, Don't return branch WUK in cc.Get_WebUserContractLocations
	order by Location		
END


GO
