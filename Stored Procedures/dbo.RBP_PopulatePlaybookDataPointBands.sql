SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO








CREATE       PROCEDURE [dbo].[RBP_PopulatePlaybookDataPointBands]
@PlaybookDataPointGroupKey INT, @SegmentFactorPlaybookKey INT
AS
SET QUOTED_IDENTIFIER ON 
SET ANSI_NULLS ON 
SET ARITHABORT ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
SET CONCAT_NULL_YIELDS_NULL ON
SET NUMERIC_ROUNDABORT OFF
DECLARE @RowsI Quantity_Normal_type, @RowsU Quantity_Normal_type, @RowsD Quantity_Normal_type
DECLARE @ProjectKey Key_Normal_Type
SET @ProjectKey = (SELECT ProjectKey FROM PlaybookDataPointGroup WHERE PlaybookDataPointGroupKey = @PlaybookDataPointGroupKey)


DECLARE @WeightedACcountItemBanding NVARCHAR(10)
DECLARE @WeightedItemBanding NVARCHAR(10)

IF (SELECT AccountItemSalesWeight+AccountItemFrequencyWeight+AccountItemQuantityWeight FROM ATKSegmentFactorPlaybook WHERE SegmentFactorPlaybookKey = @SegmentFactorPlaybookKey) = 1
	SET @WeightedACcountItemBanding = 'On'
ELSE
	SET @WeightedACcountItemBanding = 'Off'

IF (SELECT ItemSalesWeight+ItemFrequencyWeight+ItemQuantityWeight FROM ATKSegmentFactorPlaybook WHERE SegmentFactorPlaybookKey = @SegmentFactorPlaybookKey) = 1
	SET @WeightedItemBanding = 'On'
ELSE
	SET @WeightedItemBanding = 'Off'


DECLARE @B_ItemPercentage Percent_Type
SELECT @B_ItemPercentage = B_ItemPercentage FROM ATKSegmentFactorPlaybook WHERE SegmentFactorPlaybookKey = @SegmentFactorPlaybookKey

--Calculate all of the bands for this set of data points

IF @WeightedItemBanding = 'On'	--Only calculate if the weights addd up to 100%.  If they do not, then consider this turned off
BEGIN
	EXEC dbo.RBP_CalculateItemBands @PlaybookDataPointGroupKey,@SegmentFactorPlaybookKey
END

IF @B_ItemPercentage <> 0		--If this is set to 0, consider it turned off
BEGIN
	EXEC dbo.RBP_CalculateItemQuantityBands @PlaybookDataPointGroupKey, @B_ItemPercentage
END

IF @WeightedACcountItemBanding = 'On'  --Only calculate if the weights addd up to 100%.  If they do not, then consider this turned off
BEGIN
	EXEC dbo.RBP_CalculateAccountItemBands @PlaybookDataPointGroupKey,@SegmentFactorPlaybookKey
END

EXEC dbo.RBP_CalculateFullScopeAccountBands @PlaybookDataPointGroupKey, @SegmentFactorPlaybookKey

--Exec dbo.RBP_CalculateScenarioScopeAccountBand @PlaybookDataPointGroupKey, @SegmentFactorPlaybookKey

EXEC dbo.RBP_CalculatePlaybookDataPointGroupFullScopeCoreNonCore @PlaybookDataPointGroupKey,@SegmentFactorPlaybookKey

GO
