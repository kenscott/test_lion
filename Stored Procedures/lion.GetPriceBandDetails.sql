SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [lion].[GetPriceBandDetails]
    (
      @PlaybookRuleNumber INT
    )
AS

/*
	select top 10 RPBPlaybookPricingGroupKey, * from FactLastPriceAndCost WHERE RPBPlaybookPricingGroupKey IS NOT NULL

	EXEC lion.GetPriceBandDetails 1713509
	EXEC lion.GetPriceBandDetails 1680655
	
*/
SET NOCOUNT ON

SELECT
		a.Branch,
        a.AccountManagerFullName AS [Acct Mgr Name],
        a.AccountNumber AS [Acct Number],
        a.AccountName AS [Acct Name],
        a.ItemNumber AS [Item Number],
        a.ItemDescription AS [Item Description],
        a.TotalActual12MonthPrice AS [Total Sales],
		a.Total12MonthQuantity AS [Total Quantity],
		a.LastItemPrice AS [Last Item Price],
		a.LastItemCost AS [Last Item Cost],
        a.LastItemGPP AS [Current GP %],
        a.LastItemDiscountPercent AS [Current Discount],
        a.StretchImpact AS [Stretch Impact],
        a.TargetImpact AS [Target Impact],
		a.FloorImpact AS [Floor Impact],
        a.StretchGPP AS [Green GP %],
        a.TargetGPP AS [Amber GP %],
        a.FloorGPP AS [Red GP %],
        ( LastUnitListPrice - LastItemCost ) / NULLIF(LastUnitListPrice, 0) AS [Trade GP %],
        PlaybookPricingGroupKey,
		a.PricingRuleSequenceNumber AS [Price Rule Level],
		
	a.PlaybookRegion AS [Playbook Region],
	a.CustomerSpecialism AS [Customer Specialism],
	a.Sales603010Bucket AS [Sales603010 Bucket],
	a.StrategicItem AS [Strategic Item],
	a.PriceApproach AS [Price Approach],
	a.FullItemDescription AS [Item#],
	a.LLSPG AS [LL SPG],
	a.ContractClaimsIndicator AS [Contract Claims Indicator],
	a.ShipmentType AS [Ship Type],
	a.RegionExceptionIndicator AS [Region Exception Indicator],
	a.PlaybookProductBrand AS [Playbook Item Brand],
	a.ItemPyramidCode AS [Pyramid Code] 
		--,Score
FROM -- [pg].[RBP_Iteration_1_3_BandDatapointReview_ScatterData_20150618] flpac (NOLOCK)
	Lion.vwPriceBandAttributes a

	--CROSS APPLY fn_GetSCore( (a.LastItemPrice - a.LastItemCost) / NULLIF(a.LastItemPrice, 0) ,
	--                                    FloorGPP,
	--                                    ( TargetGPP - ( .25 * ( TargetGPP - FloorGPP ) ) ), 
	--                                    ( TargetGPP + ( .25 * ( StretchGPP - TargetGPP ) ) ),
	--                                     StretchGPP) sc

WHERE   
	PlaybookPricingGroupKey = @PlaybookRuleNumber
	AND RulingMemberFlag = 'Y'

ORDER BY 
	a.LastItemGPP



-- FROM    lion.vwDataPoints DataPoints ( NOLOCK )
--         INNER JOIN lion.vwPriceBand PriceBand ( NOLOCK ) ON ( DataPoints.PlaybookPricingGroupKey = PriceBand.RPBPlaybookPricingGroupKey )
--                                                           AND ( DataPoints.AccountKey = PriceBand.AccountKey )
--                                                           AND ( DataPoints.ItemKey = PriceBand.ItemKey )
--                                                           AND ( DataPoints.InvoiceLineGroup1Key = PriceBand.InvoiceLineGroup1Key )
		--INNER JOIN Lion_Analysis.lion.vwAccount a ON a.AccountKey = PriceBand.AccountKey
		--INNER JOIN Lion_Analysis.lion.vwItem i ON i.ItemKey = PriceBand.ItemKey
--WHERE   DataPoints.RulingMemberFlag = 'Y'
--        AND DataPoints.PlaybookPricingGroupKey = @PlaybookRuleNumber
--ORDER BY PriceBand.LastItemGPP
GO
