SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [cg].[GetPriceBandDetails]
    (
      @PlaybookRuleNumber INT
    )
AS

/*
	EXEC cg.GetPriceRules

	EXEC cg.GetPriceBandDetails 1340018
	EXEC cg.GetPriceBandDetails 3189028
*/
	SELECT
			dp.Branch,
			dp.AccountManagerFullName AS [Acct Mgr Name],
			dp.AccountName AS [Acct Name],

			FullContractNumber AS [Full Contract Number],
			InstructionLevelType AS [Instruction Level Type],
			--LLSPGCode,
			--MPGCode,
			--ItemNumber,
			InstructionLevelCode AS [Instruction Level Code],
			ClaimBackPercent AS [Claim Back Percent],
			ContractReference AS [Contract Reference],
			ContractStartDate AS [Contract Start Date],
			ContractExpirationDate AS [Contract Expiration Date],
			JobSite AS [Job Site],
			JobFlag AS [Job Flag],
			CCProfit AS [CC Profit],

			dp.PlaybookPricingGroupKey,
			SectionNumber AS [Playbook Section #],
			PlaybookPricingGroupKey,
			CASE 
				WHEN PricingRuleSequenceNumber = 1
					OR PricingRuleSequenceNumber = 4
					THEN Sales603010Bucket
				ELSE 
					'ALL'
			END AS  [Sales Bucket],

			CASE 
				WHEN PricingRuleSequenceNumber BETWEEN 1 AND 2
					OR PricingRuleSequenceNumber BETWEEN 4 AND 5
					THEN CAST(JobFlag AS VARCHAR(3))
				ELSE 
					'ALL'
			END AS [Job Flag],

			CASE 
				WHEN PricingRuleSequenceNumber BETWEEN 1 AND 3
					THEN ItemNumber
				ELSE 
					'ALL'
			END AS [Item Number],

			CASE 
				WHEN PricingRuleSequenceNumber BETWEEN 4 AND 6
					THEN LLSPGCode
				ELSE 
					'ALL'
			END AS [LLSPG Code],

			CASE 
				WHEN PricingRuleSequenceNumber BETWEEN 1 AND 3
					OR PricingRuleSequenceNumber BETWEEN 4 AND 6
					THEN VendorNumber
				ELSE 
					'ALL'
			END AS [Vendor Number],
			PyramidCode AS [Pyramid Code],
		
			ScenarioName AS [Scenario Name],
			PricingRuleSequenceNumber AS [Price Rule Level],
			PricingRuleDescription AS [Price Rule Level Description],
			
			FloorClaimBackPercent AS [Floor %],
			TargetClaimBackPercent AS [Target %],
			StretchClaimBackPercent AS [Stretch%],
			MemberRank AS [Member Rank],
			RulingMemberCount AS [Ruling Member Count],
			RulingMemberFlag AS [Ruling Member Flag]

	FROM
		cg.vwDatapoint dp
    WHERE   
		PlaybookPricingGroupKey = @PlaybookRuleNumber
    ORDER BY 
		1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18
GO
