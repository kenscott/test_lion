SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE [lion].[GetTRAccounts]
AS

SET NOCOUNT ON

SELECT  a.AccountNumber ,
        a.AccountName ,
		a.AccountManagerFullName ,
		a.AccountManagerEmail ,
		a.BranchName ,
		a.BranchPrimaryBrand as [Customer Brand] ,
        SUM(TotalActualPrice) AS Sales ,
		SUM(TotalActualCost) AS Cost,
		SUM(TotalActualPrice) - SUM(TotalActualCost) AS GP,
		(SUM(TotalActualPrice) - SUM(TotalActualCost)) / NULLIF(SUM(TotalActualPrice), 0) AS GPP
FROM    lion.vwAccount a
        INNER JOIN lion.vwFactInvoiceLinePricebandScore i ON a.AccountKey = i.AccountKey
		inner join lion.vwFactLastPriceAndCost l on i.AccountKey = l.AccountKey and i.ItemKey = l.ItemKey
		and i.InvoiceLineGroup1Key = l.InvoiceLineGroup1Key
WHERE   ( [Total12MonthSales] > 0 )
            AND i.RPBPlaybookPricingGroupKey IS NOT NULL 
			AND a.BranchPrimaryBrand  IN ('PLUMB', 'PARTS')
GROUP BY a.AccountNumber ,
        a.AccountName ,
		a.AccountManagerFullName ,
		a.AccountManagerEmail ,
		a.BranchName ,
		a.BranchPrimaryBrand 
HAVING  SUM(TotalActualPrice) > 0
ORDER BY 7 DESC

GO
