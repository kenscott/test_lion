SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [cg].[GetEnvelopePlaybookPricingGroupKey]
	@ParamPyramidCode NVARCHAR(2),
	@ParamItemNumber Description_Small_type,
	@ParamMPGCode UDVarchar_type,
	@ParamLLSPGCode UDVarchar_type,
	@ParamVendorKey Key_Normal_type,
	@ParamJobFlag BIT,
	@ParamSales603010Bucket NVARCHAR(50),
	@ScenarioKey Key_Normal_type,
	@PlaybookDataPointGroupKey Key_Normal_type,
	@ParamPlaybookPricingGroupKey Key_Normal_type OUTPUT,
	@ParamDebug CHAR(1) = 'N'
AS

/*

DECLARE @TheOut Key_Normal_type

EXEC cg.GetEnvelopePlaybookPricingGroupKey
	'1',
	'1124688',
	'HDQH',
	'HD54', --llspg
	3514,
	1,
	'Sales Team',
	17,
	20,
	@TheOut OUTPUT,
	'y'

SELECT '@TheOut', @TheOut



DECLARE @TheOut Key_Normal_type

EXEC cg.GetEnvelopePlaybookPricingGroupKey
	'1',
	NULL,--'1124688AAA',
	'HDQH',
	'HD54', --llspg
	3514,
	1,
	'Sales Team',
	17,
	20,
	@TheOut OUTPUT,
	'y'



DECLARE @TheOut Key_Normal_type

EXEC cg.GetEnvelopePlaybookPricingGroupKey
	'1',
	NULL,--'1124688AAA',
	NULL, --'HDQH',
	'HD54', --llspg
	3514,
	1,
	'Sales Team',
	17,
	20,
	@TheOut OUTPUT,
	'y'



DECLARE @TheOut Key_Normal_type

EXEC cg.GetEnvelopePlaybookPricingGroupKey
	'1',
	'1F01991',
	'DATJ',
	'DA40', --llspg
	1223,
	0,
	'Sales Team',
	17,
	20,
	@TheOut OUTPUT,
	'y'


DECLARE @TheOut Key_Normal_type

EXEC cg.GetEnvelopePlaybookPricingGroupKey
	'1',
	null,--'1F01991',
	'DA26',
	'DA26', --llspg
	7554,
	0,
	'Sales Team',
	17,
	20,
	@TheOut OUTPUT,
	'y'

	DECLARE @TheOut Key_Normal_type

	EXEC cg.GetEnvelopePlaybookPricingGroupKey
		'1',
		null,--'1F01991',
		'DA37',
		'DA37', --llspg
		2451,
		0,
		null,
		17,
		20,
		@TheOut OUTPUT,
		'y'
*/

SET NOCOUNT ON


DECLARE
	@GroupingColumnsSearchStr NVARCHAR(MAX),
	@GroupValuesSearchStr NVARCHAR(MAX),
	@SQLString NVARCHAR(MAX),
	@PricingRuleSequenceNumber INT,
	@SectionNumber SMALLINT,
	@PricingRuleAttributeSequenceNumber INT,
	@PricingRuleAttributeType NVARCHAR(250),
	@PricingRuleAttributeSourceSchema NVARCHAR(256),
	@PricingRuleAttributeSourceTable NVARCHAR(250),
	@DefaultValueForMatch Description_Normal_type,
	@StrValue NVARCHAR(255)


SET @GroupValuesSearchStr = N''
SET @GroupingColumnsSearchStr = N''
SET @SQLString = N''


DECLARE PricingRuleSequenceCursor 
CURSOR 
FAST_FORWARD 
FOR
	SELECT 
		--TOP 1
		PricingRuleSequenceNumber,
		SectionNumber,
		PricingRuleAttributeSequenceNumber,
		PricingRuleAttributeType,
		PricingRuleAttributeSourceSchema,
		PricingRuleAttributeSourceTable,
		DefaultValueForMatch
	FROM dbo.ATKPricingRuleAttribute pra (NOLOCK)
	INNER JOIN dbo.ATKPricingRuleAttributeType prat  (NOLOCK)
		ON prat.PricingRuleAttributeTypeKey = pra.PricingRuleAttributeTypeKey
	INNER JOIN dbo.ATKPricingRule pr (NOLOCK)
		ON pr.PricingRuleKey = pra.PricingRuleKey
	INNER JOIN dbo.ATKScenario s (NOLOCK)
		ON s.PricingRulePlaybookKey = pr.PricingRulePlaybookKey
	WHERE ScenarioKey = @ScenarioKey
	ORDER BY
		pr.PricingRuleSequenceNumber,
		pra.PricingRuleAttributeSequenceNumber

OPEN PricingRuleSequenceCursor

FETCH NEXT FROM PricingRuleSequenceCursor 
INTO 	
	@PricingRuleSequenceNumber,
	@SectionNumber,
	@PricingRuleAttributeSequenceNumber,
	@PricingRuleAttributeType,
	@PricingRuleAttributeSourceSchema,
	@PricingRuleAttributeSourceTable,
	@DefaultValueForMatch


SET @ParamPlaybookPricingGroupKey = NULL 


WHILE (@@FETCH_STATUS = 0) AND (@ParamPlaybookPricingGroupKey IS NULL)
BEGIN

	IF ((@SectionNumber = 1 AND @ParamItemNumber IS NOT NULL) OR @SectionNumber <> 1) --or 1=1
	BEGIN

		/** override(s) and shortcuts for performance **/
		--  select * from dbo.ATKPricingRuleAttributeType order by 5,2,3
		SELECT @StrValue = 
			CASE 
				WHEN @PricingRuleAttributeSourceTable = N'cc.SupplierContract' AND @PricingRuleAttributeType = N'JobFlag' AND @ParamJobFlag IS NOT NULL THEN CAST(@ParamJobFlag AS NVARCHAR(1))
				WHEN @PricingRuleAttributeSourceTable = N'cc.SupplierContract' AND @PricingRuleAttributeType = N'PyramidCode' AND @ParamJobFlag IS NOT NULL THEN @ParamPyramidCode
				WHEN @PricingRuleAttributeSourceTable = N'cc.SupplierContract' AND @PricingRuleAttributeType = N'VendorKey' AND @ParamJobFlag IS NOT NULL THEN CAST(@ParamVendorKey AS NVARCHAR(25))
		
				WHEN @PricingRuleAttributeSourceSchema = N'cc' AND @PricingRuleAttributeSourceTable = N'SupplierContract' AND @PricingRuleAttributeType = N'JobFlag' AND @ParamJobFlag IS NOT NULL THEN CAST(@ParamJobFlag AS NVARCHAR(1))
				WHEN @PricingRuleAttributeSourceSchema = N'cc' AND @PricingRuleAttributeSourceTable = N'SupplierContract' AND @PricingRuleAttributeType = N'PyramidCode' AND @ParamJobFlag IS NOT NULL THEN @ParamPyramidCode
				WHEN @PricingRuleAttributeSourceSchema = N'cc' AND @PricingRuleAttributeSourceTable = N'SupplierContract' AND @PricingRuleAttributeType = N'VendorKey' AND @ParamJobFlag IS NOT NULL THEN CAST(@ParamVendorKey AS NVARCHAR(25))
		
				WHEN @PricingRuleAttributeSourceSchema = N'cg' AND @PricingRuleAttributeSourceTable = N'PlaybookDatapoint' AND @PricingRuleAttributeType = N'Sales603010Bucket' AND @ParamSales603010Bucket IS NOT NULL THEN @ParamSales603010Bucket

				WHEN @PricingRuleAttributeSourceSchema = N'cg' AND @PricingRuleAttributeSourceTable = N'PlaybookDatapoint' AND @PricingRuleAttributeType = N'ItemNumber' AND @ParamItemNumber IS NOT NULL THEN @ParamItemNumber
				WHEN @PricingRuleAttributeSourceSchema = N'cg' AND @PricingRuleAttributeSourceTable = N'PlaybookDatapoint' AND @PricingRuleAttributeType = N'MPGCode' AND @ParamMPGCode IS NOT NULL THEN @ParamMPGCode
				WHEN @PricingRuleAttributeSourceSchema = N'cg' AND @PricingRuleAttributeSourceTable = N'PlaybookDatapoint' AND @PricingRuleAttributeType = N'LLSPGCode' AND @ParamLLSPGCode IS NOT NULL THEN @ParamLLSPGCode

				WHEN @PricingRuleAttributeSourceSchema = N'cg' AND @PricingRuleAttributeSourceTable = N'PlaybookDatapoint' AND @PricingRuleAttributeType = N'InstructionLevelType' AND @SectionNumber = 1 THEN 'Item'
				WHEN @PricingRuleAttributeSourceSchema = N'cg' AND @PricingRuleAttributeSourceTable = N'PlaybookDatapoint' AND @PricingRuleAttributeType = N'InstructionLevelType' AND @SectionNumber <> 1 THEN 'LLSPG'

				ELSE N''
			END

			IF @StrValue = N''
			BEGIN
				SELECT
					@SQLString = N''

				-- something is bad/wrong if we get here
		
				SELECT 'ERROR - Unexpected playbook attribute encountered'   -- !!! do this select to cause the app to present an error (the raiserror does not seem to work)
			
				--SELECT @PricingRuleSequenceNumber
				RAISERROR ('ERROR - Unexpected playbook attribute encountered - PricingRuleSequenceNumber: %d, PricingRuleAttributeSequenceNumber: %d, PricingRuleAttributeType: %s, PricingRuleAttributeSourceTable: %s', 
					0, 1, @PricingRuleSequenceNumber, @PricingRuleAttributeSequenceNumber, @PricingRuleAttributeType, @PricingRuleAttributeSourceTable) WITH NOWAIT 
			END
	
			--IF @ParamDebug = 'Y'-- or 1=1
			--BEGIN
			--	SELECT '@SQLString: ', @SQLString
			--END	
	
		IF @PricingRuleAttributeSequenceNumber = 1 
		BEGIN
			SET @GroupValuesSearchStr = ISNULL(@StrValue, N'')				-- @StrValue could be null if there was no @SQLString build (as would be the case if this could received an attribute that it did not know how to handle)
			SET @GroupingColumnsSearchStr = @PricingRuleAttributeType
		END
		ELSE
		BEGIN 
			SET @GroupValuesSearchStr = @GroupValuesSearchStr + N',' + ISNULL(@StrValue, N'')				-- @StrValue could be null if there was no @SQLString build (as would be the case if this could received an attribute that it did not know how to handle)
			SET @GroupingColumnsSearchStr = @GroupingColumnsSearchStr + N',' + @PricingRuleAttributeType
		END

		--SELECT @GroupValuesSearchStr

	END

	FETCH NEXT FROM PricingRuleSequenceCursor INTO 	
		@PricingRuleSequenceNumber,
		@SectionNumber,
		@PricingRuleAttributeSequenceNumber,
		@PricingRuleAttributeType,
		@PricingRuleAttributeSourceSchema,
		@PricingRuleAttributeSourceTable,
		@DefaultValueForMatch

	IF @PricingRuleAttributeSequenceNumber = 1 AND @GroupValuesSearchStr <> '' -- just rolled over to a new rule; stop and search
	BEGIN
		
		IF @ParamDebug = 'Y'
		BEGIN 
			SELECT 'Searching for values: ', @GroupValuesSearchStr
			SELECT 'Searching for columns: ', @GroupingColumnsSearchStr
		END
		
		SELECT @ParamPlaybookPricingGroupKey = PlaybookPricingGroupKey
		FROM dbo.PlaybookPricingGroup (NOLOCK)
		WHERE 
			PlaybookDataPointGroupKey = @PlaybookDataPointGroupKey
			AND GroupValues = @GroupValuesSearchStr
		
		SET @GroupValuesSearchStr = N''
		SET @GroupingColumnsSearchStr = N''

	END

END

CLOSE PricingRuleSequenceCursor
DEALLOCATE PricingRuleSequenceCursor


-- search last one
IF @ParamPlaybookPricingGroupKey IS NULL AND @GroupValuesSearchStr <> '' -- just rolled over to a new rule; stop and search
BEGIN
	
	IF @ParamDebug = 'Y'
	BEGIN 
		SELECT 'Searching for values: ', @GroupValuesSearchStr
		SELECT 'Searching for columns: ', @GroupingColumnsSearchStr
	END
	
	SELECT @ParamPlaybookPricingGroupKey = PlaybookPricingGroupKey
	FROM dbo.PlaybookPricingGroup (NOLOCK)
	WHERE 
		PlaybookDataPointGroupKey = @PlaybookDataPointGroupKey
		AND GroupValues = @GroupValuesSearchStr
	
	--SET @GroupValuesSearchStr = ''
	--SET @GroupingColumnsSearchStr = ''

END

IF @ParamDebug = 'Y'
BEGIN 
	SELECT '@ParamPlaybookPricingGroupKey', @ParamPlaybookPricingGroupKey
END









GO
