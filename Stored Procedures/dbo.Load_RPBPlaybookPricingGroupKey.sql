SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




CREATE 
PROCEDURE [dbo].[Load_RPBPlaybookPricingGroupKey]
	@TableName VARCHAR(255),
	@TargetColumn VARCHAR(255),	
	@AlternateTargetColumn VARCHAR(255),	
	@WhereClause VARCHAR(MAX),
	@ParamDebug CHAR(1) = 'N'
AS

/*
EXEC dbo.Load_RPBPlaybookPricingGroupKey '', 'Y'

exec dcplog
*/

SET NOCOUNT ON

EXEC LogDCPEvent 'DCP --- dbo.Load_RPBPlaybookPricingGroupKey', 'B'


DECLARE
	@SQLString NVARCHAR(MAX),
	@ScenarioKey Key_Normal_type,
	@PlaybookDataPointGroupKey Key_Normal_type,
	@ProjectKey Key_Normal_type,
	@ProductLines VARCHAR(255)

IF ISNULL(LTRIM(RTRIM(@WhereClause)), '') <> ''
BEGIN
	
	SET @SQLString = '
		UPDATE ' + @TableName  + '
		SET
			' + @TargetColumn + ' = NULL
		FROM ' + @TableName  + '
		WHERE ' + LTRIM(RTRIM(@WhereClause))
	
END
ELSE
BEGIN

	SET @SQLString = '
		UPDATE ' + @TableName  + '
		SET
			' + @TargetColumn + ' = NULL '
END

--select @SQLString
EXECUTE sp_executesql @SQLString


EXEC dbo.GetRPBScenarioKeys @ScenarioKey OUTPUT, @PlaybookDataPointGroupKey OUTPUT, @ProjectKey OUTPUT

IF @ParamDebug = 'Y'
BEGIN 
	RAISERROR ('@ScenarioKey: %d', 0, 1, @ScenarioKey) WITH NOWAIT
	RAISERROR ('@PlaybookDataPointGroupKey: %d', 0, 1, @PlaybookDataPointGroupKey) WITH NOWAIT
END

EXEC dbo.Load_SetRPBPlaybookPricingGroupKey @TableName, @TargetColumn, @AlternateTargetColumn, @WhereClause, @ScenarioKey, @PlaybookDataPointGroupKey, @ParamDebug



EXEC LogDCPEvent 'DCP --- dbo.Load_RPBPlaybookPricingGroupKey', 'E'--, @RowsI, @RowsU, @RowsD



GO
