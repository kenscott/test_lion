SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE [dbo].[Load_ATKAccountGroup1ItemGroup1]
AS
/*
EXEC dbo.Load_ATKAccountGroup1ItemGroup1

EXEC dba_find_string 'ATKAccountGroup1ItemGroup1'

*/

SET NOCOUNT ON

EXEC LogDCPEvent 'DCP - Load_ATKAccountGroup1ItemGroup1', 'B'


CREATE TABLE #UpdateKeys (KeyID INT)

DECLARE 
	@RowsI Quantity_Normal_type, 
	@RowsU Quantity_Normal_type, 
	@RowsD Quantity_Normal_type,
	@TodayDayKey Key_Small_Type


SET @RowsI = 0
SET @RowsU = 0
SET @RowsD = 0

SET @TodayDayKey = (SELECT DayKey FROM DimDay WHERE CONVERT(VARCHAR(10), SQLDate, 120) = CONVERT(VARCHAR(10), GETDATE(), 120))


--DECLARE @UnknownAccountGroup1Key Description_Small_Type
--DECLARE @UnknownItemKey Description_Small_Type

--SET @UnknownAccountGroup1Key = (SELECT AccountGroup1Key FROM DimAccountGroup1 WHERE AG1Level1 = 'Unknown')
--SET @UnknownItemKey = (SELECT ItemKey FROM DimItem WHERE ItemClientUniqueIdentifier = 'Unknown')



----
--TRUNCATE TABLE dbo.ATKAccountGroup1ItemGroup1

--INSERT INTO dbo.ATKAccountGroup1ItemGroup1
--(
--	AccountGroup1Key,
--	ItemGroup1Key,
--	FloorPct,
--	TargetPct,
--	StretchPct
--)

--SELECT DISTINCT
--	fil.AccountGroup1Key,
--	fil.ItemGroup1Key,
--	0.25 AS [Floor Percentile],
--	0.50 AS [Target Percentile],
--	0.75 AS [Stretch Percentile]
--FROM dbo.FactInvoiceLine fil
--GROUP BY
--	fil.AccountGroup1Key,
--	fil.ItemGroup1Key
--UNION
--SELECT DISTINCT
--	da.AccountGroup1Key,
--	fil.ItemGroup1Key,
--	0.25 AS [Floor Percentile],
--	0.50 AS [Target Percentile],
--	0.75 AS [Stretch Percentile]
--FROM dbo.FactInvoiceLine fil
--INNER JOIN dbo.DimAccount da
--	ON da.AccountKey = fil.AccountKey
--GROUP BY
--	da.AccountGroup1Key,
--	fil.ItemGroup1Key
--ORDER BY 1,2,3
--SELECT @RowsI = @RowsI + @@ROWCOUNT

--DECLARE @loop AS INT,
--		@maxloop AS INT
--SET @loop = 0
--SET @maxloop = (SELECT MAX(SequenceNumber) FROM dbo.ATKPriceBandPercentile)

--WHILE (@loop <> @maxloop) 
--BEGIN 
--	SET @loop = @loop + 1

--UPDATE t1
--	SET FloorPct = t2.FloorPercentile / 100,
--	TargetPct = t2.TargetPercentile / 100,
--	StretchPct = t2.StretchPercentile / 100
--FROM dbo.ATKAccountGroup1ItemGroup1 t1 
--	INNER JOIN dbo.ATKPriceBandPercentile t2
--		ON 
--		(CASE WHEN t2.AccountGroup1Key = '1' THEN '1' ELSE t1.AccountGroup1Key END) = (CASE WHEN t2.AccountGroup1Key = '1' THEN '1' ELSE t2.AccountGroup1Key END)
--		AND 
--		(CASE WHEN t2.ItemGroup1Key IS NULL THEN '1' ELSE t1.ItemGroup1Key END) = (CASE WHEN t2.ItemGroup1Key IS NULL THEN '1' ELSE t2.ItemGroup1Key END)
--WHERE t2.SequenceNumber = @loop
--SELECT @RowsU = @RowsU + @@ROWCOUNT

--END


----SET @RowsI = @RowsI + @@RowCount

--EXEC LogDCPEvent 'DCP - Load_ATKAccountGroup1ItemGroup1', 'E', @RowsI, @RowsU, @RowsD


--SELECT TOP 10 * FROM dbo.ATKAccountGroup1ItemGroup1












SET QUOTED_IDENTIFIER OFF


GO
