SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE               PROCEDURE [dbo].[RBP_CalculatePricingGroupOptimals]
@PlaybookDataPointGroupKey INT
AS
SET QUOTED_IDENTIFIER ON 
SET ANSI_NULLS ON 
SET ARITHABORT ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
SET CONCAT_NULL_YIELDS_NULL ON
SET NUMERIC_ROUNDABORT OFF

DECLARE @ScenarioKey INT
DECLARE @RowsI Quantity_Normal_type, @RowsU Quantity_Normal_type, @RowsD Quantity_Normal_type, @PricingRuleWhereClause NVARCHAR(2000), @tmpValuesAssigned NVARCHAR(2000)
SET @RowsI = 0
SET @RowsU = 0
SET @RowsD = 0


DECLARE @ProjectKey INT
SELECT @ProjectKey = ProjectKey FROM PlaybookDataPointGroup WHERE PlaybookDataPointGroupKey = @PlaybookDataPointGroupKey
SELECT @ScenarioKey = ScenarioKey FROM PlaybookDataPointGroup_View WHERE PlaybookDataPointGroupKey = @PlaybookDataPointGroupKey

DECLARE @ApproachTypeKey INT
SELECT @ApproachTypeKey = ApproachTypeKey FROM ATKScenario WHERE ScenarioKey = @ScenarioKey

DECLARE @ApproachTypeDesc NVARCHAR(500)
SELECT @ApproachTypeDesc = ApproachTypeDesc FROM ATKApproachType WHERE ApproachTypeKey = @ApproachTypeKey

EXEC LogDCPEvent 'DCP --- RBP_CalculatePricingGroupOptimals', 'B',0,0,0,@ProjectKey


DELETE FROM ProposalCost WHERE PlaybookDataPointGroupKey = @PlaybookDataPointGroupKey
SET @RowsD = @RowsD + @@RowCount
DELETE FROM ProposalPrice WHERE PlaybookDataPointGroupKey = @PlaybookDataPointGroupKey
SET @RowsD = @RowsD + @@RowCount
DELETE FROM PlaybookPriceProposal WHERE PlaybookDataPointGroupKey = @PlaybookDataPointGroupKey
SET @RowsD = @RowsD + @@RowCount
DELETE FROM PlaybookDataPointOptimalPrice WHERE PlaybookDataPointOptimalPriceKey IN (SELECT PlaybookDataPointOptimalPriceKey FROM PlaybookDataPointOptimalPrice_View WHERE PlaybookDataPointGroupKey = @PlaybookDataPointGroupKey)
SET @RowsD = @RowsD + @@RowCount
DELETE FROM PlaybookPricingGroupOptimalPrice WHERE PlaybookPricingGroupOptimalPriceKey IN (SELECT PlaybookPricingGroupOptimalPriceKey FROM PlaybookPricingGroupOptimalPrice_View WHERE PlaybookDataPointGroupKey = @PlaybookDataPointGroupKey)
SET @RowsD = @RowsD + @@RowCount

--select * From PlaybookPricingGroupQuantityBand_View



INSERT INTO PlaybookPricingGroupOptimalPrice
(PricingBandOptimalPricePercentKey, 
PlaybookPricingGroupQuantityBandKey, 
OptimalPrice,
OptimalCost,
OptimalMargin)
SELECT 
OPP.PricingBandOptimalPricePercentKey, 
PlaybookPricingGroupQuantityBandKey,
CASE
	WHEN @ApproachTypeDesc = 'Reality Based Pricing' AND OPP.OptimalPriceMethodDesc = 'OptimalPriceByPrice'
		THEN dbo.fn_OptimalPriceByPrice (QB.PlaybookPricingGroupKey, QB.LowerBandQuantity, QB.UpperBandQuantity, OPP.OptimalPricePercent, P.UseAllDataPointsForOptimalIndicator)
	ELSE NULL
END AS 'OptimalPrice',
CASE
	WHEN @ApproachTypeDesc = 'Deviated Cost (Reality Based Costing)' 
		THEN dbo.fn_OptimalCostByCost (QB.PlaybookPricingGroupKey, QB.LowerBandQuantity, QB.UpperBandQuantity, OPP.OptimalPricePercent, P.UseAllDataPointsForOptimalIndicator)
	ELSE NULL
END AS 'OptimalCost',
CASE
	WHEN (@ApproachTypeDesc = 'Reality Based Pricing' OR @ApproachTypeDesc = 'Price Discovery' OR @ApproachTypeDesc = 'Recommended Price Band') AND OPP.OptimalPriceMethodDesc = 'OptimalPriceByMargin'
		THEN dbo.fn_OptimalMarginByMargin (QB.PlaybookPricingGroupKey, QB.LowerBandQuantity, QB.UpperBandQuantity, OPP.OptimalPricePercent, P.UseAllDataPointsForOptimalIndicator)
	ELSE NULL
END AS 'OptimalMargin'
FROM PlaybookPricingGroupQuantityBand/*_View*/ QB--PlaybookDataPointPricingGroupQuantityBand_View 
JOIN PlaybookPricingGroup PPG ON PPG.PlaybookPricingGroupKey = QB.PlaybookPricingGroupKey
JOIN ATKPricingRule P ON P.PricingRuleKey = PPG.PricingRuleKey 
JOIN ATKPricingBandOptimalPricePercent OPP ON
	OPP.PricingRuleBandKey = QB.PricingRuleBandKey
WHERE PPG.PlaybookDataPointGroupKey = @PlaybookDataPointGroupKey
-- 	and (
-- 			(@ApproachTypeDesc = 'Reality Based Pricing' and OPP.OptimalPriceMethodDesc in ('OptimalPriceByPrice'))	--Only use optimal Price percents (as opposed to ones used to calculate threshold values)
-- 			or
-- 			(@ApproachTypeDesc = 'Deviated Cost (Reality Based Costing)' and OPP.OptimalPriceMethodDesc in ('OptimalCostByCost'))	--Only use optimal Price percents (as opposed to ones used to calculate threshold values)
-- 		)

SET @RowsI = @RowsI + @@RowCount

DECLARE @USql NVARCHAR(MAX)
SET @USql = 'Update Statistics PlaybookPricingGroupOptimalPrice'
EXEC (@USql)


EXEC LogDCPEvent 'DCP --- RBP_CalculatePricingGroupOptimals', 'E', @RowsI, @RowsU, @RowsD 




GO
