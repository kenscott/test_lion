SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO






CREATE
PROCEDURE [lion].[P2P_RunProjectSpecification]
	@ProjectSpecificationKey INT,
	@OverwriteLastRun CHAR(1) = 'N',	--Set this to Y if the last run of the project spec should be overwritten
	@BeginStep INT = 1,
	@EndStep INT = 9999

AS

SET NOCOUNT ON
SET QUOTED_IDENTIFIER ON 
SET ANSI_NULLS ON 
SET ARITHABORT ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
SET CONCAT_NULL_YIELDS_NULL ON
SET NUMERIC_ROUNDABORT OFF
SET XACT_ABORT ON

DECLARE 
	@ProjectSpecificationScenarioKey Key_Normal_type, 
	@ProjectName NVARCHAR(500),
	@ProjectSpecificationName NVARCHAR(500),
	@PlaybookDataPointGroupKey Key_Normal_type,
	@ApproachTypeKey Key_Normal_type,
	@ScenarioKey Key_Normal_type, 
	@LimitDataPlaybookKey Key_Normal_type,
	@SegmentFactorPlaybookKey Key_Normal_type, 
	@PricingRulePlaybookKey Key_Normal_type, 
	@ProposalRecommendationPlaybookKey Key_Normal_type,
	@ProjectKey Key_Normal_type,
	@MaxInvoiceDateDayKey Key_Normal_type, 
	@MinInvoiceDateDayKey Key_Normal_type,
	@ProjectSpecificationDesc Description_Big_type
	--, @ScenarioUDVarchar1 Description_Small_type

	--Get the ScenarioKey we'll be using for this data point group
	SELECT @ScenarioKey = ScenarioKey FROM ATKProjectSpecificationScenario WHERE ProjectSpecificationKey = @ProjectSpecificationKey

	SET @ApproachTypeKey = (SELECT ApproachTypeKey FROM ATKScenario WHERE ScenarioKey = @ScenarioKey)


	-- The caller can now request that the most recent project, if any, with this specification type should be used, BUT ALTER  one if none exists.
	SET @ProjectKey = (SELECT MAX(ProjectKey) FROM PlaybookProject WHERE ProjectSpecificationKey = @ProjectSpecificationKey)
	IF @OverwriteLastRun = 'N' OR @ProjectKey IS NULL
	BEGIN
		--First create a project
		SELECT @ProjectSpecificationName =  (SELECT CalendarMonthInYear FROM DImMonth WHERE GETDATE() BETWEEN FirstSQLDate AND LastSQLDate) + '  ' + 
											+ (SELECT ProjectSpecificationName FROM ATKProjectSpecification WHERE ProjectSpecificationKey = @ProjectSpecificationKey)
											+ ':  Run #' + CAST((SELECT COUNT(*)+1 FROM PlaybookProject WHERE ProjectSpecificationKey = @ProjectSpecificationKey) AS NVARCHAR(200))
											+ '  CreationDate:'+CAST(GETDATE() AS NVARCHAR(100))



		-- Allow for future use of Project Names that differ from Specification Name, but default it to Specification Name for now.
		SET @ProjectName = @ProjectSpecificationName
		SELECT @ProjectSpecificationDesc = (SELECT ProjectSpecificationDescription FROM ATKProjectSpecification WHERE ProjectSpecificationKey = @ProjectSpecificationKey)
	
		SELECT	
			@MinInvoiceDateDayKey = MIN(InvoiceDateDayKey), 
			@MaxInvoiceDateDayKey = MAX(InvoiceDateDayKey)
		FROM dbo.FactInvoiceLine 
		WHERE Last12MonthsIndicator = 'Y'

		INSERT INTO PlaybookProject(ProjectSpecificationKey, ProjectName, ProjectDescription, MinInvoiceDateDayKey, MaxInvoiceDateDayKey, ApproachTypeKey)
		VALUES(@ProjectSpecificationKey, @ProjectName, @ProjectSpecificationDesc, @MinInvoiceDateDayKey, @MaxInvoiceDateDayKey, @ApproachTypeKey)
		SET @ProjectKey = @@Identity
	
		SELECT '@ProjectKey', @ProjectKey
	
-- 		Select @ProjectKey, MonthKey, PeriodNumber 
-- 		from DimMonth--, PlaybookProject
-- 		where MonthKey in (Select distinct MonthKey from DimDay where DayKey between @MinInvoiceDateDayKey and @MaxInvoiceDateDayKey)
	
		--Insert what the periodnumber for each month was at the time the project was created (since they roll each month)
		INSERT PlaybookProjectMonthPeriod (ProjectKey, InvoiceDateMonthKey, PeroidNumber)
		SELECT @ProjectKey, MonthKey, PeriodNumber 
		FROM DimMonth--, PlaybookProject
		WHERE MonthKey IN (SELECT DISTINCT MonthKey FROM DimDay WHERE DayKey BETWEEN @MinInvoiceDateDayKey AND @MaxInvoiceDateDayKey)
	
		--Then, create all of the data points groups for the project
		INSERT PlaybookDataPointGroup (ProjectSpecificationScenarioKey, ProjectKey/*, ScenarioKey*/)
		SELECT ProjectSpecificationScenarioKey, @ProjectKey--, ScenarioKey
		FROM ATKProjectSpecificationScenario
		WHERE ProjectSpecificationKey = @ProjectSpecificationKey
	
		--Assign all webroles to this project..  Later we can change this to only give access to analysts
		INSERT PlaybookProjectWebRole (ProjectKey, WebRoleKey)
		SELECT @ProjectKey, WebRoleKey
		FROM webrole
		WHERE webrolename IN ('P2PAnalyst', 'Administrator')
	END
	ELSE
	BEGIN
		SELECT	@MinInvoiceDateDayKey = MIN(InvoiceDateDayKey), 
				@MaxInvoiceDateDayKey = MAX(InvoiceDateDayKey)
		FROM dbo.FactInvoiceLine 
		WHERE Last12MonthsIndicator = 'Y'

		--This ensures that we're using the correct date range for the project
		UPDATE PlaybookProject 
		SET MinInvoiceDateDayKey = @MinInvoiceDateDayKey, 
			MaxInvoiceDateDayKey = @MaxInvoiceDateDayKey
		WHERE ProjectKey = @ProjectKey
		
		--Delete the old data
		DELETE FROM PlaybookProjectMonthPeriod WHERE ProjectKey = @ProjectKey
	
		--Insert what the periodnumber for each month was at the time the project was created (since they roll each month)
		INSERT PlaybookProjectMonthPeriod (ProjectKey, InvoiceDateMonthKey, PeroidNumber)
		SELECT @ProjectKey, MonthKey, PeriodNumber 
		FROM DimMonth--, PlaybookProject
		WHERE MonthKey IN (SELECT DISTINCT MonthKey FROM DimDay WHERE DayKey BETWEEN @MinInvoiceDateDayKey AND @MaxInvoiceDateDayKey)
	END



	EXEC dbo.LogDCPEvent 'DCP - P2P_RunProjectSpecification', 'B', 0, 0, 0, @ProjectKey
	
	

	--Loop through each playbook data point group one at a time. 
	DECLARE SK CURSOR FOR
	SELECT PlaybookDataPointGroupKey
	FROM PlaybookDataPointGroup A
	WHERE ProjectKey = @ProjectKey
	OPEN SK
	FETCH NEXT FROM SK INTO @PlaybookDataPointGroupKey
	
	WHILE @@Fetch_STatus = 0  --For each scenario in this project, create a RunKey for it
	BEGIN

		--Get the Playbooks for the scenario
		SELECT 
			@LimitDataPlaybookKey = LimitDataPlaybookKey,
			@SegmentFactorPlaybookKey = SegmentFactorPlaybookKey,
			@PricingRulePlaybookKey = PricingRulePlaybookKey,
			@ProposalRecommendationPlaybookKey = ProposalRecommendationPlaybookKey
			--, @ScenarioUDVarchar1 = UDVarchar1
		FROM ATKScenario WHERE ScenarioKey = @ScenarioKey
	
		IF (@SegmentFactorPlaybookKey IS NOT NULL)
		BEGIN
			--Fix the segment ranks (this should be done by the app)
			EXEC dbo.ATK_SetSegmentRanks @SegmentFactorPlaybookKey
		END
		
		IF @ApproachTypeKey IN (1, 4) --Margin Erosion
		BEGIN
			PRINT 'ME'
			--Exec dbo.ME_CreateMarginErosionProposal @ProjectKey, @PlaybookDataPointGroupKey, @LimitDataPlaybookKey, @PricingRulePlaybookKey, @ProposalRecommendationPlaybookKey, @BeginStep, @EndStep
			EXEC dbo.P2P_ME_PopulateProposal @ProjectKey, @PlaybookDataPointGroupKey, @LimitDataPlaybookKey, @PricingRulePlaybookKey, @ProposalRecommendationPlaybookKey, @BeginStep, @EndStep			
			EXEC dbo.P2P_ME_PopulateProjectSummary @ProjectKey
			
			IF @ApproachTypeKey = 1 -- CM a.k.a. Actional Margin Erosion (AcME)
			BEGIN 		
				EXEC dbo.P2P_PopulatePlaybookProjectMonthlySummary @ProjectKey
			END			
		END
		ELSE IF @ApproachTypeKey = 2 --Reality Based Pricing
		BEGIN
			PRINT 'RBP - Pricing'
			EXEC dbo.P2P_RBP @ProjectKey, @ScenarioKey, @PlaybookDataPointGroupKey, @LimitDataPlaybookKey, @SegmentFactorPlaybookKey, @PricingRulePlaybookKey, @ProposalRecommendationPlaybookKey, @BeginStep, @EndStep
			EXEC dbo.P2P_PopulatePlaybookProjectMonthlySummary @ProjectKey
		END
		ELSE IF @ApproachTypeKey = 3 --Reality Based Costing
		BEGIN
			PRINT 'RBP - Deviated Cost'
			EXEC dbo.P2P_RBP @ProjectKey, @ScenarioKey, @PlaybookDataPointGroupKey, @LimitDataPlaybookKey,	@SegmentFactorPlaybookKey, @PricingRulePlaybookKey, @ProposalRecommendationPlaybookKey, @BeginStep, @EndStep
			
			EXEC LogDCPEvent 'DCP --- P2P_RunProjectSpecification: Deviated Cost - Update ProductionProjectIndicator', 'B',0,0,0,@ProjectKey
			UPDATE PlaybookProject 
			SET ProductionProjectIndicator = 'Y' 
			WHERE ProjectKey = @ProjectKey
			EXEC LogDCPEvent 'DCP --- P2P_RunProjectSpecification: Deviated Cost - Update ProductionProjectIndicator', 'E',0,@@ROWCOUNT,0,@ProjectKey

		END
		ELSE IF @ApproachTypeKey = 5 --Price Discovery
		BEGIN
			PRINT 'Price Discovery: P2P_PriceDiscovery'
			IF @BeginStep <= 7
			BEGIN
				EXEC P2P_PriceDiscovery @ProjectKey, @ScenarioKey, @PlaybookDataPointGroupKey, @LimitDataPlaybookKey,	@SegmentFactorPlaybookKey, @PricingRulePlaybookKey, @ProposalRecommendationPlaybookKey, @BeginStep, @EndStep
			END

			IF @EndStep > 7  --Now, update the PD table
			BEGIN
				PRINT 'Price Discovery: RBP_PopulatePriceDiscovery'
				EXEC RBP_PopulatePriceDiscovery @ProjectKey
			END
		END
		ELSE IF @ApproachTypeKey = 6 --PROS Target Pricing
		BEGIN
			PRINT 'PROS Target Pricing'
			-- client must have allready had the segmentation loaded into DimAccountGroup4
			IF @BeginStep <= 3
			BEGIN
				EXEC dbo.P2P_PROS_CreateProposal @ProjectKey, @PlaybookDataPointGroupKey, @LimitDataPlaybookKey, @PricingRulePlaybookKey, @ProposalRecommendationPlaybookKey, @BeginStep, @EndStep

				IF @BeginStep <= 2 AND @EndStep >= 2
				BEGIN
					EXEC dbo.P2P_PopulatePlaybookProjectAccountStatus @ProjectKey, @PlaybookDataPointGroupKey
				END
			
				IF @BeginStep <= 3 AND @EndStep >= 3
				BEGIN
					EXEC dbo.P2P_PopulatePlaybookProjectAccountManagerStatus @ProjectKey, @PlaybookDataPointGroupKey
				END
			END
			IF @EndStep > 3
			BEGIN
				EXEC dbo.P2P_PROS_PopulateProjectSummary @ProjectKey
			END
		END
		ELSE IF @ApproachTypeKey = 7 --Recommended Price Band
		BEGIN
			PRINT 'Recommended Price Band'
			SELECT @EndStep = 
				CASE 
					WHEN @EndStep >= 9999 THEN 6  -- if default, the stop at step 6 (in other words, only do thru step 6 intentionally 
					ELSE @EndStep
				END
			
			/* save off any special attributes that need to be frozen (for lookup purposes */
			--UPDATE di
			--	SET
			--		PlaybookProductLine = ItemUDVarChar10,
			--		PlaybookClassCode = ItemUDVarChar11,
			--		PlaybookSubClassCode = ItemUDVarChar13,
			--		PlaybookVendorGrade = ItemUDVarChar15,
			--		PlaybookMaxCouponGradeCode = ItemUDVarChar17,
			--		PlaybookMaxCostGroupCode = ItemUDVarChar19,
			--		PlaybookVendorNumber = dv.VendorNumber
			--FROM dbo.DimItem di
			--LEFT JOIN dbo.DimVendor dv 
			--	ON dv.VendorKey = di.ItemVendorKey			

			-- Skip step 4, RBP_PopulateProjectSummary
			IF @EndStep >= 3
				EXEC dbo.P2P_RPB @ProjectKey, @ScenarioKey, @PlaybookDataPointGroupKey, @LimitDataPlaybookKey, @SegmentFactorPlaybookKey, @PricingRulePlaybookKey, @ProposalRecommendationPlaybookKey, @BeginStep, 3
			IF @EndStep >= 5
				EXEC dbo.P2P_RPB @ProjectKey, @ScenarioKey, @PlaybookDataPointGroupKey, @LimitDataPlaybookKey, @SegmentFactorPlaybookKey, @PricingRulePlaybookKey, @ProposalRecommendationPlaybookKey, 5, @EndStep
		END
		ELSE IF @ApproachTypeKey = 8 --Vendor Cost Change
		BEGIN
			PRINT 'Vendor Cost Management'
			
			EXEC dbo.P2P_VCM_PopulateProposal @ProjectKey, @ScenarioKey, @PlaybookDataPointGroupKey, @LimitDataPlaybookKey, @PricingRulePlaybookKey, @ProposalRecommendationPlaybookKey--, @BeginStep, @EndStep

			EXEC LogDCPEvent 'DCP --- P2P_RunProjectSpecification: VCM - Update ProductionProjectIndicator', 'B',0,0,0,@ProjectKey
			UPDATE PlaybookProject 
			SET ProductionProjectIndicator = 'Y' 
			WHERE ProjectKey = @ProjectKey
			EXEC LogDCPEvent 'DCP --- P2P_RunProjectSpecification: VCM - Update ProductionProjectIndicator', 'E',0,@@ROWCOUNT,0,@ProjectKey

		END
		ELSE IF @ApproachTypeKey = 11 --Rebate Change
		BEGIN
			PRINT 'Rebate Change'
			
			EXEC dbo.P2P_RC_PopulateProposal @ProjectKey, @ScenarioKey, @PlaybookDataPointGroupKey, @LimitDataPlaybookKey, @PricingRulePlaybookKey, @ProposalRecommendationPlaybookKey--, @BeginStep, @EndStep

		END
		ELSE IF @ApproachTypeKey = 12 --Cost Guidance
		BEGIN
			PRINT 'Cost Guidance'
			
			EXEC cg.P2P_CG @ProjectKey, @ScenarioKey, @PlaybookDataPointGroupKey, @LimitDataPlaybookKey, @SegmentFactorPlaybookKey, @PricingRulePlaybookKey, @ProposalRecommendationPlaybookKey, @BeginStep, @EndStep

		END
		FETCH NEXT FROM SK INTO @PlaybookDataPointGroupKey
		
	END
	CLOSE SK
	DEALLOCATE SK
	
	--new functionality to log a descriptive summary of the playbook to a seperate "archive" table
	DECLARE @datatype AS CHAR(1) 
	SELECT @datatype = 'Y' 

	CREATE TABLE #tempScenarioRules (TextData NVARCHAR(4000)) 

	INSERT INTO #tempScenarioRules
	EXEC clone_Scenario @ScenarioKey, @datatype 

	DELETE FROM playbookprojectscenariorules WHERE projectkey = @ProjectKey AND datatype = @datatype 
	
	INSERT INTO playbookprojectscenariorules (projectkey, scenariokey, textdata, datatype) 
		SELECT @ProjectKey, @ScenarioKey, TextData, @datatype FROM #tempScenarioRules
	EXEC dbo.LogDCPEvent 'DCP - P2P_RunProjectSpecification', 'E', 0, 0, 0, @ProjectKey	


















GO
