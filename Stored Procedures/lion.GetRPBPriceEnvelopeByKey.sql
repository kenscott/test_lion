SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [lion].[GetRPBPriceEnvelopeByKey]
	@ParamAccountKey Key_Normal_type,
	@ParamItemKey Key_Normal_type,
	@ParamInvoiceLineGroup1Key Key_Normal_type = 4,		-- shipment type key; default to stock

	@ParamPriceDerivationCode NVARCHAR(8),
	--@ParamDeviatedIndicator NVARCHAR(1) = 'N',		-- deviated indicator; default to No
	@ParamItemCost DECIMAL(38,3) = NULL,
	@ParamDebug CHAR(1) = 'N'

AS
/*



EXEC lion.GetRPBPriceEnvelope N'TB', N'7000B12', N'1', N'100195', N'ADJ', N'S', N'N', 787.351

EXEC lion.GetRPBPriceEnvelope N'TB', N'7000B12', N'1', N'[29066', N'ADJ', N'S', N'N', null, 'y'

EXEC lion.GetRPBPriceEnvelope N'TB', N'7168G72', N'1', N'519770', N'ADJ', N'S', N'N', 4.59, 'y'
EXEC lion.GetRPBPriceEnvelope N'TB', N'7121z18', N'1', N'f15270', N'tp', N'S', N'N', 786.24, 'y'

EXEC lion.GetRPBPriceEnvelope 'kk', '7123V22', '1', '508200', 'PGG', 'S', 'N', 0.10000000, 'y'

EXEC lion.GetRPBPriceEnvelope N'TB', N'7000B12', N'2', N'100195', N'ADJ', N'S', N'N', 787.351

EXEC lion.GetRPBPriceEnvelope N'TB', N'7000B12', N'1', N'100195', N'ADJ', NULL, NULL, 787.351
EXEC lion.GetRPBPriceEnvelope N'TB', N'7000B12', N'1', N'100195', N'ADJ', NULL, NULL, NULL
EXEC lion.GetRPBPriceEnvelope N'TB', N'7000B12', N'1', N'100195', N'ADJ'

EXEC lion.GetRPBPriceEnvelope 'BP', '7394N85', '1', 'K00279', NULL, 'S', 'N', NULL

EXEC lion.GetRPBPriceEnvelope 'BP.SB', '7123V22', '1', '508200', 'TP', 'S', 'N', 0.10

-- ADJ = Market = last
-- DP = Terms
EXEC lion.GetRPBPriceEnvelope 'BP', '7394N85', '1', 'K00279', 'ADJ', 'S', 'N', NULL --, 'Y'
EXEC lion.GetRPBPriceEnvelope 'BP', '7394N85', '1', 'K00279', 'DP', 'S', 'N', NULL --, 'Y'

EXEC lion.GetRPBPriceEnvelope 'BP', '7394N85', '1', 'K00279', 'zzz', 'S', 'N', NULL, 'Y'
EXEC lion.GetRPBPriceEnvelope 'BP', '7394N85', '1', 'K00279', NULL, 'S', 'N', NULL, 'Y'


EXEC lion.GetRPBPriceAttributes 67928	-- market
EXEC lion.GetRPBPriceAttributes 68119	-- terms


EXEC lion.GetRPBPriceEnvelope N'BR.JX',N'7798P47',N'1',N'172122',N'NDP',N'S',N'N',0.235

*/

SET NOCOUNT ON


DECLARE
	@AccountKey Key_Normal_type,
	@ItemKey Key_Normal_type,
	@InvoiceLineGroup1Key Key_Normal_type,
	--@InvoiceLineGroup2Key Key_Normal_type,
	--@LastInvoiceLineGroup2Key Key_Normal_type,
	@PricingConvFactor DEC(19,3),
	@SellingConvFactor DEC(19,3),
	@ParamItemCostConverted DEC(38,3),

	@PlaybookPricingGroupKey Key_Normal_type,
	@AlternatePlaybookPricingGroupKey Key_Normal_type,
	@AccountGroup1Key Key_Normal_type,

	@ScenarioKey Key_Normal_type,
	@PlaybookDataPointGroupKey Key_Normal_type,
	@ProjectKey Key_Normal_type,
	--@Total12MonthQuantity DEC(19,8),
	@FloorPercentile DEC(19,8),
	@TargetPercentile DEC(19,8),
	@StretchPercentile DEC(19,8),
	@Offset DEC(19,8),
	
	@FloorGPP DEC(19,8),
	@TargetGPP DEC(19,8),
	@StretchGPP DEC(19,8),
	@FloorPrice DEC(19,8),
	@TargetPrice DEC(19,8),
	@StretchPrice DEC(19,8),
	
	@Factor DEC(19,8),	
	
	
	--@MinPriceIncreaseThresholdPercent DECIMAL(19,8),
	--@BandSequenceNumber INT,
	
	@LastItemPrice DEC(19,3),
	@LastItemCost DEC(19,3),
	@LastSalesDate DATE,
		
	@RedPrice DEC(19,3),
	@AmberPrice DEC(19,3),
	@GreenPrice DEC(19,3),
	
	@RedGPP DEC(19,2),
	@AmberGPP DEC(19,2),
	@GreenGPP DEC(19,2),

	@TradePrice DEC(19,3),
	@AccountOwnerType UDVarchar_type,
	@ProductBrand UDVarchar_type,
	@Branch UDVarchar_type,
	@Region UDVarchar_type,
	@Area UDVarchar_type,
	@Network UDVarchar_type,
	@CustomerSegment UDVarchar_type,
	@CustomerRebateIndicator UDVarchar_type,
	@LLSPGCode UDVarchar_type,
	@ItemPyramidCode UDVarchar_type,
	@ItemNumber Description_Small_type,
	@ShipmentType UDVarchar_type,
	@DeviatedIndicator UDVarchar_type,
	@Sales603010Bucket UDVarchar_type,
	@StrategicIndicator UDVarchar_type,
	@CustomerType UDVarchar_type,
	
	@PriceApproach UDVarchar_type,
	@LastPriceApproach UDVarchar_type,
	
	@InvoiceLineGroup2Key Key_Normal_type


SET @Factor = 0.05		-- 5%

SET	@FloorGPP = NULL
SET	@TargetGPP = NULL
SET	@StretchGPP = NULL

SET @PlaybookPricingGroupKey = NULL


/*** Get All Needed Attributes ***/

SELECT 
	@AccountKey = da.AccountKey,
	@AccountGroup1Key = da.AccountGroup1Key,
	
	@AccountOwnerType = AccountUDVarChar8,
	@Branch = AG1Level1,
	@Area = AG1Level3,
	@Region = AG1Level4,
	@Network = AG1Level2,
	@CustomerSegment = AccountUDVarChar10,
	@CustomerRebateIndicator = AccountUDVarChar11,
	@CustomerType = ISNULL(AG2Level1, ''),

	-- set default, will pull the real value from last sales data if available
	@Sales603010Bucket = 
		CASE
			WHEN ISNULL(AG2Level1, '') = N'CS' THEN 'Small/Cash'
			WHEN ISNULL(AccountUDVarChar8, '') = N'National' THEN 'National'
			WHEN ISNULL(AccountUDVarChar8, '') = N'Sales Team' THEN 'Sales Team'
			ELSE 'Small/Cash'
		END,
	--@PriceApproach = 
	--	CASE
	--		WHEN ISNULL(AccountUDVarChar8, N'') = N'National' THEN N'National'
	--		ELSE NULL
	--	END

	-- Iteration 2 default...
	@PriceApproach = 
		CASE
			WHEN ISNULL(AG2Level1, N'') = N'CS' THEN N'Cash'
			WHEN ISNULL(AccountUDVarChar8, N'') = N'National' THEN N'National'
			WHEN ISNULL(AccountUDVarChar8, N'') = N'Sales Team' THEN N'LargeSalesTeam'
			ELSE N'SmallMedium'
		END
FROM dbo.DimAccount da (NOLOCK)
INNER JOIN dbo.DimAccountGroup1 dag1
	ON dag1.AccountGroup1Key = da.AccountGroup1Key
INNER JOIN dbo.DimAccountGroup2 dag2
	ON dag2.AccountGroup2Key = da.AccountGroup2Key
WHERE AccountKey = @ParamAccountKey

--IF @AccountKey IS NULL	-- look for parent account
--	SELECT 
--		@AccountKey = da.AccountKey,
--		@AccountGroup1Key = da.AccountGroup1Key,
		
--		@BranchBrand = AG1Level1UDVarchar6,
--		@PlaybookRegion = AG1Level1UDVarchar2,
--		@Branch = AG1Level1,
--		@CustomerSegment = AccountUDVarChar10,
--		@CustomerRebateIndicator = AccountUDVarChar11,
--		@Sales603010Bucket = -- set default, will pull the real value from last sales data if available
--			CASE
--				WHEN ISNULL(AG2Level1, '') = N'CS' THEN 'Cash Sales'
--				WHEN ISNULL(AccountUDVarChar8, '') = N'National' THEN 'National'
--				WHEN ISNULL(AccountUDVarChar8, '') = N'Sales Team' THEN 'Sales Team'
--				ELSE 'Low'
--			END
--	FROM dbo.DimAccount da (NOLOCK)
--	INNER JOIN dbo.DimAccountGroup1 dag1
--		ON dag1.AccountGroup1Key = da.AccountGroup1Key
--	INNER JOIN dbo.DimAccountGroup2 dag2
--		ON dag2.AccountGroup2Key = da.AccountGroup2Key
--	WHERE AccountNumberParent = @ParamAccountId
--		AND @AccountKey IS NULL

SELECT 
	@ItemKey = di.ItemKey,
	@ItemNumber = di.ItemNumber,
	@TradePrice	= ItemUDDecimal1,
	@LLSPGCode = IG3Level1,
	@ItemPyramidCode = IG3Level4,
	@ProductBrand = ItemUDVarChar2,
	@ParamItemCostConverted = 
		@ParamItemCost * (ISNULL(CostingConvFactor, 1)/ISNULL(PricingConvFact, 1)),
	@PricingConvFactor = ISNULL(PricingConvFact, 1),
	@SellingConvFactor = ISNULL(SellingConvFactor, 1)
FROM dbo.DimItem di (NOLOCK)
INNER JOIN dbo.DimItemGroup3 dig3
	ON dig3.ItemGroup3Key = di.ItemGroup3Key
LEFT JOIN dbo.ItemUOMConversion uc
	ON uc.ItemKey = di.ItemKey
WHERE di.ItemKey = @ParamItemKey

SELECT
	@InvoiceLineGroup1Key = InvoiceLineGroup1Key,	
	@ShipmentType = dilg1.InvoiceLineGroup1UDVarchar1,
	@DeviatedIndicator = dilg1.InvoiceLineGroup1UDVarchar2
FROM dbo.DimInvoiceLineGroup1 dilg1
WHERE InvoiceLineGroup1Key = @ParamInvoiceLineGroup1Key
--WHERE 
--	dilg1.InvoiceLineGroup1UDVarchar1 = ISNULL(@ParamShipmentType, N'S')
--	AND dilg1.InvoiceLineGroup1UDVarchar2 = ISNULL(@ParamDeviatedIndicator, N'N')


SELECT 
	@PlaybookPricingGroupKey = RPBPlaybookPricingGroupKey,
	@AlternatePlaybookPricingGroupKey = AlternateRPBPlaybookPricingGroupKey,
	@LastItemPrice = LastItemPrice,
	@LastItemCost = LastItemCost,
	@LastSalesDate = dd.SQLDate,
	@Sales603010Bucket = UDVarchar4,
	@StrategicIndicator = UDVarchar1,
	@LastPriceApproach = UDVarchar10
FROM dbo.FactLastPriceAndCost flpac (NOLOCK)
INNER JOIN dbo.DimDay dd
	ON dd.DayKey = flpac.LastSaleDayKey
WHERE 
	AccountKey = @AccountKey
	AND ItemKey = @ItemKey
	AND InvoiceLineGroup1Key = @InvoiceLineGroup1Key

-- need defaults if no last sale data
SELECT @StrategicIndicator = ISNULL(@StrategicIndicator, N'N')

-- need defaults if no last sale data
SELECT @PriceApproach = COALESCE(@LastPriceApproach, @PriceApproach, N'SmallMedium') -- vs. Cash, SmallMedium, LargeSalesTeam, National

SELECT
	@InvoiceLineGroup2Key = InvoiceLineGroup2Key	
	--,
	---- take out the following for iteration 2
	--@PriceApproach = 
	--	CASE
	--		WHEN @AccountOwnerType /* Owner Type */ = N'National' THEN 'National'
	--		ELSE InvoiceLineGroup2UDVarchar2 /* last derivation Bucket */
	--	END
FROM dbo.DimInvoiceLineGroup2 dilg2
WHERE 
	dilg2.InvoiceLineGroup2UDVarchar1 = @ParamPriceDerivationCode
		
		
/*** Check for Manual Price Band (MPB) ***/

SELECT
	@FloorGPP = MPBFloorGPP,
	@TargetGPP = MPBTargetGPP,
	@StretchGPP = MPBStretchGPP
FROM lion.fn_GetManualPriceBand (
		@ItemPyramidCode,
		@LLSPGCode,
		@ItemNumber,
		@PriceApproach,
		@DeviatedIndicator, -- = @ContractClaimsIndicator,
		@Region,
		@Area,
		@Network,
		@Branch
	)


/*** Get price band ***/
IF 
	(@FloorGPP IS NULL OR @TargetGPP IS NULL OR @StretchGPP IS NULL)	-- then no mpb
	AND (@PriceApproach IS NOT NULL)									-- and we have a valid Price Approach
BEGIN

	--IF @PriceApproach <> @LastPriceApproach	-- the last in history does not match the param; thus, try alternate
		--SET @PlaybookPricingGroupKey = @AlternatePlaybookPricingGroupKey		
	
	IF @PlaybookPricingGroupKey IS NULL				-- then no key was found in last data (during ETL); thus we must look up via attributes
		OR @LastPriceApproach IS NULL				-- no data in history so we will need to do a look up based on the param
	BEGIN

		EXEC dbo.GetRPBScenarioKeys @ScenarioKey OUTPUT, @PlaybookDataPointGroupKey OUTPUT, @ProjectKey OUTPUT

		--select @ScenarioKey , @PlaybookDataPointGroupKey , @ProjectKey, @PlaybookPricingGroupKey

		-- need to do some searching
		EXEC lion.GetRPBPriceEnvelopePlaybookPricingGroupKey
			@AccountKey, 
			@ItemKey, 
			@InvoiceLineGroup1Key, 
			@ScenarioKey,
			@PlaybookDataPointGroupKey,
			@PlaybookPricingGroupKey OUTPUT,
			@PriceApproach,
			@ParamDebug
	END

	IF @PlaybookPricingGroupKey IS NOT NULL
	BEGIN

		SELECT
			@FloorPercentile = FloorPercentile,
			@TargetPercentile = TargetPercentile,
			@StretchPercentile = StretchPercentile,
			@Offset = Offset
		FROM
			lion.fn_GetPercentile(
				@ProductBrand, 
				@Region,
				@Area,
				@Network,
				@Branch,
				@Sales603010Bucket, 
				@ItemPyramidCode, 
				@LLSPGCode) Pcts

		---- NOT SUPPORTED AT THIS TIME IS QUANTITY BANDS!!!
		---- get a 12 month quantity for use in determining the quantity band (SEE NOTE BELOW)
		--SET @Total12MonthQuantity = NULL
		--SELECT @Total12MonthQuantity = SUM(TotalQuantity)
		--FROM dbo.FactInvoiceLine (NOLOCK)
		--WHERE 
		--	Last12MonthsIndicator = 'Y'
		--	AND AccountKey = @AccountKey
		--	AND ItemKey = @ItemKey
		--	AND InvoiceLineGroup1Key = @InvoiceLineGroup1Key
		--/** if no value found then leave as null so that the first qty band will be selected (below) **/

		; WITH Datapoints AS (
			SELECT 
				PlaybookPricingGroupKey,
				PDPGPP AS GPP,
				COUNT(*) OVER () AS GroupCount,
				ROW_NUMBER() OVER (ORDER BY PDPGPP ASC) AS RowNumber
			FROM dbo.PlaybookPricingGroupPriceBand ppgpb (NOLOCK)
			WHERE ppgpb.PlaybookPricingGroupKey =  @PlaybookPricingGroupKey
				-- NOT SUPPORTED AT THIS TIME IS QUANTITY BANDS!!!
				
				--AND (
				--	(@BandSequenceNumber IS NOT NULL AND BandSequenceNumber = @BandSequenceNumber)
				--	OR (@BandSequenceNumber IS NULL AND @Total12MonthQuantity IS NOT NULL AND @Total12MonthQuantity BETWEEN ppgpb.LowerBandQuantity AND ppgpb.UpperBandQuantity)
				--	OR (@BandSequenceNumber IS NULL AND @Total12MonthQuantity IS NULL AND BandSequenceNumber = 1)
				--)

		), TheFloor AS (
			SELECT 
				GPP + @Offset AS GPP
			FROM Datapoints
			WHERE RowNumber = ROUND (GroupCount * @FloorPercentile, 0)
		), TheTarget AS (
			SELECT GPP + @Offset AS GPP
			FROM Datapoints
			WHERE RowNumber = ROUND (GroupCount * @TargetPercentile, 0)
		), TheStretch AS (
			SELECT GPP + @Offset AS GPP
			FROM Datapoints
			WHERE RowNumber = ROUND (GroupCount * @StretchPercentile, 0)
		)
		/* avoided using a temp table or table variable I did */
		SELECT
			@FloorGPP   = (SELECT GPP FROM TheFloor),
			@TargetGPP  = (SELECT GPP FROM TheTarget),
			@StretchGPP = (SELECT GPP FROM TheStretch)

	END
END


SELECT @RedGPP   = @FloorGPP   * 100.
SELECT @AmberGPP = @TargetGPP  * 100.
SELECT @GreenGPP = @StretchGPP * 100.


IF @ParamDebug = 'Y' 
BEGIN
	SELECT '@ParamItemCost', @ParamItemCost
	SELECT '@ParamItemCostConverted', @ParamItemCostConverted	
	SELECT '@ParamDebug', @ParamDebug

	SELECT '@AccountGroup1Key', @AccountGroup1Key
	SELECT '@AccountKey', @AccountKey
	SELECT '@ItemKey', @ItemKey
	SELECT '@InvoiceLineGroup1Key', @InvoiceLineGroup1Key
	
	SELECT '@ProductBrand', @ProductBrand
	SELECT '@Branch ', @Branch
	SELECT '@Area   ', @Area
	SELECT '@Region ', @Region
	SELECT '@Network', @Network
	SELECT '@CustomerSegment', @CustomerSegment
	SELECT '@Sales603010Bucket', @Sales603010Bucket
	SELECT '@CustomerRebateIndicator', @CustomerRebateIndicator
	SELECT '@ItemPyramidCode', @ItemPyramidCode
	SELECT '@LLSPGCode', @LLSPGCode
	SELECT '@StrategicIndicator', @StrategicIndicator
	SELECT '@ShipmentType', @ShipmentType
	SELECT '@DeviatedIndicator', @DeviatedIndicator
	
	SELECT '@PriceApproach', @PriceApproach
	SELECT '@LastPriceApproach', @LastPriceApproach

	SELECT '@FloorGPP', @FloorGPP
	SELECT '@TargetGPP', @TargetGPP
	SELECT '@StretchGPP', @StretchGPP
	
	SELECT '@LastItemPrice', @LastItemPrice
	SELECT '@LastSalesDate', @LastSalesDate
	
	SELECT '@PlaybookPricingGroupKey', @PlaybookPricingGroupKey
	
	SELECT '@TradePrice', @TradePrice
	
	SELECT '@RedGPP', @RedGPP
	SELECT '@AmberGPP', @AmberGPP
	SELECT '@GreenGPP', @GreenGPP

END


/* Determine prices if a cost was provided */
IF ISNULL(@ParamItemCostConverted, 0.) > 0. AND @FloorGPP IS NOT NULL
BEGIN


	--SELECT @FloorPrice   = @ParamItemCostConverted / (1. - @FloorGPP)
	--SELECT @TargetPrice = @ParamItemCostConverted / (1. - @TargetGPP)
	--SELECT @StretchPrice = @ParamItemCostConverted / (1. - @StretchGPP)

	--SELECT @FloorPrice = 
	--	CASE
	--		WHEN @TradePrice > 0. AND @FloorPrice >= (1. - @Factor) * @TradePrice THEN @TradePrice
	--		ELSE @FloorPrice
	--	END

	--SELECT @TargetPrice = 
	--	CASE
	--		WHEN @TradePrice > 0. AND @TargetPrice >= (1. - @Factor) * @TradePrice THEN @TradePrice
	--		ELSE @TargetPrice
	--	END
	--SELECT @StretchPrice = 
	--	CASE
	--		WHEN @TradePrice > 0. AND @StretchPrice >= (1. - @Factor) * @TradePrice THEN @TradePrice
	--		ELSE @StretchPrice
	--	END
	
	--/* round price to three digits */
	--SELECT @RedPrice   = ROUND(@FloorPrice, 3)
	--SELECT @AmberPrice = ROUND(@TargetPrice, 3)
	--SELECT @GreenPrice = ROUND(@StretchPrice, 3)

	--/* recalc gpp; truncate to 4 digits */
	--SELECT @RedGPP   = ROUND(((@RedPrice   - @ParamItemCostConverted) / @RedPrice)   , 4, 1) * 100.
	--SELECT @AmberGPP = ROUND(((@AmberPrice - @ParamItemCostConverted) / @AmberPrice) , 4, 1)	* 100.
	--SELECT @GreenGPP = ROUND(((@GreenPrice - @ParamItemCostConverted) / @GreenPrice) , 4, 1)	* 100.

	SELECT
		@RedPrice =   RedPrice,
		@AmberPrice = AmberPrice,
		@GreenPrice = GreenPrice
	FROM lion.fn_GetTradePriceMatchedRAGPrices (
		@TradePrice,
		@ParamItemCost, --@ParamItemCostConverted,
		@FloorGPP,
		@TargetGPP,
		@StretchGPP,
		@PricingConvFactor,
		@SellingConvFactor
	)

	/* recalc gpp; truncate to 4 digits */
	SELECT @RedGPP   = ROUND(((@RedPrice   - @ParamItemCostConverted) / @RedPrice)   , 4, 1) * 100.
	SELECT @AmberGPP = ROUND(((@AmberPrice - @ParamItemCostConverted) / @AmberPrice) , 4, 1)	* 100.
	SELECT @GreenGPP = ROUND(((@GreenPrice - @ParamItemCostConverted) / @GreenPrice) , 4, 1)	* 100.
	
END

SELECT
	@PlaybookPricingGroupKey AS PlaybookPricingGroupKey,
	CASE
		-- 2 is supposed to be for pyrmaid code but we decided not to check this outside of item number / product code
		-- WHEN @AccountGroup1Key IS NULL THEN 3							-- branch; this parameter is currently ignored
		WHEN @ItemKey IS NULL THEN 4										-- item/product
		WHEN @AccountKey IS NULL THEN 5										-- account/customer
		WHEN @ParamItemCostConverted IS NOT NULL AND @ParamItemCostConverted <= 0. THEN 6		-- bad cost supplied
		WHEN @InvoiceLineGroup2Key IS NULL THEN 7							-- bad price derivation code supplied
		WHEN @PlaybookPricingGroupKey IS NULL AND @TargetGPP IS NULL THEN 1
		ELSE 0
	END AS ReturnCode,
	
	NEWID() AS GUID,
	
	CASE
		WHEN @CustomerType = N'CS' OR DATEDIFF(MONTH, @LastSalesDate, GETDATE()) > 3 THEN NULL
		ELSE
			CAST(ROUND(
				CASE
					WHEN 
						ISNULL(@ParamItemCostConverted, 0.) > 0.
						AND ISNULL(@LastItemPrice, 0.) > 0.
						THEN @ParamItemCostConverted / NULLIF((1.0 -  ((@LastItemPrice-@LastItemCost)/@LastItemPrice)), 0.)
					ELSE NULL
				END,
				3) AS DECIMAL(19,3)) 
	END AS LastMarginPrice,
	CASE
		WHEN @CustomerType = N'CS' OR DATEDIFF(MONTH, @LastSalesDate, GETDATE()) > 3 THEN NULL
		ELSE @LastSalesDate 
	END AS LastSaleDate,
	
	@CustomerRebateIndicator AS CustomerRebate,
	
	@RedGPP AS RedGPP,
	@AmberGPP AS AmberGPP,
	@GreenGPP AS GreenGPP,

	@RedPrice AS RedPrice,
	@AmberPrice AS AmberPrice,
	@GreenPrice AS GreenPrice

GO
