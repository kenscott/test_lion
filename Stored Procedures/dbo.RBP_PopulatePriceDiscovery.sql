SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



--dbo.RBP_PopulatePriceDiscovery 6

--select * From dbo.PriceDiscoveryParamater
--select * From dbo.PriceDiscoveryParamaterValue
--select * from atkscenario_view


CREATE   PROCEDURE [dbo].[RBP_PopulatePriceDiscovery]
(@ProjectKey INT, @Execute CHAR(1) = 'Y')
AS

DECLARE @SQL NVARCHAR(MAX)
--Find the name of the custom user in this database
DECLARE @CustomUserName NVARCHAR(100)
SELECT @CustomUserName = dbo.fn_FindCustomUserName()

PRINT '@Execute: '+@Execute

IF @Execute = 'Y'
	EXEC LogDCPEvent 'DCP --- RBP_PopulatePriceDiscovery', 'B',0,0,0,@ProjectKey

--1st cursor
DECLARE 
@PricingRuleKey Key_Normal_type, 
@PricingRulePlaybookKey Key_Normal_type, 
@PricingRuleSequenceNumber Int_255_type, 
@MinimumPoints Int_Type, 
@MinimumPointsType Description_Normal_type, 
@RulingPointThreshold Int_Type, 
@PricingRuleWhereClause Description_Big_type,
@UseAllDataPointsForOptimalIndicator Description_Small_type, 
@PricingRuleJoinClause Description_Big_type

--2nd cursor
DECLARE 
@PricingRuleAttributeType Description_Normal_type, 
@PricingRuleAttributeJoinClause1 Description_Normal_type, 
@PricingRuleAttributeJoinClause2 Description_Normal_type,
@groupvaluecolumns NVARCHAR(MAX), 
@b NVARCHAR(MAX),
@c NVARCHAR(MAX),
@JoinStmt NVARCHAR(MAX),
@SQLStmt NVARCHAR(MAX)


--Think about adding:
--AccountMatrixnvarchar1 
--AccountMatrixnvarchar2
--AccountMatrixnvarchar3

--ItemMatrixnvarchar1 
--ItemMatrixnvarchar2
--ItemMatrixnvarchar3

--Price Discovery Work...
DECLARE @PlaybookDAtaPointGroupKey INT
SELECT @PlaybookDAtaPointGroupKey = PlaybookDAtaPointGroupKey
FROM PlaybookDAtaPointGroup
WHERE ProjectKey = @ProjectKey



SELECT @PricingRulePlaybookKey = B.PricingRulePlaybookKey 
FROM ATKScenario_View A
JOIN ATKScenario B ON A.ScenarioKey = B.ScenarioKey
WHERE LastProjectKey = @ProjectKey
--Need a table to store the possible combos in.  Not sure how to do this if it needs to be dynamic..
--Get a list of all combos for the 3 drop downs from price discovery
--SKU will always be one of them.  The others may be UD's or may be AGx's or IGx's.
--The PD table may need to store ALL of these and allow NULLs for ones that are not used or maybe store UNKNOWN for those??
--The project needs to store the drop down paramaters as a playbook rule

--Create table dbo.PriceDiscoveryParamater (PriceDiscoveryParamaterKey key_normal_type identity(1,1), DisplayName description_normal_type, ColumnName description_normal_type, TableName description_normal_type, ExcludeIfNullColumn description_normal_type)
--  Insert dbo.PriceDiscoveryParamater (ColumnName, DisplayName, TableName, ExcludeIfNullColumn) values ('ItemKey', 'Product (SKU)', 'DimItem', 'ItemUDDecimal1')
--  Insert dbo.PriceDiscoveryParamater (ColumnName, DisplayName, TableName, ExcludeIfNullColumn) values ('AccountUDVarchar2', 'Market' 'DimAccount', 'AccountUDVarchar2')
--  Insert dbo.PriceDiscoveryParamater (ColumnName, DisplayName, TableName, ExcludeIfNullColumn) values ('AccountUDVarchar1', 'Sales Segment', 'DimAccount', 'AccountUDVarchar1')
--Create unique clustered index UI_PriceDiscoveryParamater_1 on dbo.PriceDiscoveryParamater (ColumnName, TableName, ExcludeIfNullColumn)

--Create table dbo.PriceDiscoveryParamaterValue (PriceDiscoveryParamaterKey key_normal_type, Value description_normal_type)
--Create unique clustered index UI_PriceDiscoveryParamaterValue_1 on dbo.PriceDiscoveryParamaterValue (PriceDiscoveryParamaterKey, Value)



IF @Execute = 'Y'
BEGIN
	--Delete the existing values for drop downs
	TRUNCATE TABLE dbo.PriceDiscoveryParamaterValue
	
	----Drop the table & re-create using dynamic column names
	--SET @SQL = 'Drop table '+@CustomUserName+'.PriceDiscovery'
	--EXEC(@SQL)

	--SET @SQL = 'Create table '+@CustomUserName+'.PriceDiscovery (PriceDiscoveryKey int identity(1,1) NOT NULL, PlaybookPricingGroupKey Key_Normal_Type NOT NULL)'  	--The rest of the columns will be added later
	--EXEC(@SQL)	
	
		--Drop the table & re-create using dynamic column names
	SET @SQL = 'Drop table '+'dbo'+'.PriceDiscovery'
	EXEC(@SQL)

	SET @SQL = 'Create table '+'dbo'+'.PriceDiscovery (PriceDiscoveryKey int identity(1,1) NOT NULL, PlaybookPricingGroupKey Key_Normal_Type NOT NULL)'  	--The rest of the columns will be added later
	EXEC(@SQL)	
	
END
--
CREATE TABLE #AllCombos(TempVarRemoveLater TINYINT NOT NULL)

DECLARE @Main1 NVARCHAR(MAX), @Sub1 NVARCHAR(MAX), @SubValueJoinStmt NVARCHAR(MAX)


--Repopulate the values for drop downs
DECLARE @ColumnName description_normal_type, @TableName description_normal_type, @ExcludeIfNullColumn description_normal_type, @PriceDiscoveryParamaterKey key_normal_type, @SubTable1Name description_normal_type, @SubColumn1Name description_normal_type
DECLARE @ColList NVARCHAR(MAX) , @x INT, @SelectList NVARCHAR(MAX), @InsertColList NVARCHAR(MAX), @JoinFilterColList NVARCHAR(MAX), @FilterTestCol NVARCHAR(MAX)
SET @x = 1
SET @ColList = ''
SET @SelectList = ''
SET @SQLStmt = ''
SET @InsertColList = ''
SET @JoinFilterColList = ''
SET @FilterTestCol = ''
SET @SubValueJoinStmt = ''
DECLARE PDCombos CURSOR FOR
SELECT ColumnName, TableName, ExcludeIfNullColumn, PriceDiscoveryParamaterKey, SubTable1Name, SubColumn1Name

FROM dbo.PriceDiscoveryParamater
OPEN PDCombos
FETCH NEXT FROM PDCombos INTO @ColumnName, @TableName, @ExcludeIfNullColumn, @PriceDiscoveryParamaterKey, @SubTable1Name, @SubColumn1Name
WHILE @@fetch_status = 0
BEGIN
	SET @SubValueJoinStmt = ''

	IF @FilterTestCol = ''
		SET @FilterTestCol = 'C.'+@ColumnName

--Select Distinct 5,AccountUDVarchar1 From DimAccount where AccountUDVarchar1 is not null
	SET @Main1 = @TableName + '.' + @ColumnName

	IF @SubTable1Name = '' OR @SubColumn1Name = ''
	BEGIN
		SET @Sub1 = ''''''
	END
	ELSE
	BEGIN
		SET @Sub1 = @SubTable1Name + '.' + @SubColumn1Name
		IF @SubTable1Name <> @TableName 	--If they are from different tables, we need to add a join
			IF @SubTable1Name = 'DimAccountGroup1'
				SET @SubValueJoinStmt = 'Join DimAccountGroup1 on DimAccountGroup1.AccountGroup1Key = DimAccount.AccountGroup1Key'
			ELSE IF @SubTable1Name = 'DimAccountGroup2'
				SET @SubValueJoinStmt = 'Join DimAccountGroup2 on DimAccountGroup2.AccountGroup2Key = DimAccount.AccountGroup2Key'
			ELSE IF @SubTable1Name = 'DimAccountGroup3'
				SET @SubValueJoinStmt = 'Join DimAccountGroup3 on DimAccountGroup3.AccountGroup3Key = DimAccount.AccountGroup3Key'
			ELSE IF @SubTable1Name = 'DimItemGroup1'
				SET @SubValueJoinStmt = 'Join DimItemGroup1 on DimItemGroup1.ItemGroup1Key = DimItem.ItemGroup1Key'
			ELSE IF @SubTable1Name = 'DimItemGroup2'
				SET @SubValueJoinStmt = 'Join DimItemGroup2 on DimItemGroup2.ItemGroup2Key = DimItem.ItemGroup2Key'
			ELSE IF @SubTable1Name = 'DimItemGroup3'
				SET @SubValueJoinStmt = 'Join DimItemGroup3 on DimItemGroup3.ItemGroup3Key = DimItem.ItemGroup3Key'
	END

-- selecT *from dbo.PriceDiscoveryParamater
-- Select Distinct 5,AccountUDVarchar1, AccountUDVarchar4
-- From DimAccount where AccountUDVarchar1 is not null

	SET @ColList = @ColList  + @ColumnName + ','
	SET @InsertColList = @InsertColList  + 'A.'+@ColumnName + ','
	SET @JoinFilterColList = @JoinFilterColList  + 'A.'+@ColumnName + ' = C.'+@ColumnName+' and '
	SET @SelectList = @SelectList + '(Select Distinct '+@ColumnName+' From '+@TableName+' where '+@ExcludeIfNullColumn+' is not null) A'+CAST(@X AS NVARCHAR(50))+','+ CHAR(10)

	SET @SQLStmt = 'Insert dbo.PriceDiscoveryParamaterValue (PriceDiscoveryParamaterKey, Value, SubValue1)'
	SET @SQLStmt = @SQLStmt + CHAR(10)+ 'Select Distinct '+CAST(@PriceDiscoveryParamaterKey AS NVARCHAR(50))+','+@Main1 + ', ' + @Sub1 
	SET @SQLStmt = @SQLStmt + CHAR(10)+ ' From '+@TableName
	SET @SQLStmt = @SQLStmt + CHAR(10)+ @SubValueJoinStmt
	SET @SQLStmt = @SQLStmt + CHAR(10)+' where '+@ExcludeIfNullColumn+' is not null'
	PRINT @SQLStmt
	IF @Execute = 'Y'
		EXEC (@SQLStmt)

	SET @SQLStmt = 'Alter table #AllCombos add '+@ColumnName+' nvarchar(50)'
	PRINT @SQLStmt
	IF @Execute = 'Y'
		EXEC (@SQLStmt)

	SET @SQLStmt = 'Alter table '+'dbo'+'.PriceDiscovery add '+@ColumnName+' nvarchar(50) NOT NULL default '''''
	PRINT @SQLStmt
	IF @Execute = 'Y'
		EXEC (@SQLStmt)

	SET @x = @x + 1
	FETCH NEXT FROM PDCombos INTO @ColumnName, @TableName, @ExcludeIfNullColumn, @PriceDiscoveryParamaterKey, @SubTable1Name, @SubColumn1Name
END
CLOSE PDCombos
DEALLOCATE PDCombos
IF LEN(@ColList) > 0
	SET @ColList = LEFT(@ColList, LEN(@ColList)-1) --remove the last comma
IF LEN(@SelectList) > 0
	SET @SelectList = LEFT(@SelectList, LEN(@SelectList)-2) --remove the last comma & line feed
IF LEN(@JoinFilterColList) > 0
	SET @JoinFilterColList = LEFT(@JoinFilterColList, LEN(@JoinFilterColList)-4) --remove the last and
IF LEN(@InsertColList) > 0
	SET @InsertColList = LEFT(@InsertColList, LEN(@InsertColList)-1) --remove the last comma

IF @Execute = 'Y' AND LEN(@ColList) > 0
	ALTER TABLE #AllCombos DROP COLUMN TempVarRemoveLater

--Populate a table containing all combos of the drop down values
-- set @SQLStmt =  'Select '+@ColList+'
-- into #AllCombos
-- from
-- '+@SelectList

SET @SQLStmt = ''
IF LEN(@ColList) > 0
BEGIN
	SET @SQLStmt =  
	'Insert into #AllCombos ('+@ColList+')
	Select '+@ColList+'
	from
	'+@SelectList
END

PRINT @SQLStmt
IF @Execute = 'Y'
	EXEC (@SQLStmt)

/* OLD CODE REPLACED BY ABOVE CODE
SelecT ItemKey, AccountUDVarchar2, AccountUDVarchar1
into #AllCombos
from
(Select Distinct AccountUDVarchar2 From DimAccount where AccountUDVarchar2 is not null) A	--All markets (bucket 1-6)
,(Select Distinct AccountUDVarchar1 from DimAccount where AccountUDVarchar1 is not null) B  --All segments (a,b,c)
,(Select Distinct ItemKey from DimItem where ItemUDDecimal1 is not null) C  --All items that have a relevant cost (Current if one exists, Last if one exists, otherwise NULL)

Create clustered index I_AC_1 on #AllCombos(ItemKey)
Create index I_AC_2 on #AllCombos(ItemKey, AccountUDVarchar1, AccountUDVarchar2)
*/

--This is all the different pricing groups that are available to put things into
--These are discovered by the Price Discovery project
PRINT '@PlaybookDataPointGroupKey: '+ISNULL(CAST( @PlaybookDataPointGroupKey AS NVARCHAR(100)),'')

SELECT B.PricingRuleSequenceNumber, A.PlaybookPricingGroupKey, A.GroupingCOlumns, A.GroupValues, B.PricingRuleWhereClause, B.PricingRuleJoinClause
INTO #AllPricingGroups
FROM PlaybookPricingGroup A
JOIN ATKPRicingRule B ON A.PricingRUleKey = B.PricingRuleKey
WHERE PlaybookDataPointGroupKey = @PlaybookDataPointGroupKey
ORDER BY PricingRuleSequenceNumber
--Now, we need 1 rule for each of the 9 passes of the pricing playbook:

CREATE CLUSTERED INDEX I_APG_1 ON #AllPricingGroups (PricingRuleSequenceNumber, GroupValues)


--Now, we just fit each combo into its appropriate pricing group, based on the groups we already discovered in the RBP/Price Discovery project
--First, truncate existing data.  PD data has already been dropped (above)
IF @Execute = 'Y'
BEGIN
	TRUNCATE TABLE dbo.PriceDiscoveryProject


	INSERT INTO dbo.PriceDiscoveryProject (ProjectKey, PlaybookDAtaPointGroupKey) 
	VALUES ( @ProjectKey, @PlaybookDAtaPointGroupKey )
END

PRINT '@PricingRulePlaybookKey: '+ISNULL(CAST( @PricingRulePlaybookKey AS NVARCHAR(100)),'')

--This will generate the insert statements for the PD table
DECLARE PRs CURSOR FOR
SELECT PricingRuleKey, PricingRuleSequenceNumber, MinimumPoints, MinimumPointsType, RulingPointThreshold, PricingRuleWhereClause, UseAllDataPointsForOptimalIndicator, PricingRuleJoinClause
FROM dbo.ATKPricingRule
WHERE PricingRulePlaybookKey = @PricingRulePlaybookKey
ORDER BY PricingRuleSequenceNumber
OPEN PRs
FETCH NEXT FROM PRs INTO @PricingRuleKey, @PricingRuleSequenceNumber, @MinimumPoints, @MinimumPointsType, @RulingPointThreshold, @PricingRuleWhereClause, @UseAllDataPointsForOptimalIndicator, @PricingRuleJoinClause
WHILE @@fetch_status = 0
BEGIN
	SET @JoinStmt = ''

	--We need to convert the @PricingRuleWhereClause to usable values
	--Print 'Before:'+@PricingRuleWhereClause
	SET @PricingRuleWhereClause = REPLACE(@PricingRuleWhereClause, 'DimAccount.', 'A.')
	--Print 'After:'+@PricingRuleWhereClause

	SET @groupvaluecolumns = ''
	DECLARE PRATs CURSOR FOR
	SELECT PricingRuleAttributeType, PricingRuleAttributeJoinClause1, PricingRuleAttributeJoinClause2
	FROM ATKPricingRuleAttribute A1
	JOIN ATKPricingRuleAttributeType A2 ON A1.PricingRuleAttributeTypeKey = A2.PricingRuleAttributeTypeKey
	WHERE PricingRuleKey = @PRicingRuleKey
	OPEN PRATs
	FETCH NEXT FROM PRATs INTO @PricingRuleAttributeType, @PricingRuleAttributeJoinClause1, @PricingRuleAttributeJoinClause2
	WHILE @@fetch_status = 0
	BEGIN
		--SPecific rules for building the SQL:
		IF @PricingRuleAttributeType = 'ItemKey' SET @PricingRuleAttributeType = 'A.ItemKey'

		SET @groupvaluecolumns = @groupvaluecolumns + 'Cast('+@PricingRuleAttributeType+' as nvarchar(255)) + '','' + '
--		print @PricingRuleAttributeType
		--print @PricingRuleAttributeJoinClause1
		--print @PricingRuleAttributeJoinClause2

		PRINT '@groupvaluecolumns:'+ISNULL(@groupvaluecolumns, '')


		FETCH NEXT FROM PRATs INTO @PricingRuleAttributeType, @PricingRuleAttributeJoinClause1, @PricingRuleAttributeJoinClause2
	END
	CLOSE PRATs
	DEALLOCATE PRATs

	--Now, add and join statements we might need:
	--For now, I'm not sure how we'll know which ones we need so I'm going to add them as I see them come up.
	IF @groupvaluecolumns LIKE '%ItemKey%' OR @groupvaluecolumns LIKE '%ItemVendorKey%'
		SET @JoinStmt = 'join DimItem on DimItem.ItemKey = A.ItemKey'
-- 	if @groupvaluecolumns like '%VendorKey%'
-- 		set @JoinStmt = @JoinStmt + char(10) +  'join DimVendor on DimVendor.VendorKey = DimItem.ItemVendorKey'

	PRINT '@groupvaluecolumns:'+ISNULL(@groupvaluecolumns, '')

	--Strip off the extra comma at the end of this var:
	SET @groupvaluecolumns = LEFT(@groupvaluecolumns, LEN(@groupvaluecolumns)-8)


PRINT '--->'+CAST(@PricingRuleSequenceNumber AS NVARCHAR(10))
PRINT '--->'+CAST(@ProjectKey AS NVARCHAR(10))
PRINT '--->'+@ColList
PRINT '--->'+@InsertColList
PRINT '--->'+@JoinStmt
PRINT '--->'+@PricingRuleWhereClause
PRINT '--->'+@groupvaluecolumns
PRINT '--->'+CAST(@PricingRuleSequenceNumber AS NVARCHAR(10))
PRINT '--->'+@JoinFilterColList
PRINT '--->'+@FilterTestCol


	IF LEN(@ColList) > 0
	BEGIN
		--Print out the next rule that needs to run:
		PRINT '------------vvv-------Rule '+CAST(@PricingRuleSequenceNumber AS NVARCHAR(50))+'-------vvv------------'
		SET @SQLStmt = 'exec LogDCPEvent ''DCP --------- RBP_PopulatePriceDiscovery @PricingRuleSequenceNumber:'+CAST(@PricingRuleSequenceNumber AS NVARCHAR(50))+''',''B'',0,0,0,'+CAST(@ProjectKey AS NVARCHAR(50))

		SET @SQLStmt = @SQLStmt + CHAR(10) + 'Insert '+'dbo'+'.PriceDiscovery ('+@ColList+', PlaybookPricingGroupKey)'
		SET @SQLStmt = @SQLStmt + CHAR(10) + 'Select '+@InsertColList+', B.PlaybookPricingGroupKey'
		SET @SQLStmt = @SQLStmt + CHAR(10) + 'from #AllCombos A'
		SET @SQLStmt = @SQLStmt + CHAR(10) + @JoinStmt
		SET @SQLStmt = @SQLStmt + CHAR(10) + 'join #AllPricingGroups B on'
		SET @SQLStmt = @SQLStmt + CHAR(10) + '   ' + @PricingRuleWhereClause
		SET @SQLStmt = @SQLStmt + CHAR(10) + '    and ' + @groupvaluecolumns + ' = B.GroupValues'
		SET @SQLStmt = @SQLStmt + CHAR(10) + '    and B.PricingRuleSequenceNumber = '+CAST(@PricingRuleSequenceNumber AS NVARCHAR(50))+'--pass# '+CAST(@PricingRuleSequenceNumber AS NVARCHAR(50))+' only'
		SET @SQLStmt = @SQLStmt + CHAR(10) + 'left join '+'dbo'+'.PriceDiscovery C on '+@JoinFilterColList+'	--To filter out anything that has already been found by an earlier rule'
		SET @SQLStmt = @SQLStmt + CHAR(10) + 'where '+@FilterTestCol+' is NULL	--To filter out anything that has already been found by an earlier rule'

		SET @SQLStmt = @SQLStmt + CHAR(10) + 'exec LogDCPEvent ''DCP --------- RBP_PopulatePriceDiscovery @PricingRuleSequenceNumber:'+CAST(@PricingRuleSequenceNumber AS NVARCHAR(50))+''',''E'',@@RowCount,0,0,'+CAST(@ProjectKey AS NVARCHAR(50))


				
		PRINT @SQLStmt
		IF @Execute = 'Y'
			EXEC(@SQLStmt)

		PRINT '------------^^^--------------------^^^------------'
	END
	FETCH NEXT FROM PRs INTO @PricingRuleKey, @PricingRuleSequenceNumber, @MinimumPoints, @MinimumPointsType, @RulingPointThreshold, @PricingRuleWhereClause, @UseAllDataPointsForOptimalIndicator, @PricingRuleJoinClause
END
CLOSE PRs
DEALLOCATE PRs

IF LEN(@ColList) > 0
BEGIN
	--Now cluster the table so it's fast:
	SET @SQLStmt = 'Create unique clustered index I_PriceDiscovery_1 on '+'dbo'+'.PriceDiscovery('+@ColList+')'
	PRINT @SQLStmt
	IF @Execute = 'Y'
		EXEC (@SQLStmt)
END

--The count from each table should match if we have 100% coverage
SELECT COUNT(*) 'AllCombos' FROM #AllCombos

SET @SQL = 'Select Count(*) ''Price Discovery Found on'' from '+'dbo'+'.PriceDiscovery'
EXEC(@SQL)

DROP TABLE #AllCombos
DROP TABLE #AllPricingGroups

IF @Execute = 'Y'
	EXEC LogDCPEvent 'DCP --- RBP_PopulatePriceDiscovery', 'E',0,0,0,@ProjectKey



GO
