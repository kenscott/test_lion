SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [ppe].[PurgeRecommendTables]
WITH EXECUTE AS CALLER
AS

/*

https://enterbridge.atlassian.net/browse/LPF-2164 - Optimize Add New Requests for Large Recommendation Sets


EXEC ppe.PurgeRecommendTables

*/

SET NOCOUNT ON

EXEC LogDCPEvent 'ETL - ppe.PurgeRecommendTables', 'B'

DECLARE 
	@RowsI INT, 
	@RowsU INT, 
	@RowsD INT

SET @RowsI = 0
SET @RowsU = 0
SET @RowsD = 0

DELETE ro 
FROM ppe.Recommend r
JOIN ppe.RecommendOutput ro ON ro.RecommendKey = r.RecommendKey
WHERE DATEDIFF(DAY,r.CreatedOn,GETDATE()) > 30
SET @RowsD = @RowsD + @@RowCount


EXEC LogDCPEvent 'ETL - ppe.PurgeRecommendTables', 'E', @RowsI, @RowsU, @RowsD


GO
