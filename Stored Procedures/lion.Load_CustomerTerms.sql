SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [lion].[Load_CustomerTerms]
WITH EXECUTE AS CALLER
AS
/*

EXEC lion.Load_CustomerTerms

*/


SET ANSI_WARNINGS OFF;
SET ARITHABORT OFF;
SET ARITHIGNORE ON;

SET NOCOUNT ON;

EXEC LogDCPEvent 'ETL - Load_CustomerTerms', 'B';


DECLARE 
	@RowsI Quantity_Normal_type, 
	@RowsU Quantity_Normal_type, 
	@RowsD Quantity_Normal_type,
	@BeginRowCount INT,
	@TodayDayKey Key_Small_Type

SET @RowsI = 0;
SET @RowsU = 0;
SET @RowsD = 0;


SET @TodayDayKey = (SELECT DayKey FROM dbo.DimDay WHERE CONVERT(VARCHAR(10), SQLDate, 120) = CONVERT(VARCHAR(10), GETDATE(), 120));



/*** Per https://enterbridge.atlassian.net/browse/LP2P-1059, we no longer want to track in the history table changes to the four "last" columns ***/
EXEC LogDCPEvent 'ETL - Load_CustomerTerms: "Lasts"', 'B'
UPDATE Tgt
	SET
			Tgt.LastChangeInitials  = Src.[LastChange Initials],
			Tgt.LastChangeDate      = Src.[LastChange Date]    ,
			Tgt.LastChangeTime      = Src.[LastChange Time]    ,
			Tgt.LastChangePLID      = Src.[LastChangePLID]     ,
			Tgt.LastChangeBranch    = Src.[LastChange Branch]  ,
			Tgt.ModificationDayKey = @TodayDayKey
FROM Lion_Staging.dbo.CustomerTerms Src
INNER JOIN lion.CustomerTerms Tgt
	ON Tgt.AccountId = Src.AccountID
	AND Tgt.PyramidCode = Src.PyramidCode
	AND Tgt.SPGCode = Src.SPGCode
	AND Tgt.ExceptionProduct = Src.ExceptionProduct
WHERE 
	Tgt.LastChangeDate <> Src.[LastChange Date]
	OR Tgt.LastChangeTime <> Src.[LastChange Time]
	OR Tgt.LastChangePLID <> Src.[LastChangePLID]
	OR Tgt.LastChangeBranch <> Src.[LastChange Branch]
EXEC LogDCPEvent 'ETL - Load_CustomerTerms: "Lasts"', 'E', @RowsI, @RowsU, @RowsD;
SELECT @RowsU = @RowsU + @@ROWCOUNT;



SELECT @BeginRowCount = COUNT(*) FROM lion.CustomerTerms;

DECLARE @SummaryOfChanges TABLE (
	ChangeAction NVARCHAR(10) NULL,
	[CustomerTermsKey] [dbo].[Key_Normal_type] NULL,
	[AccountKey] [dbo].[Key_Normal_type] NULL DEFAULT ((1)),
	[ItemKey] [dbo].[Key_Normal_type] NULL DEFAULT ((1)),
	[ItemGroup3Key] [dbo].[Key_Normal_type] NULL DEFAULT ((1)),
	[AccountId] [NVARCHAR](255) NULL,
	[PyramidCode] [dbo].[UDVarchar_type] NULL,
	[SPGCode] [dbo].[UDVarchar_type] NULL,
	[OwningBrandCode] [dbo].[UDVarchar_type] NULL,
	[Usualdiscount1] [DECIMAL](38, 8) NULL,
	[Usualdiscount2] [DECIMAL](38, 8) NULL,
	[DeleteFlag] [dbo].[UDVarchar_type] NULL,
	[LastChangeInitials] [dbo].[UDVarchar_type] NULL,
	[LastChangeDate] [dbo].[UDVarchar_type] NULL,
	[LastChangeTime] [dbo].[UDVarchar_type] NULL,
	[LastChangePLID] [dbo].[UDVarchar_type] NULL,
	[LastChangeBranch] [dbo].[UDVarchar_type] NULL,
	[ExceptionProduct] [dbo].[UDVarchar_type] NULL,
	[ExceptionDiscount] [DECIMAL](38, 8) NULL,
	[ExceptionFixedPrice] [DECIMAL](38, 8) NULL,
	[ExceptionFromDate] [dbo].[UDVarchar_type] NULL,
	[ExceptionToDate] [dbo].[UDVarchar_type] NULL,
	[ExceptionPCF] [dbo].[UDVarchar_type] NULL,
	[FallbackMarginFlag] [BIT] NULL DEFAULT ((0)),
	[CreationDayKey] [dbo].[Key_Small_type] NULL DEFAULT ((1)),
	[ModificationDayKey] [dbo].[Key_Small_type] NULL DEFAULT ((1)),

	[CustomerTermsKey_Previous] [dbo].[Key_Normal_type] NULL,
	[AccountKey_Previous] [dbo].[Key_Normal_type] NULL DEFAULT ((1)),
	[ItemKey_Previous] [dbo].[Key_Normal_type] NULL DEFAULT ((1)),
	[ItemGroup3Key_Previous] [dbo].[Key_Normal_type] NULL DEFAULT ((1)),
	[AccountId_Previous] [NVARCHAR](255) NULL,
	[PyramidCode_Previous] [dbo].[UDVarchar_type] NULL,
	[SPGCode_Previous] [dbo].[UDVarchar_type] NULL,
	[OwningBrandCode_Previous] [dbo].[UDVarchar_type] NULL,
	[Usualdiscount1_Previous] [DECIMAL](38, 8) NULL,
	[Usualdiscount2_Previous] [DECIMAL](38, 8) NULL,
	[DeleteFlag_Previous] [dbo].[UDVarchar_type] NULL,
	[LastChangeInitials_Previous] [dbo].[UDVarchar_type] NULL,
	[LastChangeDate_Previous] [dbo].[UDVarchar_type] NULL,
	[LastChangeTime_Previous] [dbo].[UDVarchar_type] NULL,
	[LastChangePLID_Previous] [dbo].[UDVarchar_type] NULL,
	[LastChangeBranch_Previous] [dbo].[UDVarchar_type] NULL,
	[ExceptionProduct_Previous] [dbo].[UDVarchar_type] NULL,
	[ExceptionDiscount_Previous] [DECIMAL](38, 8) NULL,
	[ExceptionFixedPrice_Previous] [DECIMAL](38, 8) NULL,
	[ExceptionFromDate_Previous] [dbo].[UDVarchar_type] NULL,
	[ExceptionToDate_Previous] [dbo].[UDVarchar_type] NULL,
	[ExceptionPCF_Previous] [dbo].[UDVarchar_type] NULL,
	[FallbackMarginFlag_Previous] [BIT] NULL DEFAULT ((0)),
	[CreationDayKey_Previous] [dbo].[Key_Small_type] NULL DEFAULT ((1)),
	[ModificationDayKey_Previous] [dbo].[Key_Small_type] NULL DEFAULT ((1))
);

WITH ProductData AS (
	SELECT DISTINCT
		ISNULL(AccountKey, 1) AS AccountKey,
		di.ItemKey,
		di.ItemGroup3Key AS ItemGroup3Key,
		AccountId,
		CAST(CAST(ct.PyramidCode AS INT) AS NVARCHAR(2)) AS PyramidCode,
		SPGCode,
		OwningBrandCode,
		CASE
			WHEN LTRIM(RTRIM(Usualdiscount1)) = N'' OR Usualdiscount1 IS NULL THEN NULL
			WHEN SUBSTRING(Usualdiscount1, 1, 1) = N'+' THEN -1 * ABS(CAST(Usualdiscount1 AS DEC(38,8)))
			ELSE ABS(CAST(Usualdiscount1 AS DEC(38,8)))
		END AS Usualdiscount1,
		CASE
			WHEN LTRIM(RTRIM(Usualdiscount2)) = N'' OR Usualdiscount2 IS NULL THEN NULL
			WHEN SUBSTRING(Usualdiscount2, 1, 1) = N'+' THEN -1 * ABS(CAST(Usualdiscount2 AS DEC(38,8)))
			ELSE ABS(CAST(Usualdiscount2 AS DEC(38,8)))
		END AS Usualdiscount2,
		DeleteFlag,
		[LastChange Initials] AS LastChangeInitials,
		[LastChange Date] AS LastChangeDate,
		[LastChange Time] AS LastChangeTime,
		LastChangePLID,
		[LastChange Branch] AS LastChangeBranch,
		ExceptionProduct,
		CASE
			WHEN LTRIM(RTRIM(ExceptionDiscount)) = N'' OR ExceptionDiscount IS NULL THEN NULL
			WHEN SUBSTRING(ExceptionDiscount, 1, 1) = N'+' THEN -1 * ABS(CAST(ExceptionDiscount AS DEC(38,8)))
			ELSE ABS(CAST(ExceptionDiscount AS DEC(38,8)))
		END AS ExceptionDiscount,
		CAST(NULLIF([ExceptionFixed Price], N'') AS DEC(38,8)) AS ExceptionFixedPrice,
		[ExceptionFrom Date] AS ExceptionFromDate,
		ExceptionToDate,
		ExceptionPCF,
		CASE
			WHEN Usualdiscount1 = '-0' THEN 1
			ELSE 0
		END AS FallbackMarginFlag,
		ROW_NUMBER() OVER (PARTITION BY ISNULL(AccountKey, 1), di.ItemKey ORDER BY ct.[LastChange date] DESC, ct.CustomerTermsKey) AS RowRank
	FROM Lion_Staging.dbo.CustomerTerms ct
	INNER JOIN dbo.DimAccount da
		ON da.AccountNumber = ct.AccountID
	INNER JOIN DimItem di
		ON di.ItemNumber = CAST(CAST(ct.PyramidCode AS INT) AS NVARCHAR(2)) + ct.ExceptionProduct
	INNER JOIN DimItemGroup3 dig3
		ON dig3.ItemGroup3Key = di.ItemGroup3Key
	WHERE
		CAST(CAST(ct.PyramidCode AS INT) AS NVARCHAR(2)) = N'1'
		--AND OwningBrandCode = N'PLUMB'
		--AND NOT ( CAST(ISNULL([ExceptionFixed price], 0.) AS FLOAT) = 0. AND CAST(ISNULL(ExceptionDiscount, 0.) AS FLOAT) = 0. ) 
		AND NOT ( ISNUMERIC(ISNULL([ExceptionFixed price], '')) = 1 AND ISNUMERIC(ISNULL(ExceptionDiscount, '')) = 1 ) 
		AND dig3.IG3Level1 = ct.SPGCode									-- LP2P-504 Customer Terms - Don't Load Terms with Invalid SPG/Product Combinations 
),
SPGData AS (
	SELECT DISTINCT
		ISNULL(AccountKey, 1) AS AccountKey,
		1 AS ItemKey,
		ItemGroup3Key,
		AccountId,
		CAST(CAST(ct.PyramidCode AS INT) AS NVARCHAR(2)) AS PyramidCode,
		SPGCode,
		OwningBrandCode,
		CASE
			WHEN LTRIM(RTRIM(Usualdiscount1)) = N'' OR Usualdiscount1 IS NULL THEN NULL
			WHEN SUBSTRING(Usualdiscount1, 1, 1) = N'+' THEN -1 * ABS(CAST(Usualdiscount1 AS DEC(38,8)))
			ELSE ABS(CAST(Usualdiscount1 AS DEC(38,8)))
		END AS Usualdiscount1,
		CASE
			WHEN LTRIM(RTRIM(Usualdiscount2)) = N'' OR Usualdiscount2 IS NULL THEN NULL
			WHEN SUBSTRING(Usualdiscount2, 1, 1) = N'+' THEN -1 * ABS(CAST(Usualdiscount2 AS DEC(38,8)))
			ELSE ABS(CAST(Usualdiscount2 AS DEC(38,8)))
		END AS Usualdiscount2,
		DeleteFlag,
		[LastChange Initials] AS LastChangeInitials,
		[LastChange Date] AS LastChangeDate,
		[LastChange Time] AS LastChangeTime,
		LastChangePLID,
		[LastChange Branch] AS LastChangeBranch,
		ExceptionProduct,
		CASE
			WHEN LTRIM(RTRIM(ExceptionDiscount)) = N'' OR ExceptionDiscount IS NULL THEN NULL
			WHEN SUBSTRING(ExceptionDiscount, 1, 1) = N'+' THEN -1 * ABS(CAST(ExceptionDiscount AS DEC(38,8)))
			ELSE ABS(CAST(ExceptionDiscount AS DEC(38,8)))
		END AS ExceptionDiscount,
		CAST(NULLIF([ExceptionFixed Price], N'') AS DEC(38,8)) AS ExceptionFixedPrice,
		[ExceptionFrom Date] AS ExceptionFromDate,
		ExceptionToDate,
		ExceptionPCF,
		CASE
			WHEN Usualdiscount1 = '-0' THEN 1
			ELSE 0
		END AS FallbackMarginFlag,
		ROW_NUMBER() OVER (PARTITION BY ISNULL(AccountKey, 1), ItemGroup3Key ORDER BY ct.[LastChange date] DESC, ct.CustomerTermsKey) AS RowRank
	FROM Lion_Staging.dbo.CustomerTerms ct
	INNER JOIN dbo.DimAccount da
		ON da.AccountNumber = ct.AccountID
	INNER JOIN DimItemGroup3 dig3
		ON dig3.IG3Level4 = CAST(CAST(ct.PyramidCode AS INT) AS NVARCHAR(2))
		AND dig3.IG3Level1 = ct.SPGCode	
	WHERE
		CAST(CAST(ct.PyramidCode AS INT) AS NVARCHAR(2)) = N'1'
		--AND OwningBrandCode = N'PLUMB'
		AND ISNULL(ct.ExceptionProduct, N'') =  N''  -- only go the spg path if no item specified
		AND ISNULL([ExceptionFixed price], N'') = N''
		AND ISNULL(ExceptionDiscount, N'') = N''
)

MERGE lion.CustomerTerms AS Tgt
USING 
	(
	SELECT DISTINCT
		AccountKey,
		ItemKey,
		ItemGroup3Key,
		AccountId,
		PyramidCode,
		SPGCode,
		OwningBrandCode,
		Usualdiscount1,
		Usualdiscount2,
		DeleteFlag,
		LastChangeInitials,
		LastChangeDate,
		LastChangeTime,
		LastChangePLID,
		LastChangeBranch,
		ExceptionProduct,
		ExceptionDiscount,
		ExceptionFixedPrice,
		ExceptionFromDate,
		ExceptionToDate,
		ExceptionPCF,
		FallbackMarginFlag
	FROM ProductData
	WHERE
		RowRank = 1
	UNION
		SELECT DISTINCT
		AccountKey,
		ItemKey,
		ItemGroup3Key,
		AccountId,
		PyramidCode,
		SPGCode,
		OwningBrandCode,
		Usualdiscount1,
		Usualdiscount2,
		DeleteFlag,
		LastChangeInitials,
		LastChangeDate,
		LastChangeTime,
		LastChangePLID,
		LastChangeBranch,
		ExceptionProduct,
		ExceptionDiscount,
		ExceptionFixedPrice,
		ExceptionFromDate,
		ExceptionToDate,
		ExceptionPCF,
		FallbackMarginFlag
	FROM SPGData
	WHERE
		RowRank = 1
	) AS Src
ON Tgt.AccountKey = Src.AccountKey
AND Tgt.ItemKey = Src.ItemKey
AND Tgt.ItemGroup3Key = Src.ItemGroup3Key
WHEN MATCHED 
	AND (Tgt.AccountKey <> Src.AccountKey
	OR Tgt.ItemKey <> Src.ItemKey
	OR Tgt.ItemGroup3Key <> Src.ItemGroup3Key
	OR Tgt.AccountId <> Src.AccountId
	OR Tgt.PyramidCode <> Src.PyramidCode
	OR Tgt.SPGCode <> Src.SPGCode
	OR Tgt.OwningBrandCode <> Src.OwningBrandCode
	OR Tgt.Usualdiscount1 <> Src.Usualdiscount1
	OR Tgt.Usualdiscount2 <> Src.Usualdiscount2
	OR Tgt.DeleteFlag <> Src.DeleteFlag
	OR Tgt.LastChangeInitials <> Src.LastChangeInitials
	--OR Tgt.LastChangeDate <> Src.LastChangeDate --/* see update above, prior to merge statement */
	--OR Tgt.LastChangeTime <> Src.LastChangeTime
	--OR Tgt.LastChangePLID <> Src.LastChangePLID
	--OR Tgt.LastChangeBranch <> Src.LastChangeBranch
	OR Tgt.ExceptionProduct <> Src.ExceptionProduct
	OR Tgt.ExceptionDiscount <> Src.ExceptionDiscount
	OR Tgt.ExceptionFixedPrice <> Src.ExceptionFixedPrice
	OR Tgt.ExceptionFromDate <> Src.ExceptionFromDate
	OR Tgt.ExceptionToDate <> Src.ExceptionToDate
	OR Tgt.ExceptionPCF <> Src.ExceptionPCF
	OR Tgt.FallbackMarginFlag <> Src.FallbackMarginFlag
) THEN 
	UPDATE 
		SET
			Tgt.ModificationDayKey  = @TodayDayKey,
			Tgt.AccountId           = Src.AccountId           ,
			Tgt.PyramidCode         = Src.PyramidCode         ,
			Tgt.SPGCode             = Src.SPGCode             ,
			Tgt.OwningBrandCode     = Src.OwningBrandCode     ,
			Tgt.Usualdiscount1      = Src.Usualdiscount1      ,
			Tgt.Usualdiscount2      = Src.Usualdiscount2      ,
			Tgt.DeleteFlag          = Src.DeleteFlag          ,
			Tgt.LastChangeInitials  = Src.LastChangeInitials  ,
			Tgt.LastChangeDate      = Src.LastChangeDate      ,
			Tgt.LastChangeTime      = Src.LastChangeTime      ,
			Tgt.LastChangePLID      = Src.LastChangePLID      ,
			Tgt.LastChangeBranch    = Src.LastChangeBranch    ,
			Tgt.ExceptionProduct    = Src.ExceptionProduct    ,
			Tgt.ExceptionDiscount   = Src.ExceptionDiscount   ,
			Tgt.ExceptionFixedPrice = Src.ExceptionFixedPrice ,
			Tgt.ExceptionFromDate   = Src.ExceptionFromDate   ,
			Tgt.ExceptionToDate     = Src.ExceptionToDate     ,
			Tgt.ExceptionPCF        = Src.ExceptionPCF        ,
			Tgt.FallbackMarginFlag  = Src.FallbackMarginFlag  

WHEN NOT MATCHED BY TARGET THEN 
	INSERT (
		AccountKey,
		ItemKey,
		ItemGroup3Key,
		AccountId,
		PyramidCode,
		SPGCode,
		OwningBrandCode,
		Usualdiscount1,
		Usualdiscount2,
		DeleteFlag,
		LastChangeInitials,
		LastChangeDate,
		LastChangeTime,
		LastChangePLID,
		LastChangeBranch,
		ExceptionProduct,
		ExceptionDiscount,
		ExceptionFixedPrice,
		ExceptionFromDate,
		ExceptionToDate,
		ExceptionPCF,
		FallbackMarginFlag,
		ModificationDayKey,
		CreationDayKey
		)
	VALUES (
		Src.AccountKey,
		Src.ItemKey,
		Src.ItemGroup3Key,
		Src.AccountId,
		Src.PyramidCode,
		Src.SPGCode,
		Src.OwningBrandCode,
		Src.Usualdiscount1,
		Src.Usualdiscount2,
		Src.DeleteFlag,
		Src.LastChangeInitials,
		Src.LastChangeDate,
		Src.LastChangeTime,
		Src.LastChangePLID,
		Src.LastChangeBranch,
		Src.ExceptionProduct,
		Src.ExceptionDiscount,
		Src.ExceptionFixedPrice,
		Src.ExceptionFromDate,
		Src.ExceptionToDate,
		Src.ExceptionPCF,
		Src.FallbackMarginFlag,
		@TodayDayKey,
		@TodayDayKey
)
WHEN NOT MATCHED BY SOURCE THEN 
	DELETE

OUTPUT

	$ACTION AS ChangeAction, 
	INSERTED.CustomerTermsKey,
	INSERTED.AccountKey,
	INSERTED.ItemKey,
	INSERTED.ItemGroup3Key,
	INSERTED.AccountId,
	INSERTED.PyramidCode,
	INSERTED.SPGCode,
	INSERTED.OwningBrandCode,
	INSERTED.Usualdiscount1,
	INSERTED.Usualdiscount2,
	INSERTED.DeleteFlag,
	INSERTED.LastChangeInitials,
	INSERTED.LastChangeDate,
	INSERTED.LastChangeTime,
	INSERTED.LastChangePLID,
	INSERTED.LastChangeBranch,
	INSERTED.ExceptionProduct,
	INSERTED.ExceptionDiscount,
	INSERTED.ExceptionFixedPrice,
	INSERTED.ExceptionFromDate,
	INSERTED.ExceptionToDate,
	INSERTED.ExceptionPCF,
	INSERTED.FallbackMarginFlag,
	INSERTED.CreationDayKey,
	INSERTED.ModificationDayKey,

	DELETED.CustomerTermsKey	AS CustomerTermsKey_Previous,
	DELETED.AccountKey          AS AccountKey_Previous,
	DELETED.ItemKey				AS ItemKey_Previous,
	DELETED.ItemGroup3Key		AS ItemGroup3Key_Previous,
	DELETED.AccountId			AS AccountId_Previous,
	DELETED.PyramidCode			AS PyramidCode_Previous,
	DELETED.SPGCode				AS SPGCode_Previous,
	DELETED.OwningBrandCode		AS OwningBrandCode_Previous,
	DELETED.Usualdiscount1		AS Usualdiscount1_Previous,
	DELETED.Usualdiscount2		AS Usualdiscount2_Previous,
	DELETED.DeleteFlag			AS DeleteFlag_Previous,
	DELETED.LastChangeInitials	AS LastChangeInitials_Previous,
	DELETED.LastChangeDate		AS LastChangeDate_Previous,
	DELETED.LastChangeTime		AS LastChangeTime_Previous,
	DELETED.LastChangePLID		AS LastChangePLID_Previous,
	DELETED.LastChangeBranch	AS LastChangeBranch_Previous,
	DELETED.ExceptionProduct	AS ExceptionProduct_Previous,
	DELETED.ExceptionDiscount	AS ExceptionDiscount_Previous,
	DELETED.ExceptionFixedPrice	AS ExceptionFixedPrice_Previous,
	DELETED.ExceptionFromDate	AS ExceptionFromDate_Previous,
	DELETED.ExceptionToDate		AS ExceptionToDate_Previous,
	DELETED.ExceptionPCF		AS ExceptionPCF_Previous,
	DELETED.FallbackMarginFlag	AS FallbackMarginFlag_Previous,
	DELETED.CreationDayKey	    AS CreationDayKey_Previous,
	DELETED.ModificationDayKey	AS ModificationDayKey_Previous

INTO @SummaryOfChanges
;

/* This could me modified to keep x number of days of history. Right now, we are only keeping one. */
TRUNCATE TABLE lion.CustomerTermsPrevious;

INSERT lion.CustomerTermsPrevious (
	ChangeAction,
	CustomerTermsKey,
	AccountKey,
	ItemKey,
	ItemGroup3Key,
	AccountId,
	PyramidCode,
	SPGCode,
	OwningBrandCode,
	Usualdiscount1,
	Usualdiscount2,
	DeleteFlag,
	LastChangeInitials,
	LastChangeDate,
	LastChangeTime,
	LastChangePLID,
	LastChangeBranch,
	ExceptionProduct,
	ExceptionDiscount,
	ExceptionFixedPrice,
	ExceptionFromDate,
	ExceptionToDate,
	ExceptionPCF,
	FallbackMarginFlag,
	CreationDayKey,
	ModificationDayKey,
	CustomerTermsKey_Previous,
	AccountKey_Previous,
	ItemKey_Previous,
	ItemGroup3Key_Previous,
	AccountId_Previous,
	PyramidCode_Previous,
	SPGCode_Previous,
	OwningBrandCode_Previous,
	Usualdiscount1_Previous,
	Usualdiscount2_Previous,
	DeleteFlag_Previous,
	LastChangeInitials_Previous,
	LastChangeDate_Previous,
	LastChangeTime_Previous,
	LastChangePLID_Previous,
	LastChangeBranch_Previous,
	ExceptionProduct_Previous,
	ExceptionDiscount_Previous,
	ExceptionFixedPrice_Previous,
	ExceptionFromDate_Previous,
	ExceptionToDate_Previous,
	ExceptionPCF_Previous,
	FallbackMarginFlag_Previous,
	CreationDayKey_Previous,
	ModificationDayKey_Previous
)
SELECT
	ChangeAction,
	CustomerTermsKey,
	AccountKey,
	ItemKey,
	ItemGroup3Key,
	AccountId,
	PyramidCode,
	SPGCode,
	OwningBrandCode,
	Usualdiscount1,
	Usualdiscount2,
	DeleteFlag,
	LastChangeInitials,
	LastChangeDate,
	LastChangeTime,
	LastChangePLID,
	LastChangeBranch,
	ExceptionProduct,
	ExceptionDiscount,
	ExceptionFixedPrice,
	ExceptionFromDate,
	ExceptionToDate,
	ExceptionPCF,
	FallbackMarginFlag,
	CreationDayKey,
	ModificationDayKey,

	CustomerTermsKey_Previous,
	AccountKey_Previous,
	ItemKey_Previous,
	ItemGroup3Key_Previous,
	AccountId_Previous,
	PyramidCode_Previous,
	SPGCode_Previous,
	OwningBrandCode_Previous,
	Usualdiscount1_Previous,
	Usualdiscount2_Previous,
	DeleteFlag_Previous,
	LastChangeInitials_Previous,
	LastChangeDate_Previous,
	LastChangeTime_Previous,
	LastChangePLID_Previous,
	LastChangeBranch_Previous,
	ExceptionProduct_Previous,
	ExceptionDiscount_Previous,
	ExceptionFixedPrice_Previous,
	ExceptionFromDate_Previous,
	ExceptionToDate_Previous,
	ExceptionPCF_Previous,
	FallbackMarginFlag_Previous,
	CreationDayKey_Previous,
	ModificationDayKey_Previous
FROM 
	@SummaryOfChanges;



WITH TheCounts AS (
	SELECT
		ChangeAction,
		COUNT(*) AS ActionCount
	FROM @SummaryOfChanges
	GROUP BY ChangeAction
)
SELECT
	@RowsI = @RowsI + ISNULL((SELECT ActionCount FROM TheCounts WHERE ChangeAction = 'INSERT'), 0),
	@RowsU = @RowsU + ISNULL((SELECT ActionCount FROM TheCounts WHERE ChangeAction = 'UPDATE'), 0),
	@RowsD = @RowsD + ISNULL((SELECT ActionCount FROM TheCounts WHERE ChangeAction = 'DELETE'), 0)
;

SELECT TOP 10 * FROM lion.CustomerTerms;


EXEC LogDCPEvent 'ETL - Load_CustomerTerms', 'E', @RowsI, @RowsU, @RowsD;













GO
