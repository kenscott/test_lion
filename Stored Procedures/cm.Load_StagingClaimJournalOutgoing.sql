SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- Stored Procedure

CREATE PROCEDURE [cm].[Load_StagingClaimJournalOutgoing]

--EXEC [cm].[Load_StagingClaimJournalOutgoing]

AS

SET NOCOUNT ON

DECLARE
	@SQLString NVARCHAR(4000) = '',
	@ODSPrefix NVARCHAR(4000) = ''


SELECT 
	@ODSPrefix = ParamValue
FROM 
	dbo.[Param]
WHERE 
	ParamName = 'ClaimsManagerODSPrefix'


DECLARE @OutgoingRowData TABLE (
		ClaimJournalKey KEY_NORMAL_TYPE,
		CreationDate DATETIME,
		ClaimAction VARCHAR(20)
	)

DECLARE @BatchKeyTbl TABLE (
		BatchKey INT
		)

DECLARE @BatchKey INT

/*
 During the overnight process, these claim lines (claim line type = A (Adjustment) or W (Writeoff) and claim line status = P) 
 will be assigned a ClaimID and sent for immediate processing.
*/

--Create Invoice Claims
--InvoiceClaimKey is the InvoiceClaimId
INSERT INTO [cm].[InvoiceClaim]
           ([VendorKey]
           ,[ClaimMonth]
           ,[ContractId]
           ,[CreationDate]
           ,[CreatedByWebUserKey]
		   ,[ClaimType])
SELECT DISTINCT 
	cj.VendorKey,
	cj.ClaimMonth,
	cj.CurrentContractID,
	GETDATE(),
	1,
	'A'
FROM cm.ClaimJournal cj
INNER JOIN cm.SupplierClaim sc
	ON sc.ClaimMonth = cj.ClaimMonth
	AND sc.VendorKey = cj.VendorKey
WHERE ClaimType = 'A'
	--AND ClaimLineValue < 0
	AND ClaimJournalStatus = 'P'
ORDER BY cj.VendorKey, cj.ClaimMonth, cj.CurrentContractID

INSERT INTO [cm].[InvoiceClaim]
           ([VendorKey]
           ,[ClaimMonth]
           ,[ContractId]
           ,[CreationDate]
           ,[CreatedByWebUserKey]
		   ,[ClaimType])
SELECT DISTINCT 
	cj.VendorKey,
	cj.ClaimMonth,
	cj.CurrentContractID,
	GETDATE(),
	1,
	'W' 
FROM cm.ClaimJournal cj
INNER JOIN cm.SupplierClaim sc
	ON sc.ClaimMonth = cj.ClaimMonth
	AND sc.VendorKey = cj.VendorKey
WHERE ClaimType = 'W'
	--AND ClaimLineValue < 0
	AND ClaimJournalStatus = 'P'
ORDER BY cj.VendorKey, cj.ClaimMonth, cj.CurrentContractID

/*
--Set the Supplier Claim Status = Claimed
UPDATE sc
SET sc.ClaimStatus = 'C'
FROM cm.SupplierClaim sc
INNER JOIN cm.ClaimJournal cj
	ON cj.ClaimMonth = sc.ClaimMonth
	AND cj.VendorKey = sc.VendorKey
WHERE cj.ClaimType IN ('A', 'W')
	AND cj.ClaimLineValue < 0
	AND cj.ClaimJournalStatus = 'P'
*/

--Create Supplier Claim with Status = Claimed
DECLARE @NewSupplierClaimRowData TABLE (
		SupplierClaimKey KEY_NORMAL_TYPE,
		ClaimStatus varchar(1))

INSERT INTO [cm].[SupplierClaim]
           ([VendorKey]
           ,[ClaimMonth]
           ,[CreationDate]
           ,[CreatedByWebUserKey]
           ,[ClaimStatus])

	OUTPUT 
			INSERTED.SupplierClaimKey,
			INSERTED.ClaimStatus
			INTO @NewSupplierClaimRowData

	SELECT DISTINCT 
		cj.VendorKey,
		CAST(CONVERT(VARCHAR(4), DATEPART(yy, GETDATE())) + CONVERT(VARCHAR(2), DATEPART(mm, GETDATE())) AS INT), 
		GETDATE(),
		1,
		'C'
	FROM cm.ClaimJournal cj
	WHERE cj.ClaimType IN ('A', 'W')
		--AND cj.ClaimLineValue < 0
		AND cj.ClaimJournalStatus = 'P'

--Log new Supplier Claim
INSERT INTO cm.ClaimLog
	(
	[WebUserKey],
	[SupplierClaimKey],
	[Notes],
	[CreationDate],
	[ClaimAction]
	)
SELECT 
	1,
	d.SupplierClaimKey,
	'From: <blank> To: ' + s.SupplierClaimStatusDescription,
	GETDATE(),
	'Create Supplier Claim'
FROM @NewSupplierClaimRowData d
INNER JOIN cm.SupplierClaimStatus s
	ON s.SupplierClaimStatus = d.ClaimStatus

--Set Claim Keys and set status = Claimed in cm.ClaimJournal for matching with status = Pending
--LPF - 6269, added logging for updated claimsJournal

DECLARE @UpdateSupplierClaim TABLE (
		ClaimJournalKey KEY_NORMAL_TYPE,
		ClaimJournalStatus varchar(1),
		ClaimJournalStatusoriginal varchar(1)
		)

UPDATE cj
SET cj.SupplierClaimKey = c.SupplierClaimKey,
	cj.InvoiceClaimKey = ic.InvoiceClaimKey,
	cj.ClaimJournalStatus = 'C',
	cj.ADVBatchKey = NULL
--SELECT *
OUTPUT 
			INSERTED.ClaimJournalKey,
			INSERTED.ClaimJournalStatus,
			DELETED.ClaimJournalStatus
			INTO @UpdateSupplierClaim


FROM cm.ClaimJournal cj
INNER JOIN cm.SupplierClaim c
	ON c.ClaimMonth = cj.ClaimMonth
	AND c.VendorKey = cj.VendorKey
INNER JOIN @NewSupplierClaimRowData d
	ON d.SupplierClaimKey = c.SupplierClaimKey
LEFT JOIN cm.InvoiceClaim ic
	ON ic.ClaimMonth = cj.ClaimMonth
	AND ic.VendorKey = cj.VendorKey 
	AND ic.ContractId = cj.CurrentContractID
	AND ic.ClaimType = cj.ClaimType
WHERE cj.ClaimType IN ('A', 'W')
	--AND cj.ClaimLineValue < 0
	AND cj.ClaimJournalStatus = 'P'
	AND c.ClaimStatus = 'C' 
	--AND CAST(CONVERT(VARCHAR(4), DATEPART(yy, c.CreationDate)) + CONVERT(VARCHAR(2), DATEPART(mm, c.CreationDate)) + CONVERT(VARCHAR(2), DATEPART(dd, c.CreationDate)) AS INT) = CAST(CONVERT(VARCHAR(4), DATEPART(yy, GETDATE())) + CONVERT(VARCHAR(2), DATEPART(mm, GETDATE())) + CONVERT(VARCHAR(2), DATEPART(dd, GETDATE())) AS INT)
	--Supplier Claim that was created today

INSERT INTO cm.ClaimJournalLog
(
    WebUserKey,
    ClaimJournalKey,
    Notes,
    CreationDate,
    ClaimAction
)

SELECT 
	1,
	u.ClaimJournalKey,
	'Status Changed from P to C',
	GETDATE(),
	'Process Supplier Claim'
FROM cm.ClaimJournal cj 
INNER JOIN @UpdateSupplierClaim u
	ON u.ClaimJournalKey = cj.ClaimJournalKey
INNER JOIN cm.SupplierClaim c
	ON c.ClaimMonth = cj.ClaimMonth
	AND c.VendorKey = cj.VendorKey
INNER JOIN @NewSupplierClaimRowData d
	ON d.SupplierClaimKey = c.SupplierClaimKey
LEFT JOIN cm.InvoiceClaim ic
	ON ic.ClaimMonth = cj.ClaimMonth
	AND ic.VendorKey = cj.VendorKey 
	AND ic.ContractId = cj.CurrentContractID
	AND ic.ClaimType = cj.ClaimType
WHERE cj.ClaimType IN ('A', 'W')
	AND c.ClaimStatus = 'C' 
--End Logging	








INSERT INTO [cm].[StagingClaimJournalOutgoing]
           ([BatchID]
           ,[ODSClaimJournalKey]
           ,[ADVClaimJournalKey]
           ,[ClaimMonth]
           ,[TransactionID]
           ,[PyramidCode]
           ,[SupplierID]
           ,[SupplierName]
           ,[POSContractID]
           ,[ARPContractID]
		   ,[ClaimType]
           ,[ClaimLineValue]
           ,[ClaimLineMargin]
           ,[Status]
           ,[StatusDate]
           ,[ARPResult]
           ,[ARPResultDate]
           ,[ARPReason]
           ,[ADVRecommendation]
           ,[ADVRecommendedUser]
           ,[ADVRecommendedDate]
		   ,[ADVRejectReason]
		   ,[ADVAdjustmentReason]
		   ,[ADVComment]
           ,[ClaimID]
		   ,[CostCenter]
		   ,[OriginalClaimId]
		   ,[ADVAdjustmentReasonCode]
		   )

OUTPUT 
			INSERTED.ADVClaimJournalKey,
			GETDATE(),
			'Moved to outgoing'
			INTO @OutgoingRowData

SELECT
		[ADVBatchKey],
		[ODSClaimJournalKey],
		[ClaimJournalKey],
		[ClaimMonth],
		[ClientInvoiceLineUniqueID],
		[PyramidCode],
		[VendorNumber],
		[VendorDescription],
		[OriginalContractID],
		[CurrentContractID],
		[ClaimType],
		[ClaimLineValue],
		[TradingMargin],
		[ClaimJournalStatus],
		[ClaimJournalStatusDate],
		[ARPResult],
		[ARPResultDate],
		[ARPReason],
		[ADVRecommendation],
		wu.UserName,
		[ADVRecommendationDate],
		LEFT(rr.RejectionReasonValue, 2000),
		LEFT(ar.AdjustmentReasonValue, 2000),
		LEFT([Comment], 2000),
		[InvoiceClaimKey],
		ar.CostCenter,
		cj.OriginalInvoiceClaimKey,
		LEFT(ar.AdjustmentReasonCode, 5) 


FROM [cm].[ClaimJournal] cj
LEFT JOIN dbo.WebUser wu
	ON wu.WebUserKey = cj.ADVRecommendationWebUserKey
LEFT JOIN cm.RejectionReason rr
	ON rr.RejectionReasonKey = cj.RejectionReasonKey
LEFT JOIN cm.AdjustmentReason ar
	ON ar.AdjustmentReasonKey = cj.AdjustmentReasonKey

WHERE cj.ADVBatchKey IS NULL


--Log outgoing records
INSERT INTO cm.ClaimJournalLog (ClaimJournalKey, CreationDate, ClaimAction)
SELECT 
	ClaimJournalKey,
	CreationDate,
	ClaimAction
FROM @OutgoingRowData

--Set ADVBatchKey
INSERT INTO [lion].[Batch]
           ([CreationDate]
           ,[SourceKey])
	OUTPUT INSERTED.BatchKey
	INTO @BatchKeyTbl 
     VALUES
			(GETDATE(),
			12)

SET @BatchKey = (SELECT t.BatchKey 
FROM @BatchKeyTbl t)

UPDATE cj
SET [ADVBatchKey] = @BatchKey
FROM [cm].[ClaimJournal] cj
INNER JOIN @OutgoingRowData o
	ON o.ClaimJournalKey = cj.ClaimJournalKey

UPDATE cjo
SET BatchID = @BatchKey
FROM [cm].[StagingClaimJournalOutgoing] cjo
INNER JOIN @OutgoingRowData o
	ON o.ClaimJournalKey = cjo.ADVClaimJournalKey
WHERE cjo.BatchID IS NULL

--Clear ODS table for this batch
--SET REMOTE_PROC_TRANSACTIONS ON
--BEGIN DISTRIBUTED TRAN

	SELECT @SQLString = '
	DELETE FROM ' + @ODSPrefix + '..ODS_DATA_IN.ODS_CONTRACT_CLAIMS_JOURNAL
	WHERE BATCH_ID = @BatchKey

	INSERT INTO ' + @ODSPrefix + '..ODS_DATA_IN.ODS_CONTRACT_CLAIMS_JOURNAL
				([BATCH_ID]
			   ,[ODS_CC_SK]
			   ,[ADV_SK]
			   ,[YRMTH]
			   ,[SRC_TXN_SK]
			   ,[PYRAMID_CODE]
			   ,[SUPPLIER_CODE]
			   ,[SUPPLIER_NAME]
			   ,[POS_CONTRACT_ID]
			   ,[ARP_CONTRACT_ID]
			   ,[CLAIM_TYPE]
			   ,[VALUE]
			   ,[MARGIN]
			   ,[STATUS]
			   ,[STATUS_DATE]
			   ,[ARP_RESULT]
			   ,[ARP_RESULT_DATE]
			   ,[ARP_REASON]
			   ,[ADV_RECOMMENDATION]
			   ,[ADV_RECOMMENDED_USER]
			   ,[ADV_RECOMMENDED_DATE]
			   ,[ADV_REJECT_REASON]
			   ,[ADV_ADJUST_REASON_VALUE]
			   ,[ADV_COMMENT]
			   ,[CLAIM_ID]
			   ,[COST_CENTRE]
			   ,[ORIGINAL_CLAIM_ID]
		       ,[ADV_ADJUST_REASON_CODE]
		       )
	SELECT
		   [BatchID]
		  ,[ODSClaimJournalKey]
		  ,[ADVClaimJournalKey]
		  ,[ClaimMonth]
		  ,[TransactionID]
		  ,[PyramidCode]
		  ,[SupplierID]
		  ,[SupplierName]
		  ,[POSContractID]
		  ,[ARPContractID]
		  ,[ClaimType]
		  ,[ClaimLineValue]
		  ,[ClaimLineMargin]
		  ,[Status]
		  ,[StatusDate]
		  ,[ARPResult]
		  ,[ARPResultDate]
		  ,[ARPReason]
		  ,[ADVRecommendation]
		  ,[ADVRecommendedUser]
		  ,[ADVRecommendedDate]
		  ,[ADVRejectReason]
		  ,[ADVAdjustmentReason]
		  ,[ADVComment]
		  ,[ClaimID]
		  ,[CostCenter]
		  ,[OriginalClaimId]
	      ,[ADVAdjustmentReasonCode]

	FROM cm.StagingClaimJournalOutgoing
	WHERE BatchID = @BatchKey
	'

	EXECUTE sp_executesql @SQLString, N'@BatchKey INT', @BatchKey

	--CALL ODS procedure to collect and process outgoing claims journals
	DECLARE @batchid integer
	DECLARE @vol integer

	SET @batchid = @BatchKey
	SET @vol = (SELECT COUNT(*) FROM [cm].[StagingClaimJournalOutgoing] WHERE BatchID = @BatchKey)

	--EXECUTE ('BEGIN ODS_API.adv_to_ods_notify.req_adv_to_ods_jnl_process(?,?); END;', @batchid,@vol) AT ODSD

	SELECT @SQLString = '
	EXECUTE (''BEGIN ODS_API.adv_to_ods_notify.req_adv_to_ods_jnl_process(?,?); END;'', @batchid,@vol) AT ' + @ODSPrefix
	EXECUTE sp_executesql @SQLString, N'@batchid INT, @vol INT', @batchid, @vol

	--Send email notification
	DECLARE @Message varchar(250)
	SET @Message = CAST(@batchid AS VARCHAR(20)) + ' ADVBatchKey; ' + CAST(@vol AS VARCHAR(20)) + ' # of Records Sent'

	EXEC msdb.dbo.sp_send_dbmail  
	@recipients = 'melanie.zaremba@enterbridge.com; scott.toolson@enterbridge.com; devin.huch@enterbridge.com; kevin.kennedy@enterbridge.com',
	@subject = 'Claim Journal Outgoing Processed',
	@body = @Message, 
	@body_format = 'HTML'

--COMMIT TRAN

SELECT GETDATE() 

GO
