SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- Stored Procedure

CREATE PROCEDURE [cm].[Load_StagingTransaction] (@BatchId INT, @RecordCount INT)

AS

SET NOCOUNT ON 


DECLARE
	@SQLString NVARCHAR(4000) = '',
	@ODSPrefix NVARCHAR(4000) = ''


SELECT 
	@ODSPrefix = ParamValue
FROM 
	dbo.[Param]
WHERE 
	ParamName = 'ClaimsManagerODSPrefix'


TRUNCATE TABLE [cm].[StagingTransaction]

SELECT @SQLString = '
INSERT INTO [cm].[StagingTransaction]
           ([TransactionID]
           ,[DelNo]
           ,[DeliveryDate]
           ,[TransNo]
           ,[TransactionDate]
           ,[SLedgerNo]
           ,[SLedgerDate]
           ,[BranchCode]
           ,[BranchDescription]
           ,[BranchBrand]
           ,[GpAllocBranchCode]
           ,[PriceOrderType]
           ,[TransactionType]
           ,[PriceDerivation]
           ,[LineNumber]
           ,[ChargeNoteID]
           ,[CustCntNo]
           ,[AccountID]
           ,[AccountName]
           ,[CustomerOrderReference]
           ,[PyramidCode]
           ,[ProductCode]
           ,[ProductDescription]
           ,[SupplierProductCode]
           ,[LLSPG]
           ,[Supplier]
           ,[KnownValueItem]
           ,[VariablePackInd]
           ,[SupplyType]
           ,[Qty]
           ,[UnitPrice]
           ,[NettPrice]
           ,[TradingPrice]
		   ,[SupplierNettCost]
           ,[ODSBatchKey]
		   ,[CTSReleaseCode]
		   ,[CTSReleaseUser]
		   ,[CTSReleaseDate]
		   ,[TransactionSource]
		   ,[DeliveryAddressLine1]
		   ,[DeliveryAddressLine2]
		   ,[DeliveryAddressLine3]
		   ,[DeliveryAddressLine4]
		   ,[DeliveryAddressLine5]
		   ,[DeliveryPostCode]
		   ,[TradingMargin]
		   ,[FinancialMargin]
		   ,[CreditReasonCode]
		   )
     SELECT
		    SRC_TXN_SK
           ,DEL_NO 
           ,DELIVERY_DATE 
           ,TRANS_NO 
           ,TRANSACTION_DATE 
           ,SLEDGER_NO 
           ,SLEDGER_DATE 
           ,BRANCH_CODE 
           ,BRANCH_DESCRIPTION 
           ,BRANCH_BRAND 
           ,GP_ALLOC_BRANCH_CODE
           ,PRICE_ORDER_TYPE
           ,TRANSACTION_TYPE
           ,PRICE_DERIVATION
           ,LINE_NUMBER
           ,CHARGE_NOTE_ID 
           ,CUST_CNT_NO
           ,ACCOUNT_ID
           ,ACCOUNT_NAME
           ,CUST_ORD_REF 
           ,PYRAMID_CODE
           ,PRODUCT_CODE
           ,PRODUCT_DESCRIPTION
		   ,SUPPLER_PROD_CODE
           ,LLSPG
           ,SUPPLIER
           ,KNOWN_VALUE_ITEM
           ,VARIABLE_PACK_IND
           ,SUPPLY_TYPE
           ,QTY
           ,UNIT_PRICE 
           ,LINE_GOODS_VALUE 
           ,TRADING_PRICE
           ,CAST(SUPPLIERS_NETT_COST AS decimal(18,9))
           ,ODS_OUT_BATCH_ID
		   ,CTS_REL_CODE
		   ,CTS_REL_USER
		   ,CTS_REL_DATE
		   ,TRANSACTION_SOURCE 
		   ,DEL_ADDRESS_LINE_1
		   ,DEL_ADDRESS_LINE_2
		   ,DEL_ADDRESS_LINE_3
		   ,DEL_ADDRESS_LINE_4
		   ,DEL_ADDRESS_LINE_5
		   ,DEL_POSTCODE 
		   ,TRADING_MARGIN
		   ,FINANCIAL_MARGIN
		   ,CRD_REASON_CODE 



FROM ' + @ODSPrefix + '..[ODS_DATA_OUT].[ODS_CONTRACT_CLAIMS_TXNS]

WHERE ODS_OUT_BATCH_ID = @BatchId
'

EXECUTE sp_executesql @SQLString, N'@BatchId INT', @BatchId


IF
(
SELECT COUNT(*)
FROM cm.StagingTransaction
WHERE ODSBatchKey = @BatchId
)
= @RecordCount

BEGIN

	SELECT @SQLString = '
		UPDATE ' + @ODSPrefix + '..ODS_DATA_OUT.ADV_INTERFACE_CONTROL
		SET COLLECT_DATETIME = GETDATE()
		WHERE TABLE_NAME = ''ODS_CONTRACT_CLAIMS_TXNS''
			AND BATCH_ID = @BatchId
	'

	EXECUTE sp_executesql @SQLString, N'@BatchId INT', @BatchId

	SELECT @SQLString = 'EXECUTE (''BEGIN ODS_API.adv_to_ods_notify.ack_advanous_txn_out_success(?,?,?,?); COMMIT; END;'', @BatchId,@RecordCount,'''','''') AT '  + @ODSPrefix
	EXECUTE sp_executesql @SQLString, N'@BatchId INT, @RecordCount INT', @BatchId, @RecordCount



END

GO
