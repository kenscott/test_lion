SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [lion].[Load_WebUserRolesAndPermissions]
WITH EXECUTE AS CALLER
AS
SET NOCOUNT ON

EXEC LogDCPEvent 'ETL - lion.Load_WebUserRolesAndPermissions', 'B'

DECLARE 
	@RowsI Int_type,
	@RowsU Int_type,
	@RowsD Int_type


SET @RowsI = 0
SET @RowsU = 0
SET @RowsD = 0


-- check our special users for their roles --
DECLARE	
	@CoordinatorWebUserKey INT
		
SELECT TOP 1
	@CoordinatorWebUserKey = MIN(WebUserKey)
FROM WebUser wu
INNER JOIN dbo.ODSPerson odsp
	ON odsp.ODSPersonKey = wu.ODSPersonKey
WHERE 
	odsp.FullName = 'coordinator'
	OR odsp.ExtID = '0'

IF (SELECT COUNT(*) FROM WebUserRole WHERE WebUserKey = @CoordinatorWebUserKey) <= 0
BEGIN
	INSERT INTO WebUserRole (WebUserKey, WebRoleKey)
	SELECT @CoordinatorWebUserKey, wr.WebRoleKey
	FROM dbo.WebRole wr 
	WHERE @CoordinatorWebUserKey IS NOT NULL
	SET @RowsI = @RowsI + @@ROWCOUNT
END


-- delete any (hierarchy) Permissions/Roles users no longer have
; WITH a AS (
	-- the list of permssions each user has through their current place in the hiearchy file (LevelId) --
	SELECT DISTINCT
		wu.WebUserKey,
		wrp.WebPermissionKey
	FROM dbo.WebUser wu
	INNER JOIN dbo.ODSPerson odsp	
		ON odsp.ODSPersonKey = wu.ODSPersonKey
	INNER JOIN dbo.WebRole wr
		ON wr.WebRoleName = ISNULL(odsp.LevelId, N'')
	INNER JOIN dbo.WebRolePermission wrp
		ON wrp.WebRoleKey = wr.WebRoleKey
	WHERE
		wr.SystemManaged = 1
)
DELETE wup
FROM dbo.WebUserPermission wup
INNER JOIN dbo.WebRolePermission wrp
	ON wrp.WebPermissionKey = wup.WebPermissionKey
INNER JOIN dbo.WebRole wr
	ON wr.WebRoleKey = wrp.WebRoleKey
WHERE 
	wr.SystemManaged = 1
	AND WebUserKey <> @CoordinatorWebUserKey
	AND NOT EXISTS (SELECT 1 FROM a WHERE a.WebUserKey = wup.WebUserKey AND a.WebPermissionKey = wup.WebPermissionKey)
SET @RowsD = @RowsD + @@ROWCOUNT

--DELETE FROM dbo.WebUserPermission
--WHERE WebUserPermissionKey IN (
--	SELECT wup.WebUserPermissionKey
--	FROM dbo.ODSPerson odsp
--	INNER JOIN WebUser wu 
--		ON wu.ODSPersonKey = odsp.ODSPersonKey
--	INNER JOIN dbo.WebUserRole wur
--		ON wur.WebUserKey = wu.WebUserKey
--	INNER JOIN dbo.WebRole wr
--		ON wr.WebRoleKey = wur.WebRoleKey
--		AND wr.WebRoleName IN ('Administrator', 'PPEManager', 'CentralPricing', 'CentralBusinessUnit', 'RegionManager', 'AreaManager', 'NetworkManager','BranchManager')
--	INNER JOIN dbo.WebRolePermission wrp
--		ON wrp.WebRoleKey = wr.WebRoleKey
--	INNER JOIN dbo.WebUserPermission wup
--		ON wup.WebUserKey = wu.WebUserKey
--		AND wup.WebPermissionKey = wrp.WebPermissionKey
--	WHERE 
--		ISNULL(odsp.LevelId, N'') <> WebRoleName
--	)
--SET @RowsD = @RowsD + @@ROWCOUNT

DELETE from dbo.WebUserRole
WHERE WebUserRoleKey IN (
	SELECT wur.WebUserRoleKey
	FROM dbo.ODSPerson odsp
	INNER JOIN WebUser wu 
		ON wu.ODSPersonKey = odsp.ODSPersonKey
	INNER JOIN dbo.WebUserRole wur
		ON wur.WebUserKey = wu.WebUserKey
	INNER JOIN dbo.WebRole wr
		ON wr.WebRoleKey = wur.WebRoleKey
	WHERE 
		wr.SystemManaged = 1
		AND wr.WebRoleName <> N'P2PUser'
		AND ISNULL(odsp.LevelId, N'') <> WebRoleName
	)
AND WebUserKey <> @CoordinatorWebUserKey
SET @RowsD = @RowsD + @@ROWCOUNT
	

-- LPF-5144 Ken Scott and Kevin K. -- removed insert into web user permssion table as its use has been depricated
-- ensure that web users have the latest role assignment (permissions [for the role] first, then the role)
--INSERT dbo.WebUserPermission ( WebUserKey , WebPermissionKey  )
--SELECT DISTINCT wu.WebUserKey, wrp.WebPermissionKey
--FROM dbo.ODSPerson odsp
--INNER JOIN WebUser wu 
--	ON wu.ODSPersonKey = odsp.ODSPersonKey
--INNER JOIN dbo.WebRole wr
--	ON wr.WebRoleName = odsp.LevelId
--INNER JOIN dbo.WebRolePermission wrp
--	ON wrp.WebRoleKey = wr.WebRoleKey
--WHERE
--	wr.SystemManaged = 1
--	AND WebUserKey <> @CoordinatorWebUserKey
--	AND NOT EXISTS (SELECT 1 FROM WebUserPermission wup WHERE wup.WebUserKey = wu.WebUserKey AND wup.WebPermissionKey = wrp.WebPermissionKey)
--SET @RowsI = @RowsI + @@ROWCOUNT
		
INSERT WebUserRole (WebUserKey, WebRoleKey)
SELECT DISTINCT WebUserKey, wr.WebRoleKey AS WebRoleKey
FROM 
	ODSPerson odsp 
INNER JOIN WebUser wu 
	ON wu.ODSPersonKey = odsp.ODSPersonKey
INNER JOIN dbo.WebRole wr
	ON wr.WebRoleName = odsp.LevelId
WHERE
	wr.SystemManaged = 1
	AND WebUserKey <> @CoordinatorWebUserKey
	AND NOT EXISTS (SELECT 1 FROM WebUserRole wur WHERE wur.WebUserKey = wu.WebUserKey AND wur.WebRoleKey = wr.WebRoleKey)
SET @RowsI = @RowsI + @@ROWCOUNT


EXEC LogDCPEvent 'ETL - lion.Load_WebUserRolesAndPermissions', 'E', @RowsI, @RowsU, @RowsD




GO
