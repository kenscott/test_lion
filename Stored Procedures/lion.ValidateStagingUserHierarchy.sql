SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE [lion].[ValidateStagingUserHierarchy] (
	@ParamDebug [char](1) = 'N'
)
AS

/*
LPF-5680, Remove Check that the New Level 0 rows cannot be blank

LPF-5734, Hierarchy - Don't load two managers for the same branch brand, 2017-07-27

EXEC lion.ValidateStagingUserHierarchy

EXEC lion.ValidateStagingUserHierarchy 'Y'

*/



SET NOCOUNT ON

	

IF @ParamDebug = 'Y'
BEGIN

	INSERT lion.StagingUserHierarchy (
		[Level0 Name],
		[Level0 Email Address],
		[Level0 Branch],
		[Level0 Brand],
		[Level1 Name],
		[Level1 Email Address],
		[Level1 Title],
		[Level1 Assignment],
		[Level2 Name],
		[Level2 Email Address],
		[Level2 Title],
		[Level2 Assignment],
		RowIndex)
	VALUES (
		'Space Cowboy',
		'scowboy@steveMillerBand.net',
		'Brand Specialist',
		'Pipe',
		'Fred Flintstone',
		'fred.flintstone@flintstones.com',
		'Branch Manager',
		'USA',
		'Fred Flintstone',
		'fred.flintstone@flintstones.com',
		'Network Manager',
		'L10A',
		1
	)

	INSERT lion.StagingUserHierarchy (
		[Level0 Name],
		[Level0 Email Address],
		[Level0 Branch],
		[Level0 Brand],
		[Level1 Name],
		[Level1 Email Address],
		[Level1 Title],
		[Level1 Assignment],
		[Level2 Name],
		[Level2 Email Address],
		[Level2 Title],
		[Level2 Assignment],
		RowIndex)
	VALUES (
		'Gangster Love',
		'Glove@steveMillerBand.net',
		'Brand Specialist',
		'Climate',
		'Barney Rubble',
		'barney.rubble@flintstones.com',
		'Branch Manager',
		'USA',
		'Fred Flintstone',
		'fred.flintstone@flintstones.com',
		'Network Manager',
		'L10A',
		2
	)

	INSERT lion.StagingUserHierarchy (
		[Level0 Name],
		[Level0 Email Address],
		[Level0 Branch],
		[Level0 Brand],
		[Level1 Name],
		[Level1 Email Address],
		[Level1 Title],
		[Level1 Assignment],
		[Level2 Name],
		[Level2 Email Address],
		[Level2 Title],
		[Level2 Assignment],
		RowIndex)
	VALUES (
		'Maurice',
		'Maurice@steveMillerBand.net',
		'Brand Specialist',
		'Climate',
		'Barney Rubble',
		'Barney.Rubble@flintstones.com',
		'Branch Manager',
		'USA',
		'Fred Flintstone',
		'fred.flintstone@flintstones.com',
		'Network Manager',
		'L10A',
		3
	)

	INSERT lion.StagingUserHierarchy (
		[Level0 Name],
		[Level0 Email Address],
		[Level0 Branch],
		[Level0 Brand],
		[Level1 Name],
		[Level1 Email Address],
		[Level1 Title],
		[Level1 Assignment],
		[Level2 Name],
		[Level2 Email Address],
		[Level2 Title],
		[Level2 Assignment],
		RowIndex)
	VALUES (
		'The Joker',
		'TJoke@steveMillerBand.net',
		'Brand Specialist',
		'Infrastructure',
		'BamBam Rubble1',
		'bambam.rubble1flintstones.com',
		'Branch Manager',
		'USA',
		'Fred Flintstone',
		'fred.flintstone@flintstones.com',
		'Network Manager',
		'L10A',
		4
	)

	INSERT lion.StagingUserHierarchy (
		[Level0 Name],
		[Level0 Email Address],
		[Level0 Branch],
		[Level0 Brand],
		[Level1 Name],
		[Level1 Email Address],
		[Level1 Title],
		[Level1 Assignment],
		[Level2 Name],
		[Level2 Email Address],
		[Level2 Title],
		[Level2 Assignment],
		RowIndex)
	VALUES (
		'The Sinner',
		'Tsinner@steveMillerBand.net',
		'Brand Specialist',
		'Plumb',
		'BamBam Rubble2',
		'bambam.rubble2@flintstones.com',
		'Branch Manager',
		'USA',
		'Fred Flintstone',
		'fred.flintstoneflintstones.com',
		'Network Manager',
		'L10A',
		5
	)

	INSERT lion.StagingUserHierarchy (
		[Level0 Name],
		[Level0 Email Address],
		[Level0 Branch],
		[Level0 Brand],
		[Level1 Name],
		[Level1 Email Address],
		[Level1 Title],
		[Level1 Assignment],
		[Level2 Name],
		[Level2 Email Address],
		[Level2 Title],
		[Level2 Assignment],
		RowIndex)
	VALUES (
		'The Grinner',
		'TGrinner@steveMillerBand.net',
		'Brand Specialist',
		'Drain',
		'BamBam Rubble3',
		'bambamrubble3@flintstonescom',
		'Branch Manager',
		'USA',
		'Fred Flintstone',
		'fred.flintstone@flintstones.com',
		'Network Manager',
		'L10A',
		6
	)

	INSERT lion.StagingUserHierarchy (
		[Level0 Name],
		[Level0 Email Address],
		[Level0 Branch],
		[Level0 Brand],
		[Level1 Name],
		[Level1 Email Address],
		[Level1 Title],
		[Level1 Assignment],
		[Level2 Name],
		[Level2 Email Address],
		[Level2 Title],
		[Level2 Assignment],
		RowIndex)
	VALUES (
		'The Lover',
		'TLover@steveMillerBand.net',
		'Brand Specialist',
		'Parts',
		'BamBam Rubble4',
		'bambam.rubble4@flintstones.com',
		'Branch Manager',
		'USA',
		'Fred Flintstone',
		'fredflintstone@flintstonescom',
		'Network Manager',
		'L10A',
		7
	)

	INSERT lion.StagingUserHierarchy (
		[Level0 Name],
		[Level0 Email Address],
		[Level0 Branch],
		[Level0 Brand],
		[Level1 Name],
		[Level1 Email Address],
		[Level1 Title],
		[Level1 Assignment],
		[Level2 Name],
		[Level2 Email Address],
		[Level2 Title],
		[Level2 Assignment],
		RowIndex)
	VALUES (
		'Lovey Dovey',
		'LoveyDovey@steveMillerBand.net',
		'Brand Specialist',
		'Pipe',
		'Fred Flintstone',
		'fred.flintstone@flintstones.com',
		'Branch Manager',
		'USAaa',
		'Fred Flintstone',
		'fred.flintstone@flintstones.com',
		'Network Manager',
		'L10A',
		8
	)
	--SELECT * FROM lion.StagingUserHierarchy

END


/* initial scribbing */
UPDATE lion.StagingUserHierarchy 
SET 
	Message = '',
	[Level0 Name] = RTRIM(LTRIM([Level0 Name])),
	[Level0 Email Address]= RTRIM(LTRIM([Level0 Email Address])),
	[Level0 Branch] = RTRIM(LTRIM([Level0 Branch])),
	[Level0 Brand] = RTRIM(LTRIM([Level0 Brand])),
	[Level1 Name] = RTRIM(LTRIM([Level1 Name])),
	[Level1 Email Address]= RTRIM(LTRIM([Level1 Email Address])),
	[Level1 Title] = RTRIM(LTRIM([Level1 Title])),
	[Level1 Assignment] = RTRIM(LTRIM([Level1 Assignment])),
	[Level2 Name] = RTRIM(LTRIM([Level2 Name])),
	[Level2 Email Address] = RTRIM(LTRIM([Level2 Email Address])),
	[Level2 Title] = RTRIM(LTRIM([Level2 Title])),
	[Level2 Assignment] = RTRIM(LTRIM([Level2 Assignment]))



UPDATE s
SET 
	s.Message = ISNULL(s.Message, N'') + N'Empty/Null row ' 
FROM lion.StagingUserHierarchy s
WHERE 
	/*      LPF-5680
	ISNULL(NULLIF([Level0 Name], 'NULL'), N'') = N''
	--AND ISNULL(NULLIF([Level0 Email Address], 'NULL'), N'') = N''
	AND ISNULL(NULLIF([Level0 Branch], 'NULL'), N'') = N''
	AND ISNULL(NULLIF([Level0 Brand], 'NULL'), N'') = N'' 
	*/
	ISNULL(NULLIF([Level1 Name], 'NULL'), N'') = N''
	AND ISNULL(NULLIF([Level1 Email Address], 'NULL'), N'') = N''
	AND ISNULL(NULLIF([Level1 Title], 'NULL'), N'') = N''
	AND ISNULL(NULLIF([Level1 Assignment], 'NULL'), N'') = N''
	AND ISNULL(NULLIF([Level2 Name], 'NULL'), N'') = N''
	AND ISNULL(NULLIF([Level2 Email Address], 'NULL'), N'') = N''
	AND ISNULL(NULLIF([Level2 Title], 'NULL'), N'') = N''
	AND ISNULL(NULLIF([Level2 Assignment], 'NULL'), N'') = N''



/* check file for dups */
; WITH a AS (
	SELECT
		RowIndex,
		MIN(RowIndex) OVER (PARTITION BY 
			[Level0 Name],
			[Level0 Email Address],
			[Level0 Branch],
			[Level0 Brand],
			[Level1 Name],
			[Level1 Email Address],
			[Level1 Title],
			[Level1 Assignment],
			[Level2 Name],
			[Level2 Email Address],
			[Level2 Title],
			[Level2 Assignment]
			ORDER BY 
				RowIndex 
			) FirstRowIndex,
		ROW_NUMBER() OVER (PARTITION BY 
			[Level0 Name],
			[Level0 Email Address],
			[Level0 Branch],
			[Level0 Brand],
			[Level1 Name],
			[Level1 Email Address],
			[Level1 Title],
			[Level1 Assignment],
			[Level2 Name],
			[Level2 Email Address],
			[Level2 Title],
			[Level2 Assignment]
			ORDER BY 
				RowIndex 
			) 
		AS RowRank
	FROM lion.StagingUserHierarchy s 
)
UPDATE s
SET 
	s.Message = ISNULL(s.Message, N'') + N'Row duplicates row ' + CAST(FirstRowIndex AS NVARCHAR(25)) + N' '
FROM lion.StagingUserHierarchy s
INNER JOIN a 
	ON a.RowIndex = s.RowIndex
WHERE
	a.RowRank <> 1 



/* check for missing email address (very key) */
--UPDATE s
--SET 
--	s.Message = ISNULL(s.Message, N'') + N'Missing Level 0 Email Address ' 
--FROM lion.StagingUserHierarchy s
--WHERE 
--	ISNULL(NULLIF([Level0 Email Address], 'NULL'), N'') = N''

UPDATE s
SET 
	s.Message = ISNULL(s.Message, N'') + N'Missing Level 1 Email Address ' 
FROM lion.StagingUserHierarchy s
WHERE 
	ISNULL(NULLIF([Level1 Email Address], 'NULL'), N'') = N''

UPDATE s
SET 
	s.Message = ISNULL(s.Message, N'') + N'Missing Level 2 Email Address ' 
FROM lion.StagingUserHierarchy s
WHERE 
	ISNULL(NULLIF([Level2 Email Address], 'NULL'), N'') = N''


/* check for invalid characters in email addresses */
UPDATE s
SET 
	s.Message = ISNULL(s.Message, N'') + N'Invalid character(s) in Level 0 Email Address ' 
FROM lion.StagingUserHierarchy s
WHERE 
	ISNULL([Level0 Email Address], N'') <> N''
	AND CHARINDEX(N',', [Level0 Email Address]) >= 1

UPDATE s
SET 
	s.Message = ISNULL(s.Message, N'') + N'Invalid character(s) in Level 1 Email Address ' 
FROM lion.StagingUserHierarchy s
WHERE 
	CHARINDEX(N',', [Level1 Email Address]) >= 1

UPDATE s
SET 
	s.Message = ISNULL(s.Message, N'') + N'Invalid character(s) in Level 2 Email Address ' 
FROM lion.StagingUserHierarchy s
WHERE 
	CHARINDEX(N',', [Level2 Email Address]) >= 1


/* check for valid formatted email addresses */
UPDATE s
SET 
	s.Message = ISNULL(s.Message, N'') + N'Invalid Level 0 Email Address ' 
FROM lion.StagingUserHierarchy s
WHERE 
	ISNULL([Level0 Email Address], N'') <> N''
	AND (
		CHARINDEX(N'@', [Level0 Email Address]) <= 0
		OR CHARINDEX(N'.', [Level0 Email Address]) <= 0
	)

UPDATE s
SET 
	s.Message = ISNULL(s.Message, N'') + N'Invalid Level 1 Email Address ' 
FROM lion.StagingUserHierarchy s
WHERE 
	CHARINDEX(N'@', [Level1 Email Address]) <= 0
	OR CHARINDEX(N'.', [Level1 Email Address]) <= 0

UPDATE s
SET 
	s.Message = ISNULL(s.Message, N'') + N'Invalid Level 2 Email Address ' 
FROM lion.StagingUserHierarchy s
WHERE 
	CHARINDEX(N'@', [Level2 Email Address]) <= 0
	OR CHARINDEX(N'.', [Level2 Email Address]) <= 0




-- ; WITH a AS (
--	SELECT
--		RowIndex,
--		MIN(RowIndex) OVER (PARTITION BY 
--			[Level1 Email Address]
--			ORDER BY 
--				RowIndex 
--			) FirstRowIndex,
--		ROW_NUMBER() OVER (PARTITION BY 
--			[Level1 Email Address]
--			ORDER BY 
--				RowIndex 
--			) 
--		AS RowRank
--	FROM lion.StagingUserHierarchy s 
--)
--UPDATE s
--SET 
--	s.Message = ISNULL(s.Message, N'') + N'Level 1 Email address specified multiple times and repeates value from row  ' + CAST(FirstRowIndex AS NVARCHAR(25)) + N' '
--FROM lion.StagingUserHierarchy s
--INNER JOIN a 
--	ON a.RowIndex = s.RowIndex
--WHERE
--	a.RowRank <> 1 



UPDATE s
SET 
	s.Message = ISNULL(s.Message, N'') + N'Level 1 Manager reports to more than one Level 2 manager! ' 
FROM lion.StagingUserHierarchy s
WHERE [Level1 Email Address] IN (
	SELECT 
		[Level1 Email Address]
	FROM (
		SELECT DISTINCT
			[Level1 Email Address], 
			[Level2 Email Address]
	FROM lion.StagingUserHierarchy
	) a
	GROUP BY 
		[Level1 Email Address]
	HAVING 
		COUNT(*) > 1 
)


--UPDATE s
--SET 
--	s.Message = ISNULL(s.Message, N'') + N'Level 0 Manager reports to more than one Level 1 manager! ' 
--FROM lion.StagingUserHierarchy s
--WHERE [Level0 Email Address] IN (
--	SELECT 
--		[Level0 Email Address]
--	FROM (
--		SELECT DISTINCT
--			[Level0 Email Address], 
--			[Level1 Email Address]
--	FROM lion.StagingUserHierarchy
--	) a
--	GROUP BY 
--		[Level0 Email Address]
--	HAVING 
--		COUNT(*) > 1 
--)


UPDATE s
SET 
	s.Message = ISNULL(s.Message, N'') + N'Branch is managed by more than one Level 1 manager! ' 
FROM lion.StagingUserHierarchy s
WHERE [level1 assignment] IN (
	SELECT 
		[level1 Assignment]
	FROM (
		SELECT DISTINCT 
			[level1 Assignment],
			[Level1 Email Address]
		FROM 
			lion.StagingUserHierarchy s
		WHERE 
			s.[level1 Assignment] IS NOT NULL
			AND s.[Level1 Title] IS NOT NULL
			AND s.[Level1 Title] = 'Branch Manager'
	) a
	GROUP BY 
		[level1 Assignment]
	HAVING 
		COUNT(*) > 1 
)


UPDATE s
SET 
	s.Message = ISNULL(s.Message, N'') + N'Network is managed by more than one Level 2 manager! ' 
FROM lion.StagingUserHierarchy s
WHERE [level2 assignment] IN (
	SELECT 
		[level2 Assignment]
	FROM (
		SELECT DISTINCT 
			[level2 Assignment],
			[Level2 Email Address]
		FROM 
			lion.StagingUserHierarchy s
		WHERE 
			s.[level2 Assignment] IS NOT NULL
			AND s.[Level2 Title] IS NOT NULL
			AND s.[Level2 Title] = 'Network Manager'
	) a
	GROUP BY 
		[level2 Assignment]
	HAVING 
		COUNT(*) > 1 
)


DECLARE @ErrorTable TABLE (
	[Level0 Name] [nvarchar](255) NULL,
	[Level0 Email Address] [nvarchar](255) NULL,
	[Level0 Branch] [nvarchar](255) NULL,
	[Level0 Brand] [nvarchar](255) NULL,
	[Level1 Name] [nvarchar](255) NULL,
	[Level1 Email Address] [nvarchar](255) NULL,
	[Level1 Title] [nvarchar](255) NULL,
	[Level1 Assignment] [nvarchar](255) NULL,
	[Level2 Name] [nvarchar](255) NULL,
	[Level2 Email Address] [nvarchar](255) NULL,
	[Level2 Title] [nvarchar](255) NULL,
	[Level2 Assignment] [nvarchar](255) NULL,
	[RowIndex] [int] NOT NULL,
	[Message] [nvarchar](255) NOT NULL DEFAULT ('')
)

INSERT @ErrorTable (
	[Level0 Name],
	[Level0 Email Address],
	[Level0 Branch],
	[Level0 Brand],
	[Level1 Name],
	[Level1 Email Address],
	[Level1 Title],
	[Level1 Assignment],
	[Level2 Name],
	[Level2 Email Address],
	[Level2 Title],
	[Level2 Assignment],
	[RowIndex],
	[Message]
	)
SELECT
	[Level0 Name],
	[Level0 Email Address],
	[Level0 Branch],
	[Level0 Brand],
	[Level1 Name],
	[Level1 Email Address],
	[Level1 Title],
	[Level1 Assignment],
	[Level2 Name],
	[Level2 Email Address],
	[Level2 Title],
	[Level2 Assignment],
	[RowIndex],
	[Message]
FROM 
	lion.StagingUserHierarchy 
WHERE 
	ISNULL(Message, N'') <> N''



IF (SELECT COUNT(*) FROM @ErrorTable) >= 1
BEGIN
	
	TRUNCATE TABLE lion.StagingUserHierarchy

END

SELECT
	--NEWID() AS GUID,
	* 
FROM @ErrorTable

GO
