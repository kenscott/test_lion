SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE [ppe].[Get_RequestCalcs] (@RequestKey INT, @Price MONEY, @Disc1 AS DEC(38,8), @Disc2 AS DEC(38,8), @TermType AS VARCHAR(2) = 'TD') AS
BEGIN

	--DECLARE @RequestKey INT, @Price MONEY, @Disc1 AS DEC(38,8), @Disc2 AS DEC(38,8), @TermType AS VARCHAR(2)
	--SELECT @Price=2.256, @Disc1 = 0.45, @Disc2 = NULL, @RequestKey = 2167, @TermType = 'TD'

	DECLARE 
	    @DPC AS DEC(38, 8),
		@PPC AS DEC(38,8), 
		@AcceptedImpact MONEY,
		--@RecommendedImpact MONEY, 
		@GPP DEC(38,8), 
		@PriceIndex INT,
		@LTMSalesAtTrade MONEY,
		@AccountKey INT,
		@ItemKey INT,
		@ItemGroup3Key INT,
		--@PropDiscount DEC(38,8),
		--@NewDiscount DEC(38,8),
		@NewCompoundDiscount DEC(38,8),
		@RedDiscount  DEC(38,8),
		@AmberDiscount  DEC(38,8),
		@GreenDiscount  DEC(38,8),
		@DeleteTerm BIT,
		@SPGSkippedSales MONEY 

	IF @TermType = 'FP' AND @Disc1 IS NULL
	BEGIN
		SELECT @Disc1 =  1 - (@Price / NULLIF(di.ItemUDDecimal1, 0))
		  FROM ppe.RequestMain r
		  JOIN dbo.DimItem di ON di.ItemKey = r.ItemKey
		 WHERE r.RequestKey = @RequestKey
	END

	SELECT @NewCompoundDiscount = ISNULL(Discount, 0)
	  FROM ppe.fn_GetCompoundDiscount(@Disc1, @Disc2)

	IF @Price IS NULL
	BEGIN
		SELECT @Price = di.ItemUDDecimal1 * (1 - @NewCompoundDiscount)
		  FROM ppe.RequestMain r
		  JOIN dbo.DimItem di ON di.ItemKey = r.ItemKey
		 WHERE r.RequestKey = @RequestKey
	END

	SELECT 
	    @PPC=
		CASE 
			WHEN r.ItemKey <> 1 
				THEN (@Price-ISNULL(CurrentExceptionFixedPrice, di.ItemUDDecimal1 * (1 - CurrentExceptionDiscount)))/NULLIF(ISNULL(CurrentExceptionFixedPrice, di.ItemUDDecimal1 * (1 - CurrentExceptionDiscount)),0)
			ELSE ((1 - @NewCompoundDiscount) / NULLIF((1 - ISNULL(CurrentCompoundDiscount,0)),0))-1
		END,
	  --  @DPC=
			--CASE 
			--	WHEN @TermType = 'FP' 
			--		THEN (@Price-ISNULL(CurrentExceptionFixedPrice, di.ItemUDDecimal1 * (1 - r.CurrentExceptionDiscount)))/NULLIF(di.ItemUDDecimal1,0)
			--	ELSE (COALESCE(CurrentExceptionDiscount, CurrentCompoundDiscount, 0) - @NewCompoundDiscount)
			--END,
        @DPC= -- below is Jose's code from LPF-1143
            CASE
                WHEN @TermType = 'FP' 
					THEN (@Price-di.ItemUDDecimal1 * (1 - r.LastDiscount))/NULLIF(di.ItemUDDecimal1,0)
                ELSE (ISNULL(r.LastDiscount, ISNULL(CurrentCompoundDiscount,DefaultDiscount)) - @NewCompoundDiscount)
            END,
		@AccountKey = r.AccountKey,
		@ItemKey = r.ItemKey,
		@ItemGroup3Key = r.ItemGroup3Key,
		@GPP = 	CASE 
					WHEN @Price IS NOT NULL THEN (@Price-di.ItemUDDecimal3 - di.ItemUDDecimal4 - di.ItemUDDecimal5)/NULLIF(@Price,0)
					ELSE NULL
				END ,
		@PriceIndex = ndi.DiscountIndex,
		@Price = CASE 
				WHEN @TermType = 'FP' THEN @Price
				ELSE ROUND(di.ItemUDDecimal1 * ( 1.0 - @NewCompoundDiscount ), 3)
			END,
		@Disc1 = CASE
				WHEN @TermType = 'FP' THEN  1 - (@Price / NULLIF(di.ItemUDDecimal1, 0))
				ELSE @Disc1
			END ,
		@RedDiscount = rd.Discount,
		@AmberDiscount = ad.Discount,
		@GreenDiscount = gd.Discount,
		@DeleteTerm = r.ProposedDeleteTerm,
		@SPGSkippedSales = ISNULL(r.SPGSkippedSales,0)

	FROM ppe.RequestMain r
	JOIN dbo.DimItem di ON di.ItemKey = r.ItemKey
    CROSS APPLY ppe.fn_GetDiscount(r.AccountKey, r.ItemGroup3Key, r.ItemKey, r.GreenPrice, di.ItemUDDecimal1, 'G') gd
    CROSS APPLY ppe.fn_GetDiscount(r.AccountKey, r.ItemGroup3Key, r.ItemKey, r.AmberPrice, di.ItemUDDecimal1, 'A') ad
    CROSS APPLY ppe.fn_GetDiscount(r.AccountKey, r.ItemGroup3Key, r.ItemKey, r.RedPrice, di.ItemUDDecimal1, 'R') rd
	CROSS APPLY ppe.fn_GetCompoundDiscount (@Disc1, @Disc2) cd
	CROSS APPLY ppe.fn_GetSimpleDiscount(CASE WHEN @TermType='FP' THEN @Price ELSE NULL END, di.ItemUDDecimal1,
	                                     NULL, cd.Discount) nsd
	CROSS APPLY ppe.fn_GetDiscountIndex(rd.Discount, ad.Discount, gd.Discount, nsd.Discount) ndi
	WHERE r.RequestKey = @RequestKey

	--IF @ItemKey <> 1
	--	SELECT @LTMSalesAtTrade= SUM(ISNULL(TotalNetQuantity, TotalQuantity) * ItemUDDecimal1 ) 
	--	FROM dbo.FactInvoiceLine fil
	--	JOIN dbo.DimItem di ON di.ItemKey = fil.ItemKey
	--	WHERE fil.Last12MonthsIndicator = N'Y'
	--	AND fil.AccountKey = @AccountKey
	--	AND fil.ItemKey = @ItemKey
	--	AND fil.InvoiceLineGroup1Key = 4
	--	AND fil.UDVarchar1 /*PriceDerivation*/ NOT IN ( 'DC', 'OCD', 'OCL')

	--ELSE	
	--	SELECT @LTMSalesAtTrade= SUM(ISNULL(TotalNetQuantity, TotalQuantity) * ItemUDDecimal1 )
	--	FROM dbo.FactInvoiceLine fil
	--	JOIN dbo.DimItem di ON di.ItemKey = fil.ItemKey
	--	LEFT JOIN lion.CustomerTerms ct
	--		ON ct.AccountKey = fil.AccountKey
	--		AND ct.ItemKey = fil.ItemKey
	--	WHERE fil.Last12MonthsIndicator = N'Y'
	--	AND fil.AccountKey = @AccountKey
	--	AND fil.ItemGroup3Key = @ItemGroup3Key
	--	AND fil.InvoiceLineGroup1Key = 4
	--	AND ct.AccountKey IS NULL
	--	AND fil.UDVarchar1 /*PriceDerivation*/ NOT IN ( 'DC', 'OCD', 'OCL')

	IF @ItemKey <> 1 --LPF-2047
		SELECT @AcceptedImpact= f.SalesAtTrade * ( ALPCD - @NewCompoundDiscount ) 
		FROM ppe.vw_AccountItemSPGImpact f
		WHERE AccountKey = @AccountKey
		AND ItemKey = @ItemKey
	ELSE  --LPF-2047  LPF-2260
	BEGIN
		SELECT @LTMSalesAtTrade = SUM(SalesAtTrade)
		FROM ppe.vw_AccountItemSPGImpact f
		WHERE f.AccountKey = @AccountKey
		AND f.ItemGroup3Key = @ItemGroup3Key 
		AND f.TermsKey IS NULL 

		SELECT @AcceptedImpact = (@LTMSalesAtTrade - @SPGSkippedSales) * ((1.0-a.SPGLastDiscount) - @NewCompoundDiscount )
		FROM  [ppe].[fn_GetDiscountSPG] (@AccountKey,@ItemGroup3Key) a


	END

		--SELECT @AcceptedImpact= SUM(CASE WHEN f.KVIIndicator = 'Y' AND f.InCustomerSPG = 'N' THEN 0 ELSE (f.SalesAtTrade-@SPGSkippedSales) * ( (1.0-a.SPGLastDiscount) - @NewCompoundDiscount ) END )
		--FROM ppe.vw_AccountItemSPGImpact f
		--CROSS APPLY [ppe].[fn_GetDiscountSPG] (@AccountKey,@ItemGroup3Key) a
		--WHERE f.AccountKey = @AccountKey
		--AND f.ItemGroup3Key = @ItemGroup3Key
		----AND f.TermsKey IS NULL

	--SELECT @AcceptedImpact = CASE WHEN @DeleteTerm = 1 AND @ItemKey = 1 THEN 0 ELSE  @LTMSalesAtTrade * @DPC END --LPF-1949 SPG delete impact = 0

	SELECT @NewCompoundDiscount = ISNULL(Discount, 0)
	  FROM ppe.fn_GetCompoundDiscount(@Disc1, @Disc2)

	SELECT
		NEWID() AS GUID,
		@PPC AS PPC,
		CASE WHEN @DeleteTerm = 1 AND @ItemKey = 1 THEN 0 ELSE @AcceptedImpact END AS AcceptedImpact,
		@GPP AS GPP,
		@PriceIndex AS PricingIndex,
		@NewCompoundDiscount AS TotalDiscount,
		@Price AS FixedPrice,
		@Disc1 AS Discount1,
		@RedDiscount AS RedDiscount,
		@AmberDiscount AS AmberDiscount,
		@GreenDiscount AS GreenDiscount,
		@TermType AS TermType

END







GO
