SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




--CreateScript_PricingRulePlaybook 1

CREATE        PROCEDURE [dbo].[Clone_PricingRulePlaybook]
@PricingRulePlaybookKey INT,
@Command CHAR(1) = 'N',
@ApproachTypeKey INT
AS

DECLARE @SQL NVARCHAR(MAX)

DECLARE @OptimalPriceMethodDesc NVARCHAR(MAX), @OptimalPricePercent NVARCHAR(MAX), @OptimalPriceType NVARCHAR(MAX), @DisplayName NVARCHAR(MAX), @DataPointReUseIndicator CHAR(1)

DECLARE @PricingRuleKey NVARCHAR(MAX), @PricingRuleAttributeSequenceNumber NVARCHAR(MAX), @PricingRuleAttributeTypeKey NVARCHAR(MAX), @PricingBandOptimalPricePercentKey NVARCHAR(MAX), @PricingRuleDescription NVARCHAR(MAX)

DECLARE
@TotalSalesCDFMinPercent NVARCHAR(MAX), 
@TotalInvoiceLinesCDFMinPercent NVARCHAR(MAX), 
@GPPFrequencyThreshold NVARCHAR(MAX)

SELECT 	
@TotalSalesCDFMinPercent = TotalSalesCDFMinPercent,
@TotalInvoiceLinesCDFMinPercent = TotalInvoiceLinesCDFMinPercent,
@GPPFrequencyThreshold = GPPFrequencyThreshold
FROM ATKPricingRulePlaybook WHERE PricingRulePlaybookKey = @PricingRulePlaybookKey


DECLARE @PricingRuleSequenceNumber NVARCHAR(MAX),@MinimumPoints NVARCHAR(MAX), @MinimumSales dec(38,8), @MinimumPointsType NVARCHAR(MAX),@RulingPointThreshold NVARCHAR(MAX),@PricingRuleWhereClause NVARCHAR(MAX),@UseAllDataPointsForOptimalIndicator NVARCHAR(MAX),@PricingRuleJoinClause NVARCHAR(MAX), @HavingClause NVARCHAR(MAX), @SectionNumber SMALLINT

DECLARE @PricingRuleAttributeKey NVARCHAR(MAX)

DECLARE
@PricingRuleBandKey NVARCHAR(MAX),@LowerBandPercent NVARCHAR(MAX),@UpperBandPercent NVARCHAR(MAX),@LowerBandQuantity NVARCHAR(MAX),@UpperBandQuantity NVARCHAR(MAX),@PercentOrQuantityIndicator NVARCHAR(MAX)

IF @Command = 'N'
BEGIN
	INSERT INTO #ScenarioData SELECT  '-------------------------------------'
	INSERT INTO #ScenarioData SELECT  '--Insert new ATKPricingRulePlaybook Row'
	INSERT INTO #ScenarioData SELECT  '-------------------------------------'
	INSERT INTO #ScenarioData SELECT  'Declare @NewPricingRulePlaybookKey int'
	INSERT INTO #ScenarioData SELECT  'Declare @NewATKPricingRuleKey int'
	INSERT INTO #ScenarioData SELECT  'Declare @NewATKPricingRuleBandKey int'
		
	SET @SQL = 
	'
	INSERT INTO ATKPricingRulePlaybook(TotalSalesCDFMinPercent, TotalInvoiceLinesCDFMinPercent, GPPFrequencyThreshold, Version)
	Values ('+ISNULL(@TotalSalesCDFMinPercent,'NULL')+','+ISNULL(@TotalInvoiceLinesCDFMinPercent,'NULL')+','+ISNULL(@GPPFrequencyThreshold,'NULL')+',0)'
	
	INSERT INTO #ScenarioData SELECT  @SQL
	INSERT INTO #ScenarioData SELECT  'Set @NewPricingRulePlaybookKey = @@Identity'
	
	INSERT INTO #ScenarioData SELECT  'Print ''Your new Pricing Rule Playbook Key is: ''+Cast(@NewPricingRulePlaybookKey as nvarchar(10))'
END
ELSE IF @Command = 'U'
BEGIN
	INSERT INTO #ScenarioData SELECT  '------------------------------------------------------'
	INSERT INTO #ScenarioData SELECT  '--Updating ATKPricingRulePlaybook: '+CAST(@PricingRulePlaybookKey AS NVARCHAR(100))
	INSERT INTO #ScenarioData SELECT  '------------------------------------------------------'
	INSERT INTO #ScenarioData SELECT  '	
	Update ATKPricingRulePlaybook
	Set TotalSalesCDFMinPercent = '+ISNULL(@TotalSalesCDFMinPercent,'NULL')+',
	TotalInvoiceLinesCDFMinPercent = '+ISNULL(@TotalInvoiceLinesCDFMinPercent,'NULL')+',
	GPPFrequencyThreshold='+ISNULL(@GPPFrequencyThreshold,'NULL')+'
	Where PricingRulePlaybookKey = '+CAST(@PricingRulePlaybookKey AS NVARCHAR(100))
END
ELSE
BEGIN
	INSERT INTO #ScenarioData SELECT  '------------------------------------------------------'
	INSERT INTO #ScenarioData SELECT  'Your current PricingRulePlaybookKey is: '+CAST(@PricingRulePlaybookKey AS NVARCHAR(100))
	INSERT INTO #ScenarioData SELECT  '------------------------------------------------------'
	IF @ApproachTypeKey IN (1, 4) --Only show this for ME
		INSERT INTO #ScenarioData SELECT  '--TotalSalesCDFMinPercent:          '+ISNULL(@TotalSalesCDFMinPercent, 'NULL')
	IF @ApproachTypeKey IN (1, 4) --Only show this for ME
		INSERT INTO #ScenarioData SELECT  '--TotalInvoiceLinesCDFMinPercent:   '+ISNULL(@TotalInvoiceLinesCDFMinPercent, 'NULL')
	IF @ApproachTypeKey IN (1, 4) --Only show this for ME
		INSERT INTO #ScenarioData SELECT  '--GPPFrequencyThreshold:            '+ISNULL(@GPPFrequencyThreshold, 'NULL')
END


--Now we need to loop through all the segment customer rules for this playbook and insert them
DECLARE PricingRules CURSOR FOR
SELECT 
PricingRuleKey, PricingRulePlaybookKey, PricingRuleSequenceNumber, MinimumPoints, MinimumSales, MinimumPointsType, RulingPointThreshold, REPLACE(PricingRuleWhereClause, '''',''''''), UseAllDataPointsForOptimalIndicator, REPLACE(PricingRuleJoinClause, '''',''''''), DataPointReUseIndicator, PricingRuleDescription, REPLACE(HavingClause, '''',''''''), SectionNumber
FROM ATKPricingRule
WHERE PricingRulePlaybookKey = @PricingRulePlaybookKey
ORDER BY PricingRuleSequenceNumber

OPEN PricingRules
FETCH NEXT FROM PricingRules INTO @PricingRuleKey,@PricingRulePlaybookKey,@PricingRuleSequenceNumber,@MinimumPoints,@MinimumSales,@MinimumPointsType,@RulingPointThreshold,@PricingRuleWhereClause,@UseAllDataPointsForOptimalIndicator,@PricingRuleJoinClause, @DataPointReUseIndicator, @PricingRuleDescription, @HavingClause, @SectionNumber

WHILE @@Fetch_Status = 0
BEGIN
	IF @Command = 'N'
	BEGIN
		INSERT INTO #ScenarioData SELECT  '     -------------------------------------'
		INSERT INTO #ScenarioData SELECT  '     --Insert new ATKPricingRule Row'
		INSERT INTO #ScenarioData SELECT  '     -------------------------------------'
	
		SET @SQL = '
	     INSERT INTO ATKPricingRule(PricingRulePlaybookKey, PricingRuleSequenceNumber, MinimumPoints, MinimumSales, MinimumPointsType, RulingPointThreshold, PricingRuleWhereClause, UseAllDataPointsForOptimalIndicator, PricingRuleJoinClause, DataPointReUseIndicator, Version, HavingClause, SectionNumber, PricingRuleDescription)
	     Values (@NewPricingRulePlaybookKey,'''+ISNULL(@PricingRuleSequenceNumber,'')+''','''+ISNULL(@MinimumPoints,'')+''','''+CAST(ISNULL(@MinimumSales,-9999999999999.99999999) AS NVARCHAR(500))+''','''+ISNULL(@MinimumPointsType,'')+''','''+ISNULL(@RulingPointThreshold,'')+''','''+ISNULL(@PricingRuleWhereClause,'')+''','''+ISNULL(@UseAllDataPointsForOptimalIndicator,'')+''','''+ISNULL(@PricingRuleJoinClause,'')+''','''+@DataPointReUseIndicator+''',0,''' + ISNULL(@HavingClause, '') + ''',''' + ISNULL(CAST(@SectionNumber AS NVARCHAR(25)), '') + ''',''' +  ISNULL(@PricingRuleDescription,'') + ''')'
		INSERT INTO #ScenarioData SELECT  @SQL
	
		INSERT INTO #ScenarioData SELECT  '     Set @NewATKPricingRuleKey = @@Identity'
	END
	ELSE IF @Command = 'U'
	BEGIN
		INSERT INTO #ScenarioData SELECT  '------------------------------------------------------'
		INSERT INTO #ScenarioData SELECT  '--Updating ATKPricingRule: '+CAST(@PricingRuleKey AS NVARCHAR(100))
		INSERT INTO #ScenarioData SELECT  '------------------------------------------------------'
		INSERT INTO #ScenarioData SELECT  '	
		Update ATKPricingRule Set
		PricingRuleSequenceNumber = '''+ISNULL(@PricingRuleSequenceNumber,'')+''',
		MinimumPoints = '''+ISNULL(@MinimumPoints,'')+''',
		MinimumSales = '''+CAST(ISNULL(@MinimumSales,-9999999999999.99999999) AS NVARCHAR(500))+''',
		MinimumPointsType =  '''+ISNULL(@MinimumPointsType,'')+''',
		RulingPointThreshold = '''+ISNULL(@RulingPointThreshold,'')+''',
		PricingRuleWhereClause = '''+ISNULL(@PricingRuleWhereClause,'')+''',
		UseAllDataPointsForOptimalIndicator = '''+ISNULL(@UseAllDataPointsForOptimalIndicator,'')+''',
		PricingRuleJoinClause = '''+ISNULL(@PricingRuleJoinClause,'')+''',
		--DataPointReUseIndicator = '+@DataPointReUseIndicator+'
		DataPointReUseIndicator = '''+@DataPointReUseIndicator+''',
		HavingClause = '''+ISNULL(@HavingClause, '')+''',
		SectionNumber = '''+ISNULL(CAST(@SectionNumber AS NVARCHAR(25)), '')+''',
		PricingRuleDescription = '''+ISNULL(@PricingRuleDescription, '')+'''
		Where PricingRuleKey = '+CAST(@PricingRuleKey AS NVARCHAR(100))
	END
	ELSE
	BEGIN
		INSERT INTO #ScenarioData SELECT  '      ------------------------------------------------------'
		INSERT INTO #ScenarioData SELECT  '      --Your current ATKPricingRule is: '+CAST(@PricingRuleKey AS NVARCHAR(100))
		INSERT INTO #ScenarioData SELECT  '      ------------------------------------------------------'
		INSERT INTO #ScenarioData SELECT  '      --PricingRuleSequenceNumber:            '+ISNULL(@PricingRuleSequenceNumber, 'NULL')
		INSERT INTO #ScenarioData SELECT  '      --MinimumPoints:                        '+ISNULL(@MinimumPoints, 'NULL')
		INSERT INTO #ScenarioData SELECT  '      --MinimumSales:                         '+CAST(ISNULL(@MinimumSales,-9999999999999.99999999) AS NVARCHAR(500))
		INSERT INTO #ScenarioData SELECT  '      --MinimumPointsType:                    '+ISNULL(@MinimumPointsType, 'NULL')
		INSERT INTO #ScenarioData SELECT  '      --RulingPointThreshold:                 '+ISNULL(@RulingPointThreshold, 'NULL')
		INSERT INTO #ScenarioData SELECT  '      --PricingRuleWhereClause:               '+ISNULL(@PricingRuleWhereClause, 'NULL')
		INSERT INTO #ScenarioData SELECT  '      --UseAllDataPointsForOptimalIndicator:  '+ISNULL(@UseAllDataPointsForOptimalIndicator, 'NULL')
		INSERT INTO #ScenarioData SELECT  '      --PricingRuleJoinClause:                '+ISNULL(@PricingRuleJoinClause, 'NULL')
		INSERT INTO #ScenarioData SELECT  '      --DataPointReUseIndicator:              '+ISNULL(@DataPointReUseIndicator, 'NULL')
		INSERT INTO #ScenarioData SELECT  '      --HavingClause:                         '+ISNULL(@HavingClause, 'NULL')
		INSERT INTO #ScenarioData SELECT  '      --SectionNumber:                        '+ISNULL(CAST(@SectionNumber AS NVARCHAR(25)), 'NULL')
		INSERT INTO #ScenarioData SELECT  '      --PricingRuleDescription:               '+ISNULL(@PricingRuleDescription, 'NULL')
	END
	-----------------------------------------------------------------------------
	--Now, load the PricingRuleAttributes for each PricingRule
	-----------------------------------------------------------------------------
		DECLARE Csr_PricingRuleAttribute CURSOR FOR
		SELECT ATKPricingRuleAttribute.PricingRuleAttributeKey, PricingRuleAttributeSequenceNumber, DisplayName, ATKPricingRuleAttribute.PricingRuleAttributeTypeKey
		FROM ATKPricingRuleAttribute
			JOIN ATKPricingRuleAttributeType ON ATKPricingRuleAttribute.PricingRuleAttributeTypeKey = ATKPricingRuleAttributeType.PricingRuleAttributeTypeKey
		WHERE PricingRuleKey = @PricingRuleKey
		ORDER BY PricingRuleAttributeSequenceNumber
		
		OPEN Csr_PricingRuleAttribute
		
		FETCH NEXT FROM Csr_PricingRuleAttribute INTO @PricingRuleAttributeKey, @PricingRuleAttributeSequenceNumber, @DisplayName, @PricingRuleAttributeTypeKey
		WHILE @@Fetch_Status = 0
		BEGIN

			IF @Command = 'N'
			BEGIN
			
				INSERT INTO #ScenarioData SELECT  '          -------------------------------------'
				INSERT INTO #ScenarioData SELECT  '          --Insert new ATKPricingRuleAttribute Row'
				INSERT INTO #ScenarioData SELECT  '          -------------------------------------'
				SET @SQL = '
	          INSERT INTO ATKPricingRuleAttribute(PricingRuleKey, PricingRuleAttributeSequenceNumber, PricingRuleAttributeTypeKey, Version)
	          Values (@NewATKPricingRuleKey,'''+ISNULL(@PricingRuleAttributeSequenceNumber,'')+''','''+ISNULL(@PricingRuleAttributeTypeKey,'')+''',0)  --' + @DisplayName
				INSERT INTO #ScenarioData SELECT  @SQL

			END
			ELSE IF @Command = 'U'
			BEGIN
				INSERT INTO #ScenarioData SELECT  '------------------------------------------------------'
				INSERT INTO #ScenarioData SELECT  '--Updating ATKPricingRuleAttribute: '+CAST(@PricingRuleAttributeKey AS NVARCHAR(100))
				INSERT INTO #ScenarioData SELECT  '------------------------------------------------------'
				INSERT INTO #ScenarioData SELECT  '	
				Update ATKPricingRuleAttribute Set
				PricingRuleAttributeSequenceNumber = '''+ISNULL(@PricingRuleAttributeSequenceNumber,'')+''',
				PricingRuleAttributeTypeKey = '''+ISNULL(@PricingRuleAttributeTypeKey,'')+'''	-- ' + @DisplayName + '
				Where PricingRuleAttributeKey = '+CAST(@PricingRuleAttributeKey AS NVARCHAR(100))

			END
			ELSE
			BEGIN
				INSERT INTO #ScenarioData SELECT  '            ------------------------------------------------------'
				INSERT INTO #ScenarioData SELECT  '            --Your current ATKPricingRuleAttributeKey is: '+CAST(@PricingRuleAttributeKey AS NVARCHAR(100))
				INSERT INTO #ScenarioData SELECT  '            ------------------------------------------------------'
				
				INSERT INTO #ScenarioData SELECT  '            --PricingRuleAttributeSequenceNumber:'+ISNULL(@PricingRuleAttributeSequenceNumber, 'NULL')
				INSERT INTO #ScenarioData SELECT  '            --PricingRuleAttributeTypeKey:'+ISNULL(@PricingRuleAttributeTypeKey, 'NULL')+ '        Display Name:  '+@DisplayName

			END

			FETCH NEXT FROM Csr_PricingRuleAttribute INTO @PricingRuleAttributeKey, @PricingRuleAttributeSequenceNumber, @DisplayName, @PricingRuleAttributeTypeKey
		END
		CLOSE Csr_PricingRuleAttribute
		DEALLOCATE Csr_PricingRuleAttribute
	-----------------------------------------------------------------------------

	--Now, load the pricing rule bands for each pricing rule
	-----------------------------------------------------------------------------
		DECLARE PricingRuleBands CURSOR FOR
		SELECT PricingRuleBandKey, LowerBandPercent, UpperBandPercent, LowerBandQuantity, UpperBandQuantity, PercentOrQuantityIndicator
		FROM ATKPricingRuleBand
		WHERE PricingRuleKey = @PricingRuleKey
		
		OPEN PricingRuleBands
		
		FETCH NEXT FROM PricingRuleBands INTO @PricingRuleBandKey,@LowerBandPercent,@UpperBandPercent,@LowerBandQuantity,@UpperBandQuantity,@PercentOrQuantityIndicator
		
		WHILE @@Fetch_Status = 0
		BEGIN
		
			IF @Command = 'N'
			BEGIN

				INSERT INTO #ScenarioData SELECT  '          -------------------------------------'
				INSERT INTO #ScenarioData SELECT  '          --Insert new ATKPricingRuleBand Row'
				INSERT INTO #ScenarioData SELECT  '          -------------------------------------'
				SET @SQL = '
	          INSERT INTO ATKPricingRuleBand(PricingRuleKey, LowerBandPercent, UpperBandPercent, LowerBandQuantity, UpperBandQuantity, PercentOrQuantityIndicator, Version)
	          Values (@NewATKPricingRuleKey,'''+ISNULL(@LowerBandPercent,'')+''','''+ISNULL(@UpperBandPercent,'')+''','''+ISNULL(@LowerBandQuantity,'')+''','''+ISNULL(@UpperBandQuantity,'')+''','''+ISNULL(@PercentOrQuantityIndicator,'')+''',0)'
	
				INSERT INTO #ScenarioData SELECT  @SQL
			
				INSERT INTO #ScenarioData SELECT  '				Set @NewATKPricingRuleBandKey = @@Identity'
			END		
			ELSE IF @Command = 'U'
			BEGIN
				INSERT INTO #ScenarioData SELECT  '------------------------------------------------------'
				INSERT INTO #ScenarioData SELECT  '--Updating ATKPricingRuleBand: '+CAST(@PricingRuleBandKey AS NVARCHAR(100))
				INSERT INTO #ScenarioData SELECT  '------------------------------------------------------'
				INSERT INTO #ScenarioData SELECT  '	
				UPDATE ATKPricingRuleBand SET
				LowerBandPercent ='''+ISNULL(@LowerBandPercent,'')+''',
				UpperBandPercent ='''+ISNULL(@UpperBandPercent,'')+''',
				LowerBandQuantity = '''+ISNULL(@LowerBandQuantity,'')+''',
				UpperBandQuantity ='''+ISNULL(@UpperBandQuantity,'')+''',
				PercentOrQuantityIndicator = '''+ISNULL(@PercentOrQuantityIndicator,'')+'''
				Where PricingRuleBandKey = '+CAST(@PricingRuleBandKey AS NVARCHAR(100))

			END
			ELSE
			BEGIN
				INSERT INTO #ScenarioData SELECT  '            ------------------------------------------------------'
				INSERT INTO #ScenarioData SELECT  '            --Your current PricingRuleBandKey is: '+CAST(@PricingRuleBandKey AS NVARCHAR(100))
				INSERT INTO #ScenarioData SELECT  '            ------------------------------------------------------'
				INSERT INTO #ScenarioData SELECT  '            --PercentOrQuantityIndicator:   '+ISNULL(@PercentOrQuantityIndicator,'NULL')
				INSERT INTO #ScenarioData SELECT  '            --LowerBandPercent:             '+ISNULL(@LowerBandPercent,'NULL')+ '     UpperBandPercent:   '+ISNULL(@UpperBandPercent,'NULL')
				INSERT INTO #ScenarioData SELECT  '            --LowerBandQuantity:            '+ISNULL(@LowerBandQuantity,'NULL')+ '     UpperBandQuantity:  '+ISNULL(@UpperBandQuantity,'NULL')
			END
			--Now, load the PricingBandOptimalPricePercents for each PRicingRuleBand
			-----------------------------------------------------------------------------
				DECLARE Csr_PricingBandOptimalPricePercents CURSOR FOR
				SELECT PricingBandOptimalPricePercentKey, OptimalPriceMethodDesc, OptimalPricePercent, OptimalPriceType
				FROM ATKPricingBandOptimalPricePercent
				WHERE PricingRuleBandKey = @PricingRuleBandKey
				
				OPEN Csr_PricingBandOptimalPricePercents
				
				FETCH NEXT FROM Csr_PricingBandOptimalPricePercents INTO @PricingBandOptimalPricePercentKey, @OptimalPriceMethodDesc, @OptimalPricePercent, @OptimalPriceType
				WHILE @@Fetch_Status = 0
				BEGIN
				
					IF @Command = 'N'
					BEGIN

						INSERT INTO #ScenarioData SELECT  '               -------------------------------------'
						INSERT INTO #ScenarioData SELECT  '               --Insert new ATKPricingBandOptimalPricePercent Row'
						INSERT INTO #ScenarioData SELECT  '               -------------------------------------'

						SET @SQL = '
	               INSERT INTO ATKPricingBandOptimalPricePercent(PricingRuleBandKey, OptimalPriceMethodDesc, OptimalPricePercent, OptimalPriceType, Version)
	               Values (@NewATKPricingRuleBandKey,'''+ISNULL(@OptimalPriceMethodDesc,'')+''','''+ISNULL(@OptimalPricePercent,'')+''','''+ISNULL(@OptimalPriceType,'')+''',0)'
	
						INSERT INTO #ScenarioData SELECT  @SQL
					
	--					INSERT INTO #ScenarioData SELECT  '
	--					Set @NewATKPricingBandOptimalPricePercentsKey = @@Identity'
					END	
					ELSE IF @Command = 'U'
					BEGIN
						INSERT INTO #ScenarioData SELECT  '------------------------------------------------------'
						INSERT INTO #ScenarioData SELECT  '--Updating ATKPricingBandOptimalPricePercent: '+CAST(@PricingBandOptimalPricePercentKey AS NVARCHAR(100))
						INSERT INTO #ScenarioData SELECT  '------------------------------------------------------'
						INSERT INTO #ScenarioData SELECT  '	
						UPDATE ATKPricingBandOptimalPricePercent SET
						OptimalPriceMethodDesc='''+ISNULL(@OptimalPriceMethodDesc,'')+''',
						OptimalPricePercent='''+ISNULL(@OptimalPricePercent,'')+''',
						OptimalPriceType='''+ISNULL(@OptimalPriceType,'')+'''
						Where PricingBandOptimalPricePercentKey = '+CAST(@PricingBandOptimalPricePercentKey AS NVARCHAR(100))
					END
					ELSE
					BEGIN
						INSERT INTO #ScenarioData SELECT  '                  ------------------------------------------------------'
						INSERT INTO #ScenarioData SELECT  '                  --Your current ATKPricingBandOptimalPricePercentKey is: '+CAST(@PricingBandOptimalPricePercentKey AS NVARCHAR(100))
						INSERT INTO #ScenarioData SELECT  '                  ------------------------------------------------------'
						INSERT INTO #ScenarioData SELECT  '                  --OptimalPriceMethodDesc:   '+ISNULL(@OptimalPriceMethodDesc,'NULL')
						INSERT INTO #ScenarioData SELECT  '                  --OptimalPricePercent:      '+ISNULL(@OptimalPricePercent,'NULL')
						INSERT INTO #ScenarioData SELECT  '                  --OptimalPriceType:         '+ISNULL(@OptimalPriceType,'NULL')
					END
					FETCH NEXT FROM Csr_PricingBandOptimalPricePercents INTO @PricingBandOptimalPricePercentKey, @OptimalPriceMethodDesc, @OptimalPricePercent, @OptimalPriceType
				END
				CLOSE Csr_PricingBandOptimalPricePercents
				DEALLOCATE Csr_PricingBandOptimalPricePercents
		-----------------------------------------------------------------------------
			FETCH NEXT FROM PricingRuleBands INTO @PricingRuleBandKey,@LowerBandPercent,@UpperBandPercent,@LowerBandQuantity,@UpperBandQuantity,@PercentOrQuantityIndicator
		END
		CLOSE PricingRuleBands
		DEALLOCATE PricingRuleBands
	-----------------------------------------------------------------------------

	FETCH NEXT FROM PricingRules INTO @PricingRuleKey,@PricingRulePlaybookKey,@PricingRuleSequenceNumber,@MinimumPoints, @MinimumSales, @MinimumPointsType,@RulingPointThreshold,@PricingRuleWhereClause,@UseAllDataPointsForOptimalIndicator,@PricingRuleJoinClause, @DataPointReUseIndicator, @PricingRuleDescription, @HavingClause, @SectionNumber
END
CLOSE PricingRules
DEALLOCATE PricingRules

IF @Command = 'N'
BEGIN
	INSERT INTO #ScenarioData SELECT  '-------------------------------------'
	INSERT INTO #ScenarioData SELECT  '-------------------------------------'
END




GO
