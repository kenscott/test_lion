SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE [ppe].[Load_StagingFromRecommend] (@WebUserKey INT, @BatchKey INT, @RecommendKey INT) AS
BEGIN

	DECLARE @Result INT

	INSERT INTO ppe.RequestStaging
	(   CreatedByWebUserKey ,
		BatchKey ,
		AccountNumber ,
		SPGCode ,
		ProductCode ,
		NewSPGDiscount1 ,
		NewSPGDiscount2 ,
		NewExceptionDiscount ,
		NewExceptionFixedPrice ,
		NewSPGFromDate ,
		NewExceptionStartDate ,
		NewExceptionExpiryDate ,
		DeleteTerm,
		StatusRollup
	)
	SELECT 
		@WebUserKey,
		@BatchKey,
		ro1.AccountNumber,
		ro1.LLSPG,
		CASE WHEN ro1.RowType = 'LLSPG' THEN NULL ELSE ro1.ProductCode END,
		CASE WHEN ro1.RowType = 'LLSPG' AND ro1.SPGDisc2 IS NOT NULL THEN ro1.SPGDisc1 
			WHEN ro1.RowType = 'LLSPG' AND ro1.SPGDisc2 IS NULL THEN ro1.RecommendedDiscount
			ELSE ro2.SPGDisc1 END,
		CASE WHEN ro1.RowType = 'LLSPG' AND ro1.SPGDisc2 IS NOT NULL THEN ro1.RecommendedDiscount2 
			ELSE NULL END, --LPF-2009
		CASE WHEN ro1.RowType = 'LLSPG' THEN NULL WHEN ro1.[Action] = 'Delete' THEN ro1.ExceptionDiscount ELSE ro1.RecommendedDiscount END,
		CASE WHEN ro1.RowType = 'LLSPG' THEN NULL WHEN ro1.[Action] = 'Delete' THEN ro1.ExceptionFixedPrice ELSE ro1.RecommendedPrice END,
		--CASE WHEN ro1.RowType = 'LLSPG' THEN ro1.SPGFromDate ELSE NULL END,		-- LPF-6366: Create Recommendations | Effective Date not being applied to products
		ro1.SPGFromDate,
		ro1.ExceptionFromDate,
		ro1.ExceptionToDate,
		CASE WHEN ro1.[Action] = 'Delete' THEN 'Y' ELSE NULL END,
		r.StatusRollup
	FROM ppe.RecommendOutput ro1
	JOIN ppe.Recommend r on ro1.RecommendKey = r.RecommendKey
	LEFT JOIN (SELECT DISTINCT AccountKey, LLSPG, SPGDisc1
				FROM ppe.RecommendOutput
				WHERE RowType = 'LLSPG'
				AND  [ACTION] NOT IN ('Skip','Rollup','Include')
				AND NOT (Action = 'Accept' AND RowType = 'Detail' AND PriceInstructionTypeLevel = '1 - LLSPG Disc %') --LPF-1845
				--AND RecommendKey = @RecommendKey	replaced per LPF-1964
				AND BatchKey = @BatchKey
				AND ExportFlag = 1) ro2
		ON ro2.AccountKey = ro1.AccountKey
		AND ro2.LLSPG = ro1.LLSPG
	WHERE ro1.[ACTION] NOT IN ('Skip','Rollup','Include') 
	AND NOT (Action = 'Accept' AND RowType = 'Detail' AND PriceInstructionTypeLevel = '1 - LLSPG Disc %') --LPF-1845
	--AND ro1.RecommendKey = @RecommendKey		replaced per LPF-1964
	--AND ro1.ExportFlag = 1
	AND ro1.BatchKey = @BatchKey

	SELECT @Result = @@ROWCOUNT

	UPDATE ppe.RecommendOutput SET ExportFlag = NULL
	WHERE  [ACTION] NOT IN ('Skip','Rollup','Include') 
	AND NOT (Action = 'Accept' AND RowType = 'Detail' AND PriceInstructionTypeLevel = '1 - LLSPG Disc %') --LPF-1845
	--AND RecommendKey = @RecommendKey		replaced per LPF-1964
	--AND ExportFlag = 1
	AND BatchKey = @BatchKey

	/*LPF-2260*/
	UPDATE s SET SPGSkippedSales = x.SPGSkippedSales, SPGSales = x.SPGSales
	FROM ppe.RequestStaging s 
	JOIN (	SELECT SUM(CASE WHEN Action IN ('Skip','Accept') THEN TradePrice * TotalQuantity ELSE 0 END ) AS SPGSkippedSales, SUM(TradePrice * TotalQuantity) SPGSales, dig3.IG3Level1 AS SPGCode, ro.AccountNumber
			FROM ppe.RecommendOutput ro
			JOIN dbo.DimItem di ON di.ItemKey = ro.ItemKey
			JOIN dbo.DimItemGroup3 dig3 ON dig3.ItemGroup3Key = di.ItemGroup3Key
			WHERE RowType='Detail'
			AND ro.BatchKey=@BatchKey
			--AND Action IN ('Skip','Accept')
			GROUP BY dig3.IG3Level1,
			ro.AccountNumber) x
		ON x.SPGCode = s.SPGCode
		AND x.AccountNumber = s.AccountNumber
	WHERE s.BatchKey=@BatchKey
	AND s.ProductCode IS NULL


	-- all collisions now handled by trigger on RequestMain LPF-2031
	---- close colliding requests per Scott
	---- log product closings
	--INSERT INTO ppe.RequestLog
	--		( WebUserKey ,
	--		  RequestKey ,
	--		  OldStatusKey ,
	--		  NewStatusKey ,
	--		  RequestType ,
	--		  LogTime ,
	--		  RequestAction ,
	--		  EmailSent ,
	--		  OldOwnerWebUserKey ,
	--		  NewOwnerWebUserKey ,
	--		  Notes
	--		)
	--SELECT  @WebUserKey , -- WebUserKey - int
	--		r.RequestKey , -- RequestKey - int
	--		r.StatusKey , -- OldStatusKey - smallint
	--		6 , -- NewStatusKey - smallint
	--		'' , -- RequestType - varchar(50)
	--		GETDATE() , -- LogTime - datetime
	--		'Closed' , -- RequestAction - varchar(50)
	--		NULL , -- EmailSent - bit
	--		r.OwnerWebUserKey , -- OldOwnerWebUserKey - int
	--		r.OwnerWebUserKey , -- NewOwnerWebUserKey - int
	--		'Closed by create recommendations'  -- Notes - varchar(100)
	--FROM ppe.RequestStaging s
	--JOIN dbo.DimAccount da ON da.AccountNumber = s.AccountNumber
	--JOIN dbo.DimItemGroup3 dig3 ON dig3.IG3Level1 = s.SPGCode AND dig3.IG3Level4 = '1'
	--JOIN ppe.RequestMain r
	--	ON r.AccountKey = da.AccountKey
	--	AND r.ItemGroup3Key = dig3.ItemGroup3Key
	--	AND ISNULL(r.ProposedSPGDiscount1,-0.01) = CAST(CAST(ISNULL(s.NewSPGDiscount1,'-0.01') AS FLOAT) AS DEC(38,5))
	--	AND ISNULL(r.ProposedSPGDiscount2,-0.01) = CAST(CAST(ISNULL(s.NewSPGDiscount2,'-0.01') AS FLOAT) AS DEC(38,5))
	--	AND r.StatusKey IN (SELECT StatusKey FROM ppe.RequestStatus WHERE StatusRollup IN ('New','Pending'))
	--WHERE s.BatchKey=@BatchKey
	--AND s.ProductCode IS NULL	

	---- close coliding products
	--UPDATE r SET r.StatusKey = (SELECT StatusKey FROM ppe.RequestStatus WHERE StatusName = 'Closed')
	--FROM ppe.RequestStaging s
	--JOIN dbo.DimAccount da ON da.AccountNumber = s.AccountNumber
	--JOIN dbo.DimItemGroup3 dig3 ON dig3.IG3Level1 = s.SPGCode AND dig3.IG3Level4 = '1'
	--JOIN ppe.RequestMain r
	--	ON r.AccountKey = da.AccountKey
	--	AND r.ItemGroup3Key = dig3.ItemGroup3Key
	--	AND ISNULL(r.ProposedSPGDiscount1,-0.01) = CAST(CAST(ISNULL(s.NewSPGDiscount1,'-0.01') AS FLOAT) AS DEC(38,5))
	--	AND ISNULL(r.ProposedSPGDiscount2,-0.01) = CAST(CAST(ISNULL(s.NewSPGDiscount2,'-0.01') AS FLOAT) AS DEC(38,5))
	--	AND r.StatusKey IN (SELECT StatusKey FROM ppe.RequestStatus WHERE StatusRollup IN ('New','Pending'))
	--WHERE s.BatchKey=@BatchKey
	--AND s.ProductCode IS NULL

	---- log SPG closings
	--INSERT INTO ppe.RequestLog
	--		( WebUserKey ,
	--		  RequestKey ,
	--		  OldStatusKey ,
	--		  NewStatusKey ,
	--		  RequestType ,
	--		  LogTime ,
	--		  RequestAction ,
	--		  EmailSent ,
	--		  OldOwnerWebUserKey ,
	--		  NewOwnerWebUserKey ,
	--		  Notes
	--		)
	--SELECT  @WebUserKey , -- WebUserKey - int
	--		r.RequestKey , -- RequestKey - int
	--		r.StatusKey , -- OldStatusKey - smallint
	--		6 , -- NewStatusKey - smallint
	--		'' , -- RequestType - varchar(50)
	--		GETDATE() , -- LogTime - datetime
	--		'Closed' , -- RequestAction - varchar(50)
	--		NULL , -- EmailSent - bit
	--		r.OwnerWebUserKey , -- OldOwnerWebUserKey - int
	--		r.OwnerWebUserKey , -- NewOwnerWebUserKey - int
	--		'Closed by create recommendations'  -- Notes - varchar(100)
	--FROM ppe.RequestStaging s
	--JOIN dbo.DimAccount da ON da.AccountNumber = s.AccountNumber
	--JOIN dbo.DimItem di ON di.ItemNumber = '1' + s.ProductCode
	--JOIN ppe.RequestMain r
	--	ON r.AccountKey = da.AccountKey
	--	AND r.ItemKey = di.ItemKey
	--	AND ISNULL(r.ProposedExceptionDiscount, -0.01) = CAST(CAST(ISNULL(s.NewExceptionDiscount,'-0.01') AS FLOAT) AS DEC(38,5))
	--	AND ISNULL(r.ProposedExceptionFixedPrice, -0.01) = CAST(CAST(ISNULL(s.NewExceptionFixedPrice,'-0.01') AS FLOAT) AS DEC(38,5))
	--	AND ISNULL(r.ProposedExceptionStartDate, CAST('1-1-1900' AS DATE)) = CAST(ISNULL(s.NewExceptionStartDate,'1-1-1900') AS DATE )
	--	AND ISNULL(r.ProposedExceptionExpiryDate, CAST('1-1-1900' AS DATE)) = CAST(ISNULL(s.NewExceptionExpiryDate,'1-1-1900') AS DATE )
	--	AND r.StatusKey IN (SELECT StatusKey FROM ppe.RequestStatus WHERE StatusRollup IN ('New','Pending'))
	--WHERE s.BatchKey = @BatchKey
	--AND s.ProductCode IS NOT NULL

	---- close SPGs
	--UPDATE r SET r.StatusKey = (SELECT StatusKey FROM ppe.RequestStatus WHERE StatusName = 'Closed')
	--FROM ppe.RequestStaging s
	--JOIN dbo.DimAccount da ON da.AccountNumber = s.AccountNumber
	--JOIN dbo.DimItem di ON di.ItemNumber = '1' + s.ProductCode
	--JOIN ppe.RequestMain r
	--	ON r.AccountKey = da.AccountKey
	--	AND r.ItemKey = di.ItemKey
	--	AND ISNULL(r.ProposedExceptionDiscount, -0.01) = CAST(CAST(ISNULL(s.NewExceptionDiscount,'-0.01') AS FLOAT) AS DEC(38,5))
	--	AND ISNULL(r.ProposedExceptionFixedPrice, -0.01) = CAST(CAST(ISNULL(s.NewExceptionFixedPrice,'-0.01') AS FLOAT) AS DEC(38,5))
	--	AND ISNULL(r.ProposedExceptionStartDate, CAST('1-1-1900' AS DATE)) = CAST(ISNULL(s.NewExceptionStartDate,'1-1-1900') AS DATE )
	--	AND ISNULL(r.ProposedExceptionExpiryDate, CAST('1-1-1900' AS DATE)) = CAST(ISNULL(s.NewExceptionExpiryDate,'1-1-1900') AS DATE )
	--	AND r.StatusKey IN (SELECT StatusKey FROM ppe.RequestStatus WHERE StatusRollup IN ('New','Pending'))
	--WHERE s.BatchKey = @BatchKey
	--AND s.ProductCode IS NOT NULL

	SELECT @Result AS Result

END
GO
