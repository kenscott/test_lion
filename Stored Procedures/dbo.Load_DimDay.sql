SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE Procedure
[dbo].[Load_DimDay]
as

declare @a datetime
declare @x int
declare @MonthKey Key_Small_Type
set @x = 1

set nocount on

exec DBA_IndexRebuild 'DimDay', 'Drop'





truncate table DimDay

If not exists (Select * from DimDay where SQLDate is null)
begin
	Set @MonthKey = (Select MonthKey from DimMonth where FirstSQLDate is null)

	INSERT INTO DimDay
	(MonthKey,
	SQLDate	,
	CalendarDay	,
	CalendarDayNumberinMonth	,
	CalendarDayNumberinQuarter	,
	CalendarDayNumberinWeek	,
	CalendarDayNumberinYear
-- 	CalendarWeek	,
-- 	CalendarWeekEndingDate	,
-- 	CalendarWeekNumber	,
-- 	DayinWeek	,
-- 	DayNumber	,
-- 	FiscalDay	,
-- 	FiscalDayNumberinMonth	,
-- 	FiscalDayNumberinQuarter	,
-- 	FiscalDayNumberinWeek	,
-- 	FiscalDayNumberinYear	,
-- 	FiscalWeek	,
-- 	FiscalWeekEndingDate	,
-- 	FiscalWeekNumber	,
-- 	FiscalWeekNumberinYear	,
-- 	Holiday	,
-- 	HolidayIndicator	,
-- 	LastDayinCalendarMonthIndicator	,
-- 	LastDayinCalendarQuarterIndicator	,
-- 	LastDayinCalendarWeekIndicator	,
-- 	LastDayinCalendarYearIndicator	,
-- 	LastDayinFiscalMonthIndicator	,
-- 	LastDayinFiscalQuarterIndicator	,
-- 	LastDayinFiscalWeekIndicator	,
-- 	LongCalendarDay	,
-- 	MajorEvent	,
-- 	SellingSeason	,
-- 	WeekdayIndicator	,
--	CognosParameterDate
)
	Values (
	@MonthKey,
	NULL, 
	'Unknown',-- CalendarDay
	null,--CalendarDayNumberinMonth,
	null, --CalendarDayNumberinQuarter
	null,--CalendarDayNumberinWeek,
	null--CalendarDayNumberinYear,
	)

end


set @a = '2000-01-01'
while @a < '2050-01-01'

begin
	Set @MonthKey = (Select MonthKey from DimMonth where @A between FirstSQLDate and LastSQLDate)

	INSERT INTO DimDay
	(MonthKey,
	SQLDate	,
	CalendarDay	,
	CalendarDayNumberinMonth	,
	CalendarDayNumberinQuarter	,
	CalendarDayNumberinWeek	,
	CalendarDayNumberinYear
-- 	CalendarWeek	,
-- 	CalendarWeekEndingDate	,
-- 	CalendarWeekNumber	,
-- 	DayinWeek	,
-- 	DayNumber	,
-- 	FiscalDay	,
-- 	FiscalDayNumberinMonth	,
-- 	FiscalDayNumberinQuarter	,
-- 	FiscalDayNumberinWeek	,
-- 	FiscalDayNumberinYear	,
-- 	FiscalWeek	,
-- 	FiscalWeekEndingDate	,
-- 	FiscalWeekNumber	,
-- 	FiscalWeekNumberinYear	,
-- 	Holiday	,
-- 	HolidayIndicator	,
-- 	LastDayinCalendarMonthIndicator	,
-- 	LastDayinCalendarQuarterIndicator	,
-- 	LastDayinCalendarWeekIndicator	,
-- 	LastDayinCalendarYearIndicator	,
-- 	LastDayinFiscalMonthIndicator	,
-- 	LastDayinFiscalQuarterIndicator	,
-- 	LastDayinFiscalWeekIndicator	,
-- 	LongCalendarDay	,
-- 	MajorEvent	,
-- 	SellingSeason	,
-- 	WeekdayIndicator	,
--	CognosParameterDate
)
	Values (
	@MonthKey,
	@a, 
	Case DatePart(Dw,@a)
		When 1 then 'Sunday'
		When 2 then 'Monday'
		When 3 then 'Tuesday'
		When 4 then 'Wednesday'
		When 5 then 'Thursday'
		When 6 then 'Friday'
		When 7 then 'Saturday'
	End,-- CalendarDay
	DatePart(Day,@a),--CalendarDayNumberinMonth,
	null, --CalendarDayNumberinQuarter
	DatePart(dw,@a),--CalendarDayNumberinWeek,
	DatePart(dy,@a)--CalendarDayNumberinYear,
	)

	set @a = DateAdd(Day,1,@a)
end

If not exists (Select * from DimDay where calendarday = 'Future Day')
begin
	Set @MonthKey = (Select MonthKey from DimMonth where CalendarMonth = 'Future Month')

	INSERT INTO DimDay
	(MonthKey,
	SQLDate	,
	CalendarDay	,
	CalendarDayNumberinMonth	,
	CalendarDayNumberinQuarter	,
	CalendarDayNumberinWeek	,
	CalendarDayNumberinYear
-- 	CalendarWeek	,
-- 	CalendarWeekEndingDate	,
-- 	CalendarWeekNumber	,
-- 	DayinWeek	,
-- 	DayNumber	,
-- 	FiscalDay	,
-- 	FiscalDayNumberinMonth	,
-- 	FiscalDayNumberinQuarter	,
-- 	FiscalDayNumberinWeek	,
-- 	FiscalDayNumberinYear	,
-- 	FiscalWeek	,
-- 	FiscalWeekEndingDate	,
-- 	FiscalWeekNumber	,
-- 	FiscalWeekNumberinYear	,
-- 	Holiday	,
-- 	HolidayIndicator	,
-- 	LastDayinCalendarMonthIndicator	,
-- 	LastDayinCalendarQuarterIndicator	,
-- 	LastDayinCalendarWeekIndicator	,
-- 	LastDayinCalendarYearIndicator	,
-- 	LastDayinFiscalMonthIndicator	,
-- 	LastDayinFiscalQuarterIndicator	,
-- 	LastDayinFiscalWeekIndicator	,
-- 	LongCalendarDay	,
-- 	MajorEvent	,
-- 	SellingSeason	,
-- 	WeekdayIndicator	,
--	CognosParameterDate
)
	Values (
	@MonthKey,
	'2050-01-01', 
	'Future Day',-- CalendarDay
	null,--CalendarDayNumberinMonth,
	null, --CalendarDayNumberinQuarter
	null,--CalendarDayNumberinWeek,
	null--CalendarDayNumberinYear,
	)

end




exec DBA_IndexRebuild 'DimDay', 'Create'
--   ALTER TABLE DimDay
--    ADD CONSTRAINT [FK_DimDay_1] FOREIGN KEY 
--   	(MonthKey) REFERENCES DimMonth(MonthKey)

select top 10 * From DimDay





GO
