SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE PROCEDURE [sup].[GetSpInvoiceInfo]
    @claimMonth INT,
    @vendorKey INT
AS


SET NOCOUNT ON;
SET ANSI_WARNINGS OFF; 


/*TESTINg
DECLARE @claimMonth INT
DECLARE @vendorKey INT
SET @claimMonth = 201712
SET @vendorKey=1348
*/

--DETERMINE IF @claimMonth is "Current month", which is pased on any month with "P" or "R" claim status
DECLARE @isCurrent INTEGER;
SET @isCurrent = 0;

SELECT @isCurrent = ClaimMonth
FROM cm.SupplierClaim
WHERE (ClaimStatus IN ( 'P', 'R' ))
      AND VendorKey = @vendorKey
      AND ClaimMonth = @claimMonth;


DECLARE @currentMonth BIT;
IF @isCurrent = @claimMonth
    SET @currentMonth = 1;
ELSE
    SET @currentMonth = 0;

IF @currentMonth = 1
BEGIN
    WITH CTE_COUNT (ClaimMonth, supplierRef, ClaimLines, InvoiceAmount)
    AS (SELECT cj.ClaimMonth,
               cj.SupplierRef,
               COUNT(cj.InvoiceClaimKey) AS ClaimLines,
               SUM(cj.ClaimLineValue) AS InvoiceAmount
        FROM cm.ClaimJournal cj
        GROUP BY cj.ClaimMonth,
                 cj.ClaimJournalStatus,
                 cj.SupplierRef,
                 cj.VendorKey
        HAVING cj.ClaimMonth = @claimMonth
               AND cj.VendorKey = @vendorKey
               AND
               
                   cj.ClaimJournalStatus IN ('P','C')
		 )
    SELECT NEWID() AS GUID,
           cj.ClaimMonth AS Month,
           cj.VendorKey,
           ic.InvoiceClaimKey InvoiceNumber,
           cj.SupplierRef AS ContractID,
           cj.CurrentContractID AS WolseleyContractID,
           cte.ClaimLines,
           cte.InvoiceAmount,
		   ic.CreationDate AS ClaimDate
    FROM cm.ClaimJournal cj
        LEFT JOIN cm.InvoiceClaim ic
            ON cj.InvoiceClaimKey = ic.InvoiceClaimKey
        LEFT JOIN CTE_COUNT cte
            ON cj.ClaimMonth = cte.ClaimMonth
               AND ISNULL(cj.SupplierRef,0) = ISNULL(cte.supplierRef,0)
	WHERE cj.ClaimJournalStatus IN ('P','C')
    GROUP BY cj.SupplierRef,
             cj.ClaimMonth,
             cj.InvoiceClaimKey,
             ic.InvoiceClaimKey,
             cj.CurrentContractID,
             cj.VendorKey,
             cte.ClaimLines,
             cte.InvoiceAmount,
			 ic.CreationDate
    HAVING cj.ClaimMonth = @claimMonth
           AND cj.VendorKey = @vendorKey;
END;

/* claim date notes
Comes from ClaimLog..  claim action = Process Supplier Claim 
AND supplierClaimKey = 


*/





IF @currentMonth = 0
BEGIN
    WITH CTE_COUNT (ClaimMonth, supplierRef, ClaimLines, InvoiceAmount)
    AS (SELECT cj.ClaimMonth,
               cj.SupplierRef,
               COUNT(cj.InvoiceClaimKey) AS ClaimLines,
               SUM(cj.ClaimLineValue) AS InvoiceAmount
        FROM cm.ClaimJournal cj
        GROUP BY cj.ClaimMonth,
                 cj.ClaimJournalStatus,
                 cj.SupplierRef,
                 cj.VendorKey
        HAVING cj.ClaimMonth = @claimMonth
               AND cj.VendorKey = @vendorKey
               AND cj.ClaimJournalStatus = 'C')
    SELECT NEWID() AS GUID,
           cj.ClaimMonth AS Month,
           cj.VendorKey,
           ic.InvoiceClaimKey InvoiceNumber,
           cj.SupplierRef AS ContractID,
           cj.CurrentContractID AS WolseleyContractID,
           cte.ClaimLines,
           cte.InvoiceAmount,
		   ic.CreationDate AS ClaimDate

    FROM cm.ClaimJournal cj
        LEFT JOIN cm.InvoiceClaim ic
            ON cj.InvoiceClaimKey = ic.InvoiceClaimKey
        LEFT JOIN CTE_COUNT cte
            ON cj.ClaimMonth = cte.ClaimMonth AND
              ISNULL(cj.SupplierRef,0) = ISNULL(cte.supplierRef,0)
    GROUP BY cj.SupplierRef,
             cj.ClaimMonth,
             cj.InvoiceClaimKey,
             ic.InvoiceClaimKey,
             cj.VendorKey,
             cj.CurrentContractID,
             cte.ClaimLines,
             cte.InvoiceAmount,
			 ic.CreationDate
    HAVING cj.ClaimMonth = @claimMonth
           AND cj.VendorKey = @vendorKey;
END;
GO
