SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [cm].[CalculateCurrentMonthTallyAutoReject]
	@BatchKey INT = NULL

AS

--IF @BatchKey is populated then this is being run from cm.ProcessClaims (Execute Claims)


-------Rejected Claim Line Tally Reset-------------------------------------------- 
UPDATE sci
SET CurrentMonthTallyOld = NULL,
	CurrentMonthTally = NULL,
	LimitPreviousRatio = NULL,
	LimitCurrentRatio = NULL 
FROM cc.SupplierContractItem sci
WHERE ContractItemKey IN (
SELECT DISTINCT sci.ContractItemKey
FROM cm.ClaimJournal cj
INNER JOIN cc.SupplierContract sc
	ON sc.ContractID = cj.CurrentContractID
INNER JOIN cc.SupplierContractItem sci
	ON sci.ContractKey = sc.ContractKey
INNER JOIN cm.FactInvoiceLine fil
	ON fil.ClientInvoiceLineUniqueID = cj.ClientInvoiceLineUniqueID
	AND fil.ItemKey = sci.ItemKey
LEFT JOIN cm.SupplierClaim c
	ON c.ClaimMonth = cj.ClaimMonth
	AND c.VendorKey = cj.VendorKey
	AND c.BatchKey = @BatchKey 
	AND @BatchKey IS NOT NULL
WHERE cj.ClaimJournalStatus = 'X'  --Rejected
	AND sc.StatusKey IN (4, 6)  --Pending or Approved
	AND ((@BatchKey IS NOT NULL AND c.SupplierClaimKey IS NOT NULL) OR @BatchKey IS NULL)
)

UPDATE scig
SET CurrentMonthTallyOld = NULL,
	CurrentMonthTally = NULL,
	LimitPreviousRatio = NULL,
	LimitCurrentRatio = NULL 
FROM cc.SupplierContractItemGroupX scig
WHERE ContractItemGroupXKey IN (
SELECT DISTINCT sci.ContractItemGroupXKey
FROM cm.ClaimJournal cj
INNER JOIN cc.SupplierContract sc
	ON sc.ContractID = cj.CurrentContractID
INNER JOIN cc.SupplierContractItemGroupX sci
	ON sci.ContractKey = sc.ContractKey
LEFT JOIN dbo.DimItem di1
	ON di1.ItemGroup1Key = sci.ItemGroupXKey
	AND sci.ItemGroupLevel = 1
LEFT JOIN dbo.DimItem di3
	ON di3.ItemGroup3Key = sci.ItemGroupXKey
	AND sci.ItemGroupLevel = 3
INNER JOIN cm.FactInvoiceLine fil
	ON fil.ClientInvoiceLineUniqueID = cj.ClientInvoiceLineUniqueID
	AND fil.ItemKey = ISNULL(di1.ItemKey, di3.ItemKey)
LEFT JOIN cm.SupplierClaim c
	ON c.ClaimMonth = cj.ClaimMonth
	AND c.VendorKey = cj.VendorKey
	AND c.BatchKey = @BatchKey 
	AND @BatchKey IS NOT NULL
WHERE cj.ClaimJournalStatus = 'X'  --Rejected
	AND sc.StatusKey IN (4, 6)  --Pending or Approved
	AND ((@BatchKey IS NOT NULL AND c.SupplierClaimKey IS NOT NULL) OR @BatchKey IS NULL)
)
------------------------------------------------------------------------------------

--Item Quantity Limit
SELECT sci.ContractItemKey, SUM(fil.Qty) AS Tally
INTO #tallyCount
FROM cm.ClaimJournal cj
INNER JOIN cc.SupplierContract sc
	ON sc.ContractID = cj.CurrentContractID
INNER JOIN cc.SupplierContractItem sci
	ON sci.ContractKey = sc.ContractKey
INNER JOIN cm.FactInvoiceLine fil
	ON fil.ClientInvoiceLineUniqueID = cj.ClientInvoiceLineUniqueID
	AND fil.ItemKey = sci.ItemKey
LEFT JOIN cm.SupplierClaim c
	ON c.ClaimMonth = cj.ClaimMonth
	AND c.VendorKey = cj.VendorKey
	AND c.BatchKey = @BatchKey 
	AND @BatchKey IS NOT NULL
WHERE cj.ClaimJournalStatus = 'P'  --Pending 
	AND sc.StatusKey IN (4, 6)  --Pending or Approved
	AND sci.LimitType = 'U'
	AND ((@BatchKey IS NOT NULL AND c.SupplierClaimKey IS NOT NULL) OR @BatchKey IS NULL)
GROUP BY sci.ContractItemKey

UNION

--Item Sales Limit
SELECT sci.ContractItemKey, SUM(fil.NettPrice) AS Tally
FROM cm.ClaimJournal cj
INNER JOIN cc.SupplierContract sc
	ON sc.ContractID = cj.CurrentContractID
INNER JOIN cc.SupplierContractItem sci
	ON sci.ContractKey = sc.ContractKey
INNER JOIN cm.FactInvoiceLine fil
	ON fil.ClientInvoiceLineUniqueID = cj.ClientInvoiceLineUniqueID
	AND fil.ItemKey = sci.ItemKey
LEFT JOIN cm.SupplierClaim c
	ON c.ClaimMonth = cj.ClaimMonth
	AND c.VendorKey = cj.VendorKey
	AND c.BatchKey = @BatchKey 
	AND @BatchKey IS NOT NULL
WHERE cj.ClaimJournalStatus = 'P'  --Pending 
	AND sc.StatusKey IN (4, 6)  --Pending or Approved
	AND sci.LimitType = 'S'
	AND ((@BatchKey IS NOT NULL AND c.SupplierClaimKey IS NOT NULL) OR @BatchKey IS NULL)
GROUP BY sci.ContractItemKey

UNION

--Item Claims Limit
SELECT sci.ContractItemKey, SUM(cj.ClaimLineValue) AS Tally
FROM cm.ClaimJournal cj
INNER JOIN cc.SupplierContract sc
	ON sc.ContractID = cj.CurrentContractID
INNER JOIN cc.SupplierContractItem sci
	ON sci.ContractKey = sc.ContractKey
INNER JOIN cm.FactInvoiceLine fil
	ON fil.ClientInvoiceLineUniqueID = cj.ClientInvoiceLineUniqueID
	AND fil.ItemKey = sci.ItemKey
LEFT JOIN cm.SupplierClaim c
	ON c.ClaimMonth = cj.ClaimMonth
	AND c.VendorKey = cj.VendorKey
	AND c.BatchKey = @BatchKey 
	AND @BatchKey IS NOT NULL
WHERE cj.ClaimJournalStatus = 'P'  --Pending 
	AND sc.StatusKey IN (4, 6)  --Pending or Approved
	AND sci.LimitType = 'C'
	AND ((@BatchKey IS NOT NULL AND c.SupplierClaimKey IS NOT NULL) OR @BatchKey IS NULL)
GROUP BY sci.ContractItemKey

--Update Current Month Tally
UPDATE sci
SET CurrentMonthTallyOld = sci.CurrentMonthTally,
	CurrentMonthTally = t.Tally,
	LimitPreviousRatio = sci.LimitCurrentRatio,
	LimitCurrentRatio = ((t.Tally + ISNULL(sci.PreviousMonthTally, 0)) / sci.LimitValue) * 100
FROM cc.SupplierContractItem sci
INNER JOIN #tallyCount t
	ON t.ContractItemKey = sci.ContractItemKey


---------Item Groups (MPG/SPG)-------------------------------
--Item Group Quantity Limit
SELECT sci.ContractItemGroupXKey, SUM(fil.Qty) AS Tally
INTO #tallyCountGroup
FROM cm.ClaimJournal cj
INNER JOIN cc.SupplierContract sc
	ON sc.ContractID = cj.CurrentContractID
INNER JOIN cc.SupplierContractItemGroupX sci
	ON sci.ContractKey = sc.ContractKey
LEFT JOIN dbo.DimItem di1
	ON di1.ItemGroup1Key = sci.ItemGroupXKey
	AND sci.ItemGroupLevel = 1
LEFT JOIN dbo.DimItem di3
	ON di3.ItemGroup3Key = sci.ItemGroupXKey
	AND sci.ItemGroupLevel = 3
INNER JOIN cm.FactInvoiceLine fil
	ON fil.ClientInvoiceLineUniqueID = cj.ClientInvoiceLineUniqueID
	AND fil.ItemKey = ISNULL(di1.ItemKey, di3.ItemKey)
LEFT JOIN cm.SupplierClaim c
	ON c.ClaimMonth = cj.ClaimMonth
	AND c.VendorKey = cj.VendorKey
	AND c.BatchKey = @BatchKey 
	AND @BatchKey IS NOT NULL
WHERE cj.ClaimJournalStatus = 'P'  --Pending 
	AND sc.StatusKey IN (4, 6)  --Pending or Approved
	AND sci.LimitType = 'U'
	AND ((@BatchKey IS NOT NULL AND c.SupplierClaimKey IS NOT NULL) OR @BatchKey IS NULL)
GROUP BY sci.ContractItemGroupXKey

UNION

--Item Group Sales Limit
SELECT sci.ContractItemGroupXKey, SUM(fil.NettPrice) AS Tally
FROM cm.ClaimJournal cj
INNER JOIN cc.SupplierContract sc
	ON sc.ContractID = cj.CurrentContractID
INNER JOIN cc.SupplierContractItemGroupX sci
	ON sci.ContractKey = sc.ContractKey
LEFT JOIN dbo.DimItem di1
	ON di1.ItemGroup1Key = sci.ItemGroupXKey
	AND sci.ItemGroupLevel = 1
LEFT JOIN dbo.DimItem di3
	ON di3.ItemGroup3Key = sci.ItemGroupXKey
	AND sci.ItemGroupLevel = 3
INNER JOIN cm.FactInvoiceLine fil
	ON fil.ClientInvoiceLineUniqueID = cj.ClientInvoiceLineUniqueID
	AND fil.ItemKey = ISNULL(di1.ItemKey, di3.ItemKey)
LEFT JOIN cm.SupplierClaim c
	ON c.ClaimMonth = cj.ClaimMonth
	AND c.VendorKey = cj.VendorKey
	AND c.BatchKey = @BatchKey 
	AND @BatchKey IS NOT NULL
WHERE cj.ClaimJournalStatus = 'P'  --Pending 
	AND sc.StatusKey IN (4, 6)  --Pending or Approved
	AND sci.LimitType = 'S'
	AND ((@BatchKey IS NOT NULL AND c.SupplierClaimKey IS NOT NULL) OR @BatchKey IS NULL)
GROUP BY sci.ContractItemGroupXKey

UNION

--Item Group Claims Limit
SELECT sci.ContractItemGroupXKey, SUM(cj.ClaimLineValue) AS Tally
FROM cm.ClaimJournal cj
INNER JOIN cc.SupplierContract sc
	ON sc.ContractID = cj.CurrentContractID
INNER JOIN cc.SupplierContractItemGroupX sci
	ON sci.ContractKey = sc.ContractKey
LEFT JOIN dbo.DimItem di1
	ON di1.ItemGroup1Key = sci.ItemGroupXKey
	AND sci.ItemGroupLevel = 1
LEFT JOIN dbo.DimItem di3
	ON di3.ItemGroup3Key = sci.ItemGroupXKey
	AND sci.ItemGroupLevel = 3
INNER JOIN cm.FactInvoiceLine fil
	ON fil.ClientInvoiceLineUniqueID = cj.ClientInvoiceLineUniqueID
	AND fil.ItemKey = ISNULL(di1.ItemKey, di3.ItemKey)
LEFT JOIN cm.SupplierClaim c
	ON c.ClaimMonth = cj.ClaimMonth
	AND c.VendorKey = cj.VendorKey
	AND c.BatchKey = @BatchKey 
	AND @BatchKey IS NOT NULL
WHERE cj.ClaimJournalStatus = 'P'  --Pending 
	AND sc.StatusKey IN (4, 6)  --Pending or Approved
	AND sci.LimitType = 'C'
	AND ((@BatchKey IS NOT NULL AND c.SupplierClaimKey IS NOT NULL) OR @BatchKey IS NULL)
GROUP BY sci.ContractItemGroupXKey

--Update Current Month Tally
UPDATE sci
SET CurrentMonthTallyOld = sci.CurrentMonthTally,
	CurrentMonthTally = t.Tally,
	LimitPreviousRatio = sci.LimitCurrentRatio,
	LimitCurrentRatio = ((t.Tally + ISNULL(sci.PreviousMonthTally, 0)) / sci.LimitValue) * 100
FROM cc.SupplierContractItemGroupX sci
INNER JOIN #tallyCountGroup t
	ON t.ContractItemGroupXKey = sci.ContractItemGroupXKey

IF @BatchKey IS NULL --only set email once a day
BEGIN
--Approaching limit email
TRUNCATE TABLE cm.ApproachingLimitEmail

INSERT INTO cm.ApproachingLimitEmail
	([ContractItemKey],
	[CreationDate])
SELECT ContractItemKey, GETDATE()
FROM cc.SupplierContractItem sci
WHERE ISNULL(LimitPreviousRatio, 0) < 90.0
	and LimitCurrentRatio >= 90.0

INSERT INTO cm.ApproachingLimitEmail
	([ContractItemGroupXKey],
	[CreationDate])
SELECT ContractItemGroupXKey, GETDATE()
FROM cc.SupplierContractItemGroupX 
WHERE ISNULL(LimitPreviousRatio, 0) < 90.0
	and LimitCurrentRatio >= 90.0

END
GO
