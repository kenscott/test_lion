SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE PROCEDURE [cg].[GetPriceRules]
AS

DECLARE
	@ScenarioKey Key_Normal_type,
	@PlaybookDataPointGroupKey Key_Normal_type,
	@ProjectKey Key_Normal_type

SELECT 
	@ScenarioKey = ScenarioKey, 
	@PlaybookDataPointGroupKey = PlaybookDataPointGroupKey, 
	@ProjectKey = ProjectKey
FROM dbo.fn_GetScenarioKeys (12)	-- 12 is the approach type key


SELECT DISTINCT
	p.*
FROM dbo.PlaybookPricingGroup (NOLOCK) p
WHERE
	p.PlaybookDataPointGroupKey = @PlaybookDataPointGroupKey
	AND p.GroupValues IS NOT NULL
ORDER BY 1



GO
