SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





CREATE FUNCTION [lion].[fn_GetRPBPercentages]
(
	@PlaybookPricingGroupKey Key_Normal_type,
	@Total12MonthQuantity Quantity_Normal_type,
	@FloorPercentile Percent_type,
	@TargetPercentile Percent_type,
	@StretchPercentile Percent_type
)
RETURNS TABLE
AS
RETURN

/*
SELECT * FROM lion.fn_GetRPBPercentages(27442, 2, 0, 0, 0)
*/


SELECT
TOP 1

	TheFloor.PricingRuleSequenceNumber,
	--TheFloor.GroupValues,
	--TheFloor.GroupingColumns,
	TheFloor.PDPGPP AS FloorGPP,
	TheTarget.PDPGPP AS TargetGPP,	
	TheStretch.PDPGPP AS StretchGPP,

	TheFloor.PDPDiscountPercent AS FloorDiscount,
	TheTarget.PDPDiscountPercent AS TargetDiscount,
	TheStretch.PDPDiscountPercent AS StretchDiscount

FROM lion.PlaybookPricingGroupPriceBandActive TheFloor



--FROM dbo.PlaybookPricingGroupPriceBand TheFloor (NOLOCK)
--INNER JOIN dbo.PlaybookPricingGroup ppg (NOLOCK)
--	ON ppg.PlaybookPricingGroupKey = TheFloor.PlaybookPricingGroupKey


INNER JOIN lion.PlaybookPricingGroupPriceBandActive TheTarget (NOLOCK)
	ON TheTarget.PlaybookPricingGroupKey = @PlaybookPricingGroupKey
	AND @Total12MonthQuantity BETWEEN TheTarget.LowerBandQuantity AND TheTarget.UpperBandQuantity 
	AND TheTarget.MemberRank = 
		CASE
			WHEN  ROUND ( TheTarget.RulingMemberCount * @TargetPercentile, 0) BETWEEN 1 AND TheTarget.RulingMemberCount THEN  ROUND ( TheTarget.RulingMemberCount * @TargetPercentile, 0)
			ELSE 1
		END
INNER JOIN lion.PlaybookPricingGroupPriceBandActive TheStretch (NOLOCK)
	ON TheStretch.PlaybookPricingGroupKey = @PlaybookPricingGroupKey
	AND @Total12MonthQuantity BETWEEN TheStretch.LowerBandQuantity AND TheStretch.UpperBandQuantity 
	AND TheStretch.MemberRank = 
		CASE
			WHEN  ROUND ( TheStretch.RulingMemberCount * @StretchPercentile, 0) BETWEEN 1 AND TheStretch.RulingMemberCount THEN  ROUND ( TheStretch.RulingMemberCount * @StretchPercentile, 0)
			ELSE 1
		END
WHERE
	TheFloor.PlaybookPricingGroupKey = @PlaybookPricingGroupKey
	AND @Total12MonthQuantity BETWEEN TheFloor.LowerBandQuantity AND TheFloor.UpperBandQuantity 
	AND TheFloor.MemberRank = 
		CASE
			WHEN  ROUND ( TheFloor.RulingMemberCount * @FloorPercentile, 0) BETWEEN 1 AND TheFloor.RulingMemberCount THEN  ROUND ( TheFloor.RulingMemberCount * @FloorPercentile, 0)
			ELSE 1
		END



GO
