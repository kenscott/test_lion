SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO






CREATE FUNCTION [lion].[fn_GetRPBPriceBandAttributes]
(
	@ParamGroupValues NVARCHAR(4000),
	@ParamGroupingColumns NVARCHAR(4000)
)
RETURNS @ListTable TABLE 
(
	
	PlaybookRegion NVARCHAR(900),
	CustomerBrandSegment NVARCHAR(900),
	CustomerSegment NVARCHAR(900),
	Sales603010Bucket NVARCHAR(900),
	StrategicItem NVARCHAR(900),
	PriceApproach NVARCHAR(900),
	Item NVARCHAR(900),
	LLSPG NVARCHAR(900),
	HLSPG NVARCHAR(900),
	GSPG NVARCHAR(900),
	ContractClaimsIndicator NVARCHAR(900),
	ShipmentType NVARCHAR(900),
	PlaybookProductBrand NVARCHAR(900),
	PyramidCode NVARCHAR(900),
	PlaybookBrandIndicator NVARCHAR(900),
	RegionExceptionIndicator NVARCHAR(900),
	CustomerSize NVARCHAR(900),
	CustomerSpecialism NVARCHAR(900)
)
AS

BEGIN

DECLARE
	@CustomerSpecialism NVARCHAR(900),
	@CustomerSize NVARCHAR(900),
	@CustomerBrandSegment NVARCHAR(900),
	@PlaybookBrandIndicator NVARCHAR(900),
	@RegionExceptionIndicator NVARCHAR(900),
	@PlaybookRegion NVARCHAR(900),
	@CustomerSegment NVARCHAR(900),
	@Sales603010Bucket NVARCHAR(900),
	@Item NVARCHAR(900),
	@StrategicItem NVARCHAR(900),
	@LLSPG NVARCHAR(900),
	@HLSPG NVARCHAR(900),
	@GSPG NVARCHAR(900),
	@PriceApproach NVARCHAR(900),
	@ContractClaimsIndicator NVARCHAR(900),
	@ShipmentType NVARCHAR(900),
	@PlaybookProductBrand NVARCHAR(900),
	@PyramidCode NVARCHAR(900),
	@CurPos INT, 
	@OldPos INT,
	@CurPosGroupingColumn INT, 
	@OldPosGroupingColumn INT,
	@TokenNbr INT,
	@GroupingColumn NVARCHAR(MAX)

	IF ISNULL(LTRIM(RTRIM(@ParamGroupValues)), N'') = N''
		RETURN

	SELECT 	
		@OldPos = 1,
		@CurPos = CHARINDEX(N',', @ParamGroupValues, @OldPos),

		@OldPosGroupingColumn = 1,
		@CurPosGroupingColumn = CHARINDEX(N',', @ParamGroupingColumns, @OldPosGroupingColumn),

		@TokenNbr = 1,
		@CustomerSpecialism  = N'ALL',
		@CustomerSize = N'ALL',
		@CustomerBrandSegment = N'ALL',
		@PlaybookBrandIndicator = N'ALL',
		@RegionExceptionIndicator = N'ALL',
		@PlaybookRegion = N'ALL',
		@CustomerSegment = N'ALL',
		@Sales603010Bucket = N'ALL',
		@Item = N'ALL',
		@StrategicItem = N'ALL',
		@LLSPG = N'ALL',
		@HLSPG = N'ALL',
		@GSPG = N'ALL',
		@PriceApproach = N'ALL',
		@ContractClaimsIndicator = N'ALL',
		@ShipmentType = N'ALL',
		@PlaybookProductBrand = N'ALL',
		@PyramidCode = N'ALL'

	WHILE @CurPos >= 1 
	BEGIN

		SELECT @GroupingColumn = SUBSTRING(@ParamGroupingColumns, @OldPosGroupingColumn, @CurPosGroupingColumn-@OldPosGroupingColumn)
		
		IF @GroupingColumn = N'DimItem.ItemUDVarChar1'								SELECT @PyramidCode = SUBSTRING(@ParamGroupValues, @OldPos, @CurPos-@OldPos)
		IF @GroupingColumn = N'DimItem.ItemUDVarChar21'								SELECT @PlaybookProductBrand = SUBSTRING(@ParamGroupValues, @OldPos, @CurPos-@OldPos)
		IF @GroupingColumn = N'DimInvoiceLineGroup1.InvoiceLineGroup1UDVarchar1'	SELECT @ShipmentType = SUBSTRING(@ParamGroupValues, @OldPos, @CurPos-@OldPos)
		IF @GroupingColumn = N'DimInvoiceLineGroup1.InvoiceLineGroup1UDVarchar2'	SELECT @ContractClaimsIndicator = SUBSTRING(@ParamGroupValues, @OldPos, @CurPos-@OldPos)
		IF @GroupingColumn = N'FactLastPriceAndCost.UDVarchar10'					SELECT @PriceApproach = SUBSTRING(@ParamGroupValues, @OldPos, @CurPos-@OldPos)
		IF @GroupingColumn = N'DimItemGroup3.IG3Level3'								SELECT @GSPG = SUBSTRING(@ParamGroupValues, @OldPos, @CurPos-@OldPos)
		IF @GroupingColumn = N'DimItemGroup3.IG3Level2'								SELECT @HLSPG = SUBSTRING(@ParamGroupValues, @OldPos, @CurPos-@OldPos)
		IF @GroupingColumn = N'DimItemGroup3.IG3Level1'								SELECT @LLSPG = SUBSTRING(@ParamGroupValues, @OldPos, @CurPos-@OldPos)
		IF @GroupingColumn = N'FactLastPriceAndCost.UDVarchar1'						SELECT @StrategicItem = SUBSTRING(@ParamGroupValues, @OldPos, @CurPos-@OldPos)
		IF @GroupingColumn = N'DimItem.ItemNumber'									SELECT @Item = SUBSTRING(@ParamGroupValues, @OldPos, @CurPos-@OldPos)
		IF @GroupingColumn = N'FactLastPriceAndCost.UDVarchar4'						SELECT @Sales603010Bucket = SUBSTRING(@ParamGroupValues, @OldPos, @CurPos-@OldPos)
		IF @GroupingColumn = N'DimAccount.AccountUDVarChar10'						SELECT @CustomerSegment = SUBSTRING(@ParamGroupValues, @OldPos, @CurPos-@OldPos)
		IF @GroupingColumn = N'DimAccountGroup1.AG1Level1UDVarChar2'				SELECT @PlaybookRegion = SUBSTRING(@ParamGroupValues, @OldPos, @CurPos-@OldPos)

		IF @GroupingColumn = N'DimAccountGroup1.AG1Level1UDVarchar20'				SELECT @PlaybookBrandIndicator = SUBSTRING(@ParamGroupValues, @OldPos, @CurPos-@OldPos)
		IF @GroupingColumn = N'FactLastPriceAndCost.CustomerBrandSegment'			SELECT @CustomerBrandSegment = SUBSTRING(@ParamGroupValues, @OldPos, @CurPos-@OldPos)
		IF @GroupingColumn = N'DimAccountGroup1.AG1Level1UDVarchar25'			    SELECT @RegionExceptionIndicator = SUBSTRING(@ParamGroupValues, @OldPos, @CurPos-@OldPos)

		IF @GroupingColumn = N'DimAccount.CustomerSpecialism'						SELECT @CustomerSpecialism = SUBSTRING(@ParamGroupValues, @OldPos, @CurPos-@OldPos)
		IF @GroupingColumn = N'FactLastPriceAndCost.CustomerSize'					SELECT @CustomerSize = SUBSTRING(@ParamGroupValues, @OldPos, @CurPos-@OldPos)

		SELECT 	
			@OldPos = @CurPos+1,
			@CurPos = CHARINDEX(N',', @ParamGroupValues, @OldPos),

			@OldPosGroupingColumn = @CurPosGroupingColumn+1,
			@CurPosGroupingColumn = CHARINDEX(N',', @ParamGroupingColumns, @OldPosGroupingColumn)
				
	END

	SELECT @GroupingColumn = SUBSTRING(@ParamGroupingColumns, @OldPosGroupingColumn, LEN(@ParamGroupingColumns))
	
	IF @GroupingColumn = N'DimItem.ItemUDVarChar1'								SELECT @PyramidCode = SUBSTRING(@ParamGroupValues, @OldPos, LEN(@ParamGroupValues))
	IF @GroupingColumn = N'DimItem.ItemUDVarChar21'								SELECT @PlaybookProductBrand = SUBSTRING(@ParamGroupValues, @OldPos, LEN(@ParamGroupValues))
	IF @GroupingColumn = N'DimInvoiceLineGroup1.InvoiceLineGroup1UDVarchar1'	SELECT @ShipmentType = SUBSTRING(@ParamGroupValues, @OldPos, LEN(@ParamGroupValues))
	IF @GroupingColumn = N'DimInvoiceLineGroup1.InvoiceLineGroup1UDVarchar2'	SELECT @ContractClaimsIndicator = SUBSTRING(@ParamGroupValues, @OldPos, LEN(@ParamGroupValues))
	IF @GroupingColumn = N'FactLastPriceAndCost.UDVarchar10'					SELECT @PriceApproach = SUBSTRING(@ParamGroupValues, @OldPos, LEN(@ParamGroupValues))
	IF @GroupingColumn = N'DimItemGroup3.IG3Level3'								SELECT @GSPG = SUBSTRING(@ParamGroupValues, @OldPos, LEN(@ParamGroupValues))
	IF @GroupingColumn = N'DimItemGroup3.IG3Level2'								SELECT @HLSPG = SUBSTRING(@ParamGroupValues, @OldPos, LEN(@ParamGroupValues))
	IF @GroupingColumn = N'DimItemGroup3.IG3Level1'								SELECT @LLSPG = SUBSTRING(@ParamGroupValues, @OldPos, LEN(@ParamGroupValues))
	IF @GroupingColumn = N'FactLastPriceAndCost.UDVarchar1'						SELECT @StrategicItem = SUBSTRING(@ParamGroupValues, @OldPos, LEN(@ParamGroupValues))
	IF @GroupingColumn = N'DimItem.ItemNumber'									SELECT @Item = SUBSTRING(@ParamGroupValues, @OldPos, LEN(@ParamGroupValues))
	IF @GroupingColumn = N'FactLastPriceAndCost.UDVarchar4'						SELECT @Sales603010Bucket = SUBSTRING(@ParamGroupValues, @OldPos, LEN(@ParamGroupValues))
	IF @GroupingColumn = N'DimAccount.AccountUDVarChar10'						SELECT @CustomerSegment = SUBSTRING(@ParamGroupValues, @OldPos, LEN(@ParamGroupValues))
	IF @GroupingColumn = N'DimAccountGroup1.AG1Level1UDVarChar2'				SELECT @PlaybookRegion = SUBSTRING(@ParamGroupValues, @OldPos, LEN(@ParamGroupValues))
	IF @GroupingColumn = N'DimAccountGroup1.AG1Level1UDVarchar20'				SELECT @PlaybookBrandIndicator = SUBSTRING(@ParamGroupValues, @OldPos, LEN(@ParamGroupValues))
	IF @GroupingColumn = N'FactLastPriceAndCost.CustomerBrandSegment'			SELECT @CustomerBrandSegment = SUBSTRING(@ParamGroupValues, @OldPos, LEN(@ParamGroupValues))
	IF @GroupingColumn = N'DimAccountGroup1.AG1Level1UDVarchar25'			    SELECT @RegionExceptionIndicator = SUBSTRING(@ParamGroupValues, @OldPos, LEN(@ParamGroupValues))
	IF @GroupingColumn = N'DimAccount.CustomerSpecialism'						SELECT @CustomerSpecialism = SUBSTRING(@ParamGroupValues, @OldPos, LEN(@ParamGroupValues))
	IF @GroupingColumn = N'FactLastPriceAndCost.CustomerSize'					SELECT @CustomerSize = SUBSTRING(@ParamGroupValues, @OldPos, LEN(@ParamGroupValues))

	SELECT @Item = di.ItemNumber + N' - ' + di.ItemDescription FROM dbo.DimItem di (NOLOCK) WHERE ISNULL(@Item, N'') <> N'' AND ItemNumber = @Item
	SELECT @LLSPG = (SELECT TOP 1 IG3Level1 + N' - ' + IG3Level1UDVarchar1 FROM dbo.DimItemGroup3 (NOLOCK) WHERE ISNULL(@LLSPG, N'') <> N'' AND IG3Level1 = @LLSPG)
	SELECT @HLSPG = (SELECT TOP 1 IG3Level2 + N' - ' + IG3Level2UDVarchar1 FROM dbo.DimItemGroup3 (NOLOCK) WHERE ISNULL(@HLSPG, N'') <> N'' AND IG3Level2 = @HLSPG)
	SELECT @GSPG =  (SELECT TOP 1 IG3Level3 + N' - ' + IG3Level3UDVarchar1 FROM dbo.DimItemGroup3 (NOLOCK) WHERE ISNULL(@GSPG, N'')  <> N'' AND IG3Level3 = @GSPG)

	--SELECT @Item = di.ItemNumber + N' - ' + di.ItemDescription FROM dbo.DimItem di WHERE ItemNumber = @Item
	--SELECT @LLSPG = (SELECT TOP 1 IG3Level1 + N' - ' + IG3Level1UDVarchar1 FROM dbo.DimItemGroup3 WHERE IG3Level1 = @LLSPG)
	--SELECT @HLSPG = (SELECT TOP 1 IG3Level2 + N' - ' + IG3Level2UDVarchar1 FROM dbo.DimItemGroup3 WHERE IG3Level2 = @HLSPG)
	--SELECT @GSPG =  (SELECT TOP 1 IG3Level3 + N' - ' + IG3Level3UDVarchar1 FROM dbo.DimItemGroup3 WHERE IG3Level3 = @GSPG)

			
	INSERT 	@ListTable (
		PlaybookRegion,
		CustomerBrandSegment,
		CustomerSegment,
		Sales603010Bucket,
		StrategicItem,
		PriceApproach,
		Item,
		LLSPG,
		HLSPG,
		GSPG,
		ContractClaimsIndicator,
		ShipmentType,
		PlaybookProductBrand,
		PyramidCode,
		PlaybookBrandIndicator,
		RegionExceptionIndicator,
		CustomerSize,
		CustomerSpecialism
	)
	SELECT
		@PlaybookRegion,
		@CustomerBrandSegment,
		@CustomerSegment,
		@Sales603010Bucket,
		@StrategicItem,
		@PriceApproach,
		@Item,
		@LLSPG,
		@HLSPG,
		@GSPG,
		@ContractClaimsIndicator,
		@ShipmentType,
		@PlaybookProductBrand,
		@PyramidCode,
		@PlaybookBrandIndicator,
		@RegionExceptionIndicator,
		@CustomerSize,
		@CustomerSpecialism

	RETURN
END















GO
