SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE FUNCTION [ppe].[fn_GetDiscountSPG]
    (
      @AccountKey INT ,
      @ItemGroup3Key INT 
    )
RETURNS TABLE
AS
RETURN
    WITH cte AS
		(SELECT
			flpc.AccountKey,
			flpc.ItemKey,
			di.ItemGroup3Key,
			Total12MonthQuantity,
			ItemUDDecimal1,
			alip.Max,
			flpc.LastItemPrice,
			lip.Price,
			Total12MonthQuantity * di.ItemUDDecimal1 /* CurrBranchTradePrice*/ AS Total12MonthSalesAtTrade,
			1 - (alip.Max / NULLIF(di.ItemUDDecimal1 /* CurrBranchTradePrice*/, 0)) ALPCD 
		FROM dbo.FactLastPriceAndCost flpc
		JOIN dbo.FactInvoiceLine fil ON flpc.LastInvoiceLineUniqueIdentifier = fil.InvoiceLineUniqueIdentifier
		JOIN lion.vwFactInvoiceLineGroup1 g1 ON flpc.InvoiceLineGroup1Key = g1.InvoiceLineGroup1Key
		JOIN dbo.DimItem di ON di.ItemKey = flpc.ItemKey
		CROSS APPLY lion.fn_GetPriceEx(di.CurrentCost, 
										(flpc.LastItemPrice - flpc.LastItemCost) / NULLIF(flpc.LastItemPrice, 0.0),
										0.99,
										di.ItemUDDecimal1 /* CurrBranchTradePrice*/) lip
		CROSS APPLY lion.fn_Max(lip.Price, flpc.LastItemPrice) alip
		LEFT JOIN lion.CustomerTerms ct
			ON ct.AccountKey = flpc.AccountKey
			AND ct.ItemKey = flpc.ItemKey
		WHERE ct.CustomerTermsKey IS NULL
		AND flpc.InvoiceLineGroup1Key = 4
		AND fil.UDVarchar1 NOT IN ('DP', 'DC', 'ADJ', 'SPO', 'OCD', 'OCL') 
		AND flpc.Total12MonthQuantity > 0
		AND flpc.LastItemPrice > 0
		AND di.ItemUDDecimal1 > 0
		AND di.ItemUDVarchar1 = '1'
		AND g1.ShipmentType = 'S'
		AND g1.DeviatedIndicator = 'N'


		)

	SELECT 
		AccountKey, 
		ItemGroup3Key, 
		SUM(Total12MonthSalesAtTrade * (1.0 - ALPCD ) ) / NULLIF(SUM(Total12MonthSalesAtTrade),0) SPGLastDiscount, 
		SUM(Total12MonthSalesAtTrade)  Total12MonthSalesAtTrade
	FROM cte 
	WHERE AccountKey=@AccountKey
	AND ItemGroup3Key=@ItemGroup3Key
	GROUP BY AccountKey, ItemGroup3Key
GO
