SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [lion].[fn_GetImpact]
    (
      @Goal Percent_Type ,
      @Current Percent_Type ,
      @Factor Quantity_Normal_Type
    )
RETURNS TABLE
AS
RETURN
    SELECT  CASE WHEN ( @Goal - @Current ) > 0
                 THEN ( @Goal - @Current ) * @Factor
                 ELSE 0
            END AS [Impact]
GO
