SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




CREATE FUNCTION [lion].[fn_GetPriceEx]
    (
      @Cost Money_Normal_type ,
      @GPP Percent_type  ,
	  @CapFactor Percent_type = 0,
	  @Cap Money_Normal_type = 0
    )
RETURNS TABLE
AS
RETURN
    SELECT ROUND(CASE WHEN (@GPP IS NULL)
	                  THEN @Cap
	                  WHEN (@CapFactor > 0) 
	                  THEN CASE WHEN (@Cost / NULLIF( 1 - @GPP, 0)) >= (@Cap * @CapFactor)
				                THEN  @Cap
						        ELSE @Cost / NULLIF( 1 - @GPP, 0)
				           END
				      ELSE @Cost / NULLIF( 1 - @GPP, 0)
			      END, 3) AS [Price] 
GO
