SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS OFF
GO
-- select * from report
-- where reporturlpath like '%item%'

--select 775142.53220200 - 292219.51190000, 823814.79079900 - 378030.84950000

-- SELECT * FROM master.dbo.fn_ListToTableMax('A, B, C, D', ',')
-- SELECT * FROM master.dbo.fn_ListToTableMax('1.2, 1.3, Q, 3.333333333', ',')
-- SELECT * FROM master.dbo.fn_ListToTableMax(NULL, ',')
-- GO
-- 




CREATE FUNCTION [dbo].[fn_ListToTableMax] 
	(
		@List NVARCHAR(MAX),
		@Delimiter NVARCHAR(1)
	)
RETURNS @ListTable TABLE 
	(
		ListID 	INT IDENTITY,
		Value 	NVARCHAR(MAX)
	)
AS

/* 
I used the following as a model for this routine:
	fn_ListToTableMax from tomk@inforamp.net as the model:
	Taken from:  http://www.sqlservercentral.com/scripts/contributions/1581.asp
	By: http://www.sqlservercentral.com/scripts/listscriptsbyauthor.asp?author=1631
However, I changed it to suit my needs.
*/

/*
SELECT * FROM dbo.fn_ListToTableMax('a, b, c', ',')
SELECT * FROM dbo.fn_ListToTableMax('1.2, 1.3, Q, 3.333333333', ',')
SELECT * FROM dbo.fn_ListToTableMax(NULL, ',')
SELECT * FROM dbo.fn_ListToTableMax('', ',')
SELECT * FROM dbo.fn_ListToTableMax('a, b, c', ' ')
SELECT * FROM dbo.fn_ListToTableMax('a, b, c', '')
SELECT * FROM dbo.fn_ListToTableMax('a, b, c', NULL)
SELECT * FROM dbo.fn_ListToTableMax(NULL, NULL)
SELECT * FROM dbo.fn_ListToTableMax('a, b, c,', ',')
SELECT * FROM dbo.fn_ListToTableMax('a, b, c  ', ',')
SELECT * FROM dbo.fn_ListToTableMax('a, b, c,  ', ',')
*/

BEGIN

	DECLARE @Token 	NVARCHAR(MAX),	
		@CurPos INT, 
		@OldPos INT

	IF ISNULL(LTRIM(RTRIM(@List)), '') = ''
		RETURN


-- 	-- IF @Delimiter IS EMPTY, SEND EMPTY SET TO OUTPUT
-- 	IF @Delimiter IS NULL
-- 		RETURN
-- 	  ELSE IF @Delimiter='' -- @Delimiter VALUE IS IMPLICITLY CONVERTED TO nvarchar, STRIPS TRAILING SPACES
-- 				-- AS A RESULT, '  ' (2 SPACES) CAN NEVER BE USED AS A DELIMITER
-- 		SELECT @Delimiter=' '


	SELECT 	@OldPos = 1,
		@Token  = '',
		@CurPos = CHARINDEX(@Delimiter, @List, @OldPos)

	WHILE @CurPos >= 1 
	BEGIN
		SELECT 	@Token = SUBSTRING(@List, @OldPos, @CurPos-@OldPos)

		INSERT 	@ListTable (Value)
		SELECT 	LTRIM(RTRIM(@Token))
		
		SELECT 	@OldPos = @CurPos+1,
			@CurPos = CHARINDEX(@Delimiter, @List, @OldPos)
	END

-- 	IF (@OldPos <= LEN(@List))
-- 	BEGIN
	
		SELECT	@Token = SUBSTRING(@List, @OldPos, LEN(@List))

		INSERT 	@ListTable (Value)
		SELECT	LTRIM(RTRIM(@Token))

-- 	END


	RETURN
END





GO
