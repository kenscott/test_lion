SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE FUNCTION [ppe].[fn_GetCompoundDiscount]
    (
      @Discount1 DECIMAL(38, 8) ,
      @Discount2 DECIMAL(38, 8)
    )
RETURNS TABLE
AS
RETURN
    SELECT CASE WHEN @Discount1 IS NULL AND @Discount2 IS NULL THEN NULL
	            ELSE 1.0-(1.0 - ISNULL(@Discount1, 0)) * (1.0 - ISNULL(@Discount2, 0))
		   END AS Discount

GO
