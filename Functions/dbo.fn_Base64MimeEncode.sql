SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE FUNCTION [dbo].[fn_Base64MimeEncode] (@ai_int INT)
RETURNS NVARCHAR(1)
AS
BEGIN

	DECLARE @Base64Chars NVARCHAR(64)
	DECLARE @return NVARCHAR(1)
	SET @Base64Chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/'

	IF(@ai_int >= 0)
		SET @return = SUBSTRING(@Base64Chars,@ai_int+1,1)
	ELSE
		SET @return = '='
	RETURN @return
END




GO
