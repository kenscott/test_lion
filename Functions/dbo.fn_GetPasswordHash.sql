SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





CREATE 
FUNCTION [dbo].[fn_GetPasswordHash] (@pw_str VARCHAR(255))
RETURNS VARCHAR(30)
AS
BEGIN

DECLARE
	@Hash VARCHAR(4000),
	@HashedEncodedPW VARCHAR(150)
	

/* adapted from http://www.codeproject.com/database/xp_md5.asp */
SELECT @Hash = master.dbo.fn_varbintohexstr(HASHBYTES('SHA', @pw_str))

IF LEFT(@Hash, 2) = '0x'
BEGIN
	SELECT @Hash = SUBSTRING(@Hash, 3, LEN(@Hash) - 2)
END


--Convert the nvarchar, 1 hex per 2 chars  (e.g. '0cab7f...'), to a nvarchar containing
--the actual characters

DECLARE @hashChars VARCHAR(100)
DECLARE @position INT
DECLARE @offset INT
DECLARE @currentChar CHAR
DECLARE @initHexIntValue INT
DECLARE @hexIntValue INT

SET @hashChars = ''
SET @position = 1
SET @offset = 0

WHILE @position < 40
BEGIN
	
	SET @currentChar = SUBSTRING(@hash,@position+@offset,1)
	SELECT @initHexIntValue =
		CASE @currentChar
			WHEN 'a' THEN 10
			WHEN 'b' THEN 11
			WHEN 'c' THEN 12
			WHEN 'd' THEN 13
			WHEN 'e' THEN 14
			WHEN 'f' THEN 15
			ELSE CAST(@currentChar AS INT)
		END

	IF @offset = 0 
	BEGIN
		SET @hexIntValue = @initHexIntValue * 16
		SET @offset = 1
	END
	ELSE
	BEGIN
		SET @hexIntValue = @hexIntValue + @initHexIntValue
		SET @hashChars = @hashChars + CHAR(@hexIntValue)
		SET @offset = 0
		SET @position = @position + 2

	END
		 			

END

SET @HashedEncodedPW = dbo.fn_Base64Encode(@hashChars)

	
RETURN @HashedEncodedPW
	
END








GO
