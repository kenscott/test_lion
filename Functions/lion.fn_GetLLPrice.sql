SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE FUNCTION [lion].[fn_GetLLPrice]
    (
      @ListPrice Money_Normal_type ,
      @EffectiveDiscountPercent Percent_type 
    )
RETURNS TABLE
AS
RETURN
    SELECT  @ListPrice * ( 1 - @EffectiveDiscountPercent ) AS [LLPrice] 

GO
