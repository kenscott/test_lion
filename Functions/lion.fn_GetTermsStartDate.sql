SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		José A. Castaños
-- Create date: 15-Dec-2016
-- Description:	Starting date for terms benefit review
-- =============================================
CREATE FUNCTION [lion].[fn_GetTermsStartDate] ()
RETURNS date
AS
BEGIN
	DECLARE @Result date

	--SELECT @Result = DATEADD(yy, DATEDIFF(yy,0,getdate() - 37), 0)
	SELECT @Result = CAST('2016-01-01' AS DATE)

	-- Return the result of the function
	RETURN @Result

	-- select lion.fn_GetTermsStartDate()
END



GO
