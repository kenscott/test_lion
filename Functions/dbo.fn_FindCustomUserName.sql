SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE
FUNCTION [dbo].[fn_FindCustomUserName] ()

RETURNS VARCHAR(100)

AS
BEGIN
DECLARE @CustomName VARCHAR(100)

SELECT TOP 1 @CustomName = u.name
FROM sysobjects o
JOIN sysusers u ON u.uid = o.uid
WHERE u.name <> 'dbo' AND u.name <> 'sys'
GROUP BY u.name
ORDER BY COUNT(*) DESC

IF @CustomName IS NULL
	SET @CustomName = 'dbo'

RETURN @CustomName
END


GO
