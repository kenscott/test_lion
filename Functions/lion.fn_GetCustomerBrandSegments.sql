SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [lion].[fn_GetCustomerBrandSegments]
(
	@AccountKey Key_Normal_type,
	@ItemKey Key_Normal_type 
)
RETURNS @Results TABLE (
	CustomerBrandSegment NVARCHAR(250)
)
AS

BEGIN

		DECLARE @CustomerBrandSegment UDVarchar_type


		DECLARE @CustomerBrands TABLE (
			Brand NVARCHAR(50),
			SegmentBrand NVARCHAR(250),
			Segment NVARCHAR(50)	
		)

		INSERT @CustomerBrands (
			Brand,
			SegmentBrand,
			Segment
		)
		SELECT 
			db.Brand,
			db.SegmentBrand,
			asb.Segment
		FROM DimAccountBrandSegment asb
		INNER JOIN DimBrand db
			ON db.BrandKey = asb.BrandKey
		WHERE
			asb.AccountKey = @AccountKey
		
		-- Pass 1
		SELECT 
			@CustomerBrandSegment = SegmentBrand + N'-' + Segment
		FROM dbo.DimItem di
		INNER JOIN @CustomerBrands cb
			ON cb.Brand = ItemUDVarChar2
		WHERE 
			di.ItemKey = @ItemKey

		IF @CustomerBrandSegment IS NULL
		-- Pass 2
			SELECT 
				@CustomerBrandSegment = SegmentBrand + N'-' + Segment
			FROM dbo.DimAccount da
			INNER JOIN DimAccountGroup1 dag1
				ON dag1.AccountGroup1Key = da.AccountGroup1Key
			INNER JOIN @CustomerBrands cb
				ON cb.Brand = AG1Level1UDVarchar6
			WHERE
				da.AccountKey = @AccountKey
				AND cb.Brand <> N'BURDN'

		IF @CustomerBrandSegment IS NULL
		-- Pass 3
		SELECT
			@CustomerBrandSegment = db.Brand + N'-' + N'ALL'
		FROM dbo.DimItem di
		INNER JOIN DimBrand db
			ON db.Brand = di.ItemUDVarChar2
		WHERE 
			di.ItemKey = @ItemKey 

INSERT @Results (CustomerBrandSegment)

SELECT @CustomerBrandSegment



	RETURN
END


GO
