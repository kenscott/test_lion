SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE FUNCTION [lion].[fn_GetDiscountChangePct]
    (
      @LastPrice Money_Normal_Type ,
      @GreenPrice Money_Normal_Type ,
      @AmberPrice Money_Normal_Type ,
      @RedPrice Money_Normal_Type 
    )
RETURNS TABLE
AS
RETURN
    SELECT  CASE WHEN @LastPrice < @RedPrice THEN 0.015
	             WHEN @LastPrice < @AmberPrice THEN 0.0125
				 WHEN @LastPrice < @GreenPrice THEN 0.01
				 ELSE 0.00
			END AS ChangePct
GO
