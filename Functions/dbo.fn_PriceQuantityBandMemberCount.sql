SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





CREATE FUNCTION [dbo].[fn_PriceQuantityBandMemberCount]
 (@PlaybookPricingGroupKey Key_Normal_Type,
  @RulingIndicator Description_Small_Type,
  @LowerUpperBandQuantity Quantity_Normal_Type,
  @UpperBandQuantity Quantity_Normal_Type)
RETURNS INT
AS
BEGIN
 DECLARE @Result INT
 SET @Result = (SELECT COUNT(*)
				 FROM PlaybookDataPointPricingGroup
				 WHERE PlaybookPricingGroupKey = @PlaybookPricingGroupKey
					AND RulingMemberFlag = @RulingIndicator
					AND Total12MonthQuantity BETWEEN @LowerUpperBandQuantity AND @UpperBandQuantity)

 RETURN @Result
END


























GO
