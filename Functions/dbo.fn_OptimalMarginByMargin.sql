SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





--dba_find_string 'Bearcat.fn_OptimalMarginByMargin'



CREATE FUNCTION [dbo].[fn_OptimalMarginByMargin]
 (@PlaybookPricingGroupKey INT,
  @LowerBandQuantity DECIMAL(19,8),
  @UpperBandQuantity DECIMAL(19,8),
  @Pctile NUMERIC(19,8),  --The percentile rank we care to find
  @UseAllGroupDPs VARCHAR(10) = 'N')--
RETURNS DECIMAL(19,8)
AS
----------------Customiztions--------------
--TBW:	07/20/2005	This proc was created just for bearcat but will eventually become DBO owned and used by everyone
-------------------------------------------
BEGIN
 DECLARE @Result DECIMAL(19,8)
 DECLARE @sqlstmt NVARCHAR(500)
 DECLARE @midCount INT

 DECLARE @tempOptimalMarginByMargin TABLE (RowID INT IDENTITY(1,1), col DECIMAL(19,8))

INSERT @tempOptimalMarginByMargin (col)
SELECT (LastItemPrice-LastItemCost)/LastItemPrice
FROM PlaybookDataPointPricingGroup A
WHERE A.PlaybookPricingGroupKey = @PlaybookPricingGroupKey
	AND 
	(	
		(@UseAllGroupDPs = 'N' AND A.Total12MonthQuantity BETWEEN @LowerBandQuantity AND @UpperBandQuantity)	--Should we only look at data points in this quantity band?
		OR
		(@UseAllGroupDPs = 'Y')	--Or should we look at all data points for the pricing group?
	)
AND RulingMemberFlag = 'Y'
AND LastItemPrice <> 0
ORDER BY (LastItemPrice-LastItemCost)/LastItemPrice	--Customization:  Order by GPP (margin) instead of by price


-- declare c_med cursor SCROLL for select col from @tempOptimalMarginByMargin
SELECT @midCount = ROUND ( COUNT(*) * @Pctile, 0) FROM @tempOptimalMarginByMargin
IF @midCount = 0 SET @midCount = 1
--This gets the quantity at the data point that matches the % (rounded up to the nearest data point)
SET @Result = (SELECT col FROM @tempOptimalMarginByMargin WHERE RowID = @midCount)

RETURN @Result

END






















GO
