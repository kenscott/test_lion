SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE FUNCTION [lion].[fn_RPBPriceBandRAGForAnalysis](@RPBPlaybookPricingGroupKey [int], @Total12MonthQuantity [decimal](19, 8), @FloorPercentile [decimal](19, 8), @TargetPercentile [decimal](19, 8), @StretchPercentile [decimal](19, 8))
RETURNS TABLE AS 
RETURN

/*
SELECT * FROM lion.fn_RPBPriceBandRAGForAnalysis(10351, 1, 0.25, 0.60, 0.80)
*/


SELECT TOP 1
	TheFloor.PDPGPP AS FloorGPP,
	TheTarget.PDPGPP AS TargetGPP,	
	TheStretch.PDPGPP AS StretchGPP,

	ppg.GroupValues AS CoveringBandGroupValues,
	ppg.PricingRuleKey AS CoveringBandPricingRuleKey,	
	TheTarget.PlaybookPricingGroupQuantityBandKey AS CoveringBandPlaybookPricingGroupQuantityBandKey,
	TheTarget.LowerBandQuantity AS CoveringBandLowerBandQuantity,
	TheTarget.UpperBandQuantity AS CoveringBandUpperBandQuantity,
	TheTarget.RulingMemberCount AS CoveringBandRulingMemberCount, 
	TheTarget.BandSequenceNumber AS CoveringBandBandSequenceNumber,
	TheTarget.PricingRuleSequenceNumber AS CoveringBandPricingRuleSequenceNumber,
		
	TheFloor.PlaybookPricingGroupPriceBandKey AS FloorPlaybookPricingGroupPriceBandKey,
	TheFloor.PlaybookPricingGroupKey AS FloorPlaybookPricingGroupKey,
	TheFloor.LowerBandQuantity AS FloorLowerBandQuantity,
	TheFloor.UpperBandQuantity AS FloorUpperBandQuantity,
	TheFloor.MemberRank AS FloorMemberRank,
	TheFloor.RulingMemberCount AS FloorRulingMemberCount,
	TheFloor.BandSequenceNumber AS FloorBandSequenceNumber,
	TheFloor.PlaybookPricingGroupQuantityBandKey AS FloorPlaybookPricingGroupQuantityBandKey,
	TheFloor.PricingRuleSequenceNumber AS FloorPricingRuleSequenceNumber,

	TheTarget.PlaybookPricingGroupPriceBandKey AS TargetPlaybookPricingGroupPriceBandKey,
	TheTarget.PlaybookPricingGroupKey AS TargetPlaybookPricingGroupKey,
	TheTarget.LowerBandQuantity AS TargetLowerBandQuantity,
	TheTarget.UpperBandQuantity AS TargetUpperBandQuantity,
	TheTarget.MemberRank AS TargetMemberRank,
	TheTarget.RulingMemberCount AS TargetRulingMemberCount,
	TheTarget.BandSequenceNumber AS TargetBandSequenceNumber,
	TheTarget.PlaybookPricingGroupQuantityBandKey AS TargetPlaybookPricingGroupQuantityBandKey,
	TheTarget.PricingRuleSequenceNumber AS TargetPricingRuleSequenceNumber,

	TheStretch.PlaybookPricingGroupPriceBandKey AS StretchPlaybookPricingGroupPriceBandKey,
	TheStretch.PlaybookPricingGroupKey AS StretchPlaybookPricingGroupKey,
	TheStretch.LowerBandQuantity AS StretchLowerBandQuantity,
	TheStretch.UpperBandQuantity AS StretchUpperBandQuantity,
	TheStretch.MemberRank AS StretchMemberRank,
	TheStretch.RulingMemberCount AS StretchRulingMemberCount,
	TheStretch.BandSequenceNumber AS StretchBandSequenceNumber,
	TheStretch.PlaybookPricingGroupQuantityBandKey AS StretchPlaybookPricingGroupQuantityBandKey,
	TheStretch.PricingRuleSequenceNumber AS StretchPricingRuleSequenceNumber,

	TheFloorDiscount.PDPDiscountPercent AS EffectiveFloorDiscountPercent,
	TheTargetDiscount.PDPDiscountPercent AS EffectiveTargetDiscountPercent,
	TheStretchDiscount.PDPDiscountPercent AS EffectiveStretchDiscountPercent
	
FROM dbo.PlaybookPricingGroupPriceBand TheFloor (NOLOCK)
INNER JOIN dbo.PlaybookPricingGroup ppg (NOLOCK)
	ON ppg.PlaybookPricingGroupKey = TheFloor.PlaybookPricingGroupKey


INNER JOIN dbo.PlaybookPricingGroupPriceBand TheTarget (NOLOCK)
	ON TheTarget.PlaybookPricingGroupKey = @RPBPlaybookPricingGroupKey
	AND @Total12MonthQuantity BETWEEN TheTarget.LowerBandQuantity AND TheTarget.UpperBandQuantity 
	AND TheTarget.MemberRank = 
		CASE
			WHEN  ROUND ( TheTarget.RulingMemberCount * @TargetPercentile, 0) BETWEEN 1 AND TheTarget.RulingMemberCount THEN  ROUND ( TheTarget.RulingMemberCount * @TargetPercentile, 0)
			ELSE 1
		END
INNER JOIN dbo.PlaybookPricingGroupPriceBand TheStretch (NOLOCK)
	ON TheStretch.PlaybookPricingGroupKey = @RPBPlaybookPricingGroupKey
	AND @Total12MonthQuantity BETWEEN TheStretch.LowerBandQuantity AND TheStretch.UpperBandQuantity 
	AND TheStretch.MemberRank = 
		CASE
			WHEN  ROUND ( TheStretch.RulingMemberCount * @StretchPercentile, 0) BETWEEN 1 AND TheStretch.RulingMemberCount THEN  ROUND ( TheStretch.RulingMemberCount * @StretchPercentile, 0)
			ELSE 1
		END



INNER JOIN dbo.PlaybookPricingGroupPriceBand TheFloorDiscount (NOLOCK)
	ON TheFloorDiscount.PlaybookPricingGroupKey = @RPBPlaybookPricingGroupKey
	AND @Total12MonthQuantity BETWEEN TheFloorDiscount.LowerBandQuantity AND TheFloorDiscount.UpperBandQuantity 
	AND TheFloorDiscount.MemberRankDiscount = 
		CASE
			WHEN  ROUND ( TheFloorDiscount.RulingMemberCount * @FloorPercentile, 0) BETWEEN 1 AND TheFloorDiscount.RulingMemberCount THEN  ROUND ( TheFloorDiscount.RulingMemberCount * @FloorPercentile, 0)
			ELSE 1
		END
INNER JOIN dbo.PlaybookPricingGroupPriceBand TheTargetDiscount (NOLOCK)
	ON TheTargetDiscount.PlaybookPricingGroupKey = @RPBPlaybookPricingGroupKey
	AND @Total12MonthQuantity BETWEEN TheTargetDiscount.LowerBandQuantity AND TheTargetDiscount.UpperBandQuantity 
	AND TheTargetDiscount.MemberRankDiscount = 
		CASE
			WHEN  ROUND ( TheTargetDiscount.RulingMemberCount * @TargetPercentile, 0) BETWEEN 1 AND TheTargetDiscount.RulingMemberCount THEN  ROUND ( TheTargetDiscount.RulingMemberCount * @TargetPercentile, 0)
			ELSE 1
		END
INNER JOIN dbo.PlaybookPricingGroupPriceBand TheStretchDiscount (NOLOCK)
	ON TheStretchDiscount.PlaybookPricingGroupKey = @RPBPlaybookPricingGroupKey
	AND @Total12MonthQuantity BETWEEN TheStretchDiscount.LowerBandQuantity AND TheStretchDiscount.UpperBandQuantity 
	AND TheStretchDiscount.MemberRankDiscount = 
		CASE
			WHEN  ROUND ( TheStretchDiscount.RulingMemberCount * @StretchPercentile, 0) BETWEEN 1 AND TheStretchDiscount.RulingMemberCount THEN  ROUND ( TheStretchDiscount.RulingMemberCount * @StretchPercentile, 0)
			ELSE 1
		END


WHERE
	TheFloor.PlaybookPricingGroupKey = @RPBPlaybookPricingGroupKey
	AND @Total12MonthQuantity BETWEEN TheFloor.LowerBandQuantity AND TheFloor.UpperBandQuantity 
	AND TheFloor.MemberRank = 
		CASE
			WHEN  ROUND ( TheFloor.RulingMemberCount * @FloorPercentile, 0) BETWEEN 1 AND TheFloor.RulingMemberCount THEN  ROUND ( TheFloor.RulingMemberCount * @FloorPercentile, 0)
			ELSE 1
		END



GO
