SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE FUNCTION [lion].[fn_GetListPrice]
    (
      @LastInvoiceTradePrice UDVarchar_Small_type ,
      @CurrentPrice Money_Normal_type 
    )
RETURNS TABLE
AS
RETURN
    SELECT  CASE WHEN @LastInvoiceTradePrice = NULL THEN @CurrentPrice
                 ELSE CASE WHEN ISNUMERIC(@LastInvoiceTradePrice) = 1
                           THEN @LastInvoiceTradePrice
                           ELSE @CurrentPrice
                      END
            END AS [List Price] 


GO
