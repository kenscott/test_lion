SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE FUNCTION [lion].[fn_GetDiscPrice]
    (
      @FullPrice Money_Normal_type ,
      @Discount Percent_type
    )
RETURNS TABLE
AS
RETURN
    SELECT @FullPrice * ( 1 - @Discount) AS [Price] 

GO
