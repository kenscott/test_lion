SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE  FUNCTION [dbo].[fn_WrapColumnsWithBrackets] (@InCols NVARCHAR(500))
RETURNS NVARCHAR(600)
AS
BEGIN

DECLARE 
	@COMMA CHAR,
	@SPACE CHAR

SET @COMMA = ','
SET @SPACE = ' '

DECLARE 
	@currentCharPos INT,
	@currentChar CHAR,
	@started INT,
	@returnString NVARCHAR(600)

SET @started = 0
SET @currentCharPos = 1	
SET @returnString = ''	


WHILE @currentCharPos <= LEN(@InCols)
BEGIN
	
	SET @currentChar = SUBSTRING(@InCols,@currentCharPos,1)


	--print str(@currentCharPos)+'  '+@currentChar

	IF @currentChar = @COMMA
	BEGIN
		IF @started = 1
		BEGIN
			SET @returnString = @returnString + '], '
			SET @started = 0
		END
				
		
	END ELSE
	BEGIN
		IF @currentChar <> @SPACE OR (@currentChar = @SPACE  AND @started = 1)
		BEGIN
			IF @started = 0
			BEGIN
				SET @started = 1	
				SET @returnString = @returnString + '['+@currentChar
			END ELSE
			BEGIN
				SET @returnString = @returnString + @currentChar
			END
		END

	END

	SET @currentCharPos = @currentCharPos + 1

	

END
IF @started = 1 
	SET @returnString = @returnString + ']'

RETURN @returnString
	
END





GO
