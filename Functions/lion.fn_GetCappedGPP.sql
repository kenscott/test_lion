SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE FUNCTION [lion].[fn_GetCappedGPP]
    (
      @Cost Money_Normal_type ,
      @GPP Percent_type  ,
	  @CapFactor Percent_type = 0,
	  @Cap Money_Normal_type = 0
    )
RETURNS TABLE
AS
RETURN
    SELECT CASE WHEN (@Cap > 0) 
	            THEN CASE WHEN (@Cost / NULLIF( 1 - @GPP, 0)) > @Cap
				          THEN (@Cap - @Cost) / NULLIF(@Cap, 0)
						  ELSE @GPP
				     END
				ELSE @GPP
			END AS [GPP] 

GO
