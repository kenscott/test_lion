SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE FUNCTION [ppe].[fn_GetDiscountIndex]
    (
      @RedDisc DECIMAL (19,8) ,
      @AmberDisc DECIMAL (19,8) ,
      @GreenDisc DECIMAL (19,8) ,
      @ActualDisc DECIMAL (19,8)
    )
RETURNS TABLE
AS
RETURN 
    SELECT  --per LIO-699
		CASE 
			WHEN @RedDisc IS NULL OR @AmberDisc IS NULL OR @GreenDisc IS NULL THEN NULL
			WHEN @ActualDisc > @RedDisc THEN 1 
			WHEN @ActualDisc > @AmberDisc - ((@AmberDisc - @RedDisc) * 0.25) THEN 2
			WHEN @ActualDisc > @AmberDisc + ((@GreenDisc - @AmberDisc) * 0.25) THEN 3
			WHEN @ActualDisc > @GreenDisc THEN 4 
			ELSE 5 
		END AS DiscountIndex
GO
