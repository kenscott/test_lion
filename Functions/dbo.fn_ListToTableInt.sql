SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS OFF
GO

-- select * from report
-- where reporturlpath like '%item%'

--select 775142.53220200 - 292219.51190000, 823814.79079900 - 378030.84950000

-- SELECT * FROM master.dbo.fn_ListToTable('A, B, C, D', ',')
-- SELECT * FROM master.dbo.fn_ListToTable('1.2, 1.3, Q, 3.333333333', ',')
-- SELECT * FROM master.dbo.fn_ListToTable(NULL, ',')
-- GO
-- 




CREATE FUNCTION [dbo].[fn_ListToTableInt] 
	(
		@List NVARCHAR(MAX),
		@Delimiter NVARCHAR(1)
	)
RETURNS @ListTable TABLE 
	(
		ListID 	INT IDENTITY,
		Value 	INT
	)
AS

/* 
I used the following as a model for this routine:
	fn_ListToTable from tomk@inforamp.net as the model:
	Taken from:  http://www.sqlservercentral.com/scripts/contributions/1581.asp
	By: http://www.sqlservercentral.com/scripts/listscriptsbyauthor.asp?author=1631
However, I changed it to suit my needs.
*/

BEGIN

	DECLARE @Token 	NVARCHAR(MAX),	
		@CurPos INT, 
		@OldPos INT


	IF ISNULL(LTRIM(RTRIM(@List)), '') = ''
		RETURN


	SELECT 	@OldPos = 1,
		@Token  = '',
		@CurPos = CHARINDEX(@Delimiter, @List, @OldPos)

	WHILE @CurPos >= 1 
	BEGIN
		SELECT 	@Token = SUBSTRING(@List, @OldPos, @CurPos-@OldPos)

		INSERT 	@ListTable (Value)
		SELECT 	CAST(LTRIM(RTRIM(@Token)) AS INT)
		
		SELECT 	@OldPos = @CurPos+1,
			@CurPos = CHARINDEX(@Delimiter, @List, @OldPos)
	END

-- 	IF (@OldPos <= LEN(@List))
-- 	BEGIN
	
		SELECT	@Token = SUBSTRING(@List, @OldPos, LEN(@List))

		INSERT 	@ListTable (Value)
		SELECT	CAST(LTRIM(RTRIM(@Token)) AS INT)

-- 	END


	RETURN
END





GO
