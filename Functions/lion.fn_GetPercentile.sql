SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE FUNCTION [lion].[fn_GetPercentile]
(
	@ParamBrand UDVarchar_type,
	@ParamRegion UDVarchar_type,
	@ParamArea UDVarchar_type,
	@ParamNetwork UDVarchar_type,
	@ParamBranch UDVarchar_type,
	@ParamSales603010Bucket UDVarchar_type,
	@ParamItemPyramidCode UDVarchar_type,
	@ParamLLSPGCode UDVarchar_type
)
RETURNS TABLE
AS
RETURN

/*
SELECT * FROM lion.fn_GetPercentile('','National','9BL','','1','AZ80')
SELECT * FROM lion.fn_GetPercentile('','National','9BL','Cash Sales','1','AZ80')

SELECT
TOP 1
pcts.*,
flpac.*
FROM dbo.FactLastPriceAndCost flpac
INNER JOIN dbo.DimAccount da
	ON da.AccountKey = flpac.AccountKey
INNER JOIN dbo.DimAccountGroup1 dag1
	ON dag1.AccountGroup1Key = da.AccountGroup1Key
INNER JOIN dbo.DimItem di
	ON di.ItemKey = flpac.ItemKey
INNER JOIN dbo.DimItemGroup3 dig3
	ON dig3.ItemGroup3Key = di.ItemGroup3Key
CROSS APPLY lion.fn_GetPercentile(AG1Level1UDVarchar6, AG1Level1UDVarchar2, AG1Level1, flpac.UDVarchar4, IG3Level4, IG3Level1) Pcts
*/

WITH A AS (
	SELECT 		
		FloorPercentile,
		TargetPercentile,
		StretchPercentile,
		Offset,
		ROW_NUMBER() OVER (ORDER BY SequenceNumber DESC) AS RowRank
	FROM lion.ATKPriceBandPercentile pbp
	WHERE 		
		(Brand = @ParamBrand OR Brand = 'ALL')
		AND (Region  = @ParamRegion  OR Region = 'ALL')
		AND (Area    = @ParamArea    OR Area = 'ALL')
		AND (Network = @ParamNetwork OR Network = 'ALL')
		AND (Branch  = @ParamBranch  OR Branch = 'ALL')
		AND (Sales603010Bucket = @ParamSales603010Bucket OR Sales603010Bucket = 'ALL')
		AND (ItemPyramidCode = @ParamItemPyramidCode OR ItemPyramidCode = 'ALL')
		AND (LLSPGCode = @ParamLLSPGCode OR LLSPGCode = 'ALL')
)
SELECT
	FloorPercentile,
	TargetPercentile,
	StretchPercentile,
	Offset
FROM A
WHERE 
	RowRank = 1

GO
