SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE  FUNCTION [dbo].[fn_GetDateWOTime] (@fulldate DATETIME)
RETURNS DATETIME
AS
BEGIN

DECLARE 
	@newDate DATETIME,
	@dateString NVARCHAR(50)

SET @dateString = CAST(DATEPART(month,@fulldate) AS NVARCHAR(5)) +
	'/'+CAST(DATEPART(day,@fulldate) AS NVARCHAR(5)) +
	'/'+CAST(DATEPART(year,@fulldate) AS NVARCHAR(5))

SET @newDate = CAST(@dateString AS DATETIME)

	
RETURN @newDate
	
END



GO
