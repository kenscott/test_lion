SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE                  FUNCTION [dbo].[fn_SustainedMargin]
(
@AccountKey INT, 
@ItemKey INT, 
@InvoiceLineGroup1Key INT,
@SalesPercentThreshold DECIMAL(19,8),
@TotalInvoiceLinesPercentThreshold DECIMAL(19,8),
@GPPFrequencyThreshold DECIMAL(19,8),
@MinInvoiceDateDayKey INT,
@MaxInvoiceDateDayKey INT,
@SMEMinGPP Percent_Type,
@SMEMaxGPP Percent_Type
)
RETURNS DECIMAL(19,8)
AS
BEGIN

DECLARE @GPP Percent_type, @TotalP DECIMAL(19,8), @TotalI DECIMAL(19,8), @Result DECIMAL(19,8)

DECLARE @Temp TABLE 
	(RowID INT IDENTITY(1,1), GPP DECIMAL(19,8), TotalActualPrice DECIMAL(19,8), TotalInvoiceLines INT, PRIMARY KEY (RowID))

--This table will contain one row per GPP and the lowest GPP will be row 1
INSERT @Temp (GPP, TotalActualPrice, TotalInvoiceLines)

SELECT GPP, TotalActualPrice, TotalInvoiceLines
FROM 
	(SELECT 
		(TotalActualPrice - TotalActualCost) / TotalActualPrice AS GPP,
		SUM(TotalActualPrice) AS TotalActualPrice,
		COUNT(*) AS 'TotalInvoiceLines'
	FROM	FactInvoiceLine
	WHERE	AccountKey = @AccountKey
			AND ItemKey = @ItemKey
			AND InvoiceLineGroup1Key = @InvoiceLineGroup1Key
			AND InvoiceDateDayKey BETWEEN @MinInvoiceDateDayKey AND @MaxInvoiceDateDayKey
			AND	TotalActualPrice > 0 AND TotalActualCost > 0 AND TotalQuantity > 0
			AND (TotalActualPrice - TotalActualCost) / TotalActualPrice BETWEEN @SMEMinGPP AND @SMEMaxGPP
	GROUP BY 	
		(TotalActualPrice - TotalActualCost) / TotalActualPrice
	)ResultTable
--Where GPP > 0
ORDER BY GPP	
--		(TotalActualPrice - TotalActualCost) / TotalActualPrice)


SELECT @TotalP = SUM(TotalActualPrice), @TotalI = SUM(TotalInvoiceLines) FROM @Temp


--This finds the maximum GPP that as a Sales and Frequency CDF above the threshold

SET @Result = 
				(SELECT TOP 1 GPP AS 'SustainedGPP' 
				FROM 
					(SELECT 
					A.GPP, 
					(SELECT SUM(B.TotalActualPrice) FROM @Temp B WHERE B.RowID >= A.RowID)/@TotalP TotalActualPriceCDF,
					(SELECT SUM(B.TotalInvoiceLines) FROM @Temp B WHERE B.RowID >= A.RowID)/@TotalI TotalInvoiceLinesCDF,
					(SELECT SUM(B.TotalInvoiceLines) FROM @Temp B WHERE B.GPP = A.GPP) / @TotalI AS GPPFrequency
					FROM @Temp A) Final
				WHERE	TotalActualPriceCDF > @SalesPercentThreshold
						AND TotalInvoiceLinesCDF > @TotalInvoiceLinesPercentThreshold
						AND GPPFrequency > @GPPFrequencyThreshold
				ORDER BY GPP DESC)
	
RETURN @Result

END
















--select top 10 AccountKey, ItemKey, count(*) from FactInvoiceLine group by AccountKey, ItemKey having count(*) > 50







GO
