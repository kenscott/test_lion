SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE FUNCTION [lion].[fn_GetCappedDiscountValue]
(
	@Value DECIMAL(19,8),
	@Cap DECIMAL(19,8)
)
RETURNS TABLE
AS
RETURN 

	-- SELECT IIF (@Value < @Cap OR @Cap IS NULL, @Value, @Cap) AS CappedDiscountValue
	
	
	SELECT IIF (@Cap < @Value OR @Cap IS NULL, @Value, @Cap) AS CappedDiscountValue

GO
