SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE FUNCTION [lion].[fn_GetCost]
    (
      @CurrentBranchCost UDDecimal_type,
	  @Padding UDDecimal_type,
	  @CurrNotSettVal UDDecimal_Type,
      @SellingCF UDDecimal_type,
	  @PricingCF UDDecimal_type 
    )
RETURNS TABLE
AS
RETURN
    SELECT (@CurrentBranchCost - @Padding - @CurrNotSettVal) * ISNULL(@SellingCF, 1) / ISNULL(@PricingCF, 1) as Cost

GO
