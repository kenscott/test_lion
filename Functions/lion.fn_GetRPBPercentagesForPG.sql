SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO






CREATE FUNCTION [lion].[fn_GetRPBPercentagesForPG]
(
	@PlaybookPricingGroupKey Key_Normal_type,
	@Total12MonthQuantity Quantity_Normal_type,
	@FloorPercentile Percent_type,
	@TargetPercentile Percent_type,
	@StretchPercentile Percent_type,
	@Offset Percent_type
)
RETURNS TABLE
AS
RETURN

/*
SELECT * FROM lion.fn_GetRPBPercentagesForPG(27442, 2, 0, 0, 0)
*/


SELECT
TOP 1

	TheFloor.PricingRuleSequenceNumber,
	--TheFloor.GroupValues,
	--TheFloor.GroupingColumns,
	TheFloor.PDPGPP   + @Offset AS FloorGPP,
	TheTarget.PDPGPP  + @Offset AS TargetGPP,	
	TheStretch.PDPGPP + @Offset AS StretchGPP,

	TheFloor.PDPDiscountPercent AS FloorDiscount,
	TheTarget.PDPDiscountPercent AS TargetDiscount,
	TheStretch.PDPDiscountPercent AS StretchDiscount

FROM dbo.PlaybookPricingGroupPriceBand TheFloor


--FROM dbo.PlaybookPricingGroupPriceBand TheFloor (NOLOCK)
--INNER JOIN dbo.PlaybookPricingGroup ppg (NOLOCK)
--	ON ppg.PlaybookPricingGroupKey = TheFloor.PlaybookPricingGroupKey


INNER JOIN dbo.PlaybookPricingGroupPriceBand TheTarget (NOLOCK)
	ON TheTarget.PlaybookPricingGroupKey = @PlaybookPricingGroupKey
	AND (@Total12MonthQuantity BETWEEN TheTarget.LowerBandQuantity AND TheTarget.UpperBandQuantity OR @Total12MonthQuantity IS NULL)
	AND TheTarget.MemberRank = 
		CASE
			WHEN  ROUND ( TheTarget.RulingMemberCount * @TargetPercentile, 0) BETWEEN 1 AND TheTarget.RulingMemberCount THEN  ROUND ( TheTarget.RulingMemberCount * @TargetPercentile, 0)
			ELSE 1
		END
INNER JOIN dbo.PlaybookPricingGroupPriceBand TheStretch (NOLOCK)
	ON TheStretch.PlaybookPricingGroupKey = @PlaybookPricingGroupKey
	AND (@Total12MonthQuantity BETWEEN TheStretch.LowerBandQuantity AND TheStretch.UpperBandQuantity OR @Total12MonthQuantity IS NULL)
	AND TheStretch.MemberRank = 
		CASE
			WHEN  ROUND ( TheStretch.RulingMemberCount * @StretchPercentile, 0) BETWEEN 1 AND TheStretch.RulingMemberCount THEN  ROUND ( TheStretch.RulingMemberCount * @StretchPercentile, 0)
			ELSE 1
		END
WHERE
	TheFloor.PlaybookPricingGroupKey = @PlaybookPricingGroupKey
	AND (@Total12MonthQuantity BETWEEN TheFloor.LowerBandQuantity AND TheFloor.UpperBandQuantity OR @Total12MonthQuantity IS NULL)
	AND TheFloor.MemberRank = 
		CASE
			WHEN  ROUND ( TheFloor.RulingMemberCount * @FloorPercentile, 0) BETWEEN 1 AND TheFloor.RulingMemberCount THEN  ROUND ( TheFloor.RulingMemberCount * @FloorPercentile, 0)
			ELSE 1
		END




GO
