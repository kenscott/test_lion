SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE FUNCTION [lion].[fn_GetPrice]
    (
      @Cost Money_Normal_type ,
      @GPP Percent_type 
    )
RETURNS TABLE
AS
RETURN
    SELECT  tf.[Price] FROM lion.[fn_getPriceEx] (@Cost, @GPP, DEFAULT, DEFAULT) tf
	
GO
