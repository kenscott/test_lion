SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE FUNCTION [ppe].[fn_GetNewTerm]
    (
      @NewTermType VARCHAR(2),
      @FixedPrice DECIMAL(19, 8) ,
      @ExceptionDiscount DECIMAL(19, 8) ,
      @SPGDiscount DECIMAL(19, 8)
    )
RETURNS TABLE
AS
RETURN
	SELECT IIF(@NewTermType = 'FP',
	            FORMAT(@FixedPrice, 'C3', 'en-GB'), 
	            CASE WHEN ISNULL(@ExceptionDiscount, @SPGDiscount) > 0
					 THEN ''
                     WHEN ISNULL(@ExceptionDiscount, @SPGDiscount) = 0
                     THEN '-'
                     ELSE '+'
                END + FORMAT(-ISNULL(@ExceptionDiscount, @SPGDiscount), '0.00%')
			   ) AS Term;


--    SELECT  IIF(@FixedPrice IS NOT NULL, 
--	            FORMAT(@FixedPrice, 'C3', 'en-GB'), 
--	            CASE WHEN ISNULL(@ExceptionDiscount, @SPGDiscount) > 0
--					 THEN ''
--                     WHEN ISNULL(@ExceptionDiscount, @SPGDiscount) = 0
--                     THEN '-'
--                     ELSE '+'
--                END + FORMAT(-ISNULL(@ExceptionDiscount, @SPGDiscount), '0.00%')
--			   ) AS Term;

GO
