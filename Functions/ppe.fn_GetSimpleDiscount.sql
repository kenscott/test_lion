SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE FUNCTION [ppe].[fn_GetSimpleDiscount]
    (
      @FixedPrice DECIMAL(19, 8) ,
      @TradePrice DECIMAL(19, 8) ,
      @ExceptionDiscount DECIMAL(19, 8) ,
      @SPGDiscount DECIMAL(19, 8)
    )
RETURNS TABLE
AS
RETURN
    SELECT  IIF(@FixedPrice IS NOT NULL, 
	            ( @TradePrice - @FixedPrice ) / NULLIF(@TradePrice, 0), 
				ISNULL(@ExceptionDiscount, @SPGDiscount)
			   ) AS Discount;
GO
