SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
* Creates a table out of a delimited list
* Input:
*   @List NVARCHAR(4000) - delimited list to be split
*   @Del NVARCHAR(10) - delimiter (trailing spaces are ignored)
* Output: 2 Column table, with columns ListID int, and ListItem NVARCHAR(200)
* Updated 02/08/02 by Oskar Austegard
*/
CREATE FUNCTION [dbo].[fn_Split]
  (@List NVARCHAR(MAX),
   @Del NVARCHAR(10) = ',')
RETURNS @ListTable TABLE
  (ListID INT IDENTITY ,
   Item NVARCHAR(200))
AS
BEGIN
  DECLARE @Pos INT
  DECLARE @Item NVARCHAR(200)
  SET @Pos = CHARINDEX(@Del, @List)
  WHILE @Pos > 0
  BEGIN
    SET @Item = LTRIM(SUBSTRING(@List, 1, @Pos-1))
    INSERT @ListTable (Item)
    VALUES (@Item)
    SET @List = STUFF(@List, 1, @Pos+LEN(@Del)-1, '')
    SET @Pos = CHARINDEX(@Del, @List)    
  END
  
  INSERT @ListTable (Item)
  VALUES (LTRIM(@List))
  RETURN
END


GO
