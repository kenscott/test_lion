SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




CREATE FUNCTION [ppe].[fn_GetPriceIndex]
    (
      @Current DECIMAL (19,8) ,
      @Floor DECIMAL (19,8) ,
      @Target DECIMAL (19,8) ,
      @Stretch DECIMAL (19,8) 
    )
RETURNS TABLE
AS
RETURN 
    SELECT  CASE WHEN @Floor IS NULL OR @Target IS NULL OR @Stretch IS NULL THEN NULL 
	             WHEN @Current < @Floor THEN 1
                 WHEN @Current < @Floor + ( @Target - @Floor ) * 0.75 THEN 2
                 WHEN @Current < @Target + ( @Stretch - @Target ) * 0.25 THEN 3
                 WHEN @Current < @Stretch THEN 4
				 ELSE 5
            END AS PriceIndex
GO
