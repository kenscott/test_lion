SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO






CREATE FUNCTION [dbo].[fn_PriceQuantityBand]
 (@PlaybookPricingGroupKey INT,
  @Pctile NUMERIC(19,8)) --The percentile rank we care to find,

RETURNS DECIMAL(19,8)
AS
BEGIN
 DECLARE @Result DECIMAL(19,8)
 DECLARE @sqlstmt NVARCHAR(500)
 DECLARE @midCount INT

 DECLARE @tempPriceQuantityBand TABLE (RowID INT IDENTITY(1,1), col DECIMAL(19,8))

 INSERT @tempPriceQuantityBand (col)
 SELECT Total12MonthQuantity
 FROM PlaybookDataPointPricingGroup
 WHERE PlaybookPricingGroupKey = @PlaybookPricingGroupKey
	AND RulingMemberFlag = 'Y'
 ORDER BY Total12MonthQuantity

-- declare c_med cursor SCROLL for select col from @tempPriceQuantityBand
 SELECT @midCount = ROUND ( COUNT(*) * @Pctile,0 ) FROM @tempPriceQuantityBand
 IF @midCount = 0 SET @midCount = 1

--This gets the quantity at the data point that matches the % (rounded up to the nearest data point)
 SET @Result = (SELECT col FROM @tempPriceQuantityBand WHERE RowID = @midCount)

 IF @Result IS NULL SET @Result = 0
 RETURN @Result

END



















GO
