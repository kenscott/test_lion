SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE FUNCTION [lion].[fn_Max]
    (
      @a Money_Normal_type ,
      @b Money_Normal_type 
    )
RETURNS TABLE
AS
RETURN
    SELECT IIF(@a < @b, @b, isnull(@a, @b)) Max
GO
