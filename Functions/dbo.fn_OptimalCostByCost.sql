SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO








CREATE FUNCTION [dbo].[fn_OptimalCostByCost]
 (@PlaybookPricingGroupKey INT,
  @LowerBandQuantity DECIMAL(19,8),
  @UpperBandQuantity DECIMAL(19,8),
  @Pctile NUMERIC(19,8),  --The percentile rank we care to find
  @UseAllGroupDPs VARCHAR(10) = 'N')--
RETURNS DECIMAL(19,8)
AS
BEGIN
 DECLARE @Result DECIMAL(19,8)
 DECLARE @sqlstmt NVARCHAR(500)
 DECLARE @midCount INT

 DECLARE @tempOptimalCostByCost TABLE (RowID INT IDENTITY(1,1), col DECIMAL(19,8))

INSERT @tempOptimalCostByCost (col)
SELECT LastItemCost
FROM PlaybookDataPointPricingGroup A
WHERE A.PlaybookPricingGroupKey = @PlaybookPricingGroupKey
	AND 
	(	
		(@UseAllGroupDPs = 'N' AND A.Total12MonthQuantity BETWEEN @LowerBandQuantity AND @UpperBandQuantity)	--Should we only look at data points in this quantity band?
		OR
		(@UseAllGroupDPs = 'Y')	--Or should we look at all data points for the Costing group?
	)
AND RulingMemberFlag = 'Y'
ORDER BY LastItemCost


-- declare c_med cursor SCROLL for select col from @tempOptimalCostByCost
SELECT @midCount = ROUND ( COUNT(*) * @Pctile, 0) FROM @tempOptimalCostByCost
IF @midCount = 0 SET @midCount = 1
--This gets the quantity at the data point that matches the % (rounded up to the nearest data point)
SET @Result = (SELECT col FROM @tempOptimalCostByCost WHERE RowID = @midCount)

RETURN @Result

END





















GO
