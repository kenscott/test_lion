SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- User Defined Function

CREATE FUNCTION [lion].[fn_GetGPP]
    (
      @Price Money_Normal_Type ,
      @Cost Money_Normal_Type
    )
RETURNS TABLE
AS
RETURN
    SELECT  (@Price - @Cost) / NULLIF(@Price, 0) AS GPP
GO
