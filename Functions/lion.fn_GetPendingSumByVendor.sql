SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [lion].[fn_GetPendingSumByVendor]
	(
		@ContractID VARCHAR(9)
	)
RETURNS TABLE
AS
RETURN
	SELECT VendorNumber,
		   CurrentContractID,
		   SUM(ClaimLineValue) AS PendingClaimAmounts,
		   COUNT(ClaimJournalKey) AS PendingClaims
	FROM cm.ClaimJournal
	WHERE (ClaimJournalStatus = 'P')
		  AND CurrentContractID = @ContractID
	GROUP BY VendorNumber,
			 CurrentContractID

GO
