SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS OFF
GO
CREATE FUNCTION [dbo].[fn_CheckPassword] (@password [nvarchar] (4000), @password2 [nvarchar] (4000))
RETURNS [bit]
WITH EXECUTE AS CALLER
EXTERNAL NAME [BCrypt].[BCrypt].[CheckPassword]
GO
