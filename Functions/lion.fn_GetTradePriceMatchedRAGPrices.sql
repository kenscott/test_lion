SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE FUNCTION [lion].[fn_GetTradePriceMatchedRAGPrices]
(
	@TradePrice [DECIMAL](19, 3), 
	@ItemCost [DECIMAL](38, 3), 
	@FloorGPP [DECIMAL](19, 8), 
	@TargetGPP [DECIMAL](19, 8), 
	@StretchGPP [DECIMAL](19, 8),
	@ConvFact1 [DECIMAL](19, 3),
	@ConvFact2 [DECIMAL](19, 3)
)
RETURNS TABLE AS 
RETURN
	SELECT 
		ROUND(
			CASE
				WHEN @TradePrice > 0. AND ((@ItemCost * (@ConvFact2/@ConvFact1)) / (1. - @FloorGPP)) >= (1. - (0.01)) * @TradePrice THEN @TradePrice
				ELSE ((@ItemCost * (@ConvFact2/@ConvFact1)) / (1. - @FloorGPP))
			END * (@ConvFact1/@ConvFact2)
		, 3) AS RedPrice,
		ROUND(
			CASE
				WHEN @TradePrice > 0. AND ((@ItemCost * (@ConvFact2/@ConvFact1)) / (1. - @TargetGPP)) >= (1. - (0.01)) * @TradePrice THEN @TradePrice
				ELSE ((@ItemCost * (@ConvFact2/@ConvFact1)) / (1. - @TargetGPP))
			END * (@ConvFact1/@ConvFact2)
		, 3) AS AmberPrice,
		ROUND(
			CASE
				WHEN @TradePrice > 0. AND ((@ItemCost * (@ConvFact2/@ConvFact1)) / (1. - @StretchGPP)) >= (1. - (0.01)) * @TradePrice THEN @TradePrice
				ELSE ((@ItemCost * (@ConvFact2/@ConvFact1)) / (1. - @StretchGPP))
			END * (@ConvFact1/@ConvFact2)	
		, 3) AS GreenPrice
		-- the trade price is stored in a converted "base" if you will; thus we need to convert the passed in param cost to the same UOM 
		--      for comparaison and calculations

GO
