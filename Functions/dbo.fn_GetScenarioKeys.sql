SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE
FUNCTION [dbo].[fn_GetScenarioKeys] (
	@ApproachTypeKey Key_Small_type
)
RETURNS @Return TABLE 
(
	ScenarioKey INT,
	PlaybookDataPointGroupKey INT,
	ProjectKey INT
)

AS
BEGIN
	


/*

DECLARE
	@ScenarioKey Key_Normal_type,
	@PlaybookDataPointGroupKey Key_Normal_type,
	@ProjectKey Key_Normal_type

EXEC dbo.GetScenarioKeys 7, @ScenarioKey OUTPUT, @PlaybookDataPointGroupKey OUTPUT, @ProjectKey OUTPUT

SELECT '@ScenarioKey', @ScenarioKey
SELECT '@PlaybookDataPointGroupKey', @PlaybookDataPointGroupKey
SELECT '@ProjectKey', @ProjectKey



DECLARE
	@ScenarioKey Key_Normal_type,
	@PlaybookDataPointGroupKey Key_Normal_type,
	@ProjectKey Key_Normal_type

EXEC dbo.GetScenarioKeys 12, @ScenarioKey OUTPUT, @PlaybookDataPointGroupKey OUTPUT, @ProjectKey OUTPUT

SELECT '@ScenarioKey', @ScenarioKey
SELECT '@PlaybookDataPointGroupKey', @PlaybookDataPointGroupKey
SELECT '@ProjectKey', @ProjectKey

*/


; WITH Keys AS (
	SELECT DISTINCT
		a.ScenarioKey,
		PlaybookDataPointGroupKey,
		pp.ProjectKey,
		ROW_NUMBER() OVER (ORDER BY a.ScenarioKey DESC, PlaybookDataPointGroupKey DESC, pp.ProjectKey) AS RowRank
	FROM dbo.ATKScenario A (NOLOCK)
	INNER JOIN dbo.ATKProjectSpecificationScenario C (NOLOCK)
		ON C.ScenarioKey = A.ScenarioKey
	INNER JOIN dbo.PlaybookProject pp (NOLOCK)
		ON pp.ProjectSpecificationKey = c.ProjectSpecificationKey
	INNER JOIN dbo.PlaybookDataPointGroup pdpg (NOLOCK)
		ON pdpg.ProjectSpecificationScenarioKey = c.ProjectSpecificationScenarioKey
		AND pdpg.ProjectKey = pp.ProjectKey
	WHERE 
		a.ApproachTypeKey = @ApproachTypeKey
		AND pp.ProductionProjectIndicator = 'Y'
)
INSERT @Return (
	ScenarioKey,
	PlaybookDataPointGroupKey,
	ProjectKey
)
SELECT
	ScenarioKey,
	PlaybookDataPointGroupKey,
	ProjectKey
FROM Keys
WHERE RowRank = 1


RETURN


END










GO
