SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE FUNCTION [ppe].[fn_GetTerm]
    (
      @FixedPrice DECIMAL(19, 8) ,
      @ExceptionDiscount DECIMAL(19, 8) ,
      @SPGDiscount DECIMAL(19, 8),
	  @TermType VARCHAR(10) = 'TD'
    )
RETURNS TABLE
AS
RETURN
    SELECT 
		CASE ISNULL(@TermType,'TD') WHEN 'TD' THEN 
			CASE WHEN ISNULL(@ExceptionDiscount, @SPGDiscount) > 0
					 THEN ''
                     WHEN ISNULL(@ExceptionDiscount, @SPGDiscount) = 0
                     THEN '-'
                     ELSE '+'
             END + FORMAT(-ISNULL(@ExceptionDiscount, @SPGDiscount), '0.00%')

		ELSE 
			 FORMAT(@FixedPrice, 'C3', 'en-GB')
		END  AS Term;
GO
