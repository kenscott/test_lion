SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





CREATE FUNCTION [ppe].[fn_GetDiscount]
    (
      @AccountKey INT ,
      @ItemGroup3Key INT ,
      @ItemKey INT ,
      @Price DECIMAL(19, 8) ,
      @TradePrice DECIMAL(19, 8) ,
      @Band CHAR(1) = NULL 
    )
RETURNS TABLE
AS
RETURN
    SELECT  CASE WHEN @ItemKey = 1
                 THEN ( 1
                        - ISNULL(( SELECT  SUM(f.TotalQuantity * ((i.ItemUDDecimal3 - i.ItemUDDecimal4 - i.ItemUDDecimal5) / NULLIF( 1 - gpp.GPP, 0 )))
                                    / NULLIF(SUM(f.TotalQuantity * i.ItemUDDecimal1), 0)
							FROM    FactInvoiceLinePricebandScore f
							INNER JOIN dbo.DimItem i /*WITH(INDEX(PK_ItemDimension)) */ ON f.ItemKey = i.ItemKey
							LEFT OUTER JOIN ppe.RequestMain rm ON rm.AccountKey = f.AccountKey AND rm.ItemKey = f.ItemKey
							CROSS APPLY lion.fn_GetCappedGPP(i.ItemUDDecimal3 - i.ItemUDDecimal4 - i.ItemUDDecimal5,
														CASE @Band WHEN 'G' THEN f.StretchGPP
																	WHEN 'A' THEN f.TargetGPP
																	ELSE f.FloorGPP
														END, 
														0,
														i.ItemUDDecimal1) gpp
							WHERE   f.AccountKey = @AccountKey
									AND i.ItemGroup3Key = @ItemGroup3Key
									AND rm.ItemKey IS NULL --NOT EXISTS (SELECT TOP 1 * FROM ppe.RequestMain rm WHERE rm.AccountKey = f.AccountKey AND rm.ItemKey = f.ItemKey)

                          ), 1 ))
                 ELSE ( 1 - ( @Price / NULLIF(@TradePrice,0) ) )
            END AS Discount;
GO
