SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE  FUNCTION [dbo].[fn_Base64Encode] (@as_str NVARCHAR(MAX))
RETURNS NVARCHAR(MAX)
AS
BEGIN

	DECLARE @c1 INT
	DECLARE @c2 INT
	DECLARE @c3 INT
	DECLARE @w1 VARBINARY(400)
	DECLARE @w2 VARBINARY(400)
	DECLARE @w3 VARBINARY(400)
	DECLARE @w4 VARBINARY(400)
	DECLARE @str_out NVARCHAR(MAX)
	DECLARE @currentbyte INT

	SET @currentbyte = 1
	SET @str_out = ''

	WHILE(@currentbyte <= LEN(@as_str))
	BEGIN
		SET @c1 = ASCII(SUBSTRING(@as_str,@currentbyte + 0,1))
		SET @c2 = ASCII(SUBSTRING(@as_str,@currentbyte + 1,1) + CHAR(0))
		SET @c3 = ASCII(SUBSTRING(@as_str,@currentbyte + 2,1) + CHAR(0))
		SET @w1 = CAST(@c1 / 4 AS INT)  
		SET @w2 = ((@c1 & 3) * 16) + CAST(@c2 / 16 AS INT)
		IF LEN(@as_str)  >= @currentbyte+1
			SET @w3 = (@c2 & 15) * 4 + CAST(@c3 / 64 AS INT) 
		ELSE
			SET @w3 = -1
		IF LEN(@as_str)  >= @currentbyte+2
			SET @w4 = @c3 & 63
		ELSE
			SET @w4 = -1
		
		SET @str_out = @str_out + dbo.fn_Base64MimeEncode(@w1) + dbo.fn_Base64MimeEncode(@w2) + dbo.fn_Base64MimeEncode(@w3) + dbo.fn_Base64MimeEncode(@w4)		
		SET @currentbyte = @currentbyte + 3
	END
	RETURN @str_out
	
END




GO
