SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE 
FUNCTION [cg].[fn_GetCGPriceEnvelope] (
	@ParamPyramidCode NVARCHAR(2) = N'1',
	@ParamInstructionLevelType NVARCHAR(50),
	@ParamInstructionLevelCode Description_Small_type,
	@ParamAccountKey Key_Normal_type,
	@ParamVendorKey Key_Normal_type,
	@ParamJobFlag BIT,
	@ParamSales603010Bucket NVARCHAR(50) = N'Large',
	@ParamDebug CHAR(1) = N'N'
)
RETURNS @EnvelopeReturn TABLE 
	(
		PlaybookPricingGroupKey INT,
		ReturnCode INT,
	
		RedClaimBackPercent   DEC(19,8),
		AmberClaimBackPercent DEC(19,8),
		GreenClaimBackPercent DEC(19,8),

		RedClaimBack   DEC(19,8),
		AmberClaimBack DEC(19,8),
		GreenClaimBack DEC(19,8),
		DebugString VARCHAR(4000)
	)

AS
BEGIN




/*

EXEC cg.GetCGPriceEnvelope N'1', N'Item', N'1124688', 309836, 3514, 1, N'Sales Team'

SELECT *, LEN(DebugString) AS DebugStringLen FROM cg.fn_GetCGPriceEnvelope ( N'1', N'Item', N'1124688', 309836, 3514, 1, N'Sales Team', N'Y' )
SELECT *, LEN(DebugString) AS DebugStringLen FROM cg.fn_GetCGPriceEnvelope ( N'1', N'Item', N'1124688', 309836, 3514, 1, N'Sales Team', N'N' )

SELECT *, LEN(DebugString) AS DebugStringLen FROM cg.fn_GetCGPriceEnvelope ( N'1', N'Item', N'1124688', NULL, 3514, 1, N'Sales Team', N'N' )


SELECT *, LEN(DebugString) AS DebugStringLen FROM cg.fn_GetCGPriceEnvelope ( N'1', N'Item', N'1251002', 309836, 3000, 1, N'Sales Team', N'Y' )


/*** Item Pass 1***/
select * from dimvendor where vendornumber = '727C77'
select * from lion.vwitem where itemnumber = '1251002'
EXEC cg.GetCGPriceEnvelope2 N'1', N'Item', N'1251002', 309836, 3000, 1, N'Sales Team'
--,'y'
SELECT *, LEN(DebugString) AS DebugStringLen FROM cg.fn_GetCGPriceEnvelope ( N'1', N'Item', N'1251002', 309836, 3000, 1, N'Sales Team', N'n' )



/*** Item Pass 2 ***/
select * from lion.vwitem where itemnumber = '1D00024'
EXEC cg.GetCGPriceEnvelope2 N'1', N'Item', N'1D00024', 309836, 27, 0, N'Sales Team'
--,'y'
SELECT *, LEN(DebugString) AS DebugStringLen FROM cg.fn_GetCGPriceEnvelope ( N'1', N'Item', N'1D00024', 309836, 27, 0, N'Sales Team', N'Y' )



/*** Item Pass 3 ***/
select * from lion.vwitem where itemnumber = '1D08271'
EXEC cg.GetCGPriceEnvelope2 N'1', N'Item', N'1D08271', 309836, 27, 0, N'Sales Team'
--,'y'
SELECT *, LEN(DebugString) AS DebugStringLen FROM cg.fn_GetCGPriceEnvelope ( N'1', N'Item', N'1D08271', 309836, 27, 0, N'Sales Team', N'Y' )
SELECT *, LEN(DebugString) AS DebugStringLen FROM cg.fn_GetCGPriceEnvelope ( N'1', N'Item', N'1D08271999', 309836, 27, 0, N'Sales Team', N'Y' )



/*** LLSPG Pass 4 ***/
EXEC cg.GetCGPriceEnvelope2 N'1', N'LLSPG', N'DT75', 309836, 1246, 0, N'Sales Team'
--,'y'
--1631440
SELECT *, LEN(DebugString) AS DebugStringLen FROM cg.fn_GetCGPriceEnvelope ( N'1', N'LLSPG', N'DT75', 309836, 1246, 0, N'Sales Team', N'Y' )


/*** LLSPG Pass 5 ***/
EXEC cg.GetCGPriceEnvelope2 N'1', N'LLSPG', N'HR58', 309836, 14621, 0, N'Small/Cash'
--,'y'
--1633688
SELECT *, LEN(DebugString) AS DebugStringLen FROM cg.fn_GetCGPriceEnvelope ( N'1', N'LLSPG', N'HR58', 309836, 14621, 0, N'Small/Cash', N'Y' )


/*** LLSPG Pass 6 ***/
EXEC cg.GetCGPriceEnvelope2 N'1', N'LLSPG', N'IM19', 309836, 1223, 0, N'Small/Cash'
--,'y'
--1634514
SELECT *, LEN(DebugString) AS DebugStringLen FROM cg.fn_GetCGPriceEnvelope ( N'1', N'LLSPG', N'IM19', 309836, 1223, 0, N'Small/Cash', N'Y' )



/*** LLSPG Pass 4.2 MPG ***/
select top 1 mpgcode from lion.vwItem where LLSPGCode = 'DT75'
EXEC cg.GetCGPriceEnvelope2 N'1', N'MPG', N'DTCU', 309836, 1246, 0, N'Sales Team'
--,'y'
--1631440
SELECT *, LEN(DebugString) AS DebugStringLen FROM cg.fn_GetCGPriceEnvelope ( N'1', N'MPG', N'DTCU', 309836, 1246, 0, N'Sales Team', N'Y' )


/*** LLSPG Pass 5.2 MPG ***/
select top 1 mpgcode from lion.vwItem where LLSPGCode = 'HR58'
EXEC cg.GetCGPriceEnvelope2 N'1', N'MPG', N'HRNC', 309836, 14621, 0, N'Small/Cash'
--,'y'
--1633688
SELECT *, LEN(DebugString) AS DebugStringLen FROM cg.fn_GetCGPriceEnvelope ( N'1', N'MPG', N'HRNC', 309836, 14621, 0, N'Small/Cash', N'Y' )


/*** LLSPG Pass 6.2 MPG ***/
select top 1 mpgcode from lion.vwItem where LLSPGCode = 'IM19'
EXEC cg.GetCGPriceEnvelope2 N'1', N'MPG', N'IMGC', 309836, 1223, 0, N'Small/Cash'
--,'y'
SELECT *, LEN(DebugString) AS DebugStringLen FROM cg.fn_GetCGPriceEnvelope ( N'1', N'MPG', N'IMGC', 309836, 1223, 0, N'Small/Cash', N'Y' )


*/

--SET NOCOUNT ON


DECLARE
	@PlaybookPricingGroupKey Key_Normal_type,
	@ScenarioKey Key_Normal_type,
	@PlaybookDataPointGroupKey Key_Normal_type,
	@ProjectKey Key_Normal_type,

	@FloorPercentile DEC(19,8),
	@TargetPercentile DEC(19,8),
	@StretchPercentile DEC(19,8),
	@Offset DEC(19,8),

	@RedClaimBackPercent   DEC(19,8),
	@AmberClaimBackPercent DEC(19,8),
	@GreenClaimBackPercent DEC(19,8),

	@RedClaimBack   DEC(19,8),
	@AmberClaimBack DEC(19,8),
	@GreenClaimBack DEC(19,8),

	@CurrentInvoiceCost Money_Normal_type,

	@VendorKey Key_Normal_type,
	@AccountKey Key_Normal_type,
	@ItemKey Key_Normal_type,
	@ContractKey INT,
	@Sales603010Bucket NVARCHAR(50),

	@ItemNumber Description_Small_type,
	@LLSPGCode UDVarchar_type,
	@MPGCode UDVarchar_type,

	@DebugString VARCHAR(4000) = ''--CHAR(13) + CHAR(10)




SET @PlaybookPricingGroupKey = NULL

IF @ParamDebug = 'S'	-- S for stub/dummy data
BEGIN

	INSERT @EnvelopeReturn (
		PlaybookPricingGroupKey,
		ReturnCode,
		RedClaimBackPercent,
		AmberClaimBackPercent,
		GreenClaimBackPercent,
		RedClaimBack,
		AmberClaimBack,
		GreenClaimBack
	)

	SELECT
		CAST(1243337 AS INT) AS PlaybookPricingGroupKey,
		CAST(0 AS INT) AS ReturnCode,
	
		CAST(0.20 AS DEC(19,8)) AS RedClaimBackPercent,
		CAST(0.25 AS DEC(19,8)) AS AmberClaimBackPercent,
		CAST(0.30 AS DEC(19,8)) AS GreenClaimBackPercent,

		CAST(10.00 AS DEC(19,8)) AS RedClaimBack,
		CAST(12.50000001 AS DEC(19,8)) AS AmberClaimBack,
		CAST(15.00 AS DEC(19,8)) AS GreenClaimBack

		RETURN
END 

SELECT
	@ItemKey = ItemKey,
	@ItemNumber = ItemNumber,
	@LLSPGCode = IG3Level1,
	@CurrentInvoiceCost = CurrentInvoiceCost,
	@MPGCode = ItemUDVarChar3
FROM dbo.DimItem di WITH (NOLOCK)
INNER JOIN dbo.DimItemGroup3 dig3 WITH (NOLOCK)
	ON dig3.ItemGroup3Key = di.ItemGroup3Key
WHERE
	ItemNumber = @ParamInstructionLevelCode
	AND @ParamInstructionLevelType = N'Item'

IF @ParamInstructionLevelType = N'LLSPG'
	SELECT @LLSPGCode = @ParamInstructionLevelCode

IF @ParamInstructionLevelType = N'MPG'
BEGIN
	SELECT @MPGCode = @ParamInstructionLevelCode
	; WITH LLSPGLookup AS (
		SELECT
			IG3Level1,
			ROW_NUMBER() OVER (ORDER BY 
				CASE
					WHEN InActive = 0 AND ItemUDDate1 <> CAST('1900-01-01' AS DATE) AND ItemUDDate1 IS NOT NULL THEN 1
					WHEN InActive = 1 AND ItemUDDate1 <> CAST('1900-01-01' AS DATE) AND ItemUDDate1 IS NOT NULL THEN 2
					ELSE 3
				END ASC,
				IG3Level1 DESC
			) AS RowRank
		FROM dbo.DimItem di WITH (NOLOCK)
		INNER JOIN dbo.DimItemGroup3 dig3 WITH (NOLOCK)
			ON dig3.ItemGroup3Key = di.ItemGroup3Key
		WHERE ItemUDVarChar1 = @ParamPyramidCode
		AND ItemUDVarChar3 = @MPGCode
	)
	SELECT @LLSPGCode = IG3Level1
	FROM LLSPGLookup
	WHERE RowRank = 1
END

SELECT
	@VendorKey = VendorKey
FROM dbo.DimVendor WITH (NOLOCK)
WHERE
	VendorKey = @ParamVendorKey

SELECT
	@AccountKey = AccountKey
FROM dbo.DimAccount WITH (NOLOCK)
WHERE
	AccountKey = @ParamAccountKey

SELECT @Sales603010Bucket = ISNULL(@ParamSales603010Bucket, N'Large')

--SELECT
--	@ContractKey = ContractKey
--FROM cc.SupplierContract
--WHERE
--	ContractKey = @ParamContractKey


SELECT @ScenarioKey = ScenarioKey, @PlaybookDataPointGroupKey = PlaybookDataPointGroupKey, @ProjectKey = ProjectKey
FROM dbo.fn_GetScenarioKeys (12)	-- 12 is the approach type key


-- need to do some searching
SELECT 
	@PlaybookPricingGroupKey = NULLIF(PlaybookPricingGroupKey, -1),
	@DebugString = @DebugString + ISNULL(DebugString, '')
FROM cg.fn_GetEnvelopePlaybookPricingGroupKey(
	@ParamPyramidCode,
	@ItemNumber,
	@MPGCode,
	@LLSPGCode,
	@VendorKey,
	@ParamJobFlag,
	@Sales603010Bucket,
	@ScenarioKey,
	@PlaybookDataPointGroupKey,
	@ParamDebug
	)

IF @PlaybookPricingGroupKey IS NOT NULL
BEGIN

	SELECT
		@FloorPercentile = FloorPercentile,
		@TargetPercentile = TargetPercentile,
		@StretchPercentile = StretchPercentile,
		@Offset = Offset
	FROM
		cg.fn_GetPercentile() Pcts

	; WITH Datapoints AS (
		SELECT 
			PlaybookPricingGroupKey,
			ClaimBackPercent,
			RulingMemberCount,
			MemberRank
			--ROW_NUMBER() OVER (ORDER BY PDPGPP ASC) AS RowNumber
		FROM cg.PlaybookPricingGroupPriceBand ppgpb WITH (NOLOCK)
		WHERE ppgpb.PlaybookPricingGroupKey =  @PlaybookPricingGroupKey
	), TheFloor AS (
		SELECT 
			ClaimBackPercent + @Offset AS ClaimBackPercent
		FROM Datapoints
		WHERE MemberRank = ROUND (RulingMemberCount * @FloorPercentile, 0)
	), TheTarget AS (
		SELECT ClaimBackPercent + @Offset AS ClaimBackPercent
		FROM Datapoints
		WHERE MemberRank = ROUND (RulingMemberCount * @TargetPercentile, 0)
	), TheStretch AS (
		SELECT ClaimBackPercent + @Offset AS ClaimBackPercent
		FROM Datapoints
		WHERE MemberRank = ROUND (RulingMemberCount * @StretchPercentile, 0)
	)
	/* avoided using a temp table or table variable I did */
	SELECT
		@RedClaimBackPercent   = (SELECT ClaimBackPercent FROM TheFloor),
		@AmberClaimBackPercent = (SELECT ClaimBackPercent FROM TheTarget),
		@GreenClaimBackPercent = (SELECT ClaimBackPercent FROM TheStretch)

END


/* Determine values if cost was available */
IF ISNULL(@CurrentInvoiceCost, 0.) > 0. AND @RedClaimBackPercent IS NOT NULL
BEGIN

	SELECT @RedClaimBack   = @CurrentInvoiceCost * @RedClaimBackPercent
	SELECT @AmberClaimBack = @CurrentInvoiceCost * @AmberClaimBackPercent
	SELECT @GreenClaimBack = @CurrentInvoiceCost * @GreenClaimBackPercent
	
END



IF @ParamDebug = 'Y'
	BEGIN
		SELECT @DebugString = @DebugString 
			+ '@ParamPyramidCode: ' + ISNULL(@ParamPyramidCode, ' ') + CHAR(13) + CHAR(10)
			+ '@ParamInstructionLevelType: ' + ISNULL(@ParamInstructionLevelType, ' ') + CHAR(13) + CHAR(10)
			+ '@ParamInstructionLevelCode: ' + ISNULL(@ParamInstructionLevelCode, ' ') + CHAR(13) + CHAR(10)
			+ '@ParamAccountKey: ' + ISNULL(CAST(@ParamAccountKey AS VARCHAR(21)), ' ') + CHAR(13) + CHAR(10)
			+ '@ParamVendorKey: ' +  ISNULL(CAST(@ParamVendorKey AS VARCHAR(21)), ' ') + CHAR(13) + CHAR(10)
			+ '@ParamJobFlag: ' +  ISNULL(CAST(@ParamJobFlag AS VARCHAR(1)), ' ') + CHAR(13) + CHAR(10)
			+ '@ParamSales603010Bucket: ' + ISNULL(@ParamSales603010Bucket, ' ') + CHAR(13) + CHAR(10)
			+ '@PlaybookPricingGroupKey: ' + ISNULL(CAST(@PlaybookPricingGroupKey AS VARCHAR(21)), ' ') + CHAR(13) + CHAR(10)
			+ '@ScenarioKey: ' +  ISNULL(CAST(@ScenarioKey AS VARCHAR(21)), ' ') + CHAR(13) + CHAR(10)
			+ '@PlaybookDataPointGroupKey: ' +  ISNULL(CAST(@PlaybookDataPointGroupKey AS VARCHAR(21)), ' ') + CHAR(13) + CHAR(10)
			+ '@ProjectKey: ' +  ISNULL(CAST(@ProjectKey AS VARCHAR(21)), ' ') + CHAR(13) + CHAR(10)
			+ '@FloorPercentile: ' + ISNULL(CAST(@FloorPercentile AS VARCHAR(38)), ' ')  + CHAR(13) + CHAR(10)
			+ '@TargetPercentilev ' + ISNULL(CAST(@TargetPercentile AS VARCHAR(38)), ' ')  + CHAR(13) + CHAR(10)
			+ '@StretchPercentile: ' + ISNULL(CAST(@StretchPercentile AS VARCHAR(38)), ' ') + CHAR(13) + CHAR(10)
			+ '@Offset: ' +  ISNULL(CAST(@Offset AS VARCHAR(38)), ' ') + CHAR(13) + CHAR(10)
			+ '@RedClaimBackPercent: ' + ISNULL(CAST(@RedClaimBackPercent AS VARCHAR(38)), ' ') + CHAR(13) + CHAR(10)
			+ '@AmberClaimBackPercent: ' + ISNULL(CAST(@AmberClaimBackPercent AS VARCHAR(38)), ' ') + CHAR(13) + CHAR(10)
			+ '@GreenClaimBackPercent: ' +ISNULL( CAST(@GreenClaimBackPercent AS VARCHAR(38)), ' ') + CHAR(13) + CHAR(10)
			+ '@RedClaimBack: ' + ISNULL(CAST(@RedClaimBack AS VARCHAR(38)), ' ') + CHAR(13) + CHAR(10)
			+ '@AmberClaimBack: ' + ISNULL(CAST(@AmberClaimBack AS VARCHAR(38)), ' ') + CHAR(13) + CHAR(10)
			+ '@GreenClaimBack: ' + ISNULL(CAST(@GreenClaimBack AS VARCHAR(38)), ' ') + CHAR(13) + CHAR(10)
			+ '@CurrentInvoiceCost: ' + ISNULL(CAST(@CurrentInvoiceCost AS VARCHAR(38)), ' ') + CHAR(13) + CHAR(10)
			+ '@VendorKey: ' + ISNULL(CAST(@VendorKey AS VARCHAR(21)), ' ') + CHAR(13) + CHAR(10)
			+ '@AccountKey: ' + ISNULL(CAST(@AccountKey AS VARCHAR(21)), ' ') + CHAR(13) + CHAR(10)
			+ '@ItemKey: ' + ISNULL(CAST(@ItemKey AS VARCHAR(21)), ' ') + CHAR(13) + CHAR(10)
			+ '@ItemNumber: ' + ISNULL(@ItemNumber, ' ') + CHAR(13) + CHAR(10)
			+ '@MPGCode: ' + ISNULL(@MPGCode, ' ') + CHAR(13) + CHAR(10)
			+ '@LLSPGCode: ' + ISNULL(@LLSPGCode, ' ') + CHAR(13) + CHAR(10)
END



INSERT @EnvelopeReturn (
	PlaybookPricingGroupKey,
	ReturnCode,
	RedClaimBackPercent,
	AmberClaimBackPercent,
	GreenClaimBackPercent,
	RedClaimBack,
	AmberClaimBack,
	GreenClaimBack,
	DebugString
)
SELECT
	@PlaybookPricingGroupKey AS PlaybookPricingGroupKey,
	
	CAST(
			CASE
				--WHEN @ContractKey IS NULL THEN 5
				WHEN @VendorKey IS NULL THEN 4
				WHEN @AccountKey IS NULL AND @ParamAccountKey IS NOT NULL THEN 3
				WHEN @ItemKey IS NULL AND @ParamInstructionLevelType = 'Item' THEN 2
				WHEN @PlaybookPricingGroupKey IS NULL AND @RedClaimBackPercent IS NULL THEN 1
				ELSE 0
		END AS INT)
	AS ReturnCode,
	
	--NEWID() AS GUID,

	@RedClaimBackPercent   AS RedClaimBackPercent,
	@AmberClaimBackPercent AS AmberClaimBackPercent,
	@GreenClaimBackPercent AS GreenClaimBackPercent,

	@RedClaimBack   AS RedClaimBack,  
	@AmberClaimBack AS AmberClaimBack,
	@GreenClaimBack AS GreenClaimBack,
	@DebugString




RETURN


END


GO
