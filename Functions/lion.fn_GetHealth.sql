SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [lion].[fn_GetHealth]
	(
	@ContractKey INTEGER
	)
RETURNS TABLE
AS
RETURN
	SELECT ContractKey,
	MAX(LimitCurrentRatio) AS max_LimitCurrentRatio,
	Health = 
	CASE
		WHEN MAX(LimitCurrentRatio) IS NULL THEN 'Green'
		WHEN MAX(LimitCurrentRatio) <=89 THEN 'Green'
		WHEN MAX(LimitCurrentRatio) BETWEEN 90 AND 99 THEN 'Yellow'
		WHEN MAX(LimitCurrentRatio) >= 100 THEN 'Red'
	END 
	FROM [cc].[SupplierContractItem]
	GROUP BY ContractKey
  HAVING ContractKey = @ContractKey	
GO
