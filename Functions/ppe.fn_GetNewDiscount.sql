SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE FUNCTION [ppe].[fn_GetNewDiscount]
    (
      @ActionType			Description_Small_type,
      @ActionPercent		Percent_Type,
	  @CapType				Description_Small_type,
	  @CapPercent			Percent_Type,
	  @RecommendedDiscount	DECIMAL(38,6),
	  @RedDiscount			DECIMAL(19,8),
	  @AmberDiscount		DECIMAL(19,8),
	  @GreenDiscount		DECIMAL(19,8)

    )
RETURNS TABLE
AS
RETURN

		SELECT
			CASE
				WHEN CappedDiscountValue < 0 THEN 0									-- Global cap at @ trade: LPF-5164 PF-613 Amber to green to green change by 2%
				ELSE CappedDiscountValue
			END AS RecommendedDiscount
		FROM lion.fn_GetCappedDiscountValue(
			CASE @ActionType
				WHEN 'Change' THEN @RecommendedDiscount - (@ActionPercent)
				WHEN 'MoveToRed' THEN @RedDiscount
				WHEN 'MoveToAmber' THEN @AmberDiscount
				WHEN 'MoveToGreen' THEN @GreenDiscount
				WHEN 'Delete' THEN @RecommendedDiscount
				WHEN 'MoveToNext' THEN 
					CASE
						WHEN @RecommendedDiscount > @RedDiscount THEN @RedDiscount
						WHEN @RecommendedDiscount > @AmberDiscount THEN @AmberDiscount
						WHEN @RecommendedDiscount > @GreenDiscount THEN @GreenDiscount
						ELSE @RecommendedDiscount
					END
				WHEN 'MoveTo' THEN @ActionPercent
				ELSE @RecommendedDiscount
			END,
			CASE @CapType
				WHEN 'NoCap' THEN NULL
				WHEN 'CapAt' THEN (@CapPercent)
				WHEN 'CapAtRed' THEN @RedDiscount
				WHEN 'CapAtAmber' THEN @AmberDiscount
				WHEN 'CapAtGreen' THEN @GreenDiscount
				ELSE NULL
			END
		)

GO
