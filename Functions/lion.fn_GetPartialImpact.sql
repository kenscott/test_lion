SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- User Defined Function

CREATE FUNCTION [lion].[fn_GetPartialImpact]
    (
      @Bottom Percent_Type ,
      @Goal Percent_Type ,
      @Current Percent_Type ,
      @Factor Quantity_Normal_Type
    )
RETURNS TABLE
AS
RETURN
    SELECT  CASE WHEN (@Current < @Bottom) THEN 0
                 ELSE CASE WHEN ( @Goal - @Current ) > 0
                           THEN ( @Goal - @Current ) * @Factor
                           ELSE 0
                      END
            END AS [Impact]

GO
