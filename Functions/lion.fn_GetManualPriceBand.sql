SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE FUNCTION [lion].[fn_GetManualPriceBand]
(

	@ParamItemPyramidCode UDVarchar_type,
	@ParamLLSPGCode UDVarchar_type,
	@ParamItemNumber UDVarchar_type,
	@ParamPriceApproach UDVarchar_type,
	@ParamContractClaimsIndicator UDVarchar_type,
	@ParamRegion UDVarchar_type,
	@ParamArea UDVarchar_type,
	@ParamNetwork UDVarchar_type,
	@ParamBranch UDVarchar_type
)
RETURNS TABLE
AS
RETURN

/*
EXEC dba_findstring 'fn_GetManualPriceBand'
*/

WITH A AS (
	SELECT 		
		FloorGPP,
		TargetGPP,
		StretchGPP,
		ROW_NUMBER() OVER (
			ORDER BY 
				CASE
				/*				prior to LPF-5663
					WHEN ProductCode <> N'ALL' AND PriceApproach <> N'ALL' AND ContractClaimsIndicator <> N'ALL' AND Region <> N'ALL' AND Area <> N'ALL' AND Network <> N'ALL' THEN 1
					WHEN ProductCode <> N'ALL' AND PriceApproach <> N'ALL' AND ContractClaimsIndicator <> N'ALL' AND Region = N'ALL' AND Area <> N'ALL' AND Network <> N'ALL' THEN 2
					WHEN ProductCode <> N'ALL' AND PriceApproach <> N'ALL' AND ContractClaimsIndicator <> N'ALL' AND Region = N'ALL' AND Area = N'ALL' AND Network <> N'ALL' THEN 3
					WHEN ProductCode <> N'ALL' AND PriceApproach <> N'ALL' AND ContractClaimsIndicator <> N'ALL' AND Region = N'ALL' AND Area = N'ALL' AND Network = N'ALL' THEN 4
					WHEN ProductCode <> N'ALL' AND PriceApproach <> N'ALL' AND ContractClaimsIndicator = N'ALL' AND Region <> N'ALL' AND Area <> N'ALL' AND Network <> N'ALL' THEN 5
					WHEN ProductCode <> N'ALL' AND PriceApproach <> N'ALL' AND ContractClaimsIndicator = N'ALL' AND Region = N'ALL' AND Area <> N'ALL' AND Network <> N'ALL' THEN 6
					WHEN ProductCode <> N'ALL' AND PriceApproach <> N'ALL' AND ContractClaimsIndicator = N'ALL' AND Region = N'ALL' AND Area = N'ALL' AND Network <> N'ALL' THEN 7
					WHEN ProductCode <> N'ALL' AND PriceApproach <> N'ALL' AND ContractClaimsIndicator = N'ALL' AND Region = N'ALL' AND Area = N'ALL' AND Network = N'ALL' THEN 8
					WHEN ProductCode <> N'ALL' AND PriceApproach = N'ALL' AND ContractClaimsIndicator <> N'ALL' AND Region <> N'ALL' AND Area <> N'ALL' AND Network <> N'ALL' THEN 9
					WHEN ProductCode <> N'ALL' AND PriceApproach = N'ALL' AND ContractClaimsIndicator <> N'ALL' AND Region = N'ALL' AND Area <> N'ALL' AND Network <> N'ALL' THEN 10
					WHEN ProductCode <> N'ALL' AND PriceApproach = N'ALL' AND ContractClaimsIndicator <> N'ALL' AND Region = N'ALL' AND Area = N'ALL' AND Network <> N'ALL' THEN 11
					WHEN ProductCode <> N'ALL' AND PriceApproach = N'ALL' AND ContractClaimsIndicator <> N'ALL' AND Region = N'ALL' AND Area = N'ALL' AND Network = N'ALL' THEN 12
					WHEN ProductCode <> N'ALL' AND PriceApproach = N'ALL' AND ContractClaimsIndicator = N'ALL' AND Region <> N'ALL' AND Area <> N'ALL' AND Network <> N'ALL' THEN 13
					WHEN ProductCode <> N'ALL' AND PriceApproach = N'ALL' AND ContractClaimsIndicator = N'ALL' AND Region = N'ALL' AND Area <> N'ALL' AND Network <> N'ALL' THEN 14
					WHEN ProductCode <> N'ALL' AND PriceApproach = N'ALL' AND ContractClaimsIndicator = N'ALL' AND Region = N'ALL' AND Area = N'ALL' AND Network <> N'ALL' THEN 15
					WHEN ProductCode <> N'ALL' AND PriceApproach = N'ALL' AND ContractClaimsIndicator = N'ALL' AND Region = N'ALL' AND Area = N'ALL' AND Network = N'ALL' THEN 16
					WHEN ProductCode = N'ALL' AND PriceApproach <> N'ALL' AND ContractClaimsIndicator <> N'ALL' AND Region <> N'ALL' AND Area <> N'ALL' AND Network <> N'ALL' THEN 17
					WHEN ProductCode = N'ALL' AND PriceApproach <> N'ALL' AND ContractClaimsIndicator <> N'ALL' AND Region = N'ALL' AND Area <> N'ALL' AND Network <> N'ALL' THEN 18
					WHEN ProductCode = N'ALL' AND PriceApproach <> N'ALL' AND ContractClaimsIndicator <> N'ALL' AND Region = N'ALL' AND Area = N'ALL' AND Network <> N'ALL' THEN 19
					WHEN ProductCode = N'ALL' AND PriceApproach <> N'ALL' AND ContractClaimsIndicator <> N'ALL' AND Region = N'ALL' AND Area = N'ALL' AND Network = N'ALL' THEN 20
					WHEN ProductCode = N'ALL' AND PriceApproach <> N'ALL' AND ContractClaimsIndicator = N'ALL' AND Region <> N'ALL' AND Area <> N'ALL' AND Network <> N'ALL' THEN 21
					WHEN ProductCode = N'ALL' AND PriceApproach <> N'ALL' AND ContractClaimsIndicator = N'ALL' AND Region = N'ALL' AND Area <> N'ALL' AND Network <> N'ALL' THEN 22
					WHEN ProductCode = N'ALL' AND PriceApproach <> N'ALL' AND ContractClaimsIndicator = N'ALL' AND Region = N'ALL' AND Area = N'ALL' AND Network <> N'ALL' THEN 23
					WHEN ProductCode = N'ALL' AND PriceApproach <> N'ALL' AND ContractClaimsIndicator = N'ALL' AND Region = N'ALL' AND Area = N'ALL' AND Network = N'ALL' THEN 24
					WHEN ProductCode = N'ALL' AND PriceApproach = N'ALL' AND ContractClaimsIndicator <> N'ALL' AND Region <> N'ALL' AND Area <> N'ALL' AND Network <> N'ALL' THEN 25
					WHEN ProductCode = N'ALL' AND PriceApproach = N'ALL' AND ContractClaimsIndicator <> N'ALL' AND Region = N'ALL' AND Area <> N'ALL' AND Network <> N'ALL' THEN 26
					WHEN ProductCode = N'ALL' AND PriceApproach = N'ALL' AND ContractClaimsIndicator <> N'ALL' AND Region = N'ALL' AND Area = N'ALL' AND Network <> N'ALL' THEN 27
					WHEN ProductCode = N'ALL' AND PriceApproach = N'ALL' AND ContractClaimsIndicator <> N'ALL' AND Region = N'ALL' AND Area = N'ALL' AND Network = N'ALL' THEN 28
					WHEN ProductCode = N'ALL' AND PriceApproach = N'ALL' AND ContractClaimsIndicator = N'ALL' AND Region <> N'ALL' AND Area <> N'ALL' AND Network <> N'ALL' THEN 29
					WHEN ProductCode = N'ALL' AND PriceApproach = N'ALL' AND ContractClaimsIndicator = N'ALL' AND Region = N'ALL' AND Area <> N'ALL' AND Network <> N'ALL' THEN 30
					WHEN ProductCode = N'ALL' AND PriceApproach = N'ALL' AND ContractClaimsIndicator = N'ALL' AND Region = N'ALL' AND Area = N'ALL' AND Network <> N'ALL' THEN 31
					WHEN ProductCode = N'ALL' AND PriceApproach = N'ALL' AND ContractClaimsIndicator = N'ALL' AND Region = N'ALL' AND Area = N'ALL' AND Network = N'ALL' THEN 32
			*/
					-- LPF-5663 Need New Precedent Rules that Incorporate Branch, Kbs 07.21.2017
					WHEN ProductCode <> N'ALL' AND PriceApproach <> N'ALL' AND ContractClaimsIndicator <> N'ALL' AND Region <> N'ALL' AND Area <> N'ALL' AND Network <> N'ALL' AND Branch <> N'ALL' THEN 1
					WHEN ProductCode <> N'ALL' AND PriceApproach <> N'ALL' AND ContractClaimsIndicator <> N'ALL' AND Region = N'ALL' AND Area <> N'ALL' AND Network <> N'ALL' AND Branch <> N'ALL'THEN 2
					WHEN ProductCode <> N'ALL' AND PriceApproach <> N'ALL' AND ContractClaimsIndicator <> N'ALL' AND Region = N'ALL' AND Area = N'ALL' AND Network <> N'ALL' AND Branch <> N'ALL'THEN 3
					WHEN ProductCode <> N'ALL' AND PriceApproach <> N'ALL' AND ContractClaimsIndicator <> N'ALL' AND Region = N'ALL' AND Area = N'ALL' AND Network = N'ALL' AND Branch <> N'ALL'THEN 4
					WHEN ProductCode <> N'ALL' AND PriceApproach <> N'ALL' AND ContractClaimsIndicator <> N'ALL' AND Region = N'ALL' AND Area = N'ALL' AND Network = N'ALL' AND Branch = N'ALL'THEN 5	
					WHEN ProductCode <> N'ALL' AND PriceApproach <> N'ALL' AND ContractClaimsIndicator = N'ALL' AND Region <> N'ALL' AND Area <> N'ALL' AND Network <> N'ALL' AND Branch <> N'ALL'THEN 6
					WHEN ProductCode <> N'ALL' AND PriceApproach <> N'ALL' AND ContractClaimsIndicator = N'ALL' AND Region = N'ALL' AND Area <> N'ALL' AND Network <> N'ALL' AND Branch <> N'ALL'THEN 7
					WHEN ProductCode <> N'ALL' AND PriceApproach <> N'ALL' AND ContractClaimsIndicator = N'ALL' AND Region = N'ALL' AND Area = N'ALL' AND Network <> N'ALL' AND Branch <> N'ALL'THEN 8
					WHEN ProductCode <> N'ALL' AND PriceApproach <> N'ALL' AND ContractClaimsIndicator = N'ALL' AND Region = N'ALL' AND Area = N'ALL' AND Network = N'ALL' AND Branch <> N'ALL'THEN 9
					WHEN ProductCode <> N'ALL' AND PriceApproach <> N'ALL' AND ContractClaimsIndicator = N'ALL' AND Region = N'ALL' AND Area = N'ALL' AND Network = N'ALL' AND Branch = N'ALL'THEN 10
					WHEN ProductCode <> N'ALL' AND PriceApproach = N'ALL' AND ContractClaimsIndicator <> N'ALL' AND Region <> N'ALL' AND Area <> N'ALL' AND Network <> N'ALL' AND Branch <> N'ALL'THEN 11
					WHEN ProductCode <> N'ALL' AND PriceApproach = N'ALL' AND ContractClaimsIndicator <> N'ALL' AND Region = N'ALL' AND Area <> N'ALL' AND Network <> N'ALL' AND Branch <> N'ALL' THEN 12
					WHEN ProductCode <> N'ALL' AND PriceApproach = N'ALL' AND ContractClaimsIndicator <> N'ALL' AND Region = N'ALL' AND Area = N'ALL' AND Network <> N'ALL' AND Branch <> N'ALL'THEN 13
					WHEN ProductCode <> N'ALL' AND PriceApproach = N'ALL' AND ContractClaimsIndicator <> N'ALL' AND Region = N'ALL' AND Area = N'ALL' AND Network = N'ALL' AND Branch <> N'ALL'THEN 14
					WHEN ProductCode <> N'ALL' AND PriceApproach = N'ALL' AND ContractClaimsIndicator <> N'ALL' AND Region = N'ALL' AND Area = N'ALL' AND Network = N'ALL' AND Branch = N'ALL'THEN 15
					WHEN ProductCode <> N'ALL' AND PriceApproach = N'ALL' AND ContractClaimsIndicator = N'ALL' AND Region <> N'ALL' AND Area <> N'ALL' AND Network <> N'ALL' AND Branch <> N'ALL'THEN 16
					WHEN ProductCode <> N'ALL' AND PriceApproach = N'ALL' AND ContractClaimsIndicator = N'ALL' AND Region = N'ALL' AND Area <> N'ALL' AND Network <> N'ALL' AND Branch <> N'ALL'THEN 17
					WHEN ProductCode <> N'ALL' AND PriceApproach = N'ALL' AND ContractClaimsIndicator = N'ALL' AND Region = N'ALL' AND Area = N'ALL' AND Network <> N'ALL' AND Branch <> N'ALL'THEN 18
					WHEN ProductCode <> N'ALL' AND PriceApproach = N'ALL' AND ContractClaimsIndicator = N'ALL' AND Region = N'ALL' AND Area = N'ALL' AND Network = N'ALL' AND Branch <> N'ALL'THEN 19
					WHEN ProductCode <> N'ALL' AND PriceApproach = N'ALL' AND ContractClaimsIndicator = N'ALL' AND Region = N'ALL' AND Area = N'ALL' AND Network = N'ALL' AND Branch = N'ALL'THEN 20
					WHEN ProductCode = N'ALL' AND PriceApproach <> N'ALL' AND ContractClaimsIndicator <> N'ALL' AND Region <> N'ALL' AND Area <> N'ALL' AND Network <> N'ALL' AND Branch <> N'ALL'THEN 21
					WHEN ProductCode = N'ALL' AND PriceApproach <> N'ALL' AND ContractClaimsIndicator <> N'ALL' AND Region = N'ALL' AND Area <> N'ALL' AND Network <> N'ALL' AND Branch <> N'ALL'THEN 22
					WHEN ProductCode = N'ALL' AND PriceApproach <> N'ALL' AND ContractClaimsIndicator <> N'ALL' AND Region = N'ALL' AND Area = N'ALL' AND Network <> N'ALL' AND Branch <> N'ALL'THEN 23
					WHEN ProductCode = N'ALL' AND PriceApproach <> N'ALL' AND ContractClaimsIndicator <> N'ALL' AND Region = N'ALL' AND Area = N'ALL' AND Network = N'ALL' AND Branch <> N'ALL'THEN 24
					WHEN ProductCode = N'ALL' AND PriceApproach <> N'ALL' AND ContractClaimsIndicator <> N'ALL' AND Region = N'ALL' AND Area = N'ALL' AND Network = N'ALL' AND Branch = N'ALL'THEN 25
					WHEN ProductCode = N'ALL' AND PriceApproach <> N'ALL' AND ContractClaimsIndicator = N'ALL' AND Region <> N'ALL' AND Area <> N'ALL' AND Network <> N'ALL' AND Branch <> N'ALL'THEN 26
					WHEN ProductCode = N'ALL' AND PriceApproach <> N'ALL' AND ContractClaimsIndicator = N'ALL' AND Region = N'ALL' AND Area <> N'ALL' AND Network <> N'ALL' AND Branch <> N'ALL'THEN 27
					WHEN ProductCode = N'ALL' AND PriceApproach <> N'ALL' AND ContractClaimsIndicator = N'ALL' AND Region = N'ALL' AND Area = N'ALL' AND Network <> N'ALL' AND Branch <> N'ALL'THEN 28
					WHEN ProductCode = N'ALL' AND PriceApproach <> N'ALL' AND ContractClaimsIndicator = N'ALL' AND Region = N'ALL' AND Area = N'ALL' AND Network = N'ALL' AND Branch <> N'ALL'THEN 29
					WHEN ProductCode = N'ALL' AND PriceApproach <> N'ALL' AND ContractClaimsIndicator = N'ALL' AND Region = N'ALL' AND Area = N'ALL' AND Network = N'ALL' AND Branch = N'ALL'THEN 30
					WHEN ProductCode = N'ALL' AND PriceApproach = N'ALL' AND ContractClaimsIndicator <> N'ALL' AND Region <> N'ALL' AND Area <> N'ALL' AND Network <> N'ALL' AND Branch <> N'ALL'THEN 31
					WHEN ProductCode = N'ALL' AND PriceApproach = N'ALL' AND ContractClaimsIndicator <> N'ALL' AND Region = N'ALL' AND Area <> N'ALL' AND Network <> N'ALL' AND Branch <> N'ALL'THEN 32
					WHEN ProductCode = N'ALL' AND PriceApproach = N'ALL' AND ContractClaimsIndicator <> N'ALL' AND Region = N'ALL' AND Area = N'ALL' AND Network <> N'ALL' AND Branch <> N'ALL'THEN 33
					WHEN ProductCode = N'ALL' AND PriceApproach = N'ALL' AND ContractClaimsIndicator <> N'ALL' AND Region = N'ALL' AND Area = N'ALL' AND Network = N'ALL' AND Branch <> N'ALL'THEN 34
					WHEN ProductCode = N'ALL' AND PriceApproach = N'ALL' AND ContractClaimsIndicator <> N'ALL' AND Region = N'ALL' AND Area = N'ALL' AND Network = N'ALL' AND Branch = N'ALL'THEN 35
					WHEN ProductCode = N'ALL' AND PriceApproach = N'ALL' AND ContractClaimsIndicator = N'ALL' AND Region <> N'ALL' AND Area <> N'ALL' AND Network <> N'ALL' AND Branch <> N'ALL'THEN 36
					WHEN ProductCode = N'ALL' AND PriceApproach = N'ALL' AND ContractClaimsIndicator = N'ALL' AND Region = N'ALL' AND Area <> N'ALL' AND Network <> N'ALL' AND Branch <> N'ALL'THEN 37
					WHEN ProductCode = N'ALL' AND PriceApproach = N'ALL' AND ContractClaimsIndicator = N'ALL' AND Region = N'ALL' AND Area = N'ALL' AND Network <> N'ALL' AND Branch <> N'ALL'THEN 38
					WHEN ProductCode = N'ALL' AND PriceApproach = N'ALL' AND ContractClaimsIndicator = N'ALL' AND Region = N'ALL' AND Area = N'ALL' AND Network = N'ALL' AND Branch <> N'ALL'THEN 39
					WHEN ProductCode = N'ALL' AND PriceApproach = N'ALL' AND ContractClaimsIndicator = N'ALL' AND Region = N'ALL' AND Area = N'ALL' AND Network = N'ALL' AND Branch = N'ALL'THEN 40
				END ASC,
				mpb.ManualPriceBandKey DESC
		) AS RowRank
	FROM lion.ATKManualPriceBand mpb
	WHERE
		ItemPyramidCode = @ParamItemPyramidCode
		AND LLSPGCode = @ParamLLSPGCode
		AND (ItemPyramidCode + ProductCode = @ParamItemNumber OR ProductCode = N'ALL')
		AND (PriceApproach = @ParamPriceApproach OR PriceApproach = N'ALL')
		AND (ContractClaimsIndicator = @ParamContractClaimsIndicator OR ContractClaimsIndicator = N'ALL')
		AND (Region = @ParamRegion OR Region = N'ALL')
		AND (Area = @ParamArea OR Area = N'ALL')
		AND (Network = @ParamNetwork OR Network = N'ALL')
		AND (Branch = @ParamBranch OR Branch = N'ALL')
)
SELECT
		FloorGPP AS MPBFloorGPP,
		TargetGPP AS MPBTargetGPP,
		StretchGPP AS MPBStretchGPP
FROM A
WHERE 
	RowRank = 1

GO
