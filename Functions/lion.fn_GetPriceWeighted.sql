SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE FUNCTION [lion].[fn_GetPriceWeighted]
    (
      @TotalCost Money_Normal_type ,
	  @TotalQuantity Quantity_Normal_type,
      @GPP Percent_type
    )
RETURNS TABLE
AS
RETURN
    SELECT
		CAST(
				(@TotalCost / NULLIF( 1 - @GPP, 0)) /NULLIF(@TotalQuantity, 0.) 
			AS DEC(19,8)
		) AS [Price] 

GO
