SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




CREATE FUNCTION [cg].[fn_GetPercentile]
(
--	@ParamBrand UDVarchar_type,
--	@ParamPlaybookRegion UDVarchar_type,
--	@ParamBranch UDVarchar_type,
--	@ParamSales603010Bucket UDVarchar_type,
--	@ParamItemPyramidCode UDVarchar_type,
--	@ParamLLSPGCode UDVarchar_type
)
RETURNS TABLE
AS
RETURN

/*

*/

WITH A AS (
	SELECT TOP 1
		FloorPercentile,
		TargetPercentile,
		StretchPercentile,
		Offset,
		ROW_NUMBER() OVER (ORDER BY SequenceNumber DESC) AS RowRank
	FROM cg.ATKBandPercentile p
	--WHERE 		
	--	(Brand = @ParamBrand OR Brand = 'ALL')
	--	AND (PlaybookRegion = @ParamPlaybookRegion OR PlaybookRegion = 'ALL')
	--	AND (Branch = @ParamBranch OR Branch = 'ALL')
	--	AND (Sales603010Bucket = @ParamSales603010Bucket OR Sales603010Bucket = 'ALL')
	--	AND (ItemPyramidCode = @ParamItemPyramidCode OR ItemPyramidCode = 'ALL')
	--	AND (LLSPGCode = @ParamLLSPGCode OR LLSPGCode = 'ALL')
)
SELECT
	FloorPercentile,
	TargetPercentile,
	StretchPercentile,
	Offset
FROM A
WHERE 
	RowRank = 1


GO
