SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE FUNCTION [lion].[fn_GetScore]
    (
      @Current DECIMAL (19,8) ,
      @Floor DECIMAL (19,8) ,
      @LowTarget DECIMAL (19,8) ,
      @HighTarget DECIMAL (19,8) ,
      @Stretch DECIMAL (19,8) 
    )
RETURNS TABLE
AS
RETURN
    SELECT  CASE WHEN @Current < @Floor THEN 1
                 ELSE CASE WHEN @Current < @LowTarget THEN 2
                           ELSE CASE WHEN @Current < @HighTarget THEN 3
                                     ELSE CASE WHEN @Current < @Stretch
                                               THEN 4
                                               ELSE 5
                                          END
                                END
                      END
            END AS Score


GO
