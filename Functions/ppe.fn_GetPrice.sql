SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE FUNCTION [ppe].[fn_GetPrice]
    (
      @FixedPrice DECIMAL(19, 8) ,
      @TradePrice DECIMAL(19, 8) ,
      @ExceptionDiscount DECIMAL(19, 8) ,
      @SPGDiscount DECIMAL(19, 8)
    )
RETURNS TABLE
AS
RETURN
    SELECT  IIF(@FixedPrice IS NOT NULL, 
	            @FixedPrice, 
				ROUND(@TradePrice * ( 1 - ISNULL(@ExceptionDiscount, @SPGDiscount) ), 3)
			   ) AS Price;
GO
