SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS OFF
GO









CREATE FUNCTION [dbo].[fn_GetFilterdefinitionForIndex]
	(
		@SchemaName NVARCHAR(MAX),
		@TableName NVARCHAR(MAX),
		@IndexName NVARCHAR(MAX)
	)
RETURNS NVARCHAR(MAX)

AS

	
/* 
	sp_helpindex DimPerson
	select dbo.fn_GetFilterdefinitionForIndex('dbo', 'DimPerson', 'I_DimPerson_1')
	select dbo.fn_GetFilterdefinitionForIndex('dbo', 'DimPerson', 'I_DimPerson_2')
*/


BEGIN
	DECLARE 
		@Result NVARCHAR(MAX)
	
	SELECT @Result = ''

	SELECT TOP 1

		@Result = i.filter_definition
	FROM
		sys.tables AS st
	JOIN sys.schemas AS sts 
		ON sts.schema_id = st.schema_id
	JOIN sys.indexes AS i 
		ON st.object_id = i.object_id
	JOIN sys.index_columns AS ic 
		ON i.object_id = ic.object_id
		AND i.index_id = ic.index_id
	JOIN sys.columns AS c 
		ON i.object_id = c.object_id
		AND ic.column_id = c.column_id
	WHERE 
		sts.name = @SchemaName
		AND st.name = @TableName
		AND i.name = @IndexName
		AND i.has_filter = 1
	ORDER BY 
		sts.name,
		st.name,
		i.OBJECT_ID,
		i.index_id,
	    ic.is_included_column ASC,
        ic.key_ordinal,
        index_column_id

	RETURN NULLIF(@Result, '')
END









GO
