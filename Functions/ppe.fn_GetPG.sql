SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE FUNCTION [ppe].[fn_GetPG]
    (
      @ActualDiscount DECIMAL(19, 8) ,
      @AmberDiscount DECIMAL(19, 8) ,
      @GreenDiscount DECIMAL(19, 8)
    )
RETURNS TABLE
AS
RETURN
    SELECT  CASE WHEN @AmberDiscount IS NULL OR @GreenDiscount IS NULL THEN NULL 
				 WHEN @ActualDiscount > @AmberDiscount THEN 'R'
                 WHEN @ActualDiscount > @GreenDiscount THEN 'A'
                 ELSE 'G'
            END AS PG;
GO
