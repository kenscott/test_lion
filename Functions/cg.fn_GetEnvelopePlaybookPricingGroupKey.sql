SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE 
FUNCTION [cg].[fn_GetEnvelopePlaybookPricingGroupKey]
(
	@ParamPyramidCode NVARCHAR(2),
	@ParamItemNumber Description_Small_type,
	@ParamMPGCode UDVarchar_type,
	@ParamLLSPGCode UDVarchar_type,
	@ParamVendorKey Key_Normal_type,
	@ParamJobFlag BIT,
	@ParamSales603010Bucket NVARCHAR(50),
	@ScenarioKey Key_Normal_type,
	@PlaybookDataPointGroupKey Key_Normal_type,	
	@ParamDebug CHAR(1) = 'N'
)
RETURNS @ReturnTable TABLE 
	(
		PlaybookPricingGroupKey INT,
		DebugString VARCHAR(4000)
	)
AS

/*

DECLARE @TheOut Key_Normal_type

EXEC cg.GetEnvelopePlaybookPricingGroupKey
	'1',
	'1124688',
	'HDQH',
	'HD54', --llspg
	3514,
	1,
	'Sales Team',
	17,
	20,
	@TheOut OUTPUT,
	'y'

SELECT '@TheOut', @TheOut



DECLARE @TheOut Key_Normal_type

EXEC cg.GetEnvelopePlaybookPricingGroupKey
	'1',
	NULL,--'1124688AAA',
	'HDQH',
	'HD54', --llspg
	3514,
	1,
	'Sales Team',
	17,
	20,
	@TheOut OUTPUT,
	'y'



DECLARE @TheOut Key_Normal_type

EXEC cg.GetEnvelopePlaybookPricingGroupKey
	'1',
	NULL,--'1124688AAA',
	NULL, --'HDQH',
	'HD54', --llspg
	3514,
	1,
	'Sales Team',
	17,
	20,
	@TheOut OUTPUT,
	'y'



DECLARE @TheOut Key_Normal_type

EXEC cg.GetEnvelopePlaybookPricingGroupKey
	'1',
	'1F01991',
	'DATJ',
	'DA40', --llspg
	1223,
	0,
	'Sales Team',
	17,
	20,
	@TheOut OUTPUT,
	'y'


DECLARE @TheOut Key_Normal_type

EXEC cg.GetEnvelopePlaybookPricingGroupKey
	'1',
	null,--'1F01991',
	'DA26',
	'DA26', --llspg
	7554,
	0,
	'Sales Team',
	17,
	20,
	@TheOut OUTPUT,
	'y'

	DECLARE @TheOut Key_Normal_type

	EXEC cg.GetEnvelopePlaybookPricingGroupKey
		'1',
		null,--'1F01991',
		'DA37',
		'DA37', --llspg
		2451,
		0,
		null,
		17,
		20,
		@TheOut OUTPUT,
		'y'
*/

BEGIN


DECLARE
	@GroupingColumnsSearchStr NVARCHAR(MAX),
	@GroupValuesSearchStr NVARCHAR(MAX),
	@SQLString NVARCHAR(MAX),
	@PricingRuleSequenceNumber INT,
	@SectionNumber SMALLINT,
	@PricingRuleAttributeSequenceNumber INT,
	@PricingRuleAttributeType NVARCHAR(250),
	@PricingRuleAttributeSourceSchema NVARCHAR(256),
	@PricingRuleAttributeSourceTable NVARCHAR(250),
	@DefaultValueForMatch Description_Normal_type,
	@StrValue NVARCHAR(255),
	@PlaybookPricingGroupKey Key_Normal_type,
	@DebugString VARCHAR(4000) = ''


SET @GroupValuesSearchStr = N''
SET @GroupingColumnsSearchStr = N''
SET @SQLString = N''


DECLARE PricingRuleSequenceCursor 
CURSOR 
FAST_FORWARD 
FOR
	SELECT 
		--TOP 1
		PricingRuleSequenceNumber,
		SectionNumber,
		PricingRuleAttributeSequenceNumber,
		PricingRuleAttributeType,
		PricingRuleAttributeSourceSchema,
		PricingRuleAttributeSourceTable,
		DefaultValueForMatch
	FROM dbo.ATKPricingRuleAttribute pra WITH (NOLOCK)
	INNER JOIN dbo.ATKPricingRuleAttributeType prat WITH (NOLOCK)
		ON prat.PricingRuleAttributeTypeKey = pra.PricingRuleAttributeTypeKey
	INNER JOIN dbo.ATKPricingRule pr WITH (NOLOCK)
		ON pr.PricingRuleKey = pra.PricingRuleKey
	INNER JOIN dbo.ATKScenario s WITH (NOLOCK)
		ON s.PricingRulePlaybookKey = pr.PricingRulePlaybookKey
	WHERE ScenarioKey = @ScenarioKey
	ORDER BY
		pr.PricingRuleSequenceNumber,
		pra.PricingRuleAttributeSequenceNumber

OPEN PricingRuleSequenceCursor

FETCH NEXT FROM PricingRuleSequenceCursor 
INTO 	
	@PricingRuleSequenceNumber,
	@SectionNumber,
	@PricingRuleAttributeSequenceNumber,
	@PricingRuleAttributeType,
	@PricingRuleAttributeSourceSchema,
	@PricingRuleAttributeSourceTable,
	@DefaultValueForMatch


SET @PlaybookPricingGroupKey = NULL 


WHILE (@@FETCH_STATUS = 0) AND (@PlaybookPricingGroupKey IS NULL)
BEGIN

	IF ((@SectionNumber = 1 AND @ParamItemNumber IS NOT NULL) OR @SectionNumber <> 1) --or 1=1
	BEGIN

		/** override(s) and shortcuts for performance **/
		--  select * from dbo.ATKPricingRuleAttributeType order by 5,2,3
		SELECT @StrValue = 
			CASE 
				WHEN @PricingRuleAttributeSourceTable = N'cc.SupplierContract' AND @PricingRuleAttributeType = N'JobFlag' AND @ParamJobFlag IS NOT NULL THEN CAST(@ParamJobFlag AS NVARCHAR(1))
				WHEN @PricingRuleAttributeSourceTable = N'cc.SupplierContract' AND @PricingRuleAttributeType = N'PyramidCode' AND @ParamJobFlag IS NOT NULL THEN @ParamPyramidCode
				WHEN @PricingRuleAttributeSourceTable = N'cc.SupplierContract' AND @PricingRuleAttributeType = N'VendorKey' AND @ParamJobFlag IS NOT NULL THEN CAST(@ParamVendorKey AS NVARCHAR(25))
		
				WHEN @PricingRuleAttributeSourceSchema = N'cc' AND @PricingRuleAttributeSourceTable = N'SupplierContract' AND @PricingRuleAttributeType = N'JobFlag' AND @ParamJobFlag IS NOT NULL THEN CAST(@ParamJobFlag AS NVARCHAR(1))
				WHEN @PricingRuleAttributeSourceSchema = N'cc' AND @PricingRuleAttributeSourceTable = N'SupplierContract' AND @PricingRuleAttributeType = N'PyramidCode' AND @ParamJobFlag IS NOT NULL THEN @ParamPyramidCode
				WHEN @PricingRuleAttributeSourceSchema = N'cc' AND @PricingRuleAttributeSourceTable = N'SupplierContract' AND @PricingRuleAttributeType = N'VendorKey' AND @ParamJobFlag IS NOT NULL THEN CAST(@ParamVendorKey AS NVARCHAR(25))
		
				WHEN @PricingRuleAttributeSourceSchema = N'cg' AND @PricingRuleAttributeSourceTable = N'PlaybookDatapoint' AND @PricingRuleAttributeType = N'Sales603010Bucket' AND @ParamSales603010Bucket IS NOT NULL THEN @ParamSales603010Bucket

				WHEN @PricingRuleAttributeSourceSchema = N'cg' AND @PricingRuleAttributeSourceTable = N'PlaybookDatapoint' AND @PricingRuleAttributeType = N'ItemNumber' AND @ParamItemNumber IS NOT NULL THEN @ParamItemNumber
				WHEN @PricingRuleAttributeSourceSchema = N'cg' AND @PricingRuleAttributeSourceTable = N'PlaybookDatapoint' AND @PricingRuleAttributeType = N'MPGCode' AND @ParamMPGCode IS NOT NULL THEN @ParamMPGCode
				WHEN @PricingRuleAttributeSourceSchema = N'cg' AND @PricingRuleAttributeSourceTable = N'PlaybookDatapoint' AND @PricingRuleAttributeType = N'LLSPGCode' AND @ParamLLSPGCode IS NOT NULL THEN @ParamLLSPGCode

				WHEN @PricingRuleAttributeSourceSchema = N'cg' AND @PricingRuleAttributeSourceTable = N'PlaybookDatapoint' AND @PricingRuleAttributeType = N'InstructionLevelType' AND @SectionNumber = 1 THEN 'Item'
				WHEN @PricingRuleAttributeSourceSchema = N'cg' AND @PricingRuleAttributeSourceTable = N'PlaybookDatapoint' AND @PricingRuleAttributeType = N'InstructionLevelType' AND @SectionNumber <> 1 THEN 'LLSPG'

				ELSE N''
			END

			IF @StrValue = N''
			BEGIN
				SELECT
					@SQLString = N''

				-- something is bad/wrong if we get here
				
				SELECT @DebugString = @DebugString 
					+ 'ERROR - Unexpected playbook attribute encountered' + CHAR(13) + CHAR(10)
					+ '@PricingRuleSequenceNumber: ' + CAST(@PricingRuleSequenceNumber AS NVARCHAR(25)) + N', ' 
					+ '@PricingRuleAttributeSequenceNumber: ' + CAST(@PricingRuleAttributeSequenceNumber AS NVARCHAR(25)) + N', ' 
					+ '@PricingRuleAttributeType: ' + @PricingRuleAttributeType + N', '
					+ '@PricingRuleAttributeSourceSchema: ' + @PricingRuleAttributeSourceSchema + N', '
					+ '@PricingRuleAttributeSourceTable: ' + @PricingRuleAttributeSourceTable + CHAR(13) + CHAR(10)

				SET @PlaybookPricingGroupKey = -1
			
				----RAISERROR ('ERROR - Unexpected playbook attribute encountered - PricingRuleSequenceNumber: %d, PricingRuleAttributeSequenceNumber: %d, PricingRuleAttributeType: %s, PricingRuleAttributeSourceTable: %s', 
				----	0, 1, @PricingRuleSequenceNumber, @PricingRuleAttributeSequenceNumber, @PricingRuleAttributeType, @PricingRuleAttributeSourceTable) WITH NOWAIT 
			END
	
			--IF @ParamDebug = 'Y'-- or 1=1
			--BEGIN
			--	SELECT '@SQLString: ', @SQLString
			--END	
	
		IF @PricingRuleAttributeSequenceNumber = 1 
		BEGIN
			SET @GroupValuesSearchStr = ISNULL(@StrValue, N'')				-- @StrValue could be null if there was no @SQLString build (as would be the case if this could received an attribute that it did not know how to handle)
			SET @GroupingColumnsSearchStr = @PricingRuleAttributeType
		END
		ELSE
		BEGIN 
			SET @GroupValuesSearchStr = @GroupValuesSearchStr + N',' + ISNULL(@StrValue, N'')				-- @StrValue could be null if there was no @SQLString build (as would be the case if this could received an attribute that it did not know how to handle)
			SET @GroupingColumnsSearchStr = @GroupingColumnsSearchStr + N',' + @PricingRuleAttributeType
		END

		--SELECT @GroupValuesSearchStr

	END

	FETCH NEXT FROM PricingRuleSequenceCursor INTO 	
		@PricingRuleSequenceNumber,
		@SectionNumber,
		@PricingRuleAttributeSequenceNumber,
		@PricingRuleAttributeType,
		@PricingRuleAttributeSourceSchema,
		@PricingRuleAttributeSourceTable,
		@DefaultValueForMatch

	IF @PricingRuleAttributeSequenceNumber = 1 AND @GroupValuesSearchStr <> N'' -- just rolled over to a new rule; stop and search
	BEGIN
		
		IF @ParamDebug = 'Y'
		BEGIN 
			SELECT @DebugString = @DebugString + 'Searching for values: ' + @GroupValuesSearchStr + CHAR(13) + CHAR(10)
			SELECT @DebugString = @DebugString + 'Searching for columns: ' + @GroupingColumnsSearchStr + char(13) + char(10)
		END
		
		SELECT @PlaybookPricingGroupKey = PlaybookPricingGroupKey
		FROM dbo.PlaybookPricingGroup WITH (NOLOCK)
		WHERE 
			PlaybookDataPointGroupKey = @PlaybookDataPointGroupKey
			AND GroupValues = @GroupValuesSearchStr
		
		SET @GroupValuesSearchStr = N''
		SET @GroupingColumnsSearchStr = N''

	END

END

CLOSE PricingRuleSequenceCursor
DEALLOCATE PricingRuleSequenceCursor


-- search last one
IF @PlaybookPricingGroupKey IS NULL AND @GroupValuesSearchStr <> N'' -- just rolled over to a new rule; stop and search
BEGIN
	
	IF @ParamDebug = 'Y'
	BEGIN 
		SELECT @DebugString = @DebugString + 'Searching for values: ' + @GroupValuesSearchStr + CHAR(13) + CHAR(10)
		SELECT @DebugString = @DebugString + 'Searching for columns: ' + @GroupingColumnsSearchStr + char(13) + char(10)
	END
	
	SELECT @PlaybookPricingGroupKey = PlaybookPricingGroupKey
	FROM dbo.PlaybookPricingGroup WITH (NOLOCK)
	WHERE 
		PlaybookDataPointGroupKey = @PlaybookDataPointGroupKey
		AND GroupValues = @GroupValuesSearchStr
	
	--SET @GroupValuesSearchStr = N''
	--SET @GroupingColumnsSearchStr = N''

END

IF @ParamDebug = 'Y'
BEGIN 
	SELECT @DebugString = @DebugString + 
		'@PlaybookPricingGroupKey: ' + ISNULL(CAST(@PlaybookPricingGroupKey AS VARCHAR(21)), 'nil') + CHAR(13) + CHAR(10)
END

INSERT @ReturnTable(PlaybookPricingGroupKey, DebugString)
SELECT @PlaybookPricingGroupKey, @DebugString

RETURN


END







GO
