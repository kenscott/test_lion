SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- User Defined Function

CREATE FUNCTION [lion].[fn_GetLowTargetGTM]
    (
      @FloorGTM Percent_type ,
      @TargetGTM Percent_type 
    )
RETURNS TABLE
AS
RETURN
    SELECT  @FloorGTM + ( @TargetGTM - @FloorGTM ) * 0.75 AS LowTargetGTM

GO
