SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- User Defined Function

CREATE FUNCTION [lion].[fn_GetHighTargetGTM]
    (
      @TargetGTM Percent_type ,
      @StretchGTM Percent_type 
    )
RETURNS TABLE
AS
RETURN
    SELECT  @TargetGTM + ( @StretchGTM - @TargetGTM ) * 0.25 AS HighTargetGTM

GO
