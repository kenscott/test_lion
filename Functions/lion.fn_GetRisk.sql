SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE FUNCTION [lion].[fn_GetRisk]
    (
      @Goal Percent_Type ,
      @Current Percent_Type ,
      @Factor Quantity_Normal_Type
    )
RETURNS TABLE
AS
RETURN
    SELECT  CASE WHEN ( @Current - @Goal ) > 0
                 THEN ( @Current - @Goal ) * -@Factor
                 ELSE 0
            END AS [Risk]

GO
