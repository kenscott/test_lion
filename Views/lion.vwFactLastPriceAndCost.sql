SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO







CREATE
VIEW [lion].[vwFactLastPriceAndCost] 

/*
SELECT TOP 0 * FROM lion.vwFactLastPriceAndCost 
SELECT TOP 10 * FROM lion.vwFactLastPriceAndCost 
*/


AS

SELECT
	flpac.AccountKey,
	flpac.ItemKey,
	flpac.InvoiceLineGroup1Key,
	flpac.LastInvoiceLineGroup2Key,
	flpac.LastInvoiceLineGroup3Key,
	flpac.LastInvoiceLineUniqueIdentifier,
	flpac.LastInvoiceLineJunkKey,

	--InvoiceLineGroup1UDVarchar1 AS ShipmentType,
	--InvoiceLineGroup1UDVarchar2 AS ContractClaimsIndicator,  -- formally DeviatedIndicator

	ISNULL(flpac.UDVarchar1, '') AS StrategicItem,
	ISNULL(flpac.UDVarchar2, '') AS SubLLSPG,
	ISNULL(flpac.UDVarchar3, '') AS NewItemForAccountIndicator,
	ISNULL(flpac.UDVarchar4, '') AS Sales603010Bucket,
	ISNULL(flpac.UDVarchar5, '') AS TotalSalesAccountBucket,
	ISNULL(flpac.UDVarchar6, '') AS FrequencyBucket,
	ISNULL(flpac.UDVarchar7, '') AS CustBaseBucket,
	ISNULL(flpac.UDVarchar8, '') AS FreqPurchBucket,
	ISNULL(flpac.UDVarchar9, '') AS TopSellerBucket,
	flpac.UDVarchar10 AS PriceApproach,
	flpac.CustomerSize,
	flpac.CustomerBrandSegment,
	flpac.LastPriceDerivationCode,

	ISNULL(flpac.UDDecimal1, 0.) AS TotalAccountSales,
	ISNULL(flpac.UDDecimal2, 0.) AS TotalNumberOfItemsSoldByAccount,
	ISNULL(flpac.UDDecimal3, 0.) AS TotalAccountGP,
	ISNULL(flpac.UDDecimal4, 0.) AS TotalNumberOfSuperCategoriesSold,
	
	--ISNULL(flpac.UDDecimal5, 0.) AS LastUnitListPrice,
	--ISNULL(flpac.UDDecimal6, 0.) AS LastUnitRetail,

	ISNULL(flpac.PercentAccountItemSalesToAccountSales, 0.) AS PercentAccountItemSalesToAccountSales,
	ISNULL(flpac.AverageQuantityPerOrder, 0.) AS AverageQuantityPerInvoiceLine,

	LastSaleDayKey,
	--CONVERT (VARCHAR(10), dd.SQLDate, 120) AS LastInvoiceDate,
	
	flpac.LastQuantity,
	flpac.LastItemPrice,
	flpac.LastItemCost,
	flpac.LastItemDiscountPercent,

	flpac.LastItemPrice - flpac.LastItemCost AS LastItemGP,
	(flpac.LastItemPrice - flpac.LastItemCost) / NULLIF(flpac.LastItemPrice, 0.0) AS LastItemGPP,

	--LastClientInvoiceID AS LastTransactionId,
	--LastClientInvoiceLineID AS LastTransactionLineId,

	flpac.Total12MonthInvoiceLines AS Total12MonthInvoiceLineCount,
	flpac.Total12MonthQuantity AS Total12MonthQuantity,
	flpac.Total12MonthSales AS Total12MonthSales,
	flpac.Total12MonthCost AS Total12MonthCost,
	
	ISNULL(flpac.Total12MonthSales - flpac.Total12MonthCost, 0.0) AS Total12MonthGP,
	ISNULL((flpac.Total12MonthSales - flpac.Total12MonthCost) / NULLIF(flpac.Total12MonthSales, 0.0), 0.0) AS Total12MonthGPP,


	--InvoiceLineGroup2UDVarchar1 AS LastPriceDerivationBucket,
	--InvoiceLineGroup3UDVarchar1 AS LastCustomerContractIndicator,	-- If Transaction.PriceDerivation = {DC, FM, GT, IP, OCD, OCL, OTG} then "Y", else "N"

	--ISNULL(InvoiceLineJunkUDVarchar1, '') AS LastBranchCode,
	--ISNULL(InvoiceLineJunkUDVarchar2, '') AS LastTransNo,
	--ISNULL(InvoiceLineJunkUDVarchar3, '') AS LastDelNo,
	--ISNULL(InvoiceLineJunkUDVarchar4, '') AS LastLineNumber,
	--ISNULL(InvoiceLineJunkUDVarchar5, '') AS LastNetQty,
	--ISNULL(InvoiceLineJunkUDVarchar6, '') AS LastNetLinePrice,
	--ISNULL(InvoiceLineJunkUDVarchar7, '') AS LastNetApTradingMarginPadRelease,
	--ISNULL(InvoiceLineJunkUDVarchar8, '') AS LastDeliveryDate,
	--ISNULL(InvoiceLineJunkUDVarchar9, '') AS LastSledgerNo,
	--ISNULL(InvoiceLineJunkUDVarchar10, '') AS LastSledgerDate,
	--ISNULL(InvoiceLineJunkUDVarchar11, '') AS LastYMTH,
	--ISNULL(InvoiceLineJunkUDVarchar12, '') AS LastSupplyType,
	--ISNULL(InvoiceLineJunkUDVarchar13, '') AS LastDocInd,
	--ISNULL(InvoiceLineJunkUDVarchar14, '') AS LastCreditReason,
	--ISNULL(InvoiceLineJunkUDVarchar15, '') AS LastKnownValueItem,
	--ISNULL(InvoiceLineJunkUDVarchar16, '') AS LastCtsRelCode,
	--ISNULL(InvoiceLineJunkUDVarchar17, '') AS LastCustCntNo,
	--ISNULL(InvoiceLineJunkUDVarchar18, '') AS LastMthdOfDespatch,
	--ISNULL(InvoiceLineJunkUDVarchar19, '') AS LastTransType,
	--ISNULL(InvoiceLineJunkUDVarchar20, '') AS LastSalesOrigin,
	--ISNULL(InvoiceLineJunkUDVarchar21, '') AS LastTakenBy,
	--ISNULL(InvoiceLineJunkUDVarchar22, '') AS LastMethodOfPayment,
	--ISNULL(InvoiceLineJunkUDVarchar23, '') AS LastHeaderID,
	--ISNULL(InvoiceLineJunkUDVarchar24, '') AS LastCrdOrigInvNo,
	--ISNULL(InvoiceLineJunkUDVarchar25, '') AS LastCrdOrigInvDate,
	--ISNULL(InvoiceLineJunkUDVarchar26, '') AS LastOriginalOrdNo,
	--ISNULL(InvoiceLineJunkUDVarchar27, '') AS LastCarriagePack,
	--ISNULL(InvoiceLineJunkUDVarchar28, '') AS LastDelAddPhone,
	--ISNULL(InvoiceLineJunkUDVarchar29, '') AS LastGpAllocBranchCode,
	--ISNULL(InvoiceLineJunkUDVarchar30, '') AS LastSalesOriginDesc,
	--ISNULL(InvoiceLineJunkUDVarchar31, '') AS LastPriceOrderType,
	--ISNULL(InvoiceLineJunkUDVarchar32, '') AS LastTransactionDate,
	--ISNULL(InvoiceLineJunkUDVarchar33, '') AS LastOrderDate,	

	--ISNULL(fil.UnitListPrice, 0.) AS LastTradingPrice,
	
	RPBPlaybookPricingGroupKey,
	flpac.UDVarchar1 AS BaseStrategicItem,
	AlternateLastInvoiceLineUniqueIdentifier
	
FROM
	dbo.FactLastPriceAndCost flpac (NOLOCK)
--INNER JOIN dbo.DimInvoiceLineJunk diljunk (NOLOCK)
--	ON diljunk.InvoiceLineJunkKey = flpac.LastInvoiceLineJunkKey
--INNER JOIN dbo.DimDay dd (NOLOCK)
--	ON dd.daykey = flpac.LastSaleDayKey
--INNER JOIN dbo.DimInvoiceLineGroup1 (NOLOCK)
--	ON DimInvoiceLineGroup1.InvoiceLineGroup1Key = flpac.InvoiceLineGroup1Key
--INNER JOIN dbo.DimInvoiceLineGroup2 (NOLOCK)
--	ON DimInvoiceLineGroup2.InvoiceLineGroup2Key = flpac.LastInvoiceLineGroup2Key
--INNER JOIN dbo.DimInvoiceLineGroup3 (NOLOCK)
--	ON DimInvoiceLineGroup3.InvoiceLineGroup3Key = flpac.LastInvoiceLineGroup3Key
--LEFT JOIN dbo.FactInvoiceLine fil
--	ON flpac.LastInvoiceLineUniqueIdentifier = fil.InvoiceLineUniqueIdentifier
--LEFT JOIN dbo.DimAccountGroup1 dag1Sale
--	ON dag1Sale.AccountGroup1Key = fil.AccountGroup1Key

		




























































































GO
