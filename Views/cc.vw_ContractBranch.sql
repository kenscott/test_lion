SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE VIEW [cc].[vw_ContractBranch] AS 
	SELECT 
		NEWID() AS GUID, 
		scl.ContractKey, 
		AG1Level1 AS BranchCode,
		AG1Level2 AS Network,
		AG1Level3 AS Area,
		AG1Level4 AS Region,
		AG1Level1UDVarchar6 AS Brand,
		AG1Level1UDVarchar1 AS BranchName,
		AG1Level2UDVarchar1 AS NetworkDescription,
		AG1Level3UDVarchar1 AS AreaDescription,
		AG1Level4UDVarchar1 AS RegionDescription,
		AG1Level1UDVarchar6 AS BrandDescription

	FROM cc.SupplierContractLocation scl
	JOIN dbo.DimAccountGroup1 dag1 ON dag1.AccountGroup1Key = scl.LocationKey
	WHERE ContractLocationLevelKey = 1

GO
