SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE 
VIEW [lion].[vwAccountLookup] 

/*
SELECT TOP 100 * FROM lion.vwAccountLookup 
*/


AS


SELECT distinct  
	da.[AccountKey],

	dag1.AG1Level1 AS Branch,
	
	da.AccountManagerKey as AccountManagerKey,
	dam.AccountManagerCode,
	dp.FullName AS AccountManagerFullName,
	dp.Email AS AccountManagerEmail,
	
	AccountClientUniqueIdentifier,
	AccountNumber,
	AccountName,
	--AccountNumberParent,
	--AccountAddress1,
	--AccountAddress2,
	--AccountAddress3,
	--AccountAddress4,
	--AccountZipCode AS AccountPostCode,
	
	--AccountMemo AS CustomerType,
	--AccountUDVarChar1 AS CreditLimit,
	--AccountUDVarChar2 AS PaymentTermName,
	--AccountUDVarChar3 AS IntercompanyAccount,
	--AccountUDVarChar4 AS OverLimitStatus,
	--AccountUDVarChar5 AS OverdueStatus,
	--AccountUDVarChar6 AS TermsParent,
	--AccountUDVarChar7 AS PricingCategory,
	--AccountUDVarChar8 AS AccountOwnerType,
	--AccountUDVarChar9 AS AccountOwnerBucket,
	--AccountUDVarChar10 AS CustomerSegment,
	--AccountUDVarChar11 AS CustomerRebateIndicator,
	--AccountUDVarChar12 AS FixedPriceIndicator,
	--AccountUDVarChar13 AS ContractName,
	
	--AccountUDDateTime1 AS DateOpened,
	--AccountUDDateTime2 AS DateClosed,
	da.Inactive,

	AG1Level1 AS BranchCode,
	AG1Level2 AS Network,
	AG1Level3 AS Area,
	AG1Level4 AS Region,
	--AG1Level5 AS CompanyName,

	AG1Level1UDVarchar1 AS BranchName,
	AG1Level2UDVarchar1 AS NetworkDescription,
	AG1Level3UDVarchar1 AS AreaDescription,
	AG1Level4UDVarchar1 AS RegionDescription,
	--AG1Level5UDVarchar1 AS CompanyNameDescription,

	--AG1Level1UDVarchar2 AS PlaybookRegion,
	--AG1Level1UDVarchar3 AS PlaybookRegionDescription,
	--AG1Level1UDVarchar4 AS PlaybookArea,
	--AG1Level1UDVarchar5 AS PlaybookAreaDescription,

	--AG1Level1UDVarchar6 AS BranchPrimaryBrand,
	--AG1Level1UDVarchar7 AS BranchTradingStatus,
	--AG1Level1UDVarchar8 AS BranchType,
	--AG1Level1UDVarchar9 AS BranchAddressLine1,
	--AG1Level1UDVarchar10 AS BranchAddressLine2,
	--AG1Level1UDVarchar11 AS BranchAddressLine3,
	--AG1Level1UDVarchar12 AS BranchAddressLine4,
	--AG1Level1UDVarchar13 AS BranchAddressLine5,
	--AG1Level1UDVarchar14 AS BranchPostcode,
	--AG1Level1UDVarchar15 AS BranchEmail,
	--AG1Level1UDVarchar16 AS BranchTelephone,

	--AG1Level1UDVarchar17 AS CustomerBranchPostCodeArea,
	--AG1Level1UDVarchar18 AS CustomerBranchPostCodeDistrict,

	--AG1Level1UDVarchar19 AS BranchPyramidCode,
	--AG1Level1UDVarchar21 AS BrandGroup,
	--AG1Level1UDVarchar22 AS PGActiveIndicator,

	--AG1Level1UDVarchar23 AS Manager,
	--AG1Level1UDVarchar24 AS BranchManagerEmail,

	--AG1Level1UDVarchar25 AS RegionExceptionIndicator,
	--AG1Level1UDVarchar26 AS BusinessUnitRegion,

	--G2Level1 AS CustomerType,
	--AG2Level1UDVarchar1 AS CustomerTypeDescription,

	--AG3Level1 AS TrandingStatusCategory,
	--AG3Level1UDVarchar1 AS TrandingStatusCategoryDescription,

	--db.Brand AS AccountOwnersBrand,
	
	dabm.AccountManagerKey as AccountBrandManagerKey,
	dam2.AccountManagerCode as AccountBrandManagerCode,
	dp2.FullName as AccountBrandManagerFullName,
	
	dag1.BrandKey as BranchBrandKey,
	db.Brand as BranchBrand
	
FROM dbo.DimAccount da (NOLOCK)

INNER JOIN dbo.DimAccountGroup1 dag1 (NOLOCK)
	ON dag1.AccountGroup1Key = da.AccountGroup1Key
INNER JOIN dbo.DimAccountGroup2 dag2 (NOLOCK)
	ON dag2.AccountGroup2Key = da.AccountGroup2Key
INNER JOIN dbo.DimAccountGroup3 dag3 (NOLOCK)
	ON dag3.AccountGroup3Key = da.AccountGroup3Key
INNER JOIN dbo.DimAccountManager dam (NOLOCK)
	ON dam.AccountManagerKey = da.AccountManagerKey
INNER JOIN dbo.DimPerson dp (NOLOCK)
	ON dp.DimPersonKey = dam.DimPersonKey

INNER JOIN dbo.DimBrand db
	ON db.BrandKey = dag1.BrandKey

LEFT JOIN dbo.DimAccountBrandManager dabm (NOLOCK)
	ON dabm.AccountKey = da.AccountKey	
LEFT JOIN dbo.DimAccountManager dam2 (NOLOCK)
	ON dam2.AccountManagerKey = dabm.AccountManagerKey	
LEFT JOIN dbo.DimPerson dp2 (NOLOCK)
	ON dp2.DimPersonKey = dam2.DimPersonKey
	

GO
