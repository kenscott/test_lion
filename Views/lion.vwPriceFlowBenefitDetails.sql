SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE VIEW [lion].[vwPriceFlowBenefitDetails]
AS

SELECT pfb.RequestKey, a.AccountManagerFullName AS [Account Owner], IIF(a.AccountOwnerType='', 'Branch', a.AccountOwnerType) AS [Account Owner Type], a.AccountNumber, a.AccountName, i.LLSPGCode, i.LLSPGDescription, SUBSTRING(i.ItemNumber, 2, 255) AS ItemNumber, i.ItemDescription, 
       ag1.Region, ag1.RegionDescription, ag1.Area, ag1.AreaDescription, ag1.Network, ag1.NetworkDescription,
	   ag1.Branch, ag1.BranchName, dd.SQLDate AS DeliveryDate,
	   dilj.InvoiceLineJunkUDVarchar2 + '/' + REPLACE(dilj.InvoiceLineJunkUDVarchar3,'.00000000','') AS ChargeNote,
	   f.PriceDerivation,
	   ISNULL(f.TotalNetQuantity, f.TotalQuantity) AS NetQuantity, ISNULL(f.TotalNetPrice, f.TotalActualPrice) AS NetSales, ISNULL(f.TotalNetCost, f.TotalActualCost) AS NetCost, ISNULL(f.TotalNetPrice, f.TotalActualPrice) - ISNULL(f.TotalNetCost, f.TotalActualCost) AS GP,
	   (ISNULL(f.TotalNetPrice, f.TotalActualPrice) - ISNULL(f.TotalNetCost, f.TotalActualCost)) / NULLIF(ISNULL(f.TotalNetPrice, f.TotalActualPrice), 0) AS GPP, pfb.[Actual Benefit],
	   pfb.RuleCode, pfb.RuleGroup, pfb.[Potential Benefit], pft.OriginalDiscount, pft.NewDiscount,
	   ROUND((ISNULL(f.TotalNetQuantity, f.TotalQuantity) * f.TradingPrice - ISNULL(f.TotalNetPrice, f.TotalActualPrice)) / NULLIF((ISNULL(f.TotalNetQuantity, f.TotalQuantity) * f.TradingPrice), 0), 4) AS ActualDiscount,
	   ilg1.DeviatedIndicator as [Contract Claims Indicator], f.CCProfit as [Claim Amount], 
	   pft.NewException, f.ClientInvoiceLineUniqueID
  FROM lion.PriceFlowBenefits pfb INNER JOIN
       lion.vwFactInvoiceLine f ON f.ClientInvoiceLineUniqueID = pfb.ClientInvoiceLineUniqueID INNER JOIN
	   lion.vwAccount a ON f.AccountKey = a.AccountKey INNER JOIN
	   lion.vwItem i ON f.ItemKey = i.ItemKey INNER JOIN
	   lion.vwFactInvoiceLineGroup1 ilg1 on f.InvoiceLineGroup1Key = ilg1.InvoiceLineGroup1Key INNER JOIN
	   lion.vwAccountGroup1 ag1 ON f.AccountGroup1Key = ag1.AccountGroup1Key INNER JOIN
	   dbo.DimDay dd ON f.InvoiceDateDayKey = dd.DayKey INNER JOIN
	   dbo.DimInvoiceLineJunk dilj ON f.InvoiceLineJunkKey = dilj.InvoiceLineJunkKey INNER JOIN
       lion.PriceFlowTracker pft ON pft.RequestKey = pfb.RequestKey  AND pft.CurrentIndicator = 1




GO
