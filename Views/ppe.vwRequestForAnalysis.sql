SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE VIEW [ppe].[vwRequestForAnalysis]
AS
 SELECT RequestKey ,
        r.CreatedByWebUserKey AS CreateByID ,
        wu1.UserName AS CreatedByName ,
        r.CreationDate ,
        av.AccountManagerCode AS AccountOwnerID ,
        av.AccountManagerFullName AS AccountOwnerFullName ,
		NULL AS AccountBrandOwnerID,
		NULL AS AccountBrandOwnerFullName,
        [OwningBrandCode] ,
        r.AccountKey ,
        da.AccountNumber ,
        da.AccountName ,
        BranchCode ,
        BranchName ,
        Network AS NetworkCode ,
        NetworkDescription AS NetworkName ,
        Area AS AreaCode ,
        AreaDescription AS AreaName ,
        l3.LLSPGCode AS LLSPG ,
        l3.LLSPGDescription ,
        [DefaultDiscount] ,
        [CurrentSPGDiscount1] ,
        [CurrentSPGDiscount2] ,
        [CurrentCompoundDiscount] ,
        r.ItemKey ,
        CASE WHEN r.ItemKey <> 1 THEN SUBSTRING(di.ItemNumber, 2, 255)
             ELSE NULL
        END AS ExceptionProductNumber ,
        di.ItemDescription AS ProductDescription ,
        [CurrentExceptionDiscount] ,
        [CurrentExceptionFixedPrice] ,
        [CurrentExceptionStartDate] ,
        [CurrentExceptionExpiryDate] ,
        [LastChangeInitials] ,
        [LastChangeDateTime] ,
        [LastChangeBranch] ,
        [StrategicItemFlag] ,
        [FixedPriceFlag] ,
        [KVIFlag] ,
        [KVIOverrideFlag] ,
        r.ProposedDeleteTerm ,
        [ProposedSPGDiscount1] ,
        [ProposedSPGDiscount2] ,
        [ProposedCompoundDiscount] ,
        [ProposedExceptionDiscount] ,
        [ProposedExceptionFixedPrice] ,
        [ProposedExceptionStartDate] ,
        [ProposedExceptionExpiryDate] ,
        rp.Price AS RecommendedPrice ,
        [RecommendedPriceLogic] ,
        [RecommendedImpact] ,
        r.NewDeleteTerm ,
        r.NewTermType ,
        r.NewSPGDiscount1 ,
        r.NewSPGDiscount2 ,
        ncd.Discount AS NewCompoundDiscount ,
        r.NewSPGFromDate AS NewSPGFromDate ,
        [NewExceptionDiscount] ,
        [NewExceptionFixedPrice] ,
        [NewExceptionStartDate] ,
        [NewExceptionExpiryDate] ,
        r.AcceptedImpact ,
        [LastSaleDate] ,
        [LastPriceDerivation] ,
        [LastPriceLevel] ,
        r.[LastItemPrice] ,
        [AdjustedLastItemPrice] ,
        r.[LastItemGPP] ,
        [AdjustedLastItemGPP] ,
        r.LastDiscount ,
        [LastItemDiscount] ,
        r.CurrentCost ,
        cp.Price AS CurrentPrice ,
        r.[Total12MonthSales] ,
        l.[Total12MonthQuantity] ,
        [PriceBandLevel] ,
        [SalesSize] ,
        r.[PriceApproach] ,
        [GreenPrice] ,
        [AmberPrice] ,
        [RedPrice] ,
        [TradePrice] ,
        [GreenGPP] ,
        [AmberGPP] ,
        [RedGPP] ,
        [TradeGPP] ,
        r.ModifiedByWebUserKey AS ModifyByID ,
        wu2.UserName AS ModifiedByName ,
        r.ModificationDate AS ModifiedDate ,
        r.ModificationDate ,
        r.[StatusKey] AS StatusCode ,
        rs.StatusName AS StatusDescription ,
        r.LastActionKey ,
        ActionAbbr AS LastAction ,
        OwnerWebUserKey AS RequestOwnerKey ,
        od5.FullName AS RequestOwnerFullName ,
        r.NotificationEmailSentIndicator ,
        r.RealityResponseCode
 FROM   [ppe].[RequestMain] r
        JOIN dbo.DimAccount da ON da.AccountKey = r.AccountKey
        JOIN dbo.DimAccountManager dam ON dam.AccountManagerKey = da.AccountManagerKey
        JOIN dbo.DimItem di ON di.ItemKey = r.ItemKey
        JOIN dbo.WebUser wu1 ON wu1.WebUserKey = r.[CreatedByWebUserKey]
        JOIN dbo.WebUser wu2 ON wu2.WebUserKey = r.ModifiedByWebUserKey
        JOIN ppe.RequestStatus rs ON rs.StatusKey = r.StatusKey
        JOIN ppe.RequestType rqt ON rqt.TypeKey = r.TypeKey
        JOIN dbo.DimInvoiceLineGroup1 ilg1 ON ilg1.InvoiceLineGroup1Key = r.InvoiceLineGroup1Key
        JOIN dbo.DimItemGroup3 ig3 ON ig3.ItemGroup3Key = r.ItemGroup3Key
        JOIN [ppe].[RequestAction] ra ON r.LastActionKey = ra.ActionKey
        JOIN dbo.WebUser wu3 ON wu3.AccountManagerKey = dam.AccountManagerKey
        JOIN dbo.ODSPerson od5 ON od5.ODSPersonKey = wu3.ODSPersonKey
        CROSS APPLY ppe.fn_GetPrice(r.CurrentExceptionFixedPrice, r.TradePrice,
                                    r.CurrentExceptionDiscount,
                                    r.CurrentCompoundDiscount) cp
        CROSS APPLY ppe.fn_GetPrice(r.ProposedExceptionFixedPrice,
                                    r.TradePrice, r.ProposedExceptionDiscount,
                                    r.ProposedCompoundDiscount) rp
        CROSS APPLY ppe.fn_GetPrice(r.NewExceptionFixedPrice, r.TradePrice,
                                    r.NewExceptionDiscount,
                                    r.NewCompoundDiscount) np
        CROSS APPLY ppe.fn_GetPriceIndex(np.Price, r.RedPrice, r.AmberPrice,
                                         r.GreenPrice) npi
        CROSS 
                         APPLY ppe.fn_GetDiscount(r.AccountKey,
                                                  r.ItemGroup3Key, r.ItemKey,
                                                  r.GreenPrice, r.TradePrice,
                                                  'G') gd
        CROSS APPLY ppe.fn_GetDiscount(r.AccountKey, r.ItemGroup3Key,
                                       r.ItemKey, r.AmberPrice, r.TradePrice,
                                       'A') ad
        CROSS APPLY ppe.fn_GetDiscount(r.AccountKey, r.ItemGroup3Key,
                                       r.ItemKey, r.RedPrice, r.TradePrice,
                                       'R') rd
        CROSS APPLY ppe.fn_GetTerm(r.CurrentExceptionFixedPrice,
                                   r.CurrentExceptionDiscount,
                                   r.CurrentCompoundDiscount,
                                   CASE WHEN r.CurrentExceptionFixedPrice IS NULL
                                        THEN 'TD'
                                        ELSE 'FP'
                                   END) ct
        CROSS APPLY ppe.fn_GetTerm(r.ProposedExceptionFixedPrice,
                                   r.ProposedExceptionDiscount,
                                   ISNULL(r.ProposedCompoundDiscount, 0),
                                   r.NewTermType) rt
        CROSS APPLY ppe.fn_GetNewTerm(r.NewTermType, r.NewExceptionFixedPrice,
                                      r.NewExceptionDiscount,
                                      ISNULL(r.NewCompoundDiscount, 0)) nt
        CROSS APPLY ppe.fn_GetTermType(r.CurrentExceptionFixedPrice) ctt
        CROSS APPLY ppe.fn_GetTermType(r.ProposedExceptionFixedPrice) rtt
        CROSS APPLY ppe.fn_GetTermType(r.NewExceptionFixedPrice) ntt
        CROSS 
                         APPLY ppe.fn_GetSimpleDiscount(r.CurrentExceptionFixedPrice,
                                                        r.TradePrice,
                                                        r.CurrentExceptionDiscount,
                                                        r.CurrentCompoundDiscount) csd
        CROSS 
                         APPLY ppe.fn_GetSimpleDiscount(r.ProposedExceptionFixedPrice,
                                                        r.TradePrice,
                                                        r.ProposedExceptionDiscount,
                                                        r.ProposedCompoundDiscount) rsd
        CROSS 
                         APPLY ppe.fn_GetSimpleDiscount(r.NewExceptionFixedPrice,
                                                        r.TradePrice,
                                                        r.NewExceptionDiscount,
                                                        r.NewSPGDiscount1) nsd
        CROSS 
                         APPLY ppe.fn_GetSimpleDiscount(r.NewExceptionFixedPrice,
                                                        r.TradePrice,
                                                        r.NewExceptionDiscount,
                                                        r.NewCompoundDiscount) ncd
        CROSS 
                         APPLY ppe.fn_GetPG(csd.Discount, ad.Discount,
                                            gd.Discount) cpg
        CROSS APPLY ppe.fn_GetPG(ncd.Discount, ad.Discount, gd.Discount) npg
        LEFT JOIN lion.vwFactLastPriceAndCost l ON l.AccountKey = r.AccountKey
                                                   AND l.ItemKey = r.ItemKey
                                                   AND l.InvoiceLineGroup1Key = r.InvoiceLineGroup1Key
        LEFT JOIN lion.vwItemGroup3 l3 /*ON l3.ItemGroup3Key = di.ItemGroup3Key*/ ON l3.ItemGroup3Key = r.ItemGroup3Key
        LEFT JOIN lion.vwAccount av ON av.AccountKey = da.AccountKey
		WHERE r.ModificationDate > DATEADD(DAY,-60,GETDATE())
		;


GO
