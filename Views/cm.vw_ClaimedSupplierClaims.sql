SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO







-- select * from cm.vw_ClaimedSupplierClaims





CREATE VIEW [cm].[vw_ClaimedSupplierClaims]

AS

SELECT 
	NEWID() AS GUID,
	v.VendorKey, 
	v.VendorNumber, 
	v.VendorDescription, 
	cj.ClaimMonth, 
	v.VendorDescription + ' (' + v.VendorNumber + ')' AS VendorLabel, 
	(SELECT COUNT(DISTINCT ic.InvoiceClaimKey)) AS InvoiceCount,  
	(SELECT COUNT(*) FROM cm.ClaimJournal cj2 WHERE cj2.VendorKey = cj.VendorKey AND cj2.ClaimMonth = cj.ClaimMonth AND cj2.SupplierClaimKey = c.SupplierClaimKey AND cj2.ClaimJournalStatus = 'C') AS ClaimLines, 
	(SELECT SUM(cj.ClaimLineValue)) AS InvoiceTotal,
	c.ClaimStatus,
	c.LastSentDate,
	c.SupplierClaimKey  

FROM cm.ClaimJournal cj WITH (NOLOCK) 
INNER JOIN dbo.DimVendor v   
	ON v.VendorKey = cj.VendorKey 
INNER JOIN cm.SupplierClaim c
	ON c.SupplierClaimKey = cj.SupplierClaimKey
INNER JOIN cm.InvoiceClaim ic
	on ic.InvoiceClaimKey = cj.InvoiceClaimKey
	--ON ic.VendorKey = cj.VendorKey
	--AND ic.ClaimMonth = cj.ClaimMonth
	--AND ic.ContractId = cj.CurrentContractID

WHERE  c.ClaimStatus IN ('C', 'D')  --Processing or Delivered



GROUP BY v.VendorKey, cj.VendorKey, v.VendorNumber, v.VendorDescription, cj.ClaimMonth, c.ClaimStatus, c.LastSentDate, c.SupplierClaimKey 












GO
