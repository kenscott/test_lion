SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [ppe].[vw_RequestLogNote]
AS 
SELECT 
	NEWID() AS GUID,
	'Log' AS LogOrNote,
	rl.WebUserKey, 
	RequestKey, 
	LogTime, 
	wu.UserName,
	rl.RequestAction,
	rl.Notes

FROM ppe.RequestLog rl
JOIN dbo.WebUser wu ON wu.WebUserKey = rl.WebUserKey
LEFT JOIN ppe.RequestStatus rsOld ON rl.OldStatusKey = rsOld.StatusKey
LEFT JOIN ppe.RequestStatus rsNew ON rl.NewStatusKey = rsNew.StatusKey

UNION

SELECT 	NEWID() AS GUID,
	'Note' AS LogOrNote, rn.WebUserKey, RequestKey, CreatedDate, wu.UserName, '' AS RequestAction, Notes 
	
	FROM ppe.RequestNote rn
	JOIN dbo.WebUser wu ON wu.WebUserKey = rn.WebUserKey



GO
