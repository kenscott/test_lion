SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [cm].[vw_ApproachingLimitAll]

AS


WITH Accounts AS (
SELECT ISNULL(sci.ContractItemKey, scig.ContractItemGroupXKey) AS ProdGroupId,
	da.AccountNumber,
	da.AccountName,
	COUNT(*) AS AccountCnt
FROM cc.SupplierContract sc
LEFT JOIN cc.SupplierContractItem sci
	ON sci.ContractKey = sc.ContractKey
LEFT JOIN cc.SupplierContractItemGroupX scig
	ON scig.ContractKey = sc.ContractKey
LEFT JOIN cc.SupplierContractAccount sca
	ON sca.ContractKey = sc.ContractKey
LEFT JOIN dbo.DimAccount da
	ON da.AccountKey = sca.AccountKey
GROUP BY ISNULL(sci.ContractItemKey, scig.ContractItemGroupXKey),
	da.AccountNumber,
	da.AccountName
HAVING COUNT(*) > 1
	)

SELECT  
		NEWID() AS GUID,
		sc.ContactName,
		sc.ContactEmail,
		sc.ContractID AS ContractID,
		dv.VendorDescription AS SupplierName,
		di.ItemUDVarChar20 AS ItemNumberOrGroup,
		CASE
			WHEN a.AccountNumber IS NULL THEN 'VARIOUS'
			ELSE a.AccountNumber + ' ' + a.AccountName 
		END AS Account,
		CASE
			WHEN sci.LimitType = 'U' THEN 'Unit'
			WHEN sci.LimitType = 'S' THEN 'Sales'
			WHEN sci.LimitType = 'C' THEN 'Claim'
			ELSE NULL
		END AS LimitType,
		sci.LimitValue AS LimitAmount,
		sci.CurrentMonthTally AS CurrentLimitTally,
		sci.LimitCurrentRatio AS Ratio
	
FROM cc.SupplierContractItem sci
INNER JOIN cc.SupplierContract sc 
	ON sc.ContractKey = sci.ContractKey
INNER JOIN dbo.DimItem di
	ON di.ItemKey = sci.ItemKey
INNER JOIN dbo.DimVendor dv
	ON dv.VendorKey = sc.VendorKey
LEFT JOIN Accounts a
	ON a.ProdGroupId = sci.ContractItemKey
WHERE LimitCurrentRatio >= 90.0


UNION

SELECT  
		NEWID() AS GUID,
		sc.ContactName,
		sc.ContactEmail,
		sc.ContractID AS ContractID,
		dv.VendorDescription AS SupplierName,
		ISNULL(dig1.IG1Level1, dig3.IG3Level1) AS ItemNumberOrGroup,
		CASE
			WHEN a.AccountNumber IS NULL THEN 'VARIOUS'
			ELSE a.AccountNumber + ' ' + a.AccountName 
		END AS Account,
		CASE
			WHEN scig.LimitType = 'U' THEN 'Unit'
			WHEN scig.LimitType = 'S' THEN 'Sales'
			WHEN scig.LimitType = 'C' THEN 'Claim'
			ELSE NULL
		END AS LimitType,
		scig.LimitValue AS LimitAmount,
		scig.CurrentMonthTally AS CurrentLimitTally,
		scig.LimitCurrentRatio AS Ratio
	
FROM cc.SupplierContractItemGroupX scig
INNER JOIN cc.SupplierContract sc 
	ON sc.ContractKey = scig.ContractKey
INNER JOIN dbo.DimVendor dv
	ON dv.VendorKey = sc.VendorKey
LEFT JOIN dbo.DimItemGroup1 dig1
	ON dig1.ItemGroup1Key = scig.ItemGroupXKey
	AND scig.ItemGroupLevel = 1
LEFT JOIN dbo.DimItemGroup3 dig3
	ON dig3.ItemGroup3Key = scig.ItemGroupXKey
	AND scig.ItemGroupLevel = 3
LEFT JOIN Accounts a
	ON a.ProdGroupId = scig.ContractItemGroupXKey
WHERE scig.LimitCurrentRatio >= 90.0

GO
