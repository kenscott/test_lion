SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO










/*

select top 10 * from cm.vw_ClaimLines where ClientInvoiceLineUniqueID = 626829369

select top 100 * from dimitem where itemnumber like '%200615%'

select top 100 * from DimVendor

*/


CREATE VIEW [cm].[vw_ClaimLines] 

AS


--Get multiple claim lines (claim lines with the same transaction id)
WITH MultiplesLines AS (
	SELECT DISTINCT ClientInvoiceLineUniqueID 
	FROM cm.ClaimJournal
	WHERE ClaimType = 'C'   --do not group Adjustment or Manual claims
		AND ClaimJournalStatus <> 'D'  --do not include claims with a Deleted status 
	GROUP BY ClientInvoiceLineUniqueID
	HAVING COUNT(*) > 1 
), 
Multiples AS (
	SELECT 
		MIN(cj.ClaimJournalKey) AS MinClaimJournalKey, 
		cj.ClientInvoiceLineUniqueID
	FROM cm.ClaimJournal cj
	INNER JOIN MultiplesLines  ml
		ON ml.ClientInvoiceLineUniqueID = cj.ClientInvoiceLineUniqueID
	WHERE ClaimType = 'C'
	GROUP BY 
		cj.ClientInvoiceLineUniqueID

)

SELECT DISTINCT 
	CASE 
		WHEN m.MinClaimJournalKey IS NOT NULL THEN cj2.ClaimJournalKey
		ELSE cj.ClaimJournalKey
	END AS ClaimJournalKey,
	cj.ClientInvoiceLineUniqueID,
	CASE
		WHEN m.MinClaimJournalKey IS NOT NULL THEN cj2.ClaimMonth
		ELSE cj.ClaimMonth
	END AS ClaimMonth,
	v.VendorNumber AS VendorNumber,
	v.VendorKey AS VendorKey,
	v.VendorDescription AS VendorDescription,
	fil.ChargeNoteID,
	fil.SLedgerNo AS InvoiceNumber,
	fil.SLedgerDate AS InvoiceDate,
	fil.TransactionDate,
	fil.BranchCode,
	fil.BranchDescription,
	fil.AccountNumber,
	fil.AccountName,
	fil.ProductCode AS ItemNumber,
	fil.ProductDescription AS ItemDescription,
	fil.TransType,
	fil.SupplyType AS SupplyType,
	CASE
		WHEN cj.ClaimType = 'M' THEN cj.ProductQty
		ELSE fil.Qty 
	END AS Quantity,
	fil.NettPrice AS Price,
	fil.NettPrice / NULLIF(fil.Qty, 0) AS UnitPrice,
	fil.SupplierNettCost AS Cost,
	fil.SupplierNettCost / NULLIF(fil.Qty, 0) AS UnitCost,
	fil.TradingMargin AS TransTradingMargin,
	fil.FinancialMargin AS TransFinancialMargin,
	CASE 
		WHEN m.MinClaimJournalKey IS NOT NULL THEN NULL
		ELSE cj.OriginalContractID 
	END AS POSContractID,
	CASE 
		WHEN m.MinClaimJournalKey IS NOT NULL THEN 'Multiple'
		ELSE cj.CurrentContractID 
	END AS ARPContractID,
	CASE 
		WHEN m.MinClaimJournalKey IS NOT NULL THEN NULL
		ELSE sc.PricingContractID 
	END AS ARPPricingContractID,
	CASE 
		WHEN m.MinClaimJournalKey IS NOT NULL THEN NULL
		ELSE sc2.PricingContractID 
	END AS POSPricingContractID,
	CASE 
		WHEN m.MinClaimJournalKey IS NOT NULL THEN NULL
		ELSE sc.ContractOwner
	END AS ContractOwner,
	CASE 
		WHEN m.MinClaimJournalKey IS NOT NULL THEN 'Multiple'
		ELSE cj.SupplierRef
	END AS SupplierReferenceNo,
	CASE 
		WHEN m.MinClaimJournalKey IS NOT NULL THEN NULL
		ELSE sc.ContractType1 
	END AS ContractType,
	CASE 
		WHEN m.MinClaimJournalKey IS NOT NULL THEN NULL
		ELSE sc.JobSite
	END AS JobSite,
	CASE 
		WHEN m.MinClaimJournalKey IS NOT NULL THEN NULL
		ELSE cj.ClaimType
	END AS ClaimType,
	cj.ARPReason,
	cj.ARPResult AS ARPResult,
	cj.ClaimJournalStatus AS [Status],
	cjs.ClaimJournalStatusDescription AS StatusDescription,
	CASE 
		WHEN m.MinClaimJournalKey IS NOT NULL THEN NULL
		ELSE wu.UserName 
	END AS ADVRecommendedUser,
	CASE 
		WHEN m.MinClaimJournalKey IS NOT NULL THEN NULL
		ELSE rr.RejectionReasonValue 
	END AS ADVRejectReason,
	CASE 
		WHEN m.MinClaimJournalKey IS NOT NULL THEN NULL
		ELSE LEFT(cj.Comment, 10) 
	END AS ADVShortComment,
	CASE 
		WHEN m.MinClaimJournalKey IS NOT NULL THEN NULL
		ELSE cj.Comment 
	END AS ADVFullComment,
	CASE 
		WHEN m.MinClaimJournalKey IS NOT NULL THEN NULL
		ELSE cj.InvoiceClaimKey 
	END AS ClaimID,
	fil.TradingMargin / NULLIF(fil.NettPrice, 0) AS BeforeMarginPerc,
	fil.TradingMargin AS BeforeMarginValue,
	CASE 
		WHEN m.MinClaimJournalKey IS NOT NULL THEN cj2.FinancialMargin
		ELSE cj.FinancialMargin
	END AS FinancialMargin,
	CASE 
		WHEN m.MinClaimJournalKey IS NOT NULL THEN NULL
		ELSE cj.TradingMargin / NULLIF(fil.NettPrice, 0)
	END AS AfterMarginPerc,
	CASE 
		WHEN m.MinClaimJournalKey IS NOT NULL THEN NULL
		ELSE cj.TradingMargin
	END AS AfterMarginValue,
	CASE 
		WHEN m.MinClaimJournalKey IS NOT NULL THEN 'Multiple'
		ELSE CAST(cj.ClaimLineValue AS VARCHAR(50))
	END AS ClaimValue,
	CASE 
		WHEN m.MinClaimJournalKey IS NOT NULL THEN 1000000
		ELSE cj.ClaimLineValue 
	END AS ClaimValueNumeric,
	CASE 
		WHEN m.MinClaimJournalKey IS NOT NULL THEN NULL
		ELSE cj.ConfidentialTermsGPAdjustAmt 
	END AS CTGPAdjustment,
	CASE 
		WHEN m.MinClaimJournalKey IS NOT NULL THEN NULL
		ELSE cj.PriceDeferralGPAdjustAmt 
	END AS PDGPAdjustment,
	CASE 
		WHEN m.MinClaimJournalKey IS NOT NULL THEN NULL
		ELSE cj.SpecialDealGPAdjustAmt 
	END AS SDGPAdjustment,
	CASE 
		WHEN m.MinClaimJournalKey IS NOT NULL THEN NULL
		ELSE cj.ClearanceDealGPAdjustAmt 
	END AS CDGPAdjustment,
	CASE
		WHEN cj.ClaimType = 'M' THEN 'Y'
		ELSE 'N'
	END AS ManualClaimFlag,
	CASE 
		WHEN fil.CTSReleaseCode IS NOT NULL THEN 'Y'
		ELSE 'N'
	END AS CTSFlag,
	CASE 
		WHEN m.MinClaimJournalKey IS NOT NULL THEN NULL
		ELSE sc.JobFlag
	END AS JobFlag,
	ar.AdjustmentReasonCode + ' - ' + ar.AdjustmentReasonValue AS AdjustmentReason

FROM cm.ClaimJournal cj
LEFT JOIN Multiples m
	ON m.ClientInvoiceLineUniqueID = cj.ClientInvoiceLineUniqueID
	AND cj.ClaimType = 'C'
LEFT JOIN cm.ClaimJournal cj2
	ON cj2.ClaimJournalKey = m.MinClaimJournalKey
LEFT JOIN DimVendor v ON
	v.VendorKey = cj.VendorKey
LEFT JOIN cm.ClaimJournalStatus cjs
	ON cjs.ClaimJournalStatus = cj.ClaimJournalStatus
LEFT JOIN cm.FactInvoiceLine fil ON
	fil.ClientInvoiceLineUniqueID = cj.ClientInvoiceLineUniqueID
LEFT JOIN cc.SupplierContract sc ON
	sc.ContractID = cj.CurrentContractID
LEFT JOIN cc.SupplierContract sc2 ON
	sc2.ContractId = cj.OriginalContractID
LEFT JOIN cm.RejectionReason rr
	ON rr.RejectionReasonKey = cj.RejectionReasonKey
LEFT JOIN dbo.WebUser wu
	ON wu.WebUserKey = cj.ADVRecommendationWebUserKey
LEFT JOIN dbo.DimAccountGroup1 dag1
	ON dag1.AG1Level1 = cj.BranchCode
LEFT JOIN dbo.DimAccount da
	ON da.AccountNumber = cj.CustomerId
LEFT JOIN dbo.DimItem di
	ON di.ItemNumber = cj.ProductCode
LEFT JOIN cm.AdjustmentReason ar
	ON ar.AdjustmentReasonKey = cj.AdjustmentReasonKey










GO
