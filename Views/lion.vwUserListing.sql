SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





CREATE
VIEW [lion].[vwUserListing] 

/*
SELECT TOP 1000 * FROM lion.vwUserListing
SELECT * FROM lion.vwUser ORDER BY 1
SELECT * FROM WebRole
*/


AS

WITH 
 P2PUsers AS (
	SELECT DISTINCT
		WebUserKey
	FROM dbo.WebUserRole wur
	INNER JOIN dbo.WebRole wr
		ON wr.WebRoleKey = wur.WebRoleKey
	WHERE
		WebRoleName = 'P2PUser'
)

SELECT
	wu.WebUserKey,
	wu.ODSPersonKey,
	wu.AccountManagerKey,
	wu.WebUserPassword,
	wu.WebUserPasswordExpirationDate,

	wu.UserName,
	o.FullName,
	o.Email,
	o.ExtID,
	/*
	SELECT TOP 10 * FROM lion.vwUserListing
	*/
	STUFF((select ', ' + WebRoleDescription 
				from WebRole wr 
				join WebUserRole wur on 
					wr.WebRoleKey = wur.WebRoleKey 
				where wur.WebUserKey = wu.webuserkey and
					wr.WebRoleName <> 'P2PUser' and wr.WebRoleName <> 'Administrator' --and
					--wr.WorkflowLevel is null
				FOR XML PATH ('')), 1,1,'') as RoleList,
	CASE ISNULL(o.LevelId, '')
		WHEN 'BranchManager' THEN 'B'
		WHEN 'NetworkManager' THEN 'N'
		WHEN 'CentralPricing' THEN 'C'
		ELSE ''
	END AS  WorkflowLevelIndicator,		
	--CASE
	--	WHEN Admins.WebUserKey IS NULL THEN 'N'
	--	ELSE 'Y'
	--END AdminRoleIndicator,
	CASE
		WHEN P2PUsers.WebUserKey IS NULL THEN 'N'
		ELSE 'Y'
	END P2PUserRoleIndicator
FROM dbo.WebUser wu
JOIN dbo.ODSPerson o 
	ON o.ODSPersonKey = wu.ODSPersonKey
--LEFT JOIN Admins
--	ON Admins.WebUserKey = wu.WebUserKey
LEFT JOIN P2PUsers
	ON P2PUsers.WebUserKey = wu.WebUserKey








GO
