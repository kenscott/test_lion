SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO











CREATE
VIEW [lion].[vwRPBPriceBandAttributes]
AS

-- select top 1 * from lion.vwRPBPriceBandAttributes


SELECT DISTINCT
	ppg.PlaybookPricingGroupKey,
	PricingRuleSequenceNumber,
	ISNULL(PlaybookRegion, N'ALL') AS PlaybookRegion,
	ISNULL(CustomerBrandSegment, N'ALL') AS CustomerBrandSegment,
	ISNULL(Sales603010Bucket, N'ALL') AS Sales603010Bucket,
	ISNULL(StrategicItem, N'ALL') AS StrategicItem,
	ISNULL(PriceApproach, N'ALL') AS PriceApproach,
	ISNULL(Item, N'ALL') AS Item,
	ISNULL(LLSPG, N'ALL') AS LLSPG,
	--ISNULL(HLSPG, N'ALL') AS HLSPG,
	--ISNULL(GSPG, N'ALL') AS GSPG,
	
	ISNULL(ContractClaimsIndicator, N'ALL') AS ContractClaimsIndicator,
	ISNULL(ShipmentType, N'ALL') AS ShipmentType,
	ISNULL(PlaybookProductBrand, N'ALL') AS PlaybookProductBrand,
	ISNULL(RegionExceptionIndicator, N'ALL') AS RegionExceptionIndicator,
	ISNULL(PyramidCode, N'ALL') AS PyramidCode

	
FROM PlaybookPricingGroup ppg (NOLOCK)
INNER JOIN dbo.ATKPricingRule apr (NOLOCK)
	ON ppg.PricingRuleKey = apr.PricingRuleKey
CROSS APPLY [lion].[fn_GetRPBPriceBandAttributes](GroupValues, GroupingColumns) v
--WHERE ppg.PlaybookPricingGroupKey = 27442














GO
