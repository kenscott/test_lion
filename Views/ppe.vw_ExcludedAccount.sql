SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
Author: James Dameron
Purpose:  The original purpose of this view was to populate the Exclude Accounts grid
		  in the Price Flow application.

Example: 

select * from ppe.excludeaccount
Select * from ppe.vw_ExcludedAccount

*/

CREATE VIEW [ppe].[vw_ExcludedAccount] AS 
	SELECT 
		NEWID() AS GUID,
		ea.AccountKey,
		ea.ExcludeAccountKey,
		da.AccountNumber,
		da.AccountName,
		ea.CreationDate
	FROM ppe.ExcludeAccount ea
	JOIN DimAccount da ON da.AccountKey = ea.AccountKey

GO
