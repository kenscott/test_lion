SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE 
VIEW [lion].[vwAccountBrandOwner] 

/*
SELECT TOP 100 * FROM lion.vwAccountBrandOwner 
*/


AS

SELECT DISTINCT
	--dabm.AccountBrandManagerKey,
	dabm.AccountManagerKey,
	dam.AccountManagerCode,
	dp.FullName AS AccountManagerFullName
FROM dbo.DimAccountBrandManager dabm (NOLOCK)
INNER JOIN dbo.DimAccountManager dam (NOLOCK)
	ON dam.AccountManagerKey = dabm.AccountManagerKey
JOIN dbo.DimPerson dp (NOLOCK)
	ON dp.DimPersonKey = dam.DimPersonKey











































GO
