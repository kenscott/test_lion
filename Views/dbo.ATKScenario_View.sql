SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [dbo].[ATKScenario_View] 
AS
SELECT 
C.ProjectSpecificationKey, 
A.ScenarioKey, 
A.ScenarioName, 
B.ApproachTypeKey,
B.ApproachTypeDesc, 
A.SuppressionTypeKey, 
SuppressionTypeDesc,
A.CreationDate 'ScenarioCreationDate',
(SELECT MAX(ProjectKey) FROM PlaybookProject P WHERE P.ProjectSpecificationKey = C.ProjectSpecificationKey) LastProjectKey,
(SELECT MAX(PlaybookDataPointGroupKey) FROM PlaybookDataPointGroup P WHERE P.ProjectKey = (SELECT MAX(ProjectKey) FROM PlaybookProject P WHERE P.ProjectSpecificationKey = C.ProjectSpecificationKey)) LastPlaybookDataPointGroupKey,
(SELECT ProjectName FROM PlaybookProject P WHERE P.ProjectKey = (SELECT MAX(ProjectKey) FROM PlaybookProject P WHERE P.ProjectSpecificationKey = C.ProjectSpecificationKey)) LastProjectName,
(SELECT ProjectDescription FROM PlaybookProject P WHERE P.ProjectKey = (SELECT MAX(ProjectKey) FROM PlaybookProject P WHERE P.ProjectSpecificationKey = C.ProjectSpecificationKey)) LastProjectDescription,
(SELECT CreationDate FROM PlaybookProject P WHERE P.ProjectKey = (SELECT MAX(ProjectKey) FROM PlaybookProject P WHERE P.ProjectSpecificationKey = C.ProjectSpecificationKey)) LastProjectCreationDate

FROM ATKScenario A
JOIN ATKApproachType B ON A.ApproachTypeKey = B.ApproachTypeKey
JOIN ATKProjectSpecificationScenario C ON C.ScenarioKey = A.ScenarioKey
JOIN ATKSuppressionType D ON D.SuppressionTypeKey = A.SuppressionTypeKey




GO
