SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE  
VIEW [dbo].[LogDCP_View] 
AS

SELECT 
	ProjectKey, 
	DATEDIFF(SECOND, BeginTime, EndTime)/60.0 AS 'Minutes', 
	StepDesc, 
	BeginTime, 
	RowsInserted, 
	RowsUpdated, 
	RowsDeleted,
	LogDCPID
FROM 
	LogDCP WITH (NOLOCK)


GO
