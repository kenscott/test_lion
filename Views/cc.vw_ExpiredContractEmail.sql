SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO






/*
	Author: James Dameron
	Date: 9/15/2016
	Jira Reference: LPF-2947
	Description:  

	Example usage:
		select * from cc.vw_ExpiredContractEmail order by contactemail
		
		select count(*) from cc.SupplierContract where contactemail is null

	Reference queries:

		SELECT CAST(DATEADD(dd, -(DAY(GETDATE())-1), GETDATE()) AS DATE) AS FirstDayOfCurrentMonth
		SELECT CAST(DATEADD(s, -1, DATEADD(mm, DATEDIFF(m,0,GETDATE())+1,0)) AS Date ) AS LastDayOfCurrentMonth

		SELECT CAST(DATEADD(dd, -(DAY(GETDATE())-1), DATEADD(m, 1, GETDATE())) AS DATE) AS FirstDayOfNextMonth
		SELECT CAST(DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,GETDATE())+2,0)) AS Date) AS LastDayOfNextMonth
		
		SELECT CAST(DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,GETDATE())+3,0)) AS Date) AS LastDayOfNext2Months
		
		select top 10 * from dimaccountgroup1
*/


CREATE VIEW [cc].[vw_ExpiredContractEmail]
AS


		SELECT 
			NEWID() as GUID,
			'ThisMonth' AS ExpiresWhen,
			sc.ContractID,
			dv.VendorDescription AS SupplierName,
			sc.ContractReference AS SupplierReference,
			sc.ExpirationDate,
			sc.ContractOwner,
			ISNULL(sc.ContactName, 'Unknown') as ContactName,
			ISNULL(sc.ContactEmail, 'contract.claims@wolseley.co.uk') as ContactEmail,
			CASE 
				WHEN sc.IncludeAllAccounts = 1 THEN 'ALL' 
				WHEN 
					( SELECT COUNT(Distinct sca.AccountKey) 
						FROM cc.SupplierContractAccount sca WITH (NOLOCK)
						WHERE sca.ContractKey = sc.ContractKey ) = 0 THEN 'ALL'
				WHEN 
					( SELECT COUNT(Distinct sca.AccountKey) 
						FROM cc.SupplierContractAccount sca WITH (NOLOCK)
						WHERE sca.ContractKey = sc.ContractKey ) > 1 THEN 'Various'
				
				ELSE ( SELECT TOP 1 ( da.AccountNumber + ' ' + da.AccountName  )
						FROM cc.SupplierContractAccount sca WITH (NOLOCK)
						JOIN dbo.DimAccount da ON da.AccountKey = sca.AccountKey 
						WHERE sca.ContractKey = sc.ContractKey )
			END AS AccountDisplay
		FROM cc.SupplierContract sc WITH (NOLOCK)
		JOIN dbo.DimVendor dv
			ON dv.VendorKey = sc.VendorKey
		WHERE ( ExpirationDate BETWEEN CAST(DATEADD(dd, -(DAY(GETDATE())-1), GETDATE()) AS DATE) AND CAST(DATEADD(s, -1, DATEADD(mm, DATEDIFF(m,0,GETDATE())+1,0)) AS Date ) )

		UNION

		SELECT 
			NEWID() as GUID,
			'NextMonth' AS ExpiresWhen,
			sc.ContractID,
			dv.VendorDescription AS SupplierName,
			sc.ContractReference AS SupplierReference,
			sc.ExpirationDate,
			sc.ContractOwner,
			ISNULL(sc.ContactName, 'Unknown') as ContactName,
			ISNULL(sc.ContactEmail, 'contract.claims@wolseley.co.uk') as ContactEmail,
			CASE 
				WHEN sc.IncludeAllAccounts = 1 THEN 'ALL' 
				WHEN 
					( SELECT COUNT(Distinct sca.AccountKey) 
						FROM cc.SupplierContractAccount sca WITH (NOLOCK)
						WHERE sca.ContractKey = sc.ContractKey ) = 0 THEN 'ALL'
				WHEN 
					( SELECT COUNT(Distinct sca.AccountKey) 
						FROM cc.SupplierContractAccount sca WITH (NOLOCK)
						WHERE sca.ContractKey = sc.ContractKey ) > 1 THEN 'Various'
				
				ELSE ( SELECT TOP 1 ( da.AccountNumber + ' ' + da.AccountName  )
						FROM cc.SupplierContractAccount sca WITH (NOLOCK)
						JOIN dbo.DimAccount da ON da.AccountKey = sca.AccountKey 
						WHERE sca.ContractKey = sc.ContractKey )
			END AS AccountDisplay
		FROM cc.SupplierContract sc WITH (NOLOCK)
		JOIN dbo.DimVendor dv
			ON dv.VendorKey = sc.VendorKey
		WHERE (ExpirationDate BETWEEN CAST(DATEADD(dd, -(DAY(GETDATE())-1), DATEADD(m, 1, GETDATE())) AS Date) AND 
									 CAST(DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,GETDATE())+2,0)) AS Date) )

		UNION
			
		SELECT
			NEWID() as GUID,
			'Next2Months' AS ExpiresWhen,
			sc.ContractID,
			dv.VendorDescription AS SupplierName,
			sc.ContractReference AS SupplierReference,
			sc.ExpirationDate,
			sc.ContractOwner,
			ISNULL(sc.ContactName, 'Unknown') as ContactName,
			ISNULL(sc.ContactEmail, 'contract.claims@wolseley.co.uk') as ContactEmail,
			CASE 
				WHEN sc.IncludeAllAccounts = 1 THEN 'ALL' 
				WHEN 
					( SELECT COUNT(Distinct sca.AccountKey) 
						FROM cc.SupplierContractAccount sca WITH (NOLOCK)
						WHERE sca.ContractKey = sc.ContractKey ) = 0 THEN 'ALL'
				WHEN 
					( SELECT COUNT(Distinct sca.AccountKey) 
						FROM cc.SupplierContractAccount sca WITH (NOLOCK)
						WHERE sca.ContractKey = sc.ContractKey ) > 1 THEN 'Various'
				ELSE 
					( SELECT TOP 1 ( da.AccountNumber + ' ' + da.AccountName  )
						FROM cc.SupplierContractAccount sca WITH (NOLOCK)
						JOIN dbo.DimAccount da ON da.AccountKey = sca.AccountKey 
						WHERE sca.ContractKey = sc.ContractKey )
			END AS AccountDisplay
			FROM cc.SupplierContract sc WITH (NOLOCK)
			JOIN dbo.DimVendor dv
				ON dv.VendorKey = sc.VendorKey
			WHERE (ExpirationDate BETWEEN CAST(DATEADD(dd, -(DAY(GETDATE())-1), DATEADD(m, 1, GETDATE())) AS Date) AND 
									 CAST(DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,GETDATE())+3,0)) AS Date))







GO
