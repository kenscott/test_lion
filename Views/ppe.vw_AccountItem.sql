SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE VIEW [ppe].[vw_AccountItem]  AS
	SELECT 
		NEWID() AS GUID,
		di.ItemKey,
		ItemUDVarChar1 AS ItemPyramidCode,
		da.AccountKey,
		dig3.IG3Level1 AS SPGCode,
		dig3.IG3Level1UDVarchar1 AS SPGDescription,
		di.ItemNumber,
		di.ItemDescription,
		AccountUDVarChar12 AS FixedPriceIndicator,
		CASE
			WHEN ItemUDVarChar13 = 'Y' AND NOT EXISTS (SELECT TOP 1 * FROM lion.AccountSPG 
														WHERE AccountKey = da.AccountKey
														AND (LLSPGCode = dig3.IG3Level1 OR LLSPGCode = 'ALL') ) THEN 'Y'
			ELSE 'N'
		END AS KVIIndicator,
	ItemUDVarChar20 AS ProductCode
	FROM dbo.DimItem di JOIN dbo.DimItemGroup3 dig3 ON dig3.ItemGroup3Key = di.ItemGroup3Key,
	dbo.DimAccount da 
	WHERE da.accountkey <> 1
	AND di.itemkey <> 1

GO
