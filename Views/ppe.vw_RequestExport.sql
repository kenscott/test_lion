SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





CREATE VIEW [ppe].[vw_RequestExport]
AS 

/*
select * from ppe.vw_requestexport

select * from ppe.requestmain where statuskey = 7
*/

	SELECT 
		NEWID() AS GUID,
		rm.RequestKey,
		rm.StatusKey,
		rs.StatusName,
		da.AccountNumber,
		dig3.IG3Level4 AS PyramidCode,
		CASE WHEN rm.OwningBrandCode <> '' THEN rm.OwningBrandCode ELSE da1.AG1Level1UDVarchar6 END AS OwningBrandCode,
		IG3Level1 AS LLSPGCode,--1
		rm.NewSPGDiscount1,
		rm.NewSPGDiscount2,
		rm.NewExceptionDiscount,
		wu.UserName AS UpdateName,
		AG1Level1 AS UpdateBranch,
		di.ItemClientUniqueIdentifier,
		rm.NewExceptionFixedPrice, 
		rm.NewExceptionStartDate,
		rm.NewExceptionExpiryDate,
		CASE
			WHEN rm.NewDeleteTerm IS NULL THEN 0
			ELSE rm.NewDeleteTerm
		END AS NewDeleteTerm,
		CASE
			WHEN ISNULL(di.itemudvarchar18,'') = '' THEN 1
			ELSE ISNULL(iuc.PricingConvFact,1)
		END AS productPricingConversionFactor,
		FixedPriceFlag,
		rm.NewSPGFromDate,
		rm.NewTermType,
		rm.TypeKey,
		rm.PriorOwnerWebUserKey,
		rm.OwnerWebUserKey

	FROM ppe.RequestMain rm
	JOIN dbo.DimItem di ON di.ItemKey = rm.ItemKey
	JOIN dbo.DimItemGroup3 dig3 ON dig3.ItemGroup3Key = rm.ItemGroup3Key
	JOIN dbo.DimAccount da ON da.AccountKey = rm.AccountKey
	JOIN dbo.DimAccountGroup1 da1 ON da1.AccountGroup1Key = da.AccountGroup1Key
	JOIN ppe.RequestStatus rs ON rs.StatusKey = rm.StatusKey
	JOIN dbo.WebUser wu ON wu.WebUserKey = rm.ModifiedByWebUserKey
	LEFT JOIN ItemUOMConversion iuc ON iuc.ItemKey = rm.ItemKey
	WHERE rs.StatusName = 'Approved'
	AND (rm.NewSPGFromDate <= GETDATE() OR rm.NewSPGFromDate IS NULL)		-- LPF-6366: Create Recommendations | Effective Date not being applied to products











GO
