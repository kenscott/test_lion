SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [lion].[vwAccountManager]
AS 

/*
SELECT * FROM lion.vwAccountManager
*/

SELECT
	dam.AccountManagerKey,
	dp.FullName,
	wu.Username
FROM dbo.DimAccountManager dam
INNER JOIN dbo.DimPerson dp
	ON dp.DimPersonKey = dam.DimPersonKey
LEFT JOIN dbo.WebUser wu
	ON wu.AccountManagerKey = dam.AccountManagerKey

GO
