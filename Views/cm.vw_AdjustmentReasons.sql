SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



--select top 10 * from [cm].[vw_AdjustmentReasons]


CREATE VIEW [cm].[vw_AdjustmentReasons] AS

SELECT 
	ar.AdjustmentReasonKey,
	ar.AdjustmentReasonCode,
	ar.AdjustmentReasonValue,
	ar.CreationDate,
	ar.CreatedByWebUserKey,
	cwu.UserName AS 'CreatedByUsername',
	codsp.FullName AS 'CreatedByFullname',
	ar.ModificationDate,
	ar.ModifiedByWebUserKey,
	mwu.UserName AS 'ModifiedByUsername',
	modsp.FullName AS 'ModifiedByFullname',
	ar.Inactive,
	ar.CostCenter
FROM cm.AdjustmentReason ar
INNER JOIN dbo.WebUser cwu 
	ON cwu.WebUserKey = ar.CreatedByWebUserKey
INNER JOIN dbo.ODSPerson codsp 
	ON codsp.ODSPersonKey = cwu.ODSPersonKey
LEFT JOIN dbo.WebUser mwu 
	ON mwu.WebUserKey = ar.ModifiedByWebUserKey
LEFT JOIN dbo.ODSPerson modsp 
	ON modsp.ODSPersonKey = mwu.ODSPersonKey






GO
