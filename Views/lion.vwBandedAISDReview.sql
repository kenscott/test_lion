SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





CREATE
VIEW [lion].[vwBandedAISDReview] 

/*
SELECT TOP 0 * FROM lion.vwBandedAISDReview 
SELECT TOP 10 * FROM lion.vwBandedAISDReview 
*/


AS

SELECT
	flpac.[AccountKey],
	flpac.[ItemKey],
	flpac.[InvoiceLineGroup1Key],

	dag1.AG1Level1 AS CustomerBranch,
	dag1.AG1Level2 AS CustomerNetwork,
	dag1.AG1Level3 AS CustomerArea,
	dag1.AG1Level4 AS CustomerRegion,
	--dag1.AG1Level5 AS CustomerCompanyName,

	dag1.AG1Level1UDVarchar1 AS CustomerBranchName,
	dag1.AG1Level2UDVarchar1 AS CustomerNetworkDescription,
	dag1.AG1Level3UDVarchar1 AS CustomerAreaDescription,
	dag1.AG1Level4UDVarchar1 AS CustomerRegionDescription,
	--dag1.AG1Level5UDVarchar1 AS CustomerCompanyNameDescription,

	dag1.AG1Level1UDVarchar2 AS CustomerPlaybookRegion,
	dag1.AG1Level1UDVarchar3 AS CustomerPlaybookRegionDescription,
	dag1.AG1Level1UDVarchar4 AS CustomerPlaybookArea,
	dag1.AG1Level1UDVarchar5 AS CustomerPlaybookAreaDescription,
	
	dag1.AG1Level1UDVarchar20 AS CustomerBranchPlaybookBrandIndicator,
	dag1.AG1Level1UDVarchar21 AS CustomerBranchBrandGroup,
	dag1.AG1Level1UDVarchar22 AS CustomerBranchPGActiveIndicator,


	dag1.AG1Level1UDVarchar6 AS CustomerBranchPrimaryBrand,
	dag1.AG1Level1UDVarchar7 AS CustomerBranchTradingStatus,
	dag1.AG1Level1UDVarchar8 AS CustomerBranchType,
	dag1.AG1Level1UDVarchar9 AS CustomerBranchAddressLine1,
	dag1.AG1Level1UDVarchar10 AS CustomerBranchAddressLine2,
	dag1.AG1Level1UDVarchar11 AS CustomerBranchAddressLine3,
	dag1.AG1Level1UDVarchar12 AS CustomerBranchAddressLine4,
	dag1.AG1Level1UDVarchar13 AS CustomerBranchAddressLine5,
	dag1.AG1Level1UDVarchar14 AS CustomerBranchPostcode,
	dag1.AG1Level1UDVarchar15 AS CustomerBranchEmail,
	dag1.AG1Level1UDVarchar16 AS CustomerBranchTelephone,
	dag1.AG1Level1UDVarchar17 AS CustomerBranchPostCodeArea,
	dag1.AG1Level1UDVarchar18 AS CustomerBranchPostCodeDistrict,
	dag1.AG1Level1UDVarchar19 AS CustomerBranchPyramidCode,
	dag1.AG1Level1UDVarchar25 AS RegionExceptionIndicator,
		
	dag1Sale.AG1Level1 AS LastSaleBranch,
	dag1Sale.AG1Level2 AS LastSaleNetwork,
	dag1Sale.AG1Level3 AS LastSaleArea,
	dag1Sale.AG1Level4 AS LastSaleRegion,
	--dag1Sale.AG1Level5 AS LastSaleCompanyName,

	dag1Sale.AG1Level1UDVarchar1 AS LastSaleBranchName,
	dag1Sale.AG1Level2UDVarchar1 AS LastSaleNetworkDescription,
	dag1Sale.AG1Level3UDVarchar1 AS LastSaleAreaDescription,
	dag1Sale.AG1Level4UDVarchar1 AS LastSaleRegionDescription,
	--dag1Sale.AG1Level5UDVarchar1 AS LastSaleCompanyNameDescription,

	dag1Sale.AG1Level1UDVarchar2 AS LastSalePlaybookRegion,
	dag1Sale.AG1Level1UDVarchar3 AS LastSalePlaybookRegionDescription,
	dag1Sale.AG1Level1UDVarchar4 AS LastSalePlaybookArea,
	dag1Sale.AG1Level1UDVarchar5 AS LastSalePlaybookAreaDescription,

	dag1Sale.AG1Level1UDVarchar6 AS LastSaleBranchPrimaryBrand,
	dag1Sale.AG1Level1UDVarchar7 AS LastSaleBranchTradingStatus,
	dag1Sale.AG1Level1UDVarchar8 AS LastSaleBranchType,
	dag1Sale.AG1Level1UDVarchar9 AS LastSaleBranchAddressLine1,
	dag1Sale.AG1Level1UDVarchar10 AS LastSaleBranchAddressLine2,
	dag1Sale.AG1Level1UDVarchar11 AS LastSaleBranchAddressLine3,
	dag1Sale.AG1Level1UDVarchar12 AS LastSaleBranchAddressLine4,
	dag1Sale.AG1Level1UDVarchar13 AS LastSaleBranchAddressLine5,
	dag1Sale.AG1Level1UDVarchar14 AS LastSaleBranchPostcode,
	dag1Sale.AG1Level1UDVarchar15 AS LastSaleBranchEmail,
	dag1Sale.AG1Level1UDVarchar16 AS LastSaleBranchTelephone,
	dag1Sale.AG1Level1UDVarchar17 AS LastSaleBranchPostCodeArea,
	dag1Sale.AG1Level1UDVarchar18 AS LastSaleBranchPostCodeDistrict,
	dag1Sale.AG1Level1UDVarchar19 AS LastSaleBranchPyramidCode,

	AG2Level1 AS CustomerType,
	AG2Level1UDVarchar1 AS CustomerTypeDescription,

	AG3Level1 AS TradingStatusCategory,
	AG3Level1UDVarchar1 AS TradingStatusCategoryDescription,

	dam.AccountManagerCode,
	dp.FullName AS AccountManagerFullName,
	dp.Email AS AccountManagerEmail,
	
	AccountClientUniqueIdentifier,
	AccountNumber,
	AccountName,
	AccountNumberParent,
	AccountAddress1,
	AccountAddress2,
	AccountAddress3,
	AccountAddress4,
	AccountZipCode AS AccountPostCode,
	
	--AccountMemo AS CustomerType,
	AccountUDVarChar1 AS CreditLimit,
	AccountUDVarChar2 AS PaymentTermName,
	AccountUDVarChar3 AS IntercompanyAccount,
	AccountUDVarChar4 AS OverLimitStatus,
	AccountUDVarChar5 AS OverdueStatus,
	AccountUDVarChar6 AS TermsParent,
	AccountUDVarChar7 AS PricingCategory,
	AccountUDVarChar8 AS AccountOwnerType,
	da.AccountUDVarChar9 AS AccountOwnerBucket,
	AccountUDVarChar10 AS CustomerSegment,
	AccountUDVarchar11 AS CustomerRebate,
	
	AccountUDDateTime1 AS DateOpened,
	AccountUDDateTime2 AS DateClosed,

	DimVendor.VendorKey AS VendorKey,
	VendorDescription,
	VendorNumber,

	ItemNumber,
	ItemDescription,
	--BaseUnitOfMeasure, 
	VendorStockNumber,
	
	ItemUDVarChar1 AS ItemPyramidCode,
	ItemUDVarChar2 AS ProdBrandAtLLSPG,
	ItemUDVarChar3 AS MPGCode,
	ItemUDVarChar4 AS MPGDescription,
	ItemUDVarChar5 AS SuperCategory,
	ItemUDVarChar6 AS Category,
	ItemUDVarChar7 AS SubCategory,
	ItemUDVarChar8 AS ItemObsolete,
	ItemUDVarChar9 AS HireInd,
	ItemUDVarChar10 AS SpecialInd,
	ItemUDVarChar11 AS ProductLifecycle,
	ItemUDVarChar12 AS OwnBrandInd,
	ItemUDVarChar13 AS KnownValueInd,
	ItemUDVarChar14 AS NDPInd,
	ItemUDVarChar15 AS MultiSourcedProductInd,
	ItemUDVarChar16 AS VariablePackInd,
	ItemUDVarChar17 AS ZonedInd,
	ItemUDVarChar18 AS UnitCode,
	ItemUDVarChar19 AS GenericProductID,
	ItemUDVarChar21 AS PlaybookProductBrand,
	ItemUDDate1 AS ItemDateForDeletion,
	
	--IG2Level1 AS ProdBrandAtLLSPG,
	--IG2Level1UDVarchar1 AS ProductTypeDescription,
	
	IG3Level1 AS LLSPGCode,--1
	IG3Level2 AS HLSPGCode,--2
	IG3Level3 AS GSPGCode,--3
	--IG3Level4 AS PYRCode,--4
	IG3Level1UDVarchar1 AS LLSPGDescription,--1
	IG3Level2UDVarchar1 AS HLSPGDescription,--2
	IG3Level3UDVarchar1 AS GSPGDescription, --3

	InvoiceLineGroup1UDVarchar1 AS ShipmentType,
	--InvoiceLineGroup1UDVarchar2 AS DeviatedIndicator,
	InvoiceLineGroup1UDVarchar2 AS ContractClaimsIndicator,  -- formally DeviatedIndicator

	ISNULL(flpac.UDVarchar1, '') AS StrategicItem,
	ISNULL(flpac.UDVarchar2, '') AS SubLLSPG,
	ISNULL(flpac.UDVarchar3, '') AS NewItemForAccountIndicator,
	ISNULL(flpac.UDVarchar4, '') AS Sales603010Bucket,
	ISNULL(flpac.UDVarchar5, '') AS TotalSalesAccountBucket,
	ISNULL(flpac.UDVarchar6, '') AS FrequencyBucket,
	ISNULL(flpac.UDVarchar7, '') AS CustBaseBucket,
	ISNULL(flpac.UDVarchar8, '') AS FreqPurchBucket,
	ISNULL(flpac.UDVarchar9, '') AS TopSellerBucket,
	flpac.UDVarchar10 AS PriceApproach,
	flpac.CustomerBrandSegment AS CustomerBrandSegment,

	--ISNULL(flpac.UDVarchar10, '') AS '???',
	
	ISNULL(flpac.UDDecimal1, 0.) AS TotalAccountSales,
	ISNULL(flpac.UDDecimal2, 0.) AS TotalNumberOfItemsSoldByAccount,
	ISNULL(flpac.UDDecimal3, 0.) AS TotalAccountGP,
	ISNULL(flpac.UDDecimal4, 0.) AS TotalNumberOfSuperCategoriesSold,
	
	--ISNULL(flpac.UDDecimal5, 0.) AS LastUnitListPrice,
	--ISNULL(flpac.UDDecimal6, 0.) AS LastUnitRetail,

	CASE
		WHEN ItemUDVarChar1 <> N'1' THEN N'Y'		-- pyramid code
		WHEN ISNULL((flpac.LastItemPrice - flpac.LastItemCost) / NULLIF(flpac.LastItemPrice, 0.0), 0.0) <= 0. THEN N'Y'	-- LastGPP
		WHEN ISNULL(ItemUDVarChar10, N'') = N'Y' THEN N'Y'		-- SpecialInd
		WHEN ISNULL(ItemUDVarChar8, N'') IN (N'Y', N'YY') THEN N'Y'		-- Obsolete
		WHEN InvoiceLineGroup1UDVarchar1 NOT IN (N'S') THEN N'Y'
		-----WHEN filsum.TotalActualPrice < 10. THEN N'Y'
		WHEN AccountName = N'Unknown' THEN N'Y'
		WHEN ItemNumber = N'Unknown' THEN N'Y'
		WHEN flpac.LastQuantity < 0 THEN N'Y'
		WHEN flpac.LastItemPrice = 0 THEN N'Y'
		--WHEN ISNULL(fil.UDVarchar1, '') IN (N'SPO') THEN N'Y'  --last price derivation code
		WHEN ISNULL(InvoiceLineGroup2UDVarchar1, '') IN (N'SPO', N'CCP', N'QB') THEN 'Y'  --last price derivation code
		ELSE 'N'
	END AS ExcludeForSegmentationIndicator,
	CASE
		WHEN ItemUDVarChar1 <> N'1' THEN N'Y'		-- pyramid code
		WHEN ISNULL((flpac.LastItemPrice - flpac.LastItemCost) / NULLIF(flpac.LastItemPrice, 0.0), 0.0) <= 0. THEN N'Y'	-- LastGPP
		WHEN ISNULL(ItemUDVarChar10, N'') = N'Y' THEN N'Y'		-- SpecialInd
		WHEN ISNULL(ItemUDVarChar8, N'') IN (N'Y', N'YY') THEN N'Y'		-- Obsolete
		WHEN InvoiceLineGroup1UDVarchar1 NOT IN (N'S') THEN N'Y'
		-----WHEN filsum.TotalActualPrice < 10. THEN N'Y'
		WHEN AccountName = N'Unknown' THEN N'Y'
		WHEN ItemNumber = N'Unknown' THEN N'Y'
		WHEN flpac.LastQuantity < 0 THEN N'Y'
		WHEN flpac.LastItemPrice = 0 THEN N'Y'
		--WHEN ISNULL(fil.UDVarchar1, '') IN (N'SPO') THEN N'Y'  --last price derivation code
		WHEN ISNULL(InvoiceLineGroup2UDVarchar1, '') IN (N'SPO', N'CCP', N'QB') THEN 'Y'  --last price derivation code
		WHEN flpac.Total12MonthSales = 0 THEN 'Y'
		WHEN flpac.Total12MonthCost = 0 THEN 'Y'
		WHEN flpac.Total12MonthQuantity <= 0 THEN 'Y'
		WHEN ISNULL( (flpac.Total12MonthSales - flpac.Total12MonthCost) / NULLIF(flpac.Total12MonthSales, 0), 0) = 0 THEN 'Y'
		ELSE 'N'
	END AS ExcludeForSegmentationIndicatorWithAISDCriteria,

	 ddMax.DayKey - LastSaleDayKey AS DaysSinceLastSale,

	ISNULL(flpac.PercentAccountItemSalesToAccountSales, 0.) AS PercentAccountItemSalesToAccountSales,
	ISNULL(flpac.AverageQuantityPerOrder, 0.) AS AverageQuantityPerInvoiceLine,

	--leaving out as "invoice number" is what is being counted and I do not think that is right - sb legacy_sys, invoicedate, sys_inv, cred_code I think
	--CAST (
	--	CASE 
	--		WHEN flpac.Total12MonthInvoices IS NOT NULL AND flpac.Total12MonthInvoices <> 0 THEN (CAST(filsum.TotalActualPrice - filsum.TotalActualCost AS DEC(19,8))) / flpac.Total12MonthInvoices 
	--	END 
	--	AS DEC(19,8)
	--) AS AverageGrossProfitPerOrder,
	--
	--CASE 
	--	WHEN SQLdate >= DATEADD(DAY,-180,GETDATE()) THEN 'Y' 
	--	ELSE 'N' 
	--END AS InLast180Days,

	CONVERT (VARCHAR(10), dd.SQLDate, 120) AS LastInvoiceDate,
	
	flpac.LastQuantity,
	flpac.LastItemPrice,
	flpac.LastItemCost,
	flpac.LastItemDiscountPercent,

	flpac.LastItemPrice - flpac.LastItemCost AS LastItemGP,
	(flpac.LastItemPrice - flpac.LastItemCost) / NULLIF(flpac.LastItemPrice, 0.0) AS LastItemGPP,

	--LastClientInvoiceID AS LastTransactionId,
	--LastClientInvoiceLineID AS LastTransactionLineId,

	--flpac.Total12MonthInvoices,
	--filsum.Cnt AS Total12MonthInvoiceLineCount,
	--filsum.TotalQuantity AS Total12MonthQuantity,
	--filsum.TotalActualPrice AS Total12MonthSales,
	--filsum.TotalActualCost AS Total12MonthCost,

	--ISNULL(filsum.TotalActualPrice - filsum.TotalActualCost, 0.0) AS Total12MonthGP,
	--ISNULL((filsum.TotalActualPrice - filsum.TotalActualCost) / NULLIF(filsum.TotalActualPrice, 0.0), 0.0) AS Total12MonthGPP,
	
	flpac.Total12MonthInvoiceLines as Total12MonthInvoiceLineCount,
	flpac.Total12MonthQuantity as Total12MonthQuantity,
	flpac.Total12MonthSales as Total12MonthSales,
	flpac.Total12MonthCost as Total12MonthCost,
	
	ISNULL(flpac.Total12MonthSales - flpac.Total12MonthCost, 0.0) AS Total12MonthGP,
	ISNULL((flpac.Total12MonthSales - flpac.Total12MonthCost) / NULLIF(flpac.Total12MonthSales, 0.0), 0.0) AS Total12MonthGPP,

	--InvoiceLineGroup2UDVarchar1 AS LastDeviatedIndicator,  -- ClaimNo based... Supplier Contract; If Transaction.ClaimNo <> <blank> then "Y", else "N"
	InvoiceLineGroup2UDVarchar1 AS LastPriceDerivationBucket,
	InvoiceLineGroup3UDVarchar1 AS LastCustomerContractIndicator,	-- If Transaction.PriceDerivation = {DC, FM, GT, IP, OCD, OCL, OTG} then "Y", else "N"
	
	
	

	ISNULL(InvoiceLineJunkUDVarchar1, '') AS LastBranchCode,
	ISNULL(InvoiceLineJunkUDVarchar2, '') AS LastTransNo,
	ISNULL(InvoiceLineJunkUDVarchar3, '') AS LastDelNo,
	ISNULL(InvoiceLineJunkUDVarchar4, '') AS LastLineNumber,
	ISNULL(InvoiceLineJunkUDVarchar5, '') AS LastNetQty,
	ISNULL(InvoiceLineJunkUDVarchar6, '') AS LastNetLinePrice,
	ISNULL(InvoiceLineJunkUDVarchar7, '') AS LastNetApTradingMarginPadRelease,
	ISNULL(InvoiceLineJunkUDVarchar8, '') AS LastDeliveryDate,
	ISNULL(InvoiceLineJunkUDVarchar9, '') AS LastSledgerNo,
	ISNULL(InvoiceLineJunkUDVarchar10, '') AS LastSledgerDate,
	ISNULL(InvoiceLineJunkUDVarchar11, '') AS LastYMTH,
	ISNULL(InvoiceLineJunkUDVarchar12, '') AS LastSupplyType,
	ISNULL(InvoiceLineJunkUDVarchar13, '') AS LastDocInd,
	ISNULL(InvoiceLineJunkUDVarchar14, '') AS LastCreditReason,
	ISNULL(InvoiceLineJunkUDVarchar15, '') AS LastKnownValueItem,
	ISNULL(InvoiceLineJunkUDVarchar16, '') AS LastCtsRelCode,
	ISNULL(InvoiceLineJunkUDVarchar17, '') AS LastCustCntNo,
	ISNULL(InvoiceLineJunkUDVarchar18, '') AS LastMthdOfDespatch,
	ISNULL(InvoiceLineJunkUDVarchar19, '') AS LastTransType,
	ISNULL(InvoiceLineJunkUDVarchar20, '') AS LastSalesOrigin,
	ISNULL(InvoiceLineJunkUDVarchar21, '') AS LastTakenBy,
	ISNULL(InvoiceLineJunkUDVarchar22, '') AS LastMethodOfPayment,
	ISNULL(InvoiceLineJunkUDVarchar23, '') AS LastHeaderID,
	ISNULL(InvoiceLineJunkUDVarchar24, '') AS LastCrdOrigInvNo,
	ISNULL(InvoiceLineJunkUDVarchar25, '') AS LastCrdOrigInvDate,
	ISNULL(InvoiceLineJunkUDVarchar26, '') AS LastOriginalOrdNo,
	ISNULL(InvoiceLineJunkUDVarchar27, '') AS LastCarriagePack,
	ISNULL(InvoiceLineJunkUDVarchar28, '') AS LastDelAddPhone,
	ISNULL(InvoiceLineJunkUDVarchar29, '') AS LastGpAllocBranchCode,
	ISNULL(InvoiceLineJunkUDVarchar30, '') AS LastSalesOriginDesc,
	ISNULL(InvoiceLineJunkUDVarchar31, '') AS LastPriceOrderType,
	ISNULL(InvoiceLineJunkUDVarchar32, '') AS LastTransactionDate,
	ISNULL(InvoiceLineJunkUDVarchar33, '') AS LastOrderDate,	

	ISNULL(fil.UnitListPrice, 0.) AS LastTradingPrice,
	ROUND((ISNULL(fil.UnitListPrice, 0.) - LastItemCost)  / NULLIF(ISNULL(fil.UnitListPrice, 0.), 0),3) AS LastTradeGPP,
	ISNULL(
		CASE
			WHEN TotalQuantity IS NOT NULL AND TotalQuantity > 0. 
			AND UnitListPrice IS NOT NULL AND UnitListPrice > 0. 
			AND TotalActualPrice IS NOT NULL AND TotalActualPrice > 0.
			THEN 
				(UnitListPrice * flpac.LastQuantity) - (LastItemPrice * flpac.LastQuantity)
			ELSE
				NULL
		END
		, 0.) 
	AS LastTotalDiscount,
	
	fil.UDVarchar1 AS LastPriceDerivation,
	fil.UDVarchar2 AS LastVariablePackInd,
	fil.UDVarchar3 AS LastClaimNo,

	fil.UDDecimal1 AS LastApTradingMarginPadRelease,
	fil.UDDecimal2 AS LastCCProfit,
	fil.UDDecimal3 AS LastRetailPrice,
	
	RPBPlaybookPricingGroupKey,

	CoveringBandGroupValues,
	CoveringBandPricingRuleKey,	
	CoveringBandPlaybookPricingGroupQuantityBandKey,
	CoveringBandLowerBandQuantity,
	CoveringBandUpperBandQuantity,
	CoveringBandRulingMemberCount, 
	CoveringBandBandSequenceNumber,
	CoveringBandPricingRuleSequenceNumber,

	FloorPlaybookPricingGroupPriceBandKey,
	FloorPlaybookPricingGroupKey,
	FloorLowerBandQuantity,
	FloorUpperBandQuantity,
	FloorMemberRank,
	FloorRulingMemberCount,
	FloorBandSequenceNumber,
	FloorPlaybookPricingGroupQuantityBandKey,
	FloorPricingRuleSequenceNumber,

	TargetPlaybookPricingGroupPriceBandKey,
	TargetPlaybookPricingGroupKey,
	TargetLowerBandQuantity,
	TargetUpperBandQuantity,
	TargetMemberRank,
	TargetRulingMemberCount,
	TargetBandSequenceNumber,
	TargetPlaybookPricingGroupQuantityBandKey,
	TargetPricingRuleSequenceNumber,

	StretchPlaybookPricingGroupPriceBandKey,
	StretchPlaybookPricingGroupKey,
	StretchLowerBandQuantity,
	StretchUpperBandQuantity,
	StretchMemberRank,
	StretchRulingMemberCount,
	StretchBandSequenceNumber,
	StretchPlaybookPricingGroupQuantityBandKey,
	StretchPricingRuleSequenceNumber,

	EffectiveFloorDiscountPercent,
	EffectiveTargetDiscountPercent,
	EffectiveStretchDiscountPercent,

	
	--ppg.GroupValues AS CoveringBandGroupValues,
	--ppg.PricingRuleKey AS CoveringBandPricingRuleKey,	
	--TheTarget.PlaybookPricingGroupQuantityBandKey AS CoveringBandPlaybookPricingGroupQuantityBandKey,
	--TheTarget.LowerBandQuantity AS CoveringBandLowerBandQuantity,
	--TheTarget.UpperBandQuantity AS CoveringBandUpperBandQuantity,
	--TheTarget.RulingMemberCount AS CoveringBandRulingMemberCount, 
	--TheTarget.BandSequenceNumber AS CoveringBandBandSequenceNumber,
	--TheTarget.PricingRuleSequenceNumber AS CoveringBandPricingRuleSequenceNumber,
		
	--TheFloor.PlaybookPricingGroupPriceBandKey AS FloorPlaybookPricingGroupPriceBandKey,
	--TheFloor.PlaybookPricingGroupKey AS FloorPlaybookPricingGroupKey,
	--TheFloor.LowerBandQuantity AS FloorLowerBandQuantity,
	--TheFloor.UpperBandQuantity AS FloorUpperBandQuantity,
	--TheFloor.MemberRank AS FloorMemberRank,
	--TheFloor.RulingMemberCount AS FloorRulingMemberCount,
	--TheFloor.BandSequenceNumber AS FloorBandSequenceNumber,
	--TheFloor.PlaybookPricingGroupQuantityBandKey AS FloorPlaybookPricingGroupQuantityBandKey,
	--TheFloor.PricingRuleSequenceNumber AS FloorPricingRuleSequenceNumber,

	--TheTarget.PlaybookPricingGroupPriceBandKey AS TargetPlaybookPricingGroupPriceBandKey,
	--TheTarget.PlaybookPricingGroupKey AS TargetPlaybookPricingGroupKey,
	--TheTarget.LowerBandQuantity AS TargetLowerBandQuantity,
	--TheTarget.UpperBandQuantity AS TargetUpperBandQuantity,
	--TheTarget.MemberRank AS TargetMemberRank,
	--TheTarget.RulingMemberCount AS TargetRulingMemberCount,
	--TheTarget.BandSequenceNumber AS TargetBandSequenceNumber,
	--TheTarget.PlaybookPricingGroupQuantityBandKey AS TargetPlaybookPricingGroupQuantityBandKey,
	--TheTarget.PricingRuleSequenceNumber AS TargetPricingRuleSequenceNumber,

	--TheStretch.PlaybookPricingGroupPriceBandKey AS StretchPlaybookPricingGroupPriceBandKey,
	--TheStretch.PlaybookPricingGroupKey AS StretchPlaybookPricingGroupKey,
	--TheStretch.LowerBandQuantity AS StretchLowerBandQuantity,
	--TheStretch.UpperBandQuantity AS StretchUpperBandQuantity,
	--TheStretch.MemberRank AS StretchMemberRank,
	--TheStretch.RulingMemberCount AS StretchRulingMemberCount,
	--TheStretch.BandSequenceNumber AS StretchBandSequenceNumber,
	--TheStretch.PlaybookPricingGroupQuantityBandKey AS StretchPlaybookPricingGroupQuantityBandKey,
	--TheStretch.PricingRuleSequenceNumber AS StretchPricingRuleSequenceNumber,
	
	FloorPercentile,
	TargetPercentile,
	StretchPercentile,

	FloorGPP,
	TargetGPP,	
	StretchGPP
	
	--TheFloor.PDPGPP AS FloorGPP,
	--TheTarget.PDPGPP AS TargetGPP,	
	--TheStretch.PDPGPP AS StretchGPP,
	
	------o.FloorGPP AS PMPBFloorGP,
	------o.TargetGPP AS PMPBTargetGPP,
	------o.StretchGPP AS PMPBStretchGPP,
	
	------ISNULL(o.FloorGPP, TheFloor.PDPGPP) AS EffectiveFloorGPP,
	------ISNULL(o.TargetGPP, TheTarget.PDPGPP) AS EffectiveTargetGPP,
	------ISNULL(o.StretchGPP, TheStretch.PDPGPP) AS EffectiveStretchGPP,

	----TheFloor.PDPGPP AS EffectiveFloorGPP,
	----TheTarget.PDPGPP AS EffectiveTargetGPP,
	----TheStretch.PDPGPP AS EffectiveStretchGPP,

	--TheFloorDiscount.PDPDiscountPercent AS EffectiveFloorDiscountPercent,
	--TheTargetDiscount.PDPDiscountPercent AS EffectiveTargetDiscountPercent,
	--TheStretchDiscount.PDPDiscountPercent AS EffectiveStretchDiscountPercent

FROM
	dbo.FactLastPriceAndCost flpac (NOLOCK)
INNER JOIN dbo.DimInvoiceLineJunk diljunk (NOLOCK)
	ON diljunk.InvoiceLineJunkKey = flpac.LastInvoiceLineJunkKey
INNER JOIN dbo.DimDay dd (NOLOCK)
	ON dd.daykey = flpac.LastSaleDayKey
INNER JOIN dbo.DimDay ddMax (NOLOCK)
	ON ddMax.DayKey = (SELECT  MAX(InvoiceDateDayKey) FROM FactInvoiceLine)
INNER JOIN dbo.DimAccount da (NOLOCK)
	ON da.AccountKey = flpac.AccountKey
INNER JOIN dbo.DimAccountGroup1 dag1 (NOLOCK)
	ON dag1.AccountGroup1Key = da.AccountGroup1Key
INNER JOIN dbo.DimAccountGroup2 dag2 (NOLOCK)
	ON dag2.AccountGroup2Key = da.AccountGroup2Key
INNER JOIN dbo.DimAccountGroup3 dag3 (NOLOCK)
	ON dag3.AccountGroup3Key = da.AccountGroup3Key
INNER JOIN dbo.DimAccountManager dam (NOLOCK)
	ON dam.AccountManagerKey = da.AccountManagerKey
JOIN dbo.DimPerson dp (NOLOCK)
	ON dp.DimPersonKey = dam.DimPersonKey
--LEFT JOIN dbo.BridgeAccountManager bam (NOLOCK)
--	ON bam.SubsidiaryAccountManagerKey = da.AccountManagerKey
--	AND NumberOfLevels = 1
--LEFT JOIN dbo.WebUser wuMgr (NOLOCK)
--	ON wuMgr.AccountManagerKey = bam.ParentAccountManagerKey
--LEFT JOIN dbo.ODSPerson odspMgr (NOLOCK)
--	ON odspMgr.ODSPersonKey = wuMgr.ODSPersonKey
INNER JOIN dbo.DimItem di (NOLOCK)
	ON di.ItemKey = flpac.ItemKey
INNER JOIN dbo.DimItemGroup1 dig1 (NOLOCK)
	ON dig1.ItemGroup1Key = di.ItemGroup1Key
INNER JOIN dbo.DimItemGroup2 dig2 (NOLOCK)
	ON dig2.ItemGroup2Key = di.ItemGroup2Key
INNER JOIN dbo.DimItemGroup3 dig3 (NOLOCK)
	ON dig3.ItemGroup3Key = di.ItemGroup3Key
INNER JOIN dbo.DimVendor (NOLOCK)
	ON DimVendor.VendorKey = di.ItemVendorKey
INNER JOIN dbo.DimInvoiceLineGroup1 (NOLOCK)
	ON DimInvoiceLineGroup1.InvoiceLineGroup1Key = flpac.InvoiceLineGroup1Key
INNER JOIN dbo.DimInvoiceLineGroup2 (NOLOCK)
	ON DimInvoiceLineGroup2.InvoiceLineGroup2Key = flpac.LastInvoiceLineGroup2Key
INNER JOIN dbo.DimInvoiceLineGroup3 (NOLOCK)
	ON DimInvoiceLineGroup3.InvoiceLineGroup3Key = flpac.LastInvoiceLineGroup3Key
--INNER JOIN (
--			SELECT 
--				AccountKey, 
--				ItemKey, 
--				InvoiceLineGroup1Key,
--				SUM(TotalQuantity) TotalQuantity,
--				COUNT(*) Cnt,
--				SUM(TotalActualCost) TotalActualCost,
--				SUM(TotalActualPrice) TotalActualPrice
--			FROM dbo.FactInvoiceLine fil (NOLOCK) 
--			WHERE Last12MonthsIndicator = 'Y'
--			GROUP BY 
--				AccountKey, 
--				ItemKey, 
--				InvoiceLineGroup1Key
--		) FILSUM
----INNER JOIN  FILSUM
--		ON FILSUM.AccountKey = flpac.AccountKey 
--		AND FILSUM.ItemKey = flpac.ItemKey 
--		AND FILSUM.InvoiceLineGroup1Key = flpac.InvoiceLineGroup1Key		
LEFT JOIN dbo.FactInvoiceLine fil
	ON flpac.LastInvoiceLineUniqueIdentifier = fil.InvoiceLineUniqueIdentifier
LEFT JOIN dbo.DimAccountGroup1 dag1Sale
	ON dag1Sale.AccountGroup1Key = fil.AccountGroup1Key

--INNER JOIN dbo.FactRecommendedPriceBand pb
--	ON pb.AccountKey = flpac.AccountKey
--	AND pb.ItemKey = flpac.ItemKey
--	AND pb.InvoiceLineGroup1Key = flpac.InvoiceLineGroup1Key

OUTER APPLY lion.fn_GetPercentile (
		di.ItemUDVarchar2,				-- product brand
		dag1.AG1Level4,					-- Region,
		dag1.AG1Level3,					-- Area,
		dag1.AG1Level2,					-- Network
		dag1.AG1Level1,					-- Branch,
		flpac.UDVarchar4,				-- Sales603010Bucket
		ItemUDVarChar1,					-- ItemPyramidCode
		IG3Level1						-- LLSPGCodee
	)
OUTER APPLY lion.fn_RPBPriceBandRAGForAnalysis (
		flpac.RPBPlaybookPricingGroupKey,
		flpac.Total12MonthQuantity,		-- CustomerPlaybookRegion
		FloorPercentile,					-- Branch
		TargetPercentile,				-- Sales603010Bucket
		StretchPercentile
	)



--LEFT JOIN dbo.PlaybookPricingGroupPriceBand TheFloor (NOLOCK)
--	ON TheFloor.PlaybookPricingGroupKey = flpac.RPBPlaybookPricingGroupKey
--	AND flpac.Total12MonthQuantity BETWEEN TheFloor.LowerBandQuantity AND TheFloor.UpperBandQuantity 
--	AND TheFloor.MemberRank = 
--		CASE
--			WHEN  ROUND ( TheFloor.RulingMemberCount * FloorPercentile, 0) BETWEEN 1 AND TheFloor.RulingMemberCount THEN  ROUND ( TheFloor.RulingMemberCount * FloorPercentile, 0)
--			ELSE 1
--		END
--LEFT JOIN dbo.PlaybookPricingGroup ppg (NOLOCK)
--	ON ppg.PlaybookPricingGroupKey = TheFloor.PlaybookPricingGroupKey
--LEFT JOIN dbo.PlaybookPricingGroupPriceBand TheTarget (NOLOCK)
--	ON TheTarget.PlaybookPricingGroupKey = flpac.RPBPlaybookPricingGroupKey
--	AND flpac.Total12MonthQuantity BETWEEN TheTarget.LowerBandQuantity AND TheTarget.UpperBandQuantity 
--	AND TheTarget.MemberRank = 
--		CASE
--			WHEN  ROUND ( TheTarget.RulingMemberCount * TargetPercentile, 0) BETWEEN 1 AND TheTarget.RulingMemberCount THEN  ROUND ( TheTarget.RulingMemberCount * TargetPercentile, 0)
--			ELSE 1
--		END
--LEFT JOIN dbo.PlaybookPricingGroupPriceBand TheStretch (NOLOCK)
--	ON TheStretch.PlaybookPricingGroupKey = flpac.RPBPlaybookPricingGroupKey
--	AND flpac.Total12MonthQuantity BETWEEN TheStretch.LowerBandQuantity AND TheStretch.UpperBandQuantity 
--	AND TheStretch.MemberRank = 
--		CASE
--			WHEN  ROUND ( TheStretch.RulingMemberCount * StretchPercentile, 0) BETWEEN 1 AND TheStretch.RulingMemberCount THEN  ROUND ( TheStretch.RulingMemberCount * StretchPercentile, 0)
--			ELSE 1
--		END



--LEFT JOIN dbo.PlaybookPricingGroupPriceBand TheFloorDiscount (NOLOCK)
--	ON TheFloorDiscount.PlaybookPricingGroupKey = flpac.RPBPlaybookPricingGroupKey
--	AND flpac.Total12MonthQuantity BETWEEN TheFloorDiscount.LowerBandQuantity AND TheFloorDiscount.UpperBandQuantity 
--	AND TheFloorDiscount.MemberRankDiscount = 
--		CASE
--			WHEN  ROUND ( TheFloorDiscount.RulingMemberCount * FloorPercentile, 0) BETWEEN 1 AND TheFloorDiscount.RulingMemberCount THEN  ROUND ( TheFloorDiscount.RulingMemberCount * FloorPercentile, 0)
--			ELSE 1
--		END
--LEFT JOIN dbo.PlaybookPricingGroupPriceBand TheTargetDiscount (NOLOCK)
--	ON TheTargetDiscount.PlaybookPricingGroupKey = flpac.RPBPlaybookPricingGroupKey
--	AND flpac.Total12MonthQuantity BETWEEN TheTargetDiscount.LowerBandQuantity AND TheTargetDiscount.UpperBandQuantity 
--	AND TheTargetDiscount.MemberRankDiscount = 
--		CASE
--			WHEN  ROUND ( TheTargetDiscount.RulingMemberCount * TargetPercentile, 0) BETWEEN 1 AND TheTargetDiscount.RulingMemberCount THEN  ROUND ( TheTargetDiscount.RulingMemberCount * TargetPercentile, 0)
--			ELSE 1
--		END
--LEFT JOIN dbo.PlaybookPricingGroupPriceBand TheStretchDiscount (NOLOCK)
--	ON TheStretchDiscount.PlaybookPricingGroupKey = flpac.RPBPlaybookPricingGroupKey
--	AND flpac.Total12MonthQuantity BETWEEN TheStretchDiscount.LowerBandQuantity AND TheStretchDiscount.UpperBandQuantity 
--	AND TheStretchDiscount.MemberRankDiscount = 
--		CASE
--			WHEN  ROUND ( TheStretchDiscount.RulingMemberCount * StretchPercentile, 0) BETWEEN 1 AND TheStretchDiscount.RulingMemberCount THEN  ROUND ( TheStretchDiscount.RulingMemberCount * StretchPercentile, 0)
--			ELSE 1
--		END
		






GO
