SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

	
CREATE VIEW [cm].[vw_SupplierReference]

AS

WITH MaxSCR AS
	(SELECT ContractKey, MAX(ModifiedOnDT) as MaxDate
	FROM cc.SupplierContractReference
	WHERE GETDATE() BETWEEN StartDate AND ExpirationDate 
	GROUP BY ContractKey)


	SELECT scr.ContractKey, m.MaxDate, MIN(scr.ContractReferenceKey) AS ContractReferenceKey
	FROM cc.SupplierContractReference scr
	INNER JOIN MaxSCR m
		ON scr.ContractKey = m.ContractKey 
		AND scr.ModifiedOnDT = m.MaxDate
	GROUP BY scr.ContractKey, m.MaxDate
	

GO
