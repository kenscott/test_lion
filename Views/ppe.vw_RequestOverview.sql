SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE VIEW [ppe].[vw_RequestOverview] AS

	SELECT NEWID() AS GUID, ViewerWebUserKey, StatusKey, StatusName, SUM(RequestCount) AS RequestCount
	FROM (

		SELECT wd.DelegateWebUserKey AS ViewerWebUserKey, -1 AS StatusKey, 'My Pending' AS StatusName, COUNT(DISTINCT r.RequestKey) AS RequestCount
		FROM [ppe].[RequestMain] r
		JOIN ppe.RequestStatus rs ON rs.StatusKey = r.StatusKey
		JOIN dbo.WebUserDelegate wd
			ON wd.DelegatorWebUserKey = r.OwnerWebUserKey --wu3.WebUserKey   -- this connects the delegators accounts
			AND GETDATE() BETWEEN wd.StartDate AND ISNULL(wd.EndDate,'12-31-9999')
		WHERE rs.StatusRollup = 'Pending'
		GROUP BY wd.DelegateWebUserKey

		UNION

		SELECT WebUserKey AS ViewerWebUserKey, -1 AS StatusKey, 'My Pending' AS StatusName, 0 AS RequestCount
		FROM dbo.WebUser

		UNION

		SELECT wuv.ViewerWebUserKey, rm.StatusKey, rs.StatusName, COUNT(DISTINCT rm.RequestKey) RequestCount
		FROM ppe.RequestMain rm 
		INNER JOIN ppe.RequestStatus rs ON rs.StatusKey = rm.StatusKey
		INNER JOIN dbo.DimAccount da ON da.AccountKey = rm.AccountKey
		INNER JOIN dbo.DimItemGroup3 dig3 ON dig3.ItemGroup3Key = rm.ItemGroup3Key--LPF-2650
		INNER JOIN dbo.DimBrand db ON db.Brand = dig3.IG3Level5 --LPF-2650
		LEFT JOIN dbo.DimAccountBrandManager dabm ON dabm.AccountKey = rm.AccountKey AND dabm.BrandKey = db.BrandKey  --LPF-2650
		INNER JOIN dbo.WebUser wu ON wu.AccountManagerKey = ISNULL(dabm.AccountManagerKey,da.AccountManagerKey) --changed back per LIO-1064 -- changed per LIO-1046 and email thread. 7/31/2015
		INNER JOIN dbo.WebUserDelegate wd
			ON wd.DelegatorWebUserKey = wu.WebUserKey --rm.OwnerWebUserKey   -- this connects the delegators accounts
			AND GETDATE() BETWEEN wd.StartDate AND ISNULL(wd.EndDate,'12-31-9999')
		INNER JOIN dbo.WebUserView wuv ON wuv.ViewedWebUserKey = wd.DelegateWebUserKey
		--WHERE wuv.ViewerWebUserKey = 1
		GROUP BY wuv.ViewerWebUserKey, rm.StatusKey, rs.StatusName

		UNION

		SELECT WebUserKey, StatusKey, StatusName, 0
		FROM ppe.RequestStatus ,
		dbo.WebUser
	) a
	GROUP BY ViewerWebUserKey, StatusKey, StatusName





GO
