SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO









CREATE 
VIEW [lion].[vwPGDatapointReview]

AS

/*

EXEC dba_findstring 'vwPGDatapointReview'
EXEC dba_findstring 'vwBandDatapointReview'
SELECT TOP 10 * FROM [lion].[vwBandDatapointReview]

SELECT TOP 0 * FROM [lion].[vwBandDatapointReview]

SELECT COUNT(*) FROM [lion].[vwBandDatapointReview]

*/


SELECT
	pdp.AccountKey,
	pdp.ItemKey,
	pdp.InvoiceLineGroup1Key,

	dag1.AG1Level1 AS Branch,
	dag1.AG1Level2 AS Network,
	dag1.AG1Level3 AS Area,
	dag1.AG1Level4 AS Region,

	dag1.AG1Level1UDVarchar1 AS BranchName,
	dag1.AG1Level2UDVarchar1 AS NetworkDescription,
	dag1.AG1Level3UDVarchar1 AS AreaDescription,
	dag1.AG1Level4UDVarchar1 AS RegionDescription,

	ISNULL(pbattris.PlaybookRegion, dag1.AG1Level1UDVarchar2) AS PlaybookRegion,
	dag1.AG1Level1UDVarchar3 AS PlaybookRegionDescription,
	dag1.AG1Level1UDVarchar4 AS PlaybookArea,
	dag1.AG1Level1UDVarchar5 AS PlaybookAreaDescription,
	dag1.AG1Level1UDVarchar20 AS PlaybookBrandIndicator,
	dag1.AG1Level1UDVarchar21 AS BrandGroup,
	dag1.AG1Level1UDVarchar22 AS PGActiveIndicator,

	dag1.AG1Level1UDVarchar6 AS BranchPrimaryBrand,
	dag1.AG1Level1UDVarchar7 AS BranchTradingStatus,
	dag1.AG1Level1UDVarchar8 AS BranchType,
	dag1.AG1Level1UDVarchar9 AS BranchAddressLine1,
	dag1.AG1Level1UDVarchar10 AS BranchAddressLine2,
	dag1.AG1Level1UDVarchar11 AS BranchAddressLine3,
	dag1.AG1Level1UDVarchar12 AS BranchAddressLine4,
	dag1.AG1Level1UDVarchar13 AS BranchAddressLine5,
	dag1.AG1Level1UDVarchar14 AS BranchPostcode,
	dag1.AG1Level1UDVarchar15 AS BranchEmail,
	dag1.AG1Level1UDVarchar16 AS BranchTelephone,

	AG1Level1UDVarchar17 AS CustomerBranchPostCodeArea,
	AG1Level1UDVarchar18 AS CustomerBranchPostCodeDistrict,
	AG1Level1UDVarchar19 AS CustomerBranchPyramidCode,
	ISNULL(pbattris.RegionExceptionIndicator, AG1Level1UDVarchar25) AS RegionExceptionIndicator,

	--AG2Level1 AS CustomerType,
	--AG2Level1UDVarchar1 AS CustomerTypeDescription,

	--AG3Level1 AS TradingStatusCategory,
	--AG3Level1UDVarchar1 AS TradingStatusCategoryDescription,

	--dam.AccountManagerCode,
	dp.FullName AS AccountManagerFullName,
	--dp.Email AS AccountManagerEmail,
	
	da.AccountClientUniqueIdentifier,
	da.AccountNumber,
	da.AccountName,
	da.AccountNumberParent,
	da.AccountAddress1,
	da.AccountAddress2,
	da.AccountAddress3,
	da.AccountAddress4,
	da.AccountZipCode AS AccountPostCode,
	
	--da.AccountMemo AS CustomerType,
	da.AccountUDVarChar1 AS CreditLimit,
	da.AccountUDVarChar2 AS PaymentTermName,
	da.AccountUDVarChar3 AS IntercompanyAccount,
	da.AccountUDVarChar4 AS OverLimitStatus,
	da.AccountUDVarChar5 AS OverdueStatus,
	da.AccountUDVarChar6 AS TermsParent,
	da.AccountUDVarChar7 AS PricingCategory,
	da.AccountUDVarChar8 AS AccountOwnerType,
	da.AccountUDVarChar9 AS AccountOwnerBucket,
	da.AccountUDVarChar10 AS CustomerSegment,

	da.AccountUDDateTime1 AS DateOpened,
	da.AccountUDDateTime2 AS DateClosed,
	ISNULL(pbattris.CustomerSpecialism, da.CustomerSpecialism) AS CustomerSpecialism,

	--VendorKey AS VendorKey,
	--VendorDescription,
	--VendorNumber,

	di.ItemNumber,
	di.ItemDescription,
	--di.BaseUnitOfMeasure, 
	di.VendorStockNumber,

	di.ItemUDVarChar1 AS ItemPyramidCode,
	di.ItemUDVarChar2 AS ProdBrandAtLLSPG,
	di.ItemUDVarChar3 AS MPGCode,
	di.ItemUDVarChar4 AS MPGDescription,
	di.ItemUDVarChar5 AS SuperCategory,
	di.ItemUDVarChar6 AS Category,
	di.ItemUDVarChar7 AS SubCategory,
	di.ItemUDVarChar8 AS ItemObsolete,
	di.ItemUDVarChar9 AS HireInd,
	di.ItemUDVarChar10 AS SpecialInd,
	di.ItemUDVarChar11 AS ProductLifecycle,
	di.ItemUDVarChar12 AS OwnBrandInd,
	di.ItemUDVarChar13 AS KnownValueInd,
	di.ItemUDVarChar14 AS NDPInd,
	di.ItemUDVarChar15 AS MultiSourcedProductInd,
	di.ItemUDVarChar16 AS VariablePackInd,
	di.ItemUDVarChar17 AS ZonedInd,
	di.ItemUDVarChar18 AS UnitCode,
	di.ItemUDVarChar19 AS GenericProductID,
	ISNULL(pbattris.PlaybookProductBrand, di.ItemUDVarChar21) AS PlaybookProductBrand,
	di.ItemUDDate1 AS ItemDateForDeletion,
	
	--dig2.IG2Level1 AS ProdBrandAtLLSPG,
	--dig2.IG2Level1UDVarchar1 AS ProductTypeDescription,
	
	ISNULL(pbattris.LLSPG, dig3.IG3Level1) AS LLSPG,
	dig3.IG3Level1 AS LLSPGCode,--1
	dig3.IG3Level2 AS HLSPGCode,--2
	dig3.IG3Level3 AS GSPGCode,--3
	--dig3.IG3Level4 AS PYRCode,--4
	dig3.IG3Level1UDVarchar1 AS LLSPGDescription,--1
	dig3.IG3Level2UDVarchar1 AS HLSPGDescription,--2
	dig3.IG3Level3UDVarchar1 AS GSPGDescription, --3

	dilg1.InvoiceLineGroup1UDVarchar1 AS ShipmentType,
	ISNULL(pbattris.ContractClaimsIndicator, dilg1.InvoiceLineGroup1UDVarchar2) AS ContractClaimsIndicator,  -- formally DeviatedIndicator

	ISNULL(pbattris.StrategicItem, flpac.UDVarchar1) AS StrategicItem,
	flpac.UDVarchar2 AS SubLLSPG,
	flpac.UDVarchar3 AS NewItemForAccountIndicator,
	ISNULL(pbattris.Sales603010Bucket, flpac.UDVarchar4) AS Sales603010Bucket,
	flpac.UDVarchar5 AS TotalSalesAccountBucket,
	flpac.UDVarchar6 AS FrequencyBucket,
	flpac.UDVarchar7 AS CustBaseBucket,
	flpac.UDVarchar8 AS FreqPurchBucket,
	flpac.UDVarchar9 AS TopSellerBucket,
	ISNULL(pbattris.PriceApproach, flpac.UDVarchar10) AS PriceApproach,
	flpac.CustomerBrandSegment AS CustomerBrandSegment,
	--flpac.UDVarchar10 AS '???',
	
	flpac.UDDecimal1 AS TotalAccountSales,
	flpac.UDDecimal2 AS TotalNumberOfItemsSoldByAccount,
	flpac.UDDecimal3 AS TotalAccountGP,
	flpac.UDDecimal4 AS TotalNumberOfSuperCategoriesSold,
	ISNULL(flpac.UDDecimal5, 0.) AS LastUnitListPrice,
	ISNULL(flpac.UDDecimal6, 0.) AS LastUnitRetail,

	--InvoiceLineGroup2UDVarchar1 AS LastPriceDerivationBucket,

	pdp.PlaybookDataPointKey,
	pdp.PlaybookDataPointGroupKey,
	pdp.Total12MonthQuantity,
	pdp.TotalActual12MonthPrice,
	pdp.TotalActual12MonthCost,
	ISNULL(pdp.TotalActual12MonthPrice - pdp.TotalActual12MonthCost, 0.0) AS TotalActual12MonthGP,
	ISNULL((pdp.TotalActual12MonthPrice - pdp.TotalActual12MonthCost) / NULLIF(pdp.TotalActual12MonthPrice, 0.0), 0.0) AS TotalActual12MonthGPP,

	pdp.LastItemPrice,
	pdp.LastItemCost,
	pdp.LastItemDiscountPercent,
	(pdp.LastItemPrice - pdp.LastItemCost) AS LastItemGP,
	(pdp.LastItemPrice - pdp.LastItemCost) / NULLIF(pdp.LastItemPrice, 0.0) AS LastItemGPP,
	CONVERT (VARCHAR(10), dd.SQLDate, 120) AS LastInvoiceDate,

	pdp.AccountItemBandRank AS AccountItemBandRank__AB,	-- __Weighted  *** ohown in core app; AccountItemBandRank__AB
	pdp.FullScopeAccountTotalSalesBand AS FullScopeAccountTotalSalesBand__CustomerBand, 	-- AB.SalesBand driven from sales ranking set in the playbook (ATKSegmentCustomer)

	pdp.CoreNonCoreIndicator,

	pdppg.PlaybookPricingGroupKey,
	
	ppg.GroupingColumns, 
	ppg.GroupValues,

	pdppg.RulingMemberFlag,
	apr.PricingRuleSequenceNumber,
	apr.PricingRuleDescription,
	Scenario.ScenarioName,

	ppgqb.PlaybookPricingGroupQuantityBandKey,
	ppgqb.LowerBandQuantity,
	ppgqb.UpperBandQuantity,
	ppgqb.BandSequenceNumber,
	ppgqb.RulingMemberCount, 
	ppgqb.NonRulingMemberCount,

	a.FloorGPP,
	a.TargetGPP,	
	a.StretchGPP

FROM PlaybookDataPoint pdp WITH (NOLOCK)
INNER JOIN dbo.PlaybookDataPointPricingGroup pdppg WITH (NOLOCK)
	ON pdp.PlaybookDataPointKey = pdppg.PlaybookDataPointKey
INNER JOIN dbo.PlaybookPricingGroup ppg WITH (NOLOCK)
	ON ppg.PlaybookPricingGroupKey = pdppg.PlaybookPricingGroupKey
INNER JOIN dbo.ATKPricingRule apr WITH (NOLOCK)
	ON ppg.PricingRuleKey = apr.PricingRuleKey
INNER JOIN dbo.ATKScenario Scenario (NOLOCK)
	ON Scenario.PricingRulePlaybookKey = apr.PricingRulePlaybookKey
INNER JOIN dbo.PlaybookPricingGroupQuantityBand ppgqb WITH (NOLOCK)
	ON pdp.Total12MonthQuantity BETWEEN ppgqb.LowerBandQuantity
								AND     ppgqb.UpperBandQuantity 	--Assigns each data point to a band
	AND pdppg.PlaybookPricingGroupKey = ppgqb.PlaybookPricingGroupKey
INNER JOIN dbo.DimAccount da WITH (NOLOCK)
	ON da.AccountKey = pdp.AccountKey
INNER JOIN dbo.DimAccountGroup1 dag1 WITH (NOLOCK)
	ON dag1.AccountGroup1Key = da.AccountGroup1Key
--INNER JOIN dbo.DimAccountGroup2 dag2 WITH (NOLOCK)
--	ON dag2.AccountGroup2Key = da.AccountGroup2Key
--INNER JOIN dbo.DimAccountGroup3 dag3 WITH (NOLOCK)
--	ON dag3.AccountGroup3Key = da.AccountGroup3Key
INNER JOIN dbo.DimAccountManager dam WITH (NOLOCK)
	ON dam.AccountManagerKey = da.AccountManagerKey
INNER JOIN dbo.DimPerson dp WITH (NOLOCK)
	ON dp.DimPersonKey = dam.DimPersonKey
INNER JOIN dbo.DimItem di WITH (NOLOCK)
	ON di.ItemKey = pdp.ItemKey
--INNER JOIN dbo.DimItemGroup1 dig1 WITH (NOLOCK)
--	ON dig1.ItemGroup1Key = di.ItemGroup1Key
--INNER JOIN dbo.DimItemGroup2 dig2 WITH (NOLOCK)
--	ON dig2.ItemGroup2Key = di.ItemGroup2Key
INNER JOIN dbo.DimItemGroup3 dig3 WITH (NOLOCK)
	ON dig3.ItemGroup3Key = di.ItemGroup3Key
--INNER JOIN dbo.DimVendor dv WITH (NOLOCK)
--	ON dv.VendorKey = di.ItemVendorKey
INNER JOIN dbo.DimInvoiceLineGroup1 dilg1 WITH (NOLOCK)
	ON dilg1.InvoiceLineGroup1Key = pdp.InvoiceLineGroup1Key

INNER JOIN dbo.DimDay dd WITH (NOLOCK)
	ON dd.DayKey = pdp.LastSaleDayKey

LEFT JOIN dbo.FactLastPriceAndCost flpac WITH (NOLOCK)
	ON flpac.AccountKey = da.AccountKey
	AND flpac.ItemKey = di.ItemKey
	AND flpac.InvoiceLineGroup1Key = dilg1.InvoiceLineGroup1Key

LEFT JOIN lion.RPBPriceBandAttributes pbattris WITH (NOLOCK)
	ON pbattris.PlaybookPricingGroupKey = pdppg.PlaybookPricingGroupKey

--LEFT JOIN dbo.DimInvoiceLineGroup2 (NOLOCK)
--	ON DimInvoiceLineGroup2.InvoiceLineGroup2Key = flpac.LastInvoiceLineGroup2Key

OUTER APPLY lion.fn_GetPercentile (
		di.ItemUDVarchar2,													-- product brand
		dag1.AG1Level4,														-- Region,
		dag1.AG1Level3,														-- Area,
		dag1.AG1Level2,														-- Network
		dag1.AG1Level1,														-- Branch,
		ISNULL(pbattris.Sales603010Bucket, flpac.UDVarchar4),				-- Sales603010Bucket
		ItemUDVarChar1,														-- ItemPyramidCode
		ISNULL(pbattris.LLSPG, IG3Level1)									-- LLSPGCode
	)
OUTER APPLY lion.fn_GetRPBPercentages (
	pdppg.PlaybookPricingGroupKey,
	flpac.Total12MonthQuantity,
	FloorPercentile,
	TargetPercentile,
	StretchPercentile) a

WHERE 
	--RulingMemberFlag = 'Y'
	--AND pdppg.LastItemPrice <> 0.0
	(pdp.PlaybookDataPointGroupKey = (
			SELECT 
				MAX(PlaybookDataPointGroupKey)
			FROM dbo.ATKScenario a WITH (NOLOCK)
			INNER JOIN dbo.ATKProjectSpecificationScenario c WITH (NOLOCK)
				ON c.ScenarioKey = a.ScenarioKey
			INNER JOIN dbo.PlaybookProject pp WITH (NOLOCK)
				ON pp.ProjectSpecificationKey = c.ProjectSpecificationKey
			INNER JOIN dbo.PlaybookDataPointGroup pdpg WITH (NOLOCK)
				ON pdpg.ProjectSpecificationScenarioKey = c.ProjectSpecificationScenarioKey
				AND pdpg.ProjectKey = pp.ProjectKey
			WHERE 
				pp.ProductionProjectIndicator = 'Y'
				AND a.ApproachTypeKey = 7
			)
	)





























GO
