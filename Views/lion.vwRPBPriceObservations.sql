SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO








CREATE
VIEW [lion].[vwRPBPriceObservations]
AS


SELECT
	pdppg.PlaybookPricingGroupKey,
	BandSequenceNumber,
	pdp.AccountKey,
	pdp.ItemKey,
	pdp.InvoiceLineGroup1Key,
	PricingRuleSequenceNumber,
		CASE 
		WHEN PricingRuleSequenceNumber < 2 THEN 
			dag1.AG1Level1UDVarChar2
		ELSE 
			'All'
	END AS PlaybookRegion,
	CASE 
		WHEN PricingRuleSequenceNumber < 3 THEN 
			CustomerBrandSegment
		ELSE 
			'All'
	END AS CustomerBrandSegment,
	CASE 
		WHEN PricingRuleSequenceNumber < 4 THEN 
			flpac.UDVarchar4
		ELSE 
			'All'
	END AS Sales603010Bucket,
	CASE 
		WHEN PricingRuleSequenceNumber < 5 THEN 
			flpac.UDVarchar1
		ELSE 
			'All'
	END AS StrategicItem,
	CASE 
		WHEN PricingRuleSequenceNumber < 6 THEN 
			flpac.UDVarchar10
		ELSE 
			'All'
	END AS PriceApproach,
	CASE 
		WHEN PricingRuleSequenceNumber < 7 THEN 
			di.ItemNumber + ' - ' + di.ItemDescription
		ELSE 
			'All'
	END AS Item,

	CASE 
		WHEN PricingRuleSequenceNumber < 8 THEN 
			IG3Level1 + ' - ' + IG3Level1UDVarchar1 
		ELSE 
			'All'
	END AS LLSPG,

	--CASE 
	--	WHEN PricingRuleSequenceNumber < 8 THEN 
	--		IG3Level2 + ' - ' + IG3Level2UDVarchar1
	--	ELSE 
	--		'All'
	--END AS HLSPG,
	--IG3Level3 + ' - ' + IG3Level3UDVarchar1 AS GSPG,

	InvoiceLineGroup1UDVarchar2 AS ContractClaimsIndicator,  -- formally DeviatedIndicator
	InvoiceLineGroup1UDVarchar1 AS ShipmentType,
	ItemUDVarchar21 AS PlaybookProductBrand,	
	--dag1.AG1Level1UDVarchar20 AS PlaybookBrandIndicator,
	AG1Level1UDVarchar25 AS RegionExceptionIndicator,
	ItemUDVarChar1 AS PyramidCode
FROM PlaybookDataPointPricingGroup pdppg
INNER JOIN dbo.ATKPricingRule apr
	ON pdppg.PricingRuleKey = apr.PricingRuleKey
INNER JOIN dbo.PlaybookDataPoint pdp
	ON pdp.PlaybookDataPointKey = pdppg.PlaybookDataPointKey
INNER JOIN dbo.PlaybookPricingGroupQuantityBand ppgqb
	ON ppgqb.PlaybookPricingGroupKey = pdppg.PlaybookPricingGroupKey
	AND pdppg.Total12MonthQuantity BETWEEN ppgqb.LowerBandQuantity
							       AND     ppgqb.UpperBandQuantity 	--gets the datapoints with their associated bands
INNER JOIN dbo.DimAccount da
	ON da.AccountKey = pdp.AccountKey
INNER JOIN dbo.DimItem di
	ON di.ItemKey = pdp.ItemKey
INNER JOIN dbo.DimItemGroup3 dig3 (NOLOCK)
	ON dig3.ItemGroup3Key = di.ItemGroup3Key
--INNER JOIN dbo.DimVendor dv
--	ON dv.VendorKey = di.ItemVendorKey
INNER JOIN dbo.DimInvoiceLineGroup1 dilg1
	ON dilg1.InvoiceLineGroup1Key = pdp.InvoiceLineGroup1Key
INNER JOIN dbo.DimAccountGroup1 dag1 
	ON dag1.AccountGroup1Key = da.AccountGroup1Key
INNER JOIN dbo.FactLastPriceAndCost flpac
	ON pdp.AccountKey = flpac.AccountKey
	AND pdp.ItemKey = flpac.ItemKey
	AND pdp.InvoiceLineGroup1Key = flpac.InvoiceLineGroup1Key
INNER JOIN dbo.DimInvoiceLineGroup2 (NOLOCK)
	ON DimInvoiceLineGroup2.InvoiceLineGroup2Key = flpac.LastInvoiceLineGroup2Key
WHERE 
	RulingMemberFlag = 'Y'
	--AND pdppg.PlaybookPricingGroupKey = 651225


--FROM dbo.PlaybookPricingGroup ppg
--INNER JOIN dbo.PlaybookDataPointPricingGroup pdppg
--	ON pdppg.PlaybookPricingGroupKey = ppg.PlaybookPricingGroupKey
--INNER JOIN dbo.PlaybookDataPoint pdp
--	ON pdp.PlaybookDataPointKey = pdppg.PlaybookDataPointKey
--INNER JOIN dbo.DimDay dd
--	ON dd.DayKey = pdp.LastSaleDayKey
--INNER JOIN dbo.DimAccount da
--	ON da.AccountKey = pdp.AccountKey
--INNER JOIN dbo.DimAccountGroup1 dag1
--	ON dag1.AccountGroup1Key = da.AccountGroup1Key
--INNER JOIN dbo.DimItem di
--	ON di.ItemKey = pdp.ItemKey
--INNER JOIN dbo.PlaybookPricingGroupQuantityBand ppgqb
--	ON ppgqb.PlaybookPricingGroupKey = ppg.PlaybookPricingGroupKey
--	AND pdppg.Total12MonthQuantity BETWEEN ppgqb.LowerBandQuantity
--							       AND     ppgqb.UpperBandQuantity 	--gets the datapoints with their associated bands








GO
