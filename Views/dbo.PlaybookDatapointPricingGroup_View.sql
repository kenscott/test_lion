SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





CREATE   view
[dbo].[PlaybookDatapointPricingGroup_View]
as
Select B.PlaybookDataPointGroupKey, 
	A.PlaybookDataPointPricingGroupKey,
	A.PlaybookDataPointKey,
	A.PlaybookPricingGroupKey,
	A.PricingRuleKey,
	A.RulingMemberFlag,
	A.Total12MonthQuantity,
	A.LastItemPrice,
	A.LastItemCost

From  PlaybookDatapointPricingGroup A
join PlaybookPricingGroup B on 
	A.PlaybookPricingGroupKey = B.PlaybookPricingGroupKey





GO
