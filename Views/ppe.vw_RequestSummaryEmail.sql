SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE VIEW [ppe].[vw_RequestSummaryEmail] AS 


SELECT 
	NEWID() AS GUID, 
	a.ViewerWebUserKey, 
	a.StatusKey, 
	a.StatusName, 
	SUM(a.RequestCount) AS RequestCount,
	op.Email, 
	op.Fullname, 
	ISNULL( wus.SettingValue,'P') AS SummaryEmailIndicator
FROM (

		SELECT ViewerWebUserKey, StatusKey, StatusName, RequestCount
		FROM ppe.vw_RequestOverview (NOLOCK)
		WHERE StatusKey <> -1
		
		UNION

		SELECT wd.Delegatewebuserkey AS ViewerWebUserKey, -1 AS StatusKey, rs.StatusRollup AS StatusName, COUNT(*) AS RequestCount
		FROM ppe.RequestMain rm (NOLOCK)
		INNER JOIN ppe.RequestStatus rs ON rs.StatusKey = rm.StatusKey
		--INNER JOIN dbo.DimAccount da ON da.AccountKey = rm.AccountKey 
		--INNER JOIN dbo.WebUser wu ON wu.AccountManagerKey = da.AccountManagerKey 
		INNER JOIN dbo.WebUserDelegate wd (NOLOCK)
			ON wd.DelegatorWebUserKey = rm.OwnerWebUserKey --per LIO-1086 to match My Pending in vw_RequestOverview
			-- wu.WebUserKey   -- this connects the delegators accounts
			AND GETDATE() BETWEEN wd.StartDate AND ISNULL(wd.EndDate,'12-31-9999')
		WHERE rs.StatusRollup = 'Pending'
		GROUP BY wd.Delegatewebuserkey, rs.StatusRollup
		
		UNION
		
		SELECT WebUserKey, -1, 'Pending', 0
		FROM dbo.WebUser

) a
JOIN dbo.WebUser wu 
	ON a.viewerwebuserkey = wu.WebUserKey
JOIN dbo.ODSPerson op 
	ON wu.ODSPersonKey = op.ODSPersonKey 
LEFT JOIN dbo.WebUserSetting wus 
	ON wus.WebUserKey = wu.WebUserKey
	AND wus.SettingName = 'SummaryEmailIndicator'

GROUP BY
	a.ViewerWebUserKey, 
	a.StatusKey, 
	a.StatusName, 
	op.Email, 
	op.Fullname, 
	ISNULL(wus.SettingValue,'P')


--WHERE ( ISNULL( wus.SettingValue,'P' ) = 'Y' ) OR   -- always send summary
--	  ( ISNULL( wus.SettingValue,'P' ) = 'P' AND RequestCount > 0 AND StatusKey = -1 )  -- only send summary when there are pending requests






GO
