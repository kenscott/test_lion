SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [ppe].[vw_AccountItemSPGImpact] AS  
	SELECT
		flpc.AccountKey, 
		flpc.ItemKey, 
		MAX(di2.ItemGroup3Key) ItemGroup3Key,
		MAX(di2.ItemUDDecimal1) AS TradePrice,
		SUM(flpc.Total12MonthSales) SalesAtInvoice, 
		SUM(flpc.Total12MonthQuantity) Qty,
		SUM(flpc.Total12MonthQuantity * di2.ItemUDDecimal1) SalesAtTrade,
		MAX(flpc.UDVarchar1) AS StratgicItemFlag,
		MAX(fil.UDVarchar1) AS LastPriceDerivationCode,
		MAX((fil.UnitListPrice - flpc.LastItemPrice) / NULLIF(fil.UnitListPrice,0)) AS LastItemDiscount,
		MAX(flpc.LastItemPrice) AS LastItemPrice,
		MAX((flpc.LastItemPrice - flpc.LastItemPrice) / NULLIF(flpc.LastItemPrice,0)) AS LastItemGPP,
		MAX(flpc.UDVarchar4) AS SalesSize,
		MAX(flpc.UDVarchar10) AS PriceApproach,
		MAX(ct.CustomerTermsKey) TermsKey,
		MAX(dd.SQLDate) AS LastSaleDayKey,
		MAX(1.0 - (alip.Max / NULLIF(di2.ItemUDDecimal1, 0))) AS ALPCD ,
		MAX(di2.ItemUDVarchar13) AS KVIIndicator,
		MAX(CASE WHEN aspg.AccountKey IS NULL THEN 'N' ELSE 'Y' END) AS InCustomerSPG
	FROM FactLastPriceAndCost flpc
	JOIN dbo.DimItem di2 ON di2.ItemKey = flpc.ItemKey
	JOIN dbo.DimItemGroup3 dig3 ON dig3.ItemGroup3Key = di2.ItemGroup3Key
	JOIN dbo.FactInvoiceLine fil ON flpc.LastInvoiceLineUniqueIdentifier = fil.InvoiceLineUniqueIdentifier
	JOIN dbo.DimDay dd ON dd.DayKey = flpc.LastSaleDayKey

	JOIN lion.vwFactInvoiceLineGroup1 g1 ON flpc.InvoiceLineGroup1Key = g1.InvoiceLineGroup1Key
	LEFT JOIN lion.CustomerTerms ct
		ON ct.AccountKey = flpc.AccountKey
		AND ct.ItemKey = flpc.ItemKey
		AND ct.ItemKey <> 1
	LEFT JOIN lion.AccountSPG aspg
		ON aspg.AccountKey = flpc.AccountKey
		AND (aspg.LLSPGCode = dig3.IG3Level1 OR aspg.LLSPGCode = 'ALL')
	CROSS APPLY lion.fn_GetGPP(flpc.LastItemPrice, flpc.LastItemCost) l
	CROSS APPLY lion.fn_GetPriceEx(di2.CurrentCost, l.GPP, 0.99, di2.ItemUDDecimal1) lip  -- Last item's price at same margin as last item
	CROSS APPLY lion.fn_Max(lip.Price, flpc.LastItemPrice) alip

	WHERE flpc.InvoiceLineGroup1Key = 4
	AND fil.UDVarchar1 NOT IN ('DC', 'OCD', 'OCL','DP') 
	AND flpc.Total12MonthQuantity > 0
	AND flpc.LastItemPrice > 0
	AND di2.ItemUDDecimal1 > 0
	AND di2.ItemUDVarchar1 = '1'
	AND g1.ShipmentType = 'S'
	AND g1.DeviatedIndicator = 'N'
	GROUP BY 
		flpc.AccountKey, 
		flpc.ItemKey
GO
