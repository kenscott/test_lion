SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




/*
	Author: James Dameron
	Date: 11/29/2016
	Jira Reference: LPF-3697
	Description:  This view flattens out the geographical locations.  When written, it was used to populate the locations
				  multi-selector in the Contract Manager module.


	Examples:
		select top 1000 * from cc.vw_LocationLookup where Location like '%Pipe%'

	Modifications:
		2017-01-10 kfk Modified to filter out blanks and "none" -- LPF-4099 CC-1128 User can select a reference of NONE in the Locations tab.
*/


CREATE VIEW [cc].[vw_LocationLookup]
AS

WITH DistinctLocations AS (

	SELECT DISTINCT 
		1 AS ContractLocationLevelKey,
		AccountGroup1Key AS LocationKey ,
		AG1Level1 AS Location,
		AG1Level1UDVarchar1 AS LocationName,
		'Branch' AS LocationLevelName
	FROM dbo.DimAccountGroup1
	WHERE Inactive = 0   --only show active Branches
		AND AccountGroup1Key <> 1   --Unknown

	UNION 

	SELECT DISTINCT 
		2 AS ContractLocationLevelKey,
		dn.NetworkKey AS LocationKey ,
		dn.NetworkId AS Location,
		dn.NetworkDescription AS LocationName,
		'Network' AS LocationLevelName
	FROM dbo.DimNetwork dn
	JOIN dbo.DimAccountGroup1 dag1 ON dag1.NetworkKey = dn.NetworkKey
	WHERE dn.NetworkKey <> 1 
		AND dag1.Inactive = 0  

	UNION 

	SELECT DISTINCT
		3 AS ContractLocationLevelKey,
		da.AreaKey AS LocationKey ,
		da.Area AS Location,
		da.AreaName AS LocationName,
		'Area' AS LocationLevelName
	FROM dbo.DimArea da
	JOIN dbo.DimAccountGroup1 dag1 ON dag1.AreaKey = da.AreaKey
	WHERE da.AreaKey <> 1  
		AND dag1.Inactive = 0  
 

	UNION 

	SELECT DISTINCT
		4 AS ContractLocationLevelKey,
		dr.RegionKey AS LocationKey ,
		dr.Region AS Location,
		dr.RegionName AS LocationName,
		'Region' AS LocationLevelName
	FROM dbo.DimRegion dr
	JOIN dbo.DimAccountGroup1 dag1 ON dag1.RegionKey = dr.RegionKey
	WHERE dr.RegionKey <> 1   --Unknown
		AND dag1.Inactive = 0  --Unknown

	UNION 

	SELECT DISTINCT
		5 AS ContractLocationLevelKey,
		db.BrandKey AS LocationKey ,
		db.Brand AS Location,
		db.Brand AS LocationName,
		'Brand' AS LocationLevelName
	FROM dbo.DimBrand db
	JOIN dbo.DimAccountGroup1 dag1 ON db.BrandKey = db.BrandKey
	WHERE db.BrandKey <> 1   --Unknown
		AND dag1.Inactive = 0  --Unknown
) 

SELECT NEWID() AS GUID,
	ContractLocationLevelKey,
	LocationKey,
	Location,
	LocationName,
	LocationLevelName
FROM DistinctLocations
WHERE
	Location IS NOT NULL
	AND Location <> ''
	AND Location <> 'NONE'	-- LPF-4099 CC-1128 User can select a reference of NONE in the Locations tab.



GO
