SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO









CREATE 
VIEW [lion].[vwBandDatapointReview]

AS

/*

SELECT TOP 10 * FROM [lion].[vwBandDatapointReview]

SELECT TOP 0 * FROM [lion].[vwBandDatapointReview]

SELECT COUNT(*) FROM [lion].[vwBandDatapointReview]

*/


SELECT
	pdp.AccountKey,
	pdp.ItemKey,
	pdp.InvoiceLineGroup1Key,

	Branch,
	Network,
	Area,
	Region,

	BranchName,
	NetworkDescription,
	AreaDescription,
	RegionDescription,

	PlaybookRegion,
	PlaybookRegionDescription,
	PlaybookArea,
	PlaybookAreaDescription,
	PlaybookBrandIndicator,
	BrandGroup,
	PGActiveIndicator,

	BranchPrimaryBrand,
	BranchTradingStatus,
	BranchType,
	BranchAddressLine1,
	BranchAddressLine2,
	BranchAddressLine3,
	BranchAddressLine4,
	BranchAddressLine5,
	BranchPostcode,
	BranchEmail,
	BranchTelephone,

	CustomerBranchPostCodeArea,
	CustomerBranchPostCodeDistrict,
	CustomerBranchPyramidCode,
	RegionExceptionIndicator,

	AG2Level1 AS CustomerType,
	AG2Level1UDVarchar1 AS CustomerTypeDescription,

	AG3Level1 AS TradingStatusCategory,
	AG3Level1UDVarchar1 AS TradingStatusCategoryDescription,

	-----dam.AccountManagerCode,
	AccountManagerFullName,
	-----dp.Email AS AccountManagerEmail,
	
	pdp.AccountClientUniqueIdentifier,
	pdp.AccountNumber,
	pdp.AccountName,
	pdp.AccountNumberParent,
	pdp.AccountAddress1,
	pdp.AccountAddress2,
	pdp.AccountAddress3,
	pdp.AccountAddress4,
	pdp.AccountPostCode,
	
	--pdp.CustomerType,
	pdp.CreditLimit,
	pdp.PaymentTermName,
	pdp.IntercompanyAccount,
	pdp.OverLimitStatus,
	pdp.OverdueStatus,
	pdp.TermsParent,
	pdp.PricingCategory,
	pdp.AccountOwnerType,
	pdp.AccountOwnerBucket,
	pdp.CustomerSegment,
	
	pdp.DateOpened,
	pdp.DateClosed,

	VendorKey AS VendorKey,
	VendorDescription,
	VendorNumber,

	pdp.ItemNumber,
	pdp.ItemDescription,
	--BaseUnitOfMeasure, 
	pdp.VendorStockNumber,
	
	pdp.ItemPyramidCode,
	pdp.ProdBrandAtLLSPG,
	pdp.MPGCode,
	pdp.MPGDescription,
	pdp.SuperCategory,
	pdp.Category,
	pdp.SubCategory,
	pdp.ItemObsolete,
	pdp.HireInd,
	pdp.SpecialInd,
	pdp.ProductLifecycle,
	pdp.OwnBrandInd,
	pdp.KnownValueInd,
	pdp.NDPInd,
	pdp.MultiSourcedProductInd,
	pdp.VariablePackInd,
	pdp.ZonedInd,
	pdp.UnitCode,
	pdp.GenericProductID,
	pdp.PlaybookProductBrand,
	pdp.ItemDateForDeletion,
	
	--IG2Level1 AS ProdBrandAtLLSPG,
	--IG2Level1UDVarchar1 AS ProductTypeDescription,
	
	pdp.LLSPGCode,--1
	pdp.HLSPGCode,--2
	pdp.GSPGCode,--3
	--pdp.PYRCode,--4
	pdp.LLSPGDescription,--1
	pdp.HLSPGDescription,--2
	pdp.GSPGDescription, --3

	ShipmentType,
	pdp.ContractClaimsIndicator AS DeviatedIndicator,

	StrategicItem,
	SubLLSPG,
	NewItemForAccountIndicator,
	pdp.Sales603010Bucket,
	TotalSalesAccountBucket,
	FrequencyBucket,
	CustBaseBucket,
	FreqPurchBucket,
	TopSellerBucket,
	PriceApproach,
	CustomerBrandSegment,
	
	TotalAccountSales,
	TotalNumberOfItemsSoldByAccount,
	TotalAccountGP,
	TotalNumberOfSuperCategoriesSold,
	--ISNULL(flpac.UDDecimal5, 0.) AS LastUnitListPrice,
	--ISNULL(flpac.UDDecimal6, 0.) AS LastUnitRetail,

	--InvoiceLineGroup2UDVarchar1 AS LastPriceDerivationBucket,

	PlaybookDataPointKey,
	PlaybookDataPointGroupKey,
	Total12MonthQuantity,
	TotalActual12MonthPrice,
	TotalActual12MonthCost,
	LastItemPrice,
	LastItemCost,
	LastItemDiscountPercent,

	pdp.AccountItemBandRank__AB,	-- __Weighted  *** ohown in core app; AccountItemBandRank__AB
	pdp.FullScopeAccountTotalSalesBand__CustomerBand, 	-- AB.SalesBand driven from sales ranking set in the playbook (ATKSegmentCustomer)

	CoreNonCoreIndicator,

	PlaybookPricingGroupKey,
	
	GroupingColumns, 
	GroupValues,

	RulingMemberFlag,
	PricingRuleSequenceNumber,

	PlaybookPricingGroupQuantityBandKey,
	LowerBandQuantity,
	UpperBandQuantity,
	BandSequenceNumber,
	RulingMemberCount, 
	NonRulingMemberCount

FROM lion.vwPGDatapointReview pdp
INNER JOIN dbo.DimAccount da
	ON da.AccountKey = pdp.AccountKey
INNER JOIN dbo.DimAccountGroup2 dag2
	ON dag2.AccountGroup2Key = da.AccountGroup2Key
INNER JOIN dbo.DimAccountGroup3 dag3
	ON dag3.AccountGroup3Key = da.AccountGroup3Key
INNER JOIN dbo.DimItem di
	ON di.ItemKey = pdp.ItemKey
INNER JOIN dbo.DimItemGroup1 dig1
	ON dig1.ItemGroup1Key = di.ItemGroup1Key
INNER JOIN dbo.DimItemGroup2 dig2
	ON dig2.ItemGroup2Key = di.ItemGroup2Key
INNER JOIN dbo.DimVendor dv
	ON dv.VendorKey = di.ItemVendorKey

WHERE 
	RulingMemberFlag = 'Y'
	AND pdp.LastItemPrice <> 0.0



GO
