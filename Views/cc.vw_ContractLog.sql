SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





CREATE VIEW [cc].[vw_ContractLog] AS
/*
	select * from cc.SupplierContractLog

	select count(*) from cc.SupplierContractLog
	select count(*) from cc.vw_ContractLog
*/

	SELECT scl.ContractLogKey,
		   scl.ContractKey,
		   scl.WebUserKey,
		   wu.UserName,
		   p.FullName,
		   sc.ContractID,
		   v.VendorNumber AS SupplierNumber,
		   v.VendorDescription AS SupplierName,
		   scl.ContractAction,
		   scl.Notes,
		   scl.CreationDate
		 
		   
		
		FROM cc.SupplierContractLog scl WITH (NOLOCK)
		LEFT OUTER JOIN cc.SupplierContract sc WITH (NOLOCK) ON sc.ContractKey = scl.ContractKey
		LEFT OUTER JOIN dbo.DimVendor v WITH (NOLOCK) ON v.VendorKey = sc.VendorKey
		LEFT OUTER JOIN dbo.WebUser wu WITH (NOLOCK) ON wu.WebUserKey = scl.WebUserKey
		LEFT OUTER JOIN dbo.ODSPerson p WITH (NOLOCK) ON p.ODSPersonKey = wu.ODSPersonKey
		




GO
