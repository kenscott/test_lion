SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE VIEW [lion].[vwItemGroup1] 

/*
SELECT TOP 10 * FROM lion.vwItemGroup1
*/

AS

SELECT
	dig1.[ItemGroup1Key],

	IG1Level1 AS MPGCode,
	IG1Level2 AS PyramidCode,
	IG1Level1UDVarchar1 AS MPGDescription
	
FROM dbo.DimItemGroup1 dig1 (NOLOCK)




GO
