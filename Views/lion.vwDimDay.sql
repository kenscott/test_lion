SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE VIEW [lion].[vwDimDay]
AS
SELECT        DayKey, MonthKey, SQLDate, CalendarDay, CalendarDayNumberinMonth, CalendarDayNumberinQuarter, CalendarDayNumberinWeek, CalendarDayNumberinYear, CalendarWeek, CalendarWeekEndingDate, 
                         CalendarWeekNumber, DayinWeek, DayNumber, FiscalDay, FiscalDayNumberinMonth, FiscalDayNumberinQuarter, FiscalDayNumberinWeek, FiscalDayNumberinYear, FiscalWeek, FiscalWeekEndingDate, 
                         FiscalWeekNumber, FiscalWeekNumberinYear, Holiday, HolidayIndicator, LastDayinCalendarMonthIndicator, LastDayinCalendarQuarterIndicator, LastDayinCalendarWeekIndicator, 
                         LastDayinCalendarYearIndicator, LastDayinFiscalMonthIndicator, LastDayinFiscalQuarterIndicator, LastDayinFiscalWeekIndicator, LongCalendarDay, MajorEvent, SellingSeason, WeekdayIndicator, 
                         CASE WHEN DayKey =
                             (SELECT        MAX(InvoiceDateDayKey)
                               FROM            lion.vwFactInvoiceLinePricebandScore) THEN 1 ELSE 0 END AS LatestDayIndicator,
						 LatestDay = 
                             (SELECT        MAX(SQLDate) as LatestDay
                               FROM            lion.vwFactInvoiceLinePricebandScore filpbs 
											   inner join dbo.DimDay dd on filpbs.InvoiceDateDayKey = dd.DayKey) 

FROM            dbo.DimDay


GO
