SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE 
VIEW [lion].[vwFactInvoiceLineGroup3] 

AS

/*
SELECT * FROM lion.vwFactInvoiceLineGroup3
*/

SELECT
	InvoiceLineGroup3Key,
	InvoiceLineGroup3UDVarchar1 AS ContractIndicator
FROM DimInvoiceLineGroup3 (NOLOCK)




GO
