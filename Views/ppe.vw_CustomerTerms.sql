SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


--Emily McDonough the [ppe].[vw_CustomerTerms] view needs to be changed to return an empty string '' for the
-- ProductCode and ProductDescription columns when the ItemKey = 1.

/*
	select top 10 * from ppe.vw_CustomerTerms
*/

CREATE VIEW [ppe].[vw_CustomerTerms] AS         
	SELECT
		NEWID() AS GUID,
		ct.OwningBrandCode,
		da.AccountNumber,
		da.AccountName,
		dp.FullName AS AccountOwnerFullName,
		dig3.IG3Level1 AS SPGCode,
		dig3.IG3Level1UDVarchar1 AS SPGDescription,
		dag1.AG1Level3 AS Area,
		dag1.AG1Level2 AS Network,
		dag1.AG1Level1 AS Branch,
		cd.Discount AS TermDiscount,		
		CAST(ct.Usualdiscount1 AS DEC(38,8)) / 100.0 AS SPGDiscount1,
		CAST(ct.Usualdiscount2 AS DEC(38,8)) / 100.0 AS SPGDiscount2,
		CASE 
        WHEN di.Itemkey = 1 THEN ''
        ELSE di.ItemUDVarchar20
        END AS ProductCode,
		CASE 
        WHEN di.Itemkey = 1 THEN ''
        ELSE di.ItemDescription 
        END AS ProductDescription,
		CAST(ct.ExceptionDiscount AS DEC(38,8)) / 100.0 AS ExceptionDiscount ,
		ct.ExceptionFixedPrice,
		CASE 
			WHEN ISDATE(ct.ExceptionFromDate) = 1 THEN  CAST(ct.ExceptionFromDate AS DATE) 
			ELSE NULL
		END AS ExceptionStartDate,
		CASE 
			WHEN ISDATE(ct.ExceptionToDate) = 1 THEN  CAST(ct.ExceptionToDate AS DATE) 
			ELSE NULL
		END AS ExceptionExpiryDate,
		da.AccountKey,
		di.ItemKey,
		ct.ItemGroup3Key,
		ISNULL(CASE ct.ItemKey WHEN 1 THEN s2.Sales ELSE s1.Sales END,0) AS LTMSales,
		dig3.IG3Level5 AS ProdBrandAtLLSPG,
		dam.AccountManagerKey as AccountOwnerKey
	FROM lion.CustomerTerms ct
	JOIN dbo.DimAccount da ON da.AccountKey = ct.AccountKey
	JOIN dbo.DimAccountGroup1 dag1 ON dag1.AccountGroup1Key = da.AccountGroup1Key
	JOIN dbo.DimItemGroup3 dig3 ON dig3.ItemGroup3Key = ct.ItemGroup3Key
	JOIN dbo.DimItem di ON di.ItemKey = ct.ItemKey
	CROSS APPLY ppe.fn_GetCompoundDiscount(CAST(ct.Usualdiscount1 AS DEC(38,8)) / 100.0, CAST(ct.Usualdiscount2 AS DEC(38,8)) / 100.0) cd
	JOIN dbo.DimAccountManager dam ON dam.AccountManagerKey = da.AccountManagerKey
	JOIN dbo.DimPerson dp ON dp.DimPersonKey = dam.DimPersonKey
	LEFT JOIN (SELECT AccountKey, ItemKey, SUM(Total12MonthSales) AS Sales
				FROM dbo.FactLastPriceAndCost
				GROUP BY AccountKey, ItemKey) s1
		ON s1.AccountKey = ct.AccountKey
		AND s1.ItemKey = ct.ItemKey
		AND ct.ItemKey <> 1
	LEFT JOIN (SELECT AccountKey, ItemGroup3Key, SUM(Total12MonthSales) AS Sales
				FROM dbo.FactLastPriceAndCost f
				JOIN dbo.DimItem di ON di.ItemKey = f.ItemKey
				GROUP BY AccountKey, ItemGroup3Key) s2
		ON s2.AccountKey = ct.AccountKey
		AND s2.ItemGroup3Key = ct.ItemGroup3Key
		AND ct.ItemKey = 1





GO
