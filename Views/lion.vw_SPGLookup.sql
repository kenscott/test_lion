SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




CREATE VIEW [lion].[vw_SPGLookup] AS
/*
	select top 5 * from dimitem
	select top 5 * from dbo.vw_SPGLookup where VendorKey = 1246 order by spgcode desc
	
	drop view dbo.vw_SPGLookup
*/

SELECT DISTINCT 
	di.ItemVendorKey AS VendorKey,
	dig3.ItemGroup3Key AS SPGKey,
	dig3.IG3Level1 AS SPGCode, 
	dig3.IG3Level1UDVarchar1 AS SPGDescription,
	di.ItemUDVarchar1 AS ItemPyramidCode 
	FROM DimItem di
	JOIN DimItemGroup3 dig3 
		ON dig3.ItemGroup3Key = di.ItemGroup3Key



GO
