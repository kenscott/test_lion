SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [cm].[vw_ApproachingLimitEmail]

AS


WITH Accounts AS (
SELECT e.ApproachingLimitEmailKey,
	da.AccountNumber,
	da.AccountName,
	COUNT(*) AS AccountCnt
FROM cm.ApproachingLimitEmail e
LEFT JOIN cc.SupplierContractItem sci
	ON sci.ContractItemKey = e.ContractItemKey
LEFT JOIN cc.SupplierContractItemGroupX scig
	ON scig.ContractItemGroupXKey = e.ContractItemGroupXKey
LEFT JOIN cc.SupplierContract sc
	ON sc.ContractKey = ISNULL(sci.ContractKey, scig.ContractKey)
LEFT JOIN cc.SupplierContractAccount sca
	ON sca.ContractKey = sc.ContractKey
LEFT JOIN dbo.DimAccount da
	ON da.AccountKey = sca.AccountKey
GROUP BY e.ApproachingLimitEmailKey,
	da.AccountNumber,
	da.AccountName
HAVING COUNT(*) > 1
	)

SELECT
	e.[ApproachingLimitEmailKey],
	sci.ContractItemKey,
	scig.ContractItemGroupXKey,
	sc.ContactName,
	sc.ContactEmail,
	sc.ContractID,
	dv.VendorDescription AS SupplierName,
	ISNULL(di.ItemUDVarChar20, ISNULL(dig1.IG1Level1, dig3.IG3Level1)) AS ProductOrGroupCode,
	CASE
		WHEN a.AccountNumber IS NULL THEN 'VARIOUS'
		ELSE a.AccountNumber + ' ' + a.AccountName 
	END AS Account,
	CASE
		WHEN ISNULL(sci.LimitType, scig.LimitType) = 'U' THEN 'Unit'
		WHEN ISNULL(sci.LimitType, scig.LimitType) = 'S' THEN 'Sales'
		WHEN ISNULL(sci.LimitType, scig.LimitType) = 'C' THEN 'Claim'
		ELSE NULL
	END AS LimitType,
	ISNULL(sci.LimitValue, scig.LimitValue) AS LimitValue,
	ISNULL(sci.CurrentMonthTally, scig.CurrentMonthTally) AS CurrentMonthTally,
	ISNULL(sci.LimitCurrentRatio, scig.LimitCurrentRatio) AS LimitCurrentRatio

FROM cm.ApproachingLimitEmail e
LEFT JOIN cc.SupplierContractItem sci
	ON sci.ContractItemKey = e.ContractItemKey
LEFT JOIN cc.SupplierContractItemGroupX scig
	ON scig.ContractItemGroupXKey = e.ContractItemGroupXKey
LEFT JOIN cc.SupplierContract sc
	ON sc.ContractKey = ISNULL(sci.ContractKey, scig.ContractKey)
LEFT JOIN dbo.DimVendor dv
	ON dv.VendorKey = sc.VendorKey
LEFT JOIN dbo.DimItem di
	ON di.ItemKey = sci.ItemKey
LEFT JOIN dbo.DimItemGroup1 dig1
	ON dig1.ItemGroup1Key = scig.ItemGroupXKey
	AND scig.ItemGroupLevel = 1
LEFT JOIN dbo.DimItemGroup3 dig3
	ON dig3.ItemGroup3Key = scig.ItemGroupXKey
	AND scig.ItemGroupLevel = 3
LEFT JOIN Accounts a
	ON a.ApproachingLimitEmailKey = e.ApproachingLimitEmailKey


GO
