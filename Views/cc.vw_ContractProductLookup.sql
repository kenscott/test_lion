SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO













CREATE VIEW [cc].[vw_ContractProductLookup] AS
/*
	select top 55 * from dimitem
	select top 5 * from cc.vw_ContractProductLookup
*/

	SELECT 
		di.ItemKey,
		di.ItemUDVarchar20 AS ProductCode,
		di.ItemDescription,
		
		di.VendorStockNumber AS SupplierCatalogNumber,

		di.CurrentInvoiceCost,
		di.ItemVendorKey AS VendorKey,
		ItemUDVarChar1 AS ItemPyramidCode,

		IG1Level1 AS MPGCode,
		IG1Level1UDVarchar1 AS MPGDescription,

		IG3Level1 AS LLSPGCode,
		IG3Level1UDVarchar1 AS LLSPGDescription

	FROM dbo.DimItem di (NOLOCK)
	INNER JOIN dbo.DimItemGroup3 dig3 (NOLOCK) 
		ON dig3.ItemGroup3Key = di.ItemGroup3Key
	INNER JOIN dbo.DimItemGroup1 dig1 (NOLOCK) 
		ON dig1.ItemGroup1Key = di.ItemGroup1Key

	WHERE di.InActive = 0  --only show active products














GO
