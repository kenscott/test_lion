SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [cc].[vw_LocationHierarchy]

AS

SELECT
	AG1Level1 AS BranchId,
	AG1Level1 + ' - ' + AG1Level1UDVarchar1 AS Branch,
	dn.NetworkId AS NetworkId,
	dn.NetworkId + ' - ' + dn.NetworkDescription AS Network,
	da.Area AS AreaId,
	da.Area + ' - ' + da.AreaName AS Area,
	dr.Region AS RegionId,
	dr.Region + ' - ' + dr.RegionName AS Region,
	db.Brand AS BrandId,
	db.Brand + ' - ' + db.SegmentBrand AS Brand

FROM DimAccountGroup1 dag1
INNER JOIN DimNetwork dn
	ON dn.NetworkKey = dag1.NetworkKey
INNER JOIN DimArea da
	ON da.AreaKey = dag1.AreaKey
INNER JOIN DimRegion dr
	ON dr.RegionKey = dag1.RegionKey
INNER JOIN DimBrand db
	ON db.BrandKey = dag1.BrandKey

WHERE dag1.Inactive = 0
	AND AccountGroup1Key <> 1   --Unknown
	
GO
