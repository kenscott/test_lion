SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO












CREATE VIEW [cc].[vw_ContractAccount] AS
/*
	select top 50 * from cc.SupplierContract order by contractkey desc
	
	select * from cc.vw_ContractAccount where contractkey = 152874
*/

	SELECT 
		sca.ContractAccountKey, 
		ContractKey,
		da.AccountKey,
		da.AccountNumber,
		da.AccountName,
		da.AccountNumberParent,
		sca.StartDate,
		sca.ExpirationDate,
		--AG1Level1 AS BranchCode,
		--AG1Level2 AS Network,
		--AG1Level3 AS Area,
		--AG1Level4 AS Region,
		--AG1Level1UDVarchar6 AS Brand,
		--AG1Level1UDVarchar1 AS BranchName,
		--AG1Level2UDVarchar1 AS NetworkDescription,
		--AG1Level3UDVarchar1 AS AreaDescription,
		--AG1Level4UDVarchar1 AS RegionDescription,
		--AG1Level1UDVarchar6 AS BrandDescription,
		CASE  -- LPF-2069
			WHEN da.AccountNumberParent <> da.AccountNumber THEN 'C'  -- if numbers are different, I am a child
			WHEN (SELECT COUNT(*) FROM dbo.DimAccount da2 WHERE da2.AccountNumberParent = da.AccountNumber ) > 1  THEN 'P' -- if anyone else claims me as parent, i am a parent
			ELSE '' -- else neither
		END AS ParentChild ,
		wu1.UserName AS CreatedByUserName,
		sca.CreatedOnDT,
		wu2.UserName AS ModifiedByUserName,
		sca.ModifiedOnDT
	FROM cc.SupplierContractAccount sca 
	JOIN DimAccount da ON sca.AccountKey = da.AccountKey
	LEFT JOIN dbo.WebUser wu1 ON wu1.WebUserKey = sca.CreatedByWebUserKey
	LEFT JOIN dbo.WebUser wu2 ON wu2.WebUserKey = sca.ModifiedByWebUserKey
	--JOIN DimAccountGroup1 dag1 ON dag1.AccountGroup1Key = da.AccountGroup1Key
	
	--AND sca.AccountKey <> 1
	--AND da.Inactive = 0 inactive/closed now included per LPF-2299 -- exclude closed accounts LPF-2072

	-- below removed per LPF-2485
	--UNION

	--SELECT 
	--	NEWID() AS GUID, 
	--	ContractKey,
	--	da.AccountKey,
	--	da.AccountNumber,
	--	da.AccountName,
	--	da.AccountNumberParent,
	--	sca.StartDate,
	--	sca.ExpirationDate,
	--	--AG1Level1 AS BranchCode,
	--	--AG1Level2 AS Network,
	--	--AG1Level3 AS Area,
	--	--AG1Level4 AS Region,
	--	--AG1Level1UDVarchar6 AS Brand,
	--	--AG1Level1UDVarchar1 AS BranchName,
	--	--AG1Level2UDVarchar1 AS NetworkDescription,
	--	--AG1Level3UDVarchar1 AS AreaDescription,
	--	--AG1Level4UDVarchar1 AS RegionDescription,
	--	--AG1Level1UDVarchar6 AS BrandDescription,
	--	CASE  -- LPF-2069
	--		WHEN da.AccountNumberParent <> da.AccountNumber THEN 'C'  -- if numbers are different, I am a child
	--		WHEN (SELECT COUNT(*) FROM dbo.DimAccount da2 WHERE da2.AccountNumberParent = da.AccountNumber ) > 1  THEN 'P' -- if anyone else claims me as parent, i am a parent
	--		ELSE '' -- else neither
	--	END AS ParentChild 
	--FROM cc.SupplierContractAccount dca,
	--DimAccount da 
	----JOIN DimAccountGroup1 dag1 ON dag1.AccountGroup1Key = da.AccountGroup1Key
	--WHERE sca.AccountKey = 1
	--AND da.AccountKey <> 1
	----AND da.Inactive = 0 inactive/closed now included per LPF-2299  -- exclude closed accounts LPF-2072















GO
