SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





CREATE   view
[dbo].[PlaybookDatapointOptimalPrice_View]
as
Select B.PlaybookDataPointGroupKey, 
A.PlaybookDataPointOptimalPriceKey,
A.PlaybookPricingGroupOptimalPriceKey,
A.PlaybookDataPointPricingGroupKey,
A.OptimalPrice,
A.ThresholdFrequency,
A.ThresholdTotalPrice,
A.OptimalCost
--		B.*,
--		C.*,
--		D.*
From  PlaybookDatapointOptimalPrice A
join PlaybookDataPointPricingGroup_View B on A.PlaybookDataPointPricingGroupKey = B.PlaybookDataPointPricingGroupKey
--join PlaybookPricingGroupOptimalPrice C on C.PlaybookPricingGroupOptimalPriceKey = A.PlaybookPricingGroupOptimalPriceKey
--join PlaybookPricingGroupQuantityBand D on D.PlaybookPricingGroupQuantityBandKey = C.PlaybookPricingGroupQuantityBandKey
--join PlaybookPricingGroup E on E.PlaybookPricingGroupKey = D.PlaybookPricingGroupKey




GO
