SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [ppe].[vw_CustomerTermsRequest] AS

	SELECT
		NEWID() AS GUID,
		NULL AS RequestKey, -- (nonnull if this is a request otherwise it should be null)
		dig3.IG3Level1 AS SPGCode,
		dig3.IG3Level1UDVarchar1 AS SPGDescription,
		CASE WHEN di.ItemKey = 1 THEN '' ELSE di.ItemUDVarchar20 END AS ProductCode,
		di.ItemDescription AS ProductDescription,
		ct.Usualdiscount1 / 100.0 AS SPGDiscount1,
		ct.Usualdiscount2 / 100.0 AS SPGDiscount2,
		NULL AS NewSPGFromDate,
		rd.Discount AS RedDiscount,
		ad.Discount AS AmberDiscount,
		gd.Discount AS GreenDiscount,
		p.RedPrice, --(used for product exception)
		p.GreenPrice, --(used for product exception)
		p.AmberPrice, --(used for product exception)
		NULL AS NewDeleteTerm,
		ct.ExceptionDiscount / 100.0  AS ExceptionDiscount,
		ct.ExceptionFixedPrice,
		CASE 
			WHEN ISDATE(ct.ExceptionFromDate) = 1 THEN  CAST(ct.ExceptionFromDate AS DATE) 
			ELSE NULL
		END AS ExceptionStartDate,
		CASE 
			WHEN ISDATE(ct.ExceptionToDate) = 1 THEN  CAST(ct.ExceptionToDate AS DATE) 
			ELSE NULL
		END AS ExceptionExpiryDate,
		NULL AS AcceptedImpact,
		CASE WHEN ct.ExceptionFixedPrice IS NOT NULL THEN 'FP' ELSE 'TD' END NewTermType,
		da.AccountNumber,
		da.AccountName,
		da.AccountKey,
		ct.ItemGroup3Key,
		ct.ItemKey,
		CASE WHEN da.AccountUDVarChar12 = 'N' THEN 0 ELSE 1 END FixedPriceFlag,
		1 AS HasTerm

	FROM lion.CustomerTerms ct
	LEFT JOIN FactRecommendedPriceBand frpb 
		 ON frpb.AccountKey = ct.AccountKey
		 AND frpb.ItemKey = ct.ItemKey
		 AND frpb.InvoiceLineGroup1Key = 4
	INNER JOIN dbo.DimItem di ON di.ItemKey = ct.ItemKey
	INNER JOIN dbo.DimAccount da ON da.AccountKey = ct.AccountKey
	LEFT JOIN dbo.DimItemGroup3 dig3 ON dig3.ItemGroup3Key = ct.ItemGroup3Key
	OUTER APPLY lion.fn_GetTradePriceMatchedRAGPrices (
	 di.ItemUDDecimal1,  /*CurrBranchTradePrice*/
	 di.CurrentCost,
	 frpb.FloorGPP,
	 frpb.TargetGPP,
	 frpb.StretchGPP,
	 1,
	 1
	) p
	OUTER APPLY ppe.fn_GetDiscount(ct.AccountKey, ct.ItemGroup3Key, ct.ItemKey, p.GreenPrice, di.ItemUDDecimal1, 'G') gd
	OUTER APPLY ppe.fn_GetDiscount(ct.AccountKey, ct.ItemGroup3Key, ct.ItemKey, p.AmberPrice, di.ItemUDDecimal1, 'A') ad
	OUTER APPLY ppe.fn_GetDiscount(ct.AccountKey, ct.ItemGroup3Key, ct.ItemKey, p.RedPrice, di.ItemUDDecimal1, 'R') rd


	UNION

	SELECT
		NEWID() AS GUID,
		RequestKey, -- (nonnull if this is a request otherwise it should be null)
		dig3.IG3Level1 AS SPGCode,
		dig3.IG3Level1UDVarchar1 AS SPGDescription,
		CASE WHEN di.ItemKey = 1 THEN '' ELSE di.ItemUDVarchar20 END AS ProductCode,
		di.ItemDescription AS ProductDescription,
		r.NewSPGDiscount1 AS SPGDiscount1,
		r.NewSPGDiscount2 AS SPGDiscount2,
		r.NewSPGFromDate,
		rd.Discount AS RedDiscount,
		ad.Discount AS AmberDiscount,
		gd.Discount AS GreenDiscount,
		p.RedPrice, --(used for product exception)
		p.GreenPrice, --(used for product exception)
		p.AmberPrice, --(used for product exception)
		NewDeleteTerm,
		r.NewExceptionDiscount AS ExceptionDiscount,
		r.NewExceptionFixedPrice AS ExceptionFixedPrice,
		r.NewExceptionStartDate AS ExceptionStartDate,
		r.NewExceptionExpiryDate AS ExceptionExpiryDate,
		AcceptedImpact,
		r.NewTermType,
		da.AccountNumber,
		da.AccountName,
		da.AccountKey,
		r.ItemGroup3Key,
		r.ItemKey,
		ISNULL(r.FixedPriceFlag,CASE WHEN da.AccountUDVarChar12 = 'N' THEN 0 ELSE 1 END ) FixedPriceFlag,
		CASE WHEN ct.CustomerTermsKey IS NULL THEN 0 ELSE 1 END AS HasTerm
	FROM ppe.RequestMain r 
	LEFT JOIN lion.CustomerTerms ct
		ON ct.AccountKey = r.AccountKey
		AND ct.ItemGroup3Key = r.ItemGroup3Key
		AND ct.ItemKey = r.ItemKey
	LEFT JOIN FactRecommendedPriceBand frpb 
	 ON frpb.AccountKey = r.AccountKey
	 AND frpb.ItemKey = r.ItemKey
	 AND frpb.InvoiceLineGroup1Key = 4
	INNER JOIN dbo.DimItem di 
	 ON di.ItemKey = r.ItemKey
	INNER JOIN dbo.DimAccount da 
		ON da.AccountKey = r.AccountKey
	LEFT JOIN dbo.DimItemGroup3 dig3 ON dig3.ItemGroup3Key = r.ItemGroup3Key
	OUTER APPLY lion.fn_GetTradePriceMatchedRAGPrices (
	 di.ItemUDDecimal1,  /*CurrBranchTradePrice*/
	 di.CurrentCost,
	 frpb.FloorGPP,
	 frpb.TargetGPP,
	 frpb.StretchGPP,
	 1,
	 1
	) p
	OUTER APPLY ppe.fn_GetDiscount(r.AccountKey, r.ItemGroup3Key, r.ItemKey, p.GreenPrice, di.ItemUDDecimal1, 'G') gd
	OUTER APPLY ppe.fn_GetDiscount(r.AccountKey, r.ItemGroup3Key, r.ItemKey, p.AmberPrice, di.ItemUDDecimal1, 'A') ad
	OUTER APPLY ppe.fn_GetDiscount(r.AccountKey, r.ItemGroup3Key, r.ItemKey, p.RedPrice, di.ItemUDDecimal1, 'R') rd
	WHERE r.StatusKey <= 4 --per LPF-1486










GO
