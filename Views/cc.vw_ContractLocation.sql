SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO






CREATE VIEW [cc].[vw_ContractLocation]
AS

	SELECT  scl.ContractLocationKey ,
			scl.ContractKey ,
			scl.ContractLocationLevelKey ,
			scl.LocationKey ,
			LocationLevelName ,
			AG1Level1 AS Location,
			AG1Level1UDVarchar1 AS LocationName,
			scl.StartDate,
			scl.ExpirationDate,
			wu1.UserName AS CreatedByUserName,
			scl.CreatedOnDT,
			wu2.UserName AS ModifiedByUserName,
			scl.ModifiedOnDT

	FROM cc.SupplierContractLocation scl 
	JOIN cc.SupplierContractLocationLevel scll ON scll.ContractLocationLevelKey = scl.ContractLocationLevelKey
	JOIN dbo.DimAccountGroup1 dag1 ON dag1.AccountGroup1Key = scl.LocationKey
	LEFT JOIN dbo.WebUser wu1 ON wu1.WebUserKey = scl.CreatedByWebUserKey
	LEFT JOIN dbo.WebUser wu2 ON wu2.WebUserKey = scl.ModifiedByWebUserKey
	WHERE scl.ContractLocationLevelKey = 1

	UNION 

	SELECT  scl.ContractLocationKey ,
			scl.ContractKey ,
			scl.ContractLocationLevelKey ,
			scl.LocationKey ,
			LocationLevelName ,
			NetworkId AS Location,
			NetworkDescription AS LocationName,
			scl.StartDate,
			scl.ExpirationDate,
			wu1.UserName AS CreatedByUserName,
			scl.CreatedOnDT,
			wu2.UserName AS ModifiedByUserName,
			scl.ModifiedOnDT
	FROM cc.SupplierContractLocation scl
	JOIN cc.SupplierContractLocationLevel scll ON scll.ContractLocationLevelKey = scl.ContractLocationLevelKey
	JOIN dbo.DimNetwork dn ON dn.NetworkKey = scl.LocationKey
	LEFT JOIN dbo.WebUser wu1 ON wu1.WebUserKey = scl.CreatedByWebUserKey
	LEFT JOIN dbo.WebUser wu2 ON wu2.WebUserKey = scl.ModifiedByWebUserKey
	WHERE scl.ContractLocationLevelKey = 2

	UNION 

	SELECT  scl.ContractLocationKey ,
			scl.ContractKey ,
			scl.ContractLocationLevelKey ,
			scl.LocationKey ,
			LocationLevelName ,
			Area AS Location,
			AreaName AS LocationName,
			scl.StartDate,
			scl.ExpirationDate,
			wu1.UserName AS CreatedByUserName,
			scl.CreatedOnDT,
			wu2.UserName AS ModifiedByUserName,
			scl.ModifiedOnDT
	FROM cc.SupplierContractLocation scl
	JOIN cc.SupplierContractLocationLevel scll ON scll.ContractLocationLevelKey = scl.ContractLocationLevelKey
	JOIN dbo.DimArea da ON da.AreaKey = scl.LocationKey
	LEFT JOIN dbo.WebUser wu1 ON wu1.WebUserKey = scl.CreatedByWebUserKey
	LEFT JOIN dbo.WebUser wu2 ON wu2.WebUserKey = scl.ModifiedByWebUserKey
	WHERE scl.ContractLocationLevelKey = 3

	UNION 

	SELECT  scl.ContractLocationKey ,
			scl.ContractKey ,
			scl.ContractLocationLevelKey ,
			scl.LocationKey ,
			LocationLevelName ,
			Region AS Location,
			RegionName AS LocationName,
			scl.StartDate,
			scl.ExpirationDate,
			wu1.UserName AS CreatedByUserName,
			scl.CreatedOnDT,
			wu2.UserName AS ModifiedByUserName,
			scl.ModifiedOnDT
	FROM cc.SupplierContractLocation scl
	JOIN cc.SupplierContractLocationLevel scll ON scll.ContractLocationLevelKey = scl.ContractLocationLevelKey
	JOIN dbo.DimRegion dr ON dr.RegionKey = scl.LocationKey
	LEFT JOIN dbo.WebUser wu1 ON wu1.WebUserKey = scl.CreatedByWebUserKey
	LEFT JOIN dbo.WebUser wu2 ON wu2.WebUserKey = scl.ModifiedByWebUserKey
	WHERE scl.ContractLocationLevelKey = 4

	UNION 

	SELECT  scl.ContractLocationKey ,
			scl.ContractKey ,
			scl.ContractLocationLevelKey ,
			scl.LocationKey ,
			LocationLevelName ,
			Brand AS Location,
			Brand AS LocationName,
			scl.StartDate,
			scl.ExpirationDate,
			wu1.UserName AS CreatedByUserName,
			scl.CreatedOnDT,
			wu2.UserName AS ModifiedByUserName,
			scl.ModifiedOnDT
	FROM cc.SupplierContractLocation scl
	JOIN cc.SupplierContractLocationLevel scll ON scll.ContractLocationLevelKey = scl.ContractLocationLevelKey
	JOIN dbo.DimBrand db ON db.BrandKey = scl.LocationKey
	LEFT JOIN dbo.WebUser wu1 ON wu1.WebUserKey = scl.CreatedByWebUserKey
	LEFT JOIN dbo.WebUser wu2 ON wu2.WebUserKey = scl.ModifiedByWebUserKey
	WHERE scl.ContractLocationLevelKey = 5






GO
