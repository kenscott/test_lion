SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE VIEW [ppe].[vw_ExcludedItemSPG] AS 
	SELECT 
		NEWID() AS GUID,
		NULL AS ExcludeSPGKey,
		ExcludeItemKey,
		di.ItemKey,
		NULL AS ItemGroup3Key,
		ItemUDVarChar1 AS ItemPyramidCode,
		NULL AS SPGCode,
		NULL AS SPGDescription ,
		ItemUDVarChar20 AS ProductCode,
		ItemDescription,
		ei.CreationDate
	FROM ppe.ExcludeItem ei
	JOIN DimItem di ON ei.ItemKey = di.ItemKey
	UNION
	SELECT 
		NEWID() AS GUID,
		ExcludeSPGKey,
		NULL AS ExcludeItemKey,
		NULL AS ItemKey,
		dig3.ItemGroup3Key,
		NULL AS ItemPyramidCode,
		IG3Level1 AS SPGCode,
		IG3Level1UDVarchar1 AS SPGDescription ,
		NULL AS ProductCode,
		NULL AS ItemDescription	,
		es.CreationDate
	FROM ppe.ExcludeSPG es
	JOIN dimitemgroup3 dig3 ON es.ItemGroup3Key = dig3.ItemGroup3Key

GO
