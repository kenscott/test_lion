SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





CREATE   view
[dbo].[PlaybookPriceProposal_View]
as
Select B.PlaybookDataPointGroupKey, 
		PDP.AccountKey,
		PDP.ItemKey,
		PDP.InvoiceLineGroup1Key,
		A.PlaybookPriceProposalKey,
		A.PlaybookDataPointOptimalPriceKey,
		A.ProposalRecommendationKey,
		A.ProposedPrice,
		A.ProposedCost,
		A.CreationDate

From  PlaybookPriceProposal A
join PlaybookDatapointOptimalPrice_View B on 
	A.PlaybookDatapointOptimalPriceKey = B.PlaybookDatapointOptimalPriceKey
join PlaybookDataPointPricingGroup C on
	C.PlaybookDataPointPricingGroupKey = B.PlaybookDataPointPricingGroupKey
join PlaybookDataPoint PDP on PDP.PlaybookDataPointKey = C.PlaybookDataPointKey





GO
