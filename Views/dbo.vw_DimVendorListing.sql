SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE VIEW [dbo].[vw_DimVendorListing] AS
/*
select top 10 * from vw_DimVendorListing
*/

	SELECT 
		VendorNumber,
		VendorDescription,
		VendorOracleCode AS AccountingNumber,
		VendorComments AS Comments,
		Inactive AS InactiveIndicator
	FROM DimVendor


GO
