SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE VIEW [ppe].[vw_ErrorReport] AS
	SELECT r.RequestKey AS [Request ID],
		   a.Branch,
		   a.AccountNumber AS [Account Number],
		   a.AccountName AS [Account Name],
		   a.FixedPriceIndicator AS [Fixed Price Indicator],
		   ig3.LLSPGCode AS [SPG],
		   CASE WHEN r.itemkey = 1 THEN NULL ELSE i.ProductCode END AS [Product Code],
		   CASE WHEN r.itemkey = 1 THEN ig3.LLSPGDescription ELSE i.ItemDescription END AS [Product/LLSPG Description],
		   i.KnownValueInd AS [KVI Flag],
		   CASE WHEN k.LLSPGCode IS NOT NULL THEN 'Y' ELSE NULL END AS [KVI Override Flag],
		   CASE WHEN r.ItemKey = 1 THEN r.CurrentCompoundDiscount ELSE NULL END AS [Old SPG Discount],
		   CASE WHEN r.ItemKey = 1 THEN r.NewCompoundDiscount ELSE NULL END AS [New SPG Discount],
		   CASE WHEN r.ItemKey = 1 THEN NULL ELSE r.CurrentExceptionDiscount END AS [Old Exception Discount],
		   CASE WHEN r.ItemKey = 1 THEN NULL ELSE r.NewExceptionDiscount END AS [New Exception Discount],
		   CASE WHEN r.ItemKey = 1 THEN NULL ELSE r.CurrentExceptionFixedPrice END AS [Old Fixed Price],
		   CASE WHEN r.ItemKey = 1 THEN NULL ELSE r.NewExceptionFixedPrice END AS [New Fixed Price],
		   r.RealityResponseCode AS [Error Message],
		   a.AccountManagerFullName,
		   od1.FullName AS RequestOwnerFullName,
		   r.ModificationDate
	  FROM ppe.RequestMain r
		   JOIN lion.vwAccount a ON r.AccountKey = a.AccountKey
		   JOIN lion.vwItemGroup3 ig3 ON r.ItemGroup3Key = ig3.ItemGroup3Key
		   JOIN lion.vwItem i ON r.ItemKey = i.ItemKey
		   LEFT JOIN lion.AccountSPG k ON k.AccountKey = r.AccountKey AND (k.LLSPGCode = ig3.LLSPGCode OR k.LLSPGCode = 'ALL')
		   LEFT JOIN dbo.WebUser wu1 ON wu1.WebUserKey = r.OwnerWebUserKey
		   LEFT JOIN dbo.ODSPerson od1 ON od1.ODSPersonKey = wu1.ODSPersonKey
		  
	 WHERE RealityResponseCode IS NOT NULL


GO
