SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO







CREATE VIEW [cc].[vw_ContractAccountLookup] AS
/*
	select top 5 * from dimaccount
	select top 5 * from cc.vw_ContractAccountLookup where accountnumber like 'X%'
*/

	SELECT 
		da.AccountKey,
		da.AccountNumber,
		da.AccountName,
		da.AccountNumberParent,
		dag2.AG2Level1 as CustomerType,   -- LPF-3900 - Add Customer Type to vw_ContractAccountLookup 
		CASE  -- LPF-2069
			WHEN da.AccountNumberParent <> da.AccountNumber THEN 'C'  -- if numbers are different, I am a child
			WHEN (SELECT COUNT(*) FROM dbo.DimAccount da2 WHERE da2.AccountNumberParent = da.AccountNumber ) > 1  THEN 'P' -- if anyone else claims me as parent, i am a parent
			ELSE '' -- else neither
		END AS ParentChild,
		CASE
			WHEN LEFT(da.AccountNumber, 1) = 'X' THEN 2  -- account number starts with X
			ELSE 1
		END AS AccountPyramidCode -- LPF-3735
	FROM DimAccount da 
	INNER JOIN dbo.DimAccountGroup2 dag2 (NOLOCK)
		ON dag2.AccountGroup2Key = da.AccountGroup2Key
	WHERE da.AccountKey <> 1
	AND da.Inactive = 0   --only include active accounts LPF-2707











GO
