SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [lion].[vwCommonTermsBenefitTransactions]
AS
--Used by Actual Benefits Cube

  
    SELECT  t.DayKey ,
            t.AccountKey ,
            t.ItemKey ,
            t.InvoiceLineGroup1Key ,
            t.AccountGroup1Key ,
            t.ItemGroup3Key ,
            t.DeliveryDate ,
            t.ChargeNote ,
            t.PriceDerivation ,
            t.NetQuantity ,
            t.NetSales ,
            t.NetCost ,
            t.GP ,
            t.GPP ,
            t.ActualDiscount ,
            t.TradePrice ,
            t.[Contract Claims Indicator] ,
            t.[Claim Amount] ,
            t.ClientInvoiceLineUniqueID ,
            t.AccountingBrand ,
            t.BranchBrand ,
            t.YoY ,
            t.h_QoQ ,
            t.h_MoM
    FROM    lion.mvwTermsBenefitTransactions_1 AS t
  



GO
