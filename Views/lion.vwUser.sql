SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE
VIEW [lion].[vwUser] 

/*
SELECT TOP 10 * FROM lion.vwUser
SELECT * FROM lion.vwUser ORDER BY 1
SELECT * FROM WebRole
*/


AS

WITH Admins AS (
	SELECT DISTINCT
		WebUserKey
	FROM dbo.WebUserRole wur
	INNER JOIN dbo.WebRole wr
		ON wr.WebRoleKey = wur.WebRoleKey
	WHERE
		WebRoleName = 'Administrator'
), P2PUsers AS (
	SELECT DISTINCT
		WebUserKey
	FROM dbo.WebUserRole wur
	INNER JOIN dbo.WebRole wr
		ON wr.WebRoleKey = wur.WebRoleKey
	WHERE
		WebRoleName = 'P2PUser'
), CentralPricing AS (
	SELECT DISTINCT
		WebUserKey
	FROM dbo.WebUserRole wur
	INNER JOIN dbo.WebRole wr
		ON wr.WebRoleKey = wur.WebRoleKey
	WHERE
		WebRoleName = 'CentralPricing'
), RegionManager AS (
	SELECT DISTINCT
		WebUserKey
	FROM dbo.WebUserRole wur
	INNER JOIN dbo.WebRole wr
		ON wr.WebRoleKey = wur.WebRoleKey
	WHERE
		WebRoleName = 'RegionManager'
), AreaManager AS (
	SELECT DISTINCT
		WebUserKey
	FROM dbo.WebUserRole wur
	INNER JOIN dbo.WebRole wr
		ON wr.WebRoleKey = wur.WebRoleKey
	WHERE
		WebRoleName = 'AreaManager'
), NetworkManager AS (
	SELECT DISTINCT
		WebUserKey
	FROM dbo.WebUserRole wur
	INNER JOIN dbo.WebRole wr
		ON wr.WebRoleKey = wur.WebRoleKey
	WHERE
		WebRoleName = 'NetworkManager'
), BranchManager AS (
	SELECT DISTINCT
		WebUserKey
	FROM dbo.WebUserRole wur
	INNER JOIN dbo.WebRole wr
		ON wr.WebRoleKey = wur.WebRoleKey
	WHERE
		WebRoleName = 'BranchManager'
), ITAdmins AS (
	SELECT DISTINCT
		WebUserKey
	FROM dbo.WebUserRole wur
	INNER JOIN dbo.WebRole wr
		ON wr.WebRoleKey = wur.WebRoleKey
	WHERE
		WebRoleName = 'ITAdmin'
), PricingCoordinators AS (
	SELECT DISTINCT
		WebUserKey
	FROM dbo.WebUserRole wur
	INNER JOIN dbo.WebRole wr
		ON wr.WebRoleKey = wur.WebRoleKey
	WHERE
		WebRoleName = 'PricingCoordinator'
)


SELECT
	wu.WebUserKey,
	wu.ODSPersonKey,
	wu.AccountManagerKey,
	wu.WebUserPassword,
	wu.WebUserPasswordExpirationDate,

	wu.UserName,
	o.FullName,
	o.Email,
	o.ExtID,
	CASE ISNULL(o.LevelId, '')
		WHEN 'BranchManager' THEN 'B'
		WHEN 'NetworkManager' THEN 'N'
		WHEN 'CentralPricing' THEN 'C'
		ELSE ''
	END AS  WorkflowLevelIndicator,		
	CASE
		WHEN Admins.WebUserKey IS NULL THEN 'N'
		ELSE 'Y'
	END AdminRoleIndicator,
	CASE
		WHEN ITAdmins.WebUserKey IS NULL THEN 'N'
		ELSE 'Y'
	END ITAdminRoleIndicator,
	CASE
		WHEN P2PUsers.WebUserKey IS NULL THEN 'N'
		ELSE 'Y'
	END P2PUserRoleIndicator,
	CASE
		WHEN CentralPricing.WebUserKey IS NULL THEN 'N'
		ELSE 'Y'
	END CentralPricingRoleIndicator,
	CASE
		WHEN RegionManager.WebUserKey IS NULL THEN 'N'
		ELSE 'Y'
	END RegionManagerRoleIndicator,
	CASE
		WHEN AreaManager.WebUserKey IS NULL THEN 'N'
		ELSE 'Y'
	END AreaManagerRoleIndicator,
	CASE
		WHEN NetworkManager.WebUserKey IS NULL THEN 'N'
		ELSE 'Y'
	END NetworkManagerRoleIndicator,
	CASE
		WHEN BranchManager.WebUserKey IS NULL THEN 'N'
		ELSE 'Y'
	END BranchManagerRoleIndicator,
	CASE
		WHEN PricingCoordinators.WebUserKey IS NULL THEN 'N'
		ELSE 'Y'
	END PricingCoordinatorRoleIndicator
FROM dbo.WebUser wu
JOIN dbo.ODSPerson o 
	ON o.ODSPersonKey = wu.ODSPersonKey
LEFT JOIN Admins
	ON Admins.WebUserKey = wu.WebUserKey
LEFT JOIN PricingCoordinators
	ON PricingCoordinators.WebUserKey = wu.WebUserKey	
LEFT JOIN ITAdmins
	ON ITAdmins.WebUserKey = wu.WebUserKey
LEFT JOIN P2PUsers
	ON P2PUsers.WebUserKey = wu.WebUserKey
LEFT JOIN CentralPricing
	ON CentralPricing.WebUserKey = wu.WebUserKey
LEFT JOIN RegionManager
	ON RegionManager.WebUserKey = wu.WebUserKey
LEFT JOIN AreaManager
	ON AreaManager.WebUserKey = wu.WebUserKey
LEFT JOIN NetworkManager
	ON NetworkManager.WebUserKey = wu.WebUserKey
LEFT JOIN BranchManager
	ON BranchManager.WebUserKey = wu.WebUserKey






GO
