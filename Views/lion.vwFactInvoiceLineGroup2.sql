SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




CREATE 
VIEW [lion].[vwFactInvoiceLineGroup2] 

AS

/*
SELECT * FROM lion.vwFactInvoiceLineGroup2
*/

SELECT
	InvoiceLineGroup2Key,
	InvoiceLineGroup2UDVarchar1 AS PriceDerivation,
	InvoiceLineGroup2UDVarchar2 AS PriceDerivationBucket,
	InvoiceLineGroup2UDVarchar3 AS PriceDerivationDescription,
	InvoiceLineGroup2UDVarchar4 AS ChrisHierarchy,
	InvoiceLineGroup2UDVarchar5 AS PriceflowSalesSummary
FROM DimInvoiceLineGroup2 (NOLOCK)




GO
