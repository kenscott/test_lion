SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO













CREATE 
VIEW [lion].[vwFactInvoiceLine] 

/*
SELECT TOP 10 * FROM lion.vwFactInvoiceLine 

SELECT * FROM lion.vwFactInvoiceLineGroup1
SELECT * FROM lion.vwFactInvoiceLineGroup2
SELECT * FROM lion.vwFactInvoiceLineGroup3

select count(*) from FactInvoiceLine
select count(*) from DimInvoiceLineJunk
select count(*) from lion.vwFactInvoiceLine
*/


AS

SELECT
	AccountKey,
	AlternateAccountKey,
	ItemKey,
	VendorKey,
	AccountManagerKey,
	InvoiceDateDayKey,
	InvoiceDateMonthKey,
	InvoiceLineGroup1Key,
	InvoiceLineGroup2Key,
	InvoiceLineGroup3Key,
	fil.InvoiceLineJunkKey,
	AccountGroup1Key,
	AccountGroup2Key,
	AccountGroup3Key,
	ItemGroup1Key,
	ItemGroup2Key,
	ItemGroup3Key,

	TotalQuantity,
	TotalActualPrice,
	TotalActualCost,
	--TotalActualRebate,
	
	Last12MonthsIndicator,
	
	fil.InvoiceLineUniqueIdentifier,
	fil.ClientInvoiceLineUniqueID,
	
	fil.ClientInvoiceID AS ClientInvoiceID,
	fil.ClientInvoiceLineID AS ClientInvoiceLineID,
	
	--ClientInvoiceID,
	--fil.ClientInvoiceID AS LastTransNo,
	--fil.UnitTradePrice AS TradingPrice,
	
	fil.UnitListPrice AS TradingPrice,
	
	RedPrice,
	AmberPrice,
	GreenPrice,
	BluePrice,
	TerminalEmulator,
	PriceGuidanceIndicator,

	UDVarchar1 AS PriceDerivation,
	UDVarchar2 AS VariablePackInd,
	UDVarchar3 AS ClaimNo,

	fil.UDDecimal1 AS ApTradingMarginPadRelease,
	fil.UDDecimal2 AS CCProfit,
	fil.UDDecimal3 AS RetailPrice,
	
	TotalNetQuantity,
	TotalNetPrice,
	TotalNetCost,

	InvoiceLineJunkUDVarchar1 AS BranchCode,
	InvoiceLineJunkUDVarchar2 AS TransNo,
	InvoiceLineJunkUDVarchar3 AS DelNo,
	InvoiceLineJunkUDVarchar4 AS LineNumber,
	InvoiceLineJunkUDVarchar5 AS NetQty,
	InvoiceLineJunkUDVarchar6 AS NetLinePrice,
	InvoiceLineJunkUDVarchar7 AS NetApTradingMarginPadRelease,
	InvoiceLineJunkUDVarchar8 AS DeliveryDate,
	InvoiceLineJunkUDVarchar9 AS SledgerNo,
	InvoiceLineJunkUDVarchar10 AS SledgerDate,
	InvoiceLineJunkUDVarchar11 AS YMTH,
	InvoiceLineJunkUDVarchar12 AS SupplyType,
	InvoiceLineJunkUDVarchar13 AS DocInd,
	InvoiceLineJunkUDVarchar14 AS CreditReason,
	InvoiceLineJunkUDVarchar15 AS KnownValueItem,
	InvoiceLineJunkUDVarchar16 AS CtsRelCode,
	InvoiceLineJunkUDVarchar17 AS CustCntNo,
	InvoiceLineJunkUDVarchar18 AS MthdOfDespatch,
	InvoiceLineJunkUDVarchar19 AS TransType,
	InvoiceLineJunkUDVarchar20 AS SalesOrigin,
	InvoiceLineJunkUDVarchar21 AS TakenBy,
	InvoiceLineJunkUDVarchar22 AS MethodOfPayment,
	InvoiceLineJunkUDVarchar23 AS HeaderID,
	InvoiceLineJunkUDVarchar24 AS CrdOrigInvNo,
	InvoiceLineJunkUDVarchar25 AS CrdOrigInvDate,
	InvoiceLineJunkUDVarchar26 AS OriginalOrdNo,
	InvoiceLineJunkUDVarchar27 AS CarriagePack,
	InvoiceLineJunkUDVarchar28 AS DelAddPhone,
	InvoiceLineJunkUDVarchar29 AS GpAllocBranchCode,
	InvoiceLineJunkUDVarchar30 AS SalesOriginDesc,
	InvoiceLineJunkUDVarchar31 AS PriceOrderType,
	InvoiceLineJunkUDVarchar32 AS TransactionDate,
	InvoiceLineJunkUDVarchar33 AS OrderDate,
	InvoiceLineJunkUDVarchar34 AS AccountingBrand,
	InvoiceLineJunkUDVarchar35 AS TradingBranchPrimaryBrand,
	InvoiceLineJunkUDVarchar36 AS BranchBrand,
	InvoiceLineJunkUDVarchar37 AS CustOrdRef,

	InvoiceLineJunkUDDecimal3 AS SuppliersNettCost,
	InvoiceLineJunkUDDecimal4 AS SuppliersNettCostMNTHDay1
	--,

	--InvoiceLineJunkUDVarchar37 AS SrcAddr1,
	--InvoiceLineJunkUDVarchar38 AS SrcAddr2,
	--InvoiceLineJunkUDVarchar39 AS SrcAddr3,
	--InvoiceLineJunkUDVarchar40 AS SrcAddr4,
	--InvoiceLineJunkUDVarchar41 AS SrcAddr5

FROM
	dbo.FactInvoiceLine fil (NOLOCK)
INNER JOIN dbo.DimInvoiceLineJunk diljunk (NOLOCK)
	ON diljunk.InvoiceLineJunkKey = fil.InvoiceLineJunkKey












GO
