SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE VIEW [lion].[vwFactInvoiceLinePricebandScore2] 

AS


/*

SELECT TOP 10 * FROM lion.vwFactInvoiceLinePricebandScore2

*/

SELECT 
	AccountKey,
	AlternateAccountKey,
	ItemKey,
	VendorKey,
	AccountManagerKey,
	InvoiceDateDayKey,
	InvoiceDateMonthKey,
	InvoiceLineGroup1Key,
	InvoiceLineGroup2Key,
	InvoiceLineGroup3Key,
	InvoiceLineJunkKey,
	AccountGroup1Key,
	AccountGroup2Key,
	AccountGroup3Key,
	ItemGroup1Key,
	ItemGroup2Key,
	ItemGroup3Key,
	InvoiceLineUniqueIdentifier,
	LinkedInvoiceLineUniqueIdentifier,
	ClientInvoiceID,
	ClientInvoiceLineID,
	ClientInvoiceLineUniqueID,
	
	TotalQuantity,
	TotalActualPrice,
	TotalActualCost,
	UnitListPrice,
	TotalNetQuantity,
	TotalNetPrice,
	TotalNetCost,
	
	RedPrice,
	AmberPrice,
	GreenPrice,
	BluePrice,
	TerminalEmulator,
	PriceGuidanceIndicator,

	UDVarchar1 AS PriceDerivation,

	FloorGPP,
	TargetGPP,
	StretchGPP,
	Score,
	FloorImpact,
	TargetImpact,
	StretchImpact,
	RPBPlaybookPricingGroupKey,
	c.GPP AS CurrentGPP,
	ft.Impact AS FloorTargetImpact,
	ts.Impact AS TargetStretchImpact,
	s.Risk AS StretchRisk

FROM
	lion.FactInvoiceLinePricebandScore_Historical filpbs (NOLOCK)
	CROSS APPLY lion.fn_GetGPP (TotalActualPrice, TotalActualCost) c
	CROSS APPLY lion.fn_GetPartialImpact (FloorGPP, TargetGPP, c.GPP, TotalQuantity) ft
	CROSS APPLY lion.fn_GetPartialImpact (TargetGPP, StretchGPP, c.GPP, TotalQuantity) ts
	CROSS APPLY lion.fn_GetRisk(StretchGPP, c.GPP, TotalQuantity) s
	
UNION

SELECT 
	AccountKey,
	AlternateAccountKey,
	ItemKey,
	VendorKey,
	AccountManagerKey,
	InvoiceDateDayKey,
	InvoiceDateMonthKey,
	InvoiceLineGroup1Key,
	InvoiceLineGroup2Key,
	InvoiceLineGroup3Key,
	InvoiceLineJunkKey,
	AccountGroup1Key,
	AccountGroup2Key,
	AccountGroup3Key,
	ItemGroup1Key,
	ItemGroup2Key,
	ItemGroup3Key,
	InvoiceLineUniqueIdentifier,
	LinkedInvoiceLineUniqueIdentifier,
	ClientInvoiceID,
	ClientInvoiceLineID,
	ClientInvoiceLineUniqueID,
	
	TotalQuantity,
	TotalActualPrice,
	TotalActualCost,
	UnitListPrice,
	TotalNetQuantity,
	TotalNetPrice,
	TotalNetCost,
	
	RedPrice,
	AmberPrice,
	GreenPrice,
	BluePrice,
	TerminalEmulator,
	PriceGuidanceIndicator,

	PriceDerivation,

	FloorGPP,
	TargetGPP,
	StretchGPP,
	Score,
	FloorImpact,
	TargetImpact,
	StretchImpact,
	RPBPlaybookPricingGroupKey,
	CurrentGPP,
	FloorTargetImpact,
	TargetStretchImpact,
	StretchRisk

FROM lion.vwFactInvoiceLinePricebandScore
--WHERE InvoiceDateMonthKey >= 200


GO
