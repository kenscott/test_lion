SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
















CREATE VIEW [lion].[vwItem] 

/*
SELECT TOP 10 * FROM lion.vwItem 
*/

AS

SELECT
	di.[ItemKey],

	DimVendor.VendorKey AS VendorKey,
	VendorDescription,
	VendorNumber,

	ItemNumber,
	ItemDescription,
	BaseUnitOfMeasure, 
	VendorStockNumber,
	
	ItemUDVarChar1 AS ItemPyramidCode,
	ItemUDVarChar20 AS ProductCode,
	ItemUDVarChar2 AS ProdBrandAtLLSPG,
	ItemUDVarChar3 AS MPGCode,
	ItemUDVarChar4 AS MPGDescription,
	ItemUDVarChar5 AS SuperCategory,
	ItemUDVarChar6 AS Category,
	ItemUDVarChar7 AS SubCategory,
	ItemUDVarChar8 AS ItemObsolete,
	ItemUDVarChar9 AS HireInd,
	ItemUDVarChar10 AS SpecialInd,
	ItemUDVarChar11 AS ProductLifecycle,
	ItemUDVarChar12 AS OwnBrandInd,
	ItemUDVarChar13 AS KnownValueInd,
	ItemUDVarChar14 AS NDPInd,
	ItemUDVarChar15 AS MultiSourcedProductInd,
	ItemUDVarChar16 AS VariablePackInd,
	ItemUDVarChar17 AS ZonedInd,
	ItemUDVarChar18 AS UnitCode,
	ItemUDVarChar19 AS GenericProductID,
	ItemUDDate1 AS ItemDateForDeletion,
	ItemUDDate2 AS CurrBranchCostDate,
	ItemUDDate3 AS CurrBranchTradePriceDate,
	ItemUDVarChar21 AS PlaybookProductBrand,
	ItemUDVarChar22 AS CurrentSncDate,
	
	ItemUDDecimal1 AS CurrBranchTradePrice,
	--ItemUDDecimal2 AS RetailPrice,

	ItemUDDecimal3 AS CurrentBranchCost,
	ItemUDDecimal4 AS Padding,
	ItemUDDecimal5 AS CurrNotSettVal,
	CurrentCost,
	CurrentInvoiceCost,
	di.InActive,

	--IG2Level1 AS ProdBrandAtLLSPG,
	--IG2Level1UDVarchar1 AS ProductTypeDescription,
	
	IG3Level1 AS LLSPGCode,--1
	IG3Level2 AS HLSPGCode,--2
	IG3Level3 AS GSPGCode,--3
	--IG3Level4 AS PYRCode,--4
	IG3Level1UDVarchar1 AS LLSPGDescription,--1
	IG3Level2UDVarchar1 AS HLSPGDescription,--2
	IG3Level3UDVarchar1 AS GSPGDescription --3
	--IG3Level4UDVarchar1 AS PYRCode,--4


FROM dbo.DimItem di (NOLOCK)
INNER JOIN dbo.DimItemGroup1 dig1 (NOLOCK)
	ON dig1.ItemGroup1Key = di.ItemGroup1Key
--INNER JOIN dbo.DimItemGroup2 dig2 (NOLOCK)
--	ON dig2.ItemGroup2Key = di.ItemGroup2Key
INNER JOIN dbo.DimItemGroup3 dig3 (NOLOCK)
	ON dig3.ItemGroup3Key = di.ItemGroup3Key
INNER JOIN dbo.DimVendor (NOLOCK)
	ON DimVendor.VendorKey = di.ItemVendorKey
















GO
