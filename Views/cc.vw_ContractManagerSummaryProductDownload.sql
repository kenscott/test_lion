SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO









/*
	Author: James Dameron
	Date: 10/11/2016
	Jira Reference: LPF-3268
	Description:  Provides the output for the product centric contract listing download.  Uses
				  vw_ContractManagerSummary as a base.

	Example usage:
		select top 100 * from cc.vw_ContractManagerSummaryProductDownload where contractkey = 156552

	Reference queries:
		select count( *) from cc.vw_ContractManagerSummaryProductDownload

*/

CREATE VIEW [cc].[vw_ContractManagerSummaryProductDownload] AS


SELECT
	NEWID() as GUID,
	cms.ContractKey,
	cms.Status,
	cms.ContractID,
	cms.Supplier,
	cms.ContractReference,
	cms.StartDate,
	cms.ExpirationDate,
	cms.JobFlag,
	cms.Developer,
	cms.LocationDisplay,
	cms.AccountDisplay,
	cms.AccountName,
	cms.ItemDisplay,
	cms.MPGDisplay,
	cms.spgDisplay,
	cms.CCProfit,
	cms.AuthorizedIndicator,
	cms.PyramidCode,
	cms.ContractType1,
	cms.ContractOwner,
	cms.OwnerBranchDescription,
	cms.FixedPriceDate,
	cms.PricingContractID,
	cms.Originator,
	cms.ContractNumber,
	cms.ContactEmail,
	cms.ContactName,
	CASE WHEN cms.ContractSourceKey = 3 THEN 1 ELSE 0 END AS ConvertedIndicator,
	cms.PurchaseType,
	cms.ContractCreationDT,
	cms.CreatedByFullName as ContractCreationUser,
	CONVERT(DATE, cms.ContractModificationDate) AS ContractModificationDate,
	cms.ModifiedByFullName AS ContractModificationUser,
	cms.JobSite,
	
	di.ItemUDVarChar20 as Product,
	di.ItemDescription as ProductDescription,
	di.VendorStockNumber as SupplierCatalogNumber,
	sci.ClaimBackPerc * 100 as ClaimBackPerc,
	sci.ClaimBackAmount as ClaimBackAmount,
	sci.ClaimType as ClaimType
  FROM cc.vw_ContractManagerSummary cms	WITH (NOLOCK)
	LEFT JOIN cc.SupplierContractItem sci on sci.contractkey = cms.contractkey
	JOIN dbo.dimitem di on di.itemkey = sci.itemkey
	--JOIN dbo.WebUser wu on wu.WebUserKey = cms.CreatedByWebUserKey
	
UNION

SELECT
	NEWID() as GUID,
	cms.ContractKey,
	cms.Status,
	cms.ContractID,
	cms.Supplier,
	cms.ContractReference,
	cms.StartDate,
	cms.ExpirationDate,
	cms.JobFlag,
	cms.Developer,
	cms.LocationDisplay,
	cms.AccountDisplay,
	cms.AccountName,
	cms.ItemDisplay,
	cms.MPGDisplay,
	cms.spgDisplay,
	cms.CCProfit,
	cms.AuthorizedIndicator,
	cms.PyramidCode,
	cms.ContractType1,
	cms.ContractOwner,
	cms.OwnerBranchDescription,
	cms.FixedPriceDate,
	cms.PricingContractID,
	cms.Originator,
	cms.ContractNumber,
	cms.ContactEmail,
	cms.ContactName,
	CASE WHEN cms.ContractSourceKey = 3 THEN 1 ELSE 0 END AS ConvertedIndicator,
	cms.PurchaseType,
	cms.ContractCreationDT,
	cms.CreatedByFullName as ContractCreationUser,
	CONVERT(DATE, cms.ContractModificationDate) AS ContractModificationDate,
	cms.ModifiedByFullName AS ContractModificationUser,
	cms.JobSite,
	
	dig1.IG1Level1 as Product,
	dig1.IG1Level1UDVarchar1 as ProductDescription,
	'' as SupplierCatalogNumber,
	scigx.ClaimBackPerc * 100 as ClaimBackPerc,
	scigx.ClaimBackAmount as ClaimBackAmount,
	scigx.ClaimType as ClaimType
  FROM cc.vw_ContractManagerSummary cms	WITH (NOLOCK)
  LEFT JOIN cc.SupplierContractItemGroupX scigx on scigx.contractkey = cms.contractkey
  JOIN dbo.DimItemGroup1 dig1 on dig1.ItemGroup1Key = scigx.ItemGroupXKey
  WHERE scigx.itemgrouplevel = 1

UNION

SELECT
	NEWID() AS GUID,
	cms.ContractKey,
	cms.Status,
	cms.ContractID,
	cms.Supplier,
	cms.ContractReference,
	cms.StartDate,
	cms.ExpirationDate,
	cms.JobFlag,
	cms.Developer,
	cms.LocationDisplay,
	cms.AccountDisplay,
	cms.AccountName,
	cms.ItemDisplay,
	cms.MPGDisplay,
	cms.spgDisplay,
	cms.CCProfit,
	cms.AuthorizedIndicator,
	cms.PyramidCode,
	cms.ContractType1,
	cms.ContractOwner,
	cms.OwnerBranchDescription,
	cms.FixedPriceDate,
	cms.PricingContractID,
	cms.Originator,
	cms.ContractNumber,
	cms.ContactEmail,
	cms.ContactName,
	CASE WHEN cms.ContractSourceKey = 3 THEN 1 ELSE 0 END AS ConvertedIndicator,
	cms.PurchaseType,
	cms.ContractCreationDT,
	cms.CreatedByFullName as ContractCreationUser,
	CONVERT(DATE, cms.ContractModificationDate) AS ContractModificationDate,
	cms.ModifiedByFullName AS ContractModificationUser,
	cms.JobSite,
	
	dig3.IG3Level1 as Product,
	dig3.IG3Level1UDVarchar1 as ProductDescription,
	'' as SupplierCatalogNumber,
	scigx.ClaimBackPerc * 100 as ClaimBackPerc,
	scigx.ClaimBackAmount as ClaimBackAmount,
	scigx.ClaimType as ClaimType
  FROM cc.vw_ContractManagerSummary cms	WITH (NOLOCK)
  LEFT JOIN cc.SupplierContractItemGroupX scigx on scigx.contractkey = cms.contractkey
  JOIN dbo.DimItemGroup3 dig3 on dig3.ItemGroup3Key = scigx.ItemGroupXKey
  WHERE scigx.itemgrouplevel = 3







GO
