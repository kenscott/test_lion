SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




CREATE VIEW [dbo].[vw_WebUserRole] AS

	-- SELECT * FROM dbo.vw_WebUserRole

	-- Get all of the permissions EXPLICITLY assigned to the user (through WebUserPermission) -jed 10/26/2015
	SELECT DISTINCT wr.WebRoleKey AS ID, wud.DelegateWebUserKey AS WebUserKey, wr.WebRoleKey, wr.WebRoleName, wr.WebRoleDescription, wr.WorkflowLevel, wr.SystemManaged
	FROM WebUserDelegate wud
	JOIN dbo.WebUser wu ON wu.WebUserKey = wud.DelegatorWebUserKey
	JOIN dbo.WebUserRole wur ON wur.WebUserKey = wu.WebUserKey
	JOIN dbo.WebRole wr ON wr.WebRoleKey = wur.WebRoleKey
	
	--UNION
	
	-- Get all of the permissions IMPLICITLY assigned to the user (through WebUserRole => WebRole => WebRolePermission -jed 10/26/2015
	--SELECT DISTINCT wp.WebPermissionKey AS ID, wud.DelegateWebUserKey AS WebUserKey, /*wrp.WebRolePermissionKey,*/ wp.WebPermissionName, wp.WebPermissionDescription
	--FROM WebUserDelegate wud
	--JOIN dbo.WebUser wu ON wu.WebUserKey = wud.DelegatorWebUserKey
	--JOIN dbo.WebUserRole wur ON wur.WebUserKey = wu.WebUserKey
	--JOIN dbo.WebRole wr ON wr.WebRoleKey = wur.WebRoleKey
	--JOIN dbo.WebRolePermission wrp ON wrp.WebRoleKey = wur.WebRoleKey
	--JOIN dbo.WebPermission wp ON wp.WebPermissionKey = wrp.WebPermissionKey






GO
