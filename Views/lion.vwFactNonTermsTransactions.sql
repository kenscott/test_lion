SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [lion].[vwFactNonTermsTransactions]
AS
    SELECT  f.InvoiceLineUniqueIdentifier ,
            f.InvoiceDateDayKey AS DayKey ,
            f.AccountKey ,
            f.ItemKey ,
            f.InvoiceLineGroup1Key ,
            j.InvoiceLineJunkUDVarchar2 + '/'
            + REPLACE(j.InvoiceLineJunkUDVarchar3, '.00000000', '') AS ChargeNote ,
            f.AccountGroup1Key ,
            f.TotalQuantity AS Quantity ,
            f.TotalActualCost AS Cost ,
            f.TotalActualPrice AS Sales ,
            ( f.TotalActualPrice - f.TotalActualCost ) AS GP ,
            f.Score * f.TotalActualPrice AS [Scored Sales] ,
            f.Score AS PriceIndex ,
            CASE WHEN f.TerminalEmulator LIKE '%/F%' THEN 'Flynet'
                 ELSE 'Non-Flynet'
            END AS TerminalEmulator ,
            f.PriceGuidanceIndicator ,
            CASE WHEN f.TerminalEmulator IS NOT NULL
                 THEN CASE WHEN ISNULL(f.TerminalEmulator, '') NOT LIKE '(%/F)'
                           THEN 'Non Price Guidance'
                           WHEN f.PriceGuidanceIndicator = 'Y'
                           THEN 'Non Terms'
                           ELSE 'Terms'
                      END
                 ELSE CASE WHEN f.PriceDerivation IN ( 'RP', 'LP', 'TP', 'DDC',
                                                       'MP', 'ADJ', 'PGG',
                                                       'PGA', 'PGR', 'LPC' )
                           THEN 'Non Terms'
                           ELSE 'Terms'
                      END
            END AS PriceGroup ,
            f.PriceDerivation ,
            f.GreenPrice ,
            f.AmberPrice ,
            f.RedPrice ,
            f.UnitListPrice AS TradePrice ,
            f.GreenPrice * f.TotalQuantity AS [Sales @ Green] ,
            f.AmberPrice * f.TotalQuantity AS [Sales @ Amber] ,
            f.RedPrice * f.TotalQuantity AS [Sales @ Red] ,
            f.UnitListPrice * f.TotalQuantity AS [Sales @ Trade] ,
            gm.GPP AS GreenGPP ,
            am.GPP AS AmberGPP ,
            rm.GPP AS RedGPP ,
            ( f.UnitListPrice - ( f.TotalActualCost / NULLIF(f.TotalQuantity,
                                                             0) ) )
            / NULLIF(f.UnitListPrice, 0) AS TradeGPP ,
            flpac.UDVarchar1 AS BaseStrategicItem ,
            flpac.UDVarchar4 AS BaseSales603010Bucket ,
            flpac.UDVarchar10 AS PriceApproach ,
			  --Below line commented out LP2P-800
            -- fil.TakenBy,
            j.InvoiceLineJunkUDVarchar21 AS TakenBy ,
            CASE WHEN ct.AccountKey IS NOT NULL THEN 1
                 ELSE 0
            END AS TermsIndicator ,
            i.NDPInd AS NDPIndicator ,
            ISNULL(o.BranchKey, 1) AS BranchKey ,
            j.InvoiceLineJunkUDVarchar34 AS AccountingBrand ,
            j.InvoiceLineJunkUDVarchar36 AS BranchBrand
    FROM    lion.vwFactInvoiceLinePricebandScore2 f ( NOLOCK )
            INNER JOIN lion.vwAccount (NOLOCK) a ON f.AccountKey = a.AccountKey
            INNER JOIN lion.vwAccountGroup1 (NOLOCK) ag1 ON f.AccountGroup1Key = ag1.AccountGroup1Key
            INNER JOIN --below line commented out LP2P-800
		 --   lion.vwFactInvoiceLine (NOLOCK) fil ON f.InvoiceLineUniqueIdentifier = fil.InvoiceLineUniqueIdentifier INNER JOIN
            lion.vwItem (NOLOCK) i ON f.Itemkey = i.ItemKey
            INNER JOIN lion.vwFactInvoiceLineGroup1 ilg1 ( NOLOCK ) ON f.InvoiceLineGroup1Key = ilg1.InvoiceLineGroup1Key
            LEFT OUTER JOIN dbo.DimInvoiceLineJunk (NOLOCK) j ON f.InvoiceLineJunkKey = j.InvoiceLineJunkKey
            LEFT OUTER JOIN ( SELECT    AccountKey ,
                                        ItemKey ,
                                        InvoiceLineGroup1Key
                              FROM      lion.vwFactInvoiceLinePricebandScore(NOLOCK)
                              GROUP BY  AccountKey ,
                                        ItemKey ,
                                        InvoiceLineGroup1Key
                              HAVING    SUM(TotalActualPrice) = 0
                                        AND SUM(TotalActualCost) <> 0
                            ) ex ON f.AccountKey = ex.AccountKey
                                    AND f.ItemKey = ex.ItemKey
                                    AND f.InvoiceLineGroup1Key = ex.InvoiceLineGroup1Key
            LEFT OUTER JOIN dbo.FactLastPriceAndCost (NOLOCK) flpac ON f.AccountKey = flpac.AccountKey
                                                              AND f.ItemKey = flpac.ItemKey
                                                              AND f.InvoiceLineGroup1Key = flpac.InvoiceLineGroup1Key
            LEFT OUTER JOIN lion.vwTermsExistence (NOLOCK) ct ON ct.AccountKey = f.AccountKey
                                                              AND ct.ItemGroup3Key = f.ItemGroup3Key
            LEFT OUTER JOIN lion.NewOrg (NOLOCK) o ON ag1.Branch = o.BranchCode
            CROSS APPLY lion.fn_GetCappedGPP(f.TotalActualCost
                                             / NULLIF(f.TotalQuantity, 0),
                                             f.StretchGPP, .01,
                                             f.UnitListPrice) gm
            CROSS APPLY lion.fn_GetCappedGPP(f.TotalActualCost
                                             / NULLIF(f.TotalQuantity, 0),
                                             f.TargetGPP, .01, f.UnitListPrice) am
            CROSS APPLY lion.fn_GetCappedGPP(f.TotalActualCost
                                             / NULLIF(f.TotalQuantity, 0),
                                             f.FloorGPP, .01, f.UnitListPrice) rm
    WHERE   ex.AccountKey IS NULL
            AND f.PriceDerivation IN ( 'TP', 'LP', 'RP', 'DDC', 'MP', 'ADJ',
                                       'LPC', 'PGR', 'PGA', 'PGG' )
            AND NOT ( i.ItemPyramidCode <> 1
                      OR a.AccountName = 'Unknown'
                      OR i.ItemNumber = 'Unknown'
                      OR i.ItemObsolete = 'Y'
                      OR i.SpecialInd = 'Y'
                      OR ilg1.ShipmentType = 'D'
                      OR f.TotalQuantity = 0
                      OR f.TotalActualPrice = 0
                      OR f.PriceDerivation IN ( 'SPO', 'CCP' )
                      OR a.BranchPrimaryBrand = 'BURDN'
                    ) 
GO
