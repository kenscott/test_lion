SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




CREATE VIEW [lion].[vwNonTermsTransactions]
AS
SELECT   ps.BranchType,
         SQLDate AS TransactionDate, 
	     a.AccountManagerFullName AS AccountOwner,
		 a.AccountNumber,
		 a.AccountName,
         SUBSTRING(i.ItemNumber, 2, 255) AS ProductCode,
		 i.ItemDescription,
		 i.LLSPGCode,
		 i.LLSPGDescription,
		 ilg1.ShipmentType,
		 ilg1.DeviatedIndicator AS ContractClaims,
		 j.InvoiceLineJunkUDVarchar2 + '/' + REPLACE(j.InvoiceLineJunkUDVarchar3, '.00000000', '') AS ChargeNote,
		 ag1.Branch AS SalesBranch,
		 ag1.BranchName AS SalesBranchName,
		 ag1.Network AS SalesNetwork,
		 ag1.NetworkDescription AS SalesNetworkDescription,
		 f.TotalQuantity AS Quantity,
		 f.TotalActualPrice / NULLIF(f.TotalQuantity, 0) AS UnitPrice,
		 f.TotalActualPrice AS Sales,
		 (f.TotalActualPrice - f.TotalActualCost) AS GP,
		 (f.TotalActualPrice - f.TotalActualCost) / NULLIF(f.TotalActualPrice, 0) AS GPP,
		 f.Score AS PriceIndex,
		 CASE WHEN f.TerminalEmulator LIKE '%/F%' THEN 'Flynet' ELSE 'Non-Flynet' END AS TerminalEmulator,
		 f.PriceGuidanceIndicator,
		 CASE WHEN ps.BranchType = 'Pilot' AND dd.SQLDate >= ps.LiveDate 
          THEN  
			CASE WHEN ISNULL(f.TerminalEmulator,'') NOT LIKE  '(%/F)' THEN 'Non Price Guidance'
					  WHEN f.PriceGuidanceIndicator='Y' THEN 'Non Terms'
					  ELSE 'Terms'
			END
				   ELSE
			CASE WHEN f.PriceDerivation IN ( 'RP', 'LP', 'TP', 'DDC', 'MP', 'ADJ', 'PGG', 'PGA', 'PGR', 'LPC')
					  THEN 'Non Terms'
					  ELSE 'Terms'	
			END
		 END AS PriceGroup,
		 f.PriceDerivation,
		 f.GreenPrice,
		 f.AmberPrice,
		 f.RedPrice,
		 f.UnitListPrice AS TradePrice,
		 gm.GPP AS GreenGPP,
		 am.GPP AS AmberGPP,
		 rm.GPP AS RedGPP,
		 (f.UnitListPrice - (f.TotalActualCost/NULLIF(f.TotalQuantity, 0))) / NULLIF(f.UnitListPrice, 0) AS TradeGPP,
		 a.CustomerSegment,
		 flpac.UDVarchar1 AS BaseStrategicItem, 
		 flpac.UDVarchar4 AS BaseSales603010Bucket, 
		 flpac.UDVarchar10 AS PriceApproach,
		 fil.TakenBy,
		 CASE WHEN ct.AccountKey IS NOT NULL THEN 1 ELSE 0 END AS TermsIndicator,
		 i.NDPInd AS NDPIndicator
    FROM lion.vwFactInvoiceLinePricebandScore f (NOLOCK) INNER JOIN
	     lion.vwFactInvoiceLine fil ON f.InvoiceLineUniqueIdentifier = fil.InvoiceLineUniqueIdentifier INNER JOIN
	     dbo.DimDay dd (NOLOCK) ON f.InvoiceDateDayKey = dd.DayKey INNER JOIN
	     lion.vwAccountGroup1 ag1 (NOLOCK) ON f.AccountGroup1Key = ag1.AccountGroup1Key INNER JOIN
		 lion.PilotSchedule ps (NOLOCK) ON ag1.Branch = ps.Branch INNER JOIN
		 lion.vwItem i (NOLOCK) ON f.Itemkey = i.ItemKey INNER JOIN
		 lion.vwAccount a (NOLOCK) ON f.AccountKey = a.AccountKey INNER JOIN
		 lion.vwFactInvoiceLineGroup1 ilg1 (NOLOCK) ON f.InvoiceLineGroup1Key = ilg1.InvoiceLineGroup1Key INNER JOIN
         dbo.DimInvoiceLineJunk j ON f.InvoiceLineJunkKey = j.InvoiceLineJunkKey LEFT OUTER JOIN
                             (SELECT        AccountKey, ItemKey, InvoiceLineGroup1Key
                               FROM            lion.vwFactInvoiceLinePricebandScore(NOLOCK)
                               GROUP BY AccountKey, ItemKey, InvoiceLineGroup1Key
                               HAVING         SUM(TotalActualPrice) = 0 AND SUM(TotalActualCost) <> 0) ex ON f.AccountKey = ex.AccountKey AND f.ItemKey = ex.ItemKey AND f.InvoiceLineGroup1Key = ex.InvoiceLineGroup1Key INNER JOIN
         dbo.FactLastPriceAndCost(NOLOCK) flpac ON f.AccountKey = flpac.AccountKey AND f.ItemKey = flpac.ItemKey AND f.InvoiceLineGroup1Key = flpac.InvoiceLineGroup1Key LEFT OUTER JOIN
	     lion.vwTermsExistence ct ON ct.AccountKey = f.AccountKey AND ct.ItemGroup3Key = f.ItemGroup3Key
		 CROSS APPLY lion.fn_GetCappedGPP(f.TotalActualCost/NULLIF(f.TotalQuantity,0), f.StretchGPP, .01, f.UnitListPrice) gm
		 CROSS APPLY lion.fn_GetCappedGPP(f.TotalActualCost/NULLIF(f.TotalQuantity,0), f.TargetGPP, .01, f.UnitListPrice) am
		 CROSS APPLY lion.fn_GetCappedGPP(f.TotalActualCost/NULLIF(f.TotalQuantity,0), f.FloorGPP, .01, f.UnitListPrice) rm
   WHERE ps.BranchType = 'Pilot' AND
         ex.AccountKey IS NULL AND
         f.PriceDerivation IN ('TP','LP','RP','DDC','MP','ADJ','LPC','PGR','PGA','PGG')  AND NOT (
						 i.ItemPyramidCode <> 1 OR
                         a.AccountName = 'Unknown' OR
                         i.ItemNumber = 'Unknown' OR
                         i.ItemObsolete = 'Y' OR
                         i.SpecialInd = 'Y' OR
                         ilg1.ShipmentType = 'D' OR
                         f.TotalQuantity = 0 OR
                         f.TotalActualPrice = 0 OR
                         f.PriceDerivation IN ('SPO', 'CCP')
						 -- OR
                         --a.BranchPrimaryBrand = 'BURDN'
						  ) 



GO
