SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [lion].[vwLLSPG]
AS
select distinct ItemPyramidCode, LLSPGCode, LLSPGDescription
  from lion.vwItem
GO
