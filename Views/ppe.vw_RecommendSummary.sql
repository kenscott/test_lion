SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE VIEW [ppe].[vw_RecommendSummary] AS

	SELECT 
		RecommendKey, 
		--SUM(TotalAccountSales) Sales, 
		SUM(RecommendedImpact) RecommendedImpact
		--SUM(RecommendedImpact) AcceptedImpact	
	FROM ppe.RecommendOutput 
	WHERE --RowType = 'LLSPG'
	--AND 
	Action = 'Accept'
	GROUP BY RecommendKey






GO
