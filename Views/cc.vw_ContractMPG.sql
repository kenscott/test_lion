SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



	CREATE VIEW [cc].[vw_ContractMPG] AS 
		SELECT 
			NEWID() AS GUID, 
			igx.ContractKey, 	
			dig1.ItemGroup1Key,
			IG1Level1 AS MPGCode,
			IG1Level1UDVarchar1 AS MPGDescription
		FROM cc.SupplierContractItemGroupX igx
		JOIN dbo.DimItemGroup1 dig1 ON dig1.ItemGroup1Key = igx.ItemGroupXKey
		WHERE igx.ItemGroupLevel = 1
		


GO
