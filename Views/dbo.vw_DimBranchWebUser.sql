SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




CREATE View [dbo].[vw_DimBranchWebUser]

AS

SELECT NEWID() AS GUID,PrimaryAccountGroup1Key, WebUserKey, AG1Level1 AS Branch, AG1Level1UDVarchar1 AS BranchName
 from dbo.WebUserDelegate wud
 join dbo.WebUser wu
 on wu.WebUserKey = wud.DelegateWebUserKey
 join dbo.DimPerson dp
 on dp.ODSPersonKey = wu.odspersonkey
 join dbo.DimAccountManager dam
 on dam.DimPersonKey = dp.DimPersonKey
 join dbo.DimAccountGroup1 dag1
 on dag1.AccountGroup1Key = dam.PrimaryAccountGroup1Key

 
 --odsp.ODSPersonKey, odsp.LevelID 
 --case when [AG1Level5] ='Wolseley' then 1
 --else end as ID,


--case when odsp.LevelID = 'BranchManager' then 1 else 0
--end as BranchManagerIndicator

--FROM            dbo.WebUserRole wur INNER JOIN
--                         dbo.WebRole wr ON wur.WebRoleKey = wr.WebRoleKey INNER JOIN
--                         dbo.WebUser wu ON wur.WebUserKey = wu.WebUserKey CROSS JOIN
--                         dbo.DimAccount da INNER JOIN
--                         dbo.DimAccountGroup1 dag1 ON da.AccountGroup1Key = dag1.AccountGroup1Key


 --[dbo].[DimAccount] da Left Join [dbo].[DimAccountGroup1] dag1 
	--on da.AccountGroup1Key = dag1.AccountGroup1Key

--[dbo].[WebUser] wu join [dbo].[ODSPerson] odsp
--	on wu.ODSPersonKey = odsp.ODSPersonKey
--	left join 
--	on wu.[AccountManagerKey] = 



GO
