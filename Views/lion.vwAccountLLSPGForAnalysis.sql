SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO







CREATE 
VIEW [lion].[vwAccountLLSPGForAnalysis] 

/*
SELECT TOP 10 * FROM lion.vwAccountLLSPGForAnalysis 
*/


AS

SELECT
	DISTINCT
	--faig3.AccountKey,
	--faig3.ItemGroup3Key,
	AccountNumber,
	LLSPGCode,
	faig3.ContractClaimsPercent,
	Branch,
	AccountManagerKey,
	AccountManagerCode,
	AccountManagerFullName,
	AccountManagerEmail,
	AccountClientUniqueIdentifier,
	--AccountNumber,
	AccountName,
	AccountNumberParent,
	AccountAddress1,
	AccountAddress2,
	AccountAddress3,
	AccountAddress4,
	AccountPostCode,
	CreditLimit,
	PaymentTermName,
	IntercompanyAccount,
	OverLimitStatus,
	OverdueStatus,
	TermsParent,
	PricingCategory,
	AccountOwnerType,
	AccountOwnerBucket,
	CustomerSegment,
	CustomerRebateIndicator,
	FixedPriceIndicator,
	ContractName,
	DateOpened,
	DateClosed,
	Inactive,
	BranchCode,
	Network,
	Area,
	Region,
	BranchName,
	NetworkDescription,
	AreaDescription,
	RegionDescription,
	PlaybookRegion,
	PlaybookRegionDescription,
	PlaybookArea,
	PlaybookAreaDescription,
	BranchPrimaryBrand,
	BranchTradingStatus,
	BranchType,
	BranchAddressLine1,
	BranchAddressLine2,
	BranchAddressLine3,
	BranchAddressLine4,
	BranchAddressLine5,
	BranchPostcode,
	BranchEmail,
	BranchTelephone,
	CustomerBranchPostCodeArea,
	CustomerBranchPostCodeDistrict,
	BranchPyramidCode,
	BrandGroup,
	PGActiveIndicator,
	Manager,
	BranchManagerEmail,
	RegionExceptionIndicator,
	BusinessUnitRegion,
	CustomerType,
	CustomerTypeDescription,
	TrandingStatusCategory,
	TrandingStatusCategoryDescription,
	AccountOwnersBrand,
	--LLSPGCode,
	HLSPGCode,
	GSPGCode,
	PYRCode,
	LLSPGDescription,
	HLSPGDescription,
	GSPGDescription
FROM dbo.FactAccountItemGroup3 faig3 WITH (NOLOCK)
INNER JOIN lion.vwAccount da (NOLOCK)
	ON da.AccountKey = faig3.AccountKey
INNER JOIN lion.vwItemGroup3 dag3 (NOLOCK)
	ON dag3.ItemGroup3Key = faig3.ItemGroup3Key



GO
