SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO








/*

--select * from cc.vw_ContractOverview

select top 10000 updatestatuskey, * from cc.SupplierContract where statuskey = 4

select * from cc.Suppliercontractstatus

select c.statuskey, cs.statusdescription, count(*) as ContractCount 
	from cc.SupplierContract c 
	join cc.Suppliercontractstatus cs 
		on cs.statuskey = c.statuskey
	group by c.statuskey, cs.statusdescription order by c.statuskey


*/

CREATE VIEW [cc].[vw_ContractOverview] AS

	SELECT NEWID() AS GUID, ContractStatus, sum(Contracts) AS ContractCount, MIN(Ord) AS Ord
	FROM (SELECT 
				CASE 
					WHEN ExpirationDate < DATEADD(d,-1,GETDATE()) THEN 'Expired'
					WHEN DATEADD(d,-30,ExpirationDate) <= GETDATE() THEN 'Approved - Expires within 30 Days'
					WHEN ExpirationDate >= DATEADD(d,-1,GETDATE()) THEN 'Approved - Active'
					ELSE NULL
				END AS ContractStatus,
				COUNT(*) AS Contracts,
				MIN(CASE 
					WHEN ExpirationDate < DATEADD(d,-1,GETDATE()) THEN 5 --'Expired'
					WHEN DATEADD(d,-30,ExpirationDate) <= GETDATE() THEN 4 --'Approved - Expires within 30 Days'
					WHEN ExpirationDate >= DATEADD(d,-1,GETDATE()) THEN 3 --'Approved - Active'
					ELSE NULL
				END) AS Ord
			FROM cc.SupplierContract
			WHERE StatusKey = 6 AND 
				  ( UpdateStatusKey NOT IN (2, 3) OR UpdateStatusKey IS NULL )
				  AND ExpirationDate > CAST('12-31-2014' AS DATE)
			GROUP BY 
				CASE 
					WHEN ExpirationDate < DATEADD(d,-1,GETDATE()) THEN 'Expired'
					WHEN DATEADD(d,-30,ExpirationDate) <= GETDATE() THEN 'Approved - Expires within 30 Days'
					WHEN ExpirationDate >= DATEADD(d,-1,GETDATE()) THEN 'Approved - Active'
					ELSE NULL
				END
			UNION
			SELECT 'Pending' AS ContractStatus, 
				COUNT(*) AS Contracts, 
				2 AS Ord
			FROM cc.SupplierContract
			WHERE StatusKey = 4 
				AND ( UpdateStatusKey NOT IN (2, 3) OR UpdateStatusKey IS NULL )
				AND ExpirationDate > CAST('12-31-2014' AS DATE)
			UNION
			SELECT 'Rejected' AS ContractStatus, 
				COUNT(*) AS Contracts, 
				6 AS Ord
			FROM cc.SupplierContract
			WHERE StatusKey = 5 
				AND ( UpdateStatusKey NOT IN (2, 3) OR UpdateStatusKey IS NULL )
				AND ExpirationDate > CAST('12-31-2014' AS DATE)
			UNION
			SELECT 'Closed' AS ContractStatus, 
				COUNT(*) AS Contracts, 
				7 AS Ord
			FROM cc.SupplierContract
			WHERE StatusKey = 3
				AND ( UpdateStatusKey NOT IN (2, 3) OR UpdateStatusKey IS NULL )
				AND ExpirationDate > CAST('12-31-2014' AS DATE)
			UNION
			SELECT 'Draft' AS ContractStatus, 
				COUNT(*) AS Contracts, 
				8 AS Ord
			FROM cc.SupplierContract
			WHERE StatusKey = 2
				AND ( UpdateStatusKey NOT IN (2, 3) OR UpdateStatusKey IS NULL )
				AND ExpirationDate > CAST('12-31-2014' AS DATE)
				
			UNION
			SELECT 'Processing' AS ContractStatus, 
				COUNT(*) AS Contracts, 
				9 AS Ord
			FROM cc.SupplierContract
			WHERE UpdateStatusKey = 2
				AND ExpirationDate > CAST('12-31-2014' AS DATE)
			UNION
			SELECT 'Failed' AS ContractStatus, 
				COUNT(*) AS Contracts, 
				10 AS Ord
			FROM cc.SupplierContract
			WHERE UpdateStatusKey = 3
				AND ExpirationDate > CAST('12-31-2014' AS DATE)
	) x
	GROUP BY ContractStatus











GO
