SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


/*

select newspgfromdate, newexceptionstartdate, * from ppe.vw_requestmain where requestkey = 330176



*/




--SELECT TOP 25 * FROM [ppe].[vw_RequestMain] WHERE ViewerWebUserKey = 1  --ItemKey = 1

CREATE  VIEW [ppe].[vw_RequestMain]
AS
    SELECT  
			ISNULL(wu3.UserName, 'coordinatorUser') AS [Owner] ,
            da.AccountNumber ,
            da.AccountName ,
            ig3.IG3Level1 AS SPGCode ,
            ig3.IG3Level1UDVarchar1 AS SPGDescription ,
            CASE WHEN r.ItemKey <> 1 THEN SUBSTRING(di.ItemNumber, 2, 255)
                 ELSE NULL
            END AS ProductCode ,
            di.ItemDescription AS ProductDescription ,
			gds.Discount AS [GreenDiscount],
			--CASE WHEN ItemKey = 1 THEN NULL ELSE ( 1 - ( [GreenPrice] / NULLIF(di.ItemUDDecimal1,0) ) ) END AS [GreenDiscount],
            [GreenPrice] ,
			ads.Discount AS [AmberDiscount] ,
			--CASE WHEN ItemKey = 1 THEN NULL ELSE ( 1 - ( [AmberPrice] / NULLIF(di.ItemUDDecimal1,0) ) ) END AS AmberDiscount,
            [AmberPrice] ,
			rds.Discount AS [RedDiscount] ,
 			--CASE WHEN ItemKey = 1 THEN NULL ELSE ( 1 - ( [RedPrice] / NULLIF(di.ItemUDDecimal1,0) ) ) END AS RedDiscount,
           [RedPrice] ,
			ISNULL(ct.Term,'') AS CurrentTerm ,
            ctt.TermType AS CurrentTermType ,
			csd.Discount AS CurrentDiscount ,
            cp.Price AS CurrentPrice ,
			cpg.PG AS CurrentPG ,
			rt.Term AS RecommendedTerm ,
            rtt.TermType AS RecommendedTermType ,
			rsd.Discount AS RecommendedDiscount ,
            rp.Price AS RecommendedPrice ,
            [RecommendedImpact] ,
			-- changed below from currentdiscount to lastdiscount per LPF-1947
            CASE WHEN r.itemkey <> 1
				THEN ABS([LastItemDiscount]) - ABS(ncd.Discount) 
				ELSE ABS(ISNULL(r.CurrentCompoundDiscount, NULL /*(SELECT 1.0-SPGLastDiscount FROM ppe.fn_GetDiscountSPG(r.AccountKey,r.ItemGroup3Key))*/ )) - ABS(ncd.Discount)  
			END AS ChangePercent , -- per LPF-1846  old version: ISNULL( (nsd.Discount-csd.Discount) / NULLIF(csd.Discount,0),0) 
            nt.Term AS NewTerm ,
            --ntt.TermType AS NewTermType ,
            r.NewTermType,
            CASE WHEN r.itemkey <> 1
				THEN NULL
				ELSE nsd.Discount
			END AS NewDiscount1,
            CASE WHEN r.itemkey <> 1
				THEN NULL
				ELSE r.NewSPGDiscount2
			END AS NewDiscount2,
            ncd.Discount AS NewCompoundDiscount ,
			r.NewSPGFromDate AS NewSPGFromDate ,
            np.Price AS NewPrice ,
            r.NewExceptionStartDate AS NewFromDate ,
            r.NewExceptionExpiryDate AS NewToDate ,
            ( np.Price - r.CurrentCost ) / NULLIF(np.Price, 0) AS NewMargin ,
            npg.PG AS NewPG ,
			ndi.DiscountIndex AS NewPriceIndex,
            r.AcceptedImpact AS NewImpact ,
            [Total12MonthSales] AS TotalSales ,
            ilg1.InvoiceLineGroup1UDVarchar2 AS ContractClaimsIndicator ,
            [StrategicItemFlag] AS StrategicItemIndicator ,
            (SELECT COUNT(*) 
				FROM lion.customerterms ct 
				--JOIN dbo.DimItem di ON di.ItemKey = ct.ItemKey
				WHERE ct.AccountKey =r.AccountKey  
				AND ct.ItemGroup3Key = r.ItemGroup3Key
				AND ct.ItemKey <> 1  ) AS SPGExceptions ,
			r.CreationDate,
			wd.DelegateWebUserKey AS ViewerWebUserKey,--  wuv.ViewerWebUserKey,
			r.AccountKey,
			r.ItemKey,
			r.InvoiceLineGroup1Key,
			-- ***** End of JIRA-requested columns.  Leaving additional columns below until we determine which ones are needed *****

            r.RequestKey ,
            ilg1.InvoiceLineGroup1UDVarchar1 ,
            rqt.TypeName AS RequestType ,
            rqt.TypeKey ,
            r.[StatusKey] ,
            rs.StatusName ,
			rs.StatusRollup,
            r.CreatedByWebUserKey ,
            wu1.UserName AS CreatedUserName ,
            r.ModifiedByWebUserKey ,
			r.ModificationDate ,
            wu2.UserName AS ChangedUserName ,
            [DefaultDiscount] ,
            CASE WHEN r.itemkey <> 1
				THEN NULL
				ELSE [CurrentSPGDiscount1]
			END AS CurrentSPGDiscount1,
            CASE WHEN r.itemkey <> 1
				THEN NULL
				ELSE [CurrentSPGDiscount2]
			END AS CurrentSPGDiscount2,
            [CurrentCompoundDiscount] ,
            CASE WHEN r.itemkey <> 1
				THEN NULL
				ELSE [ProposedSPGDiscount1]
			END AS ProposedSPGDiscount1,
            CASE WHEN r.itemkey <> 1
				THEN NULL
				ELSE [ProposedSPGDiscount2]
			END AS ProposedSPGDiscount2,
            ISNULL([ProposedCompoundDiscount], 0) AS [ProposedCompoundDiscount] ,
            [ProposedExceptionDiscount] ,
            [ProposedExceptionFixedPrice] ,
            [ProposedExceptionStartDate] ,
            [ProposedExceptionExpiryDate] ,
            [OwningBrandCode] ,
            [LastChangeInitials] ,
            [LastChangeDateTime],
            [LastChangeBranch] ,
            [FixedPriceFlag] ,
            [KVIFlag] ,
            [KVIOverrideFlag] ,
            [LastSaleDate] ,
            [LastPriceDerivation] ,
            [LastItemDiscount] ,
            [LastItemPrice] ,
            [LastItemGPP] ,
            [AdjustedLastItemPrice] ,
            [AdjustedLastItemGPP] ,
            [RecommendedPriceLogic] ,
            [TradePrice] ,
            [GreenGPP] ,
            [AmberGPP] ,
            [RedGPP] ,
            [TradeGPP] ,
            [LastPriceLevel] ,
            [SalesSize] ,
            [PriceApproach] ,
            [PriceBandLevel] ,
            r.[CurrentCost] ,
            [Total12MonthQty] ,
            [AcceptedImpact] ,
            [CurrentExceptionDiscount] ,
            [CurrentExceptionFixedPrice] ,
            [CurrentExceptionStartDate] ,
            [CurrentExceptionExpiryDate] ,
            [NewExceptionDiscount] ,
            [NewExceptionFixedPrice] ,
            [NewExceptionStartDate] ,
            [NewExceptionExpiryDate] ,
            r.[Version],
			ISNULL(rn.NoteCount, 0) AS NoteCount,
			MAX(CASE 
				WHEN wd.DelegatorWebUserKey = wd.DelegateWebUserKey THEN 'O'
				ELSE 'D'
			END) AS OwnerOrDelegate,
			ra.ActionAbbr AS LastAction,
			r.LastActionKey,
			r.NewDeleteTerm,
			r.ProposedDeleteTerm,
			r.OwnerWebUserKey,
			wd.DelegatorWebUserKey,
			COALESCE(r.NewExceptionFixedPrice, -r.NewExceptionDiscount, -r.NewCompoundDiscount, 0) AS RawNewTerm,
			r.PriorOwnerWebUserKey,
			wu4.WebUserKey AS AccountOwnerWebUserKey,
			od6.FullName AS AccountOwnerFullName,
			od5.FullName AS RequestOwnerFullName,
			dag1.AG1Level1 AS BranchCode,
			dag1.AG1Level1UDVarchar1 AS BranchName,
			reqsource.SourceName AS RequestSource,
			ISNULL(wu4.WebUserKey, wu6.WebUserKey) AS AccountBrandOwnerWebUserKey,
			ISNULL(od4.FullName, od6.FullName) AS AccountBrandOwnerFullName,
			ig3.ig3level5 AS ProdBrandAtLLSPG,
			CASE WHEN r.ItemKey <> 1 THEN ISNULL(NewExceptionStartDate, NewSPGFromDate)			----LPF-6366: Create Recommendations | Effective Date not being applied to products
                 ELSE NewSPGFromDate
            END AS EffectiveDate
            --SELECT TOP 100 *
    FROM    [ppe].[RequestMain] r
            JOIN dbo.DimAccount da ON da.AccountKey = r.AccountKey
			JOIN dbo.DimAccountGroup1 dag1 ON dag1.AccountGroup1Key = da.AccountGroup1Key
           -- JOIN dbo.DimAccountManager dam ON dam.AccountManagerKey = da.AccountManagerKey
            JOIN dbo.DimItem di ON di.ItemKey = r.ItemKey
            JOIN dbo.WebUser wu1 ON wu1.WebUserKey = r.[CreatedByWebUserKey]
            JOIN dbo.WebUser wu2 ON wu2.WebUserKey = r.ModifiedByWebUserKey
            JOIN ppe.RequestStatus rs ON rs.StatusKey = r.StatusKey
            JOIN ppe.RequestType rqt ON rqt.TypeKey = r.TypeKey
            JOIN dbo.DimInvoiceLineGroup1 ilg1 ON ilg1.InvoiceLineGroup1Key = r.InvoiceLineGroup1Key
            JOIN dbo.DimItemGroup3 ig3 ON ig3.ItemGroup3Key = r.ItemGroup3Key
			JOIN dbo.WebUser wu3 ON wu3.WebUserKey= r.OwnerWebUserKey
			JOIN dbo.WebUserDelegate wd
				ON wd.DelegatorWebUserKey = r.OwnerWebUserKey --wu3.WebUserKey   -- this connects the delegators accounts
				AND GETDATE() BETWEEN wd.StartDate AND ISNULL(wd.EndDate,'12-31-9999')
			-----Get Account Brand Owner
			--JOIN (SELECT di.ItemGroup3Key, MAX(di.ItemUDVarChar2 ) ProdBrandAtLLSPG FROM dbo.DimItem di GROUP BY di.ItemGroup3Key) pb ON pb.ItemGroup3Key = r.ItemGroup3Key
			JOIN dbo.DimBrand db ON db.Brand = ig3.IG3Level5
			LEFT JOIN dbo.DimAccountBrandManager dabm
				ON dabm.AccountKey = da.AccountKey
				AND dabm.BrandKey = db.BrandKey
			LEFT JOIN dbo.WebUser wu4 ON wu4.AccountManagerKey = dabm.AccountManagerKey
			LEFT JOIN dbo.ODSPerson od4 ON od4.ODSPersonKey = wu4.ODSPersonKey
			JOIN dbo.WebUser wu6 ON wu6.AccountManagerKey = da.AccountManagerKey
			JOIN dbo.ODSPerson od6 ON od6.ODSPersonKey = wu6.ODSPersonKey
			JOIN dbo.ODSPerson od5 ON od5.ODSPersonKey = wu3.ODSPersonKey

			--JOIN dbo.WebUserView wuv ON wuv.ViewedWebUserKey = wd.DelegateWebUserKey
			LEFT JOIN ppe.RequestAction ra ON ra.ActionKey = r.LastActionKey
			LEFT JOIN (SELECT RequestKey, COUNT(*) AS NoteCount FROM ppe.RequestNote GROUP BY RequestKey) rn ON rn.RequestKey = r.RequestKey
			CROSS APPLY ppe.fn_GetPrice(r.CurrentExceptionFixedPrice, di.ItemUDDecimal1, r.CurrentExceptionDiscount, ISNULL(r.CurrentCompoundDiscount, 0)) cp
			CROSS APPLY ppe.fn_GetPrice(r.ProposedExceptionFixedPrice, di.ItemUDDecimal1, r.ProposedExceptionDiscount, ISNULL(r.ProposedCompoundDiscount, 0)) rp
			CROSS APPLY ppe.fn_GetPrice(r.NewExceptionFixedPrice, di.ItemUDDecimal1, r.NewExceptionDiscount, ISNULL(r.NewCompoundDiscount, 0)) np
	        --CROSS APPLY ppe.fn_GetDiscount(r.AccountKey, r.ItemGroup3Key, r.ItemKey, r.GreenPrice, di.ItemUDDecimal1, 'G') gd
	        --CROSS APPLY ppe.fn_GetDiscount(r.AccountKey, r.ItemGroup3Key, r.ItemKey, r.AmberPrice, di.ItemUDDecimal1, 'A') ad
	        --CROSS APPLY ppe.fn_GetDiscount(r.AccountKey, r.ItemGroup3Key, r.ItemKey, r.RedPrice, di.ItemUDDecimal1, 'R') rd
	        CROSS APPLY ppe.fn_GetSimpleDiscount( r.GreenPrice, di.ItemUDDecimal1, NULL, NULL ) gds
	        CROSS APPLY ppe.fn_GetSimpleDiscount( r.AmberPrice, di.ItemUDDecimal1, NULL, NULL) ads
	        CROSS APPLY ppe.fn_GetSimpleDiscount( r.RedPrice, di.ItemUDDecimal1, NULL, NULL) rds
			CROSS APPLY ppe.fn_GetTerm(r.CurrentExceptionFixedPrice, r.CurrentExceptionDiscount, r.CurrentCompoundDiscount, CASE WHEN r.CurrentExceptionFixedPrice IS NULL THEN 'TD' ELSE 'FP' END) ct
			CROSS APPLY ppe.fn_GetTerm(r.ProposedExceptionFixedPrice, r.ProposedExceptionDiscount, ISNULL(r.ProposedCompoundDiscount, 0), r.NewTermType) rt
			CROSS APPLY ppe.fn_GetNewTerm(r.NewTermType, r.NewExceptionFixedPrice, r.NewExceptionDiscount, ISNULL(r.NewCompoundDiscount, 0)) nt
			CROSS APPLY ppe.fn_GetTermType(r.CurrentExceptionFixedPrice) ctt
			CROSS APPLY ppe.fn_GetTermType(r.ProposedExceptionFixedPrice) rtt
			--CROSS APPLY ppe.fn_GetTermType(r.NewExceptionFixedPrice) ntt
			CROSS APPLY ppe.fn_GetSimpleDiscount(r.CurrentExceptionFixedPrice, di.ItemUDDecimal1, r.CurrentExceptionDiscount, ISNULL(r.CurrentCompoundDiscount, 0)) csd
			CROSS APPLY ppe.fn_GetSimpleDiscount(r.ProposedExceptionFixedPrice, di.ItemUDDecimal1, r.ProposedExceptionDiscount, ISNULL(r.ProposedCompoundDiscount, 0)) rsd
			CROSS APPLY ppe.fn_GetSimpleDiscount(r.NewExceptionFixedPrice, di.ItemUDDecimal1, r.NewExceptionDiscount, r.NewSPGDiscount1) nsd
			CROSS APPLY ppe.fn_GetSimpleDiscount(r.NewExceptionFixedPrice, di.ItemUDDecimal1, r.NewExceptionDiscount, ISNULL(r.NewCompoundDiscount, 0)) ncd
			CROSS APPLY ppe.fn_GetDiscountIndex(rds.Discount,ads.Discount,gds.Discount,ncd.Discount) ndi
			CROSS APPLY ppe.fn_GetPG(csd.Discount, ads.Discount, gds.Discount) cpg
			CROSS APPLY ppe.fn_GetPG(ncd.Discount, ads.Discount, gds.Discount) npg
			--CROSS APPLY ppe.fn_GetDiscountSPG(r.AccountKey,r.ItemGroup3Key) spglast
			LEFT JOIN ppe.RequestSource reqsource ON reqsource.SourceKey = r.SourceKey
--WHERE ( OwnerWebUserKey = ViewerWebUserKey OR OwnerWebUserKey = DelegatorWebUserKey)
GROUP BY
ISNULL(wu3.UserName, 'coordinatorUser') ,
            da.AccountNumber ,
            da.AccountName ,
            ig3.IG3Level1 ,
            ig3.IG3Level1UDVarchar1 ,
            CASE WHEN r.ItemKey <> 1 THEN SUBSTRING(di.ItemNumber, 2, 255)
                 ELSE NULL
            END ,
            di.ItemDescription,
			gds.Discount ,
            [GreenPrice] ,
			ads.Discount,
            [AmberPrice] ,
			rds.Discount,
            [RedPrice] ,
			ct.Term ,
            ctt.TermType ,
			csd.Discount ,
            cp.Price ,
			cpg.PG ,
			rt.Term ,
            rtt.TermType,
			rsd.Discount,
            rp.Price ,
            [RecommendedImpact] ,
            CASE WHEN r.itemkey <> 1
				THEN ABS([LastItemDiscount]) - ABS(nsd.Discount) 
				ELSE ABS(ISNULL(r.CurrentCompoundDiscount,0/*spglast.SPGLastDiscount*/)) - ABS(nsd.Discount)  
			END,
            nt.Term ,
            --ntt.TermType AS NewTermType ,
            r.NewTermType,
            nsd.Discount,
            r.NewSPGDiscount2 ,
            ncd.Discount,
			r.NewSPGFromDate ,
            np.Price ,
            r.NewExceptionStartDate ,
            r.NewExceptionExpiryDate,
            ( np.Price - r.CurrentCost ) / NULLIF(np.Price, 0) ,
            npg.PG ,
            --npi.PriceIndex ,
			ndi.DiscountIndex,
            r.AcceptedImpact ,
            [Total12MonthSales] ,
            ilg1.InvoiceLineGroup1UDVarchar2,
            [StrategicItemFlag],  
			r.CreationDate,
			wd.DelegateWebUserKey, --wuv.ViewerWebUserKey,
			r.AccountKey,
			r.ItemKey,
			r.InvoiceLineGroup1Key,
			-- ***** End of JIRA-requested columns.  Leaving additional columns below until we determine which ones are needed *****

            r.RequestKey ,
            ilg1.InvoiceLineGroup1UDVarchar1 ,
            rqt.TypeName,
            rqt.TypeKey ,
            r.[StatusKey] ,
            rs.StatusName ,
			rs.StatusRollup,
            r.CreatedByWebUserKey ,
            wu1.UserName,
            r.ModifiedByWebUserKey ,
			r.ModificationDate ,
            wu2.UserName,
            [DefaultDiscount] ,
            [CurrentSPGDiscount1] ,
            [CurrentSPGDiscount2] ,
            [CurrentCompoundDiscount] ,
            [ProposedSPGDiscount1] ,
            [ProposedSPGDiscount2] ,
            ISNULL([ProposedCompoundDiscount], 0) ,
            [ProposedExceptionDiscount] ,
            [ProposedExceptionFixedPrice] ,
            [ProposedExceptionStartDate] ,
            [ProposedExceptionExpiryDate] ,
            [OwningBrandCode] ,
            [LastChangeInitials] ,
            [LastChangeDateTime] ,
            [LastChangeBranch] ,
            [FixedPriceFlag] ,
            [KVIFlag] ,
            [KVIOverrideFlag] ,
            [LastSaleDate] ,
            [LastPriceDerivation] ,
            [LastItemDiscount] ,
            [LastItemPrice] ,
            [LastItemGPP] ,
            [AdjustedLastItemPrice] ,
            [AdjustedLastItemGPP] ,
            [RecommendedPriceLogic] ,
            [TradePrice] ,
            [GreenGPP] ,
            [AmberGPP] ,
            [RedGPP] ,
            [TradeGPP] ,
            [LastPriceLevel] ,
            [SalesSize] ,
            [PriceApproach] ,
            [PriceBandLevel] ,
            r.[CurrentCost] ,
            [Total12MonthQty] ,
            [AcceptedImpact] ,
            [CurrentExceptionDiscount] ,
            [CurrentExceptionFixedPrice] ,
            [CurrentExceptionStartDate] ,
            [CurrentExceptionExpiryDate] ,
            [NewExceptionDiscount] ,
            [NewExceptionFixedPrice] ,
            [NewExceptionStartDate] ,
            [NewExceptionExpiryDate] ,
            r.[Version],
			ISNULL(rn.NoteCount, 0) ,
			ra.ActionAbbr,
			r.LastActionKey,
			r.NewDeleteTerm,
			r.ProposedDeleteTerm,
			r.ItemGroup3Key,
			r.OwnerWebUserKey,
			wd.DelegatorWebUserKey,
			COALESCE(r.NewExceptionFixedPrice, -r.NewExceptionDiscount, -r.NewCompoundDiscount, 0) ,
			r.PriorOwnerWebUserKey,
			wu4.WebUserKey,
			od6.FullName,
			od5.FullName,
			dag1.AG1Level1,
			dag1.AG1Level1UDVarchar1,
			reqsource.SourceName,
			ISNULL(wu4.WebUserKey, wu6.WebUserKey),
			ISNULL(od4.FullName, od6.FullName),
			ig3.ig3level5
			
			
			
		



























GO
