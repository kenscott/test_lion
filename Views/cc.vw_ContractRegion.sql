SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE VIEW [cc].[vw_ContractRegion] AS 
	SELECT 	NEWID() AS GUID, 
			scl.ContractKey, 
			Region,
			RegionName AS  RegionDescription
	FROM cc.SupplierContractLocation scl
	JOIN dbo.DimRegion dr ON dr.RegionKey = scl.LocationKey
	WHERE scl.ContractLocationLevelKey=4 --4=region
	

GO
