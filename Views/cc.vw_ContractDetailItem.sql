SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE
VIEW [cc].[vw_ContractDetailItem] 
AS

/*

select top 1000 * from cc.vw_ContractDetailItem where LimitValue is not null
 
*/

/*
LSP-70, Add columns to CC ContractDetailItem view for previously claimed quantity and currently claimed quantity
*/

	SELECT 
		NEWID() AS GUID, 
		sci.ContractKey, 
		di.ItemKey, 
		di.ItemUDVarchar20 AS ProductCode, 
		dig1.IG1Level1 AS MPG,
		dig3.IG3Level1 AS SPG,
		di.VendorStockNumber, 
		sci.ClaimBackAmount, 
		sci.ClaimBackPerc, 
		sci.ClaimCost, 
		sci.ClaimType,
		sci.LimitType,
		sci.LimitValue, 
		ISNULL(sci.CurrentMonthTally, 0) + ISNULL(sci.PreviousMonthTally, 0) AS CurrentMonthTally, 
		RedClaimBackPercent AS Red,
		AmberClaimBackPercent AS Amber,
		GreenClaimBackPercent AS Green,
		RedClaimBackAmount AS RedClaimBack,
		AmberClaimBackAmount AS AmberClaimBack,
		GreenClaimBackAmount AS GreenClaimBack,
		sci.StartDT,
		sci.ExpiryDT,
		di.ItemDescription,
		dig1.IG1Level1UDVarchar1 AS MPGDescription,
		dig3.IG3Level1UDVarchar1 AS SPGDescription,
		NULL AS ItemGroupXKey,
		NULL AS ItemGroupLevel,
		sci.ContractItemKey,
		NULL AS ContractItemGroupXKey,
		wu1.UserName AS CreatedByUserName,
		sci.CreatedOnDT,
		wu2.UserName AS ModifiedByUserName,
		sci.ModifiedOnDT,
		di.CurrentInvoiceCost,	-- LPF-4246 Add invoice cost to vw_ContractDetailItem
		ISNULL(sci.CurrentMonthTally, 0) + ISNULL(sci.PreviousMonthTally, 0) AS CombinedTally,  --LSP-70
		ISNULL(sci.CurrentMonthTally, 0) AS CurrentTally,                                       --LSP-70
		ISNULL(sci.PreviousMonthTally, 0) AS PreviousTally                                      --LSP-70
	FROM cc.SupplierContractItem sci 
	JOIN dbo.DimItem di ON di.ItemKey = sci.ItemKey
	JOIN dbo.DimItemGroup1 dig1 ON dig1.ItemGroup1Key = di.ItemGroup1Key
	JOIN dbo.DimItemGroup3 dig3 ON dig3.ItemGroup3Key = di.ItemGroup3Key
	LEFT JOIN dbo.WebUser wu1 ON wu1.WebUserKey = sci.CreatedByWebUserKey
	LEFT JOIN dbo.WebUser wu2 ON wu2.WebUserKey = sci.ModifiedByWebUserKey


	UNION ALL

	SELECT 
		NEWID() AS GUID, 
		igx.ContractKey, 
		1 AS ItemKey, 
		'' AS ProductCode, 
		dig1.IG1Level1 AS MPG,
		'' AS SPG,
		'' AS VendorStockNumber, 
		igx.ClaimBackAmount, 
		igx.ClaimBackPerc, 
		igx.ClaimCost, 
		igx.ClaimType,
		igx.LimitType,
		igx.LimitValue, 
		ISNULL(igx.CurrentMonthTally, 0) + ISNULL(igx.PreviousMonthTally, 0) AS CurrentMonthTally, 
		RedClaimBackPercent AS Red,
		AmberClaimBackPercent AS Amber,
		GreenClaimBackPercent AS Green,
		RedClaimBackAmount AS RedClaimBack,
		AmberClaimBackAmount AS AmberClaimBack,
		GreenClaimBackAmount AS GreenClaimBack,
		igx.StartDT,
		igx.ExpiryDT,
		'',
		dig1.IG1Level1UDVarchar1 AS MPGDescription,
		'' AS SPGDescription,
		igx.ItemGroupXKey,
		igx.ItemGroupLevel,
		NULL AS ContractItemKey,
		igx.ContractItemGroupXKey,
		wu1.UserName AS CreatedByUserName,
		igx.CreatedOnDT,
		wu2.UserName AS ModifiedByUserName,
		igx.ModifiedOnDT,
		NULL CurrentInvoiceCost,	-- LPF-4246 Add invoice cost to vw_ContractDetailItem
		ISNULL(igx.CurrentMonthTally, 0) + ISNULL(igx.PreviousMonthTally, 0) AS CombinedTally,  --LSP-70
		ISNULL(igx.CurrentMonthTally, 0) AS CurrentTally,                                       --LSP-70
		ISNULL(igx.PreviousMonthTally, 0) AS PreviousTally                                      --LSP-70
	FROM cc.SupplierContractItemGroupX igx
	JOIN dbo.DimItemGroup1 dig1 ON dig1.ItemGroup1Key = igx.ItemGroupXKey
	LEFT JOIN dbo.WebUser wu1 ON wu1.WebUserKey = igx.CreatedByWebUserKey
	LEFT JOIN dbo.WebUser wu2 ON wu2.WebUserKey = igx.ModifiedByWebUserKey

	WHERE igx.ItemGroupLevel = 1   ---MPG
	

	UNION ALL

	SELECT 
		NEWID() AS GUID, 
		igx.ContractKey, 
		1 AS ItemKey, 
		'' AS ProductCode, 
		'' AS MPG,
		dig3.IG3Level1 AS SPG,
		'' AS VendorStockNumber, 
		igx.ClaimBackAmount, 
		igx.ClaimBackPerc, 
		igx.ClaimCost, 
		igx.ClaimType,
		igx.LimitType,
		igx.LimitValue, 
		ISNULL(igx.CurrentMonthTally, 0) + ISNULL(igx.PreviousMonthTally, 0) AS CurrentMonthTally,  
		RedClaimBackPercent AS Red,
		AmberClaimBackPercent AS Amber,
		GreenClaimBackPercent AS Green,
		RedClaimBackAmount AS RedClaimBack,
		AmberClaimBackAmount AS AmberClaimBack,
		GreenClaimBackAmount AS GreenClaimBack,
		igx.StartDT,
		igx.ExpiryDT,
		'' ItemDescription,
		'' AS MPGDescription,
		dig3.IG3Level1UDVarchar1 AS SPGDescription,
		igx.ItemGroupXKey,
		igx.ItemGroupLevel,
		NULL AS ContractItemKey,
		igx.ContractItemGroupXKey,
		wu1.UserName AS CreatedByUserName,
		igx.CreatedOnDT,
		wu2.UserName AS ModifiedByUserName,
		igx.ModifiedOnDT,
		NULL CurrentInvoiceCost,	-- LPF-4246 Add invoice cost to vw_ContractDetailItem
		ISNULL(igx.CurrentMonthTally, 0) + ISNULL(igx.PreviousMonthTally, 0) AS CombinedTally,  --LSP-70
		ISNULL(igx.CurrentMonthTally, 0) AS CurrentTally,                                       --LSP-70
		ISNULL(igx.PreviousMonthTally, 0) AS PreviousTally                                      --LSP-70
	FROM cc.SupplierContractItemGroupX igx
	JOIN dbo.DimItemGroup3 dig3 ON dig3.ItemGroup3Key = igx.ItemGroupXKey
	LEFT JOIN dbo.WebUser wu1 ON wu1.WebUserKey = igx.CreatedByWebUserKey
	LEFT JOIN dbo.WebUser wu2 ON wu2.WebUserKey = igx.ModifiedByWebUserKey

	WHERE igx.ItemGroupLevel = 3   ---SPG
	
GO
