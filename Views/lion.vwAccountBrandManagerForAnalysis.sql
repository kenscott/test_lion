SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE VIEW [lion].[vwAccountBrandManagerForAnalysis]
AS

/*

SELECT * 
FROM lion.vwAccountBrandManagerForAnalysis
WHERE
	AccountNumber IN ('7800G94')
ORDER BY 1,2,3,4,5,6,7,8

*/

WITH AcctBrandSales AS (
	SELECT
		AccountKey,
		ItemUDVarChar2 AS Brand,
		SUM(TotalActualPrice) AS LTMAccountProductBrandSales
	FROM 
		FactInvoiceLine fil (nolock)
	INNER JOIN dbo.DimItem di (nolock)
		ON di.ItemKey = fil.ItemKey
	WHERE
		fil.Last12MonthsIndicator = N'Y'
	GROUP BY
		AccountKey,
		ItemUDVarChar2
)
, AcctSales AS (
	SELECT
		AccountKey,
		SUM(TotalActualPrice) AS LTMAccountSales
	FROM 
		FactInvoiceLine fil (nolock)
	WHERE
		fil.Last12MonthsIndicator = N'Y'
	GROUP BY
		AccountKey
)
SELECT
	da.AccountKey,
	AccountNumber,
	AccountName,
	dpAcctMgr.Fullname AS AccountManagerFullName,
	AG1Level1 AS BranchNumber,
	AG1Level1UDVarchar1 AS BranchName,
	AG1Level1UDVarchar6 AS BranchBrand,
	db.Brand,
	damBrand.AccountManagerCode AS BrandManager,
	LTMAccountProductBrandSales,
	LTMAccountSales
FROM dbo.DimAccountBrandManager dabm (nolock)
INNER JOIN dimBrand db  (nolock)
	ON db.BrandKey = dabm.BrandKey
INNER JOIN DimAccountManager damBrand  (nolock)
	ON damBrand.AccountManagerKey = dabm.AccountManagerKey
INNER JOIN DimPerson dpBrand  (nolock)
	ON dpBrand.DimPersonKey = damBrand.DimPersonKey
INNER JOIN DimAccount da  (nolock)
	ON da.AccountKey = dabm.AccountKey 
INNER JOIN DimAccountGroup1 dag1  (nolock)
	ON dag1.AccountGroup1Key = da.AccountGroup1Key
INNER JOIN DimAccountManager damAcct  (nolock)
	ON damAcct.AccountManagerKey = da.AccountManagerKey
INNER JOIN DimPerson dpAcctMgr  (nolock)
	ON dpAcctMgr.DimPersonKey = damAcct.DimPersonKey
LEFT JOIN AcctSales
	ON AcctSales.AccountKey = dabm.AccountKey
LEFT JOIN AcctBrandSales
	ON AcctBrandSales.AccountKey = dabm.AccountKey
	AND AcctBrandSales.Brand = db.Brand




GO
