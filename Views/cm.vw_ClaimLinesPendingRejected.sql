SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





/*

select * from cm.[vw_ClaimLinesPendingRejected] where vendorKey=15291 and claimMonth=201702 and supplierClaimKey=123

select top 100 * from dimitem where itemnumber like '%200615%'

select top 100 * from DimVendor

*/


CREATE VIEW [cm].[vw_ClaimLinesPendingRejected] AS


SELECT DISTINCT 
	cj.ClaimJournalKey,
	cj.ClientInvoiceLineUniqueID,
	cj.ClaimMonth,
	v.VendorNumber AS VendorNumber,
	v.VendorKey AS VendorKey,
	v.VendorDescription AS VendorDescription,
	fil.ChargeNoteID,
	fil.SLedgerNo AS InvoiceNumber,
	fil.SLedgerDate AS InvoiceDate,
	fil.TransactionDate,
	fil.BranchCode,
	fil.BranchDescription,
	fil.AccountNumber,
	fil.AccountName,
	fil.ProductCode,
	fil.ProductDescription,
	fil.TransType,
	fil.SupplyType AS SupplyType,
	fil.Qty,
	fil.NettPrice AS Price,
	fil.NettPrice / NULLIF(fil.Qty, 0) AS UnitPrice,
	fil.SupplierNettCost AS Cost,
	fil.SupplierNettCost / NULLIF(fil.Qty, 0) AS UnitCost,
	fil.TradingMargin AS TransTradingMargin,
	fil.FinancialMargin AS TransFinancialMargin,
	cj.OriginalContractID AS POSContractID,
	cj.CurrentContractID AS ARPContractID,
	sc.PricingContractID AS ARPPricingContractID,
	sc2.PricingContractID AS POSPricingContractID,
	sc.ContractOwner,
	cj.SupplierRef AS SupplierReferenceNo,
	sc.ContractType1 AS ContractType,
	sc.JobSite,
	cj.ClaimType,
	cj.ARPReason,
	cj.ARPResult AS ARPResult,
	cj.ClaimJournalStatus AS [Status],
	cjs.ClaimJournalStatusDescription AS StatusDescription,
	wu.UserName AS ADVRecommendedUser,
	rr.RejectionReasonValue AS ADVRejectReason,
	LEFT(cj.Comment, 10) AS ADVShortComment,
	cj.Comment AS ADVFullComment,
	cj.InvoiceClaimKey AS ClaimID,
	cj.ClaimLineValue AS ClaimValue,
	fil.TradingMargin / NULLIF(fil.NettPrice, 0) AS BeforeMarginPerc,
	fil.TradingMargin AS BeforeMarginValue,
	cj.FinancialMargin,
	cj.TradingMargin / NULLIF(fil.NettPrice, 0) AS AfterMarginPerc,
	cj.TradingMargin AS AfterMarginValue,
	cj.ConfidentialTermsGPAdjustAmt AS CTGPAdjustment,
	cj.PriceDeferralGPAdjustAmt AS PDGPAdjustment,
	cj.SpecialDealGPAdjustAmt AS SDGPAdjustment,
	cj.ClearanceDealGPAdjustAmt AS CDGPAdjustment,
	CASE
		WHEN cj.ClaimType = 'M' THEN 'Y'
		ELSE 'N'
	END AS ManualClaimFlag,
	CASE 
		WHEN fil.CTSReleaseCode IS NOT NULL THEN 'Y'
		ELSE 'N'
	END AS CTSFlag,
	sc.JobFlag,
	ar.AdjustmentReasonCode + ' - ' + ar.AdjustmentReasonValue AS AdjustmentReason,
	scl.SupplierClaimKey

FROM cm.ClaimJournal cj
INNER JOIN DimVendor v ON
	v.VendorKey = cj.VendorKey
INNER JOIN cm.ClaimJournalStatus cjs
	ON cjs.ClaimJournalStatus = cj.ClaimJournalStatus
LEFT JOIN cm.FactInvoiceLine fil ON
	fil.ClientInvoiceLineUniqueID = cj.ClientInvoiceLineUniqueID
LEFT JOIN cc.SupplierContract sc ON
	sc.ContractID = cj.CurrentContractID
LEFT JOIN cc.SupplierContract sc2 ON
	sc2.ContractId = cj.OriginalContractID
LEFT JOIN cm.RejectionReason rr
	ON rr.RejectionReasonKey = cj.RejectionReasonKey
LEFT JOIN dbo.WebUser wu
	ON wu.WebUserKey = cj.ADVRecommendationWebUserKey
LEFT JOIN dbo.DimAccountGroup1 dag1
	ON dag1.AG1Level1 = cj.BranchCode
LEFT JOIN dbo.DimAccount da
	ON da.AccountNumber = cj.CustomerId
LEFT JOIN dbo.DimItem di
	ON di.ItemNumber = cj.ProductCode
LEFT JOIN cm.AdjustmentReason ar
	ON ar.AdjustmentReasonKey = cj.AdjustmentReasonKey
LEFT JOIN cm.SupplierClaim scl
	ON scl.SupplierClaimKey = cj.SupplierClaimKey

































GO
