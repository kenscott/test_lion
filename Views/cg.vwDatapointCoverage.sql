SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO










CREATE VIEW [cg].[vwDatapointCoverage]
AS
/*

SELECT TOP 10 * FROM cg.vwDatapointCoverage

*/



SELECT
	Playbook,
	[Pricing Rule Key],
	GroupValues AS [Band Attributes],
	[Price Rule Level],
	[Price Rule Level Description],
	COUNT([Price Rule Level]) AS [Count],
	[Pyramid Code],
	[Contract Instruction Type],
	[Vendor Number],
	[LLSPG Code],
	[Item Number],
	[Job Flag],
	[Sales Bucket]
FROM
	cg.vwPriceBandAttribute
WHERE
	[Pricing Rule Key] IS NOT NULL
GROUP BY  Playbook,
	[Pricing Rule Key],
	GroupValues,
	[Price Rule Level],
	[Price Rule Level Description],
	[Pyramid Code],
	[Contract Instruction Type],
	[Vendor Number],
	[LLSPG Code],
	[Item Number],
	[Job Flag],
	[Sales Bucket]









GO
