SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




CREATE VIEW [lion].[vw_MPGLookup] AS
/*
	select top 5 * from dimitem
	select top 5 * from dbo.vw_MPGLookup where VendorKey = 1246 order by MPGCode desc
*/

SELECT DISTINCT 
	ItemVendorKey as VendorKey,
	dig1.ItemGroup1Key as MPGKey,
	dig1.IG1Level1 AS MPGCode, 
	dig1.IG1Level1UDVarchar1 AS MPGDescription,
	di.ItemUDVarchar1 AS ItemPyramidCode 
	FROM DimItem di
	JOIN DimItemGroup1 dig1 
		ON dig1.ItemGroup1Key = di.ItemGroup1Key



GO
