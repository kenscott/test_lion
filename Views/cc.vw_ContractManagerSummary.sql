SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO









/*

select * from cc.vw_ContractManagerSummary where contractkey = 128090

select top 10 statuskey, updatestatuskey, * from cc.SupplierContract

select * from cc.SupplierContractDocument where contractkey = 134448
*/

CREATE VIEW [cc].[vw_ContractManagerSummary] AS

WITH ActiveSupplierReference AS (
	SELECT ContractKey, MAX(ModifiedOnDT) as MaxDate
	FROM cc.SupplierContractReference
	WHERE GETDATE() BETWEEN StartDate AND ExpirationDate 
	GROUP BY ContractKey
	)


	SELECT 
		sc.[ContractKey]
	  ,sc.[StatusKey] AS ContractStatusKey
	  ,scs.[StatusValue] AS ContractStatusValue
	  ,sc.[UpdateStatusKey]
      ,sc.[LoadKey]
      ,sc.[ActionCode]
      ,sc.[RunDate]
      ,sc.[Region]
      ,sc.[Originator]
      ,sc.[ContractNumber]
      ,sc.[ContractOwner]
      ,sc.[VendorKey]
      ,scr.[ContractReference]
      ,sc.[StartDate]
      ,sc.[ExpirationDate]
      ,sc.[JobSite]
      ,sc.[FixedPriceDate]
      ,sc.[PSFNumber]
      ,sc.[AuthorizedIndicator]
      ,sc.[PyramidCode]
      --,scn.Notes
      ,ISNULL(sc.[ContractAmount],0) [ContractAmount]
      ,sc.[RTB]
      ,sc.[NextClaimNumber]
      ,sc.[LastClaimPeriod]
      ,sc.[ContractModificationUser]
      ,sc.[ContractModificationDate]
      ,sc.[ContractRegionalCreateDate]
      ,sc.[ContractRegionalCreateTime]
      ,sc.[ContractRegionalCreateRoutine]
      ,sc.[ContractRegionalModificationDate]
      ,sc.[ContractRegionalModificationTime]
      ,sc.[HTD]
      ,sc.[ContractType1]
      ,sc.[ContractType2]
      ,sc.[ContractType3]
      ,sc.[ContractLevel]
      ,CASE sc.[JobFlag] WHEN 1 THEN 'Y' ELSE 'N' END AS [JobFlag]
      ,sc.[FullContractNumber],
	  CASE 
		WHEN scus.UpdateStatusKey IN (2, 3) THEN scus.UpdateStatusDescription
		ELSE scs.StatusDescription
	  END AS [Status],
	 '' AS Condition,
	 sc.ContractID,
	ISNULL(dag1.AG1Level1UDVarchar6 ,'') AS BranchPrimaryBrand,
	ISNULL(dag1.AG1Level2,'') AS ReportingNetwork,
	ISNULL(dag1.AG1Level3,'') AS ReportingArea,
	ISNULL(dag1.AG1Level4,'') AS ReportingRegion,
	 sc.Developer AS Developer,
	 ISNULL(sc.CCProfit,0) AS CCProfit
	,CASE WHEN sc.IncludeAllAccounts = 1 THEN 'ALL'
	  ELSE (SELECT MIN(da.AccountNumber) + ' (' + CAST(COUNT(DISTINCT sca.AccountKey) AS VARCHAR(50)) + ')'
		 FROM cc.SupplierContractAccount sca 
		 JOIN dbo.DimAccount da ON da.AccountKey = sca.AccountKey
		 WHERE sca.ContractKey = sc.ContractKey  )
	 END AS AccountDisplay
	--,CASE WHEN sc.IncludeAllAccounts = 1 THEN 'ALL' 
	-- ELSE (SELECT MIN(da.AccountNumber) 
	--				+ ' (' + CAST(CASE WHEN MIN(sca.AccountKey) <> 1 THEN COUNT(DISTINCT sca.AccountKey) ELSE (SELECT COUNT(*) FROM dbo.DimAccount WHERE Inactive = 0 ) END AS VARCHAR(50)) + ')'
	--	 FROM cc.SupplierContractAccount sca 
	--	 JOIN dbo.DimAccount da ON da.AccountKey = sca.AccountKey
	--	 WHERE sca.ContractKey = sc.ContractKey
	--	 END AS AccountDisplayOld
	--,( SELECT COUNT(sca.AccountKey)
	--	FROM cc.SupplierContractAccount sca 
	--	 JOIN dbo.DimAccount da ON da.AccountKey = sca.AccountKey
	--	 WHERE sca.ContractKey = sc.ContractKey
	-- ) AccountCount
	,(SELECT CASE WHEN COUNT(sca.AccountKey) = 1 THEN MIN(da.AccountName) ELSE '' END
		FROM cc.SupplierContractAccount sca 
		 JOIN dbo.DimAccount da ON da.AccountKey = sca.AccountKey
		 WHERE sca.ContractKey = sc.ContractKey
	 ) AccountName
	,(SELECT MIN(di.ItemUDVarchar20) + ' (' + CAST(COUNT(DISTINCT sci.ItemKey) AS VARCHAR(50)) + ')' 
			 FROM cc.SupplierContractItem sci
			 JOIN dbo.DimItem di ON di.ItemKey = sci.ItemKey
			 WHERE sci.ContractKey = sc.ContractKey
			 --AND di.InActive = 0
			 ) ItemDisplay
	,(SELECT CASE WHEN COUNT(sci.ItemKey) = 1 THEN MIN(di.ItemDescription) ELSE '' END
		FROM cc.SupplierContractItem sci
		 JOIN dbo.DimItem di ON di.ItemKey = sci.ItemKey
		 WHERE sci.ContractKey = sc.ContractKey
	 ) ItemDescription
	,(SELECT MIN(dig1.IG1Level1) + ' (' + CAST(COUNT(DISTINCT mpg.ItemGroupXKey) AS VARCHAR(50)) + ')' 
			 FROM cc.SupplierContractItemGroupX mpg 
			 JOIN dbo.DimItemGroup1 dig1 ON dig1.ItemGroup1Key = mpg.ItemGroupXKey
			 WHERE mpg.ContractKey = sc.ContractKey
			 AND mpg.ItemGroupLevel = 1) MPGDisplay
	,(SELECT MIN(dig3.IG3Level1) + ' (' + CAST(COUNT(DISTINCT spg.ItemGroupXKey) AS VARCHAR(50)) + ')' 
			 FROM cc.SupplierContractItemGroupX spg 
			 JOIN dbo.DimItemGroup3 dig3 ON dig3.ItemGroup3Key = spg.ItemGroupXKey
			 WHERE spg.ContractKey = sc.ContractKey
			 AND spg.ItemGroupLevel = 3) spgDisplay
	,CASE WHEN sc.IncludeAllLocations = 1 THEN 'ALL' 
	 ELSE (SELECT MIN(scl1.LocationName) + ' (' + CAST(COUNT(DISTINCT scl1.LocationKey) AS VARCHAR(50))+ ')' 
			 FROM cc.SupplierContractLocation scl1
			 WHERE scl1.ContractKey = sc.ContractKey
			 ) END AS LocationDisplay
	--,(SELECT MIN(dag1.AG1Level1) + ' (' + CAST(COUNT(DISTINCT dcl1.LocationKey) AS VARCHAR(50))+ ')' 
	--		 FROM cc.SupplierContractLocation dcl1
	--		 JOIN dbo.DimAccountGroup1 dag1 ON dag1.AccountGroup1Key = dcl1.LocationKey
	--		 WHERE dcl1.ContractKey = sc.ContractKey
	--		 AND dcl1.ContractLocationLevelKey = 1 ) BranchDisplay
	--,(SELECT MIN(dr.Region) + ' (' + CAST(COUNT(DISTINCT dcl4.LocationKey) AS VARCHAR(50))+ ')'
	--		 FROM cc.SupplierContractLocation dcl4 
	--		 JOIN dbo.DimRegion dr ON dr.RegionKey = dcl4.LocationKey
	--		 WHERE dcl4.ContractKey = sc.ContractKey
	--		 AND dcl4.ContractLocationLevelKey = 4 ) RegionDisplay
	,ISNULL(sc.ContractCreationDT, sc.WolcenExtFrom) AS ContractCreationDT,
	 dv.VendorDescription AS Supplier,
	 (SELECT CASE WHEN COUNT(DISTINCT sci.ItemKey) <> 1 THEN '' ELSE MIN(di.VendorStockNumber) END
			 FROM cc.SupplierContractItem sci
			 JOIN dbo.DimItem di ON di.ItemKey = sci.ItemKey
			 WHERE sci.ContractKey = sc.ContractKey) SupplierCatalogNumber
	 ,dag1.AG1Level1UDVarchar1 AS BranchDescription
	 ,dag1b.AG1Level1UDVarchar1 AS OwnerBranchDescription
	 ,sc.PricingContractID AS PricingContractID
	 ,'' AS PostalCode
	 ,sc.CreatedByWebUserKey
	 ,ISNULL(wu1.UserName,sc.[ContractModificationUser]) AS CreatedByFullName
	 ,sc.ModifiedByWebUserKey
	 ,ISNULL(wu2.UserName,sc.[ContractModificationUser]) AS ModifiedByFullName
	 ,sc.PricingContractIndicator
	 ,sc.PurchaseType
	 ,sc.ContactName
	 ,sc.ContactEmail
	 ,sc.IncludeAllAccounts
	 ,sc.ExcludeCashAccounts
	 ,sc.Version
	 ,sc.ContractSourceKey
	 ,sc.IncludeAllLocations
	 ,(SELECT COUNT(*) FROM cc.SupplierContractLocation scl1 WHERE scl1.ContractKey = sc.ContractKey) LocationCount
	 ,(SELECT COUNT(*) FROM cc.SupplierContractDocument scd WHERE scd.ContractKey = sc.ContractKey) DocumentCount
	 ,dag1b.Converted 
	 ,sc.ContractTransmitErrorMessage 
	 ,sc.VariablePackMatching
	 ,sc.IncludeChildAccounts 
	 FROM cc.SupplierContract sc
	 LEFT JOIN dbo.DimAccountGroup1 dag1 ON sc.Originator = dag1.AG1Level1
	 LEFT JOIN dbo.DimAccountGroup1 dag1b ON sc.ContractOwner = dag1b.AG1Level1
	 LEFT JOIN dbo.DimVendor dv ON dv.VendorKey = sc.VendorKey
	 LEFT JOIN dbo.WebUser wu1 ON wu1.WebUserKey = sc.CreatedByWebUserKey
	 LEFT JOIN dbo.WebUser wu2 ON wu2.WebUserKey = sc.ModifiedByWebUserKey
	 LEFT JOIN cc.SupplierContractStatus scs ON scs.StatusKey = sc.StatusKey
	 LEFT JOIN cc.SupplierContractUpdateStatus scus ON scus.UpdateStatusKey = sc.UpdateStatusKey
	 LEFT JOIN ActiveSupplierReference asr ON asr.ContractKey = sc.ContractKey 
	 LEFT JOIN cc.SupplierContractReference scr ON scr.ContractKey = asr.ContractKey 
			AND scr.ModifiedOnDT = asr.MaxDate
	 --LEFT JOIN cc.SupplierContractNote scn ON scn.ContractKey = sc.ContractKey
	 --LEFT JOIN cc.SupplierContractAccount dca ON dca.ContractKey = sc.ContractKey
	 --LEFT JOIN dbo.DimAccount da ON da.AccountKey = dca.AccountKey
	 WHERE sc.[ExpirationDate] > CAST('12-31-2014' AS DATE)



















GO
