SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO










CREATE         view
[dbo].[PlaybookPricingGroupQuantityBand_View]
as
Select B.PlaybookDataPointGroupKey , 
		B.PricingRuleKey, 
		A.PlaybookPricingGroupQuantityBandKey,
		A.PlaybookPricingGroupKey,
		A.PricingRuleBandKey,
		A.LowerBandQuantity,
		A.UpperBandQuantity,
		A.BandSequenceNumber,
		A.RulingMemberCount,
		A.NonRulingMemberCount

From PlaybookPricingGroupQuantityBand A
join PlaybookPricingGroup B on 
	A.PlaybookPricingGroupKey = B.PlaybookPricingGroupKey










GO
