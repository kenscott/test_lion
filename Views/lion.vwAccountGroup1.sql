SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO














CREATE 
VIEW [lion].[vwAccountGroup1] 

/*
SELECT TOP 100 * FROM lion.vwAccountGroup1 
*/


AS

SELECT
	dag1.AccountGroup1Key,

	AG1Level1 AS Branch,
	AG1Level2 AS Network,
	AG1Level3 AS Area,
	AG1Level4 AS Region,
	--AG1Level5 AS CompanyName,

	AG1Level1UDVarchar1 AS BranchName,
	AG1Level2UDVarchar1 AS NetworkDescription,
	AG1Level3UDVarchar1 AS AreaDescription,
	AG1Level4UDVarchar1 AS RegionDescription,
	--AG1Level5UDVarchar1 AS CompanyNameDescription,

	AG1Level1UDVarchar2 AS PlaybookRegion,
	AG1Level1UDVarchar3 AS PlaybookRegionDescription,
	AG1Level1UDVarchar4 AS PlaybookArea,
	AG1Level1UDVarchar5 AS PlaybookAreaDescription,
	AG1Level1UDVarchar20 AS PlaybookBrandIndicator,

	AG1Level1UDVarchar6 AS BranchPrimaryBrand,
	AG1Level1UDVarchar7 AS BranchTradingStatus,
	AG1Level1UDVarchar8 AS BranchType,
	AG1Level1UDVarchar9 AS BranchAddressLine1,
	AG1Level1UDVarchar10 AS BranchAddressLine2,
	AG1Level1UDVarchar11 AS BranchAddressLine3,
	AG1Level1UDVarchar12 AS BranchAddressLine4,
	AG1Level1UDVarchar13 AS BranchAddressLine5,
	AG1Level1UDVarchar14 AS BranchPostcode,
	AG1Level1UDVarchar15 AS BranchEmail,
	AG1Level1UDVarchar16 AS BranchTelephone,

	AG1Level1UDVarchar17 AS CustomerBranchPostCodeArea,
	AG1Level1UDVarchar18 AS CustomerBranchPostCodeDistrict,
	
	AG1Level1UDVarchar19 AS BranchPyramidCode,
	
	--AG1Level1UDVarchar20 AS PlaybookBrandIndicator,
	AG1Level1UDVarchar21 AS BrandGroup,
	AG1Level1UDVarchar22 AS PGActiveIndicator,
	dag1.Inactive AS BranchInactive,
	dag1.Converted,

	AG1Level1UDVarchar23 AS Manager,
	AG1Level1UDVarchar24 AS BranchManagerEmail,
	AG1Level1UDVarchar25 AS RegionExceptionIndicator,

	AG1Level1UDVarchar26 AS BusinessUnitRegion,

	AG1Level1PersonKey

FROM dbo.DimAccountGroup1 dag1 (NOLOCK)










































GO
