SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE  View 
[dbo].[PlaybookDataPointGroup_View]
as
Select A.PlaybookDataPointGroupKey,
A.ProjectSpecificationScenarioKey,
A.ProjectKey, 
B.ScenarioKey 
from PlaybookDataPointGroup A
join ATKProjectSpecificationSCenario B
on A.ProjectSpecificationSCenarioKey = B.ProjectSpecificationSCenarioKey



GO
