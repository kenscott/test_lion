SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO








CREATE VIEW [lion].[vwManualPriceBandExport]
AS
--; 
WITH ItemRollup AS (
	SELECT
		mpb.ManualPriceBandKey,
		mpb.ItemPyramidCode,
		mpb.LLSPGCode,
		mpb.ProductCode,
		mpb.PriceApproach,
		mpb.ContractClaimsIndicator,
		mpb.Region,
		mpb.Area,
		mpb.Network,
		mpb.Branch,
		mpb.ExpirationDate,
		mpb.FloorGPP,
		mpb.TargetGPP,
		mpb.StretchGPP,
		(di.ItemUDDecimal3 - di.ItemUDDecimal4 - di.ItemUDDecimal5) AS ItemCost,
		ItemUDDecimal1 AS TradePrice,
		SUM(TotalActualPrice) AS TotalSales,
		SUM(TotalActualCost) AS TotalCost,
		SUM(TotalQuantity) AS TotalQuantity,
		SUM(TotalQuantity * UnitListPrice) AS TotalTradePrice,
		--SUM(TotalActualPrice * il.FloorGPP) AS GTMFloor,
		--SUM(TotalActualPrice * il.TargetGPP) AS GTMTarget,
		--SUM(TotalActualPrice * il.StretchGPP) AS GTMStretch,
		SUM(TotalActualPrice * il.FloorGPP)/NULLIF(SUM(TotalActualPrice), 0) AS CurrentFloorGPP,
		SUM(TotalActualPrice * il.TargetGPP)/NULLIF(SUM(TotalActualPrice), 0) AS CurrentTargetGPP,
		SUM(TotalActualPrice * il.StretchGPP)/NULLIF(SUM(TotalActualPrice), 0) AS CurrentStretchGPP
	FROM FactInvoiceLinePricebandScore il
	INNER JOIN dbo.DimAccountGroup1 dag1
		ON dag1.AccountGroup1Key = il.AccountGroup1Key
	INNER JOIN dbo.DimItem di
		ON di.ItemKey = il.ItemKey
	INNER JOIN dbo.DimInvoiceLineGroup1 dilg1
		ON dilg1.InvoiceLineGroup1Key = il.InvoiceLineGroup1Key
	INNER JOIN dbo.FactLastPriceAndCost flpac
		ON flpac.AccountKey = il.AccountKey
		AND flpac.ItemKey = il.ItemKey
		AND flpac.InvoiceLineGroup1Key = il.InvoiceLineGroup1Key
	RIGHT JOIN lion.ATKManualPriceBand mpb
		ON mpb.ItemPyramidCode = di.ItemUDVarchar1
		AND mpb.LLSPGCode <> N'ALL'
		AND mpb.ProductCode = di.itemudvarchar20
		AND (mpb.PriceApproach = flpac.UDVarchar10 OR mpb.PriceApproach = N'ALL')
		AND (mpb.ContractClaimsIndicator = InvoiceLineGroup1UDVarchar2 OR mpb.ContractClaimsIndicator = N'ALL')
		AND (mpb.Region = AG1Level4 OR mpb.Region = N'ALL')
		AND (mpb.Area = AG1Level3 OR mpb.Area = N'ALL')
		AND (mpb.Network = AG1Level2 OR mpb.Network = N'ALL')
		AND (mpb.Branch = AG1Level1 OR mpb.Branch = N'ALL')
	WHERE 
		mpb.ItemPyramidCode IS NOT NULL
		AND mpb.LLSPGCode <> N'ALL' 
		AND mpb.ProductCode <> N'ALL' 		
	GROUP BY
		mpb.ManualPriceBandKey,
		mpb.ItemPyramidCode,
		mpb.LLSPGCode,
		mpb.ProductCode,
		mpb.PriceApproach,
		mpb.ContractClaimsIndicator,
		mpb.Region,
		mpb.Area,
		mpb.Network,
		mpb.Branch,
		mpb.ExpirationDate,
		mpb.FloorGPP,
		mpb.TargetGPP,
		mpb.StretchGPP,
		(di.ItemUDDecimal3 - di.ItemUDDecimal4 - di.ItemUDDecimal5),
		ItemUDDecimal1
), LLSPGRollup AS (
	SELECT
		mpb.ManualPriceBandKey,
		mpb.ItemPyramidCode,
		mpb.LLSPGCode,
		mpb.ProductCode,
		mpb.PriceApproach,
		mpb.ContractClaimsIndicator,
		mpb.Region,
		mpb.Area,
		mpb.Network,
		mpb.Branch,
		mpb.ExpirationDate,
		mpb.FloorGPP,
		mpb.TargetGPP,
		mpb.StretchGPP,
		SUM(TotalActualPrice) AS TotalSales,
		SUM(TotalActualCost) AS TotalCost,
		SUM(TotalQuantity) AS TotalQuantity,
		SUM(TotalQuantity * UnitListPrice) AS TotalTradePrice,
		SUM(TotalActualPrice * il.FloorGPP) AS GTMFloor,
		SUM(TotalActualPrice * il.TargetGPP) AS GTMTarget,
		SUM(TotalActualPrice * il.StretchGPP) AS GTMStretch,
		SUM(TotalActualPrice * il.FloorGPP)/NULLIF(SUM(TotalActualPrice), 0) AS CurrentFloorGPP,
		SUM(TotalActualPrice * il.TargetGPP)/NULLIF(SUM(TotalActualPrice), 0) AS CurrentTargetGPP,
		SUM(TotalActualPrice * il.StretchGPP)/NULLIF(SUM(TotalActualPrice), 0) AS CurrentStretchGPP
	FROM FactInvoiceLinePricebandScore il
	INNER JOIN dbo.DimAccountGroup1 dag1
		ON dag1.AccountGroup1Key = il.AccountGroup1Key
	INNER JOIN dbo.DimItem di
		ON di.ItemKey = il.ItemKey
	INNER JOIN dbo.DimItemGroup3 dig3
		ON dig3.ItemGroup3Key = di.ItemGroup3Key
	INNER JOIN dbo.DimInvoiceLineGroup1 dilg1
		ON dilg1.InvoiceLineGroup1Key = il.InvoiceLineGroup1Key
	INNER JOIN dbo.FactLastPriceAndCost flpac
		ON flpac.AccountKey = il.AccountKey
		AND flpac.ItemKey = il.ItemKey
		AND flpac.InvoiceLineGroup1Key = il.InvoiceLineGroup1Key
	RIGHT JOIN lion.ATKManualPriceBand mpb
		ON mpb.ItemPyramidCode = di.ItemUDVarchar1
		AND mpb.ProductCode = N'ALL'
		AND (mpb.LLSPGCode = IG3Level1)
		AND (mpb.PriceApproach = flpac.UDVarchar10 OR mpb.PriceApproach = N'ALL')
		AND (mpb.ContractClaimsIndicator = InvoiceLineGroup1UDVarchar2 OR mpb.ContractClaimsIndicator = N'ALL')
		AND (mpb.Region = AG1Level4 OR mpb.Region = N'ALL')
		AND (mpb.Area = AG1Level3 OR mpb.Area = N'ALL')
		AND (mpb.Network = AG1Level2 OR mpb.Network = N'ALL')
		AND (mpb.Branch = AG1Level1 OR mpb.Branch = N'ALL')
	WHERE 
		mpb.ItemPyramidCode IS NOT NULL 
		AND mpb.LLSPGCode <> N'ALL'
		AND mpb.ProductCode = N'ALL'
	GROUP BY
		mpb.ManualPriceBandKey,
		mpb.ItemPyramidCode,
		mpb.LLSPGCode,
		mpb.ProductCode,
		mpb.PriceApproach,
		mpb.ContractClaimsIndicator,
		mpb.Region,
		mpb.Area,
		mpb.Network,
		mpb.Branch,
		mpb.ExpirationDate,
		mpb.FloorGPP,
		mpb.TargetGPP,
		mpb.StretchGPP
)

SELECT
	ManualPriceBandKey,
	ItemPyramidCode,
	LLSPGCode,
	ProductCode,
	PriceApproach,
	ContractClaimsIndicator,
	Region,
	Area,
	Network,
	Branch,
	ExpirationDate,

	FloorGPP,
	TargetGPP,
	StretchGPP,

	CurrentFloorGPP,
	CurrentTargetGPP,
	CurrentStretchGPP,
	
	1 - (rp.Price / (TotalTradePrice/TotalQuantity)) AS CurrentFloorDiscount,
	1 - (ap.Price / (TotalTradePrice/TotalQuantity)) AS CurrentTargetDiscount,
	1 - (gp.Price / (TotalTradePrice/TotalQuantity)) AS CurrentStretchDiscount,

	rp.Price AS CurrentFloorPrice,
	ap.Price AS CurrentTargetPrice,
	gp.Price AS CurrentStretchPrice,

	ISNULL(ItemCost, (di.ItemUDDecimal3 - di.ItemUDDecimal4 - di.ItemUDDecimal5)) AS ProductCost,
	ISNULL(TradePrice, ItemUDDecimal1) AS TradePrice

FROM 
	ItemRollup
LEFT JOIN dbo.DimItem di
	ON di.ItemUDVarchar1 = ItemPyramidCode
	AND di.itemudvarchar20 = ProductCode

OUTER APPLY lion.fn_GetPriceWeighted(TotalCost, TotalQuantity, FloorGPP) rp
OUTER APPLY lion.fn_GetPriceWeighted(TotalCost, TotalQuantity, TargetGPP) ap
OUTER APPLY lion.fn_GetPriceWeighted(TotalCost, TotalQuantity, StretchGPP) gp


UNION

SELECT
	ManualPriceBandKey,
	ItemPyramidCode,
	LLSPGCode,
	ProductCode,
	PriceApproach,
	ContractClaimsIndicator,
	Region,
	Area,
	Network,
	Branch,
	ExpirationDate,

	FloorGPP,
	TargetGPP,
	StretchGPP,

	FloorGPP,
	TargetGPP,
	StretchGPP,
	
	1 - (rp.Price / (TotalTradePrice/TotalQuantity)) AS CurrentFloorDisc,
	1 - (ap.Price / (TotalTradePrice/TotalQuantity)) AS CurrentTargetDisc,
	1 - (gp.Price / (TotalTradePrice/TotalQuantity)) AS CurrentStretchDisc,

	rp.Price AS FloorPrice,
	ap.Price AS TargetPrice,
	gp.Price AS StretchPrice,

	--ISNULL(ItemCost, (di.ItemUDDecimal3 - di.ItemUDDecimal4 - di.ItemUDDecimal5)) AS CurrProductCost,
	--ISNULL(TradePrice, ItemUDDecimal1) AS CurrTradePrice

	NULL AS CurrProductCost,
	NULL AS CurrTradePrice

FROM 
	LLSPGRollup

OUTER APPLY lion.fn_GetPriceWeighted(TotalCost, TotalQuantity, FloorGPP) rp
OUTER APPLY lion.fn_GetPriceWeighted(TotalCost, TotalQuantity, TargetGPP) ap
OUTER APPLY lion.fn_GetPriceWeighted(TotalCost, TotalQuantity, StretchGPP) gp


--)
--select * 
--into #t1 from b

--ORDER BY 3


--UNION


----; 
--WITH ItemRollup AS (
--	SELECT
--		mpb.ItemPyramidCode,
--		mpb.LLSPGCode,
--		mpb.ProductCode,
--		mpb.PriceApproach,
--		mpb.ContractClaimsIndicator,
--		mpb.Region,
--		mpb.Area,
--		mpb.Network,
--branch
--		(di.ItemUDDecimal3 - di.ItemUDDecimal4 - di.ItemUDDecimal5) AS ItemCost,
--		ItemUDDecimal1 AS TradePrice,
--		SUM(TotalActualPrice) AS TotalSales,
--		SUM(TotalQuantity) AS TotalQuantity,
--		SUM(TotalActualPrice * il.FloorGPP) AS GTMFloor,
--		SUM(TotalActualPrice * il.TargetGPP) AS GTMTarget,
--		SUM(TotalActualPrice * il.StretchGPP) AS GTMStretch,
--		SUM(TotalActualPrice * il.FloorGPP)/NULLIF(SUM(TotalActualPrice), 0) AS CurrFloorGPP,
--		SUM(TotalActualPrice * il.TargetGPP)/NULLIF(SUM(TotalActualPrice), 0) AS CurrTargetGPP,
--		SUM(TotalActualPrice * il.StretchGPP)/NULLIF(SUM(TotalActualPrice), 0) AS CurrStretchGPP
--	FROM #FactInvoiceLinePricebandScore il
--	INNER JOIN dbo.DimAccountGroup1 dag1
--		ON dag1.AccountGroup1Key = il.AccountGroup1Key
--	INNER JOIN dbo.DimItem di
--		ON di.ItemKey = il.ItemKey
--	INNER JOIN dbo.DimItemGroup3 dig3
--		ON dig3.ItemGroup3Key = di.ItemGroup3Key
--	INNER JOIN dbo.DimInvoiceLineGroup1 dilg1
--		ON dilg1.InvoiceLineGroup1Key = il.InvoiceLineGroup1Key
--	INNER JOIN dbo.FactLastPriceAndCost flpac
--		ON flpac.AccountKey = il.AccountKey
--		AND flpac.ItemKey = il.ItemKey
--		AND flpac.InvoiceLineGroup1Key = il.InvoiceLineGroup1Key
--	RIGHT JOIN lion.ATKManualPriceBand mpb
--		ON mpb.ItemPyramidCode = di.ItemUDVarchar1
--		AND mpb.ProductCode = di.itemudvarchar20
--		AND (mpb.LLSPGCode = IG3Level1 OR mpb.LLSPGCode = N'ALL')
--		AND (mpb.PriceApproach = flpac.UDVarchar10 OR mpb.PriceApproach = N'ALL')
--		AND (mpb.ContractClaimsIndicator = InvoiceLineGroup1UDVarchar2 OR mpb.ContractClaimsIndicator = N'ALL')
--		AND (mpb.Region = AG1Level4 OR mpb.Region = N'ALL')
--		AND (mpb.Area = AG1Level3 OR mpb.Area = N'ALL')
--		AND (mpb.Network = AG1Level2 OR mpb.Network = N'ALL')
--		branch
--	WHERE 
--		mpb.ProductCode IS NOT NULL 
--		AND mpb.ProductCode = N'ALL'
--	GROUP BY
--		mpb.ItemPyramidCode,
--		mpb.LLSPGCode,
--		mpb.ProductCode,
--		mpb.PriceApproach,
--		mpb.ContractClaimsIndicator,
--		mpb.Region,
--		mpb.Area,
--		mpb.Network,
--		branch,
--		(di.ItemUDDecimal3 - di.ItemUDDecimal4 - di.ItemUDDecimal5),
--		ItemUDDecimal1
--		,mpb.ManualPriceBandKey
--)
--SELECT
--	ItemPyramidCode,
--	LLSPGCode,
--	ProductCode,
--	PriceApproach,
--	ContractClaimsIndicator,
--	Region,
--	Area,
--	Network,
--branch
--	CurrFloorGPP,
--	CurrTargetGPP,
--	CurrStretchGPP,
--	ItemCost,
--	TradePrice,
--	rp.Price AS CurrFloorPrice,
--	ap.Price AS CurrTargetPrice,
--	gp.Price AS CurrStretchPrice,
--	1 - (rp.Price / TradePrice) AS CurrFloorDisc,
--	1 - (ap.Price / TradePrice) AS CurrTargetDisc,
--	1 - (gp.Price / TradePrice) AS CurrStretchDisc

GO
