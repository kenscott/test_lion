SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO









CREATE
VIEW [lion].[vwFactMarginErosion] 

/*
SELECT TOP 100 * FROM lion.vwFactMarginErosion
SELECT COUNT(*) FROM lion.vwFactMarginErosion
SELECT COUNT(*) FROM dbo.FactMarginErosion

SELECT TOP 100 * 
INTO #t1
FROM lion.vwFactMarginErosion
DROP TABLE #t1
*/


AS

SELECT

	AccountNumber,
	AccountName,
	--AccountNumberParent,
	AccountUDVarChar8 AS AccountOwnerType,
	AccountUDVarChar9 AS AccountOwnerBucket,
	AccountUDVarChar10 AS CustomerSegment,
	--AccountUDVarChar11 AS CustomerRebateIndicator,
	--da.Inactive AS AccountInactive,

	--AG1Level1UDVarchar21 AS BrandGroup,

	VendorDescription,
	VendorNumber,

	--ItemNumber,
	ItemDescription,
	--BaseUnitOfMeasure, 
	--VendorStockNumber,
	
	ItemUDVarChar1 AS ItemPyramidCode,
	ItemUDVarChar20 AS ProductCode,
	ItemUDVarChar2 AS ProdBrandAtLLSPG,
	ItemUDVarChar18 AS UnitCode,
	--ItemUDDate1 AS ItemDateForDeletion,
	ItemUDDate2 AS CurrBranchCostDate,
	ItemUDDate3 AS CurrBranchTradePriceDate,
	
	CAST(ItemUDVarChar22 AS DATE) AS CurrentInvoiceCostDate,		-- LP2P-1090


	ItemUDDecimal1 AS CurrBranchTradePrice,
	--ItemUDDecimal2 AS RetailPrice,

	ItemUDDecimal3 AS CurrentBranchCost,
	--ItemUDDecimal4 AS Padding,
	--ItemUDDecimal5 AS CurrNotSettVal,
	CurrentCost,
	CurrentInvoiceCost,
	--di.InActive AS ItemInActive,

	--IG2Level1 AS ProdBrandAtLLSPG,
	--IG2Level1UDVarchar1 AS ProductTypeDescription,
	
	IG3Level1 AS LLSPGCode,--1
	--IG3Level2 AS HLSPGCode,--2
	--IG3Level3 AS GSPGCode,--3
	--IG3Level4 AS PYRCode,--4
	IG3Level1UDVarchar1 AS LLSPGDescription,--1
	--IG3Level2UDVarchar1 AS HLSPGDescription,--2
	--IG3Level3UDVarchar1 AS GSPGDescription, --3
	--IG3Level4UDVarchar1 AS PYRCode,--4

	InvoiceLineGroup1UDVarchar1 AS ShipmentType,
	InvoiceLineGroup1UDVarchar2 AS DeviatedIndicator,

	
	--AccountKey,
	--ItemKey,
	--InvoiceLineGroup1Key,
	PriceDerivation,
	LastQuantity,
	LastUnitPrice,
	LastUnitCost,
	(LastUnitPrice - LastUnitCost) / NULLIF(LastUnitPrice, 0) AS LastGPP,
	LastSaleDate,
	Total12MonthQuantity,
	Total12MonthSales,
	Total12MonthCost,
	Total12MonthInvoiceLineCount,
	SustainedGPP,
	UnitPriceAtSustained,
	UnitCostAtSustained,
	MEProposedPrice,
	MEImpact,
	PLSIndicator,
	TermPyramidCode,
	TermLLSPGCode,
	TermExceptionProduct,
	Usualdiscount1,
	Usualdiscount2,
	ExceptionDiscount,
	ExceptionFixedPrice,
	ExceptionFromDate,
	ExceptionToDate
FROM dbo.FactMarginErosion me WITH (NOLOCK)
INNER JOIN dbo.DimAccount da WITH (NOLOCK)
	ON da.AccountKey = me.AccountKey
--INNER JOIN dbo.DimAccountGroup1 dag1 WITH (NOLOCK)
--	ON dag1.AccountGroup1Key = da.AccountGroup1Key
--INNER JOIN dbo.DimAccountGroup2 dag2 WITH (NOLOCK)
--	ON dag2.AccountGroup2Key = da.AccountGroup2Key
--INNER JOIN dbo.DimAccountGroup3 dag3 WITH (NOLOCK)
--	ON dag3.AccountGroup3Key = da.AccountGroup3Key
--INNER JOIN dbo.DimAccountManager dam WITH (NOLOCK)
--	ON dam.AccountManagerKey = da.AccountManagerKey
--INNER JOIN dbo.DimPerson dp WITH (NOLOCK)
--	ON dp.DimPersonKey = dam.DimPersonKey

JOIN dbo.DimItem di WITH (NOLOCK)
	ON di.ItemKey = me.ItemKey
--INNER JOIN dbo.DimItemGroup1 dig1 WITH (NOLOCK)
--	ON dig1.ItemGroup1Key = di.ItemGroup1Key
INNER JOIN dbo.DimItemGroup3 dig3 WITH (NOLOCK)
	ON dig3.ItemGroup3Key = di.ItemGroup3Key
INNER JOIN dbo.DimVendor WITH (NOLOCK)
	ON DimVendor.VendorKey = di.ItemVendorKey

INNER JOIN DimInvoiceLineGroup1 dilg1 WITH (NOLOCK)
	ON dilg1.InvoiceLineGroup1Key = me.InvoiceLineGroup1Key






GO
