SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



/*
	Programmer: James Dameron
	Date: 11-30-2016
	JIRA: LPF-3774
	Description:  Should be used in conjunction with cc.vw_ContractExport.  If that view is modified, it "might" require mods 
				  to this view.  This is a lighter weight view used to determine how many item/spg/mpg rows (count of all) exist on
				  a given contract. 
				  
	Sample: 
		select this_.sequence as y0_, count(this_.sequence) as y1_ 
		from cc.vw_ContractExport this_ 
		where this_.sequence in (154663, 547545, 547547, 547551, 154305) 
		group by this_.sequence 
		order by this_.sequence
				 

*/

CREATE VIEW [cc].[vw_ContractExportCount] AS
WITH Output AS
(
	SELECT 
	   sc.ContractKey as Sequence
      ,'Product' AS LevelType,
      sci.ItemKey
    FROM cc.SupplierContract sc WITH (NOLOCK)
    LEFT JOIN cc.SupplierContractItem sci WITH (NOLOCK) on sci.ContractKey = sc.ContractKey

	UNION
	
	SELECT 
	   sc.ContractKey
      ,'MPG' AS LevelType,
      scigx.ItemGroupXKey
    FROM cc.SupplierContract sc WITH (NOLOCK)
    LEFT JOIN cc.SupplierContractItemGroupX scigx WITH (NOLOCK) on scigx.ContractKey = sc.ContractKey
    WHERE scigx.ItemGroupLevel = 1

	UNION
	
	SELECT 
	   sc.ContractKey
       ,'SPG' AS LevelType,
       scigx.ItemGroupXKey
    FROM cc.SupplierContract sc
    LEFT JOIN cc.SupplierContractItemGroupX scigx WITH (NOLOCK) on scigx.ContractKey = sc.ContractKey
    WHERE scigx.ItemGroupLevel = 3
 )     
   
 SELECT * FROM Output   
      
GO
