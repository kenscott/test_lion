SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [lion].[vwCompetitorsItems]
AS

WITH Ranks AS (
	SELECT
		RANK() OVER (PARTITION BY ItemKey ORDER BY Retailer, ItemKey) AS competitornumber,
		RANK() OVER (PARTITION BY ItemKey ORDER BY Retailer, ItemKey) AS competitorcodenumber,
		ItemKey, 
		Retailer, 
		Price
FROM 
	lion.[CompetitorPricing]
GROUP BY 		
	ItemKey, 
	Retailer, 
	Price
), Competitors AS
	(SELECT
	ItemKey,
	CASE WHEN  CompetitorNumber = 1 THEN Retailer ELSE NULL END AS Competitor1,
	CASE WHEN CompetitorCodeNumber = 1 THEN Price ELSE NULL END AS CompetitorPrice1,
	CASE WHEN  CompetitorNumber = 2 THEN Retailer ELSE NULL END AS Competitor2,
	CASE WHEN CompetitorCodeNumber = 2 THEN Price ELSE NULL END AS CompetitorPrice2,
	CASE WHEN  CompetitorNumber = 3 THEN Retailer ELSE NULL END AS Competitor3,
	CASE WHEN CompetitorCodeNumber = 3 THEN Price ELSE NULL END AS CompetitorPrice3,
	CASE WHEN  CompetitorNumber = 4 THEN Retailer ELSE NULL END AS Competitor4,
	CASE WHEN CompetitorCodeNumber = 4 THEN Price ELSE NULL END AS CompetitorPrice4,
	CASE WHEN  CompetitorNumber = 5 THEN Retailer ELSE NULL END AS Competitor5,
	CASE WHEN CompetitorCodeNumber = 5 THEN Price ELSE NULL END AS CompetitorPrice5,
	CASE WHEN  CompetitorNumber = 6 THEN Retailer ELSE NULL END AS Competitor6,
	CASE WHEN CompetitorCodeNumber = 6 THEN Price ELSE NULL END AS CompetitorPrice6,
	CASE WHEN  CompetitorNumber = 7 THEN Retailer ELSE NULL END AS Competitor7,
	CASE WHEN CompetitorCodeNumber = 7 THEN Price ELSE NULL END AS CompetitorPrice7,
	CASE WHEN  CompetitorNumber = 8 THEN Retailer ELSE NULL END AS Competitor8,
	CASE WHEN CompetitorCodeNumber = 8 THEN Price ELSE NULL END AS CompetitorPrice8
	FROM 
		Ranks
)
SELECT 
	ItemKey,
	MAX(Competitor1) AS Competitor1,
	MAX(CompetitorPrice1) AS CompetitorPrice1,
	MAX(Competitor2) AS Competitor2,
	MAX(CompetitorPrice2) AS CompetitorPrice2,
	MAX(Competitor3) AS Competitor3,
	MAX(CompetitorPrice3) AS CompetitorPrice3,
	MAX(Competitor4) AS Competitor4,
	MAX(CompetitorPrice4) AS CompetitorPrice4,
	MAX(Competitor5) AS Competitor5,
	MAX(CompetitorPrice5) AS CompetitorPrice5,
	MAX(Competitor6) AS Competitor6,
	MAX(CompetitorPrice6) AS CompetitorPrice6,
	MAX(Competitor7) AS Competitor7,
	MAX(CompetitorPrice7) AS CompetitorPrice7,
	MAX(Competitor8) AS Competitor8,
	MAX(CompetitorPrice8) AS CompetitorPrice8
FROM 
	Competitors
GROUP BY 
	ItemKey;
GO
