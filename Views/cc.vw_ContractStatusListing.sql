SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE VIEW [cc].[vw_ContractStatusListing] AS
/*
	select * from cc.Suppliercontractstatus
	select * from cc.Suppliercontractupdatestatus
	
	select * from cc.vw_ContractStatusListing
*/

	SELECT StatusDescription AS ContractStatus
		FROM cc.SupplierContractStatus
		WHERE StatusKey <> 1
	
	UNION
	
	SELECT UpdateStatusDescription AS ContractStatus
		FROM cc.SupplierContractUpdateStatus
		WHERE UpdateStatusKey in (2,3)






GO
