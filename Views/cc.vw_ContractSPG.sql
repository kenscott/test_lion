SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



	CREATE VIEW [cc].[vw_ContractSPG] AS 
		SELECT 
			NEWID() AS GUID, 
			igx.ContractKey, 	
			dig3.ItemGroup3Key,
			IG3Level1 AS SPGCode,
			IG3Level1UDVarchar1 AS SPGDescription
		FROM cc.SupplierContractItemGroupX igx
		JOIN dbo.DimItemGroup3 dig3 ON dig3.ItemGroup3Key = igx.ItemGroupXKey
		WHERE igx.ItemGroupLevel = 3
		

GO
