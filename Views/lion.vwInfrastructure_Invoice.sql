SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

Create view [lion].[vwInfrastructure_Invoice]

as
(

SELECT  branch ,
        branchname ,
        Score ,
        SUM(TotalActualPrice) SLS ,
        SUM(TotalActualCost) Cst ,
        SUM(GP) GP ,
        CASE WHEN SUM(TotalActualPrice) <> 0
             THEN ( SUM(GP) / SUM(TotalActualPrice) )
             ELSE 0
        END GPP
FROM    ( SELECT    acct.branch ,
                    acct.branchname ,
                    f.AccountKey ,
                    f.AlternateAccountKey ,
                    f.ItemKey ,
                    f.VendorKey ,
                    f.AccountManagerKey ,
                    f.InvoiceDateDayKey ,
                    f.InvoiceDateMonthKey ,
                    f.InvoiceLineGroup1Key ,
                    f.InvoiceLineGroup2Key ,
                    f.InvoiceLineGroup3Key ,
                    f.InvoiceLineJunkKey ,
                    f.AccountGroup1Key ,
                    f.AccountGroup2Key ,
                    f.AccountGroup3Key ,
                    f.ItemGroup1Key ,
                    f.ItemGroup2Key ,
                    f.ItemGroup3Key ,
                    f.InvoiceLineUniqueIdentifier ,
                    f.InvoiceLineUniqueIdentifier AS LinkedInvoiceLineUniqueIdentifier ,
                    f.ClientInvoiceID ,
                    f.ClientInvoiceLineID ,
                    f.TotalQuantity ,
                    f.TotalActualPrice ,
                    f.TotalActualCost ,
                    f.TradingPrice AS UnitListPrice ,
                    f.TotalNetQuantity ,
                    f.TotalNetPrice ,
                    f.TotalNetCost ,
                    f.PriceDerivation ,
                    fm.GPP AS FloorGPP ,
                    tm.GPP AS TargetGPP ,
                    sm.GPP AS StretchGPP ,
                    pb.Score ,
                    pb.RPBPlaybookPricingGroupKey ,
                    f.TotalActualPrice - f.TotalActualCost AS GP ,
                    ( lt.TotalActualPrice / NULLIF(lt.TotalQuantity, 0)
                      - lt.TotalActualCost / NULLIF(lt.TotalQuantity, 0) )
                    / NULLIF(lt.TotalActualPrice / NULLIF(lt.TotalQuantity, 0),
                             0) AS LastItemGPP ,
                    fi.Impact AS FloorImpact ,
                    ti.Impact AS TargetImpact ,
                    si.Impact AS StretchImpact ,
                    fti.Impact AS [Floor to Target Impact] ,
                    tsi.Impact AS [Target to Stretch Impact] ,
                    r.Risk AS [Stretch Risk] ,
                    f.TotalActualPrice * fm.GPP AS [GTM$-Floor] ,
                    f.TotalActualPrice * tm.GPP AS [GTM$-Target] ,
                    f.TotalActualPrice * sm.GPP AS [GTM$-Stretch] ,
                    flpac.UDVarchar4 AS BaseSales603010Bucket ,
                    flpac.UDVarchar1 AS BaseStrategicItem ,
                    fil.UDVarchar1 AS LastPriceDerivation ,
                    tr.Risk AS [Trade Risk] ,
                    CASE WHEN ( f.TotalActualPrice - f.TotalActualCost ) < 0
                         THEN 'Negative'
                         ELSE 'Positive'
                    END AS MarginType ,
                    CASE WHEN item.ItemPyramidCode <> 1
                              OR acct.AccountName = 'Unknown'
                              OR item.ItemNumber = 'Unknown'
                              OR item.ItemObsolete = 'Y'
                              OR item.SpecialInd = 'Y'
                              OR ilg1.ShipmentType = 'D'
                              OR f.TotalQuantity = 0
                              OR f.TotalActualPrice = 0
                              OR f.PriceDerivation IN ( 'SPO', 'CCP' )
                              OR acct.BranchPrimaryBrand = 'BURDN' THEN 'Y'
                         ELSE 'N'
                    END AS ExclusionFlag ,
                    f.TotalActualPrice * pb.Score AS ScoredSales ,
                    f.TotalActualPrice * sc.PriceIndex AS PBScoredSales ,
                    f.GreenPrice * f.TotalQuantity AS [Sales @ Green] ,
                    f.AmberPrice * f.TotalQuantity AS [Sales @ Amber] ,
                    f.RedPrice * f.TotalQuantity AS [Sales @ Red] ,
                    f.TradingPrice * f.TotalQuantity AS [Sales @ Trade] ,
                    ISNULL(f.ClaimNo, '') AS ClaimNumber ,
                    j.InvoiceLineJunkUDVarchar2 + '/'
                    + REPLACE(j.InvoiceLineJunkUDVarchar3, '.00000000', '') AS ChargeNote ,
                    ISNULL(j.InvoiceLineJunkUDVarchar16, '') AS CTSRelCode ,
                    CASE WHEN ISNULL(f.TerminalEmulator, '') LIKE '(%/F)'
                         THEN 'Flynet'
                         ELSE 'Other'
                    END AS TerminalEmulator ,
                    f.PriceGuidanceIndicator ,
                    CASE WHEN ISNULL(f.TerminalEmulator, '') NOT LIKE '(%/F)'
                         THEN 'Non Price Guidance'
                         WHEN f.PriceGuidanceIndicator = 'Y' THEN 'Non Terms'
                         ELSE 'Terms'
                    END AS [Price Source Group] ,
                    CASE WHEN ISNULL(f.TerminalEmulator, '') NOT LIKE '(%/F)'
                         THEN CASE WHEN f.TerminalEmulator LIKE '(%/W%)'
                                        OR f.TerminalEmulator LIKE '(%/PT%)'
                                        OR f.TerminalEmulator IN ( '()',
                                                              '(Stairs)',
                                                              '(PC)' )
                                   THEN 'PowerTerm/wIntegrate'
                                   WHEN f.TerminalEmulator = 'Q' THEN 'Quotes'
                                   WHEN f.TerminalEmulator = 'E'
                                   THEN 'EDI Orders'
                                   ELSE 'Other'
                              END
                         WHEN f.PriceGuidanceIndicator = 'Y' THEN 'Non Terms'
                         ELSE 'Terms'
                    END AS [Price Source] ,
                    CASE WHEN ct.AccountKey IS NOT NULL THEN 1
                         ELSE 0
                    END AS TermsIndicator ,
                    IIF(pb.RPBPlaybookPricingGroupKey IS NULL, 'N', 'Y') AS PriceBand ,
                    flpac.CustomerBrandSegment AS BaseCustomerBrandSegment ,
                    f.CCProfit ,
                    j.InvoiceLineJunkUDVarchar34 AS AccountingBrand ,
                    j.InvoiceLineJunkUDVarchar36 AS BranchBrand ,
                    j.InvoiceLineJunkUDVarchar21 AS TakenBy ,
                    YMTH AS TradingMonth ,
                    ilg1.DeviatedIndicator AS BaseContractClaimsIndicator
          FROM      lion.vwFactInvoiceLine f
                    INNER JOIN dbo.DimDay dd ON f.InvoiceDateDayKey = dd.DayKey
                    INNER JOIN lion.vwItem item ON f.ItemKey = item.ItemKey
                                                   AND f.VendorKey = item.VendorKey
                    INNER JOIN lion.vwAccount acct ON f.AccountKey = acct.AccountKey
                    INNER JOIN lion.vwFactInvoiceLineGroup1 ilg1 ON f.InvoiceLineGroup1Key = ilg1.InvoiceLineGroup1Key
                    INNER JOIN dbo.DimInvoiceLineJunk j ON f.InvoiceLineJunkKey = j.InvoiceLineJunkKey
                    LEFT OUTER JOIN ( SELECT    AccountKey ,
                                                ItemKey ,
                                                InvoiceLineGroup1Key
                                      FROM      lion.vwFactInvoiceLinePricebandScore(NOLOCK)
                                      GROUP BY  AccountKey ,
                                                ItemKey ,
                                                InvoiceLineGroup1Key
                                      HAVING    SUM(TotalActualPrice) = 0
                                                AND SUM(TotalActualCost) <> 0
                                    ) ex ON f.AccountKey = ex.AccountKey
                                            AND f.ItemKey = ex.ItemKey
                                            AND f.InvoiceLineGroup1Key = ex.InvoiceLineGroup1Key
                    INNER JOIN dbo.FactLastPriceAndCost (NOLOCK) flpac ON f.AccountKey = flpac.AccountKey
                                                              AND f.ItemKey = flpac.ItemKey
                                                              AND f.InvoiceLineGroup1Key = flpac.InvoiceLineGroup1Key
                    INNER JOIN dbo.FactInvoiceLine (NOLOCK) fil ON flpac.LastInvoiceLineUniqueIdentifier = fil.InvoiceLineUniqueIdentifier
                    INNER JOIN dbo.FactInvoiceLine (NOLOCK) lt ON flpac.AlternateLastInvoiceLineUniqueIdentifier = lt.InvoiceLineUniqueIdentifier
                    INNER JOIN lion.vwAccountGroup1 ag1 ON ag1.AccountGroup1Key = f.AccountGroup1Key
                    LEFT OUTER JOIN lion.CustomerTerms ct ON ct.AccountKey = f.AccountKey
                                                             AND ct.ItemGroup3Key = f.ItemGroup3Key
                                                             AND ct.ItemKey = 1
                    LEFT OUTER JOIN dbo.FactInvoiceLinePricebandScore pb ON f.InvoiceLineUniqueIdentifier = pb.InvoiceLineUniqueIdentifier
                    CROSS APPLY ppe.fn_GetPriceIndex(( f.TotalActualPrice
                                                       - f.TotalActualCost )
                                                     / NULLIF(f.TotalActualPrice,
                                                              0), FloorGPP,
                                                     TargetGPP, StretchGPP) sc
                    CROSS APPLY lion.fn_GetCappedGPP(f.TotalActualCost
                                                     / NULLIF(f.TotalQuantity,
                                                              0), pb.FloorGPP,
                                                     .99, f.TradingPrice) fm
                    CROSS 
                         APPLY lion.fn_GetCappedGPP(f.TotalActualCost
                                                    / NULLIF(f.TotalQuantity,
                                                             0), pb.TargetGPP,
                                                    .99, f.TradingPrice) tm
                    CROSS APPLY lion.fn_GetCappedGPP(f.TotalActualCost
                                                     / NULLIF(f.TotalQuantity,
                                                              0),
                                                     pb.StretchGPP, .99,
                                                     f.TradingPrice) sm
                    CROSS APPLY lion.fn_GetImpact(( ( ( item.CurrentCost
                                                        - ISNULL(( lt.UDDecimal2
                                                              / NULLIF(f.TotalQuantity,
                                                              0) ), /* CCProfit*/
                                                              0) )
                                                      / NULLIF(( 1 - fm.GPP ),
                                                              0) )
                                                    - ( item.CurrentCost
                                                        - ISNULL(( lt.UDDecimal2
                                                              / NULLIF(f.TotalQuantity,
                                                              0) ), /* CCProfit*/
                                                              0) ) ),
                                                  ( lt.TotalActualPrice
                                                    / NULLIF(lt.TotalQuantity,
                                                             0)
                                                    - lt.TotalActualCost
                                                    / NULLIF(lt.TotalQuantity,
                                                             0) ),
                                                  f.[TotalQuantity]) fi
                    CROSS APPLY lion.fn_GetImpact(( ( ( item.CurrentCost
                                                        - ISNULL(( lt.UDDecimal2
                                                              / NULLIF(f.TotalQuantity,
                                                              0) ), /* CCProfit*/
                                                              0) )
                                                      / NULLIF(( 1 - tm.GPP ),
                                                              0) )
                                                    - ( item.CurrentCost
                                                        - ISNULL(( lt.UDDecimal2
                                                              / NULLIF(f.TotalQuantity,
                                                              0) ), /* CCProfit*/
                                                              0) ) ),
                                                  ( lt.TotalActualPrice
                                                    / NULLIF(lt.TotalQuantity,
                                                             0)
                                                    - lt.TotalActualCost
                                                    / NULLIF(lt.TotalQuantity,
                                                             0) ),
                                                  f.[TotalQuantity]) ti
                    CROSS APPLY lion.fn_GetImpact(( ( ( item.CurrentCost
                                                        - ISNULL(( lt.UDDecimal2
                                                              / NULLIF(f.TotalQuantity,
                                                              0) ), /* CCProfit*/
                                                              0) )
                                                      / NULLIF(( 1 - sm.GPP ),
                                                              0) )
                                                    - ( item.CurrentCost
                                                        - ISNULL(( lt.UDDecimal2
                                                              / NULLIF(f.TotalQuantity,
                                                              0) ), /* CCProfit*/
                                                              0) ) ),
                                                  ( lt.TotalActualPrice
                                                    / NULLIF(lt.TotalQuantity,
                                                             0)
                                                    - lt.TotalActualCost
                                                    / NULLIF(lt.TotalQuantity,
                                                             0) ),
                                                  f.[TotalQuantity]) si
                    CROSS APPLY lion.fn_GetPartialImpact(( ( ( item.CurrentCost
                                                              - ISNULL(( lt.UDDecimal2
                                                              / NULLIF(f.TotalQuantity,
                                                              0) ), /* CCProfit*/
                                                              0) )
                                                             / NULLIF(( 1
                                                              - fm.GPP ), 0) )
                                                           - ( item.CurrentCost
                                                              - ISNULL(( lt.UDDecimal2
                                                              / NULLIF(f.TotalQuantity,
                                                              0) ), /* CCProfit*/
                                                              0) ) ),
                                                         ( ( ( item.CurrentCost
                                                              - ISNULL(( lt.UDDecimal2
                                                              / NULLIF(f.TotalQuantity,
                                                              0) ), /* CCProfit*/
                                                              0) )
                                                             / NULLIF(( 1
                                                              - tm.GPP ), 0) )
                                                           - ( item.CurrentCost
                                                              - ISNULL(( lt.UDDecimal2
                                                              / NULLIF(f.TotalQuantity,
                                                              0) ), /* CCProfit*/
                                                              0) ) ),
                                                         ( lt.TotalActualPrice
                                                           / NULLIF(lt.TotalQuantity,
                                                              0)
                                                           - lt.TotalActualCost
                                                           / NULLIF(lt.TotalQuantity,
                                                              0) ),
                                                         f.[TotalQuantity]) AS fti
                    CROSS APPLY lion.fn_GetPartialImpact(( ( ( item.CurrentCost
                                                              - ISNULL(( lt.UDDecimal2
                                                              / NULLIF(f.TotalQuantity,
                                                              0) ), /* CCProfit*/
                                                              0) )
                                                             / NULLIF(( 1
                                                              - tm.GPP ), 0) )
                                                           - ( item.CurrentCost
                                                              - ISNULL(( lt.UDDecimal2
                                                              / NULLIF(f.TotalQuantity,
                                                              0) ), /* CCProfit*/
                                                              0) ) ),
                                                         ( ( ( item.CurrentCost
                                                              - ISNULL(( lt.UDDecimal2
                                                              / NULLIF(f.TotalQuantity,
                                                              0) ), /* CCProfit*/
                                                              0) )
                                                             / NULLIF(( 1
                                                              - sm.GPP ), 0) )
                                                           - ( item.CurrentCost
                                                              - ISNULL(( lt.UDDecimal2
                                                              / NULLIF(f.TotalQuantity,
                                                              0) ), /* CCProfit*/
                                                              0) ) ),
                                                         ( lt.TotalActualPrice
                                                           / NULLIF(lt.TotalQuantity,
                                                              0)
                                                           - lt.TotalActualCost
                                                           / NULLIF(lt.TotalQuantity,
                                                              0) ),
                                                         f.[TotalQuantity]) AS tsi
                    CROSS APPLY lion.fn_GetRisk(( ( ( item.CurrentCost
                                                      - ISNULL(( lt.UDDecimal2
                                                              / NULLIF(f.TotalQuantity,
                                                              0) ), /* CCProfit*/
                                                              0) )
                                                    / NULLIF(( 1 - sm.GPP ), 0) )
                                                  - ( item.CurrentCost
                                                      - ISNULL(( lt.UDDecimal2
                                                              / NULLIF(f.TotalQuantity,
                                                              0) ), /* CCProfit*/
                                                              0) ) ),
                                                ( lt.TotalActualPrice
                                                  / NULLIF(lt.TotalQuantity, 0)
                                                  - lt.TotalActualCost
                                                  / NULLIF(lt.TotalQuantity, 0) ),
                                                f.[TotalQuantity]) r
                    CROSS APPLY lion.fn_GetRisk(f.TradingPrice,
                                                f.TotalActualPrice
                                                / NULLIF(f.TotalQuantity, 0),
                                                f.TotalQuantity) tr
          WHERE     ex.AccountKey IS NULL
                    AND item.ItemPyramidCode = '1'
                    AND BranchBrand IN ( 'BURDENS', 'FUSION', 'MPS', 'UPS' )
                    AND datepart(wk,sqldate) =datepart(wk,dateadd(wk,-1,getdate()))
					 ) d
GROUP BY branch ,
        branchname ,
        Score

		)
GO
