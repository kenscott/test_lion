SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




CREATE
VIEW [lion].[vwPriceBandReview]

/*** OBSOLETE ***/
-- I THINK --

/*
SELECT TOP 0 * FROM lion.vwPriceBandReview 
SELECT TOP 10 * FROM lion.vwPriceBandReview 
*/


AS

SELECT
	flpac.[AccountKey],
	flpac.[ItemKey],
	flpac.[InvoiceLineGroup1Key],

	dag1.AG1Level1UDVarchar2 AS CustomerPlaybookRegion,

	--dag1.AG1Level1UDVarchar20 AS CustomerBranchPlaybookBrandIndicator,

	--AccountUDVarChar10 AS CustomerSegment,
	dag1.AG1Level1UDVarchar25 AS RegionExceptionIndicator,

	ItemNumber,
	ItemDescription,

	ItemUDVarChar1 AS ItemPyramidCode,
	ItemUDVarChar21 AS PlaybookProductBrand,

	IG3Level1 AS LLSPGCode,--1
	IG3Level2 AS HLSPGCode,--2
	IG3Level3 AS GSPGCode,--3
	--IG3Level4 AS PYRCode,--4
	IG3Level1UDVarchar1 AS LLSPGDescription,--1
	--IG3Level2UDVarchar1 AS HLSPGDescription,--2
	--IG3Level3UDVarchar1 AS GSPGDescription, --3

	InvoiceLineGroup1UDVarchar1 AS ShipmentType,
	InvoiceLineGroup1UDVarchar2 AS ContractClaimsIndicator,  -- formally DeviatedIndicator

	ISNULL(flpac.UDVarchar1, '') AS StrategicItem,
	--ISNULL(flpac.UDVarchar2, '') AS SubLLSPG,
	--ISNULL(flpac.UDVarchar3, '') AS RegionExceptionIndicator,
	

	ISNULL(flpac.UDVarchar4, '') AS Sales603010Bucket,
	--ISNULL(flpac.UDVarchar5, '') AS TotalSalesAccountBucket,
	--ISNULL(flpac.UDVarchar6, '') AS FrequencyBucket,
	--ISNULL(flpac.UDVarchar7, '') AS CustBaseBucket,
	--ISNULL(flpac.UDVarchar8, '') AS FreqPurchBucket,
	--ISNULL(flpac.UDVarchar9, '') AS TopSellerBucket,
	flpac.UDVarchar10 AS PriceApproach,
	flpac.CustomerBrandSegment,
	flpac.CustomerSize,

	--ISNULL(flpac.UDDecimal1, 0.) AS TotalAccountSales,
	--ISNULL(flpac.UDDecimal2, 0.) AS TotalNumberOfItemsSoldByAccount,
	--ISNULL(flpac.UDDecimal3, 0.) AS TotalAccountGP,
	--ISNULL(flpac.UDDecimal4, 0.) AS TotalNumberOfSuperCategoriesSold,

	--ISNULL(flpac.UDDecimal5, 0.) AS LastUnitListPrice,
	--ISNULL(flpac.UDDecimal6, 0.) AS LastUnitRetail,

	CASE
		WHEN ItemUDVarChar1 <> N'1' THEN N'Y'		-- pyramid code
		WHEN ISNULL((flpac.LastItemPrice - flpac.LastItemCost) / NULLIF(flpac.LastItemPrice, 0.0), 0.0) <= 0. THEN N'Y'	-- LastGPP
		WHEN ISNULL(ItemUDVarChar10, N'') = N'Y' THEN N'Y'		-- SpecialInd
		WHEN ISNULL(ItemUDVarChar8, N'') IN (N'Y', N'YY') THEN N'Y'		-- Obsolete
		WHEN InvoiceLineGroup1UDVarchar1 NOT IN (N'S') THEN N'Y'
		-----WHEN filsum.TotalActualPrice < 10. THEN N'Y'
		WHEN AccountName = N'Unknown' THEN N'Y'
		WHEN ItemNumber = N'Unknown' THEN N'Y'
		WHEN flpac.LastQuantity < 0 THEN N'Y'
		WHEN flpac.LastItemPrice = 0 THEN N'Y'
		--WHEN ISNULL(fil.UDVarchar1, '') IN (N'SPO') THEN N'Y'  --last price derivation code
		WHEN ISNULL(InvoiceLineGroup2UDVarchar1, '') IN (N'SPO', N'CCP', N'QB') THEN 'Y'  --last price derivation code
		ELSE 'N'
	END AS ExcludeForSegmentationIndicator,

	CONVERT (VARCHAR(10), dd.SQLDate, 120) AS LastInvoiceDate,

	flpac.LastQuantity,
	flpac.LastItemPrice,
	flpac.LastItemCost,
	flpac.LastItemDiscountPercent,

	--flpac.LastItemPrice - flpac.LastItemCost AS LastItemGP,
	(flpac.LastItemPrice - flpac.LastItemCost) / NULLIF(flpac.LastItemPrice, 0.0) AS LastItemGPP,

	flpac.Total12MonthInvoiceLines AS Total12MonthInvoiceLineCount,
	flpac.Total12MonthQuantity AS Total12MonthQuantity,
	flpac.Total12MonthSales AS Total12MonthSales,
	flpac.Total12MonthCost AS Total12MonthCost,

	ISNULL(flpac.Total12MonthSales - flpac.Total12MonthCost, 0.0) AS Total12MonthGP,
	ISNULL((flpac.Total12MonthSales - flpac.Total12MonthCost) / NULLIF(flpac.Total12MonthSales, 0.0), 0.0) AS Total12MonthGPP,

	--InvoiceLineGroup2UDVarchar1 AS LastDeviatedIndicator,  -- ClaimNo based... Supplier Contract; If Transaction.ClaimNo <> <blank> then "Y", else "N"
	InvoiceLineGroup2UDVarchar1 AS LastPriceDerivationBucket,
	--InvoiceLineGroup3UDVarchar1 AS LastCustomerContractIndicator,	-- If Transaction.PriceDerivation = {DC, FM, GT, IP, OCD, OCL, OTG} then "Y", else "N"


	ISNULL(fil.UnitListPrice, 0.) AS LastTradingPrice,
	--ROUND((ISNULL(fil.UnitListPrice, 0.) - LastItemCost)  / NULLIF(ISNULL(fil.UnitListPrice, 0.), 0),3) AS LastTradeGPP,
	--ISNULL(
	--	CASE
	--		WHEN TotalQuantity IS NOT NULL AND TotalQuantity > 0. 
	--		AND UnitListPrice IS NOT NULL AND UnitListPrice > 0. 
	--		AND TotalActualPrice IS NOT NULL AND TotalActualPrice > 0.
	--		THEN 
	--			(UnitListPrice * flpac.LastQuantity) - (LastItemPrice * flpac.LastQuantity)
	--		ELSE
	--			NULL
	--	END
	--	, 0.) 
	--AS LastTotalDiscount,

	--fil.UDVarchar1 AS LastPriceDerivation,
	--fil.UDVarchar2 AS LastVariablePackInd,
	--fil.UDVarchar3 AS LastClaimNo,

	--fil.UDDecimal1 AS LastApTradingMarginPadRelease,
	--fil.UDDecimal2 AS LastCCProfit,
	--fil.UDDecimal3 AS LastRetailPrice,

	RPBPlaybookPricingGroupKey,

	ppg.GroupValues AS CoveringBandGroupValues,
	apr.PricingRuleSequenceNumber,
	PricingRuleDescription,
	ScenarioName,

	--FloorDiscount AS EffectiveFloorDiscountPercent,
	--TargetDiscount AS EffectiveTargetDiscountPercent,
	--StretchDiscount AS EffectiveStretchDiscountPercent,

	--PBFloorGPP AS FloorGPP,
	--PBTargetGPP AS TargetGPP,	
	--PBStretchGPP AS StretchGPP

	a.FloorGPP,
	a.TargetGPP,	
	a.StretchGPP

FROM
	dbo.FactLastPriceAndCost flpac (NOLOCK)
--INNER JOIN dbo.DimInvoiceLineJunk diljunk (NOLOCK)
--	ON diljunk.InvoiceLineJunkKey = flpac.LastInvoiceLineJunkKey
INNER JOIN dbo.DimDay dd (NOLOCK)
	ON dd.daykey = flpac.LastSaleDayKey
INNER JOIN dbo.DimAccount da (NOLOCK)
	ON da.AccountKey = flpac.AccountKey
INNER JOIN dbo.DimAccountGroup1 dag1 (NOLOCK)
	ON dag1.AccountGroup1Key = da.AccountGroup1Key
--INNER JOIN dbo.DimAccountGroup2 dag2 (NOLOCK)
--	ON dag2.AccountGroup2Key = da.AccountGroup2Key
--INNER JOIN dbo.DimAccountGroup3 dag3 (NOLOCK)
--	ON dag3.AccountGroup3Key = da.AccountGroup3Key
--INNER JOIN dbo.DimAccountManager dam (NOLOCK)
--	ON dam.AccountManagerKey = da.AccountManagerKey
--JOIN dbo.DimPerson dp (NOLOCK)
--	ON dp.DimPersonKey = dam.DimPersonKey
--LEFT JOIN dbo.BridgeAccountManager bam (NOLOCK)
--	ON bam.SubsidiaryAccountManagerKey = da.AccountManagerKey
--	AND NumberOfLevels = 1
--LEFT JOIN dbo.WebUser wuMgr (NOLOCK)
--	ON wuMgr.AccountManagerKey = bam.ParentAccountManagerKey
--LEFT JOIN dbo.ODSPerson odspMgr (NOLOCK)
--	ON odspMgr.ODSPersonKey = wuMgr.ODSPersonKey
INNER JOIN dbo.DimItem di (NOLOCK)
	ON di.ItemKey = flpac.ItemKey
--INNER JOIN dbo.DimItemGroup1 dig1 (NOLOCK)
--	ON dig1.ItemGroup1Key = di.ItemGroup1Key
--INNER JOIN dbo.DimItemGroup2 dig2 (NOLOCK)
--	ON dig2.ItemGroup2Key = di.ItemGroup2Key
INNER JOIN dbo.DimItemGroup3 dig3 (NOLOCK)
	ON dig3.ItemGroup3Key = di.ItemGroup3Key
--INNER JOIN dbo.DimVendor (NOLOCK)
--	ON DimVendor.VendorKey = di.ItemVendorKey
INNER JOIN dbo.DimInvoiceLineGroup1 (NOLOCK)
	ON DimInvoiceLineGroup1.InvoiceLineGroup1Key = flpac.InvoiceLineGroup1Key
INNER JOIN dbo.DimInvoiceLineGroup2 (NOLOCK)
	ON DimInvoiceLineGroup2.InvoiceLineGroup2Key = flpac.LastInvoiceLineGroup2Key
INNER JOIN dbo.DimInvoiceLineGroup3 (NOLOCK)
	ON DimInvoiceLineGroup3.InvoiceLineGroup3Key = flpac.LastInvoiceLineGroup3Key
--INNER JOIN (
--			SELECT 
--				AccountKey, 
--				ItemKey, 
--				InvoiceLineGroup1Key,
--				SUM(TotalQuantity) TotalQuantity,
--				COUNT(*) Cnt,
--				SUM(TotalActualCost) TotalActualCost,
--				SUM(TotalActualPrice) TotalActualPrice
--			FROM dbo.FactInvoiceLine fil (NOLOCK) 
--			WHERE Last12MonthsIndicator = 'Y'
--			GROUP BY 
--				AccountKey, 
--				ItemKey, 
--				InvoiceLineGroup1Key
--		) FILSUM
----INNER JOIN  FILSUM
--		ON FILSUM.AccountKey = flpac.AccountKey 
--		AND FILSUM.ItemKey = flpac.ItemKey 
--		AND FILSUM.InvoiceLineGroup1Key = flpac.InvoiceLineGroup1Key		
LEFT JOIN dbo.FactInvoiceLine fil
	ON flpac.LastInvoiceLineUniqueIdentifier = fil.InvoiceLineUniqueIdentifier
LEFT JOIN dbo.DimAccountGroup1 dag1Sale
	ON dag1Sale.AccountGroup1Key = fil.AccountGroup1Key

INNER JOIN dbo.PlaybookPricingGroup ppg (NOLOCK)
	ON ppg.PlaybookPricingGroupKey = flpac.RPBPlaybookPricingGroupKey
INNER JOIN dbo.ATKPricingRule apr (NOLOCK)
	ON ppg.PricingRuleKey = apr.PricingRuleKey

INNER JOIN dbo.ATKScenario Scenario
	ON Scenario.PricingRulePlaybookKey = apr.PricingRulePlaybookKey

--INNER JOIN dbo.FactRecommendedPriceBand pb
--	ON pb.AccountKey = flpac.AccountKey
--	AND pb.ItemKey = flpac.ItemKey
--	AND pb.InvoiceLineGroup1Key = flpac.InvoiceLineGroup1Key

OUTER APPLY lion.fn_GetPercentile (
		di.ItemUDVarchar2,				-- product brand
		dag1.AG1Level4,					-- Region,
		dag1.AG1Level3,					-- Area,
		dag1.AG1Level2,					-- Network
		dag1.AG1Level1,					-- Branch,
		flpac.UDVarchar4,				-- Sales603010Bucket
		ItemUDVarChar1,					-- ItemPyramidCode
		IG3Level1						-- LLSPGCode
	)
OUTER APPLY lion.fn_GetRPBPercentages (
	flpac.RPBPlaybookPricingGroupKey,
	flpac.Total12MonthQuantity,
	FloorPercentile,
	TargetPercentile,
	StretchPercentile) a



--LEFT JOIN dbo.PlaybookPricingGroupPriceBand TheFloor (NOLOCK)
--	ON TheFloor.PlaybookPricingGroupKey = flpac.RPBPlaybookPricingGroupKey
--	AND flpac.Total12MonthQuantity BETWEEN TheFloor.LowerBandQuantity AND TheFloor.UpperBandQuantity 
--	AND TheFloor.MemberRank = 
--		CASE
--			WHEN  ROUND ( TheFloor.RulingMemberCount * FloorPercentile, 0) BETWEEN 1 AND TheFloor.RulingMemberCount THEN  ROUND ( TheFloor.RulingMemberCount * FloorPercentile, 0)
--			ELSE 1
--		END
--LEFT JOIN dbo.PlaybookPricingGroup ppg (NOLOCK)
--	ON ppg.PlaybookPricingGroupKey = TheFloor.PlaybookPricingGroupKey
--LEFT JOIN dbo.PlaybookPricingGroupPriceBand TheTarget (NOLOCK)
--	ON TheTarget.PlaybookPricingGroupKey = flpac.RPBPlaybookPricingGroupKey
--	AND flpac.Total12MonthQuantity BETWEEN TheTarget.LowerBandQuantity AND TheTarget.UpperBandQuantity 
--	AND TheTarget.MemberRank = 
--		CASE
--			WHEN  ROUND ( TheTarget.RulingMemberCount * TargetPercentile, 0) BETWEEN 1 AND TheTarget.RulingMemberCount THEN  ROUND ( TheTarget.RulingMemberCount * TargetPercentile, 0)
--			ELSE 1
--		END
--LEFT JOIN dbo.PlaybookPricingGroupPriceBand TheStretch (NOLOCK)
--	ON TheStretch.PlaybookPricingGroupKey = flpac.RPBPlaybookPricingGroupKey
--	AND flpac.Total12MonthQuantity BETWEEN TheStretch.LowerBandQuantity AND TheStretch.UpperBandQuantity 
--	AND TheStretch.MemberRank = 
--		CASE
--			WHEN  ROUND ( TheStretch.RulingMemberCount * StretchPercentile, 0) BETWEEN 1 AND TheStretch.RulingMemberCount THEN  ROUND ( TheStretch.RulingMemberCount * StretchPercentile, 0)
--			ELSE 1
--		END



--LEFT JOIN dbo.PlaybookPricingGroupPriceBand TheFloorDiscount (NOLOCK)
--	ON TheFloorDiscount.PlaybookPricingGroupKey = flpac.RPBPlaybookPricingGroupKey
--	AND flpac.Total12MonthQuantity BETWEEN TheFloorDiscount.LowerBandQuantity AND TheFloorDiscount.UpperBandQuantity 
--	AND TheFloorDiscount.MemberRankDiscount = 
--		CASE
--			WHEN  ROUND ( TheFloorDiscount.RulingMemberCount * FloorPercentile, 0) BETWEEN 1 AND TheFloorDiscount.RulingMemberCount THEN  ROUND ( TheFloorDiscount.RulingMemberCount * FloorPercentile, 0)
--			ELSE 1
--		END
--LEFT JOIN dbo.PlaybookPricingGroupPriceBand TheTargetDiscount (NOLOCK)
--	ON TheTargetDiscount.PlaybookPricingGroupKey = flpac.RPBPlaybookPricingGroupKey
--	AND flpac.Total12MonthQuantity BETWEEN TheTargetDiscount.LowerBandQuantity AND TheTargetDiscount.UpperBandQuantity 
--	AND TheTargetDiscount.MemberRankDiscount = 
--		CASE
--			WHEN  ROUND ( TheTargetDiscount.RulingMemberCount * TargetPercentile, 0) BETWEEN 1 AND TheTargetDiscount.RulingMemberCount THEN  ROUND ( TheTargetDiscount.RulingMemberCount * TargetPercentile, 0)
--			ELSE 1
--		END
--LEFT JOIN dbo.PlaybookPricingGroupPriceBand TheStretchDiscount (NOLOCK)
--	ON TheStretchDiscount.PlaybookPricingGroupKey = flpac.RPBPlaybookPricingGroupKey
--	AND flpac.Total12MonthQuantity BETWEEN TheStretchDiscount.LowerBandQuantity AND TheStretchDiscount.UpperBandQuantity 
--	AND TheStretchDiscount.MemberRankDiscount = 
--		CASE
--			WHEN  ROUND ( TheStretchDiscount.RulingMemberCount * StretchPercentile, 0) BETWEEN 1 AND TheStretchDiscount.RulingMemberCount THEN  ROUND ( TheStretchDiscount.RulingMemberCount * StretchPercentile, 0)
--			ELSE 1
--		END

WHERE
	[Total12MonthSales] > 0 
    AND RPBPlaybookPricingGroupKey IS NOT NULL




















GO
