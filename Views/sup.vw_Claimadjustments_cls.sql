SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE VIEW [sup].[vw_Claimadjustments_cls]

AS

--View for Claim Line Screen in supplier portal, adjustments
--LSP-149, create view in db for Claim Adjustments
/**
LSP-179, Claims Lines | Adjustments | Table should only shown claim types A or W
The Adjustments table on the Claim Lines screen should only be showing Claim Types = A or W.
Additionally, it looks like the Contract ID and WUK Contract ID columns are currently swapped. 
**/
	SELECT cj.OriginalInvoiceClaimKey AS originalInvoice
		, ic.CreationDate AS OriginalInvoiceDate
		--, cj.CurrentContractID AS ContractID   --LSP-179
		--, cj.SupplierRef AS WolseleyContractID --LSP-179
		, cj.CurrentContractID AS  WolseleyContractID
		, cj.SupplierRef AS ContractID
		, cj.ClaimType 
		, f.BranchCode AS Location
		, f.BranchDescription AS LocationDescription
		, f.AccountName AS Account
		, f.ProductCode AS Product
		, f.ProductDescription
		, cj.ClaimLineValue
		, cj.ClaimMonth
		, cj.InvoiceClaimKey
		, CAST(cj.InvoiceClaimKey AS VARCHAR(50)) AS str_invoiceclaimkey
		, f.VendorNumber
		, cj.ClaimJournalKey
	FROM cm.ClaimJournal cj INNER JOIN
	cm.FactInvoiceLine f ON f.ClientInvoiceLineUniqueID = cj.ClientInvoiceLineUniqueID
	LEFT JOIN cm.InvoiceClaim ic ON cj.OriginalInvoiceClaimKey = ic.InvoiceClaimKey
	WHERE cj.ClaimType = 'A' OR cj.ClaimType = 'W'  --LSP-179 Claims Lines | Adjustments | Table should only shown claim types A or W



	
GO
