SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE VIEW [lion].[vwTermsBenefitDetails]
AS

select a.AccountManagerFullName as [Account Owner], IIF(a.AccountOwnerType='', 'Branch', a.AccountOwnerType) as [Account Owner Type], a.AccountNumber, a.AccountName, i.LLSPGCode, i.LLSPGDescription, substring(i.ItemNumber, 2, 255) as ItemNumber, i.ItemDescription, 
       ag1.Region, ag1.RegionDescription, ag1.Area, ag1.AreaDescription, ag1.Network, ag1.NetworkDescription,
	   ag1.Branch, ag1.BranchName, dd.SQLDate as DeliveryDate,
	   dilj.InvoiceLineJunkUDVarchar2 + '/' + replace(dilj.InvoiceLineJunkUDVarchar3,'.00000000','') as ChargeNote,
	   f.PriceDerivation,
	   f.TotalQuantity, f.TotalActualPrice, f.TotalActualCost, f.TotalActualPrice - f.TotalActualCost as GP,
	   (f.TotalActualPrice - f.TotalActualCost) / NULLIF(f.TotalActualPrice, 0) as GPP, tb.[Actual Benefit],
	   tb.RuleNumber, tb.[Potential Benefit], tt.OriginalDiscount, tt.NewDiscount,
	   ROUND((f.TotalQuantity * f.TradingPrice - f.TotalActualPrice) / NULLIF((f.TotalQuantity * f.TradingPrice), 0), 4) as ActualDiscount,
	   tt.NewException, f.ClientInvoiceLineUniqueID
  from lion.TermsBenefits tb inner join
       lion.vwFactInvoiceLine f on f.ClientInvoiceLineUniqueID = tb.ClientInvoiceLineUniqueID inner join
	   lion.vwAccount a on f.AccountKey = a.AccountKey inner join
	   lion.vwItem i on f.ItemKey = i.ItemKey inner join
	   lion.vwAccountGroup1 ag1 on f.AccountGroup1Key = ag1.AccountGroup1Key inner join
	   dbo.DimDay dd on f.InvoiceDateDayKey = dd.DayKey inner join
	   dbo.DimInvoiceLineJunk dilj on f.InvoiceLineJunkKey = dilj.InvoiceLineJunkKey inner join
       lion.TermsTracker tt on tt.ID = tb.TermsTrackerID  and tt.CurrentIndicator = 1
GO
