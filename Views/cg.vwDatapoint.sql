SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO









CREATE 
VIEW [cg].[vwDatapoint]

AS

/*

SELECT TOP 10 * FROM cg.vwDatapoint

select top 10 * from cg.PlaybookDatapoint

select count(*) from cg.vwDatapoint

*/

SELECT DISTINCT

	pdp.PlaybookDataPointKey,
	pdp.PlaybookDataPointGroupKey,
	pdp.ContractKey,
	pdp.AccountKey,
	pdp.InstructionLevelType,
	pdp.LLSPGCode,
	pdp.MPGCode,
	pdp.ItemNumber,
	pdp.InstructionLevelCode,
	pdp.ClaimBackPercent,
	pdp.Sales603010Bucket,
	pdppg.SectionNumber
	,

	dc.Region AS ContractRegion,
	dc.Originator AS ContractOriginator,
	dc.ContractOwner,
	dc.ContractNumber,
	dc.ContractReference,
	dc.StartDate AS ContractStartDate,
	dc.ExpirationDate AS ContractExpirationDate,
	dc.JobSite,
	dc.JobFlag,
	dc.PyramidCode,
	--dc.Narrative1,	-- replaced by cc.SupplierContractNote table
	--dc.Narrative2,
	--dc.Narrative3,
	--dc.Narrative4,
	dc.ContractType1,
	dc.ContractLevel,
	dc.FullContractNumber,
	dc.CCProfit,

	dv.VendorKey AS VendorKey,
	dv.VendorDescription,
	dv.VendorNumber,

	da.AccountNumber,
	da.AccountName,
	da.AccountNumberParent,
	da.AccountAddress1,
	da.AccountAddress2,
	da.AccountAddress3,
	da.AccountAddress4,
	da.AccountZipCode AS AccountPostCode,
	da.AccountUDVarChar1 AS CreditLimit,
	da.AccountUDVarChar2 AS PaymentTermName,
	da.AccountUDVarChar3 AS IntercompanyAccount,
	da.AccountUDVarChar4 AS OverLimitStatus,
	da.AccountUDVarChar5 AS OverdueStatus,
	da.AccountUDVarChar6 AS TermsParent,
	da.AccountUDVarChar7 AS PricingCategory,
	da.AccountUDVarChar8 AS AccountOwnerType,
	da.AccountUDVarChar9 AS AccountOwnerBucket,
	da.AccountUDVarChar10 AS CustomerSegment,
	da.AccountUDVarChar11 AS CustomerRebateIndicator,
	da.AccountUDVarChar12 AS FixedPriceIndicator,
	da.AccountUDVarChar13 AS ContractName,
	da.AccountUDDateTime1 AS DateOpened,
	da.AccountUDDateTime2 AS DateClosed,
	da.Inactive AS AccountInactive,

	da.AccountManagerKey AS AccountManagerKey,
	dam.AccountManagerCode,
	dp.FullName AS AccountManagerFullName,
	dp.Email AS AccountManagerEmail,



	AG1Level1 AS Branch,
	AG1Level2 AS Network,
	AG1Level3 AS Area,
	AG1Level4 AS Region,	

	AG1Level1UDVarchar1 AS BranchName,
	AG1Level2UDVarchar1 AS NetworkDescription,
	AG1Level3UDVarchar1 AS AreaDescription,
	AG1Level4UDVarchar1 AS RegionDescription,
	

	AG1Level1UDVarchar2 AS PlaybookRegion,
	AG1Level1UDVarchar3 AS PlaybookRegionDescription,
	AG1Level1UDVarchar4 AS PlaybookArea,
	AG1Level1UDVarchar5 AS PlaybookAreaDescription,
	AG1Level1UDVarchar6 AS BranchPrimaryBrand,
	AG1Level1UDVarchar7 AS BranchTradingStatus,
	AG1Level1UDVarchar8 AS BranchType,
	AG1Level1UDVarchar9 AS BranchAddressLine1,
	AG1Level1UDVarchar10 AS BranchAddressLine2,
	AG1Level1UDVarchar11 AS BranchAddressLine3,
	AG1Level1UDVarchar12 AS BranchAddressLine4,
	AG1Level1UDVarchar13 AS BranchAddressLine5,
	AG1Level1UDVarchar14 AS BranchPostcode,
	AG1Level1UDVarchar15 AS BranchEmail,
	AG1Level1UDVarchar16 AS BranchTelephone,
	AG1Level1UDVarchar17 AS CustomerBranchPostCodeArea,
	AG1Level1UDVarchar18 AS CustomerBranchPostCodeDistrict,
	AG1Level1UDVarchar19 AS BranchPyramidCode,
	AG1Level1UDVarchar21 AS BrandGroup,
	AG1Level1UDVarchar22 AS PGActiveIndicator,
	AG1Level1UDVarchar23 AS Manager,
	AG1Level1UDVarchar24 AS BranchManagerEmail,
	AG1Level1UDVarchar25 AS RegionExceptionIndicator,
	AG1Level1UDVarchar26 AS BusinessUnitRegion,

	AG2Level1 AS CustomerType,
	AG2Level1UDVarchar1 AS CustomerTypeDescription,

	AG3Level1 AS TrandingStatusCategory,
	AG3Level1UDVarchar1 AS TrandingStatusCategoryDescription,


	di.ItemDescription,
	di.VendorStockNumber,
	di.ItemUDVarChar2 AS ProdBrandAtLLSPG,
	di.CurrentCost,
	di.CurrentInvoiceCost,
	di.InActive AS ItemInactive,
	
	pdppg.PlaybookPricingGroupKey,
	
	ppg.GroupingColumns AS CoveringBandGroupingColumns, 
	ppg.GroupValues AS CoveringBandGroupValues,

	pdppg.RulingMemberFlag,
	ppgpb.PricingRuleSequenceNumber,
	ppgpb.RulingMemberCount,
	ppgpb.MemberRank,

	--apr.PricingRuleSequenceNumber,
	PricingRuleDescription,
	ScenarioName,
	
	TheFloor.ClaimBackPercent AS   FloorClaimBackPercent,
	TheTarget.ClaimBackPercent AS  TargetClaimBackPercent,
	TheStretch.ClaimBackPercent AS StretchClaimBackPercent

	 --,ROUND ( TheFloor.RulingMemberCount * 0.25, 0) as a
	 --,ROUND ( TheTarget.RulingMemberCount * 0.60, 0) as b
	 --,ROUND ( TheStretch.RulingMemberCount * 0.80, 0) as c
	 --,TheStretch.PlaybookPricingGroupKey as d

FROM cg.PlaybookDataPoint pdp (NOLOCK)
LEFT JOIN cg.PlaybookDataPointPricingGroup pdppg (NOLOCK)
	ON pdp.PlaybookDataPointKey = pdppg.PlaybookDataPointKey
LEFT JOIN dbo.PlaybookPricingGroup ppg (NOLOCK)
	ON ppg.PlaybookPricingGroupKey = pdppg.PlaybookPricingGroupKey

LEFT JOIN cg.PlaybookPricingGroupPriceBand ppgpb (NOLOCK)
	ON ppgpb.PlaybookPricingGroupKey = pdppg.PlaybookPricingGroupKey
	AND ppgpb.PlaybookDataPointKey = pdp.PlaybookDataPointKey

LEFT JOIN dbo.ATKPricingRule apr (NOLOCK)
	ON ppg.PricingRuleKey = apr.PricingRuleKey
LEFT JOIN dbo.ATKScenario Scenario (NOLOCK)
	ON Scenario.PricingRulePlaybookKey = apr.PricingRulePlaybookKey
LEFT JOIN dbo.DimAccount da (NOLOCK)
	ON da.AccountKey = pdp.AccountKey
LEFT JOIN dbo.DimAccountGroup1 dag1 (NOLOCK)
	ON dag1.AccountGroup1Key = da.AccountGroup1Key
LEFT JOIN dbo.DimAccountGroup2 dag2 (NOLOCK)
	ON dag2.AccountGroup2Key = da.AccountGroup2Key
LEFT JOIN dbo.DimAccountGroup3 dag3 (NOLOCK)
	ON dag3.AccountGroup3Key = da.AccountGroup3Key
LEFT JOIN dbo.DimAccountManager dam (NOLOCK)
	ON dam.AccountManagerKey = da.AccountManagerKey
LEFT JOIN dbo.DimPerson dp (NOLOCK)
	ON dp.DimPersonKey = dam.DimPersonKey
LEFT JOIN dbo.DimItem di (NOLOCK)
	ON di.ItemNumber = pdp.ItemNumber
	AND pdp.InstructionLevelType = 'Item'
LEFT JOIN dbo.DimItemGroup1 dig1 (NOLOCK)
	ON dig1.ItemGroup1Key = di.ItemGroup1Key
LEFT JOIN dbo.DimItemGroup2 dig2 (NOLOCK)
	ON dig2.ItemGroup2Key = di.ItemGroup2Key
LEFT JOIN dbo.DimItemGroup3 dig3 (NOLOCK)
	ON dig3.ItemGroup3Key = di.ItemGroup3Key
LEFT JOIN cc.SupplierContract dc (NOLOCK)
	ON dc.ContractKey = pdp.ContractKey
LEFT JOIN dbo.DimVendor dv (NOLOCK)
	ON dv.VendorKey = dc.VendorKey

LEFT JOIN cg.PlaybookPricingGroupPriceBand TheFloor (NOLOCK)
	ON TheFloor.PlaybookPricingGroupKey = pdppg.PlaybookPricingGroupKey --?
	AND TheFloor.MemberRank = 
		CASE
			WHEN  ROUND ( TheFloor.RulingMemberCount * 0.25, 0) BETWEEN 1 AND TheFloor.RulingMemberCount THEN  ROUND ( TheFloor.RulingMemberCount * 0.25, 0)
			ELSE 1
		END
LEFT JOIN cg.PlaybookPricingGroupPriceBand TheTarget (NOLOCK)
	ON TheTarget.PlaybookPricingGroupKey = pdppg.PlaybookPricingGroupKey --?
	AND TheTarget.MemberRank = 
		CASE
			WHEN  ROUND ( TheTarget.RulingMemberCount * 0.60, 0) BETWEEN 1 AND TheTarget.RulingMemberCount THEN  ROUND ( TheTarget.RulingMemberCount * 0.60, 0)
			ELSE 1
		END
LEFT JOIN cg.PlaybookPricingGroupPriceBand TheStretch (NOLOCK)
	ON TheStretch.PlaybookPricingGroupKey = pdppg.PlaybookPricingGroupKey --?
	AND TheStretch.MemberRank = 
		CASE
			WHEN  ROUND ( TheStretch.RulingMemberCount * 0.80, 0) BETWEEN 1 AND TheStretch.RulingMemberCount THEN  ROUND ( TheStretch.RulingMemberCount * 0.80, 0)
			ELSE 1
		END

WHERE 
	--RulingMemberFlag = 'Y'
	--AND pdppg.LastItemPrice <> 0.0
	--AND (
	pdp.PlaybookDataPointGroupKey = (
			SELECT 
				MAX(PlaybookDataPointGroupKey)
			FROM dbo.ATKScenario a
			INNER JOIN dbo.ATKProjectSpecificationScenario c ON c.ScenarioKey = a.ScenarioKey
			INNER JOIN dbo.PlaybookProject pp
				ON pp.ProjectSpecificationKey = c.ProjectSpecificationKey
			INNER JOIN dbo.PlaybookDataPointGroup pdpg
				ON pdpg.ProjectSpecificationScenarioKey = c.ProjectSpecificationScenarioKey
				AND pdpg.ProjectKey = pp.ProjectKey
			WHERE 
				a.ApproachTypeKey = 12	-- Cost Guidance
				--AND pp.ProductionProjectIndicator = 'Y'
			)
	










GO
