SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO






CREATE 
VIEW [lion].[vwAccountLLSPG] 

/*
SELECT TOP 10 * FROM lion.vwAccountLLSPG 
WHERE ContractClaimsPercent > 0
*/


AS

SELECT DISTINCT
	faig3.AccountKey,
	faig3.ItemGroup3Key,
	faig3.ContractClaimsPercent
FROM dbo.FactAccountItemGroup3 faig3 WITH (NOLOCK)
/*
INNER JOIN dbo.DimAccount da (NOLOCK)
	ON da.AccountKey = faig3.AccountKey
JOIN dbo.DimItemGroup3 dag3 (NOLOCK)
	ON dag3.ItemGroup3Key = faig3.ItemGroup3Key
*/











































GO
