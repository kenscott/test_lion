SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO









CREATE VIEW [lion].[vwPriceBandAttributes]
AS

-- select top 10 * from lion.vwPriceBandAttributes

	WITH a AS  (

		SELECT

				PlaybookPricingGroupKey,
				ScenarioName,
				RulingMemberFlag,
				 CASE WHEN PricingRuleSequenceNumber < 2
					 THEN pb.PlaybookRegion
					 ELSE 'ALL'
				END AS PlaybookRegion,
				CASE WHEN PricingRuleSequenceNumber < 3
					 THEN CustomerSpecialism
					 ELSE 'ALL'
				END AS CustomerSpecialism,
				CASE WHEN PricingRuleSequenceNumber < 4
					 THEN Sales603010Bucket
					 ELSE 'ALL'
				END AS Sales603010Bucket,
				CASE WHEN PricingRuleSequenceNumber < 5
					 THEN StrategicItem
					 ELSE 'ALL'
				END AS StrategicItem,
				CASE WHEN PricingRuleSequenceNumber < 6
					 THEN PriceApproach
					 ELSE 'ALL'
				END AS PriceApproach,
				CASE WHEN PricingRuleSequenceNumber < 7
					 THEN ItemNumber + ' - ' + ItemDescription
					 ELSE 'ALL'
				END AS FullItemDescription,
				LLSPG + ' - ' + LLSPGDescription AS LLSPG,
				ContractClaimsIndicator,
				ShipmentType,
				pb.RegionExceptionIndicator,
				PlaybookProductBrand,
				ItemPyramidCode,

				GroupValues,
            
				Total12MonthQuantity,
				TotalActual12MonthPrice,
				TotalActual12MonthCost,
				TotalActual12MonthGP,
            
				-- "current" values:
				LastItemCost,
				LastItemPrice,
				LastItemGPP,
				LastItemDiscountPercent,
				LastUnitListPrice,
            
				PricingRuleSequenceNumber,
				PricingRuleDescription,
            
				FloorGPP,
				TargetGPP,
				StretchGPP,
            
				LastItemCost / ( 1 - FloorGPP   ) AS FloorPrice,
				LastItemCost / ( 1 - TargetGPP  ) AS TargetPrice,
				LastItemCost / ( 1 - StretchGPP ) AS StretchPrice,
  
  				fs.Max AS FloorSales,
				ts.Max AS TargetSales,
				ss.Max AS StretchSales,

				fti.Impact AS FloorToTargetImpact,
				tsi.Impact AS TargetToStretchImpact,
            
				r.Risk AS StretchRisk,
            
				LEFT(LastInvoiceDate, 4) + SUBSTRING(LastInvoiceDate, 6, 2) AS LastInvoiceDate,
				--lp.ListPrice,
				--LastTradingPrice,

				Branch,
				AccountManagerFullName,
				AccountNumber,
				AccountName,
				ItemNumber,
				ItemDescription

			FROM    
				lion.vwPGDatapointReview pb

				--  lion.vwPrice BandReview pb (NOLOCK) pb.ExcludeFor SegmentationIndicator = 'N'

				CROSS APPLY lion.fn_Max(TotalActual12MonthPrice, TotalActual12MonthCost / (1-FloorGPP)) fs
				CROSS APPLY lion.fn_Max(TotalActual12MonthPrice, TotalActual12MonthCost / (1-TargetGPP)) ts
				CROSS APPLY lion.fn_Max(TotalActual12MonthPrice, TotalActual12MonthCost / (1-StretchGPP)) ss

				CROSS APPLY lion.fn_GetPartialImpact(FloorGPP,  TargetGPP,  LastItemGPP, TotalActual12MonthPrice) AS fti
				CROSS APPLY lion.fn_GetPartialImpact(TargetGPP, StretchGPP, LastItemGPP, TotalActual12MonthPrice) AS tsi

				CROSS APPLY lion.fn_GetRisk(StretchGPP, LastItemGPP, TotalActual12MonthPrice) r
			
				--CROSS APPLY lion.fn_GetListPrice(LastTradingPrice, LastItemPrice) lp
			
			--WHERE 
			--	RulingMemberFlag = 'y'
	)
	SELECT
		PlaybookPricingGroupKey,
		ScenarioName,
		RulingMemberFlag,

		PlaybookRegion,
		CustomerSpecialism,
		Sales603010Bucket,
		StrategicItem,
		PriceApproach,
		FullItemDescription,
		LLSPG,
		ContractClaimsIndicator,
		ShipmentType,
		PlaybookProductBrand,
		RegionExceptionIndicator,
		ItemPyramidCode,

		PricingRuleSequenceNumber,
		PricingRuleDescription,
		GroupValues,
		Total12MonthQuantity,
		TotalActual12MonthPrice,
		TotalActual12MonthCost,
		TotalActual12MonthGP,
		--TotalSalesAtTrade,
		LastItemPrice,
		LastItemCost,
		LastItemDiscountPercent,
		LastUnitListPrice,
		--LastGP,
		LastItemGPP,
		LastInvoiceDate,
		FloorGPP,
		TargetGPP,
		StretchGPP,
		StretchSales,
		TargetSales,
		FloorSales,

		FloorSales   - TotalActual12MonthCost AS GPP$Floor,
		TargetSales  - TotalActual12MonthCost AS GPP$Target,
		StretchSales - TotalActual12MonthCost AS GPP$Stretch,
		
		FloorSales   - TotalActual12MonthPrice AS FloorImpact,
		TargetSales  - TotalActual12MonthPrice AS TargetImpact,
		StretchSales - TotalActual12MonthPrice AS StretchImpact,

		FloorToTargetImpact,
		TargetToStretchImpact,
		StretchRisk,
		Branch,
		AccountManagerFullName,
		AccountNumber,
		AccountName,
		ItemNumber,
		ItemDescription

	FROM a


GO
