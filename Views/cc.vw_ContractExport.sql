SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




/******************************** IMPORTANT - PLEASE READ!!! *******************************/
/******* Any changes to this view MAY require changes to cc.vw_ContractExportCount. ********/
/******* This is a lighter weight version used by the application to determine how  ********/
/******* many products/spgs/mpgs are on a given contract.                           ********/
/*******************************************************************************************/


/*
select top 10 * from cc.SupplierContract order by ContractKey desc

select * from cc.vw_ContractExport WHERE Sequence = 546626
select * from cc.vw_ContractExport WHERE Sequence = 128085
select * from cc.vw_ContractExport WHERE Sequence = 128076
select top 10 * from cc.vw_ContractExport order by sequence

select distinct claimtype from cc.SupplierContractItem

select top 10 * from cc.vw_ContractExport
where levelcode is null
order by sequence desc

select * from cc.suppliercontractitem where contractkey = 131593

select sc.ContractKey, 
--sci.ItemKey, 
scig.ItemGroupLevel, scig.ItemGroupXKey FROM cc.SupplierContract sc
    JOIN DimVendor dv on dv.VendorKey = sc.VendorKey
    --LEFT JOIN cc.SupplierContractItem sci on sci.ContractKey = sc.ContractKey
    LEFT JOIN cc.SupplierContractItemGroupX scig on scig.ContractKey = sc.ContractKey
    --JOIN DimItem di on di.ItemKey = sci.ItemKey
    WHERE sc.ContractKey = 456820
    
    
select top 1000 * from cc.suppliercontractaccount order by contractkey

select da.accountnumber +';'
	from cc.suppliercontractaccount sca
	join dimaccount da on da.accountkey = sca.accountkey
	where contractkey = 145974
	FOR XML PATH('')
  
*/

CREATE VIEW [cc].[vw_ContractExport] AS
WITH Output AS
(
	SELECT 
	   sc.ContractKey as Sequence
      ,sc.PyramidCode
      ,sc.Originator
      ,sc.ContractNumber
      ,sc.ContractOwner
      ,dv.VendorNumber
      ,sc.ContractReference
      ,sc.ContractType1 as ContractType
      ,sc.StartDate
      ,sc.ExpirationDate
      ,sc.PurchaseType as PurchaseType
      ,ISNULL(sc.AuthorizedIndicator,'N') AS AuthorizedIndicator
      ,sc.PricingContractIndicator
      ,sc.PricingContractID
      ,sc.JobSite
      ,sc.Developer
      ,sc.ContactName
      ,sc.ContactEmail
      
      ,CASE sc.IncludeAllAccounts 
	      WHEN 1 THEN 'ALL'					-- LPF-3991 CC-1086 Nothing in the Location/Account field for ALL (jed)
		  ELSE( SELECT da.accountnumber + ';'
		  		   FROM cc.SupplierContractAccount sca
				   JOIN dimaccount da on da.accountkey = sca.accountkey
				   WHERE sca.ContractKey = sc.ContractKey
				   FOR XML PATH('') )
	   END AS Accounts
      
      ,CASE sc.includeAllLocations
          WHEN 1 THEN 'ALL'					-- LPF-3991 CC-1086 Nothing in the Location/Account field for ALL (jed)
          ELSE ( SELECT scl.LocationName + ';'
					FROM cc.SupplierContractLocation scl
					WHERE scl.contractkey = sc.ContractKey
					FOR XML PATH('') )
	   END AS Locations
			
      --,sci.ItemKey as K
      --,'Product' AS LevelType
      ,CASE WHEN di.ItemUDVarchar20 IS NULL THEN NULL ELSE 'Product' END AS LevelType
      ,di.ItemUDVarChar20 as LevelCode
      ,sci.ClaimType
      ,CASE WHEN sci.ClaimType = 'P' THEN ( sci.ClaimBackPerc * 100 ) ELSE NULL END AS ClaimBackPerc
      ,CASE WHEN sci.ClaimType = 'A' THEN sci.ClaimBackAmount ELSE NULL END AS ClaimBackAmount
      ,CASE WHEN sci.ClaimType = 'C' THEN sci.ClaimCost ELSE NULL END AS ClaimCost
      ,sci.LimitType AS QuantityType
      ,sci.LimitValue AS QuantityLimit
    FROM cc.SupplierContract sc WITH (NOLOCK)
    LEFT JOIN DimVendor dv WITH (NOLOCK) on dv.VendorKey = sc.VendorKey
    LEFT JOIN cc.SupplierContractItem sci WITH (NOLOCK) on sci.ContractKey = sc.ContractKey
    LEFT JOIN cc.SupplierContractItemGroupX scigx WITH (NOLOCK) on scigx.ContractKey = sc.ContractKey
    LEFT JOIN DimItem di WITH (NOLOCK) on di.ItemKey = sci.ItemKey
    --WHERE di.ItemUDVarChar20 IS NOT NULL 
		  --OR scigx.ContractItemGroupXKey IS NULL -- LPF-3934 Null product rows are appearing in Contract Export (jed)

	UNION
	
	SELECT 
	   sc.ContractKey
      ,sc.PyramidCode
      ,sc.Originator
      ,sc.ContractNumber
      ,sc.ContractOwner
      ,dv.VendorNumber
      ,sc.ContractReference
      ,sc.ContractType1 as ContractType
      ,sc.StartDate
      ,sc.ExpirationDate
      ,sc.PurchaseType as PurchaseType
      ,ISNULL(sc.AuthorizedIndicator,'N') AS AuthorizedIndicator
      ,sc.PricingContractIndicator
      ,sc.PricingContractID
      ,sc.JobSite
      ,sc.Developer
      ,sc.ContactName
      ,sc.ContactEmail
      ,CASE sc.IncludeAllAccounts 
	      WHEN 1 THEN 'ALL'					-- LPF-3991 CC-1086 Nothing in the Location/Account field for ALL (jed)
		  ELSE( SELECT da.accountnumber + ';'
		  		   FROM cc.SupplierContractAccount sca
				   JOIN dimaccount da on da.accountkey = sca.accountkey
				   WHERE sca.ContractKey = sc.ContractKey
				   FOR XML PATH('') )
	   END AS Accounts
      
      ,CASE sc.includeAllLocations
          WHEN 1 THEN 'ALL'					-- LPF-3991 CC-1086 Nothing in the Location/Account field for ALL (jed)
          ELSE ( SELECT scl.LocationName + ';'
					FROM cc.SupplierContractLocation scl
					WHERE scl.contractkey = sc.ContractKey
					FOR XML PATH('') )
	   END AS Locations
			
      --,scigx.ItemGroupXKey
      ,'MPG' AS LevelType
      ,dig1.IG1Level1 as LevelCode
      ,scigx.ClaimType
      ,CASE WHEN scigx.ClaimType = 'P' THEN ( scigx.ClaimBackPerc * 100 ) ELSE NULL END AS ClaimBackPerc
      ,CASE WHEN scigx.ClaimType = 'A' THEN scigx.ClaimBackAmount ELSE NULL END AS ClaimBackAmount
      ,CASE WHEN scigx.ClaimType = 'C' THEN scigx.ClaimCost ELSE NULL END AS ClaimCost
      ,scigx.LimitType AS QuantityType
      ,scigx.LimitValue AS QuantityLimit
    FROM cc.SupplierContract sc WITH (NOLOCK)
    LEFT JOIN DimVendor dv WITH (NOLOCK) on dv.VendorKey = sc.VendorKey
    LEFT JOIN cc.SupplierContractItemGroupX scigx WITH (NOLOCK) on scigx.ContractKey = sc.ContractKey
    LEFT JOIN dbo.DimItemGroup1 dig1 WITH (NOLOCK) ON dig1.ItemGroup1Key = scigx.ItemGroupXKey
    WHERE scigx.ItemGroupLevel = 1

	UNION
	
	SELECT 
	   sc.ContractKey
      ,sc.PyramidCode
      ,sc.Originator
      ,sc.ContractNumber
      ,sc.ContractOwner
      ,dv.VendorNumber
      ,sc.ContractReference
      ,sc.ContractType1 as ContractType
      ,sc.StartDate
      ,sc.ExpirationDate
      ,sc.PurchaseType as PurchaseType
      ,ISNULL(sc.AuthorizedIndicator,'N') AS AuthorizedIndicator
      ,sc.PricingContractIndicator
      ,sc.PricingContractID
      ,sc.JobSite
      ,sc.Developer
      ,sc.ContactName
      ,sc.ContactEmail
      
      ,CASE sc.IncludeAllAccounts 
	      WHEN 1 THEN 'ALL'					-- LPF-3991 CC-1086 Nothing in the Location/Account field for ALL (jed)
		  ELSE( SELECT da.accountnumber + ';'
		  		   FROM cc.SupplierContractAccount sca
				   JOIN dimaccount da on da.accountkey = sca.accountkey
				   WHERE sca.ContractKey = sc.ContractKey
				   FOR XML PATH('') )
	   END AS Accounts
      
      ,CASE sc.includeAllLocations
          WHEN 1 THEN 'ALL'					-- LPF-3991 CC-1086 Nothing in the Location/Account field for ALL (jed)
          ELSE ( SELECT scl.LocationName + ';'
					FROM cc.SupplierContractLocation scl
					WHERE scl.contractkey = sc.ContractKey
					FOR XML PATH('') )
	   END AS Locations
			
      --,scigx.ItemGroupXKey
      ,'SPG' AS LevelType
      ,dig3.IG3Level1 as LevelCode
      ,scigx.ClaimType
      ,CASE WHEN scigx.ClaimType = 'P' THEN ( scigx.ClaimBackPerc * 100 ) ELSE NULL END AS ClaimBackPerc
      ,CASE WHEN scigx.ClaimType = 'A' THEN scigx.ClaimBackAmount ELSE NULL END AS ClaimBackAmount
      ,CASE WHEN scigx.ClaimType = 'C' THEN scigx.ClaimCost ELSE NULL END AS ClaimCost
      ,scigx.LimitType AS QuantityType
      ,scigx.LimitValue AS QuantityLimit
    FROM cc.SupplierContract sc WITH (NOLOCK)
    LEFT JOIN DimVendor dv WITH (NOLOCK) on dv.VendorKey = sc.VendorKey
    LEFT JOIN cc.SupplierContractItemGroupX scigx WITH (NOLOCK) on scigx.ContractKey = sc.ContractKey
    LEFT JOIN dbo.DimItemGroup3 dig3 WITH (NOLOCK) ON dig3.ItemGroup3Key = scigx.ItemGroupXKey
    WHERE scigx.ItemGroupLevel = 3 
)     
   
SELECT * FROM Output

     




GO
