SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE
VIEW [lion].[vwPriceRuleLevel]

/*
SELECT TOP 0 * FROM lion.vwPriceRuleLevel ORDER BY 1,2
SELECT TOP 10 * FROM lion.vwPriceRuleLevel ORDER BY 1,2
*/


AS


SELECT  PricingRuleSequenceNumber, PricingRuleDescription
FROM    ATKPricingRule pr
WHERE   EXISTS ( SELECT 1
                 FROM   dbo.PlaybookPricingGroup ppg
                 WHERE  ppg.PricingRuleKey = pr.PricingRuleKey
                        AND EXISTS ( SELECT 1
                                     FROM   dbo.FactLastPriceAndCost flpac
                                     WHERE  flpac.RPBPlaybookPricingGroupKey = ppg.PlaybookPricingGroupKey ) )




GO
