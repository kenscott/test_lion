SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




CREATE VIEW [cm].[vw_ClaimJournalLog]

AS

-- select top 10 * from cm.vw_ClaimJournalLog order by ClaimJournalLogKey desc

SELECT  cjl.ClaimJournalLogKey ,
        cj.ClaimJournalKey ,
        dv.VendorDescription ,
        cj.CurrentContractID ,
        cjl.ClaimAction ,
        cjl.Notes ,
        cjl.CreationDate ,
        p.FullName AS UserName,
		f.ChargeNoteId 
FROM    cm.ClaimJournalLog AS cjl
        LEFT OUTER JOIN dbo.WebUser AS wu ON wu.WebUserKey = cjl.WebUserKey
        LEFT OUTER JOIN cm.ClaimJournal AS cj ON cj.ClaimJournalKey = cjl.ClaimJournalKey
		LEFT OUTER JOIN cm.FactInvoiceLine as f ON cj.ClientInvoiceLineUniqueID = f.ClientInvoiceLineUniqueID
        LEFT OUTER JOIN dbo.DimVendor AS dv ON dv.VendorKey = cj.VendorKey
        LEFT OUTER JOIN dbo.ODSPerson AS p ON wu.ODSPersonKey = p.ODSPersonKey

GO
