SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [lion].[vwTermsExistence]
AS
SELECT DISTINCT AccountKey, ItemGroup3Key
FROM            lion.CustomerTerms

GO
