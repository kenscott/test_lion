SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





CREATE VIEW [sup].[vw_ClaimLines_cls]

AS

--View for Claim Line Screen in supplier portal
--LSP-148, Create view in db for Claim Lines 

	SELECT cj.InvoiceClaimKey
		, CAST(cj.InvoiceClaimKey AS VARCHAR(50)) AS str_InvoiceClaimKey
		, cj.ClaimMonth
		, ic.CreationDate
		, cj.SupplierRef
		, cj.CurrentContractID
		, cj.ClaimType
		, f.BranchCode
		, f.AccountName
		, i.VendorStockNumber AS ProductCode
		, f.Qty
		, f.SupplierNettCost
		, cj.ClaimLineValue
		, f.ProductDescription
		, f.BranchDescription
		, cj.ClientInvoiceLineUniqueID
		, f.VendorNumber
		, cj.ClaimJournalKey
	FROM cm.ClaimJournal cj INNER JOIN
	cm.FactInvoiceLine f ON f.ClientInvoiceLineUniqueID = cj.ClientInvoiceLineUniqueID
	INNER JOIN cm.InvoiceClaim ic ON cj.InvoiceClaimKey = ic.InvoiceClaimKey
	INNER JOIN lion.vwItem i ON i.ItemKey = f.ItemKey
	WHERE cj.ClaimJournalStatus IN ('P','C') AND 
	cj.ClaimType IN ('M','C')
	
GO
