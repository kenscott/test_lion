SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE VIEW [lion].[vwLastTrans] AS

SELECT * FROM (
SELECT
	fil.AccountKey, 
	fil.ItemKey, 
	fil.InvoiceLineGroup1Key,	
	fil.InvoiceLineUniqueIdentifier as LastInvoiceLineUniqueIdentifier,
    ROW_NUMBER() OVER (Partition by fil.AccountKey, fil.ItemKey, fil.InvoiceLineGroup1Key
	                   order by 	fil.AccountKey, 
									fil.ItemKey, 
									fil.InvoiceLineGroup1Key, 
									fil.InvoiceDateDayKey DESC, 
									fil.TotalQuantity DESC, 
									fil.TotalActualPrice DESC,
									fil.ClientInvoiceID DESC,
									fil.InvoiceLineUniqueIdentifier DESC) as RowNumber,
	fil.TotalActualPrice / NULLIF(fil.TotalQuantity, 0) as LastItemPrice,
	fil.TotalActualCost / NULLIF(fil.TotalQuantity, 0) as LastItemCost,
	ISNULL(fil.UDDecimal2, 0) / NULLIF(fil.TotalQuantity, 0)  as LastCCProfit,
	fil.UDVarchar1 as LastPriceDerivation
FROM dbo.FactInvoiceLine fil
WHERE 
 	TotalQuantity > 0.0 AND 
 	TotalActualPrice > 0.0 AND 
 	TotalActualCost > 0.0 AND 
	(ISNULL(TotalActualPrice, 0.0) - ISNULL(TotalActualCost, 0.0))/NULLIF(TotalActualPrice, 0) between -1.2 and 0.90 AND
    (fil.InvoiceLineGroup1Key < 5 or 
		(fil.InvoiceLineGroup1Key >= 5 and (isnull(fil.UDDecimal2, 0) > 0))
	)
) a
where RowNumber = 1




GO
