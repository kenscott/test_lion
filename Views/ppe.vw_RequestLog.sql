SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE VIEW [ppe].[vw_RequestLog] AS

/*
select top 10 * from ppe.vw_RequestLog

select * from dbo.dimitemgroup3
*/

SELECT 
	rl.RequestLogKey,
	rl.RequestKey,
	rl.WebUserKey,
	wu1.UserName AS WebUserUserName,
	od1.FullName AS WebUserFullName,
	rl.OldStatusKey,
	rs1.StatusName AS OldStatusDescription,
	rl.NewStatusKey,
	rs2.StatusName AS NewStatusDescription,
	rl.RequestType,
	rl.RequestAction,
	rl.EmailSent AS EmailSent,
	CASE WHEN rl.EmailSent = 1 THEN 'sent'
		 WHEN rl.EmailSent = 0 THEN 'pending'
		 ELSE 'n/a'
		 END AS EmailSentDescription,
	rl.OldOwnerWebUserKey,
	wu2.UserName AS OldOwnerWebUserUsername,
	od2.FullName AS OldOwnerWebUserFullName,
	rl.NewOwnerWebUserKey,
	wu3.UserName AS NewOwnerWebUserUsername,
	od3.FullName AS NewOwnerWebUserFullName,
	rl.Notes,
	rl.LogTime,
	a.AccountNumber,
	a.AccountName,
	CASE WHEN rm.ItemKey <> 1 THEN SUBSTRING(di.ItemNumber, 2, 255)
                 ELSE NULL
            END AS ProductCode ,	
	di.ItemDescription AS ProductDescription,	
	ig3.IG3Level1 AS SPGCode,
	ig3.IG3Level1UDVarchar1 AS SPGDescription
FROM ppe.RequestLog rl (NOLOCK)
LEFT JOIN dbo.WebUser wu1 ON wu1.WebUserKey = rl.WebUserKey
LEFT JOIN dbo.ODSPerson od1 ON od1.ODSPersonKey = wu1.ODSPersonKey
LEFT JOIN ppe.RequestStatus rs1 ON rs1.StatusKey = rl.OldStatusKey
LEFT JOIN ppe.RequestStatus rs2 ON rs2.StatusKey = rl.NewStatusKey
LEFT JOIN dbo.WebUser wu2 ON wu2.WebUserKey = rl.OldOwnerWebUserKey
LEFT JOIN dbo.ODSPerson od2 ON od2.ODSPersonKey = wu2.ODSPersonKey
LEFT JOIN dbo.WebUser wu3 ON wu3.WebUserKey = rl.NewOwnerWebUserKey
LEFT JOIN dbo.ODSPerson od3 ON od3.ODSPersonKey = wu3.ODSPersonKey
LEFT JOIN ppe.RequestMain rm ON rm.RequestKey = rl.RequestKey
LEFT JOIN dbo.DimAccount a ON a.AccountKey = rm.AccountKey
LEFT JOIN dbo.DimItem di ON di.ItemKey = rm.ItemKey
LEFT JOIN dbo.DimItemGroup3 ig3 ON ig3.ItemGroup3Key = rm.ItemGroup3Key




GO
