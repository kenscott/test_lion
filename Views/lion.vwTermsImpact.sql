SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [lion].[vwTermsImpact] AS
select  a.Region, a.RegionDescription, a.Area, a.AreaDescription, a.Network, a.NetworkDescription, a.Branch, a.BranchName, a.AccountNumber, a.AccountName, 
        i.LLSPGCode as LLSPG, i.LLSPGDescription,
	   sum(f.Total12MonthSales) as [Actual 12Month Sales],
	   sum(f.Total12MonthQuantity * i.CurrBranchTradePrice * (1 - tt.OriginalDiscount)) as [Sales @ Original],
	   sum(f.Total12MonthQuantity * i.CurrBranchTradePrice * (1 - tt.NewDiscount)) as [Sales @ New],
	   --( f.Total12MonthQuantity * i.CurrBranchTradePrice * (1 - )) - (f.Total12MonthQuantity * i.CurrBranchTradePrice * (1 - tt.OriginalDiscount)) as [Accepted Impact]
	   sum((f.Total12MonthQuantity * i.CurrBranchTradePrice * (1 - tt.NewDiscount)) - (f.Total12MonthQuantity * i.CurrBranchTradePrice * (1 - tt.OriginalDiscount))) as [Accepted Impact]
  from lion.vwFactLastPriceAndCost f inner join 
       lion.vwAccount a on f.AccountKey = a.AccountKey inner join
       lion.vwItem i on f.ItemKey = i.ItemKey inner join 
	   lion.vwItemGroup3 ig3 on i.LLSPGCode = ig3.LLSPGCode and i.ItemPyramidCode = ig3.PYRCode inner join 
       lion.TermsTracker tt on (tt.AccountKey = f.AccountKey and
		                        ISNULL(tt.ItemKey, f.ItemKey) = f.ItemKey and
								case when exists (select * 
								              from lion.TermsTracker itt
											 where itt.AccountKey = f.AccountKey and
											       itt.ItemKey = f.ItemKey) and
											tt.ItemKey is null
									 then 0
									 else tt.ItemGroup3Key
								 end = ig3.ItemGroup3Key
					           ) 
group by  a.Region, a.RegionDescription, a.Area, a.AreaDescription, a.Network, a.NetworkDescription, a.Branch, a.BranchName, a.AccountNumber, a.AccountName, 
        i.LLSPGCode, i.LLSPGDescription
GO
