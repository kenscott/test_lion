SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO










CREATE 
VIEW [cg].[vwDatapoint2]

AS

/*

SELECT TOP 10 * FROM cg.vwDatapoint

select top 10 * from cg.PlaybookDatapoint

select count(*) from cg.vwDatapoint

*/

WITH 
ContractSales AS (--ContractID = dc.ContractOriginator + dc.ContractNumber
	SELECT
		UDVarchar3 AS ContractID, 
		SUM(UDDecimal2) AS CCProfit,
		SUM(totalactualprice) AS TotalSales, 
		SUM(totalactualCost) AS TotalCost, 
		SUM(totalquantity) AS TotalQuantity, 
		COUNT(*) AS TotalInvoiceLineCount
	FROM dbo.FactInvoiceLine 
	WHERE Last12MonthsIndicator='Y'
	GROUP BY UDVarchar3
), ContractItemSales AS (
	SELECT 
		UDVarchar3 AS ContractID,
		da.AccountNumber,
		di.ItemNumber,
		SUM(UDDecimal2) AS CCProfit, 
		SUM(totalactualprice) AS TotalSales, 
		SUM(totalactualCost) AS TotalCost, 
		SUM(totalquantity) AS TotalQuantity, 
		COUNT(*) AS TotalInvoiceLineCount
	FROM dbo.FactInvoiceLine fil
	inner join DimItem di
		ON di.ItemKey = fil.ItemKey
	INNER JOIN dbo.DimAccount da
		ON da.AccountKey = fil.AccountKey
	WHERE Last12MonthsIndicator='Y'
	GROUP BY UDVarchar3, AccountNumber, ItemNumber
), 
PlaybookDataPointUsage AS (
	SELECT
		PlaybookDataPointKey,
		SectionNumber
	FROM cg.PlaybookDataPointPricingGroup pdppg (NOLOCK)
	INNER JOIN dbo.PlaybookPricingGroup ppg (NOLOCK)
		ON ppg.PlaybookPricingGroupKey = pdppg.PlaybookPricingGroupKey
	WHERE 
		ppg.PlaybookDataPointGroupKey = (
				SELECT 
					MAX(PlaybookDataPointGroupKey)
				FROM dbo.ATKScenario a
				INNER JOIN dbo.ATKProjectSpecificationScenario c ON c.ScenarioKey = a.ScenarioKey
				INNER JOIN dbo.PlaybookProject pp
					ON pp.ProjectSpecificationKey = c.ProjectSpecificationKey
				INNER JOIN dbo.PlaybookDataPointGroup pdpg
					ON pdpg.ProjectSpecificationScenarioKey = c.ProjectSpecificationScenarioKey
					AND pdpg.ProjectKey = pp.ProjectKey
				WHERE 
					a.ApproachTypeKey = 12	-- Cost Guidance
					--AND pp.ProductionProjectIndicator = 'Y'
				)
)
SELECT DISTINCT

	pdp.PlaybookDataPointKey,
	pdp.PlaybookDataPointGroupKey,
	pdp.ContractKey,
	pdp.AccountKey,
	pdp.InstructionLevelType,
	pdp.LLSPGCode,
	pdp.MPGCode,
	pdp.ItemNumber,
	pdp.InstructionLevelCode,
	pdp.ClaimBackPercent,
	pdp.Sales603010Bucket,
	
	dc.Region AS ContractRegion,
	dc.Originator AS ContractOriginator,
	dc.ContractOwner,
	dc.ContractNumber,
	dc.ContractReference,
	dc.StartDate AS ContractStartDate,
	dc.ExpirationDate AS ContractExpirationDate,
	dc.JobSite,
	dc.JobFlag,
	dc.PyramidCode,
	--dc.Narrative1,	-- replaced by cc.SupplierContractNote table
	--dc.Narrative2,
	--dc.Narrative3,
	--dc.Narrative4,
	dc.ContractType1,
	dc.ContractLevel,
	dc.FullContractNumber,
	dc.CCProfit,

	cs.CCProfit AS CCProfitCalc,
	cs.TotalSales, 
	cs.TotalCost, 
	cs.TotalQuantity, 
	cs.TotalInvoiceLineCount,

	cis.CCProfit AS ItemLevelCCProfit,
	cis.TotalSales AS ItemLevelTotalSales,
	cis.TotalCost AS ItemLevelTotalCost,
	cis.TotalQuantity AS ItemLevelTotalQuantity,
	cis.TotalInvoiceLineCount AS ItemLevelTotalInvoiceLineCount,

	dv.VendorKey AS VendorKey,
	dv.VendorDescription,
	dv.VendorNumber,

	da.AccountNumber,
	da.AccountName,
	da.AccountNumberParent,
	da.AccountAddress1,
	da.AccountAddress2,
	da.AccountAddress3,
	da.AccountAddress4,
	da.AccountZipCode AS AccountPostCode,
	da.AccountUDVarChar1 AS CreditLimit,
	da.AccountUDVarChar2 AS PaymentTermName,
	da.AccountUDVarChar3 AS IntercompanyAccount,
	da.AccountUDVarChar4 AS OverLimitStatus,
	da.AccountUDVarChar5 AS OverdueStatus,
	da.AccountUDVarChar6 AS TermsParent,
	da.AccountUDVarChar7 AS PricingCategory,
	da.AccountUDVarChar8 AS AccountOwnerType,
	da.AccountUDVarChar9 AS AccountOwnerBucket,
	da.AccountUDVarChar10 AS CustomerSegment,
	da.AccountUDVarChar11 AS CustomerRebateIndicator,
	da.AccountUDVarChar12 AS FixedPriceIndicator,
	da.AccountUDVarChar13 AS ContractName,
	da.AccountUDDateTime1 AS DateOpened,
	da.AccountUDDateTime2 AS DateClosed,
	da.Inactive AS AccountInactive,

	da.AccountManagerKey AS AccountManagerKey,
	dam.AccountManagerCode,
	dp.FullName AS AccountManagerFullName,
	dp.Email AS AccountManagerEmail,



	AG1Level1 AS Branch,
	AG1Level2 AS Network,
	AG1Level3 AS Area,
	AG1Level4 AS Region,	

	AG1Level1UDVarchar1 AS BranchName,
	AG1Level2UDVarchar1 AS NetworkDescription,
	AG1Level3UDVarchar1 AS AreaDescription,
	AG1Level4UDVarchar1 AS RegionDescription,
	

	AG1Level1UDVarchar2 AS PlaybookRegion,
	AG1Level1UDVarchar3 AS PlaybookRegionDescription,
	AG1Level1UDVarchar4 AS PlaybookArea,
	AG1Level1UDVarchar5 AS PlaybookAreaDescription,
	AG1Level1UDVarchar6 AS BranchPrimaryBrand,
	AG1Level1UDVarchar7 AS BranchTradingStatus,
	AG1Level1UDVarchar8 AS BranchType,
	AG1Level1UDVarchar9 AS BranchAddressLine1,
	AG1Level1UDVarchar10 AS BranchAddressLine2,
	AG1Level1UDVarchar11 AS BranchAddressLine3,
	AG1Level1UDVarchar12 AS BranchAddressLine4,
	AG1Level1UDVarchar13 AS BranchAddressLine5,
	AG1Level1UDVarchar14 AS BranchPostcode,
	AG1Level1UDVarchar15 AS BranchEmail,
	AG1Level1UDVarchar16 AS BranchTelephone,
	AG1Level1UDVarchar17 AS CustomerBranchPostCodeArea,
	AG1Level1UDVarchar18 AS CustomerBranchPostCodeDistrict,
	AG1Level1UDVarchar19 AS BranchPyramidCode,
	AG1Level1UDVarchar21 AS BrandGroup,
	AG1Level1UDVarchar22 AS PGActiveIndicator,
	AG1Level1UDVarchar23 AS Manager,
	AG1Level1UDVarchar24 AS BranchManagerEmail,
	AG1Level1UDVarchar25 AS RegionExceptionIndicator,
	AG1Level1UDVarchar26 AS BusinessUnitRegion,

	AG2Level1 AS CustomerType,
	AG2Level1UDVarchar1 AS CustomerTypeDescription,

	AG3Level1 AS TrandingStatusCategory,
	AG3Level1UDVarchar1 AS TrandingStatusCategoryDescription,


	di.ItemDescription,
	di.VendorStockNumber,
	di.ItemUDVarChar2 AS ProdBrandAtLLSPG,
	di.CurrentCost,
	di.CurrentInvoiceCost,
	di.InActive AS ItemInactive,

	CASE 
		WHEN Section1.PlaybookDataPointKey IS NOT NULL THEN 'Y'
		ELSE 'N'
	END AS Section1Indicator,
	CASE 
		WHEN Section2.PlaybookDataPointKey IS NOT NULL THEN 'Y'
		ELSE 'N'
	END AS Section2Indicator

	
FROM cg.PlaybookDataPoint pdp (NOLOCK)

LEFT JOIN PlaybookDataPointUsage Section1 (NOLOCK)
	ON pdp.PlaybookDataPointKey = Section1.PlaybookDataPointKey
	AND Section1.SectionNumber = 1

LEFT JOIN PlaybookDataPointUsage Section2 (NOLOCK)
	ON pdp.PlaybookDataPointKey = Section2.PlaybookDataPointKey
	AND Section2.SectionNumber = 2

LEFT JOIN dbo.DimAccount da (NOLOCK)
	ON da.AccountKey = pdp.AccountKey
LEFT JOIN dbo.DimAccountGroup1 dag1 (NOLOCK)
	ON dag1.AccountGroup1Key = da.AccountGroup1Key
LEFT JOIN dbo.DimAccountGroup2 dag2 (NOLOCK)
	ON dag2.AccountGroup2Key = da.AccountGroup2Key
LEFT JOIN dbo.DimAccountGroup3 dag3 (NOLOCK)
	ON dag3.AccountGroup3Key = da.AccountGroup3Key
LEFT JOIN dbo.DimAccountManager dam (NOLOCK)
	ON dam.AccountManagerKey = da.AccountManagerKey
LEFT JOIN dbo.DimPerson dp (NOLOCK)
	ON dp.DimPersonKey = dam.DimPersonKey
LEFT JOIN dbo.DimItem di (NOLOCK)
	ON di.ItemNumber = pdp.ItemNumber
	AND pdp.InstructionLevelType = 'Item'
LEFT JOIN dbo.DimItemGroup1 dig1 (NOLOCK)
	ON dig1.ItemGroup1Key = di.ItemGroup1Key
LEFT JOIN dbo.DimItemGroup2 dig2 (NOLOCK)
	ON dig2.ItemGroup2Key = di.ItemGroup2Key
LEFT JOIN dbo.DimItemGroup3 dig3 (NOLOCK)
	ON dig3.ItemGroup3Key = di.ItemGroup3Key
INNER JOIN cc.SupplierContract dc (NOLOCK)
	ON dc.ContractKey = pdp.ContractKey
INNER JOIN dbo.DimVendor dv (NOLOCK)
	ON dv.VendorKey = dc.VendorKey

LEFT JOIN ContractSales cs
	ON cs.ContractID = dc.Originator + dc.ContractNumber
LEFT JOIN ContractItemSales cis
	ON cis.ContractID = dc.Originator + dc.ContractNumber
		AND cis.AccountNumber = da.AccountNumber
		AND cis.ItemNumber = pdp.InstructionLevelCode
		AND pdp.InstructionLevelType = 'Item'
WHERE 
	--RulingMemberFlag = 'Y'
	--AND pdppg.LastItemPrice <> 0.0
	--AND (
	pdp.PlaybookDataPointGroupKey = (
			SELECT 
				MAX(PlaybookDataPointGroupKey)
			FROM dbo.ATKScenario a
			INNER JOIN dbo.ATKProjectSpecificationScenario c ON c.ScenarioKey = a.ScenarioKey
			INNER JOIN dbo.PlaybookProject pp
				ON pp.ProjectSpecificationKey = c.ProjectSpecificationKey
			INNER JOIN dbo.PlaybookDataPointGroup pdpg
				ON pdpg.ProjectSpecificationScenarioKey = c.ProjectSpecificationScenarioKey
				AND pdpg.ProjectKey = pp.ProjectKey
			WHERE 
				a.ApproachTypeKey = 12	-- Cost Guidance
				--AND pp.ProductionProjectIndicator = 'Y'
			)
	











GO
