SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO














CREATE VIEW [cm].[vw_PendingSupplierClaims]
AS
/* LPF-5824, Modify cm.vw_PendingSupplierClaims to use the new deferred date field on the SupplierClaim table.
   replaced lines 36,37,38 with line 39
   kbs, 08.08.2017	
*/
    /*
	
	select * from cm.vw_PendingSupplierClaims where InvoiceCount > 0
	
*/

	SELECT  v.VendorKey ,
            v.VendorNumber ,
            v.VendorDescription ,
            --CASE WHEN v.DeferredClaimMonth = cj.ClaimMonth THEN v.DeferredDate
            --     ELSE NULL
            --END AS DeferredDate ,
			c.DeferredDate,
            cj.ClaimMonth ,
            v.VendorDescription + ' (' + v.VendorNumber + ')' AS VendorLabel ,
            ( SELECT    COUNT(DISTINCT ( cj.ClaimJournalKey ))
            ) AS ActiveContracts ,
            ( SELECT    COUNT(*)
              FROM      cm.ClaimJournal cj2
              WHERE     cj2.VendorKey = cj.VendorKey
                        AND cj2.ClaimMonth = cj.ClaimMonth
                        AND cj2.ClaimJournalStatus = 'P'
            ) AS PendingClaimLines ,
            ISNULL(( SELECT SUM(cj2.ClaimLineValue)
                     FROM   cm.ClaimJournal cj2
                     WHERE  cj2.VendorKey = cj.VendorKey
                            AND cj2.ClaimMonth = cj.ClaimMonth
                            AND cj2.ClaimJournalStatus = 'P'
                   ), 0) AS PendingClaimValue ,
            ( SELECT    COUNT(*)
              FROM      cm.ClaimJournal cj2
              WHERE     cj2.VendorKey = cj.VendorKey
                        AND cj2.ClaimMonth = cj.ClaimMonth
                        AND cj2.ClaimJournalStatus = 'R'
            ) AS ReviewClaimLines ,
            ISNULL(( SELECT SUM(cj2.ClaimLineValue)
                     FROM   cm.ClaimJournal cj2
                     WHERE  cj2.VendorKey = cj.VendorKey
                            AND cj2.ClaimMonth = cj.ClaimMonth
                            AND cj2.ClaimJournalStatus = 'R'
                   ), 0) AS ReviewClaimValue ,
            ( SELECT    COUNT(*)
              FROM      cm.ClaimJournal cj2
              WHERE     cj2.VendorKey = cj.VendorKey
                        AND cj2.ClaimMonth = cj.ClaimMonth
                        AND cj2.ClaimJournalStatus = 'X'
            ) AS RejectedClaimLines ,
            ISNULL(( SELECT SUM(cj2.ClaimLineValue)
                     FROM   cm.ClaimJournal cj2
                     WHERE  cj2.VendorKey = cj.VendorKey
                            AND cj2.ClaimMonth = cj.ClaimMonth
                            AND cj2.ClaimJournalStatus = 'X'
                   ), 0) AS RejectedClaimValue ,
            ( SELECT    COUNT(DISTINCT ( c2.VendorKey ))
              FROM      cm.SupplierClaim c2
                        INNER JOIN cm.ClaimJournal cj2 ON c2.ClaimMonth = cj.ClaimMonth
                                                          AND c2.VendorKey = cj.VendorKey
                                                          AND c2.ClaimMonth = cj2.ClaimMonth
                                                          AND c2.VendorKey = cj2.VendorKey
                                                          AND cj2.ClaimJournalStatus = 'P'
                                                          AND cj2.ClaimType = 'M'
            ) AS ManualClaimLines ,
            ISNULL(( SELECT SUM(cj2.ClaimLineValue)
                     FROM   cm.ClaimJournal cj2
                     WHERE  cj2.VendorKey = cj.VendorKey
                            AND cj2.ClaimMonth = cj.ClaimMonth
                            AND cj2.ClaimType = 'M'
                            AND cj2.ClaimJournalStatus = 'P'
                   ), 0) AS ManualClaimValue ,
            ( SELECT    COUNT(DISTINCT ( c2.VendorKey ))
              FROM      cm.SupplierClaim c2
                        INNER JOIN cm.ClaimJournal cj2 ON c2.ClaimMonth = cj.ClaimMonth
                                                          AND c2.VendorKey = cj.VendorKey
                                                          AND c2.ClaimMonth = cj2.ClaimMonth
                                                          AND c2.VendorKey = cj2.VendorKey
                                                          AND cj2.ClaimJournalStatus = 'P'
                                                          AND cj2.ClaimType IN ('A', 'W')
            ) AS AdjustmentClaimLines ,
            ISNULL(( SELECT SUM(cj2.ClaimLineValue)
                     FROM   cm.ClaimJournal cj2
                     WHERE  cj2.VendorKey = cj.VendorKey
                            AND cj2.ClaimMonth = cj.ClaimMonth
                            AND cj2.ClaimType IN ('A', 'W')
                            AND cj2.ClaimJournalStatus = 'P'
                   ), 0) AS AdjustmentClaimValue ,
            ( SELECT    COUNT(DISTINCT ( cj2.CurrentContractID ))
              FROM      cm.SupplierClaim c2
                        INNER JOIN cm.ClaimJournal cj2 ON c2.ClaimMonth = cj.ClaimMonth
                                                          AND c2.VendorKey = cj.VendorKey
                                                          AND c2.ClaimMonth = cj2.ClaimMonth
                                                          AND c2.VendorKey = cj2.VendorKey
                                                          AND cj2.ClaimJournalStatus = 'P'
                                                          AND cj2.ClaimType = 'C'
            ) AS InvoiceCount ,  --Pending and NOT Manual
            ISNULL(( SELECT SUM(cj2.ClaimLineValue)
                     FROM   cm.ClaimJournal cj2
                     WHERE  cj2.VendorKey = cj.VendorKey
                            AND cj2.ClaimMonth = cj.ClaimMonth
                            AND cj2.ClaimType = 'C'
                            AND cj2.ClaimJournalStatus = 'P'
                   ), 0) AS InvoiceValue ,
            c.SupplierClaimKey ,
            CASE WHEN c.ClaimStatus = 'R' THEN 'Y'
                 ELSE 'N'
            END AS ReadyToClaim
    FROM    cm.ClaimJournal cj WITH ( NOLOCK )
            INNER JOIN dbo.DimVendor v ON v.VendorKey = cj.VendorKey
            INNER JOIN cm.SupplierClaim c ON c.ClaimMonth = cj.ClaimMonth
                                             AND c.VendorKey = cj.VendorKey
											 AND c.ClaimStatus IN ( 'P', 'R' )
    WHERE   cj.ClaimJournalStatus IN ( 'P', 'R' )  --Pending or Ready
    GROUP BY v.VendorKey ,
            cj.VendorKey ,
            v.VendorNumber ,
            v.VendorDescription ,
            v.DeferredClaimMonth ,
            c.DeferredDate ,
            cj.ClaimMonth ,
            c.SupplierClaimKey ,
            c.ClaimStatus










GO
