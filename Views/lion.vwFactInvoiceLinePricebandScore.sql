SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE VIEW [lion].[vwFactInvoiceLinePricebandScore] 

/*

SELECT TOP 10 * FROM lion.vwFactInvoiceLinePricebandScore

*/


AS

SELECT
	AccountKey,
	AlternateAccountKey,
	ItemKey,
	VendorKey,
	AccountManagerKey,
	InvoiceDateDayKey,
	InvoiceDateMonthKey,
	InvoiceLineGroup1Key,
	InvoiceLineGroup2Key,
	InvoiceLineGroup3Key,
	InvoiceLineJunkKey,
	AccountGroup1Key,
	AccountGroup2Key,
	AccountGroup3Key,
	ItemGroup1Key,
	ItemGroup2Key,
	ItemGroup3Key,
	InvoiceLineUniqueIdentifier,
	LinkedInvoiceLineUniqueIdentifier,
	ClientInvoiceID,
	ClientInvoiceLineID,
	ClientInvoiceLineUniqueID,

	TotalQuantity,
	TotalActualPrice,
	TotalActualCost,
	UnitListPrice,
	TotalNetQuantity,
	TotalNetPrice,
	TotalNetCost,

	RedPrice,
	AmberPrice,
	GreenPrice,
	BluePrice,
	TerminalEmulator,
	PriceGuidanceIndicator,

	UDVarchar1 AS PriceDerivation,
	UDDecimal1 AS CCProfit,

	--t.BranchMargin AS UDDecimal1,
	--t.PaddingReleaseVal AS UDDecimal2,

	FloorGPP,
	TargetGPP,
	StretchGPP,
	filpbs.Score,
	scoreCurrentPG.Score AS CurrentPGScore,
	FloorImpact,
	TargetImpact,
	StretchImpact,
	RPBPlaybookPricingGroupKey,
	c.GPP AS CurrentGPP,
	ft.Impact AS FloorTargetImpact,
	ts.Impact AS TargetStretchImpact,
	s.Risk AS StretchRisk,
	ClaimNo

FROM
	dbo.FactInvoiceLinePricebandScore filpbs (NOLOCK)
--INNER JOIN dbo.DimInvoiceLineJunk diljunk (NOLOCK)
--	ON diljunk.InvoiceLineUniqueIdentifier = fil.InvoiceLineUniqueIdentifier
CROSS APPLY lion.fn_GetGPP (TotalActualPrice, TotalActualCost) c
CROSS APPLY lion.fn_GetPartialImpact (FloorGPP, TargetGPP, c.GPP, TotalQuantity) ft
CROSS APPLY lion.fn_GetPartialImpact (TargetGPP, StretchGPP, c.GPP, TotalQuantity) ts
CROSS APPLY lion.fn_GetRisk(StretchGPP, c.GPP, TotalQuantity) s
CROSS APPLY lion.fn_GetSCore( 
	CAST( ( ISNULL( filpbs.TotalNetPrice , filpbs.TotalActualPrice ) - ISNULL( filpbs.TotalNetCost , filpbs.TotalActualCost )  )  /  NULLIF(ISNULL( filpbs.TotalNetPrice , filpbs.TotalActualPrice ), 0) AS DEC(19,8) ) , /* the gpp on the line */
	FloorGPP,
	( TargetGPP - ( .25 * ( TargetGPP - FloorGPP ) ) ), 
	( TargetGPP + ( .25 * ( StretchGPP - TargetGPP ) ) ),
	StretchGPP) scoreCurrentPG












GO
