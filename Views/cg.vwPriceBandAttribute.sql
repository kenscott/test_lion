SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO









CREATE
VIEW [cg].[vwPriceBandAttribute]
AS

/*

select top 100 * from cg.vwPriceBandAttribute

select count(*) from cg.vwPriceBandAttribute
3009855

select top 100 * from cg.vwPriceBandAttribute
where GroupValues = '1,Item,3134,HZ77'

*/

WITH a AS (
	SELECT 
		PlaybookPricingGroupKey,
		COUNT(DISTINCT ContractKey) AS BandContractCount
	FROM 
		cg.vwDatapoint
	GROUP BY
		PlaybookPricingGroupKey
)
SELECT
	dp.PlaybookPricingGroupKey AS [Pricing Rule Key],
	ScenarioName AS Playbook,

	CASE 
		WHEN PricingRuleSequenceNumber = 1
			OR PricingRuleSequenceNumber = 4
			THEN Sales603010Bucket
		ELSE 
			'ALL'
	END AS  [Sales Bucket],

	CASE 
		WHEN PricingRuleSequenceNumber BETWEEN 1 AND 2
			OR PricingRuleSequenceNumber BETWEEN 4 AND 5
			THEN CAST(JobFlag AS VARCHAR(3))
		ELSE 
			'ALL'
	END AS [Job Flag],

	CASE 
		WHEN PricingRuleSequenceNumber BETWEEN 1 AND 3
			THEN ItemNumber
		ELSE 
			'ALL'
	END AS [Item Number],

	CASE 
		WHEN PricingRuleSequenceNumber BETWEEN 4 AND 6
			THEN LLSPGCode
		ELSE 
			'ALL'
	END AS [LLSPG Code],

	CASE 
		WHEN PricingRuleSequenceNumber BETWEEN 1 AND 3
			OR PricingRuleSequenceNumber BETWEEN 4 AND 6
			THEN VendorNumber
		ELSE 
			'ALL'
	END AS [Vendor Number],

	InstructionLevelType AS [Contract Instruction Type],

    PyramidCode AS [Pyramid Code],

    CoveringBandGroupValues AS GroupValues,
    PricingRuleSequenceNumber AS [Price Rule Level],
    PricingRuleDescription AS [Price Rule Level Description],

	FloorClaimBackPercent   AS [Floor %],
	TargetClaimBackPercent  AS [Target %],
	StretchClaimBackPercent AS [Stretch%], 

    FullContractNumber,
	InstructionLevelType AS [Datapoint Instruction Level],
	InstructionLevelCode AS [Datapoint InstructionLevelCode],
	LLSPGCode AS [Datapoint LLSPGCode],
	MPGCode AS [Datapoint MPGCode],
	ItemNumber AS [Datapoint ItemNumber],
	ClaimBackPercent AS [Datapoint ClaimBackPercent],
	Sales603010Bucket AS [Datapoint Sales Bucket],
	VendorDescription AS [Vendor Description],
	ItemDescription AS [Item Description]

FROM
	cg.vwDatapoint dp


--where pb.ExcludeForSegmentationIndicator = 'N'









GO
