SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE VIEW [ppe].[vw_DelegationEmail]

/*
	 select * from webuserdelegatelog
*/

AS
    SELECT 
			dl.WebUserDelegateLogKey,
			dl.DelegatorWebUserKey,
			od1.FullName as DelegatorFullName,
			od1.Email as DelegatorEmail,
			dl.DelegateWebUserKey,
			od2.FullName as DelegateFullName,
			od2.Email as DelegateEmail,
			dl.ActorWebUserKey,
			od3.FullName as ActorFullName,
			od3.Email as ActorEmail,
			dl.Action,
			dl.StartDate,
			dl.EndDate
    FROM    [dbo].[WebUserDelegateLog] dl
            JOIN dbo.WebUser wu1 ON wu1.WebUserKey = dl.DelegatorWebUserKey
			JOIN ODSPerson od1 ON od1.ODSPersonKey = wu1.ODSPersonKey 
			JOIN dbo.WebUser wu2 ON wu2.WebUserKey = dl.DelegateWebUserKey
			JOIN ODSPerson od2 ON od2.ODSPersonKey = wu2.ODSPersonKey
			JOIN dbo.WebUser wu3 ON wu3.WebUserKey = dl.ActorWebUserKey
			JOIN ODSPerson od3 ON od3.ODSPersonKey = wu3.ODSPersonKey
	WHERE 
			dl.EmailStatus = 'P'

GO
