SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE VIEW [ppe].[vw_RequestBenefit] AS

WITH AutoEscalated AS (
	select distinct RequestKey from ppe.RequestLog
	where RequestAction = 'Auto-escalate'
	and OldStatusKey = 2 
)

SELECT 
    r.RequestKey AS [Request ID],
	da.[AccountNumber] AS [Account Number],
	da.[AccountName] AS [Account Name],
	CAST(CAST(r.CreationDate AS DATE) AS DATETIME) AS [Request Date],
	ISNULL(wu3.UserName, 'coordinatorUser') AS [Account Owner],
	dag1.AG1Level1 AS [Branch],
	dag1.AG1Level2 AS Network,
	dag1.AG1Level3 AS Area,
	dag1.AG1Level4 AS Region,
	dag1.AG1Level1UDVarchar1 AS [Branch Name],
	dag1.AG1Level2UDVarchar1 AS [Network Description],
	dag1.AG1Level3UDVarchar1 AS [Area Description],
	dag1.AG1Level4UDVarchar1 AS [Region Description],
	ig3.IG3Level1 AS [LLSPG Code] ,
	ig3.IG3Level1UDVarchar1 AS [LLSPG Description] ,
	CASE 
		WHEN r.ItemKey <> 1 THEN SUBSTRING(di.ItemNumber, 2, 255)
		ELSE NULL
	END AS [Product Code],
	CASE 
		WHEN r.ItemKey <> 1 THEN di.ItemDescription
		ELSE NULL
	END AS [Product Description],
	rs.StatusName AS Status,
	CASE WHEN r.ItemKey = 1 THEN r.Total12MonthSales ELSE 0 END AS [Sales],
	rsd.Discount AS [Recommended Discount] ,
	rp.Price AS [Recommended Price] ,
	r.RecommendedImpact AS [Recommended Impact] ,
	ISNULL([NewExceptionDiscount], [NewCompoundDiscount]) AS [New Discount] ,
	r.NewExceptionFixedPrice AS [New Exception Fixed Price] ,
	CASE WHEN rs.StatusName IN ('Approved','Exported') THEN [AcceptedImpact] ELSE 0 END AS [Accepted Impact] ,
	CASE 
		WHEN rs.StatusName IN ('Approved','Closed','Exported') and a.RequestKey IS NULL THEN 1
		ELSE 0
	END AS [Incoming Term],
	CASE 
		WHEN rs.StatusName IN ('Approved','Exported') AND rsd.Discount IS NOT NULL AND rsd.Discount=COALESCE([NewExceptionDiscount], [NewCompoundDiscount], 0) THEN 1
		WHEN rs.StatusName IN ('Approved','Exported') AND rp.Price IS NOT NULL AND rp.Price = [NewExceptionFixedPrice] THEN 1
		ELSE 0
	END AS [Accepted Term],
	CASE WHEN RTRIM(ISNULL(da.AccountUDVarChar8,'')) = '' THEN 'Branch' ELSE da.AccountUDVarChar8 END AS [Account Owner Type],
	r.AccountKey,
	r.ItemGroup3Key,
	r.ItemKey,
	ctd.Discount AS [Wolcen Discount],
	CASE 
		WHEN rs.StatusName IN ('Approved','Exported') THEN 1
		ELSE 0
	END AS Approved,
	CASE 
		WHEN rs.StatusName IN ('Approved','Exported') AND ABS(ctd.Discount - nsd.Discount) <= 0.002 THEN 1
		ELSE 0
	END AS Stuck
    FROM [ppe].[RequestMain] r
    JOIN dbo.DimAccount da ON da.AccountKey = r.AccountKey
	JOIN dbo.DimAccountGroup1 dag1 ON dag1.AccountGroup1Key = da.AccountGroup1Key
    JOIN dbo.DimItem di ON di.ItemKey = r.ItemKey
    JOIN dbo.WebUser wu1 ON wu1.WebUserKey = r.[CreatedByWebUserKey]
    JOIN dbo.WebUser wu2 ON wu2.WebUserKey = r.ModifiedByWebUserKey
    JOIN ppe.RequestStatus rs ON rs.StatusKey = r.StatusKey
    JOIN ppe.RequestType rqt ON rqt.TypeKey = r.TypeKey
    JOIN dbo.DimInvoiceLineGroup1 ilg1 ON ilg1.InvoiceLineGroup1Key = r.InvoiceLineGroup1Key
    JOIN dbo.DimItemGroup3 ig3 ON ig3.ItemGroup3Key = r.ItemGroup3Key
	JOIN dbo.WebUser wu3 ON wu3.WebUserKey= r.OwnerWebUserKey
	LEFT JOIN lion.PFCustomerTerms ct ON ct.AccountKey = r.AccountKey AND ct.ItemGroup3Key = r.ItemGroup3Key AND
	                                   ct.ItemKey = r.ItemKey
	LEFT JOIN AutoEscalated a on r.RequestKey = a.RequestKey
	CROSS APPLY ppe.fn_GetPrice(r.ProposedExceptionFixedPrice, di.ItemUDDecimal1, r.ProposedExceptionDiscount, ISNULL(r.ProposedCompoundDiscount, 0)) rp
	CROSS APPLY ppe.fn_GetSimpleDiscount(r.ProposedExceptionFixedPrice, di.ItemUDDecimal1, r.ProposedExceptionDiscount, ISNULL(r.ProposedCompoundDiscount, 0)) rsd
	CROSS APPLY ppe.fn_GetSimpleDiscount(r.NewExceptionFixedPrice, di.ItemUDDecimal1, r.NewExceptionDiscount, ISNULL(r.NewCompoundDiscount, 0)) nsd
	CROSS APPLY ppe.fn_GetCompoundDiscount(ct.UsualDiscount1, ct.UsualDiscount2) cd 
	CROSS APPLY ppe.fn_GetSimpleDiscount(CASE WHEN r.ItemKey = 1 THEN NULL ELSE ct.ExceptionFixedPrice END, di.ItemUDDecimal1, CASE WHEN r.ItemKey = 1 THEN NULL ELSE ct.ExceptionDiscount/100 END,  ISNULL(cd.Discount/100, 0)) ctd



GO
