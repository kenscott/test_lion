SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE VIEW [dbo].[vw_DimAccountWebUserView]
AS 
	SELECT wd.DelegatorWebUserKey AS ViewerWebUserKey, da.AccountKey, da.AccountNumber, da.AccountName, da.AccountUDVarchar12 AS FixedPriceIndicator
	FROM dbo.DimAccount da
	JOIN dbo.WebUser wu ON wu.AccountManagerKey = da.AccountManagerKey
	JOIN dbo.WebUserView wuv 
		ON wuv.ViewedWebUserKey = wu.WebUserKey
	JOIN dbo.WebUserDelegate wd
		ON wd.DelegateWebUserKey = wuv.ViewerWebUserKey
		AND GETDATE() BETWEEN wd.StartDate AND ISNULL(wd.EndDate,'12-31-9999')
	WHERE AccountNumber = AccountNumberParent



GO
