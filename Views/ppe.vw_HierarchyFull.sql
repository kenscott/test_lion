SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE VIEW [ppe].[vw_HierarchyFull] AS 

	SELECT NEWID() AS GUID, wu2.WebUserKey AS SubjectWebUserKey, od2.FullName SubjectFullName, 'ManagerOf' AS RelationshipType, wu1.WebUserKey AS ObjectWebUserKey, od1.FullName AS ObjectFullName
	FROM dbo.BridgeAccountManager bam
	JOIN dbo.WebUser wu1 ON bam.SubsidiaryAccountManagerKey = wu1.AccountManagerKey
	JOIN dbo.WebUser wu2 ON bam.ParentAccountManagerKey = wu2.AccountManagerKey
	JOIN dbo.ODSPerson od1 ON od1.ODSPersonKey = wu1.ODSPersonKey
	JOIN dbo.ODSPerson od2 ON od2.ODSPersonKey = wu2.ODSPersonKey
	WHERE bam.NumberOfLevels = 1
	UNION

	SELECT NEWID() AS GUID, wu1.WebUserKey AS SubjectWebUserKey, od1.FullName SubjectFullName, 'SubordinateOf' AS RelationshipType, wu2.WebUserKey AS ObjectWebUserKey, od2.FullName AS ObjectFullName
	FROM dbo.BridgeAccountManager bam
	JOIN dbo.WebUser wu1 ON bam.SubsidiaryAccountManagerKey = wu1.AccountManagerKey
	JOIN dbo.WebUser wu2 ON bam.ParentAccountManagerKey = wu2.AccountManagerKey
	JOIN dbo.ODSPerson od1 ON od1.ODSPersonKey = wu1.ODSPersonKey
	JOIN dbo.ODSPerson od2 ON od2.ODSPersonKey = wu2.ODSPersonKey
	WHERE bam.NumberOfLevels = 1
	UNION

	SELECT NEWID() AS GUID, wu2.WebUserKey AS SubjectWebUserKey, od2.FullName SubjectFullName, 'DelegateOf' AS RelationshipType, wu1.WebUserKey AS ObjectWebUserKey, od1.FullName AS ObjectFullName
	FROM dbo.WebUserDelegate wud
	JOIN dbo.WebUser wu1 ON wud.DelegatorWebUserKey = wu1.WebUserKey
	JOIN dbo.WebUser wu2 ON wud.DelegateWebUserKey = wu2.WebUserKey
	JOIN dbo.ODSPerson od1 ON od1.ODSPersonKey = wu1.ODSPersonKey
	JOIN dbo.ODSPerson od2 ON od2.ODSPersonKey = wu2.ODSPersonKey
	WHERE DelegateWebUserKey <> DelegatorWebUserKey
	AND GETDATE() BETWEEN wud.StartDate AND wud.EndDate
	UNION

	SELECT NEWID() AS GUID, wu1.WebUserKey AS SubjectWebUserKey, od1.FullName SubjectFullName, 'DelegatorOf' AS RelationshipType, wu2.WebUserKey AS ObjectWebUserKey, od2.FullName AS ObjectFullName
	FROM dbo.WebUserDelegate wud
	JOIN dbo.WebUser wu1 ON wud.DelegatorWebUserKey = wu1.WebUserKey
	JOIN dbo.WebUser wu2 ON wud.DelegateWebUserKey = wu2.WebUserKey
	JOIN dbo.ODSPerson od1 ON od1.ODSPersonKey = wu1.ODSPersonKey
	JOIN dbo.ODSPerson od2 ON od2.ODSPersonKey = wu2.ODSPersonKey
	WHERE DelegateWebUserKey <> DelegatorWebUserKey
	AND GETDATE() BETWEEN wud.StartDate AND wud.EndDate

GO
