SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO






CREATE 
VIEW [lion].[vwCustomerTermsChangesForYesterday]
AS

/*

SELECT TOP 10 * FROM lion.vwCustomerTermsChangesForYesterday WITH (NOLOCK)

SELECT TOP 10 * FROM lion.CustomerTermsPrevious WITH (NOLOCK)

*/

SELECT 
	ChangeAction,
	--CustomerTermsKey,
	--AccountKey,
	--ItemKey,
	--ItemGroup3Key,
	AccountId,
	AccountId_Previous,
	PyramidCode,
	PyramidCode_Previous,
	SPGCode,
	SPGCode_Previous,
	OwningBrandCode,
	OwningBrandCode_Previous,
	Usualdiscount1,
	Usualdiscount1_Previous,
	Usualdiscount2,
	Usualdiscount2_Previous,
	DeleteFlag,
	DeleteFlag_Previous,
	LastChangeInitials,
	LastChangeInitials_Previous,
	LastChangeDate,
	LastChangeDate_Previous,
	LastChangeTime,
	LastChangeTime_Previous,
	LastChangePLID,
	LastChangePLID_Previous,
	LastChangeBranch,
	LastChangeBranch_Previous,
	ExceptionProduct,
	ExceptionProduct_Previous,
	ExceptionDiscount,
	ExceptionDiscount_Previous,
	ExceptionFixedPrice,
	ExceptionFixedPrice_Previous,
	ExceptionFromDate,
	ExceptionFromDate_Previous,
	ExceptionToDate,
	ExceptionToDate_Previous,
	ExceptionPCF,
	ExceptionPCF_Previous,
	FallbackMarginFlag,
	FallbackMarginFlag_Previous,

	ddCreationDate.SQLDate AS CreationDate,
	ddCreationDate_Previous.SQLDate AS CreationDate_Previous,

	ddModificationDate.SQLDate AS ModificationDate,
	ddModificationDate_Previous.SQLDate AS ModificationDayKey_Previous,

	--CreationDayKey,----------------------------------------
	--ModificationDayKey,------------------------------------


	/*  vs not intermixed 
	--CustomerTermsKey_Previous,
	--AccountKey_Previous,
	--ItemKey_Previous,
	--ItemGroup3Key_Previous,
	AccountId_Previous,
	PyramidCode_Previous,
	SPGCode_Previous,

	OwningBrandCode_Previous,
	Usualdiscount1_Previous,
	Usualdiscount2_Previous,
	DeleteFlag_Previous,
	LastChangeInitials_Previous,
	LastChangeDate_Previous,
	LastChangeTime_Previous,
	LastChangePLID_Previous,
	LastChangeBranch_Previous,
	ExceptionProduct_Previous,
	ExceptionDiscount_Previous,
	ExceptionFixedPrice_Previous,
	ExceptionFromDate_Previous,
	ExceptionToDate_Previous,
	ExceptionPCF_Previous,
	FallbackMarginFlag_Previous,
	ddCreationDate_Previous.SQLDate AS CreationDate_Previous,
	ddModificationDate_Previous.SQLDate AS ModificationDayKey_Previous,

	--CreationDayKey_Previous,
	--ModificationDayKey_Previous,
	*/

	ISNULL(CASE ct.ItemKey WHEN 1 THEN s2.Sales ELSE s1.Sales END,0) AS Total12MonthSales,
	ISNULL(CASE ct.ItemKey WHEN 1 THEN s2.Cost ELSE s1.Cost END,0) AS Total12MonthCost,
	ISNULL(CASE ct.ItemKey WHEN 1 THEN (s2.Sales - s2.Cost) / NULLIF(s2.Sales, 0) ELSE s1.Cost END,0) AS Total12MonthGPP

FROM lion.CustomerTermsPrevious ct WITH (NOLOCK)

LEFT JOIN dbo.DimDay ddCreationDate WITH (NOLOCK)
	ON ddCreationDate.DayKey = CreationDayKey

LEFT JOIN dbo.DimDay ddModificationDate WITH (NOLOCK)
	ON ddModificationDate.DayKey = ModificationDayKey

LEFT JOIN dbo.DimDay ddCreationDate_Previous WITH (NOLOCK)
	ON ddCreationDate_Previous.DayKey = CreationDayKey_Previous

LEFT JOIN dbo.DimDay ddModificationDate_Previous WITH (NOLOCK)
	ON ddModificationDate_Previous.DayKey = ModificationDayKey_Previous

LEFT JOIN (
		SELECT 
			AccountKey, 
			ItemKey, 
			SUM(Total12MonthSales) AS Sales,
			SUM(Total12MonthCost) AS Cost
		FROM dbo.FactLastPriceAndCost WITH (NOLOCK)
		GROUP BY 
			AccountKey, 
			ItemKey
		) s1
	ON s1.AccountKey = ct.AccountKey
	AND s1.ItemKey = ct.ItemKey
	AND ct.ItemKey <> 1
LEFT JOIN (
		SELECT 
			AccountKey, 
			ItemGroup3Key, 
			SUM(Total12MonthSales) AS Sales,
			SUM(Total12MonthCost) AS Cost
		FROM dbo.FactLastPriceAndCost f WITH (NOLOCK)
		JOIN dbo.DimItem di 
			ON di.ItemKey = f.ItemKey
		GROUP BY 
			AccountKey, 
			ItemGroup3Key
	) s2
	ON s2.AccountKey = ct.AccountKey
	AND s2.ItemGroup3Key = ct.ItemGroup3Key
	AND ct.ItemKey = 1

--WHERE ModificationDayKey = /* yesterday */
--	(SELECT DayKey FROM dbo.DimDay WHERE CONVERT(VARCHAR(10), SQLDate, 120) = CONVERT(VARCHAR(10), GETDATE(), 120)) - 1

WHERE
	/*** Per https://enterbridge.atlassian.net/browse/LP2P-1059, do not return rows where the last change initails have a . in them ***/
	CHARINDEX(N'.', LastChangeInitials) = 0





GO
