SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE VIEW [ppe].[vw_RequestNotificationEmail]
AS
    SELECT 
			NEWID() AS guid,
			r.RequestKey,
			da.AccountKey,
            da.AccountName,
            da.AccountNumber,
            ig3.IG3Level1 AS SPGCode ,
            rl.requestaction AS RequestAction,
            ra.ActionAbbr AS LastAction,
            CASE WHEN r.ItemKey <> 1 THEN SUBSTRING(di.ItemNumber, 2, 255)
                 ELSE NULL
            END AS ProductCode ,
			gd.Discount AS [GreenDiscount],
			ct.Term AS CurrentTerm ,
			cpg.PG AS CurrentPG ,
            nt.Term AS NewTerm ,
            npg.PG AS NewPG ,
			( np.Price - r.CurrentCost ) / NULLIF(np.Price, 0) AS NewMargin ,
            r.AcceptedImpact AS NewImpact ,
            [StrategicItemFlag] AS StrategicItemIndicator ,
            od2.Email AS ActorEmail,
			wu2.WebUserKey AS ActorWebUserKey,
			od2.FullName AS ActorFullName,
            od1.Email AS RecipientEmail,
			od1.FullName AS RecipientFullName,
			r.OwnerWebUserKey AS RequestOwnerWebUserKey,
			rl.RequestLogKey,
			ISNULL( wus.SettingValue,'Y') AS RequestEmailIndicator,
			rl.NewOwnerWebUserKey,
			rl.OldOwnerWebUserKey,
			od5.FullName AS NewOwnerFullName,
			od6.FullName AS OldOwnerFullName,
			r.ReviewType AS ReviewType
    
    FROM    [ppe].[RequestMain] r
            JOIN dbo.DimAccount da ON da.AccountKey = r.AccountKey
            JOIN dbo.DimAccountManager dam ON dam.AccountManagerKey = da.AccountManagerKey
            JOIN dbo.DimItem di ON di.ItemKey = r.ItemKey
            JOIN dbo.WebUser wu2 ON wu2.WebUserKey = r.ModifiedByWebUserKey
			JOIN ODSPerson od2 ON od2.ODSPersonKey = wu2.ODSPersonKey 
            JOIN ppe.RequestStatus rs ON rs.StatusKey = r.StatusKey
            JOIN dbo.DimItemGroup3 ig3 ON ig3.ItemGroup3Key = r.ItemGroup3Key
			JOIN dbo.WebUser wu3 ON wu3.AccountManagerKey= dam.AccountManagerKey
			--JOIN BridgeAccountManager bam ON bam.SubsidiaryAccountManagerKey = da.AccountManagerKey
			--JOIN WebUser wu ON wu.AccountManagerKey = bam.ParentAccountManagerKey
			JOIN dbo.WebUserDelegate wd
				ON wd.DelegatorWebUserKey = r.OwnerWebUserKey -- wu.WebUserKey   -- this connects the delegators accounts
				AND GETDATE() BETWEEN wd.StartDate AND ISNULL(wd.EndDate,'12-31-9999')
			JOIN WebUser wu4 ON wu4.WebUserKey = wd.DelegateWebUserKey
			--JOIN WebUserRole wur ON wur.WebUserKey = wd.DelegateWebUserKey
			--JOIN WebRole wr ON wur.WebRoleKey = wr.WebRoleKey
			JOIN ODSPerson od1 ON od1.ODSPersonKey = wu4.ODSPersonKey 
			JOIN ppe.RequestLog rl ON rl.RequestKey = r.RequestKey
			JOIN ppe.RequestAction ra ON ra.ActionKey = r.LastActionKey
			LEFT JOIN WebUserSetting wus ON wus.WebUserKey = wu4.WebUserKey AND wus.SettingName = 'RequestEmailIndicator'
			LEFT JOIN dbo.WebUser wu5 ON wu5.WebUserKey = rl.NewOwnerWebUserKey
			LEFT JOIN dbo.WebUser wu6 ON wu6.WebUserKey = rl.OldOwnerWebUserKey
			LEFT JOIN dbo.ODSPerson od5 ON od5.ODSPersonKey = wu5.ODSPersonKey
			LEFT JOIN dbo.ODSPerson od6 ON od6.ODSPersonKey = wu6.ODSPersonKey

			CROSS APPLY ppe.fn_GetPrice(r.NewExceptionFixedPrice, r.TradePrice, r.NewExceptionDiscount, r.NewCompoundDiscount) np
	        CROSS APPLY ppe.fn_GetDiscount(r.AccountKey, r.ItemGroup3Key, r.ItemKey, r.GreenPrice, r.TradePrice, 'G') gd
	        CROSS APPLY ppe.fn_GetDiscount(r.AccountKey, r.ItemGroup3Key, r.ItemKey, r.AmberPrice, r.TradePrice, 'A') ad

			--CROSS APPLY ppe.fn_GetTerm(r.CurrentExceptionFixedPrice, r.CurrentExceptionDiscount, r.CurrentCompoundDiscount) ct
			--CROSS APPLY ppe.fn_GetTerm(r.NewExceptionFixedPrice, r.NewExceptionDiscount, r.NewCompoundDiscount) nt

			CROSS APPLY ppe.fn_GetTerm(r.CurrentExceptionFixedPrice, r.CurrentExceptionDiscount, r.CurrentCompoundDiscount, CASE WHEN r.CurrentExceptionFixedPrice IS NULL THEN 'TD' ELSE 'FP' END) ct
			CROSS APPLY ppe.fn_GetTerm(r.NewExceptionFixedPrice, r.NewExceptionDiscount, ISNULL(r.NewCompoundDiscount,0), r.NewTermType) nt

			CROSS APPLY ppe.fn_GetSimpleDiscount(r.CurrentExceptionFixedPrice, r.TradePrice, r.CurrentExceptionDiscount, r.CurrentCompoundDiscount) csd
			CROSS APPLY ppe.fn_GetSimpleDiscount(r.NewExceptionFixedPrice, r.TradePrice, r.NewExceptionDiscount, r.NewCompoundDiscount) ncd
			CROSS APPLY ppe.fn_GetPG(csd.Discount, ad.Discount, gd.Discount) cpg
			CROSS APPLY ppe.fn_GetPG(ncd.Discount, ad.Discount, gd.Discount) npg
	WHERE rl.EmailSent = 0
	--AND  wr.WorkflowLevel  = rs.StatusPosition -- where the level of that person is the same as the level of the request. 
	--AND ISNULL( wus.SettingValue,'Y') <> 'N'


























GO
