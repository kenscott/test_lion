SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE 
VIEW [lion].[vwFactInvoiceLineGroup1] 

AS

/*
SELECT * FROM lion.vwFactInvoiceLineGroup1
*/

SELECT
	InvoiceLineGroup1Key,	
	InvoiceLineGroup1UDVarchar1 AS ShipmentType,
	InvoiceLineGroup1UDVarchar2 AS DeviatedIndicator
FROM DimInvoiceLineGroup1 (NOLOCK)



GO
