SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO








CREATE     view
[dbo].[ProposalCost_View]
as
Select B.PlaybookDataPointGroupKey,
A.ProposalCostKey,
A.PlaybookPriceProposalKey,
A.AcceptanceCodeKey,
A.ProjectKey,
A.AccountKey,
A.ItemKey,
A.InvoiceLineGroup1Key,
A.ProposedCost,
A.NewCost,
A.TotalActual12MonthCost,
A.TotalActual12MonthPrice,
A.TotalActual12MonthQuantity,
A.TotalActual12MonthFrequency,
A.LastItemPrice,
A.LastItemCost,
A.Forecasted12MonthQuantity,
A.TotalForecasted12MonthPrice_LastPrice,
A.TotalForecasted12MonthCost_LastCost,
A.TotalForecasted12MonthCost_ProposedCost,
A.TotalForecasted12MonthCost_NewCost,
A.AccountManagerKey
From  ProposalCost A
left join PlaybookPriceProposal_View B on
	A.PlaybookPriceProposalKey = B.PlaybookPriceProposalKey











GO
