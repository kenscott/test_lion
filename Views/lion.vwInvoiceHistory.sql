SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE 
VIEW [lion].[vwInvoiceHistory] 

/*

SELECT TOP 10 * FROM lion.vwInvoiceHistory 

*/


AS

SELECT
	NEWID() AS GUID,

	fil.AccountKey,
	fil.ItemKey,
	
	--da.AccountNumber,
	--da.AccountName,

	di.ItemNumber,
	di.ItemDescription,

	dilg1.InvoiceLineGroup1UDVarchar1 AS SupplyType,
	fil.UDVarchar1 AS PriceDerivation,
	dag1.AG1Level1 AS Branch,
	dag1.AG1Level1UDVarchar1 AS BranchName,
	dd.SQLDate AS TransactionDate,
	fil.TotalQuantity,
	fil.TotalActualPrice,
	fil.TotalActualCost,
	--fil.UDDecimal1 AS TotalMargin,
	(fil.TotalActualPrice - fil.TotalActualCost) / NULLIF(fil.TotalActualPrice, 0) AS TotalGPP,
	fil.TotalActualPrice/NULLIF(fil.TotalQuantity, 0) AS UnitPrice,
	fil.TotalActualCost/NULLIF(fil.TotalQuantity, 0) AS UnitCost,
	
	fil.RedPrice,
	fil.AmberPrice,
	fil.GreenPrice,

	ilj.InvoiceLineJunkUDVarchar3 AS DelNo, -- added by Daniel per LIO-843
	ilj.InvoiceLineJunkUDVarchar9 AS SLedgerNo, -- added by Daniel per LIO-843
	di.ItemUDDecimal1 AS CurrBranchTradePrice,
	ISNULL(                (UnitListPrice - (TotalActualPrice/NULLIF(TotalQuantity, 0.0))) / NULLIF(UnitListPrice, 0.), 0.) AS DiscountOffTrade
	--fil.UnitListPrice AS TradePrice,  -- I think they may want this at some point

FROM
	dbo.FactInvoiceLine fil (NOLOCK)
--INNER JOIN dbo.DimAccount da
--	ON da.AccountKey = fil.AccountKey
INNER JOIN dbo.DimItem di
	ON di.ItemKey = fil.ItemKey
INNER JOIN dbo.DimInvoiceLineGroup1 dilg1
	ON dilg1.InvoiceLineGroup1Key = fil.InvoiceLineGroup1Key
INNER JOIN dbo.DimAccountGroup1 dag1
	ON dag1.AccountGroup1Key = fil.AccountGroup1Key
INNER JOIN dbo.DimDay dd
	ON dd.DayKey = fil.InvoiceDateDayKey
INNER JOIN dbo.DimInvoiceLineJunk ilj
	ON ilj.InvoiceLineJunkKey = fil.InvoiceLineJunkKey
WHERE
	fil.Last12MonthsIndicator = N'Y'








GO
