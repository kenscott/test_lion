SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

--select top 10 * from [cm].[vw_RejectionReasons]

CREATE VIEW [cm].[vw_RejectionReasons] AS

SELECT 
	rr.RejectionReasonKey,
	rr.RejectionReasonValue,
	rr.CreationDate,
	rr.CreatedByWebUserKey,
	cwu.UserName AS 'CreatedByUserName',
	codsp.FullName AS 'CreatedByFullname',
	rr.ModificationDate,
	rr.ModifiedByWebUserKey,
	mwu.UserName AS 'ModifiedByUsername',
	modsp.FullName AS 'ModifiedByFullname',
	rr.Inactive
FROM cm.RejectionReason rr
INNER JOIN dbo.WebUser cwu 
	ON cwu.WebUserKey = rr.CreatedByWebUserKey
INNER JOIN dbo.ODSPerson codsp 
	ON codsp.ODSPersonKey = cwu.ODSPersonKey
LEFT JOIN dbo.WebUser mwu 
	ON mwu.WebUserKey = rr.ModifiedByWebUserKey
LEFT JOIN dbo.ODSPerson modsp 
	ON modsp.ODSPersonKey = mwu.ODSPersonKey



GO
