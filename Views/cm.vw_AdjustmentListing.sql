SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO








/*

select top 10 * from cm.vw_AdjustmentListing where ClaimId = 100209

*/


CREATE VIEW [cm].[vw_AdjustmentListing] AS

SELECT 
	ic.InvoiceClaimKey AS ClaimId,
	sc.ClaimMonth,
	v.VendorNumber AS SupplierId,
	v.VendorDescription AS SupplierName,
	con.ContractReference AS SupplierRefNo,
	con.ContractID,
	fil.ClientInvoiceLineUniqueID AS TransactionId,
	fil.AccountNumber AS AccountId,
	fil.AccountName,
	fil.ProductCode AS ProductId,
	fil.ProductDescription,
	fil.BranchCode AS LocationId,
	fil.BranchDescription AS LocationDescription,
	SUM(fil.Qty) AS Quantity,
	SUM(ISNULL(sci.ClaimBackAmount, scig.ClaimBackAmount)) AS ClaimUnit,
	SUM(cj.ClaimLineValue) AS ClaimTotal,
	0.00 AS PaidTotal


FROM cm.ClaimJournal cj
INNER JOIN cm.SupplierClaim sc
	ON sc.SupplierClaimKey = cj.SupplierClaimKey
INNER JOIN cm.InvoiceClaim ic
	ON ic.InvoiceClaimKey = cj.InvoiceClaimKey
INNER JOIN cm.FactInvoiceLine fil
	ON fil.ClientInvoiceLineUniqueID = cj.ClientInvoiceLineUniqueID
INNER JOIN cc.SupplierContract con
	ON con.ContractID = cj.CurrentContractID
LEFT JOIN cc.SupplierContractItem sci
	ON sci.ContractKey = con.ContractKey 
	AND sci.ItemKey = fil.ItemKey
LEFT JOIN dbo.DimItemGroup3 dig3
	ON dig3.IG3Level1 = fil.LLSPGCode
	AND dig3.IG3Level4 = fil.PyramidCode
LEFT JOIN cc.SupplierContractItemGroupX scig
	ON scig.ContractKey = con.ContractKey 
	AND scig.ItemGroupLevel = 3
	AND scig.ItemGroupXKey = dig3.ItemGroup3Key
INNER JOIN DimVendor v 
	ON v.VendorKey = cj.VendorKey


GROUP BY ic.InvoiceClaimKey, sc.ClaimMonth, v.VendorNumber, v.VendorDescription, con.ContractReference, con.ContractID, fil.ClientInvoiceLineUniqueID, fil.AccountNumber,
	fil.AccountName, fil.ProductCode, fil.ProductDescription, fil.BranchCode, fil.BranchDescription






















GO
