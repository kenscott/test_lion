SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [cm].[vw_ClaimLog]

AS

-- select * from cm.vw_ClaimLog

SELECT  cl.ClaimLogKey ,
        sc.SupplierClaimKey ,
        dv.VendorDescription ,
        sc.ClaimMonth ,
        cl.ClaimAction ,
        cl.Notes ,
        cl.CreationDate ,
        p.FullName AS UserName
FROM    cm.ClaimLog AS cl
        INNER JOIN dbo.WebUser AS wu ON wu.WebUserKey = cl.WebUserKey
        INNER JOIN cm.SupplierClaim AS sc ON sc.SupplierClaimKey = cl.SupplierClaimKey
        INNER JOIN dbo.DimVendor AS dv ON dv.VendorKey = sc.VendorKey
        INNER JOIN dbo.ODSPerson as p ON wu.ODSPersonKey = p.ODSPersonKey


GO
