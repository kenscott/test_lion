SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO












CREATE
VIEW [cg].[vwPriceBandAttribute2]
AS

/*

select top 100 * from cg.vwPriceBandAttribute2

select count(*) from cg.vwPriceBandAttribute
3009855

select top 100 * from cg.vwPriceBandAttribute
where GroupValues = '1,Item,3134,HZ77'

*/

WITH a AS (
	SELECT 
		PlaybookPricingGroupKey,
		COUNT(DISTINCT ContractKey) AS BandContractCount
	FROM 
		cg.vwDatapoint
	GROUP BY
		PlaybookPricingGroupKey
)
SELECT
	ppg.PlaybookPricingGroupKey AS [Pricing Rule Key],
	ScenarioName AS Playbook,

	CASE 
		WHEN apr.PricingRuleSequenceNumber = 1
			OR apr.PricingRuleSequenceNumber = 4
			THEN Sales603010Bucket
		ELSE 
			'ALL'
	END AS  [Sales Bucket],

	CASE 
		WHEN apr.PricingRuleSequenceNumber BETWEEN 1 AND 2
			OR apr.PricingRuleSequenceNumber BETWEEN 4 AND 5
			THEN CAST(JobFlag AS VARCHAR(3))
		ELSE 
			'ALL'
	END AS [Job Flag],

	CASE 
		WHEN apr.PricingRuleSequenceNumber BETWEEN 1 AND 3
			THEN ItemNumber
		ELSE 
			'ALL'
	END AS [Item Number],

	CASE 
		WHEN apr.PricingRuleSequenceNumber BETWEEN 4 AND 6
			THEN LLSPGCode
		ELSE 
			'ALL'
	END AS [LLSPG Code],

	CASE 
		WHEN apr.PricingRuleSequenceNumber BETWEEN 1 AND 3
			OR apr.PricingRuleSequenceNumber BETWEEN 4 AND 6
			THEN VendorNumber
		ELSE 
			'ALL'
	END AS [Vendor Number],

	InstructionLevelType AS [Contract Instruction Type],

    PyramidCode AS [Pyramid Code],

    ppg.GroupValues AS GroupValues,
    apr.PricingRuleSequenceNumber AS [Price Rule Level],
    PricingRuleDescription AS [Price Rule Level Description],

	TheFloor.ClaimBackPercent   AS [Floor %],
	TheTarget.ClaimBackPercent  AS [Target %],
	TheStretch.ClaimBackPercent AS [Stretch%], 

    FullContractNumber,
	InstructionLevelType AS [Datapoint Instruction Level],
	InstructionLevelCode AS [Datapoint InstructionLevelCode],
	LLSPGCode AS [Datapoint LLSPGCode],
	MPGCode AS [Datapoint MPGCode],
	ItemNumber AS [Datapoint ItemNumber],
	pdp.ClaimBackPercent AS [Datapoint ClaimBackPercent],
	Sales603010Bucket AS [Datapoint Sales Bucket],
	VendorDescription AS [Vendor Description],
	ItemDescription AS [Item Description],
	pdppg.PlaybookDataPointKey,
	pdp.ClaimBackPercent,
	BandContractCount

FROM cg.PlaybookDataPointPricingGroup pdppg (NOLOCK)
INNER JOIN dbo.PlaybookPricingGroup ppg (NOLOCK)
	ON ppg.PlaybookPricingGroupKey = pdppg.PlaybookPricingGroupKey
INNER JOIN cg.vwDatapoint2 pdp
	ON pdp.PlaybookDataPointKey = pdppg.PlaybookDataPointKey
INNER JOIN dbo.ATKPricingRule apr (NOLOCK)
	ON ppg.PricingRuleKey = apr.PricingRuleKey
INNER JOIN dbo.ATKScenario Scenario (NOLOCK)
	ON Scenario.PricingRulePlaybookKey = apr.PricingRulePlaybookKey
LEFT JOIN cg.PlaybookPricingGroupPriceBand TheFloor (NOLOCK)
	ON TheFloor.PlaybookPricingGroupKey = pdppg.PlaybookPricingGroupKey --?
	AND TheFloor.MemberRank = 
		CASE
			WHEN  ROUND ( TheFloor.RulingMemberCount * 0.25, 0) BETWEEN 1 AND TheFloor.RulingMemberCount THEN  ROUND ( TheFloor.RulingMemberCount * 0.25, 0)
			ELSE 1
		END
LEFT JOIN cg.PlaybookPricingGroupPriceBand TheTarget (NOLOCK)
	ON TheTarget.PlaybookPricingGroupKey = pdppg.PlaybookPricingGroupKey --?
	AND TheTarget.MemberRank = 
		CASE
			WHEN  ROUND ( TheTarget.RulingMemberCount * 0.60, 0) BETWEEN 1 AND TheTarget.RulingMemberCount THEN  ROUND ( TheTarget.RulingMemberCount * 0.60, 0)
			ELSE 1
		END
LEFT JOIN cg.PlaybookPricingGroupPriceBand TheStretch (NOLOCK)
	ON TheStretch.PlaybookPricingGroupKey = pdppg.PlaybookPricingGroupKey --?
	AND TheStretch.MemberRank = 
		CASE
			WHEN  ROUND ( TheStretch.RulingMemberCount * 0.80, 0) BETWEEN 1 AND TheStretch.RulingMemberCount THEN  ROUND ( TheStretch.RulingMemberCount * 0.80, 0)
			ELSE 1
		END
LEFT JOIN a
	ON a.PlaybookPricingGroupKey = ppg.PlaybookPricingGroupKey

--where pb.ExcludeForSegmentationIndicator = 'N'








GO
