SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
select count(*)
  from lion.OutgoingTerms

select count(*)
  from lion.IncomingTerms

select count(*)
  from lion.OutgoingTerms o inner join
       lion.IncomingTerms i on o.recid = i.recid

select recid, count(*)
from lion.OutgoingTerms
group by recid 
having count(*) > 1

select * from lion.OutgoingTerms
where recid = '001DF7AC-C658-4925-BC66-6ABDC1992D01'

select recid, count(*)
from lion.IncomingTerms
group by recid 
having count(*) > 1

add accountmangerfullname
*/

CREATE VIEW [lion].[vwTermsReturn] AS

select a.Region, a.RegionDescription, a.Area, a.AreaDescription, a.Network, a.NetworkDescription, a.Branch, a.BranchName, 
a.AccountManagerFullName AS AccountOwner,
a.AccountNumber, a.AccountName, 
       o.LLSPG,
       count(*) as OutgoingTerms, sum(iif(i.recid is not null, 1, 0)) as IncomingTerms,
	   sum(iif((o.RowType = 'LLSPG' and abs(rec.Discount - acc.Discount) <= 0.002) or
	           (o.RowType = 'Detail' and ((o.[New Exception Discount] is not null and abs(o.[New Exception Discount] - i.[New Exception Discount]) <= 0.002) or
			                              (o.[New Exception Fixed Price] is not null and abs(o.[New Exception Fixed Price] - i.[New Exception Fixed Price]) <= 0.009))), 1, 0)) as AcceptedTerms
  from lion.OutgoingTerms o inner join
       lion.vwAccount a on o.AccountKey = a.AccountKey left outer join
       lion.IncomingTerms i on o.recid = i.recid
	   cross apply lion.fn_GetCompoundDiscount(o.[New SPG Discount 1], o.[New SPG Discount 2]) as rec
	   cross apply lion.fn_GetCompoundDiscount(i.[New SPG Discount 1], i.[New SPG Discount 2]) as acc
 where (o.RowType = 'LLSPG' and o.[New SPG Discount 1] is not null) or
       (o.RowType = 'Detail' and (o.[New Exception Discount] is not null or o.[New Exception Fixed Price] is not null))
group by  a.Region, a.RegionDescription, a.Area, a.AreaDescription, a.Network, a.NetworkDescription, a.Branch, a.BranchName, a.AccountManagerFullName, a.AccountNumber, a.AccountName, o.LLSPG


GO
