SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [cc].[vw_ContractLogNote]
AS

SELECT 
	NEWID() AS GUID,
	scn.ContractKey,
	scn.WebUserKey,
	'Note' as LogOrNote,
	scn.CreationDate,
	wu.UserName,
	dv.VendorNumber as SupplierCode,
	'' AS ContractAction,
	scn.Notes,
	null AS OldStatusKey,
	null AS NewStatusKey,
	null AS OldUpdateStatusKey,
	null AS NewUpdateStatusKey
	FROM cc.SupplierContractNote scn
	JOIN [dbo].[WebUser] wu 
		ON wu.WebUserKey = scn.WebUserKey
	JOIN cc.SupplierContract sc
		ON sc.ContractKey = scn.ContractKey
	JOIN [dbo].[DimVendor] dv
		ON dv.VendorKey = sc.VendorKey

UNION

SELECT 
	NEWID() AS GUID,
	scl.ContractKey,
	scl.WebUserKey,
	'Log' AS LogOrNote,
	scl.CreationDate,
	wu.UserName,
	dv.VendorNumber as SupplierCode,
	scl.ContractAction as ContractAction,
	scl.Notes, 
	NULL as OldStatusKey,
	NULL as NewStatusKey,
	NULL as OldUpdateStatusKey,
	NULL as NewUpdateStatusKey
	FROM cc.SupplierContractLog scl
	JOIN [dbo].[WebUser] wu 
		ON wu.WebUserKey = scl.WebUserKey
	JOIN cc.SupplierContract sc
		ON sc.ContractKey = scl.ContractKey
	JOIN [dbo].[DimVendor] dv
		ON dv.VendorKey = sc.VendorKey
GO
