SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO








CREATE      view
[dbo].[PlaybookPricingGroupOptimalPrice_View]
as
Select B.PlaybookDataPointGroupKey, 
		B.BandSequenceNumber,
		B.PlaybookPricingGroupKey,
		A.PlaybookPricingGroupOptimalPriceKey,
		A.PricingBandOptimalPricePercentKey,
		A.PlaybookPricingGroupQuantityBandKey,
		A.OptimalPrice,
		A.OptimalCost,
		A.OptimalMargin,
		C.PricingRuleBandKey, 
		C.OptimalPriceMethodDesc, 
		C.OptimalPricePercent, 
		C.OptimalPriceType
From  PlaybookPricingGroupOptimalPrice A
Left join ATKPricingBandOptimalPricePercent C on	 --Used to determine what kind of optimal price was assigned to the DP
	A.PricingBandOptimalPricePercentKey = C.PricingBandOptimalPricePercentKey
join PlaybookPricingGroupQuantityBand_View B on 
	A.PlaybookPricingGroupQuantityBandKey = B.PlaybookPricingGroupQuantityBandKey









GO
