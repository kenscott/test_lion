SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO









CREATE VIEW [cc].[vw_ContractItem] AS

	SELECT NEWID() AS GUID, sci.ContractKey, di.ItemKey, di.ItemNumber, di.ItemDescription, di.ItemUDVarchar20 AS ProductCode, di.CurrentInvoiceCost,wu1.UserName AS CreatedByUserName,sci.CreatedOnDT,wu2.UserName AS ModifiedByUserName,sci.ModifiedOnDT
	FROM cc.SupplierContractItem sci 
	JOIN dbo.DimItem di ON di.ItemKey = sci.ItemKey
	LEFT JOIN dbo.WebUser wu1 ON wu1.WebUserKey = sci.CreatedByWebUserKey
	LEFT JOIN dbo.WebUser wu2 ON wu2.WebUserKey = sci.ModifiedByWebUserKey
	

	UNION ALL

	SELECT NEWID() AS GUID, igx.ContractKey, di.ItemKey, di.ItemNumber, di.ItemDescription, di.ItemUDVarchar20 AS ProductCode, di.CurrentInvoiceCost ,wu1.UserName AS CreatedByUserName,igx.CreatedOnDT,wu2.UserName AS ModifiedByUserName,igx.ModifiedOnDT
	FROM cc.SupplierContractItemGroupX igx
	JOIN dbo.DimItemGroup1 dig1 ON dig1.ItemGroup1Key = igx.ItemGroupXKey
	JOIN dbo.DimItem di ON di.ItemGroup1Key = dig1.ItemGroup1Key
	LEFT JOIN dbo.WebUser wu1 ON wu1.WebUserKey = igx.CreatedByWebUserKey
	LEFT JOIN dbo.WebUser wu2 ON wu2.WebUserKey = igx.ModifiedByWebUserKey
	WHERE igx.ItemGroupLevel = 1
	

	UNION ALL

	SELECT NEWID() AS GUID, igx.ContractKey, di.ItemKey, di.ItemNumber, di.ItemDescription, di.ItemUDVarchar20 AS ProductCode , di.CurrentInvoiceCost,wu1.UserName AS CreatedByUserName,igx.CreatedOnDT,wu2.UserName AS ModifiedByUserName,igx.ModifiedOnDT
	FROM cc.SupplierContractItemGroupX igx
	JOIN dbo.DimItemGroup3 dig3 ON dig3.ItemGroup3Key = igx.ItemGroupXKey
	JOIN dbo.DimItem di ON di.ItemGroup3Key = dig3.ItemGroup3Key
	LEFT JOIN dbo.WebUser wu1 ON wu1.WebUserKey = igx.CreatedByWebUserKey
	LEFT JOIN dbo.WebUser wu2 ON wu2.WebUserKey = igx.ModifiedByWebUserKey
	WHERE igx.ItemGroupLevel = 3
	








GO
