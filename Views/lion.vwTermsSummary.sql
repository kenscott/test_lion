SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [lion].[vwTermsSummary]
AS
select a.AccountManagerFullName as [Account Owner], IIF(a.AccountOwnerType='', 'Branch', a.AccountOwnerType) as [Account Owner Type], a.AccountNumber, a.AccountName, ig3.LLSPGCode, ig3.LLSPGDescription, substring(i.ItemNumber,2,255) as ItemNumber, i.ItemDescription, OriginalDiscount, NewDiscount, convert(datetime, NewWolcenDate) as EffectiveDate, [Sales @ Original], [Sales @ New], ProjectedBenefit, tt.NewException
  from lion.TermsTracker tt inner join
  	   lion.vwAccount a on tt.AccountKey = a.AccountKey inner join
	   lion.vwItemGroup3 ig3 on tt.ItemGroup3Key = ig3.ItemGroup3Key left outer join
	   lion.vwItem i on tt.ItemKey = i.ItemKey



GO
