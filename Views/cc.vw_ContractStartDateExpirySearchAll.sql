SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [cc].[vw_ContractStartDateExpirySearchAll]
AS
    WITH    CTE_ContractKey ( ContractKey )
              AS ( SELECT DISTINCT
                            ContractKey
                   FROM     [cc].[vw_ContractAccount]
                   WHERE    StartDate IS NOT NULL
                            OR ExpirationDate IS NOT NULL
                   UNION
                   SELECT DISTINCT
                            ContractKey
                   FROM     [cc].[vw_ContractLocation]
                   WHERE    StartDate IS NOT NULL
                            OR ExpirationDate IS NOT NULL
                   UNION
                   SELECT DISTINCT
                            ContractKey
                   FROM     [cc].[SupplierContractItem]
                   WHERE    StartDT IS NOT NULL
                            OR ExpiryDT IS NOT NULL
                 )
    SELECT DISTINCT
            sc.ContractID ,
            sc.ContractOwner ,
            sc.VendorKey AS "Supplier ID" ,
            dv.VendorDescription AS "Supplier Name" ,
            sc.StartDate AS "Contract Start Date" ,
            sc.ExpirationDate AS "Contract Expiry Date" ,
            ca.AccountKey AS "Account ID" ,
            ca.AccountName AS "Account Name" ,
            ca.StartDate AS "Account Start Date" ,
            ca.ExpirationDate AS "Account Expiry Date" ,
            ci.ItemNumber AS "Product Code" ,
            ci.ItemDescription AS "Product Description" ,
            "Product Type" = CASE WHEN scig.ItemGroupLevel = 1 THEN 'MPG'
                                  WHEN scig.ItemGroupLevel = 3 THEN 'LLSPG'
                                  ELSE 'PRODUCT'
                             END ,
            cl.LocationKey AS "Location Code" ,
            cl.LocationName AS "Location Name" ,
            cl.StartDate AS "Location Start Date" ,
            cl.ExpirationDate AS "Location Expiry Date"
    FROM    cc.SupplierContract AS sc
            LEFT OUTER JOIN DimVendor AS dv ON sc.VendorKey = dv.VendorKey
            LEFT OUTER JOIN cc.[vw_ContractAccount] ca ON sc.ContractKey = ca.ContractKey
            LEFT OUTER JOIN [cc].[vw_ContractItem] ci ON sc.ContractKey = ci.ContractKey
            LEFT OUTER JOIN [cc].[vw_ContractLocation] cl ON sc.ContractKey = cl.ContractKey
            LEFT OUTER JOIN [cc].[SupplierContractItemGroupX] scig ON sc.ContractKey = scig.ContractKey
    WHERE   sc.ContractKey IN ( SELECT  ContractKey
                                FROM    CTE_ContractKey )




GO
