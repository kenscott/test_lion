SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE VIEW [dbo].[vw_WebUserPermission] AS
	--SELECT wup.WebUserPermissionKey AS ID, wup.WebUserKey, wup.WebUserPermissionKey, wp.WebPermissionName, wp.WebPermissionDescription
	--FROM dbo.WebUser wu
	--JOIN dbo.WebUserPermission wup ON wup.WebUserKey = wu.WebUserKey
	--JOIN dbo.WebPermission wp ON wp.WebPermissionKey = wup.WebPermissionKey



/* KFK remvoed first select as part of LPF-5927 (and LPF-5144)
	-- Get all of the permissions EXPLICITLY assigned to the user (through WebUserPermission) -jed 10/26/2015
	-- NOTE: While not really supported through design, the current schema does support this so it is included here -jed 10/26/2015
	SELECT DISTINCT wp.WebPermissionKey AS ID, wud.DelegateWebUserKey AS WebUserKey, /*wup.WebUserPermissionKey,*/ wp.WebPermissionName, wp.WebPermissionDescription
	FROM WebUserDelegate wud
	JOIN dbo.WebUser wu ON wu.WebUserKey = wud.DelegatorWebUserKey
	JOIN dbo.WebUserPermission wup ON wup.WebUserKey = wu.WebUserKey
	JOIN dbo.WebPermission wp ON wp.WebPermissionKey = wup.WebPermissionKey
	
	UNION
*/
	
	-- Get all of the permissions IMPLICITLY assigned to the user (through WebUserRole => WebRole => WebRolePermission -jed 10/26/2015
	SELECT DISTINCT wp.WebPermissionKey AS ID, wud.DelegateWebUserKey AS WebUserKey, /*wrp.WebRolePermissionKey,*/ wp.WebPermissionName, wp.WebPermissionDescription
	FROM WebUserDelegate wud
	JOIN dbo.WebUser wu ON wu.WebUserKey = wud.DelegatorWebUserKey
	JOIN dbo.WebUserRole wur ON wur.WebUserKey = wu.WebUserKey
	JOIN dbo.WebRole wr ON wr.WebRoleKey = wur.WebRoleKey
	JOIN dbo.WebRolePermission wrp ON wrp.WebRoleKey = wur.WebRoleKey
	JOIN dbo.WebPermission wp ON wp.WebPermissionKey = wrp.WebPermissionKey




GO
