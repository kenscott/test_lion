SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





CREATE VIEW [lion].[vwPriceBandCoverage]
AS

--  select top 100 * from lion.vwPriceBandCoverage

    SELECT  
            ScenarioName AS Playbook,
			PlaybookPricingGroupKey AS [Pricing Rule Key],
            GroupValues AS [Band Attributes],
			PlaybookPricingGroupKey AS [Price Rule Level],
			PricingRuleDescription AS [Price Rule Level Description],

			COUNT(PlaybookPricingGroupKey) AS [Count],
            SUM(TotalActual12MonthPrice) AS [Total Sales],
            SUM(TotalActual12MonthGP) AS [Total GP],

			AVG(StretchGPP) AS [Stretch GTM],
			AVG(TargetGPP) AS [Target GTM],
			AVG(FloorGPP) AS [Floor GTM],

			SUM(GPP$Stretch) AS [GTM$-Stretch],
			SUM(GPP$Target) AS [GTM$-Target],
			SUM(GPP$Floor) AS [GTM$-Floor],

			SUM(StretchImpact) AS [Stretch Impact],
			SUM(TargetImpact) AS [Target Impact],
			SUM(FloorImpact) AS [Floor Impact],

			SUM(TargetToStretchImpact) AS [Target to Stretch Impact],
			SUM(FloorToTargetImpact) AS [Floor to Target Impact],
			
			SUM([StretchRisk]) AS [Stretch Risk],

			ItemPyramidCode AS [Pyramid Code],
			RegionExceptionIndicator AS [Region Exception Indicator],
			PlaybookProductBrand AS [Playbook Item Brand],
			ShipmentType AS [Ship Type],
            ContractClaimsIndicator AS [Contract Claims Indicator],
            LLSPG AS [LL SPG],
            FullItemDescription AS [Item#],
			PriceApproach AS [Price Approach],
			StrategicItem AS [Strategic Item],
			Sales603010Bucket AS [Sales 603010 Bucket],
            CustomerSpecialism AS [Customer Specialism],
			PlaybookRegion AS [Playbook Region],

			SUM(TotalActual12MonthCost) AS [Total Cost],
			(SUM(TotalActual12MonthPrice) - SUM(TotalActual12MonthCost)) / NULLIF(SUM(TotalActual12MonthPrice), 0) AS [Total GTM],
			--SUM(TotalSalesAtTrade) AS [Total Sales @ Trade],

			SUM(StretchSales) AS StretchSales,
			SUM(TargetSales)  AS TargetSales,
			SUM(FloorSales)   AS FloorSales

    FROM    
		lion.vwPriceBandAttributes

    GROUP BY 
	
	        ScenarioName,
			PlaybookPricingGroupKey,
            GroupValues,
			PlaybookPricingGroupKey,
			PricingRuleDescription,
			ItemPyramidCode,
			RegionExceptionIndicator,
			PlaybookProductBrand,
			ShipmentType,
            ContractClaimsIndicator,
            LLSPG,
            FullItemDescription,
			PriceApproach,
			StrategicItem,
			Sales603010Bucket,
            CustomerSpecialism,
			PlaybookRegion









GO
