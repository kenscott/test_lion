SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [ppe].[vw_AccountSPGDiscount] AS
	SELECT  NEWID() AS GUID,
			f.AccountKey ,
			f.ItemGroup3Key ,
			( 1
			  - ISNULL(( SELECT SUM(f.TotalQuantity * ( ( i.ItemUDDecimal3
														  - i.ItemUDDecimal4
														  - i.ItemUDDecimal5 )
														/ NULLIF(1 - FloorGPP.GPP, 0) ))
								/ NULLIF(SUM(f.TotalQuantity * i.ItemUDDecimal1),
										 0)
					   ), 1) ) RedDiscount ,
			( 1
			  - ISNULL(( SELECT SUM(f.TotalQuantity * ( ( i.ItemUDDecimal3
														  - i.ItemUDDecimal4
														  - i.ItemUDDecimal5 )
														/ NULLIF(1 - TargetGPP.GPP, 0) ))
								/ NULLIF(SUM(f.TotalQuantity * i.ItemUDDecimal1),
										 0)
					   ), 1) ) AmberDiscount,
			( 1
			  - ISNULL(( SELECT SUM(f.TotalQuantity * ( ( i.ItemUDDecimal3
														  - i.ItemUDDecimal4
														  - i.ItemUDDecimal5 )
														/ NULLIF(1 - StretchGPP.GPP, 0) ))
								/ NULLIF(SUM(f.TotalQuantity * i.ItemUDDecimal1),
										 0)
					   ), 1) ) GreenDiscount 
	FROM    FactInvoiceLinePricebandScore f
	INNER JOIN dbo.DimItem i /*WITH(INDEX(PK_ItemDimension)) */ ON f.ItemKey = i.ItemKey
	CROSS APPLY lion.fn_GetCappedGPP(i.ItemUDDecimal3 - i.ItemUDDecimal4 - i.ItemUDDecimal5, f.FloorGPP,  0, i.ItemUDDecimal1) FloorGPP
	CROSS APPLY lion.fn_GetCappedGPP(i.ItemUDDecimal3 - i.ItemUDDecimal4 - i.ItemUDDecimal5, f.StretchGPP,0, i.ItemUDDecimal1) StretchGPP
	CROSS APPLY lion.fn_GetCappedGPP(i.ItemUDDecimal3 - i.ItemUDDecimal4 - i.ItemUDDecimal5, f.TargetGPP, 0, i.ItemUDDecimal1) TargetGPP
	GROUP BY f.AccountKey , f.ItemGroup3Key


GO
