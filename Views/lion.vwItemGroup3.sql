SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO









CREATE VIEW [lion].[vwItemGroup3] 

/*
SELECT TOP 10 * FROM lion.vwItemGroup3 
*/

AS

SELECT
	dig3.ItemGroup3Key,

	IG3Level1 AS LLSPGCode,--1
	IG3Level2 AS HLSPGCode,--2
	IG3Level3 AS GSPGCode,--3
	IG3Level4 AS PYRCode,--4
	IG3Level1UDVarchar1 AS LLSPGDescription,--1
	IG3Level2UDVarchar1 AS HLSPGDescription,--2
	IG3Level3UDVarchar1 AS GSPGDescription--, --3
	--IG3Level4UDVarchar1 AS PYRCode--4

FROM dbo.DimItemGroup3 dig3 (NOLOCK)









GO
