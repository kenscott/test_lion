SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE VIEW [dbo].[vw_WebUserView] AS

	SELECT *, NEWID() AS GUID FROM (
		SELECT DISTINCT 
			wd.DelegateWebUserKey AS ViewerWebUserKey,
			wuv.ViewedWebUserKey,
			op.FullName AS ViewedFullName
		FROM dbo.WebUserDelegate wd
		JOIN dbo.WebUserView wuv ON wd.DelegatorWebUserKey = wuv.ViewerWebUserKey
		JOIN dbo.WebUser wu ON wu.WebUserKey = wuv.ViewedWebUserKey
		JOIN dbo.ODSPerson op ON op.ODSPersonKey = wu.ODSPersonKey
		WHERE GETDATE() BETWEEN wd.StartDate AND ISNULL(wd.EndDate,'12-31-9999')  
		
		UNION

		SELECT -- ensure everyone has an entry for themselves
			wu.WebUserKey,
			wu.WebUserKey,
			odsp.FullName
		FROM dbo.WebUser wu
		INNER JOIN ODSPerson ODSP ON ODSP.ODSPersonKey = wu.ODSPersonKey
		
		) a1
GO
