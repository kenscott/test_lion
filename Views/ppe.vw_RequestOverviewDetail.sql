SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE VIEW [ppe].[vw_RequestOverviewDetail] AS

/*

SELECT TOP 10 * FROM ppe.vw_RequestOverviewDetail

*/

SELECT 
	DISTINCT
	--NEWID() AS GUID,
	wuv.ViewerWebUserKey, 
	rm.StatusKey, 
	rs.StatusName, 
	rm.RequestKey,

	rm.AccountKey,
	da.AccountName,
	da.AccountNumber,

	rm.ItemGroup3Key,
	dig3.IG3Level1 AS LLSPG,
	dig3.IG3Level1UDVarchar1 AS LLSPGDescription,

	rm.ItemKey,
            
	--di.ItemNumber
	CASE WHEN rm.ItemKey <> 1 THEN SUBSTRING(di.ItemNumber, 2, 255)
	ELSE NULL
	END AS ItemNumber,  -- trim the pyramid code from itemnumber  jed 1-22-2016
                  
	di.ItemDescription,

	rm.CurrentCompoundDiscount,
	rm.ProposedCompoundDiscount,
	rm.NewCompoundDiscount,

	rm.CurrentExceptionDiscount,
	rm.CurrentExceptionFixedPrice,
	rm.ProposedExceptionDiscount,
	rm.ProposedExceptionFixedPrice,
	rm.NewExceptionDiscount,
	rm.NewExceptionFixedPrice,

	rm.NewDeleteTerm,
	rm.ProposedDeleteTerm,

	rm.AcceptedImpact,

	rm.Total12MonthSales,

	od.FullName AS AccountOwnerFullName

FROM ppe.RequestMain rm 
INNER JOIN ppe.RequestStatus rs ON rs.StatusKey = rm.StatusKey
INNER JOIN dbo.DimAccount da ON da.AccountKey = rm.AccountKey
INNER JOIN dbo.DimItemGroup3 dig3 ON dig3.ItemGroup3Key = rm.ItemGroup3Key--LPF-2650
INNER JOIN dbo.DimItem di ON di.ItemKey = rm.ItemKey
INNER JOIN dbo.DimBrand db ON db.Brand = dig3.IG3Level5 --LPF-2650
LEFT JOIN dbo.DimAccountBrandManager dabm ON dabm.AccountKey = rm.AccountKey AND dabm.BrandKey = db.BrandKey  --LPF-2650
INNER JOIN dbo.WebUser wu ON wu.AccountManagerKey = ISNULL(dabm.AccountManagerKey,da.AccountManagerKey) --changed back per LIO-1064 -- changed per LIO-1046 and email thread. 7/31/2015
INNER JOIN dbo.ODSPerson od ON od.ODSPersonKey = wu.ODSPersonKey
INNER JOIN dbo.WebUserDelegate wd
	ON wd.DelegatorWebUserKey = wu.WebUserKey --rm.OwnerWebUserKey   -- this connects the delegators accounts
	AND GETDATE() BETWEEN wd.StartDate AND ISNULL(wd.EndDate,'12-31-9999')
INNER JOIN dbo.WebUserView wuv ON wuv.ViewedWebUserKey = wd.DelegateWebUserKey
--WHERE wuv.ViewerWebUserKey = 1






GO
