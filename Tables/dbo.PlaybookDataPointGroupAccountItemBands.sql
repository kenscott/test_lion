CREATE TABLE [dbo].[PlaybookDataPointGroupAccountItemBands]
(
[PlaybookDataPointGroupKey] [dbo].[Key_Normal_type] NOT NULL,
[AccountKey] [dbo].[Key_Normal_type] NOT NULL,
[ItemKey] [dbo].[Key_Normal_type] NOT NULL,
[AccountItemBand] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[PlaybookDataPointGroupAccountItemBands] ADD CONSTRAINT [PK_PlaybookDataPointGroupAccountItemBands] PRIMARY KEY CLUSTERED  ([PlaybookDataPointGroupKey], [AccountKey], [ItemKey]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[PlaybookDataPointGroupAccountItemBands] ADD CONSTRAINT [FK_PlaybookDataPointGroupAccountItemBands_1] FOREIGN KEY ([PlaybookDataPointGroupKey]) REFERENCES [dbo].[PlaybookDataPointGroup] ([PlaybookDataPointGroupKey])
GO
ALTER TABLE [dbo].[PlaybookDataPointGroupAccountItemBands] ADD CONSTRAINT [FK_PlaybookDataPointGroupAccountItemBands_2] FOREIGN KEY ([AccountKey]) REFERENCES [dbo].[DimAccount] ([AccountKey])
GO
ALTER TABLE [dbo].[PlaybookDataPointGroupAccountItemBands] ADD CONSTRAINT [FK_PlaybookDataPointGroupAccountItemBands_3] FOREIGN KEY ([ItemKey]) REFERENCES [dbo].[DimItem] ([ItemKey])
GO
