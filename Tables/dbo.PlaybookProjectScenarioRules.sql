CREATE TABLE [dbo].[PlaybookProjectScenarioRules]
(
[PlaybookProjectScenarioRulesKey] [dbo].[Key_Normal_type] NOT NULL IDENTITY(1, 1),
[ProjectKey] [dbo].[Key_Normal_type] NOT NULL,
[ScenarioKey] [dbo].[Key_Normal_type] NOT NULL,
[TextData] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[DataType] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[PlaybookProjectScenarioRules] ADD CONSTRAINT [FK_PlaybookProjectScenarioRules_1] FOREIGN KEY ([ProjectKey]) REFERENCES [dbo].[PlaybookProject] ([ProjectKey])
GO
ALTER TABLE [dbo].[PlaybookProjectScenarioRules] ADD CONSTRAINT [FK_PlaybookProjectScenarioRules_2] FOREIGN KEY ([ScenarioKey]) REFERENCES [dbo].[ATKScenario] ([ScenarioKey])
GO
