CREATE TABLE [dbo].[DimAccountGroup3]
(
[AccountGroup3Key] [dbo].[Key_Normal_type] NOT NULL IDENTITY(1, 1),
[AG3Level1] [dbo].[Description_Small_type] NOT NULL,
[AG3Level2] [dbo].[Description_Small_type] NOT NULL CONSTRAINT [DF_DimAccountGroup3_Level2] DEFAULT ('None'),
[AG3Level3] [dbo].[Description_Small_type] NOT NULL CONSTRAINT [DF_DimAccountGroup3_Level3] DEFAULT ('None'),
[AG3Level4] [dbo].[Description_Small_type] NOT NULL CONSTRAINT [DF_DimAccountGroup3_Level4] DEFAULT ('None'),
[AG3Level5] [dbo].[Description_Small_type] NOT NULL CONSTRAINT [DF_DimAccountGroup3_Level5] DEFAULT ('None'),
[AG3Level1UDVarchar1] [dbo].[UDVarchar_type] NULL,
[AG3Level1UDVarchar2] [dbo].[UDVarchar_type] NULL,
[AG3Level1UDVarchar3] [dbo].[UDVarchar_type] NULL,
[AG3Level2UDVarchar1] [dbo].[UDVarchar_type] NULL,
[AG3Level2UDVarchar2] [dbo].[UDVarchar_type] NULL,
[AG3Level2UDVarchar3] [dbo].[UDVarchar_type] NULL,
[AG3Level3UDVarchar1] [dbo].[UDVarchar_type] NULL,
[AG3Level3UDVarchar2] [dbo].[UDVarchar_type] NULL,
[AG3Level3UDVarchar3] [dbo].[UDVarchar_type] NULL,
[AG3Level4UDVarchar1] [dbo].[UDVarchar_type] NULL,
[AG3Level4UDVarchar2] [dbo].[UDVarchar_type] NULL,
[AG3Level4UDVarchar3] [dbo].[UDVarchar_type] NULL,
[AG3Level5UDVarchar1] [dbo].[UDVarchar_type] NULL,
[AG3Level5UDVarchar2] [dbo].[UDVarchar_type] NULL,
[AG3Level5UDVarchar3] [dbo].[UDVarchar_type] NULL,
[CreationDayKey] [dbo].[DateKey_type] NOT NULL,
[ModificationDayKey] [dbo].[DateKey_type] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[DimAccountGroup3] ADD CONSTRAINT [PK_DimAccountGroup3] PRIMARY KEY CLUSTERED  ([AccountGroup3Key]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [I_DimAccountGroup3_1] ON [dbo].[DimAccountGroup3] ([AG3Level1]) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [UI_DimMiscGroup3_1] ON [dbo].[DimAccountGroup3] ([AG3Level1], [AG3Level2], [AG3Level3], [AG3Level4], [AG3Level5]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [I_DimAccountGroup3_UD7] ON [dbo].[DimAccountGroup3] ([AG3Level1UDVarchar1]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [I_DimAccountGroup3_UD8] ON [dbo].[DimAccountGroup3] ([AG3Level1UDVarchar2]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [I_DimAccountGroup3_UD9] ON [dbo].[DimAccountGroup3] ([AG3Level1UDVarchar3]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [I_DimAccountGroup3_2] ON [dbo].[DimAccountGroup3] ([AG3Level2]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [I_DimAccountGroup3_UD12] ON [dbo].[DimAccountGroup3] ([AG3Level2UDVarchar1]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [I_DimAccountGroup3_UD13] ON [dbo].[DimAccountGroup3] ([AG3Level2UDVarchar2]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [I_DimAccountGroup3_UD14] ON [dbo].[DimAccountGroup3] ([AG3Level2UDVarchar3]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [I_DimAccountGroup3_3] ON [dbo].[DimAccountGroup3] ([AG3Level3]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [I_DimAccountGroup3_UD17] ON [dbo].[DimAccountGroup3] ([AG3Level3UDVarchar1]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [I_DimAccountGroup3_UD18] ON [dbo].[DimAccountGroup3] ([AG3Level3UDVarchar2]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [I_DimAccountGroup3_UD19] ON [dbo].[DimAccountGroup3] ([AG3Level3UDVarchar3]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [I_DimAccountGroup3_4] ON [dbo].[DimAccountGroup3] ([AG3Level4]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [I_DimAccountGroup3_UD22] ON [dbo].[DimAccountGroup3] ([AG3Level4UDVarchar1]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [I_DimAccountGroup3_UD23] ON [dbo].[DimAccountGroup3] ([AG3Level4UDVarchar2]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [I_DimAccountGroup3_UD24] ON [dbo].[DimAccountGroup3] ([AG3Level4UDVarchar3]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [I_DimAccountGroup3_5] ON [dbo].[DimAccountGroup3] ([AG3Level5]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [I_DimAccountGroup3_UD27] ON [dbo].[DimAccountGroup3] ([AG3Level5UDVarchar1]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [I_DimAccountGroup3_UD28] ON [dbo].[DimAccountGroup3] ([AG3Level5UDVarchar2]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [I_DimAccountGroup3_UD29] ON [dbo].[DimAccountGroup3] ([AG3Level5UDVarchar3]) ON [PRIMARY]
GO
