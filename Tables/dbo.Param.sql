CREATE TABLE [dbo].[Param]
(
[ParamName] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ParamValue] [nvarchar] (4000) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Param] ADD CONSTRAINT [PK_Param] PRIMARY KEY CLUSTERED  ([ParamName]) ON [PRIMARY]
GO
