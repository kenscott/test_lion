CREATE TABLE [dbo].[PlaybookDataPointGroupScope]
(
[PlaybookDataPointGroupKey] [dbo].[Key_Normal_type] NOT NULL,
[AccountKey] [dbo].[Key_Normal_type] NOT NULL,
[ItemKey] [dbo].[Key_Normal_type] NOT NULL,
[InvoiceLineGroup1Key] [dbo].[Key_Normal_type] NOT NULL,
[Total12MonthQuantity] [dbo].[Quantity_Normal_type] NOT NULL,
[TotalActual12MonthPrice] [dbo].[Money_Normal_Type] NOT NULL,
[TotalActual12MonthCost] [dbo].[Money_Normal_Type] NOT NULL,
[Total12MonthInvoiceLineCount] [dbo].[Quantity_Normal_type] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[PlaybookDataPointGroupScope] ADD CONSTRAINT [PK_PlaybookDataPointGroupScope] PRIMARY KEY CLUSTERED  ([PlaybookDataPointGroupKey], [AccountKey], [ItemKey], [InvoiceLineGroup1Key]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[PlaybookDataPointGroupScope] ADD CONSTRAINT [FK_PlaybookDataPointGroupScope_1] FOREIGN KEY ([PlaybookDataPointGroupKey]) REFERENCES [dbo].[PlaybookDataPointGroup] ([PlaybookDataPointGroupKey])
GO
ALTER TABLE [dbo].[PlaybookDataPointGroupScope] ADD CONSTRAINT [FK_PlaybookDataPointGroupScope_2] FOREIGN KEY ([AccountKey]) REFERENCES [dbo].[DimAccount] ([AccountKey])
GO
ALTER TABLE [dbo].[PlaybookDataPointGroupScope] ADD CONSTRAINT [FK_PlaybookDataPointGroupScope_3] FOREIGN KEY ([ItemKey]) REFERENCES [dbo].[DimItem] ([ItemKey])
GO
ALTER TABLE [dbo].[PlaybookDataPointGroupScope] ADD CONSTRAINT [FK_PlaybookDataPointGroupScope_4] FOREIGN KEY ([InvoiceLineGroup1Key]) REFERENCES [dbo].[DimInvoiceLineGroup1] ([InvoiceLineGroup1Key])
GO
