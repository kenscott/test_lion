CREATE TABLE [cg].[ATKBandPercentile]
(
[ATKBandPercentileKey] [dbo].[Key_Normal_type] NOT NULL IDENTITY(1, 1),
[SequenceNumber] [int] NOT NULL,
[FloorPercentile] [dbo].[Percent_Type] NOT NULL,
[TargetPercentile] [dbo].[Percent_Type] NOT NULL,
[StretchPercentile] [dbo].[Percent_Type] NOT NULL,
[Offset] [dbo].[Percent_Type] NOT NULL CONSTRAINT [DF_ATKPriceBandPercentile_Offset] DEFAULT ((0)),
[CreationDate] [datetime] NOT NULL,
[CreatedByWebUserKey] [dbo].[Key_Normal_type] NOT NULL,
[ModificationDate] [datetime] NOT NULL CONSTRAINT [DF_ATKPriceBandPercentile_moddate] DEFAULT (getdate()),
[ModifiedByWebUserKey] [dbo].[Key_Normal_type] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [cg].[ATKBandPercentile] ADD CONSTRAINT [PK_cg_ATKBandPercentile] PRIMARY KEY CLUSTERED  ([ATKBandPercentileKey]) ON [PRIMARY]
GO
