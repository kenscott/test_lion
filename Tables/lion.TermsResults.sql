CREATE TABLE [lion].[TermsResults]
(
[Branch] [dbo].[Description_Small_type] NOT NULL,
[CountPending] [int] NULL,
[CountReturned] [int] NULL,
[CountAccepted] [int] NULL,
[PercentAccepted] [decimal] (30, 19) NULL,
[ProposedImpactAmount] [decimal] (38, 8) NULL,
[AcceptedImpactAmount] [decimal] (38, 8) NULL,
[PercentAmountAccepted] [decimal] (38, 6) NULL,
[CountStuck] [decimal] (38, 8) NULL,
[PercentStuck] [decimal] (38, 8) NULL,
[ActualImpact] [decimal] (38, 6) NOT NULL,
[ProjectedImpact] [decimal] (38, 8) NULL,
[PriceEffect] [decimal] (38, 6) NULL,
[VolumeEffect] [decimal] (38, 6) NULL,
[TotalSalesImpact] [decimal] (38, 6) NULL
) ON [PRIMARY]
GO
