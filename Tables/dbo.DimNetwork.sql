CREATE TABLE [dbo].[DimNetwork]
(
[NetworkKey] [dbo].[Key_Small_type] NOT NULL IDENTITY(1, 1),
[NetworkId] [dbo].[Description_Small_type] NOT NULL,
[NetworkDescription] [dbo].[UDVarchar_type] NULL,
[CreationDayKey] [smallint] NOT NULL,
[ModificationDayKey] [smallint] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[DimNetwork] ADD CONSTRAINT [PK_DimNetwork] PRIMARY KEY CLUSTERED  ([NetworkKey]) ON [PRIMARY]
GO
