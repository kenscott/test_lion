CREATE TABLE [ppe].[RecommendSPG]
(
[RecommendSPGKey] [int] NOT NULL IDENTITY(1, 1),
[RecommendKey] [int] NOT NULL,
[ItemGroup3Key] [dbo].[Key_Normal_type] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [ppe].[RecommendSPG] ADD CONSTRAINT [PK_RecommendSPG] PRIMARY KEY CLUSTERED  ([RecommendSPGKey]) ON [PRIMARY]
GO
ALTER TABLE [ppe].[RecommendSPG] ADD CONSTRAINT [FK_Recommend_SPG] FOREIGN KEY ([RecommendKey]) REFERENCES [ppe].[Recommend] ([RecommendKey])
GO
ALTER TABLE [ppe].[RecommendSPG] ADD CONSTRAINT [FK_RecommendSPG_IG3] FOREIGN KEY ([ItemGroup3Key]) REFERENCES [dbo].[DimItemGroup3] ([ItemGroup3Key])
GO
