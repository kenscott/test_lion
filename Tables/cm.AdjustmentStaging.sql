CREATE TABLE [cm].[AdjustmentStaging]
(
[AdjustmentStagingKey] [int] NOT NULL IDENTITY(1, 1),
[ClaimID] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Period] [int] NULL,
[SupplierId] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SupplierRef] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ContractId] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ChargeNoteId] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TransactionId] [int] NULL,
[AccountId] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ProductId] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LocationId] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Adjustment] [decimal] (19, 3) NULL,
[Distribute] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Immediate] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AdjustmentReasonCode] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RowNumber] [int] NOT NULL,
[ErrorMessage] [varchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AdjustmentType] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BatchKey] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [cm].[AdjustmentStaging] ADD CONSTRAINT [PK_AdjustmentStaging] PRIMARY KEY CLUSTERED  ([AdjustmentStagingKey]) ON [PRIMARY]
GO
