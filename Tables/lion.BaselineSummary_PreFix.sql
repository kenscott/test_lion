CREATE TABLE [lion].[BaselineSummary_PreFix]
(
[Region] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Area] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Network] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Branch] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[PriceGroup] [varchar] (9) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[BaselineSales] [decimal] (38, 8) NULL,
[BaselineCost] [decimal] (38, 8) NULL,
[BaselineScoredSales] [decimal] (38, 15) NULL,
[BaselinePriceIndex] [decimal] (38, 7) NULL,
[BaselineAvgDailySales] [decimal] (38, 8) NULL
) ON [PRIMARY]
GO
