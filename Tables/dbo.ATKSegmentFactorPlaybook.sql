CREATE TABLE [dbo].[ATKSegmentFactorPlaybook]
(
[SegmentFactorPlaybookKey] [dbo].[Key_Normal_type] NOT NULL IDENTITY(1, 1),
[BlendedRankRule] [dbo].[Description_Small_type] NOT NULL,
[CoreNonCoreThreshold] [dbo].[Percent_Type] NOT NULL,
[AccountItemSalesLowerLimit] [dbo].[Quantity_Normal_type] NOT NULL,
[AccountItemFrequencyLowerLimit] [dbo].[Quantity_Normal_type] NOT NULL,
[AccountItemQuantityLowerLimit] [dbo].[Quantity_Normal_type] NOT NULL,
[ItemSalesLowerLimit] [dbo].[Quantity_Normal_type] NOT NULL,
[ItemFrequencyLowerLimit] [dbo].[Quantity_Normal_type] NOT NULL,
[ItemQuantityLowerLimit] [dbo].[Quantity_Normal_type] NOT NULL,
[AccountItemSalesWeight] [dbo].[Quantity_Normal_type] NOT NULL,
[AccountItemFrequencyWeight] [dbo].[Quantity_Normal_type] NOT NULL,
[AccountItemQuantityWeight] [dbo].[Quantity_Normal_type] NOT NULL,
[ItemSalesWeight] [dbo].[Quantity_Normal_type] NOT NULL,
[ItemFrequencyWeight] [dbo].[Quantity_Normal_type] NOT NULL,
[ItemQuantityWeight] [dbo].[Quantity_Normal_type] NOT NULL,
[SpanAlpha] [dbo].[Quantity_Normal_type] NOT NULL,
[SpanBeta] [dbo].[Quantity_Normal_type] NOT NULL,
[B_ItemPercentage] [dbo].[Percent_Type] NULL,
[Version] [dbo].[Int_Type] NOT NULL CONSTRAINT [DF__ATKSegmen__Versi__552D1E70] DEFAULT ((0)),
[CreationDate] [dbo].[DateTime_Type] NOT NULL CONSTRAINT [DF__ATKSegmen__Creat__6FE114AC] DEFAULT (getdate()),
[ModificationDate] [dbo].[DateTime_Type] NOT NULL CONSTRAINT [DF__ATKSegmen__Modif__0A950AE8] DEFAULT (getdate()),
[CreationUser] [dbo].[Description_Normal_type] NULL,
[ModificationUser] [dbo].[Description_Normal_type] NULL,
[ModificationUserAddress] [dbo].[Description_Normal_type] NULL,
[CreationUserAddress] [dbo].[Description_Normal_type] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ATKSegmentFactorPlaybook] ADD CONSTRAINT [PK_ATKSegmentFactorGroup] PRIMARY KEY CLUSTERED  ([SegmentFactorPlaybookKey]) ON [PRIMARY]
GO
