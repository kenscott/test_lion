CREATE TABLE [lion].[FactBaselineAISDSalesBranch_PreFix]
(
[AccountKey] [dbo].[Key_Normal_type] NOT NULL,
[ItemKey] [dbo].[Key_Normal_type] NOT NULL,
[InvoiceLineGroup1Key] [dbo].[Key_Normal_type] NOT NULL,
[AccountGroup1Key] [dbo].[Key_Normal_type] NOT NULL,
[InvoiceLineUniqueIdentifier] [int] NOT NULL,
[Sales] [dbo].[Money_Normal_Type] NOT NULL,
[Cost] [dbo].[Money_Normal_Type] NOT NULL,
[GP] [decimal] (20, 8) NULL,
[BaselineGPP] [decimal] (38, 18) NULL
) ON [PRIMARY]
GO
ALTER TABLE [lion].[FactBaselineAISDSalesBranch_PreFix] ADD CONSTRAINT [PK_FactBaselineAISDSalesBranch_PreFix] PRIMARY KEY CLUSTERED  ([AccountKey], [ItemKey], [InvoiceLineGroup1Key], [AccountGroup1Key]) ON [PRIMARY]
GO
