CREATE TABLE [dbo].[BridgeAccountManager]
(
[ParentAccountManagerKey] [dbo].[Key_Normal_type] NOT NULL,
[SubsidiaryAccountManagerKey] [dbo].[Key_Normal_type] NOT NULL,
[NumberOfLevels] [dbo].[Int_32767_type] NOT NULL,
[BottomMostFlag] [dbo].[Description_Small_type] NOT NULL,
[TopMostFlag] [dbo].[Description_Small_type] NOT NULL,
[WeightFactor] [dbo].[Quantity_Normal_type] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[BridgeAccountManager] ADD CONSTRAINT [PK_BridgeAccountManager] PRIMARY KEY CLUSTERED  ([ParentAccountManagerKey], [SubsidiaryAccountManagerKey], [NumberOfLevels]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [I_bridgeaccountmanager_1] ON [dbo].[BridgeAccountManager] ([NumberOfLevels]) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [UI_BridgeAccountManager_1] ON [dbo].[BridgeAccountManager] ([ParentAccountManagerKey], [SubsidiaryAccountManagerKey], [NumberOfLevels]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [I_BridgeAccountManager_2] ON [dbo].[BridgeAccountManager] ([SubsidiaryAccountManagerKey]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[BridgeAccountManager] ADD CONSTRAINT [FK_BridgeAccountManager_1] FOREIGN KEY ([ParentAccountManagerKey]) REFERENCES [dbo].[DimAccountManager] ([AccountManagerKey])
GO
ALTER TABLE [dbo].[BridgeAccountManager] ADD CONSTRAINT [FK_BridgeAccountManager_2] FOREIGN KEY ([SubsidiaryAccountManagerKey]) REFERENCES [dbo].[DimAccountManager] ([AccountManagerKey])
GO
