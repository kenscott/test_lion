CREATE TABLE [dbo].[ATKPricingRule]
(
[PricingRuleKey] [dbo].[Key_Normal_type] NOT NULL IDENTITY(1, 1) NOT FOR REPLICATION,
[PricingRulePlaybookKey] [dbo].[Key_Normal_type] NOT NULL,
[PricingRuleSequenceNumber] [dbo].[Int_255_type] NOT NULL,
[MinimumPoints] [dbo].[Int_Type] NOT NULL,
[MinimumPointsType] [dbo].[Description_Normal_type] NOT NULL CONSTRAINT [DF_ATKPricingRule_MinimumPointsType] DEFAULT ('PerQuantityBand'),
[RulingPointThreshold] [dbo].[Int_Type] NOT NULL,
[PricingRuleWhereClause] [dbo].[Description_Huge_type] NOT NULL CONSTRAINT [DF_ATKPricingRule_PricingRuleWhereClause] DEFAULT (''),
[UseAllDataPointsForOptimalIndicator] [dbo].[Description_Small_type] NOT NULL,
[Version] [dbo].[Int_Type] NOT NULL CONSTRAINT [DF__ATKPricin__Versi__4AAF8FFD] DEFAULT ((0)),
[CreationDate] [dbo].[DateTime_Type] NOT NULL CONSTRAINT [DF__ATKPricin__Creat__65638639] DEFAULT (getdate()),
[ModificationDate] [dbo].[DateTime_Type] NOT NULL CONSTRAINT [DF__ATKPricin__Modif__00177C75] DEFAULT (getdate()),
[CreationUser] [dbo].[Description_Normal_type] NULL,
[ModificationUser] [dbo].[Description_Normal_type] NULL,
[ModificationUserAddress] [dbo].[Description_Normal_type] NULL,
[CreationUserAddress] [dbo].[Description_Normal_type] NULL,
[PricingRuleJoinClause] [dbo].[Description_Huge_type] NOT NULL CONSTRAINT [DF_ATKPricingRule_PricingRuleJoinClause] DEFAULT (''),
[DataPointReUseIndicator] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__ATKPricin__DataP__46C230E4] DEFAULT ('N'),
[MinimumSales] [dbo].[UDDecimal_type] NOT NULL CONSTRAINT [DF_ATKPricingRule_MinimumSales] DEFAULT ((-9999999999999.99999999)),
[PricingRuleDescription] [dbo].[Description_Normal_type] NOT NULL CONSTRAINT [DF_ATKPricingRule_PricingRuleDescription] DEFAULT (''),
[HavingClause] [dbo].[Description_Huge_type] NULL,
[SectionNumber] [smallint] NOT NULL CONSTRAINT [DF_ATKPricingRule_SectionNumber] DEFAULT ((1))
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ATKPricingRule] ADD CONSTRAINT [PK_ATKPricingRule] PRIMARY KEY CLUSTERED  ([PricingRuleKey]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [I_ATKPricingRule_1] ON [dbo].[ATKPricingRule] ([PricingRulePlaybookKey]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [I_ATKPricingRule_1\2] ON [dbo].[ATKPricingRule] ([PricingRuleSequenceNumber]) INCLUDE ([PricingRuleKey]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ATKPricingRule] ADD CONSTRAINT [FK_ATKPricingRule_1] FOREIGN KEY ([MinimumPointsType]) REFERENCES [dbo].[ATKMinimumPointsType] ([MinimumPointsType])
GO
ALTER TABLE [dbo].[ATKPricingRule] ADD CONSTRAINT [FK_ATKPricingRule_2] FOREIGN KEY ([PricingRulePlaybookKey]) REFERENCES [dbo].[ATKPricingRulePlaybook] ([PricingRulePlaybookKey])
GO
EXEC sp_addextendedproperty N'MS_Description', N'90', 'SCHEMA', N'dbo', 'TABLE', N'ATKPricingRule', 'COLUMN', N'RulingPointThreshold'
GO
