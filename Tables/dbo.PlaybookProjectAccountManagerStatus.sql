CREATE TABLE [dbo].[PlaybookProjectAccountManagerStatus]
(
[ProjectAccountManagerKey] [dbo].[Key_Normal_type] NOT NULL IDENTITY(1, 1),
[ProjectKey] [dbo].[Key_Normal_type] NOT NULL,
[AccountManagerKey] [dbo].[Key_Normal_type] NOT NULL,
[ProjectAccountManagerStatus] [dbo].[Description_Normal_type] NOT NULL,
[Version] [dbo].[Int_Type] NOT NULL CONSTRAINT [DF_PlaybookProjectAccountManagerStatus_1] DEFAULT ((0)),
[CreationDate] [dbo].[DateTime_Type] NOT NULL CONSTRAINT [DF_PlaybookProjectAccountManagerStatus_2] DEFAULT (getdate()),
[ModificationDate] [dbo].[DateTime_Type] NOT NULL CONSTRAINT [DF_PlaybookProjectAccountManagerStatus_3] DEFAULT (getdate()),
[CreationUser] [dbo].[Description_Normal_type] NULL,
[ModificationUser] [dbo].[Description_Normal_type] NULL,
[ModificationUserAddress] [dbo].[Description_Normal_type] NULL,
[CreationUserAddress] [dbo].[Description_Normal_type] NULL
) ON [PRIMARY]
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE TRIGGER [dbo].[AccountManagerStatusUpdate]
ON [dbo].[PlaybookProjectAccountManagerStatus]
FOR UPDATE
AS

DECLARE @AccountManagerKey INT
DECLARE @ProjectAccountManagerStatus Description_Normal_type
DECLARE @ProjectKey INT

SELECT 
	@AccountManagerKey = AccountManagerKey,
	@ProjectAccountManagerStatus = ProjectAccountManagerStatus,
	@ProjectKey = ProjectKey
FROM Inserted

UPDATE PlaybookProjectACcountStatus
SET ProjectAccountManagerStatus = @ProjectAccountManagerStatus
WHERE ProjectKey = @ProjectKey
	AND AccountManagerKey = @AccountManagerKey
GO
ALTER TABLE [dbo].[PlaybookProjectAccountManagerStatus] ADD CONSTRAINT [PK_PlaybookProjectAccountManagerStatus] PRIMARY KEY NONCLUSTERED  ([ProjectAccountManagerKey]) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [UI_PlaybookProjectAccountManagerStatus_2] ON [dbo].[PlaybookProjectAccountManagerStatus] ([ProjectKey], [AccountManagerKey]) ON [PRIMARY]
GO
CREATE CLUSTERED INDEX [I_PlaybookProjectAccountManagerStatus_1] ON [dbo].[PlaybookProjectAccountManagerStatus] ([ProjectKey], [ProjectAccountManagerStatus]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[PlaybookProjectAccountManagerStatus] ADD CONSTRAINT [FK_PlaybookProjectAccountManagerStatus_1] FOREIGN KEY ([ProjectKey]) REFERENCES [dbo].[PlaybookProject] ([ProjectKey])
GO
ALTER TABLE [dbo].[PlaybookProjectAccountManagerStatus] ADD CONSTRAINT [FK_PlaybookProjectAccountManagerStatus_10] FOREIGN KEY ([ProjectAccountManagerStatus]) REFERENCES [dbo].[ATKAccountManagerStatus] ([AccountManagerStatus])
GO
ALTER TABLE [dbo].[PlaybookProjectAccountManagerStatus] ADD CONSTRAINT [FK_PlaybookProjectAccountManagerStatus_2] FOREIGN KEY ([AccountManagerKey]) REFERENCES [dbo].[DimAccountManager] ([AccountManagerKey])
GO
