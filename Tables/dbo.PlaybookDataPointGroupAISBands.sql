CREATE TABLE [dbo].[PlaybookDataPointGroupAISBands]
(
[PlaybookDataPointGroupKey] [dbo].[Key_Normal_type] NOT NULL,
[AccountKey] [dbo].[Key_Normal_type] NOT NULL,
[ItemKey] [dbo].[Key_Normal_type] NOT NULL,
[InvoiceLineGroup1Key] [dbo].[Key_Normal_type] NOT NULL,
[AISRiskBand] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[PlaybookDataPointGroupAISBands] ADD CONSTRAINT [PK_PlaybookDataPointGroupAISBands] PRIMARY KEY CLUSTERED  ([PlaybookDataPointGroupKey], [AccountKey], [ItemKey], [InvoiceLineGroup1Key]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[PlaybookDataPointGroupAISBands] ADD CONSTRAINT [FK_PlaybookDataPointGroupAISBands_1] FOREIGN KEY ([PlaybookDataPointGroupKey]) REFERENCES [dbo].[PlaybookDataPointGroup] ([PlaybookDataPointGroupKey])
GO
ALTER TABLE [dbo].[PlaybookDataPointGroupAISBands] ADD CONSTRAINT [FK_PlaybookDataPointGroupAISBands_2] FOREIGN KEY ([AccountKey]) REFERENCES [dbo].[DimAccount] ([AccountKey])
GO
ALTER TABLE [dbo].[PlaybookDataPointGroupAISBands] ADD CONSTRAINT [FK_PlaybookDataPointGroupAISBands_3] FOREIGN KEY ([ItemKey]) REFERENCES [dbo].[DimItem] ([ItemKey])
GO
ALTER TABLE [dbo].[PlaybookDataPointGroupAISBands] ADD CONSTRAINT [FK_PlaybookDataPointGroupAISBands_4] FOREIGN KEY ([InvoiceLineGroup1Key]) REFERENCES [dbo].[DimInvoiceLineGroup1] ([InvoiceLineGroup1Key])
GO
