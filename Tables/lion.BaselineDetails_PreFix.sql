CREATE TABLE [lion].[BaselineDetails_PreFix]
(
[Brand] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Region] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Area] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Network] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Branch] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[AccountKey] [int] NOT NULL,
[ItemKey] [int] NOT NULL,
[InvoiceLineGroup1Key] [int] NOT NULL,
[PriceGroup] [varchar] (9) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[BaselineQuantity] [decimal] (38, 8) NULL,
[BaselineSales] [decimal] (38, 8) NULL,
[BaselineCost] [decimal] (38, 8) NULL,
[BaselineGP] [decimal] (38, 8) NULL,
[LastItemPrice] [dbo].[Money_Normal_Type] NULL,
[LastItemCost] [dbo].[Money_Normal_Type] NULL,
[LastItemGPP] [decimal] (38, 18) NULL,
[BaselineScoredSales] [decimal] (38, 15) NULL,
[BaselineWorkingDays] [int] NOT NULL
) ON [PRIMARY]
GO
