CREATE TABLE [dbo].[ATKScenario]
(
[ScenarioKey] [dbo].[Key_Normal_type] NOT NULL IDENTITY(1, 1),
[ApproachTypeKey] [dbo].[Key_Small_type] NOT NULL,
[LimitDataPlaybookKey] [dbo].[Key_Normal_type] NULL,
[SegmentFactorPlaybookKey] [dbo].[Key_Normal_type] NULL,
[PricingRulePlaybookKey] [dbo].[Key_Normal_type] NULL,
[ProposalRecommendationPlaybookKey] [dbo].[Key_Normal_type] NULL,
[ScenarioCreationWebUserKey] [dbo].[Key_Normal_type] NULL,
[ScenarioName] [dbo].[Description_Small_type] NOT NULL,
[ActiveScenarioFlag] [dbo].[Description_Small_type] NOT NULL CONSTRAINT [DF_ATkScenario_ActiveScenarioFlag] DEFAULT ('Y'),
[MinDaysToRecomputeQuantityBandFromAutomaticRecompute] [dbo].[Int_32767_type] NULL,
[MinDaysToRecomputeQuantityBandFromManualChange] [dbo].[Int_32767_type] NULL,
[Version] [dbo].[Int_Type] NOT NULL CONSTRAINT [DF__ATkScenar__Versi__5344D5FE] DEFAULT ((0)),
[CreationDate] [dbo].[DateTime_Type] NOT NULL CONSTRAINT [DF__ATkScenar__Creat__6DF8CC3A] DEFAULT (getdate()),
[ModificationDate] [dbo].[DateTime_Type] NOT NULL CONSTRAINT [DF__ATkScenar__Modif__08ACC276] DEFAULT (getdate()),
[CreationUser] [dbo].[Description_Normal_type] NULL,
[ModificationUser] [dbo].[Description_Normal_type] NULL,
[ModificationUserAddress] [dbo].[Description_Normal_type] NULL,
[CreationUserAddress] [dbo].[Description_Normal_type] NULL,
[SuppressionTypeKey] [dbo].[Key_Small_type] NOT NULL CONSTRAINT [DF_ATkScenario_SuppressionTypeKey] DEFAULT ((1)),
[UDVarchar1] [dbo].[Description_Small_type] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ATKScenario] ADD CONSTRAINT [PK_ATKScenario] PRIMARY KEY CLUSTERED  ([ScenarioKey]) ON [PRIMARY]
GO
