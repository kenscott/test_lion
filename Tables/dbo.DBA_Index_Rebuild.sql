CREATE TABLE [dbo].[DBA_Index_Rebuild]
(
[SchemaName] [nvarchar] (128) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF_DBA_Index_Rebuild_SchemaName] DEFAULT ('dbo'),
[TableName] [nvarchar] (128) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[IndexName] [nvarchar] (128) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Cols] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FileGroupName] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[UniqueIndicator] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PKIndicator] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ClusteredIndicator] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IncludedColumns] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FilterDefinition] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[DBA_Index_Rebuild] ADD CONSTRAINT [PK_DBA_Index_Rebuild] PRIMARY KEY CLUSTERED  ([SchemaName], [TableName], [IndexName]) ON [PRIMARY]
GO
