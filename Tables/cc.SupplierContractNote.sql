CREATE TABLE [cc].[SupplierContractNote]
(
[ContractNoteKey] [int] NOT NULL IDENTITY(1, 1),
[WebUserKey] [int] NOT NULL,
[ContractKey] [int] NOT NULL,
[Notes] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NoteType] [tinyint] NOT NULL CONSTRAINT [DF__DimContra__NoteT__500ED158] DEFAULT ((1)),
[CreationDate] [datetime] NOT NULL CONSTRAINT [DF__DimContra__Creat__5102F591] DEFAULT (getdate())
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [cc].[SupplierContractNote] ADD CONSTRAINT [PK_ContractNote] PRIMARY KEY NONCLUSTERED  ([ContractNoteKey]) ON [PRIMARY]
GO
