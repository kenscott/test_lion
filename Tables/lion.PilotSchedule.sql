CREATE TABLE [lion].[PilotSchedule]
(
[Branch] [dbo].[Description_Small_type] NOT NULL,
[BranchType] [dbo].[Description_Small_type] NOT NULL,
[LiveDate] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [lion].[PilotSchedule] ADD CONSTRAINT [PK_PilotSchedule] PRIMARY KEY CLUSTERED  ([Branch]) ON [PRIMARY]
GO
