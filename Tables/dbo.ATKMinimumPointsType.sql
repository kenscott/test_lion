CREATE TABLE [dbo].[ATKMinimumPointsType]
(
[MinimumPointsType] [dbo].[Description_Normal_type] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ATKMinimumPointsType] ADD CONSTRAINT [PK_ATKMinimumPointsType] PRIMARY KEY CLUSTERED  ([MinimumPointsType]) ON [PRIMARY]
GO
