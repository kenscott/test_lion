CREATE TABLE [lion].[PricingContract]
(
[PricingContractKey] [int] NOT NULL IDENTITY(1, 1),
[PricingContractOwner] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PricingContractName] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Inactive] [bit] NOT NULL CONSTRAINT [DF_PricingContract_Inactive] DEFAULT ((0))
) ON [PRIMARY]
GO
ALTER TABLE [lion].[PricingContract] ADD CONSTRAINT [PK_PricingContract] PRIMARY KEY CLUSTERED  ([PricingContractKey]) ON [PRIMARY]
GO
