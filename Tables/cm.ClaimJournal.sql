CREATE TABLE [cm].[ClaimJournal]
(
[ClaimJournalKey] [int] NOT NULL IDENTITY(1, 1),
[ODSClaimJournalKey] [int] NULL,
[ClaimMonth] [int] NULL,
[ClientInvoiceLineUniqueID] [int] NULL,
[PyramidCode] [varchar] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[VendorNumber] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[VendorDescription] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[OriginalContractID] [varchar] (9) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CurrentContractID] [varchar] (9) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ClaimType] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ClaimLineValue] [decimal] (19, 8) NULL,
[TradingMargin] [decimal] (19, 8) NULL,
[FinancialMargin] [decimal] (19, 8) NULL,
[ConfidentialTermsGPAdjustAmt] [decimal] (19, 8) NULL,
[PriceDeferralGPAdjustAmt] [decimal] (19, 8) NULL,
[SpecialDealGPAdjustAmt] [decimal] (19, 8) NULL,
[ClearanceDealGPAdjustAmt] [decimal] (19, 8) NULL,
[ClaimJournalStatus] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ClaimJournalStatusDate] [datetime] NULL,
[ARPResult] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ARPResultDate] [datetime] NULL,
[ARPReason] [varchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ClaimJournalReasonCodeKey] [int] NULL,
[ARPRecommendation] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ADVRecommendation] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ADVRecommendationWebUserKey] [int] NULL,
[ADVRecommendationDate] [datetime] NULL,
[VendorKey] [int] NULL,
[CreationDate] [datetime] NULL,
[CreatedByWebUserKey] [int] NULL,
[ModificationDate] [datetime] NULL,
[ModifiedByWebUserKey] [int] NULL,
[BatchKey] [int] NULL,
[SupplierClaimKey] [int] NULL,
[InvoiceClaimKey] [int] NULL,
[ADVBatchKey] [int] NULL,
[ODSBatchKey] [int] NULL,
[RejectionReasonKey] [int] NULL,
[AdjustmentReasonKey] [int] NULL,
[Comment] [dbo].[Description_Huge_type] NULL,
[SupplierRef] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ChargeNoteId] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustomerId] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BranchCode] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ProductCode] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ProductQty] [decimal] (7, 2) NULL,
[ImportBatchKey] [int] NULL,
[OriginalInvoiceClaimKey] [int] NULL,
[OriginalClientInvoiceLineUniqueID] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [cm].[ClaimJournal] ADD CONSTRAINT [PK_ClaimJournal] PRIMARY KEY CLUSTERED  ([ClaimJournalKey]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [I_ClaimJournal_3] ON [cm].[ClaimJournal] ([ClaimJournalStatus], [VendorKey], [VendorNumber], [CurrentContractID], [ClaimLineValue]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [I_ClaimJournal_4] ON [cm].[ClaimJournal] ([ClaimType], [ClaimJournalStatus]) INCLUDE ([ClientInvoiceLineUniqueID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [I_ClaimJournal_5] ON [cm].[ClaimJournal] ([ClientInvoiceLineUniqueID], [ClaimType]) INCLUDE ([ClaimJournalKey]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [i_VendorClaimMonth] ON [cm].[ClaimJournal] ([VendorKey], [ClaimMonth]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [I_ClaimJournal_2] ON [cm].[ClaimJournal] ([VendorNumber], [ClaimJournalStatus], [CurrentContractID], [ClaimLineValue]) ON [PRIMARY]
GO
