CREATE TABLE [ppe].[RequestStatus]
(
[StatusKey] [smallint] NOT NULL IDENTITY(1, 1),
[StatusPosition] [smallint] NOT NULL,
[StatusName] [varchar] (40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[StatusRollup] [varchar] (40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [ppe].[RequestStatus] ADD CONSTRAINT [PK_RequestStatus] PRIMARY KEY CLUSTERED  ([StatusKey]) ON [PRIMARY]
GO
