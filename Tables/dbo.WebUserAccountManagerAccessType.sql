CREATE TABLE [dbo].[WebUserAccountManagerAccessType]
(
[WebUserAccountManagerAccessTypeKey] [dbo].[Key_Normal_type] NOT NULL IDENTITY(1, 1),
[WebUserKey] [dbo].[Key_Normal_type] NOT NULL,
[AccountManagerKey] [dbo].[Key_Normal_type] NOT NULL,
[AccessType] [dbo].[Description_Small_type] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[WebUserAccountManagerAccessType] ADD CONSTRAINT [PK_WebUserAccountMangerAccessType] PRIMARY KEY NONCLUSTERED  ([WebUserAccountManagerAccessTypeKey]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [I_WebUserAccountMangerAccessType_1] ON [dbo].[WebUserAccountManagerAccessType] ([AccountManagerKey]) ON [PRIMARY]
GO
CREATE UNIQUE CLUSTERED INDEX [UI_WebUserAccountMangerAccessType_1] ON [dbo].[WebUserAccountManagerAccessType] ([WebUserKey], [AccountManagerKey], [AccessType]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[WebUserAccountManagerAccessType] ADD CONSTRAINT [FK_WebUserAccountMangerAccessType_1] FOREIGN KEY ([WebUserKey]) REFERENCES [dbo].[WebUser] ([WebUserKey])
GO
ALTER TABLE [dbo].[WebUserAccountManagerAccessType] ADD CONSTRAINT [FK_WebUserAccountMangerAccessType_2] FOREIGN KEY ([AccountManagerKey]) REFERENCES [dbo].[DimAccountManager] ([AccountManagerKey])
GO
