CREATE TABLE [dbo].[Message]
(
[MessageKey] [dbo].[Key_Normal_type] NOT NULL IDENTITY(1, 1),
[MessageName] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[MessageValue] [nvarchar] (4000) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[StartDate] [datetime] NULL CONSTRAINT [DF_Message_StartDate] DEFAULT (getdate()),
[EndDate] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Message] ADD CONSTRAINT [PK_Message] PRIMARY KEY NONCLUSTERED  ([MessageKey]) ON [PRIMARY]
GO
CREATE UNIQUE CLUSTERED INDEX [UCI_Message_1] ON [dbo].[Message] ([MessageName]) ON [PRIMARY]
GO
