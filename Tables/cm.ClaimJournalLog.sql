CREATE TABLE [cm].[ClaimJournalLog]
(
[ClaimJournalLogKey] [int] NOT NULL IDENTITY(1, 1),
[WebUserKey] [int] NULL,
[ClaimJournalKey] [int] NULL,
[Notes] [varchar] (4000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreationDate] [datetime] NOT NULL CONSTRAINT [DF__ContractNo__Creat__6D430875] DEFAULT (getdate()),
[ClaimAction] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [cm].[ClaimJournalLog] ADD CONSTRAINT [PK_ClaimJournalLog] PRIMARY KEY NONCLUSTERED  ([ClaimJournalLogKey]) ON [PRIMARY]
GO
