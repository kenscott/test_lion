CREATE TABLE [lion].[StagingUserHierarchy]
(
[Level0 Name] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Level0 Email Address] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Level0 Branch] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Level0 Brand] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Level1 Name] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Level1 Email Address] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Level1 Title] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Level1 Assignment] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Level2 Name] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Level2 Email Address] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Level2 Title] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Level2 Assignment] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RowIndex] [int] NOT NULL,
[Message] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF_StagingUserHierarchy_Message] DEFAULT ('')
) ON [PRIMARY]
GO
ALTER TABLE [lion].[StagingUserHierarchy] ADD CONSTRAINT [PK_StagingUserHierarchy] PRIMARY KEY CLUSTERED  ([RowIndex]) ON [PRIMARY]
GO
