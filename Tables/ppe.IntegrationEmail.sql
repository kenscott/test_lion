CREATE TABLE [ppe].[IntegrationEmail]
(
[FromAddress] [varchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ToAddress] [varchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IsHTML] [bit] NULL CONSTRAINT [DF_IntegrationEmail_IsHTML] DEFAULT ((1)),
[EmailSubject] [nvarchar] (520) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[EmailBody] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Retries] [smallint] NULL CONSTRAINT [DF_IntegrationEmail_Retries] DEFAULT ((0)),
[EmailStatus] [varchar] (25) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[StatusComment] [varchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreatedDT] [datetime] NULL CONSTRAINT [DF_IntegrationEmail_CreatedDT] DEFAULT (getdate()),
[IntegrationEmailKey] [int] NOT NULL IDENTITY(1, 1),
[SentDT] [datetime] NULL,
[FromName] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ToName] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ReplyToAddress] [varchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Attachment1Name] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Attachment1Data] [varbinary] (max) NULL,
[Attachment2Name] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Attachment2Data] [varbinary] (max) NULL,
[Attachment3Name] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Attachment3Data] [varbinary] (max) NULL,
[Attachment4Name] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Attachment4Data] [varbinary] (max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [ppe].[IntegrationEmail] ADD CONSTRAINT [PK_EmailQueueLog] PRIMARY KEY NONCLUSTERED  ([IntegrationEmailKey]) ON [PRIMARY]
GO
