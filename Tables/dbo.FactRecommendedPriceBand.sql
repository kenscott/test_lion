CREATE TABLE [dbo].[FactRecommendedPriceBand]
(
[AccountKey] [dbo].[Key_Normal_type] NOT NULL,
[ItemKey] [dbo].[Key_Normal_type] NOT NULL,
[InvoiceLineGroup1Key] [dbo].[Key_Normal_type] NOT NULL,
[FloorGPP] [dbo].[Percent_Type] NULL,
[TargetGPP] [dbo].[Percent_Type] NULL,
[StretchGPP] [dbo].[Percent_Type] NULL,
[PlaybookPricingGroupKey] [dbo].[Key_Normal_type] NULL,
[PricingRuleSequenceNumber] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[FactRecommendedPriceBand] ADD CONSTRAINT [PK_FactRecommendedPriceBand] PRIMARY KEY CLUSTERED  ([AccountKey], [ItemKey], [InvoiceLineGroup1Key]) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [I_4] ON [dbo].[FactRecommendedPriceBand] ([AccountKey], [ItemKey], [InvoiceLineGroup1Key]) INCLUDE ([FloorGPP], [PlaybookPricingGroupKey], [StretchGPP], [TargetGPP]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [I_2] ON [dbo].[FactRecommendedPriceBand] ([ItemKey]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [I_3] ON [dbo].[FactRecommendedPriceBand] ([PlaybookPricingGroupKey]) ON [PRIMARY]
GO
CREATE STATISTICS [_dta_stat_1675869037_1_3] ON [dbo].[FactRecommendedPriceBand] ([AccountKey], [InvoiceLineGroup1Key])
GO
CREATE STATISTICS [_dta_stat_1675869037_1_10_3] ON [dbo].[FactRecommendedPriceBand] ([AccountKey], [PlaybookPricingGroupKey], [InvoiceLineGroup1Key])
GO
CREATE STATISTICS [_dta_stat_1675869037_2_3_1_10] ON [dbo].[FactRecommendedPriceBand] ([ItemKey], [InvoiceLineGroup1Key], [AccountKey], [PlaybookPricingGroupKey])
GO
CREATE STATISTICS [_dta_stat_1675869037_10_3_2] ON [dbo].[FactRecommendedPriceBand] ([ItemKey], [PlaybookPricingGroupKey], [InvoiceLineGroup1Key])
GO
ALTER TABLE [dbo].[FactRecommendedPriceBand] ADD CONSTRAINT [FK_FactRecommendedPriceBand_1] FOREIGN KEY ([AccountKey]) REFERENCES [dbo].[DimAccount] ([AccountKey])
GO
ALTER TABLE [dbo].[FactRecommendedPriceBand] ADD CONSTRAINT [FK_FactRecommendedPriceBand_2] FOREIGN KEY ([ItemKey]) REFERENCES [dbo].[DimItem] ([ItemKey])
GO
ALTER TABLE [dbo].[FactRecommendedPriceBand] ADD CONSTRAINT [FK_FactRecommendedPriceBand_3] FOREIGN KEY ([InvoiceLineGroup1Key]) REFERENCES [dbo].[DimInvoiceLineGroup1] ([InvoiceLineGroup1Key])
GO
