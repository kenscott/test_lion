CREATE TABLE [ppe].[RequestStaging]
(
[RequestStagingKey] [int] NOT NULL IDENTITY(1, 1),
[CreatedByWebUserKey] [int] NOT NULL,
[BatchKey] [int] NOT NULL,
[AccountNumber] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SPGCode] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ProductCode] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NewSPGDiscount1] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NewSPGDiscount2] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NewExceptionDiscount] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NewExceptionFixedPrice] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NewSPGFromDate] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NewExceptionStartDate] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NewExceptionExpiryDate] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DeleteTerm] [varchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ErrorMessage] [varchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RowIndex] [int] NULL,
[StatusRollup] [varchar] (40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SPGSkippedSales] [money] NULL,
[SPGSales] [money] NULL
) ON [PRIMARY]
GO
CREATE CLUSTERED INDEX [i_batchkey] ON [ppe].[RequestStaging] ([BatchKey]) ON [PRIMARY]
GO
