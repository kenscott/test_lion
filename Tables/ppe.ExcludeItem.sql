CREATE TABLE [ppe].[ExcludeItem]
(
[ExcludeItemKey] [int] NOT NULL IDENTITY(1, 1),
[ItemKey] [int] NOT NULL,
[CreatedByWebUserKey] [int] NULL,
[CreationDate] [datetime] NULL CONSTRAINT [DF__ExcludeIt__Creat__2EF19B98] DEFAULT (getdate())
) ON [PRIMARY]
GO
