CREATE TABLE [lion].[NewOrg]
(
[BranchKey] [int] NOT NULL IDENTITY(1, 1),
[BranchCode] [dbo].[Description_Small_type] NOT NULL,
[BranchName] [dbo].[UDVarchar_type] NOT NULL,
[NetworkCode] [dbo].[Description_Small_type] NOT NULL,
[NetworkName] [dbo].[UDVarchar_type] NOT NULL,
[AreaCode] [dbo].[Description_Small_type] NOT NULL,
[RegionCode] [dbo].[Description_Small_type] NOT NULL,
[RegionName] [dbo].[UDVarchar_type] NOT NULL,
[LNM] [dbo].[UDVarchar_type] NULL,
[RTD] [dbo].[UDVarchar_type] NULL
) ON [PRIMARY]
GO
ALTER TABLE [lion].[NewOrg] ADD CONSTRAINT [PK_NewOrg_1] PRIMARY KEY CLUSTERED  ([BranchKey]) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_BranchCode] ON [lion].[NewOrg] ([BranchCode]) ON [PRIMARY]
GO
