CREATE TABLE [dbo].[PlaybookMarginErosionProposal]
(
[PlaybookMarginErosionProposalKey] [dbo].[Key_Normal_type] NOT NULL IDENTITY(1, 1),
[PlaybookDataPointGroupKey] [dbo].[Key_Normal_type] NOT NULL,
[AccountKey] [dbo].[Key_Normal_type] NOT NULL,
[ItemKey] [dbo].[Key_Normal_type] NOT NULL,
[InvoiceLineGroup1Key] [dbo].[Key_Normal_type] NOT NULL,
[AverageGrossProfitPercent] [dbo].[Percent_Type] NULL,
[SustainedGrossProfitPercent] [dbo].[Percent_Type] NULL,
[ItemPrice] [dbo].[Money_Normal_Type] NULL,
[ItemCost] [dbo].[Money_Normal_Type] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[PlaybookMarginErosionProposal] ADD CONSTRAINT [PK_MarginErosion] PRIMARY KEY CLUSTERED  ([PlaybookMarginErosionProposalKey]) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [UI_PlaybookMarginErosionProposal_1] ON [dbo].[PlaybookMarginErosionProposal] ([PlaybookDataPointGroupKey], [AccountKey], [ItemKey], [InvoiceLineGroup1Key]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[PlaybookMarginErosionProposal] ADD CONSTRAINT [FK_PlaybookMarginErosionOptimalPrice_2] FOREIGN KEY ([ItemKey]) REFERENCES [dbo].[DimItem] ([ItemKey])
GO
ALTER TABLE [dbo].[PlaybookMarginErosionProposal] ADD CONSTRAINT [FK_PlaybookMarginErosionOptimalPrice_3] FOREIGN KEY ([AccountKey]) REFERENCES [dbo].[DimAccount] ([AccountKey])
GO
ALTER TABLE [dbo].[PlaybookMarginErosionProposal] ADD CONSTRAINT [FK_PlaybookMarginErosionOptimalPrice_4] FOREIGN KEY ([InvoiceLineGroup1Key]) REFERENCES [dbo].[DimInvoiceLineGroup1] ([InvoiceLineGroup1Key])
GO
ALTER TABLE [dbo].[PlaybookMarginErosionProposal] ADD CONSTRAINT [FK_PlaybookMarginErosionProposal_1] FOREIGN KEY ([PlaybookDataPointGroupKey]) REFERENCES [dbo].[PlaybookDataPointGroup] ([PlaybookDataPointGroupKey])
GO
