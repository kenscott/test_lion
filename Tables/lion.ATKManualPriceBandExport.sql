CREATE TABLE [lion].[ATKManualPriceBandExport]
(
[ManualPriceBandExportKey] [dbo].[Key_Normal_type] NOT NULL IDENTITY(1, 1),
[ItemPyramidCode] [dbo].[UDVarchar_type] NULL,
[LLSPGCode] [dbo].[UDVarchar_type] NULL,
[ProductCode] [dbo].[UDVarchar_type] NULL,
[PriceApproach] [dbo].[UDVarchar_type] NULL,
[ContractClaimsIndicator] [dbo].[UDVarchar_type] NULL,
[Region] [dbo].[UDVarchar_type] NULL,
[Area] [dbo].[UDVarchar_type] NULL,
[Network] [dbo].[UDVarchar_type] NULL,
[Branch] [dbo].[UDVarchar_type] NULL,
[CurrentFloorGPP] [dbo].[Percent_Type] NULL,
[CurrentTargetGPP] [dbo].[Percent_Type] NULL,
[CurrentStretchGPP] [dbo].[Percent_Type] NULL,
[CurrentFloorDiscount] [dbo].[Percent_Type] NULL,
[CurrentTargetDiscount] [dbo].[Percent_Type] NULL,
[CurrentStretchDiscount] [dbo].[Percent_Type] NULL,
[CurrentFloorPrice] [dbo].[Money_Normal_Type] NULL,
[CurrentTargetPrice] [dbo].[Money_Normal_Type] NULL,
[CurrentStretchPrice] [dbo].[Money_Normal_Type] NULL,
[ProductCost] [dbo].[Money_Normal_Type] NULL,
[TradePrice] [dbo].[Money_Normal_Type] NULL
) ON [PRIMARY]
GO
ALTER TABLE [lion].[ATKManualPriceBandExport] ADD CONSTRAINT [PK_ATKManualPriceBandExport] PRIMARY KEY CLUSTERED  ([ManualPriceBandExportKey]) ON [PRIMARY]
GO
