CREATE TABLE [dbo].[ODSStatus]
(
[ODSStatusKey] [dbo].[Key_Normal_type] NOT NULL IDENTITY(1, 1),
[StatusName] [dbo].[Description_Small_type] NULL,
[StatusDescription] [dbo].[Description_Normal_type] NULL,
[StatusValue] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MaxInvoiceDate] [date] NULL,
[ApplicationLogoutDateTime] [datetime] NOT NULL CONSTRAINT [DF_ODSStatus_ApplicationLogoutDateTime] DEFAULT ('2050-01-01 00:00:00.000')
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ODSStatus] ADD CONSTRAINT [PK_ODSStatus] PRIMARY KEY CLUSTERED  ([ODSStatusKey]) ON [PRIMARY]
GO
