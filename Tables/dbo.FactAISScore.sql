CREATE TABLE [dbo].[FactAISScore]
(
[FactAISScoreKey] [int] NOT NULL IDENTITY(1, 1),
[AccountKey] [dbo].[Key_Normal_type] NOT NULL,
[ItemKey] [dbo].[Key_Normal_type] NOT NULL,
[InvoiceLineGroup1Key] [dbo].[Key_Normal_type] NOT NULL CONSTRAINT [DF_FactAISScore_InvoiceLineGroup1Key] DEFAULT ((4)),
[TotalSales] [dbo].[Money_Normal_Type] NULL,
[TotalCost] [dbo].[Money_Normal_Type] NULL,
[AverageOrderSize] [dbo].[Money_Normal_Type] NULL,
[GTM] [dbo].[Money_Normal_Type] NULL,
[Score] [dbo].[Money_Normal_Type] NULL,
[FloorImpact] [dbo].[Money_Normal_Type] NULL,
[TargetImpact] [dbo].[Money_Normal_Type] NULL,
[FloorImpactPercent] [dbo].[Money_Normal_Type] NULL,
[TargetImpactPercent] [dbo].[Money_Normal_Type] NULL,
[Floor85Impact] [dbo].[Money_Normal_Type] NULL,
[FloorGPP] [dbo].[Money_Normal_Type] NULL,
[TargetGPP] [dbo].[Money_Normal_Type] NULL,
[StretchGPP] [dbo].[Money_Normal_Type] NULL,
[Rank] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[FactAISScore] ADD CONSTRAINT [PK_FactAISScore] PRIMARY KEY CLUSTERED  ([FactAISScoreKey]) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [I_FactAISScore_2] ON [dbo].[FactAISScore] ([AccountKey], [ItemKey], [InvoiceLineGroup1Key], [Score]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[FactAISScore] ADD CONSTRAINT [FK_FactAISScore_AccountKey] FOREIGN KEY ([AccountKey]) REFERENCES [dbo].[DimAccount] ([AccountKey])
GO
ALTER TABLE [dbo].[FactAISScore] ADD CONSTRAINT [FK_FactAISScore_ItemKey] FOREIGN KEY ([ItemKey]) REFERENCES [dbo].[DimItem] ([ItemKey])
GO
