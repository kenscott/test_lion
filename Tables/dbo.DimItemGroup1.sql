CREATE TABLE [dbo].[DimItemGroup1]
(
[ItemGroup1Key] [dbo].[Key_Normal_type] NOT NULL IDENTITY(1, 1),
[IG1Level1] [dbo].[Description_Small_type] NOT NULL,
[IG1Level2] [dbo].[Description_Small_type] NOT NULL CONSTRAINT [DF_DimItemGroup1_Level2] DEFAULT ('None'),
[IG1Level3] [dbo].[Description_Small_type] NOT NULL CONSTRAINT [DF_DimItemGroup1_Level3] DEFAULT ('None'),
[IG1Level4] [dbo].[Description_Small_type] NOT NULL CONSTRAINT [DF_DimItemGroup1_Level4] DEFAULT ('None'),
[IG1Level5] [dbo].[Description_Small_type] NOT NULL CONSTRAINT [DF_DimItemGroup1_Level5] DEFAULT ('None'),
[IG1Level1UDVarchar1] [dbo].[UDVarchar_type] NULL,
[IG1Level1UDVarchar2] [dbo].[UDVarchar_type] NULL,
[IG1Level1UDVarchar3] [dbo].[UDVarchar_type] NULL,
[IG1Level2UDVarchar1] [dbo].[UDVarchar_type] NULL,
[IG1Level2UDVarchar2] [dbo].[UDVarchar_type] NULL,
[IG1Level2UDVarchar3] [dbo].[UDVarchar_type] NULL,
[IG1Level3UDVarchar1] [dbo].[UDVarchar_type] NULL,
[IG1Level3UDVarchar2] [dbo].[UDVarchar_type] NULL,
[IG1Level3UDVarchar3] [dbo].[UDVarchar_type] NULL,
[IG1Level4UDVarchar1] [dbo].[UDVarchar_type] NULL,
[IG1Level4UDVarchar2] [dbo].[UDVarchar_type] NULL,
[IG1Level4UDVarchar3] [dbo].[UDVarchar_type] NULL,
[IG1Level5UDVarchar1] [dbo].[UDVarchar_type] NULL,
[IG1Level5UDVarchar2] [dbo].[UDVarchar_type] NULL,
[IG1Level5UDVarchar3] [dbo].[UDVarchar_type] NULL,
[IG1Level1UDDecimal1] [dbo].[UDDecimal_type] NULL,
[IG1Level1UDDecimal2] [dbo].[UDDecimal_type] NULL,
[IG1Level2UDDecimal1] [dbo].[UDDecimal_type] NULL,
[IG1Level2UDDecimal2] [dbo].[UDDecimal_type] NULL,
[IG1Level3UDDecimal1] [dbo].[UDDecimal_type] NULL,
[IG1Level3UDDecimal2] [dbo].[UDDecimal_type] NULL,
[IG1Level4UDDecimal1] [dbo].[UDDecimal_type] NULL,
[IG1Level4UDDecimal2] [dbo].[UDDecimal_type] NULL,
[IG1Level5UDDecimal1] [dbo].[UDDecimal_type] NULL,
[IG1Level5UDDecimal2] [dbo].[UDDecimal_type] NULL,
[CreationDayKey] [dbo].[DateKey_type] NOT NULL,
[ModificationDayKey] [dbo].[DateKey_type] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[DimItemGroup1] ADD CONSTRAINT [PK_DimItemGroup1] PRIMARY KEY CLUSTERED  ([ItemGroup1Key]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [I_DimItemGroup1_1] ON [dbo].[DimItemGroup1] ([IG1Level1]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [I_DimItemGroup1_UD32] ON [dbo].[DimItemGroup1] ([IG1Level1UDDecimal1]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [I_DimItemGroup1_UD33] ON [dbo].[DimItemGroup1] ([IG1Level1UDDecimal2]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [I_DimItemGroup1_UD7] ON [dbo].[DimItemGroup1] ([IG1Level1UDVarchar1]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [I_DimItemGroup1_UD8] ON [dbo].[DimItemGroup1] ([IG1Level1UDVarchar2]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [I_DimItemGroup1_UD9] ON [dbo].[DimItemGroup1] ([IG1Level1UDVarchar3]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [I_DimItemGroup1_2] ON [dbo].[DimItemGroup1] ([IG1Level2]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [I_DimItemGroup1_UD34] ON [dbo].[DimItemGroup1] ([IG1Level2UDDecimal1]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [I_DimItemGroup1_UD35] ON [dbo].[DimItemGroup1] ([IG1Level2UDDecimal2]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [I_DimItemGroup1_UD12] ON [dbo].[DimItemGroup1] ([IG1Level2UDVarchar1]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [I_DimItemGroup1_UD13] ON [dbo].[DimItemGroup1] ([IG1Level2UDVarchar2]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [I_DimItemGroup1_UD14] ON [dbo].[DimItemGroup1] ([IG1Level2UDVarchar3]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [I_DimItemGroup1_3] ON [dbo].[DimItemGroup1] ([IG1Level3]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [I_DimItemGroup1_UD36] ON [dbo].[DimItemGroup1] ([IG1Level3UDDecimal1]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [I_DimItemGroup1_UD37] ON [dbo].[DimItemGroup1] ([IG1Level3UDDecimal2]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [I_DimItemGroup1_UD17] ON [dbo].[DimItemGroup1] ([IG1Level3UDVarchar1]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [I_DimItemGroup1_UD18] ON [dbo].[DimItemGroup1] ([IG1Level3UDVarchar2]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [I_DimItemGroup1_UD19] ON [dbo].[DimItemGroup1] ([IG1Level3UDVarchar3]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [I_DimItemGroup1_4] ON [dbo].[DimItemGroup1] ([IG1Level4]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [I_DimItemGroup1_UD38] ON [dbo].[DimItemGroup1] ([IG1Level4UDDecimal1]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [I_DimItemGroup1_UD39] ON [dbo].[DimItemGroup1] ([IG1Level4UDDecimal2]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [I_DimItemGroup1_UD22] ON [dbo].[DimItemGroup1] ([IG1Level4UDVarchar1]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [I_DimItemGroup1_UD23] ON [dbo].[DimItemGroup1] ([IG1Level4UDVarchar2]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [I_DimItemGroup1_UD24] ON [dbo].[DimItemGroup1] ([IG1Level4UDVarchar3]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [I_DimItemGroup1_5] ON [dbo].[DimItemGroup1] ([IG1Level5]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [I_DimItemGroup1_UD40] ON [dbo].[DimItemGroup1] ([IG1Level5UDDecimal1]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [I_DimItemGroup1_UD41] ON [dbo].[DimItemGroup1] ([IG1Level5UDDecimal2]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [I_DimItemGroup1_UD27] ON [dbo].[DimItemGroup1] ([IG1Level5UDVarchar1]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [I_DimItemGroup1_UD28] ON [dbo].[DimItemGroup1] ([IG1Level5UDVarchar2]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [I_DimItemGroup1_UD29] ON [dbo].[DimItemGroup1] ([IG1Level5UDVarchar3]) ON [PRIMARY]
GO
