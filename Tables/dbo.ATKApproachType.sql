CREATE TABLE [dbo].[ATKApproachType]
(
[ApproachTypeKey] [dbo].[Key_Small_type] NOT NULL IDENTITY(1, 1),
[ApproachTypeDesc] [dbo].[Description_Normal_type] NOT NULL,
[Version] [dbo].[Int_Type] NOT NULL CONSTRAINT [DF__ATKApproa__Versi__3B6D4C6D] DEFAULT ((0)),
[CreationDate] [dbo].[DateTime_Type] NOT NULL CONSTRAINT [DF__ATKApproa__Creat__562142A9] DEFAULT (getdate()),
[ModificationDate] [dbo].[DateTime_Type] NOT NULL CONSTRAINT [DF__ATKApproa__Modif__70D538E5] DEFAULT (getdate()),
[CreationUser] [dbo].[Description_Normal_type] NULL,
[ModificationUser] [dbo].[Description_Normal_type] NULL,
[ModificationUserAddress] [dbo].[Description_Normal_type] NULL,
[CreationUserAddress] [dbo].[Description_Normal_type] NULL,
[ApproachPriority] [dbo].[Int_255_type] NOT NULL CONSTRAINT [DF_ATKApproachType_ApproachPriority] DEFAULT ((1))
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ATKApproachType] ADD CONSTRAINT [PK_ATKApproachType] PRIMARY KEY CLUSTERED  ([ApproachTypeKey]) ON [PRIMARY]
GO
