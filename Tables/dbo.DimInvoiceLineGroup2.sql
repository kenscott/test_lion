CREATE TABLE [dbo].[DimInvoiceLineGroup2]
(
[InvoiceLineGroup2Key] [dbo].[Key_Normal_type] NOT NULL IDENTITY(1, 1),
[InvoiceLineGroup2UDVarchar1] [dbo].[UDVarchar_type] NOT NULL,
[InvoiceLineGroup2UDVarchar2] [dbo].[UDVarchar_type] NOT NULL,
[InvoiceLineGroup2UDVarchar3] [dbo].[UDVarchar_type] NOT NULL,
[InvoiceLineGroup2UDVarchar4] [dbo].[UDVarchar_type] NOT NULL,
[InvoiceLineGroup2UDVarchar5] [dbo].[UDVarchar_type] NOT NULL,
[CreationDayKey] [dbo].[DateKey_type] NOT NULL,
[ModificationDayKey] [dbo].[DateKey_type] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[DimInvoiceLineGroup2] ADD CONSTRAINT [PK_DimInvoiceLineGroup2] PRIMARY KEY CLUSTERED  ([InvoiceLineGroup2Key]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [I_DimInvoiceLineGroup2_1] ON [dbo].[DimInvoiceLineGroup2] ([InvoiceLineGroup2UDVarchar1]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [I_DimInvoiceLineGroup2_UD2] ON [dbo].[DimInvoiceLineGroup2] ([InvoiceLineGroup2UDVarchar1]) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [I_DimInvoiceLineGroup2_6] ON [dbo].[DimInvoiceLineGroup2] ([InvoiceLineGroup2UDVarchar1]) INCLUDE ([InvoiceLineGroup2UDVarchar2]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [I_DimInvoiceLineGroup2_2] ON [dbo].[DimInvoiceLineGroup2] ([InvoiceLineGroup2UDVarchar2]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [I_DimInvoiceLineGroup2_UD3] ON [dbo].[DimInvoiceLineGroup2] ([InvoiceLineGroup2UDVarchar2]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [I_DimInvoiceLineGroup2_3] ON [dbo].[DimInvoiceLineGroup2] ([InvoiceLineGroup2UDVarchar3]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [I_DimInvoiceLineGroup2_UD4] ON [dbo].[DimInvoiceLineGroup2] ([InvoiceLineGroup2UDVarchar3]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [I_DimInvoiceLineGroup2_4] ON [dbo].[DimInvoiceLineGroup2] ([InvoiceLineGroup2UDVarchar4]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [I_DimInvoiceLineGroup2_UD5] ON [dbo].[DimInvoiceLineGroup2] ([InvoiceLineGroup2UDVarchar4]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [I_DimInvoiceLineGroup2_5] ON [dbo].[DimInvoiceLineGroup2] ([InvoiceLineGroup2UDVarchar5]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [I_DimInvoiceLineGroup2_UD6] ON [dbo].[DimInvoiceLineGroup2] ([InvoiceLineGroup2UDVarchar5]) ON [PRIMARY]
GO
