CREATE TABLE [dbo].[ATKProposalRecommendationPlaybook]
(
[ProposalRecommendationPlaybookKey] [dbo].[Key_Normal_type] NOT NULL IDENTITY(1, 1),
[MinMonthsBetweenProposals] [dbo].[Int_255_type] NOT NULL,
[Version] [dbo].[Int_Type] NOT NULL CONSTRAINT [DF__ATKPropos__Versi__5250B1C5] DEFAULT ((0)),
[CreationDate] [dbo].[DateTime_Type] NOT NULL CONSTRAINT [DF__ATKPropos__Creat__6D04A801] DEFAULT (getdate()),
[ModificationDate] [dbo].[DateTime_Type] NOT NULL CONSTRAINT [DF__ATKPropos__Modif__07B89E3D] DEFAULT (getdate()),
[CreationUser] [dbo].[Description_Normal_type] NULL,
[ModificationUser] [dbo].[Description_Normal_type] NULL,
[ModificationUserAddress] [dbo].[Description_Normal_type] NULL,
[CreationUserAddress] [dbo].[Description_Normal_type] NULL,
[WhereClause1] [dbo].[Description_Enormous_type] NULL,
[JoinClause1] [dbo].[Description_Enormous_type] NULL,
[ForecastType] [dbo].[Description_Small_type] NOT NULL CONSTRAINT [DF__ATKPropos__Forec__6DB1157C] DEFAULT ('12 Month Annualized'),
[DynamicDeleteWhere1] [dbo].[Description_Huge_type] NOT NULL CONSTRAINT [DF__ATKPropos__Dynam__31C713FE] DEFAULT (''),
[DynamicDeleteJoin1] [dbo].[Description_Huge_type] NOT NULL CONSTRAINT [DF__ATKPropos__Dynam__3597A4E2] DEFAULT (''),
[DynamicDeleteGroupBy1] [dbo].[Description_Huge_type] NOT NULL CONSTRAINT [DF__ATKPropos__Dynam__368BC91B] DEFAULT (''),
[DynamicDeleteHaving1] [dbo].[Description_Huge_type] NOT NULL CONSTRAINT [DF__ATKPropos__Dynam__377FED54] DEFAULT ('')
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ATKProposalRecommendationPlaybook] ADD CONSTRAINT [PK_ATKProposalRecommendationPlaybook] PRIMARY KEY CLUSTERED  ([ProposalRecommendationPlaybookKey]) ON [PRIMARY]
GO
