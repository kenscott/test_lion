CREATE TABLE [dbo].[ProposalReasonCode]
(
[ReasonCodeKey] [dbo].[Key_Small_type] NOT NULL IDENTITY(1, 1),
[ReasonCodeDescription] [dbo].[Description_Big_type] NOT NULL,
[ApproachTypeKey] [dbo].[Key_Small_type] NOT NULL,
[DaysToSuppress] [dbo].[Int_Type] NOT NULL,
[ReasonCodeType] [dbo].[Description_Small_type] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ProposalReasonCode] ADD CONSTRAINT [PK_ATKReasonCode] PRIMARY KEY CLUSTERED  ([ReasonCodeKey]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ProposalReasonCode] ADD CONSTRAINT [FK_ATKReasonCode_1] FOREIGN KEY ([ApproachTypeKey]) REFERENCES [dbo].[ATKApproachType] ([ApproachTypeKey])
GO
