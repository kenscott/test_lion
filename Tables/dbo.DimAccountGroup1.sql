CREATE TABLE [dbo].[DimAccountGroup1]
(
[AccountGroup1Key] [dbo].[Key_Normal_type] NOT NULL IDENTITY(1, 1),
[AG1Level1] [dbo].[Description_Small_type] NOT NULL,
[AG1Level2] [dbo].[Description_Small_type] NOT NULL CONSTRAINT [DF_DimAccountGroup1_Level2] DEFAULT ('None'),
[AG1Level3] [dbo].[Description_Small_type] NOT NULL CONSTRAINT [DF_DimAccountGroup1_Level3] DEFAULT ('None'),
[AG1Level4] [dbo].[Description_Small_type] NOT NULL CONSTRAINT [DF_DimAccountGroup1_Level4] DEFAULT ('None'),
[AG1Level5] [dbo].[Description_Small_type] NOT NULL CONSTRAINT [DF_DimAccountGroup1_Level5] DEFAULT ('None'),
[AG1Level1UDVarchar1] [dbo].[UDVarchar_type] NULL,
[AG1Level1UDVarchar2] [dbo].[UDVarchar_type] NULL,
[AG1Level1UDVarchar3] [dbo].[UDVarchar_type] NULL,
[AG1Level1UDVarchar4] [dbo].[UDVarchar_type] NULL,
[AG1Level1UDVarchar5] [dbo].[UDVarchar_type] NULL,
[AG1Level1UDVarchar6] [dbo].[UDVarchar_type] NULL,
[AG1Level1UDVarchar7] [dbo].[UDVarchar_type] NULL,
[AG1Level1UDVarchar8] [dbo].[UDVarchar_type] NULL,
[AG1Level1UDVarchar9] [dbo].[UDVarchar_type] NULL,
[AG1Level1UDVarchar10] [dbo].[UDVarchar_type] NULL,
[AG1Level1UDVarchar11] [dbo].[UDVarchar_type] NULL,
[AG1Level1UDVarchar12] [dbo].[UDVarchar_type] NULL,
[AG1Level1UDVarchar13] [dbo].[UDVarchar_type] NULL,
[AG1Level1UDVarchar14] [dbo].[UDVarchar_type] NULL,
[AG1Level1UDVarchar15] [dbo].[UDVarchar_type] NULL,
[AG1Level1UDVarchar16] [dbo].[UDVarchar_type] NULL,
[AG1Level1UDVarchar17] [dbo].[UDVarchar_type] NULL,
[AG1Level1UDVarchar18] [dbo].[UDVarchar_type] NULL,
[AG1Level1UDVarchar19] [dbo].[UDVarchar_type] NULL,
[AG1Level1UDVarchar20] [dbo].[UDVarchar_type] NULL,
[AG1Level1UDVarchar21] [dbo].[UDVarchar_type] NULL,
[AG1Level1UDVarchar22] [dbo].[UDVarchar_type] NULL,
[AG1Level1UDVarchar23] [dbo].[UDVarchar_type] NULL,
[AG1Level1UDVarchar24] [dbo].[UDVarchar_type] NULL,
[AG1Level1UDVarchar25] [dbo].[UDVarchar_type] NULL CONSTRAINT [DF_DimAccountGroup1_AG1Level1UDVarchar25] DEFAULT (N'N'),
[AG1Level1UDVarchar26] [dbo].[UDVarchar_type] NULL,
[AG1Level2UDVarchar1] [dbo].[UDVarchar_type] NULL,
[AG1Level2UDVarchar2] [dbo].[UDVarchar_type] NULL,
[AG1Level2UDVarchar3] [dbo].[UDVarchar_type] NULL,
[AG1Level3UDVarchar1] [dbo].[UDVarchar_type] NULL,
[AG1Level3UDVarchar2] [dbo].[UDVarchar_type] NULL,
[AG1Level3UDVarchar3] [dbo].[UDVarchar_type] NULL,
[AG1Level4UDVarchar1] [dbo].[UDVarchar_type] NULL,
[AG1Level4UDVarchar2] [dbo].[UDVarchar_type] NULL,
[AG1Level4UDVarchar3] [dbo].[UDVarchar_type] NULL,
[AG1Level5UDVarchar1] [dbo].[UDVarchar_type] NULL,
[AG1Level5UDVarchar2] [dbo].[UDVarchar_type] NULL,
[AG1Level5UDVarchar3] [dbo].[UDVarchar_type] NULL,
[CreationDayKey] [dbo].[DateKey_type] NOT NULL,
[ModificationDayKey] [dbo].[DateKey_type] NOT NULL,
[BrandKey] [dbo].[Key_Small_type] NULL,
[NetworkKey] [dbo].[Key_Small_type] NULL,
[AreaKey] [dbo].[Key_Small_type] NULL,
[RegionKey] [dbo].[Key_Small_type] NULL,
[Inactive] [bit] NOT NULL CONSTRAINT [DF_DimAccountGroup1_Inactive] DEFAULT ((0)),
[Converted] [bit] NOT NULL CONSTRAINT [DF_DimAccountGroup1_ConvertedBranch] DEFAULT ((0)),
[AG1Level1PersonKey] [dbo].[Key_Normal_type] NOT NULL CONSTRAINT [DF_DimAccountGroup1_AG1Level1PersonKey] DEFAULT ((1))
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[DimAccountGroup1] ADD CONSTRAINT [PK_DimMiscGroup1] PRIMARY KEY CLUSTERED  ([AccountGroup1Key]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [I_DimAccountGroup1_8] ON [dbo].[DimAccountGroup1] ([AccountGroup1Key]) INCLUDE ([AG1Level1], [AG1Level1UDVarchar1], [AG1Level1UDVarchar2], [AG1Level1UDVarchar6], [AG1Level1UDVarchar7], [AG1Level1UDVarchar8], [AG1Level2], [AG1Level3], [AG1Level4]) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [I_DimAccountGroup1_6] ON [dbo].[DimAccountGroup1] ([AccountGroup1Key]) INCLUDE ([AG1Level1], [AG1Level1UDVarchar2], [AG1Level1UDVarchar20], [AG1Level1UDVarchar6]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [I_DimAccountGroup1_9] ON [dbo].[DimAccountGroup1] ([AccountGroup1Key]) INCLUDE ([AG1Level1UDVarchar14]) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [I_DimAccountGroup1_7] ON [dbo].[DimAccountGroup1] ([AccountGroup1Key]) INCLUDE ([AG1Level1UDVarchar2], [AG1Level1UDVarchar20]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [I_DimAccountGroup1_1] ON [dbo].[DimAccountGroup1] ([AG1Level1]) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [UI_AG1Level1] ON [dbo].[DimAccountGroup1] ([AG1Level1]) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [UI_DimMiscGroup1_1] ON [dbo].[DimAccountGroup1] ([AG1Level1], [AG1Level2], [AG1Level3], [AG1Level4], [AG1Level5]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [I_DimAccountGroup1_UD7] ON [dbo].[DimAccountGroup1] ([AG1Level1UDVarchar1]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [I_DimAccountGroup1_UD8] ON [dbo].[DimAccountGroup1] ([AG1Level1UDVarchar2]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [I_DimAccountGroup1_UD9] ON [dbo].[DimAccountGroup1] ([AG1Level1UDVarchar3]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [I_DimAccountGroup1_2] ON [dbo].[DimAccountGroup1] ([AG1Level2]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [I_DimAccountGroup1_UD10] ON [dbo].[DimAccountGroup1] ([AG1Level2UDVarchar1]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [I_DimAccountGroup1_UD11] ON [dbo].[DimAccountGroup1] ([AG1Level2UDVarchar2]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [I_DimAccountGroup1_UD12] ON [dbo].[DimAccountGroup1] ([AG1Level2UDVarchar3]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [I_DimAccountGroup1_3] ON [dbo].[DimAccountGroup1] ([AG1Level3]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [I_DimAccountGroup1_UD13] ON [dbo].[DimAccountGroup1] ([AG1Level3UDVarchar1]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [I_DimAccountGroup1_UD14] ON [dbo].[DimAccountGroup1] ([AG1Level3UDVarchar2]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [I_DimAccountGroup1_UD15] ON [dbo].[DimAccountGroup1] ([AG1Level3UDVarchar3]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [I_DimAccountGroup1_4] ON [dbo].[DimAccountGroup1] ([AG1Level4]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [I_DimAccountGroup1_UD16] ON [dbo].[DimAccountGroup1] ([AG1Level4UDVarchar1]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [I_DimAccountGroup1_UD17] ON [dbo].[DimAccountGroup1] ([AG1Level4UDVarchar2]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [I_DimAccountGroup1_UD18] ON [dbo].[DimAccountGroup1] ([AG1Level4UDVarchar3]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [I_DimAccountGroup1_5] ON [dbo].[DimAccountGroup1] ([AG1Level5]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [I_DimAccountGroup1_UD19] ON [dbo].[DimAccountGroup1] ([AG1Level5UDVarchar1]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [I_DimAccountGroup1_UD20] ON [dbo].[DimAccountGroup1] ([AG1Level5UDVarchar2]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [I_DimAccountGroup1_UD21] ON [dbo].[DimAccountGroup1] ([AG1Level5UDVarchar3]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[DimAccountGroup1] ADD CONSTRAINT [FK_DimAccountGroup1_1] FOREIGN KEY ([NetworkKey]) REFERENCES [dbo].[DimNetwork] ([NetworkKey])
GO
ALTER TABLE [dbo].[DimAccountGroup1] ADD CONSTRAINT [FK_DimAccountGroup1_2] FOREIGN KEY ([AreaKey]) REFERENCES [dbo].[DimArea] ([AreaKey])
GO
ALTER TABLE [dbo].[DimAccountGroup1] ADD CONSTRAINT [FK_DimAccountGroup1_3] FOREIGN KEY ([RegionKey]) REFERENCES [dbo].[DimRegion] ([RegionKey])
GO
ALTER TABLE [dbo].[DimAccountGroup1] ADD CONSTRAINT [FK_DimAccountGroup1_4] FOREIGN KEY ([BrandKey]) REFERENCES [dbo].[DimBrand] ([BrandKey])
GO
ALTER TABLE [dbo].[DimAccountGroup1] ADD CONSTRAINT [FK_DimAccountGroup1_5] FOREIGN KEY ([AG1Level1PersonKey]) REFERENCES [dbo].[DimPerson] ([DimPersonKey])
GO
