CREATE TABLE [dbo].[WebUserAccountManager]
(
[WebUserAccountManagerKey] [dbo].[Key_Normal_type] NOT NULL IDENTITY(1, 1),
[WebUserKey] [dbo].[Key_Normal_type] NOT NULL,
[AccountManagerKey] [dbo].[Key_Normal_type] NOT NULL,
[AccessType] [dbo].[Description_Small_type] NOT NULL,
[Version] [dbo].[Int_Type] NOT NULL CONSTRAINT [DF__WebUserAc__Versi__706102F0] DEFAULT ((0)),
[CreationDate] [dbo].[DateTime_Type] NOT NULL CONSTRAINT [DF__WebUserAc__Creat__71552729] DEFAULT (getdate()),
[ModificationDate] [dbo].[DateTime_Type] NOT NULL CONSTRAINT [DF__WebUserAc__Modif__72494B62] DEFAULT (getdate()),
[CreationUser] [dbo].[Description_Normal_type] NULL,
[ModificationUser] [dbo].[Description_Normal_type] NULL,
[ModificationUserAddress] [dbo].[Description_Normal_type] NULL,
[CreationUserAddress] [dbo].[Description_Normal_type] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[WebUserAccountManager] ADD CONSTRAINT [PK_WebUserAccountManager] PRIMARY KEY CLUSTERED  ([WebUserAccountManagerKey]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [I_WebUserAccountManager_1] ON [dbo].[WebUserAccountManager] ([AccountManagerKey]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [I_WebUserAccountManager_2] ON [dbo].[WebUserAccountManager] ([WebUserKey]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[WebUserAccountManager] ADD CONSTRAINT [FK_WebUserAccountManager_1] FOREIGN KEY ([AccountManagerKey]) REFERENCES [dbo].[DimAccountManager] ([AccountManagerKey])
GO
ALTER TABLE [dbo].[WebUserAccountManager] ADD CONSTRAINT [FK_WebUserAccountManager_2] FOREIGN KEY ([WebUserKey]) REFERENCES [dbo].[WebUser] ([WebUserKey])
GO
