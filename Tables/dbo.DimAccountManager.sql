CREATE TABLE [dbo].[DimAccountManager]
(
[AccountManagerKey] [dbo].[Key_Normal_type] NOT NULL IDENTITY(1, 1),
[DimPersonKey] [dbo].[Key_Normal_type] NULL,
[AccountManagerCode] [dbo].[Description_Small_type] NOT NULL,
[CreationDayKey] [dbo].[Key_Small_type] NOT NULL,
[ModificationDayKey] [dbo].[Key_Small_type] NOT NULL,
[PrimaryAccountGroup1Key] [dbo].[Key_Normal_type] NULL CONSTRAINT [DF_DimAccountManager_PrimaryAccountGroup1Key] DEFAULT ((1)),
[SupervisingAccountManagerKey] [dbo].[Key_Normal_type] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[DimAccountManager] ADD CONSTRAINT [PK_DimAccountManager] PRIMARY KEY CLUSTERED  ([AccountManagerKey]) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [I_DimAccountManager_2] ON [dbo].[DimAccountManager] ([AccountManagerCode]) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [I_DimAccountManager_3] ON [dbo].[DimAccountManager] ([DimPersonKey], [AccountManagerKey]) ON [PRIMARY]
GO
CREATE STATISTICS [_dta_stat_722101613_1_2] ON [dbo].[DimAccountManager] ([AccountManagerKey], [DimPersonKey])
GO
