CREATE TABLE [dbo].[PlaybookProjectFullScopeSummary]
(
[ProjectKey] [dbo].[Key_Normal_type] NOT NULL,
[AccountManagerKey] [dbo].[Key_Normal_type] NOT NULL,
[AccountKey] [dbo].[Key_Normal_type] NOT NULL,
[ItemKey] [dbo].[Key_Normal_type] NOT NULL,
[InvoiceLineGroup1Key] [dbo].[Key_Normal_type] NOT NULL,
[TotalActual12MonthPrice] [dbo].[Money_Normal_Type] NOT NULL,
[TotalActual12MonthCost] [dbo].[Money_Normal_Type] NOT NULL,
[TotalActual12MonthQuantity] [dbo].[Quantity_Normal_type] NOT NULL,
[TotalInvoiceLineCount] [dbo].[Quantity_Normal_type] NOT NULL,
[LastInvoiceLineGroup2Key] [dbo].[Key_Normal_type] NOT NULL,
[LastItemPrice] [dbo].[Money_Normal_Type] NULL,
[LastItemCost] [dbo].[Money_Normal_Type] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[PlaybookProjectFullScopeSummary] ADD CONSTRAINT [PK_PlaybookProjectFullScopeSummary] PRIMARY KEY NONCLUSTERED  ([ProjectKey], [AccountKey], [ItemKey], [InvoiceLineGroup1Key]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [I_PlaybookProjectFullScopeSummary_1] ON [dbo].[PlaybookProjectFullScopeSummary] ([ProjectKey], [AccountManagerKey], [AccountKey]) ON [PRIMARY]
GO
CREATE CLUSTERED INDEX [I_PlaybookProjectFullScopeSummary_2] ON [dbo].[PlaybookProjectFullScopeSummary] ([ProjectKey], [AccountManagerKey], [ItemKey]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[PlaybookProjectFullScopeSummary] ADD CONSTRAINT [FK_PlaybookProjectFullScopeSummary_1] FOREIGN KEY ([ProjectKey]) REFERENCES [dbo].[PlaybookProject] ([ProjectKey])
GO
ALTER TABLE [dbo].[PlaybookProjectFullScopeSummary] ADD CONSTRAINT [FK_PlaybookProjectFullScopeSummary_2] FOREIGN KEY ([AccountManagerKey]) REFERENCES [dbo].[DimAccountManager] ([AccountManagerKey])
GO
ALTER TABLE [dbo].[PlaybookProjectFullScopeSummary] ADD CONSTRAINT [FK_PlaybookProjectFullScopeSummary_3] FOREIGN KEY ([AccountKey]) REFERENCES [dbo].[DimAccount] ([AccountKey])
GO
ALTER TABLE [dbo].[PlaybookProjectFullScopeSummary] ADD CONSTRAINT [FK_PlaybookProjectFullScopeSummary_4] FOREIGN KEY ([ItemKey]) REFERENCES [dbo].[DimItem] ([ItemKey])
GO
ALTER TABLE [dbo].[PlaybookProjectFullScopeSummary] ADD CONSTRAINT [FK_PlaybookProjectFullScopeSummary_5] FOREIGN KEY ([InvoiceLineGroup1Key]) REFERENCES [dbo].[DimInvoiceLineGroup1] ([InvoiceLineGroup1Key])
GO
ALTER TABLE [dbo].[PlaybookProjectFullScopeSummary] ADD CONSTRAINT [FK_PlaybookProjectFullScopeSummary_6] FOREIGN KEY ([LastInvoiceLineGroup2Key]) REFERENCES [dbo].[DimInvoiceLineGroup2] ([InvoiceLineGroup2Key])
GO
