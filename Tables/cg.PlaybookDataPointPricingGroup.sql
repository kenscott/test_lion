CREATE TABLE [cg].[PlaybookDataPointPricingGroup]
(
[PlaybookDataPointPricingGroupKey] [dbo].[Key_Normal_type] NOT NULL IDENTITY(1, 1),
[PlaybookDataPointKey] [dbo].[Key_Normal_type] NOT NULL,
[PlaybookPricingGroupKey] [dbo].[Key_Normal_type] NOT NULL,
[PricingRuleKey] [dbo].[Key_Normal_type] NOT NULL,
[RulingMemberFlag] [dbo].[Description_Small_type] NOT NULL,
[ClaimBackPercent] [dbo].[Percent_Type] NULL,
[SectionNumber] [smallint] NOT NULL CONSTRAINT [DF_PlaybookDataPointPricingGroup_SectionNumber] DEFAULT ((1))
) ON [PRIMARY]
GO
ALTER TABLE [cg].[PlaybookDataPointPricingGroup] ADD CONSTRAINT [PK_PlaybookDataPointPricingGroup] PRIMARY KEY NONCLUSTERED  ([PlaybookDataPointPricingGroupKey]) ON [PRIMARY]
GO
CREATE UNIQUE CLUSTERED INDEX [I_PlaybookDataPointPricingGroup_1] ON [cg].[PlaybookDataPointPricingGroup] ([PlaybookDataPointKey], [PlaybookPricingGroupKey]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [I_PlaybookDataPointPricingGroup_6] ON [cg].[PlaybookDataPointPricingGroup] ([PlaybookDataPointKey], [RulingMemberFlag], [PricingRuleKey], [PlaybookPricingGroupKey]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [I_PlaybookDataPointPricingGroup_4] ON [cg].[PlaybookDataPointPricingGroup] ([PlaybookPricingGroupKey], [RulingMemberFlag]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [I_PlaybookDataPointPricingGroup_3] ON [cg].[PlaybookDataPointPricingGroup] ([PricingRuleKey]) ON [PRIMARY]
GO
ALTER TABLE [cg].[PlaybookDataPointPricingGroup] ADD CONSTRAINT [FK_PlaybookDataPointPricingGroup_1] FOREIGN KEY ([PlaybookDataPointKey]) REFERENCES [cg].[PlaybookDatapoint] ([PlaybookDataPointKey])
GO
ALTER TABLE [cg].[PlaybookDataPointPricingGroup] ADD CONSTRAINT [FK_PlaybookDataPointPricingGroup_2] FOREIGN KEY ([PlaybookPricingGroupKey]) REFERENCES [dbo].[PlaybookPricingGroup] ([PlaybookPricingGroupKey])
GO
ALTER TABLE [cg].[PlaybookDataPointPricingGroup] ADD CONSTRAINT [FK_PlaybookDataPointPricingGroup_3] FOREIGN KEY ([PricingRuleKey]) REFERENCES [dbo].[ATKPricingRule] ([PricingRuleKey])
GO
