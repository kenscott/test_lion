CREATE TABLE [dbo].[WebUser]
(
[WebUserKey] [dbo].[Key_Normal_type] NOT NULL IDENTITY(1, 1),
[ODSPersonKey] [dbo].[Key_Normal_type] NULL,
[AccountManagerKey] [dbo].[Key_Normal_type] NULL,
[WebUserPassword] [dbo].[Description_Normal_type] NULL,
[UserName] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Version] [dbo].[Int_Type] NOT NULL CONSTRAINT [DF__WebUser__Version__6D849645] DEFAULT ((0)),
[CreationDate] [dbo].[DateTime_Type] NOT NULL CONSTRAINT [DF__WebUser__Creatio__6E78BA7E] DEFAULT (getdate()),
[ModificationDate] [dbo].[DateTime_Type] NOT NULL CONSTRAINT [DF__WebUser__Modific__6F6CDEB7] DEFAULT (getdate()),
[CreationUser] [dbo].[Description_Normal_type] NULL,
[ModificationUser] [dbo].[Description_Normal_type] NULL,
[ModificationUserAddress] [dbo].[Description_Normal_type] NULL,
[CreationUserAddress] [dbo].[Description_Normal_type] NULL,
[VendorKey] [dbo].[Key_Normal_type] NULL,
[WebUserPasswordExpirationDate] [date] NULL CONSTRAINT [DF_WebUser_WebUserPasswordExpirationDate] DEFAULT (dateadd(day,(90),getdate()))
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[WebUser] ADD CONSTRAINT [PK_WebUser] PRIMARY KEY CLUSTERED  ([WebUserKey]) ON [PRIMARY]
GO
SET ANSI_NULLS ON
GO
SET ANSI_PADDING ON
GO
SET ANSI_WARNINGS ON
GO
SET ARITHABORT ON
GO
SET CONCAT_NULL_YIELDS_NULL ON
GO
SET NUMERIC_ROUNDABORT OFF
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE UNIQUE NONCLUSTERED INDEX [I_WebUser_1] ON [dbo].[WebUser] ([AccountManagerKey]) WHERE ([AccountManagerKey] IS NOT NULL) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [I_WebUser_3] ON [dbo].[WebUser] ([ODSPersonKey]) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [I_WebUser_2] ON [dbo].[WebUser] ([UserName]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[WebUser] ADD CONSTRAINT [FK_WebUser_1] FOREIGN KEY ([AccountManagerKey]) REFERENCES [dbo].[DimAccountManager] ([AccountManagerKey])
GO
