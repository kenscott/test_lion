CREATE TABLE [cc].[SupplierContractDocument]
(
[ContractDocumentKey] [int] NOT NULL IDENTITY(1, 1),
[ContractKey] [int] NOT NULL,
[Version] [int] NOT NULL,
[ColumbusDocumentId] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[DocumentName] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Comments] [varchar] (4000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreationDate] [datetime] NULL,
[CreatedByWebUserKey] [int] NULL,
[ModifiedByWebUserKey] [int] NULL,
[ModificationDate] [datetime] NULL,
[ColumbusDocumentCreationDate] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [cc].[SupplierContractDocument] ADD CONSTRAINT [PK_SupplierContractDocument] PRIMARY KEY CLUSTERED  ([ContractDocumentKey]) ON [PRIMARY]
GO
