CREATE TABLE [ppe].[RecommendBrand]
(
[RecommendBrandKey] [int] NOT NULL IDENTITY(1, 1),
[RecommendKey] [int] NOT NULL,
[BranchPrimaryBrand] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [ppe].[RecommendBrand] ADD CONSTRAINT [PK_RecommendBrand] PRIMARY KEY CLUSTERED  ([RecommendBrandKey]) ON [PRIMARY]
GO
ALTER TABLE [ppe].[RecommendBrand] ADD CONSTRAINT [FK_Recommend_Brand] FOREIGN KEY ([RecommendKey]) REFERENCES [ppe].[Recommend] ([RecommendKey])
GO
