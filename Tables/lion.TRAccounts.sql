CREATE TABLE [lion].[TRAccounts]
(
[AccountNumber] [dbo].[Description_Small_type] NOT NULL,
[AccountName] [dbo].[Description_Normal_type] NOT NULL,
[AccountManagerFullName] [dbo].[Description_Normal_type] NOT NULL,
[AccountManagerEmail] [dbo].[Description_Normal_type] NULL,
[BranchName] [dbo].[UDVarchar_type] NULL,
[Customer Brand] [dbo].[UDVarchar_type] NULL,
[Sales] [decimal] (38, 8) NULL,
[Cost] [decimal] (38, 8) NULL,
[GP] [decimal] (38, 8) NULL,
[GPP] [decimal] (38, 6) NULL
) ON [PRIMARY]
GO
