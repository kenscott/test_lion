CREATE TABLE [dbo].[LogDCP]
(
[LogDCPID] [int] NOT NULL IDENTITY(1, 1),
[StepDesc] [dbo].[Description_Normal_type] NOT NULL,
[BeginTime] [datetime] NOT NULL,
[EndTime] [datetime] NULL,
[RowsInserted] [dbo].[Quantity_Normal_type] NOT NULL CONSTRAINT [DF_LogDCP_RowsInserted] DEFAULT ((0)),
[RowsUpdated] [dbo].[Quantity_Normal_type] NOT NULL CONSTRAINT [DF_LogDCP_RowsUpdated] DEFAULT ((0)),
[RowsDeleted] [dbo].[Quantity_Normal_type] NOT NULL CONSTRAINT [DF_LogDCP_RowsDeleted] DEFAULT ((0)),
[ProjectKey] [dbo].[Key_Normal_type] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[LogDCP] ADD CONSTRAINT [PK_LogDCP] PRIMARY KEY CLUSTERED  ([LogDCPID]) ON [PRIMARY]
GO
