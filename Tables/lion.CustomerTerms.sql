CREATE TABLE [lion].[CustomerTerms]
(
[CustomerTermsKey] [dbo].[Key_Normal_type] NOT NULL IDENTITY(1, 1),
[AccountKey] [dbo].[Key_Normal_type] NOT NULL CONSTRAINT [DF_CustomerTerms_AccountKey] DEFAULT ((1)),
[ItemKey] [dbo].[Key_Normal_type] NOT NULL CONSTRAINT [DF_CustomerTerms_ItemKey] DEFAULT ((1)),
[ItemGroup3Key] [dbo].[Key_Normal_type] NOT NULL CONSTRAINT [DF_CustomerTerms_ItemGroup3Key] DEFAULT ((1)),
[AccountId] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PyramidCode] [dbo].[UDVarchar_type] NULL,
[SPGCode] [dbo].[UDVarchar_type] NULL,
[OwningBrandCode] [dbo].[UDVarchar_type] NULL,
[Usualdiscount1] [decimal] (38, 8) NULL,
[Usualdiscount2] [decimal] (38, 8) NULL,
[DeleteFlag] [dbo].[UDVarchar_type] NULL,
[LastChangeInitials] [dbo].[UDVarchar_type] NULL,
[LastChangeDate] [dbo].[UDVarchar_type] NULL,
[LastChangeTime] [dbo].[UDVarchar_type] NULL,
[LastChangePLID] [dbo].[UDVarchar_type] NULL,
[LastChangeBranch] [dbo].[UDVarchar_type] NULL,
[ExceptionProduct] [dbo].[UDVarchar_type] NULL,
[ExceptionDiscount] [decimal] (38, 8) NULL,
[ExceptionFixedPrice] [decimal] (38, 8) NULL,
[ExceptionFromDate] [dbo].[UDVarchar_type] NULL,
[ExceptionToDate] [dbo].[UDVarchar_type] NULL,
[ExceptionPCF] [dbo].[UDVarchar_type] NULL,
[FallbackMarginFlag] [bit] NULL CONSTRAINT [DF_CustomerTerms_FallbackMarginFlag] DEFAULT ((0)),
[CreationDayKey] [dbo].[Key_Small_type] NOT NULL CONSTRAINT [DF_CustomerTerms_CreationDayKey] DEFAULT ((1)),
[ModificationDayKey] [dbo].[Key_Small_type] NOT NULL CONSTRAINT [DF_CustomerTerms_ModificationDayKey] DEFAULT ((1)),
[OwningBrandCode_Previous] [dbo].[UDVarchar_type] NULL,
[OwningBrandCode_Previous_ValidToDate] [date] NULL,
[Usualdiscount1_Previous] [decimal] (38, 8) NULL,
[Usualdiscount1_Previous_ValidToDate] [date] NULL,
[Usualdiscount2_Previous] [decimal] (38, 8) NULL,
[Usualdiscount2_Previous_ValidToDate] [date] NULL,
[DeleteFlag_Previous] [dbo].[UDVarchar_type] NULL,
[DeleteFlag_Previous_ValidToDate] [date] NULL,
[LastChangeInitials_Previous] [dbo].[UDVarchar_type] NULL,
[LastChangeInitials_Previous_ValidToDate] [date] NULL,
[LastChangeDate_Previous] [dbo].[UDVarchar_type] NULL,
[LastChangeDate_Previous_ValidToDate] [date] NULL,
[LastChangeTime_Previous] [dbo].[UDVarchar_type] NULL,
[LastChangeTime_Previous_ValidToDate] [date] NULL,
[LastChangePLID_Previous] [dbo].[UDVarchar_type] NULL,
[LastChangePLID_Previous_ValidToDate] [date] NULL,
[LastChangeBranch_Previous] [dbo].[UDVarchar_type] NULL,
[LastChangeBranch_Previous_ValidToDate] [date] NULL,
[ExceptionProduct_Previous] [dbo].[UDVarchar_type] NULL,
[ExceptionProduct_Previous_ValidToDate] [date] NULL,
[ExceptionDiscount_Previous] [decimal] (38, 8) NULL,
[ExceptionDiscount_Previous_ValidToDate] [date] NULL,
[ExceptionFixedPrice_Previous] [decimal] (38, 8) NULL,
[ExceptionFixedPrice_Previous_ValidToDate] [date] NULL,
[ExceptionFromDate_Previous] [dbo].[UDVarchar_type] NULL,
[ExceptionFromDate_Previous_ValidToDate] [date] NULL,
[ExceptionToDate_Previous] [dbo].[UDVarchar_type] NULL,
[ExceptionToDate_Previous_ValidToDate] [date] NULL,
[ExceptionPCF_Previous] [dbo].[UDVarchar_type] NULL,
[ExceptionPCF_Previous_ValidToDate] [date] NULL,
[FallbackMarginFlag_Previous] [bit] NULL CONSTRAINT [DF_CustomerTerms_FallbackMarginFlag_Previous] DEFAULT ((0)),
[FallbackMarginFlag_Previous_ValidToDate] [date] NULL
) ON [PRIMARY]
GO
ALTER TABLE [lion].[CustomerTerms] ADD CONSTRAINT [PK_CustomerTerms] PRIMARY KEY NONCLUSTERED  ([CustomerTermsKey]) ON [PRIMARY]
GO
CREATE CLUSTERED INDEX [i_1] ON [lion].[CustomerTerms] ([AccountKey]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [i_3] ON [lion].[CustomerTerms] ([AccountKey], [ItemGroup3Key]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [i_2] ON [lion].[CustomerTerms] ([ItemKey], [ItemGroup3Key], [AccountKey]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [i_4] ON [lion].[CustomerTerms] ([ModificationDayKey]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_14] ON [lion].[CustomerTerms] ([SPGCode]) INCLUDE ([AccountKey], [DeleteFlag], [LastChangeBranch], [LastChangeDate], [LastChangeInitials], [OwningBrandCode], [PyramidCode], [Usualdiscount1], [Usualdiscount2]) ON [PRIMARY]
GO
ALTER TABLE [lion].[CustomerTerms] ADD CONSTRAINT [FK_CustomerTerms_1] FOREIGN KEY ([AccountKey]) REFERENCES [dbo].[DimAccount] ([AccountKey])
GO
ALTER TABLE [lion].[CustomerTerms] ADD CONSTRAINT [FK_CustomerTerms_2] FOREIGN KEY ([ItemKey]) REFERENCES [dbo].[DimItem] ([ItemKey])
GO
ALTER TABLE [lion].[CustomerTerms] ADD CONSTRAINT [FK_CustomerTerms_3] FOREIGN KEY ([ModificationDayKey]) REFERENCES [dbo].[DimDay] ([DayKey])
GO
