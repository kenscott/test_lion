CREATE TABLE [lion].[BranchLiveStatus]
(
[Branch] [dbo].[Description_Small_type] NOT NULL,
[Phase] [dbo].[Description_Small_type] NULL,
[NonTermsStatus] [dbo].[Description_Small_type] NULL,
[NonTermsPlannedDate] [datetime] NULL,
[NonTermsActualDate] [datetime] NULL,
[InterimTermsStatus] [dbo].[Description_Small_type] NULL,
[InterimTermsPlannedDate] [datetime] NULL,
[InterimTermsActualDate] [datetime] NULL,
[PriceFlowStatus] [dbo].[Description_Small_type] NULL,
[PriceFlowPlannedDate] [datetime] NULL,
[PriceFlowActualDate] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [lion].[BranchLiveStatus] ADD CONSTRAINT [PK_BranchLiveStatus] PRIMARY KEY CLUSTERED  ([Branch]) ON [PRIMARY]
GO
