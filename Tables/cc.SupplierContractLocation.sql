CREATE TABLE [cc].[SupplierContractLocation]
(
[ContractLocationKey] [int] NOT NULL IDENTITY(1, 1),
[ContractKey] [int] NOT NULL,
[ContractLocationLevelKey] [tinyint] NOT NULL,
[LocationKey] [int] NOT NULL,
[StartDate] [date] NULL,
[ExpirationDate] [date] NULL,
[CreatedByWebUserKey] [int] NULL,
[CreatedOnDT] [datetime] NULL CONSTRAINT [DF__DimContra__Creat__38023D9D] DEFAULT (getdate()),
[ModifiedByWebUserKey] [int] NULL,
[ModifiedOnDT] [datetime] NULL CONSTRAINT [DF__DimContra__Modif__38F661D6] DEFAULT (getdate()),
[LocationName] [dbo].[Description_Small_type] NULL,
[LocationDescription] [dbo].[UDVarchar_type] NULL
) ON [PRIMARY]
GO
ALTER TABLE [cc].[SupplierContractLocation] ADD CONSTRAINT [PK_DimContractLocation] PRIMARY KEY NONCLUSTERED  ([ContractLocationKey]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [i_temp] ON [cc].[SupplierContractLocation] ([ContractKey]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [I_ContractKey_1,>] ON [cc].[SupplierContractLocation] ([ContractKey]) INCLUDE ([LocationKey], [LocationName]) ON [PRIMARY]
GO
ALTER TABLE [cc].[SupplierContractLocation] ADD CONSTRAINT [Contract_ContractLocation_FK1] FOREIGN KEY ([ContractKey]) REFERENCES [cc].[SupplierContract] ([ContractKey])
GO
ALTER TABLE [cc].[SupplierContractLocation] ADD CONSTRAINT [ContractLocationLevel_ContractLocation_FK2] FOREIGN KEY ([ContractLocationLevelKey]) REFERENCES [cc].[SupplierContractLocationLevel] ([ContractLocationLevelKey])
GO
