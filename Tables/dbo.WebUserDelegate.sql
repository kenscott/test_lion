CREATE TABLE [dbo].[WebUserDelegate]
(
[WebUserDelegateKey] [int] NOT NULL IDENTITY(1, 1),
[DelegatorWebUserKey] [int] NOT NULL,
[DelegateWebUserKey] [int] NOT NULL,
[StartDate] [datetime] NOT NULL CONSTRAINT [DF_WebUserDelegate_StartDate] DEFAULT (getdate()),
[EndDate] [datetime] NOT NULL CONSTRAINT [DF_WebUserDelegate_EndDate] DEFAULT (((12)-(31))-(9999)),
[CreatedByWebUserKey] [dbo].[Key_Normal_type] NOT NULL,
[CreationDate] [datetime] NOT NULL CONSTRAINT [DF_WebUserDelegate_CreationDate] DEFAULT (getdate()),
[ModificationDate] [datetime] NOT NULL CONSTRAINT [DF_WebUserDelegate_ModificationDate] DEFAULT (getdate()),
[EmailStatus] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF_WebUserDelegate_EmailStatus] DEFAULT ('N')
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[WebUserDelegate] ADD CONSTRAINT [PK_WebUserDelegate] PRIMARY KEY NONCLUSTERED  ([WebUserDelegateKey]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[WebUserDelegate] ADD CONSTRAINT [Idx_Delegator_Delegate] UNIQUE CLUSTERED  ([DelegatorWebUserKey], [DelegateWebUserKey], [StartDate]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[WebUserDelegate] ADD CONSTRAINT [fk_webuserdelegate_key1] FOREIGN KEY ([DelegatorWebUserKey]) REFERENCES [dbo].[WebUser] ([WebUserKey])
GO
ALTER TABLE [dbo].[WebUserDelegate] ADD CONSTRAINT [fk_webuserdelegate_key2] FOREIGN KEY ([DelegateWebUserKey]) REFERENCES [dbo].[WebUser] ([WebUserKey])
GO
ALTER TABLE [dbo].[WebUserDelegate] ADD CONSTRAINT [fk_webuserdelegate_key3] FOREIGN KEY ([CreatedByWebUserKey]) REFERENCES [dbo].[WebUser] ([WebUserKey])
GO
