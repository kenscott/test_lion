CREATE TABLE [dbo].[ATKAccountStatus]
(
[AccountStatus] [dbo].[Description_Normal_type] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ATKAccountStatus] ADD CONSTRAINT [PK_ATKAccountStatus] PRIMARY KEY CLUSTERED  ([AccountStatus]) ON [PRIMARY]
GO
