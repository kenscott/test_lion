CREATE TABLE [lion].[RPBPriceBandAttributes]
(
[PlaybookPricingGroupKey] [dbo].[Key_Normal_type] NOT NULL,
[PricingRuleSequenceNumber] [dbo].[Int_255_type] NOT NULL,
[PricingRuleKey] [dbo].[Key_Normal_type] NOT NULL,
[PlaybookRegion] [dbo].[Description_Normal_type] NULL,
[CustomerSpecialism] [dbo].[Description_Normal_type] NULL,
[Sales603010Bucket] [dbo].[Description_Normal_type] NULL,
[StrategicItem] [dbo].[Description_Normal_type] NULL,
[PriceApproach] [dbo].[Description_Normal_type] NULL,
[Item] [dbo].[Description_Normal_type] NULL,
[LLSPG] [dbo].[Description_Normal_type] NULL,
[ContractClaimsIndicator] [dbo].[Description_Normal_type] NULL,
[ShipmentType] [dbo].[Description_Normal_type] NULL,
[PlaybookProductBrand] [dbo].[Description_Normal_type] NULL,
[RegionExceptionIndicator] [dbo].[Description_Normal_type] NULL
) ON [PRIMARY]
GO
ALTER TABLE [lion].[RPBPriceBandAttributes] ADD CONSTRAINT [PK_RPBPriceBandAttributes] PRIMARY KEY CLUSTERED  ([PlaybookPricingGroupKey]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [I_1] ON [lion].[RPBPriceBandAttributes] ([PlaybookPricingGroupKey]) INCLUDE ([PriceApproach], [PricingRuleSequenceNumber]) ON [PRIMARY]
GO
ALTER TABLE [lion].[RPBPriceBandAttributes] ADD CONSTRAINT [FK_1] FOREIGN KEY ([PricingRuleKey]) REFERENCES [dbo].[ATKPricingRule] ([PricingRuleKey])
GO
