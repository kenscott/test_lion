CREATE TABLE [ppe].[RequestLog]
(
[RequestLogKey] [int] NOT NULL IDENTITY(1, 1),
[WebUserKey] [int] NULL,
[RequestKey] [int] NULL,
[OldStatusKey] [smallint] NULL,
[NewStatusKey] [smallint] NULL,
[RequestType] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LogTime] [datetime] NULL CONSTRAINT [DF__RequestLo__LogTi__77AABCF8] DEFAULT (getdate()),
[RequestAction] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[EmailSent] [bit] NULL,
[OldOwnerWebUserKey] [int] NULL,
[NewOwnerWebUserKey] [int] NULL,
[Notes] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [ppe].[RequestLog] ADD CONSTRAINT [PK_RequestLog] PRIMARY KEY NONCLUSTERED  ([RequestLogKey]) ON [PRIMARY]
GO
CREATE CLUSTERED INDEX [i_requestKey] ON [ppe].[RequestLog] ([RequestKey]) ON [PRIMARY]
GO
