CREATE TABLE [dbo].[Time]
(
[DayKey] [dbo].[Key_Small_type] NOT NULL,
[Date] [datetime] NOT NULL,
[Date_Name] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Year] [datetime] NULL,
[Year_Name] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Quarter] [datetime] NULL,
[Quarter_Name] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Month] [datetime] NULL,
[Month_Name] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Day_Of_Year] [int] NULL,
[Day_Of_Year_Name] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Day_Of_Quarter] [int] NULL,
[Day_Of_Quarter_Name] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Day_Of_Month] [int] NULL,
[Day_Of_Month_Name] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Month_Of_Year] [int] NULL,
[Month_Of_Year_Name] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Month_Of_Quarter] [int] NULL,
[Month_Of_Quarter_Name] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Quarter_Of_Year] [int] NULL,
[Quarter_Of_Year_Name] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Time] ADD CONSTRAINT [PK_Time] PRIMARY KEY CLUSTERED  ([DayKey]) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_Date] ON [dbo].[Time] ([Date]) ON [PRIMARY]
GO
EXEC sp_addextendedproperty N'AllowGen', N'True', 'SCHEMA', N'dbo', 'TABLE', N'Time', NULL, NULL
GO
EXEC sp_addextendedproperty N'DSVTable', N'Time', 'SCHEMA', N'dbo', 'TABLE', N'Time', NULL, NULL
GO
EXEC sp_addextendedproperty N'Project', N'9e95fac7-8774-4203-8400-5ce2646ca096', 'SCHEMA', N'dbo', 'TABLE', N'Time', NULL, NULL
GO
EXEC sp_addextendedproperty N'AllowGen', N'True', 'SCHEMA', N'dbo', 'TABLE', N'Time', 'COLUMN', N'Date'
GO
EXEC sp_addextendedproperty N'DSVColumn', N'Date', 'SCHEMA', N'dbo', 'TABLE', N'Time', 'COLUMN', N'Date'
GO
EXEC sp_addextendedproperty N'AllowGen', N'True', 'SCHEMA', N'dbo', 'TABLE', N'Time', 'COLUMN', N'Date_Name'
GO
EXEC sp_addextendedproperty N'DSVColumn', N'Date_Name', 'SCHEMA', N'dbo', 'TABLE', N'Time', 'COLUMN', N'Date_Name'
GO
EXEC sp_addextendedproperty N'AllowGen', N'True', 'SCHEMA', N'dbo', 'TABLE', N'Time', 'COLUMN', N'Day_Of_Month'
GO
EXEC sp_addextendedproperty N'DSVColumn', N'Day_Of_Month', 'SCHEMA', N'dbo', 'TABLE', N'Time', 'COLUMN', N'Day_Of_Month'
GO
EXEC sp_addextendedproperty N'AllowGen', N'True', 'SCHEMA', N'dbo', 'TABLE', N'Time', 'COLUMN', N'Day_Of_Month_Name'
GO
EXEC sp_addextendedproperty N'DSVColumn', N'Day_Of_Month_Name', 'SCHEMA', N'dbo', 'TABLE', N'Time', 'COLUMN', N'Day_Of_Month_Name'
GO
EXEC sp_addextendedproperty N'AllowGen', N'True', 'SCHEMA', N'dbo', 'TABLE', N'Time', 'COLUMN', N'Day_Of_Quarter'
GO
EXEC sp_addextendedproperty N'DSVColumn', N'Day_Of_Quarter', 'SCHEMA', N'dbo', 'TABLE', N'Time', 'COLUMN', N'Day_Of_Quarter'
GO
EXEC sp_addextendedproperty N'AllowGen', N'True', 'SCHEMA', N'dbo', 'TABLE', N'Time', 'COLUMN', N'Day_Of_Quarter_Name'
GO
EXEC sp_addextendedproperty N'DSVColumn', N'Day_Of_Quarter_Name', 'SCHEMA', N'dbo', 'TABLE', N'Time', 'COLUMN', N'Day_Of_Quarter_Name'
GO
EXEC sp_addextendedproperty N'AllowGen', N'True', 'SCHEMA', N'dbo', 'TABLE', N'Time', 'COLUMN', N'Day_Of_Year'
GO
EXEC sp_addextendedproperty N'DSVColumn', N'Day_Of_Year', 'SCHEMA', N'dbo', 'TABLE', N'Time', 'COLUMN', N'Day_Of_Year'
GO
EXEC sp_addextendedproperty N'AllowGen', N'True', 'SCHEMA', N'dbo', 'TABLE', N'Time', 'COLUMN', N'Day_Of_Year_Name'
GO
EXEC sp_addextendedproperty N'DSVColumn', N'Day_Of_Year_Name', 'SCHEMA', N'dbo', 'TABLE', N'Time', 'COLUMN', N'Day_Of_Year_Name'
GO
EXEC sp_addextendedproperty N'AllowGen', N'True', 'SCHEMA', N'dbo', 'TABLE', N'Time', 'COLUMN', N'Month'
GO
EXEC sp_addextendedproperty N'DSVColumn', N'Month', 'SCHEMA', N'dbo', 'TABLE', N'Time', 'COLUMN', N'Month'
GO
EXEC sp_addextendedproperty N'AllowGen', N'True', 'SCHEMA', N'dbo', 'TABLE', N'Time', 'COLUMN', N'Month_Name'
GO
EXEC sp_addextendedproperty N'DSVColumn', N'Month_Name', 'SCHEMA', N'dbo', 'TABLE', N'Time', 'COLUMN', N'Month_Name'
GO
EXEC sp_addextendedproperty N'AllowGen', N'True', 'SCHEMA', N'dbo', 'TABLE', N'Time', 'COLUMN', N'Month_Of_Quarter'
GO
EXEC sp_addextendedproperty N'DSVColumn', N'Month_Of_Quarter', 'SCHEMA', N'dbo', 'TABLE', N'Time', 'COLUMN', N'Month_Of_Quarter'
GO
EXEC sp_addextendedproperty N'AllowGen', N'True', 'SCHEMA', N'dbo', 'TABLE', N'Time', 'COLUMN', N'Month_Of_Quarter_Name'
GO
EXEC sp_addextendedproperty N'DSVColumn', N'Month_Of_Quarter_Name', 'SCHEMA', N'dbo', 'TABLE', N'Time', 'COLUMN', N'Month_Of_Quarter_Name'
GO
EXEC sp_addextendedproperty N'AllowGen', N'True', 'SCHEMA', N'dbo', 'TABLE', N'Time', 'COLUMN', N'Month_Of_Year'
GO
EXEC sp_addextendedproperty N'DSVColumn', N'Month_Of_Year', 'SCHEMA', N'dbo', 'TABLE', N'Time', 'COLUMN', N'Month_Of_Year'
GO
EXEC sp_addextendedproperty N'AllowGen', N'True', 'SCHEMA', N'dbo', 'TABLE', N'Time', 'COLUMN', N'Month_Of_Year_Name'
GO
EXEC sp_addextendedproperty N'DSVColumn', N'Month_Of_Year_Name', 'SCHEMA', N'dbo', 'TABLE', N'Time', 'COLUMN', N'Month_Of_Year_Name'
GO
EXEC sp_addextendedproperty N'AllowGen', N'True', 'SCHEMA', N'dbo', 'TABLE', N'Time', 'COLUMN', N'Quarter'
GO
EXEC sp_addextendedproperty N'DSVColumn', N'Quarter', 'SCHEMA', N'dbo', 'TABLE', N'Time', 'COLUMN', N'Quarter'
GO
EXEC sp_addextendedproperty N'AllowGen', N'True', 'SCHEMA', N'dbo', 'TABLE', N'Time', 'COLUMN', N'Quarter_Name'
GO
EXEC sp_addextendedproperty N'DSVColumn', N'Quarter_Name', 'SCHEMA', N'dbo', 'TABLE', N'Time', 'COLUMN', N'Quarter_Name'
GO
EXEC sp_addextendedproperty N'AllowGen', N'True', 'SCHEMA', N'dbo', 'TABLE', N'Time', 'COLUMN', N'Quarter_Of_Year'
GO
EXEC sp_addextendedproperty N'DSVColumn', N'Quarter_Of_Year', 'SCHEMA', N'dbo', 'TABLE', N'Time', 'COLUMN', N'Quarter_Of_Year'
GO
EXEC sp_addextendedproperty N'AllowGen', N'True', 'SCHEMA', N'dbo', 'TABLE', N'Time', 'COLUMN', N'Quarter_Of_Year_Name'
GO
EXEC sp_addextendedproperty N'DSVColumn', N'Quarter_Of_Year_Name', 'SCHEMA', N'dbo', 'TABLE', N'Time', 'COLUMN', N'Quarter_Of_Year_Name'
GO
EXEC sp_addextendedproperty N'AllowGen', N'True', 'SCHEMA', N'dbo', 'TABLE', N'Time', 'COLUMN', N'Year'
GO
EXEC sp_addextendedproperty N'DSVColumn', N'Year', 'SCHEMA', N'dbo', 'TABLE', N'Time', 'COLUMN', N'Year'
GO
EXEC sp_addextendedproperty N'AllowGen', N'True', 'SCHEMA', N'dbo', 'TABLE', N'Time', 'COLUMN', N'Year_Name'
GO
EXEC sp_addextendedproperty N'DSVColumn', N'Year_Name', 'SCHEMA', N'dbo', 'TABLE', N'Time', 'COLUMN', N'Year_Name'
GO
EXEC sp_addextendedproperty N'AllowGen', N'True', 'SCHEMA', N'dbo', 'TABLE', N'Time', 'CONSTRAINT', N'PK_Time'
GO
