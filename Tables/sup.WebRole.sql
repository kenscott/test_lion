CREATE TABLE [sup].[WebRole]
(
[WebRoleKey] [dbo].[Key_Normal_type] NOT NULL IDENTITY(1, 1),
[WebRoleDescription] [dbo].[Description_Normal_type] NOT NULL,
[WebRoleName] [dbo].[Description_Normal_type] NULL,
[WorkflowLevel] [int] NULL,
[Version] [dbo].[Int_Type] NOT NULL CONSTRAINT [DF__WEbRole__Version__6AA8299A] DEFAULT ((0)),
[CreationDate] [dbo].[DateTime_Type] NOT NULL CONSTRAINT [DF__WEbRole__Creatio__6B9C4DD3] DEFAULT (getdate()),
[ModificationDate] [dbo].[DateTime_Type] NOT NULL CONSTRAINT [DF__WEbRole__Modific__6C90720C] DEFAULT (getdate()),
[CreationUser] [dbo].[Description_Normal_type] NULL,
[ModificationUser] [dbo].[Description_Normal_type] NULL,
[ModificationUserAddress] [dbo].[Description_Normal_type] NULL,
[CreationUserAddress] [dbo].[Description_Normal_type] NULL,
[SystemManaged] [bit] NOT NULL CONSTRAINT [DF_WebRole_SystemManaged] DEFAULT ((0))
) ON [PRIMARY]
GO
ALTER TABLE [sup].[WebRole] ADD CONSTRAINT [PK_WebRole] PRIMARY KEY CLUSTERED  ([WebRoleKey]) ON [PRIMARY]
GO
