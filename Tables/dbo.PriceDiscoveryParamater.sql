CREATE TABLE [dbo].[PriceDiscoveryParamater]
(
[ColumnName] [dbo].[Description_Normal_type] NOT NULL,
[DisplayName] [dbo].[Description_Normal_type] NOT NULL,
[TableName] [dbo].[Description_Normal_type] NOT NULL,
[ExcludeIfNullColumn] [dbo].[Description_Normal_type] NOT NULL,
[PriceDiscoveryParamaterKey] [dbo].[Key_Normal_type] NOT NULL IDENTITY(1, 1),
[SubColumn1Name] [dbo].[Description_Normal_type] NOT NULL CONSTRAINT [DF_PriceDiscoveryParamater_SubColumn1Name] DEFAULT (''),
[SubTable1Name] [dbo].[Description_Normal_type] NOT NULL CONSTRAINT [DF_PriceDiscoveryParamater_SubTable1Name] DEFAULT ('')
) ON [PRIMARY]
GO
CREATE UNIQUE CLUSTERED INDEX [UI_PriceDiscoveryParamater_1] ON [dbo].[PriceDiscoveryParamater] ([ColumnName], [TableName], [ExcludeIfNullColumn]) ON [PRIMARY]
GO
