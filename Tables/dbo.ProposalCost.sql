CREATE TABLE [dbo].[ProposalCost]
(
[ProposalCostKey] [dbo].[Key_Normal_type] NOT NULL IDENTITY(1, 1),
[PlaybookPriceProposalKey] [dbo].[Key_Normal_type] NULL,
[AcceptanceCodeKey] [dbo].[Key_Small_type] NOT NULL,
[UserAcceptanceCodeKey] [dbo].[Key_Small_type] NOT NULL,
[DefaultAcceptanceCodeKey] [dbo].[Key_Small_type] NULL,
[ProjectKey] [dbo].[Key_Normal_type] NOT NULL,
[AccountKey] [dbo].[Key_Normal_type] NOT NULL,
[ItemKey] [dbo].[Key_Normal_type] NOT NULL,
[InvoiceLineGroup1Key] [dbo].[Key_Normal_type] NOT NULL,
[ProposedCost] [dbo].[Money_Normal_Type] NOT NULL,
[NewCost] [dbo].[Money_Normal_Type] NOT NULL,
[TotalActual12MonthCost] [dbo].[Money_Normal_Type] NOT NULL,
[TotalActual12MonthPrice] [dbo].[Money_Normal_Type] NOT NULL,
[TotalActual12MonthQuantity] [dbo].[Quantity_Normal_type] NOT NULL,
[TotalActual12MonthFrequency] [dbo].[Quantity_Normal_type] NOT NULL,
[LastItemPrice] [dbo].[Money_Normal_Type] NOT NULL,
[LastItemCost] [dbo].[Money_Normal_Type] NOT NULL,
[CurrentItemStandardCost] [dbo].[Money_Normal_Type] NULL,
[CurrentItemAISCost] [dbo].[Money_Normal_Type] NULL,
[FutureItemCost] [dbo].[Money_Normal_Type] NULL,
[Forecasted12MonthQuantity] [dbo].[Quantity_Normal_type] NOT NULL,
[TotalForecasted12MonthPrice_LastPrice] [dbo].[Money_Normal_Type] NOT NULL,
[TotalForecasted12MonthCost_LastCost] [dbo].[Money_Normal_Type] NOT NULL,
[TotalForecasted12MonthCost_ProposedCost] [dbo].[Money_Normal_Type] NOT NULL,
[TotalForecasted12MonthCost_NewCost] [dbo].[Money_Normal_Type] NOT NULL,
[TotalForecasted12MonthCost_RelevantCost] [dbo].[Money_Normal_Type] NOT NULL,
[Version] [dbo].[Int_Type] NOT NULL CONSTRAINT [DF_ProposalCost_Version] DEFAULT ((0)),
[CreationDate] [dbo].[DateTime_Type] NOT NULL CONSTRAINT [DF_ProposalCost_CreationDate] DEFAULT (getdate()),
[ModificationDate] [dbo].[DateTime_Type] NOT NULL CONSTRAINT [DF_ProposalCost_ModificationDate] DEFAULT (getdate()),
[CreationUser] [dbo].[Description_Normal_type] NULL,
[ModificationUser] [dbo].[Description_Normal_type] NULL,
[PlaybookDataPointGroupKey] [dbo].[Key_Normal_type] NULL,
[AccountManagerKey] [dbo].[Key_Normal_type] NULL,
[ModificationUserAddress] [dbo].[Description_Normal_type] NULL,
[CreationUserAddress] [dbo].[Description_Normal_type] NULL,
[AcceptanceStatusDate] [datetime] NULL,
[LastSaleDayKey] [dbo].[Key_Small_type] NULL,
[LastInvoiceLineGroup2Key] [dbo].[Key_Normal_type] NOT NULL,
[EffectiveDate] [dbo].[DateTime_Type] NOT NULL,
[SuppressionDate] [dbo].[DateTime_Type] NOT NULL,
[ReasonCodeKey] [dbo].[Key_Small_type] NULL,
[UDVarchar1] [dbo].[Description_Small_type] NULL,
[UDVarchar2] [dbo].[Description_Small_type] NULL,
[UDDecimal1] [dbo].[UDDecimal_type] NULL,
[UDDecimal2] [dbo].[UDDecimal_type] NULL,
[UOM] [nvarchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[UOMConversionFactor] [decimal] (38, 16) NULL,
[ListPricingUoM] [nvarchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ListPricingUoMConvFactor] [decimal] (19, 8) NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ProposalCost] ADD CONSTRAINT [PK_ProposalCost] PRIMARY KEY NONCLUSTERED  ([ProposalCostKey]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [I_ProposalCost_1] ON [dbo].[ProposalCost] ([AccountKey]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [I_ProposalCost_8] ON [dbo].[ProposalCost] ([AccountManagerKey]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [I_ProposalCost_3] ON [dbo].[ProposalCost] ([InvoiceLineGroup1Key]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [I_ProposalCost_2] ON [dbo].[ProposalCost] ([ItemKey]) ON [PRIMARY]
GO
CREATE CLUSTERED INDEX [I_ProposalCost_7] ON [dbo].[ProposalCost] ([PlaybookDataPointGroupKey]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [I_ProposalCost_4] ON [dbo].[ProposalCost] ([PlaybookPriceProposalKey]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [I_ProposalCost_6] ON [dbo].[ProposalCost] ([ProjectKey]) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [UI_ProposalCost_1] ON [dbo].[ProposalCost] ([ProjectKey], [AccountKey], [ItemKey], [InvoiceLineGroup1Key]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [I_Proposalcost_9] ON [dbo].[ProposalCost] ([ProposalCostKey], [AcceptanceCodeKey], [UserAcceptanceCodeKey], [DefaultAcceptanceCodeKey]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [I_ProposalCost_10] ON [dbo].[ProposalCost] ([SuppressionDate], [AccountKey], [ItemKey], [InvoiceLineGroup1Key]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ProposalCost] ADD CONSTRAINT [FK_ProposalCost_1] FOREIGN KEY ([AccountKey]) REFERENCES [dbo].[DimAccount] ([AccountKey])
GO
ALTER TABLE [dbo].[ProposalCost] ADD CONSTRAINT [FK_ProposalCost_10] FOREIGN KEY ([AcceptanceCodeKey]) REFERENCES [dbo].[ATKAcceptanceCode] ([AcceptanceCodeKey])
GO
ALTER TABLE [dbo].[ProposalCost] ADD CONSTRAINT [FK_ProposalCost_11] FOREIGN KEY ([UserAcceptanceCodeKey]) REFERENCES [dbo].[ATKAcceptanceCode] ([AcceptanceCodeKey])
GO
ALTER TABLE [dbo].[ProposalCost] ADD CONSTRAINT [FK_ProposalCost_14] FOREIGN KEY ([ReasonCodeKey]) REFERENCES [dbo].[ProposalReasonCode] ([ReasonCodeKey])
GO
ALTER TABLE [dbo].[ProposalCost] ADD CONSTRAINT [FK_ProposalCost_3] FOREIGN KEY ([InvoiceLineGroup1Key]) REFERENCES [dbo].[DimInvoiceLineGroup1] ([InvoiceLineGroup1Key])
GO
ALTER TABLE [dbo].[ProposalCost] ADD CONSTRAINT [FK_ProposalCost_6] FOREIGN KEY ([ProjectKey]) REFERENCES [dbo].[PlaybookProject] ([ProjectKey])
GO
ALTER TABLE [dbo].[ProposalCost] ADD CONSTRAINT [FK_ProposalCost_7] FOREIGN KEY ([PlaybookDataPointGroupKey]) REFERENCES [dbo].[PlaybookDataPointGroup] ([PlaybookDataPointGroupKey])
GO
ALTER TABLE [dbo].[ProposalCost] ADD CONSTRAINT [FK_ProposalCost_8] FOREIGN KEY ([PlaybookDataPointGroupKey]) REFERENCES [dbo].[PlaybookDataPointGroup] ([PlaybookDataPointGroupKey])
GO
ALTER TABLE [dbo].[ProposalCost] ADD CONSTRAINT [FK_ProposalCost_9] FOREIGN KEY ([AccountManagerKey]) REFERENCES [dbo].[DimAccountManager] ([AccountManagerKey])
GO
