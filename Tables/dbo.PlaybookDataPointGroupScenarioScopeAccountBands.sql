CREATE TABLE [dbo].[PlaybookDataPointGroupScenarioScopeAccountBands]
(
[PlaybookDataPointGroupKey] [dbo].[Key_Normal_type] NOT NULL,
[AccountKey] [dbo].[Key_Normal_type] NOT NULL,
[TotalActualPrice] [dbo].[Money_Normal_Type] NOT NULL,
[SalesBand] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SalesRank] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AvgGPPerOrder] [dbo].[Money_Normal_Type] NULL,
[GPBand] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[GPRank] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BlendedRank] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[PlaybookDataPointGroupScenarioScopeAccountBands] ADD CONSTRAINT [PK_PlaybookDataPointGroupScenarioScopeAccountBands] PRIMARY KEY CLUSTERED  ([PlaybookDataPointGroupKey], [AccountKey]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[PlaybookDataPointGroupScenarioScopeAccountBands] ADD CONSTRAINT [FK_PlaybookDataPointGroupScenarioScopeAccountBands_1] FOREIGN KEY ([PlaybookDataPointGroupKey]) REFERENCES [dbo].[PlaybookDataPointGroup] ([PlaybookDataPointGroupKey])
GO
ALTER TABLE [dbo].[PlaybookDataPointGroupScenarioScopeAccountBands] ADD CONSTRAINT [FK_PlaybookDataPointGroupScenarioScopeAccountBands_2] FOREIGN KEY ([AccountKey]) REFERENCES [dbo].[DimAccount] ([AccountKey])
GO
