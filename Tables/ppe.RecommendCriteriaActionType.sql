CREATE TABLE [ppe].[RecommendCriteriaActionType]
(
[RecommendCriteriaActionTypeKey] [dbo].[Key_Normal_type] NOT NULL IDENTITY(1, 1),
[ActionType] [dbo].[Description_Small_type] NOT NULL,
[ActionTypeDescription] [dbo].[Description_Small_type] NULL
) ON [PRIMARY]
GO
ALTER TABLE [ppe].[RecommendCriteriaActionType] ADD CONSTRAINT [PK_RecommendCriteriaActionType] PRIMARY KEY NONCLUSTERED  ([RecommendCriteriaActionTypeKey]) ON [PRIMARY]
GO
