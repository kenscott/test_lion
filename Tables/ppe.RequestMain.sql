CREATE TABLE [ppe].[RequestMain]
(
[RequestKey] [dbo].[Key_Normal_type] NOT NULL IDENTITY(1, 1),
[AccountKey] [dbo].[Key_Normal_type] NOT NULL,
[ItemGroup3Key] [dbo].[Key_Normal_type] NOT NULL,
[ItemKey] [dbo].[Key_Normal_type] NOT NULL,
[InvoiceLineGroup1Key] [dbo].[Key_Normal_type] NOT NULL,
[TypeKey] [smallint] NOT NULL,
[StatusKey] [smallint] NOT NULL,
[CreatedByWebUserKey] [dbo].[Key_Normal_type] NOT NULL,
[ModifiedByWebUserKey] [dbo].[Key_Normal_type] NOT NULL,
[DefaultDiscount] [decimal] (16, 4) NULL,
[CurrentSPGDiscount1] [decimal] (16, 4) NULL,
[CurrentSPGDiscount2] [decimal] (16, 4) NULL,
[CurrentCompoundDiscount] AS (case  when [CurrentSPGDiscount1] IS NULL AND [CurrentSPGDiscount2] IS NULL then NULL else (1.0)-(((1.0)-isnull([CurrentSPGDiscount1],(0)))*((1.0)-isnull([CurrentSPGDiscount2],(0)))) end),
[ProposedSPGDiscount1] [decimal] (16, 4) NULL,
[ProposedSPGDiscount2] [decimal] (16, 4) NULL,
[ProposedCompoundDiscount] AS (case  when [ProposedSPGDiscount1] IS NULL AND [ProposedSPGDiscount2] IS NULL then NULL else (1.0)-(((1.0)-isnull([ProposedSPGDiscount1],(0)))*((1.0)-isnull([ProposedSPGDiscount2],(0)))) end),
[NewSPGDiscount1] [decimal] (16, 4) NULL,
[NewSPGDiscount2] [decimal] (16, 4) NULL,
[NewCompoundDiscount] AS (case  when [NewSPGDiscount1] IS NULL AND [NewSPGDiscount2] IS NULL then NULL else (1.0)-(((1.0)-isnull([NewSPGDiscount1],(0)))*((1.0)-isnull([NewSPGDiscount2],(0)))) end),
[NewSPGFromDate] [date] NULL,
[ProposedExceptionDiscount] [decimal] (16, 4) NULL,
[ProposedExceptionFixedPrice] [money] NULL,
[ProposedExceptionStartDate] [date] NULL,
[ProposedExceptionExpiryDate] [date] NULL,
[OwningBrandCode] [dbo].[Description_Small_type] NOT NULL,
[LastChangeInitials] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LastChangeDateTime] [date] NULL,
[LastChangeBranch] [dbo].[UDVarchar_type] NOT NULL,
[StrategicItemFlag] [bit] NULL CONSTRAINT [DF__RequestMa__Strat__62707447] DEFAULT ((0)),
[FixedPriceFlag] [bit] NULL CONSTRAINT [DF__RequestMa__Fixed__63649880] DEFAULT ((0)),
[KVIFlag] [bit] NULL CONSTRAINT [DF__RequestMa__KVIFl__6458BCB9] DEFAULT ((0)),
[KVIOverrideFlag] [bit] NULL CONSTRAINT [DF__RequestMa__KVIOv__654CE0F2] DEFAULT ((0)),
[LastSaleDate] [date] NULL,
[LastPriceDerivation] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LastItemDiscount] [decimal] (16, 4) NULL,
[LastItemPrice] [money] NULL,
[LastItemGPP] [decimal] (16, 4) NULL,
[AdjustedLastItemPrice] [money] NULL,
[AdjustedLastItemGPP] [decimal] (16, 4) NULL,
[RecommendedPrice] [money] NULL,
[RecommendedPriceLogic] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[GreenPrice] [money] NULL,
[AmberPrice] [money] NULL,
[RedPrice] [money] NULL,
[TradePrice] [money] NULL,
[GreenGPP] [decimal] (16, 4) NULL,
[AmberGPP] [decimal] (16, 4) NULL,
[RedGPP] [decimal] (16, 4) NULL,
[TradeGPP] [decimal] (16, 4) NULL,
[LastPriceLevel] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SalesSize] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PriceApproach] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PriceBandLevel] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CurrentCost] [money] NULL,
[Total12MonthSales] [money] NULL,
[Total12MonthQty] [int] NULL,
[RecommendedImpact] [money] NULL,
[AcceptedImpact] [money] NULL,
[Version] [int] NOT NULL CONSTRAINT [DF__RequestMa__Versi__7FCBCD04] DEFAULT ((1)),
[CurrentExceptionDiscount] [decimal] (16, 4) NULL,
[CurrentExceptionFixedPrice] [money] NULL,
[CurrentExceptionStartDate] [date] NULL,
[CurrentExceptionExpiryDate] [date] NULL,
[NewExceptionDiscount] [decimal] (16, 4) NULL,
[NewExceptionFixedPrice] [money] NULL,
[NewExceptionStartDate] [date] NULL,
[NewExceptionExpiryDate] [date] NULL,
[BatchKey] [int] NULL,
[ModificationDate] [datetime] NULL,
[CreationDate] [datetime] NULL CONSTRAINT [DF__RequestMa__Creat__6849492E] DEFAULT (getdate()),
[NotificationEmailSentIndicator] [bit] NULL CONSTRAINT [DF__RequestMa__Notif__489B93AB] DEFAULT ((1)),
[LastActionKey] [smallint] NULL,
[NewDeleteTerm] [bit] NULL CONSTRAINT [DF__RequestMa__Delet__13F2C142] DEFAULT ((0)),
[ProposedDeleteTerm] [bit] NULL CONSTRAINT [DF__RequestMa__Propo__15DB09B4] DEFAULT ((0)),
[OwnerWebUserKey] [int] NULL,
[PriorOwnerWebUserKey] [int] NULL,
[NewTermType] [varchar] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RealityResponseCode] [varchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ReviewType] [int] NULL CONSTRAINT [DF__RequestMa__Revie__1E063B61] DEFAULT ((0)),
[LastDiscount] [decimal] (16, 4) NULL,
[SourceKey] [tinyint] NULL,
[SPGSkippedSales] [money] NULL
) ON [PRIMARY]
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		José A. Castaños
-- Create date: 25-Jun-2015
-- Description:	Insert RequestLog Entries
-- =============================================
CREATE TRIGGER [ppe].[tI_RequestMain] 
   ON  [ppe].[RequestMain] 
   AFTER INSERT
AS 
BEGIN
	SET NOCOUNT ON;

	-- JIRA LIO-620: Add a RequestLog record during request creation.
	INSERT INTO [ppe].[RequestLog]
				([WebUserKey]
				,[RequestKey]
				,[OldStatusKey]
				,[NewStatusKey]
				,[RequestType]
				,[LogTime]
				,[RequestAction]
				,[EmailSent]
				,[OldOwnerWebUserKey]
				,[NewOwnerWebUserKey])
		SELECT  CreatedByWebUserKey
				,RequestKey
				,(SELECT StatusKey FROM ppe.RequestStatus WHERE StatusPosition=0) -- New
				,StatusKey
				,'1'  -- Term
				,GETDATE()
				,'CREATED'
				,0 -- Email needs to be sent out for this action
				,[CreatedByWebUserKey]
				,[OwnerWebUserKey]
			FROM inserted
END
GO
DISABLE TRIGGER [ppe].[tI_RequestMain] ON [ppe].[RequestMain]
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		José A. Castaños / Daniel Block
-- Create date: 4-Jun-2015
-- History:		2-Nov-2015:	Update login as per revised JIRA LPF-496
-- Description:	Collision and Supression Logic (JIRA LIO-496)
-- changed:		12-May-2016: per LPF-2031
-- =============================================
CREATE TRIGGER [ppe].[tIII_RequestMain] 
   ON  [ppe].[RequestMain] 
   INSTEAD OF INSERT
AS 
BEGIN
	SET NOCOUNT ON;

	DECLARE @ClosedStatusKey int
	        
	SELECT @ClosedStatusKey = StatusKey
	  FROM ppe.RequestStatus
	 WHERE StatusName = 'Closed'

	BEGIN TRANSACTION

	UPDATE rm SET rm.StatusKey = @ClosedStatusKey
	FROM ppe.RequestMain rm 
	INNER JOIN ppe.RequestStatus rs on rm.StatusKey = rs.StatusKey
	INNER JOIN inserted i 
		ON rm.AccountKey = i.AccountKey 
		AND	rm.ItemGroup3Key = i.ItemGroup3Key 
		AND	rm.ItemKey = i.ItemKey
	WHERE rs.StatusRollup in ('New', 'Pending') 

		INSERT INTO [ppe].[RequestMain]
				   ([AccountKey]
				   ,[ItemGroup3Key]
				   ,[ItemKey]
				   ,[InvoiceLineGroup1Key]
				   ,[TypeKey]
				   ,[StatusKey]
				   ,[CreatedByWebUserKey]
				   ,[ModifiedByWebUserKey]
				   ,[DefaultDiscount]
				   ,[CurrentSPGDiscount1]
				   ,[CurrentSPGDiscount2]
				   ,[ProposedSPGDiscount1]
				   ,[ProposedSPGDiscount2]
				   ,[NewSPGDiscount1]
				   ,[NewSPGDiscount2]
				   ,[NewSPGFromDate]
				   ,[ProposedExceptionDiscount]
				   ,[ProposedExceptionFixedPrice]
				   ,[ProposedExceptionStartDate]
				   ,[ProposedExceptionExpiryDate]
				   ,[OwningBrandCode]
				   ,[LastChangeInitials]
				   ,[LastChangeDateTime]
				   ,[LastChangeBranch]
				   ,[StrategicItemFlag]
				   ,[FixedPriceFlag]
				   ,[KVIFlag]
				   ,[KVIOverrideFlag]
				   ,[LastSaleDate]
				   ,[LastPriceDerivation]
				   ,[LastItemDiscount]
				   ,[LastItemPrice]
				   ,[LastItemGPP]
				   ,[AdjustedLastItemPrice]
				   ,[AdjustedLastItemGPP]
				   ,[RecommendedPrice]
				   ,[RecommendedPriceLogic]
				   ,[GreenPrice]
				   ,[AmberPrice]
				   ,[RedPrice]
				   ,[TradePrice]
				   ,[GreenGPP]
				   ,[AmberGPP]
				   ,[RedGPP]
				   ,[TradeGPP]
				   ,[LastPriceLevel]
				   ,[SalesSize]
				   ,[PriceApproach]
				   ,[PriceBandLevel]
				   ,[CurrentCost]
				   ,[Total12MonthSales]
				   ,[Total12MonthQty]
				   ,[RecommendedImpact]
				   ,[AcceptedImpact]
				   ,[Version]
				   ,[CurrentExceptionDiscount]
				   ,[CurrentExceptionFixedPrice]
				   ,[CurrentExceptionStartDate]
				   ,[CurrentExceptionExpiryDate]
				   ,[NewExceptionDiscount]
				   ,[NewExceptionFixedPrice]
				   ,[NewExceptionStartDate]
				   ,[NewExceptionExpiryDate]
				   ,[BatchKey]
				   ,[ModificationDate]
				   ,[CreationDate]
				   ,[OwnerWebUserKey]
				   ,[PriorOwnerWebUserKey]
				   ,[NewTermType]
				   ,[NewDeleteTerm]
				   ,[ProposedDeleteTerm]
				   ,[LastActionKey]
				   ,[ReviewType]
				   ,[LastDiscount]
				   ,SourceKey
				   ,SPGSkippedSales)
			 SELECT [AccountKey]
				   ,[ItemGroup3Key]
				   ,[ItemKey]
				   ,[InvoiceLineGroup1Key]
				   ,[TypeKey]
				   ,[StatusKey]
				   ,[CreatedByWebUserKey]
				   ,[ModifiedByWebUserKey]
				   ,[DefaultDiscount]
				   ,[CurrentSPGDiscount1]
				   ,[CurrentSPGDiscount2]
				   ,[ProposedSPGDiscount1]
				   ,[ProposedSPGDiscount2]
				   ,[NewSPGDiscount1]
				   ,[NewSPGDiscount2]
				   ,[NewSPGFromDate]
				   ,[ProposedExceptionDiscount]
				   ,[ProposedExceptionFixedPrice]
				   ,[ProposedExceptionStartDate]
				   ,[ProposedExceptionExpiryDate]
				   ,[OwningBrandCode]
				   ,[LastChangeInitials]
				   ,[LastChangeDateTime]
				   ,[LastChangeBranch]
				   ,[StrategicItemFlag]
				   ,[FixedPriceFlag]
				   ,[KVIFlag]
				   ,[KVIOverrideFlag]
				   ,[LastSaleDate]
				   ,[LastPriceDerivation]
				   ,[LastItemDiscount]
				   ,[LastItemPrice]
				   ,[LastItemGPP]
				   ,[AdjustedLastItemPrice]
				   ,[AdjustedLastItemGPP]
				   ,[RecommendedPrice]
				   ,[RecommendedPriceLogic]
				   ,[GreenPrice]
				   ,[AmberPrice]
				   ,[RedPrice]
				   ,[TradePrice]
				   ,[GreenGPP]
				   ,[AmberGPP]
				   ,[RedGPP]
				   ,[TradeGPP]
				   ,[LastPriceLevel]
				   ,[SalesSize]
				   ,[PriceApproach]
				   ,[PriceBandLevel]
				   ,[CurrentCost]
				   ,[Total12MonthSales]
				   ,[Total12MonthQty]
				   ,[RecommendedImpact]
				   ,[AcceptedImpact]
				   ,[Version]
				   ,[CurrentExceptionDiscount]
				   ,[CurrentExceptionFixedPrice]
				   ,[CurrentExceptionStartDate]
				   ,[CurrentExceptionExpiryDate]
				   ,[NewExceptionDiscount]
				   ,[NewExceptionFixedPrice]
				   ,[NewExceptionStartDate]
				   ,[NewExceptionExpiryDate]
				   ,[BatchKey]
				   ,[ModificationDate]
				   ,[CreationDate]
				   ,[OwnerWebUserKey]
				   ,[PriorOwnerWebUserKey]
				   ,[NewTermType]
				   ,[NewDeleteTerm]
				   ,[ProposedDeleteTerm]
				   ,[LastActionKey]
				   ,[ReviewType]
				   ,[LastDiscount]
				   ,SourceKey
				   ,SPGSkippedSales
			  FROM Inserted
	
	COMMIT TRANSACTION
END

GO
DISABLE TRIGGER [ppe].[tIII_RequestMain] ON [ppe].[RequestMain]
GO
ALTER TABLE [ppe].[RequestMain] ADD CONSTRAINT [PK_RequestMain] PRIMARY KEY CLUSTERED  ([RequestKey]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [i_statusacct] ON [ppe].[RequestMain] ([AccountKey], [StatusKey]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [i_batchstatus] ON [ppe].[RequestMain] ([BatchKey], [StatusKey]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [I_acctItem] ON [ppe].[RequestMain] ([ItemKey], [AccountKey]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [i_ownerwebuserkey] ON [ppe].[RequestMain] ([OwnerWebUserKey]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [i_statuskey] ON [ppe].[RequestMain] ([StatusKey]) ON [PRIMARY]
GO
ALTER TABLE [ppe].[RequestMain] ADD CONSTRAINT [FK_LastAction] FOREIGN KEY ([LastActionKey]) REFERENCES [ppe].[RequestAction] ([ActionKey])
GO
ALTER TABLE [ppe].[RequestMain] ADD CONSTRAINT [FK_RM_AccountKey] FOREIGN KEY ([AccountKey]) REFERENCES [dbo].[DimAccount] ([AccountKey])
GO
ALTER TABLE [ppe].[RequestMain] ADD CONSTRAINT [FK_RM_ILGroup1Key] FOREIGN KEY ([InvoiceLineGroup1Key]) REFERENCES [dbo].[DimInvoiceLineGroup1] ([InvoiceLineGroup1Key])
GO
ALTER TABLE [ppe].[RequestMain] ADD CONSTRAINT [FK_RM_ItemGroup3Key] FOREIGN KEY ([ItemGroup3Key]) REFERENCES [dbo].[DimItemGroup3] ([ItemGroup3Key])
GO
ALTER TABLE [ppe].[RequestMain] ADD CONSTRAINT [FK_RM_ItemKey] FOREIGN KEY ([ItemKey]) REFERENCES [dbo].[DimItem] ([ItemKey])
GO
ALTER TABLE [ppe].[RequestMain] ADD CONSTRAINT [FK_RM_StatusKey] FOREIGN KEY ([StatusKey]) REFERENCES [ppe].[RequestStatus] ([StatusKey])
GO
ALTER TABLE [ppe].[RequestMain] ADD CONSTRAINT [FK_RM_TypeKey] FOREIGN KEY ([TypeKey]) REFERENCES [ppe].[RequestType] ([TypeKey])
GO
ALTER TABLE [ppe].[RequestMain] ADD CONSTRAINT [FK_RM_WU_Change] FOREIGN KEY ([ModifiedByWebUserKey]) REFERENCES [dbo].[WebUser] ([WebUserKey])
GO
ALTER TABLE [ppe].[RequestMain] ADD CONSTRAINT [FK_RM_WU_Create] FOREIGN KEY ([CreatedByWebUserKey]) REFERENCES [dbo].[WebUser] ([WebUserKey])
GO
