CREATE TABLE [dbo].[WebUserRole]
(
[WebUserRoleKey] [dbo].[Key_Normal_type] NOT NULL IDENTITY(22, 1),
[WebUserKey] [dbo].[Key_Normal_type] NOT NULL,
[WebRoleKey] [dbo].[Key_Normal_type] NOT NULL,
[Version] [dbo].[Int_Type] NOT NULL CONSTRAINT [DF__WebUserRo__Versi__733D6F9B] DEFAULT ((0)),
[CreationDate] [dbo].[DateTime_Type] NOT NULL CONSTRAINT [DF__WebUserRo__Creat__743193D4] DEFAULT (getdate()),
[ModificationDate] [dbo].[DateTime_Type] NOT NULL CONSTRAINT [DF__WebUserRo__Modif__7525B80D] DEFAULT (getdate()),
[CreationUser] [dbo].[Description_Normal_type] NULL,
[ModificationUser] [dbo].[Description_Normal_type] NULL,
[ModificationUserAddress] [dbo].[Description_Normal_type] NULL,
[CreationUserAddress] [dbo].[Description_Normal_type] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[WebUserRole] ADD CONSTRAINT [PK_WebUserRole] PRIMARY KEY CLUSTERED  ([WebUserRoleKey]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [I_WebUserRole_2] ON [dbo].[WebUserRole] ([WebRoleKey]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [I_WebUserRole_1] ON [dbo].[WebUserRole] ([WebUserKey]) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [UI_WebUserRole_1] ON [dbo].[WebUserRole] ([WebUserKey], [WebRoleKey]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[WebUserRole] ADD CONSTRAINT [FK_WebUserRole_1] FOREIGN KEY ([WebUserKey]) REFERENCES [dbo].[WebUser] ([WebUserKey])
GO
ALTER TABLE [dbo].[WebUserRole] ADD CONSTRAINT [FK_WebUserRole_2] FOREIGN KEY ([WebRoleKey]) REFERENCES [dbo].[WebRole] ([WebRoleKey])
GO
