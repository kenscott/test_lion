CREATE TABLE [dbo].[PlaybookDataPointGroupItemBands]
(
[PlaybookDataPointGroupKey] [dbo].[Key_Normal_type] NOT NULL,
[ItemKey] [dbo].[Key_Normal_type] NOT NULL,
[ItemBand] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ItemQuantityBand] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[PlaybookDataPointGroupItemBands] ADD CONSTRAINT [PK_PlaybookDataPointGroupItemBands] PRIMARY KEY CLUSTERED  ([PlaybookDataPointGroupKey], [ItemKey]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [I_PlaybookDataPointGroupItemBands_1] ON [dbo].[PlaybookDataPointGroupItemBands] ([ItemKey], [ItemQuantityBand], [PlaybookDataPointGroupKey]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[PlaybookDataPointGroupItemBands] ADD CONSTRAINT [FK_PlaybookDataPointGroupItemBands_1] FOREIGN KEY ([PlaybookDataPointGroupKey]) REFERENCES [dbo].[PlaybookDataPointGroup] ([PlaybookDataPointGroupKey])
GO
ALTER TABLE [dbo].[PlaybookDataPointGroupItemBands] ADD CONSTRAINT [FK_PlaybookDataPointGroupItemBands_3] FOREIGN KEY ([ItemKey]) REFERENCES [dbo].[DimItem] ([ItemKey])
GO
