CREATE TABLE [lion].[RegionalCoordinatorLocation]
(
[RegionalCoordinatorLocationKey] [dbo].[Key_Normal_type] NOT NULL IDENTITY(1, 1),
[WebUserKey] [dbo].[Key_Normal_type] NULL,
[ContractLocationLevelKey] [dbo].[Key_Normal_type] NULL,
[LocationKey] [dbo].[Key_Normal_type] NULL,
[CreationDate] [datetime] NULL CONSTRAINT [DF_RegionalUserLocation_creationDate] DEFAULT (getdate()),
[CreatedByWebUserKey] [dbo].[Key_Normal_type] NULL
) ON [PRIMARY]
GO
ALTER TABLE [lion].[RegionalCoordinatorLocation] ADD CONSTRAINT [CK_RegionalCoordinatorLocation_contractLocationLevelKey] CHECK (([ContractLocationLevelKey]=(5) OR [ContractLocationLevelKey]=(4) OR [ContractLocationLevelKey]=(3) OR [ContractLocationLevelKey]=(2) OR [ContractLocationLevelKey]=(1)))
GO
ALTER TABLE [lion].[RegionalCoordinatorLocation] ADD CONSTRAINT [PK_RegionalUserLocation] PRIMARY KEY CLUSTERED  ([RegionalCoordinatorLocationKey]) ON [PRIMARY]
GO
ALTER TABLE [lion].[RegionalCoordinatorLocation] ADD CONSTRAINT [FK_RegionalCoordinatorLocation_WebUser] FOREIGN KEY ([WebUserKey]) REFERENCES [dbo].[WebUser] ([WebUserKey])
GO
ALTER TABLE [lion].[RegionalCoordinatorLocation] ADD CONSTRAINT [FK_RegionalCoordinatorLocation_WebUser1] FOREIGN KEY ([CreatedByWebUserKey]) REFERENCES [dbo].[WebUser] ([WebUserKey])
GO
