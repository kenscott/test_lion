CREATE TABLE [dbo].[WebUserDelegateLog]
(
[WebUserDelegateLogKey] [int] NOT NULL IDENTITY(1, 1),
[DelegatorWebUserKey] [int] NOT NULL,
[DelegateWebUserKey] [int] NOT NULL,
[ActorWebUserKey] [int] NOT NULL,
[Action] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[StartDate] [datetime] NOT NULL CONSTRAINT [DF_WebUserDelegateLog_StartDate] DEFAULT (getdate()),
[EndDate] [datetime] NOT NULL CONSTRAINT [DF_WebUserDelegateLog_EndDate] DEFAULT (((12)-(31))-(9999)),
[EmailStatus] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF_WebUserDelegateLog_EmailStatus] DEFAULT ('N'),
[CreationDate] [datetime] NOT NULL CONSTRAINT [DF_WebUserDelegateLog_CreationDate] DEFAULT (getdate())
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[WebUserDelegateLog] ADD CONSTRAINT [PK_WebUserDelegateLog] PRIMARY KEY CLUSTERED  ([WebUserDelegateLogKey]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[WebUserDelegateLog] ADD CONSTRAINT [FK_WebUserDelegateLog_key1] FOREIGN KEY ([DelegatorWebUserKey]) REFERENCES [dbo].[WebUser] ([WebUserKey])
GO
ALTER TABLE [dbo].[WebUserDelegateLog] ADD CONSTRAINT [FK_WebUserDelegateLog_key2] FOREIGN KEY ([DelegateWebUserKey]) REFERENCES [dbo].[WebUser] ([WebUserKey])
GO
ALTER TABLE [dbo].[WebUserDelegateLog] ADD CONSTRAINT [FK_WebUserDelegateLog_key3] FOREIGN KEY ([ActorWebUserKey]) REFERENCES [dbo].[WebUser] ([WebUserKey])
GO
