CREATE TABLE [lion].[IncomingTerms]
(
[ID] [int] NOT NULL IDENTITY(1, 1),
[Filename] [nvarchar] (4000) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[RowType] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RecID] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AccountKey] [int] NULL,
[ItemKey] [int] NULL,
[InvoiceLineGroup1Key] [int] NULL,
[AccountNumber] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AccountName] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Area] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LLSPG Description] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Product Description] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LLSPG] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Product Code] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LLSPG / Product Description] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Prod KVI] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[KVI Override] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Default Discount] [decimal] (19, 8) NULL,
[Last Terms Change] [datetime] NULL,
[SPG Disc 1] [decimal] (19, 8) NULL,
[SPG Disc 2] [decimal] (19, 8) NULL,
[SPG Compounded Discount] [decimal] (19, 8) NULL,
[Exception Discount] [decimal] (19, 8) NULL,
[Exception Fixed Price] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Fixed Price Start Date] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Fixed Price Expiry Date] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Total Quantity] [decimal] (19, 8) NULL,
[Product Sales] [decimal] (19, 8) NULL,
[LLSPG Sales] [decimal] (19, 8) NULL,
[Last Sale Date] [datetime] NULL,
[New SPG Discount 1] [decimal] (19, 8) NULL,
[New SPG Discount 2] [decimal] (19, 8) NULL,
[New Compounded Discount] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[New Exception Fixed Price] [decimal] (19, 8) NULL,
[New Exception Discount] [decimal] (19, 8) NULL,
[New Exception Price Expiry Date] [datetime] NULL,
[Brand] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Exported] [bit] NOT NULL CONSTRAINT [DF_IncomingTerms_Exported] DEFAULT ((0)),
[ExportedDateTime] [datetime] NULL,
[Recommended Impact] [decimal] (19, 8) NULL,
[Accepted Impact] [decimal] (19, 8) NULL,
[WolcenRequestID] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreationDate] [datetime] NOT NULL CONSTRAINT [DF_IncomingTerms_CreationDate] DEFAULT (getdate()),
[Tracked] [bit] NOT NULL CONSTRAINT [DF_IncomingTerms_Tracked] DEFAULT ((0))
) ON [PRIMARY]
GO
ALTER TABLE [lion].[IncomingTerms] ADD CONSTRAINT [PK_IncomingTerms] PRIMARY KEY CLUSTERED  ([ID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_20] ON [lion].[IncomingTerms] ([RowType], [Exported], [New Exception Discount]) INCLUDE ([AccountKey], [Filename], [ID], [LLSPG]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_21] ON [lion].[IncomingTerms] ([RowType], [Exported], [New SPG Discount 1]) INCLUDE ([AccountKey], [Filename], [LLSPG], [LLSPG Description]) ON [PRIMARY]
GO
