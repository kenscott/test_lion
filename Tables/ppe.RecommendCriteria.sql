CREATE TABLE [ppe].[RecommendCriteria]
(
[RecommendCriteriaKey] [dbo].[Key_Normal_type] NOT NULL IDENTITY(1, 1),
[RecommendKey] [dbo].[Key_Normal_type] NOT NULL,
[SequenceNumber] [int] NOT NULL,
[RecommendCriterionKey] [dbo].[Key_Normal_type] NOT NULL,
[StrategicIndicatorSelection] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CriterionDate] [datetime] NULL,
[CriterionPercent] [dbo].[Percent_Type] NULL,
[CriterionMonth] [smallint] NULL,
[CriterionAmount] [decimal] (19, 8) NULL,
[RecommendCriteriaActionTypeKey] [dbo].[Key_Normal_type] NOT NULL,
[ActionPercent] [dbo].[Percent_Type] NULL,
[RecommendCriteriaCapTypeKey] [dbo].[Key_Normal_type] NULL,
[CapPercent] [dbo].[Percent_Type] NULL,
[CreatedByWebUserKey] [dbo].[Key_Normal_type] NOT NULL,
[CreationDate] [datetime] NOT NULL CONSTRAINT [DF_RecommendCriteria_CrateDate] DEFAULT (getdate())
) ON [PRIMARY]
GO
ALTER TABLE [ppe].[RecommendCriteria] ADD CONSTRAINT [PK_RecommendCriteria] PRIMARY KEY NONCLUSTERED  ([RecommendCriteriaKey]) ON [PRIMARY]
GO
ALTER TABLE [ppe].[RecommendCriteria] ADD CONSTRAINT [FK_RecommendCriteria_ActionType] FOREIGN KEY ([RecommendCriteriaActionTypeKey]) REFERENCES [ppe].[RecommendCriteriaActionType] ([RecommendCriteriaActionTypeKey])
GO
ALTER TABLE [ppe].[RecommendCriteria] ADD CONSTRAINT [FK_RecommendCriteria_CapType] FOREIGN KEY ([RecommendCriteriaCapTypeKey]) REFERENCES [ppe].[RecommendCriteriaCapType] ([RecommendCriteriaCapTypeKey])
GO
