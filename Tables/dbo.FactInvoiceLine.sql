CREATE TABLE [dbo].[FactInvoiceLine]
(
[AccountKey] [dbo].[Key_Normal_type] NOT NULL,
[AlternateAccountKey] [dbo].[Key_Normal_type] NOT NULL,
[ItemKey] [dbo].[Key_Normal_type] NOT NULL,
[VendorKey] [dbo].[Key_Normal_type] NOT NULL,
[AccountManagerKey] [dbo].[Key_Normal_type] NOT NULL,
[InvoiceDateDayKey] [dbo].[DateKey_type] NOT NULL,
[InvoiceDateMonthKey] [dbo].[DateKey_type] NOT NULL,
[InvoiceLineGroup1Key] [dbo].[Key_Normal_type] NOT NULL,
[InvoiceLineGroup2Key] [dbo].[Key_Normal_type] NOT NULL,
[InvoiceLineGroup3Key] [dbo].[Key_Normal_type] NOT NULL,
[InvoiceLineJunkKey] [dbo].[Key_Normal_type] NOT NULL CONSTRAINT [DF_FactInvoiceLine_InvoiceLineJunkKey] DEFAULT ((1)),
[AccountGroup1Key] [dbo].[Key_Normal_type] NOT NULL,
[AccountGroup2Key] [dbo].[Key_Normal_type] NOT NULL,
[AccountGroup3Key] [dbo].[Key_Normal_type] NOT NULL,
[ItemGroup1Key] [dbo].[Key_Normal_type] NOT NULL,
[ItemGroup2Key] [dbo].[Key_Normal_type] NOT NULL,
[ItemGroup3Key] [dbo].[Key_Normal_type] NOT NULL,
[InvoiceLineUniqueIdentifier] [int] NOT NULL,
[LinkedInvoiceLineUniqueIdentifier] [int] NULL,
[ClientInvoiceLineUniqueID] [int] NULL,
[ClientInvoiceID] [dbo].[Description_Small_type] NULL,
[ClientInvoiceLineID] [dbo].[Description_Small_type] NULL,
[TotalQuantity] [dbo].[Quantity_Normal_type] NOT NULL,
[TotalActualPrice] [dbo].[Money_Normal_Type] NOT NULL,
[TotalActualCost] [dbo].[Money_Normal_Type] NOT NULL,
[TotalActualRebate] [dbo].[Money_Normal_Type] NOT NULL,
[UnitListPrice] [dbo].[Money_Normal_Type] NOT NULL,
[Last12MonthsIndicator] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[RedPrice] [dbo].[Money_Normal_Type] NULL,
[AmberPrice] [dbo].[Money_Normal_Type] NULL,
[GreenPrice] [dbo].[Money_Normal_Type] NULL,
[BluePrice] [dbo].[Money_Normal_Type] NULL,
[TerminalEmulator] [dbo].[UDVarchar_Small_type] NULL,
[PriceGuidanceIndicator] [nvarchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[UDVarchar1] [dbo].[UDVarchar_Small_type] NULL,
[UDVarchar2] [dbo].[UDVarchar_Small_type] NULL,
[UDVarchar3] [dbo].[UDVarchar_Small_type] NULL,
[UDDecimal1] [dbo].[UDDecimal_type] NULL,
[UDDecimal2] [dbo].[UDDecimal_type] NULL,
[UDDecimal3] [dbo].[UDDecimal_type] NULL,
[ModificationDayKey] [dbo].[DateKey_type] NOT NULL,
[CreationDayKey] [dbo].[DateKey_type] NOT NULL,
[TotalNetQuantity] [dbo].[Quantity_Normal_type] NULL,
[TotalNetPrice] [dbo].[Money_Normal_Type] NULL,
[TotalNetCost] [dbo].[Money_Normal_Type] NULL,
[TotalNetDiscount] [dbo].[Money_Normal_Type] NULL,
[TotalSupplierCost] [dbo].[Money_Normal_Type] NULL,
[TotalNetSupplierCost] [dbo].[Money_Normal_Type] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[FactInvoiceLine] ADD CONSTRAINT [PK_FactInvoiceLine] PRIMARY KEY NONCLUSTERED  ([InvoiceLineUniqueIdentifier]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [I_FactInvoiceLine_10] ON [dbo].[FactInvoiceLine] ([AccountGroup1Key]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [I_FactInvoiceLine_11] ON [dbo].[FactInvoiceLine] ([AccountGroup2Key]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [I_FactInvoiceLine_12] ON [dbo].[FactInvoiceLine] ([AccountGroup3Key]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [I_FactInvoiceLine_33] ON [dbo].[FactInvoiceLine] ([AccountKey]) ON [PRIMARY]
GO
CREATE CLUSTERED INDEX [I_FactInvoiceLine_1] ON [dbo].[FactInvoiceLine] ([AccountKey], [ItemKey], [InvoiceLineGroup1Key], [InvoiceDateDayKey]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_Fil_3] ON [dbo].[FactInvoiceLine] ([AccountKey], [ItemKey], [InvoiceLineGroup1Key], [InvoiceDateDayKey] DESC, [TotalQuantity] DESC, [TotalActualPrice] DESC, [ClientInvoiceID] DESC, [InvoiceLineUniqueIdentifier] DESC, [TotalActualCost]) INCLUDE ([UDDecimal2]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [I_FactInvoiceLine_14] ON [dbo].[FactInvoiceLine] ([AccountManagerKey]) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [I_FactInvoiceLine_66] ON [dbo].[FactInvoiceLine] ([ClientInvoiceLineUniqueID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [I_FactInvoiceLine_9] ON [dbo].[FactInvoiceLine] ([InvoiceDateDayKey]) INCLUDE ([AccountGroup1Key], [AccountKey], [ItemKey], [TotalActualCost], [TotalActualPrice]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [I_FactInvoiceLine_4] ON [dbo].[FactInvoiceLine] ([InvoiceDateMonthKey]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [I_FactInvoiceLine_6] ON [dbo].[FactInvoiceLine] ([InvoiceLineGroup1Key]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [I_FactInvoiceLine_7] ON [dbo].[FactInvoiceLine] ([InvoiceLineGroup2Key]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [I_FactInvoiceLine_8] ON [dbo].[FactInvoiceLine] ([InvoiceLineGroup3Key]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [I_FactInvoiceLine_102] ON [dbo].[FactInvoiceLine] ([InvoiceLineUniqueIdentifier], [AccountKey], [ItemKey], [InvoiceLineGroup1Key], [InvoiceDateDayKey]) INCLUDE ([UDVarchar1]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [I_FactInvoiceLine_16] ON [dbo].[FactInvoiceLine] ([ItemGroup1Key]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [I_FactInvoiceLine_17] ON [dbo].[FactInvoiceLine] ([ItemGroup2Key]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [I_FactInvoiceLine_18] ON [dbo].[FactInvoiceLine] ([ItemGroup3Key]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [I_FactInvoiceLine_2] ON [dbo].[FactInvoiceLine] ([ItemKey]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [I_factInvoiceLine_101] ON [dbo].[FactInvoiceLine] ([Last12MonthsIndicator]) INCLUDE ([AccountKey], [InvoiceLineGroup1Key], [ItemKey], [TotalActualCost], [TotalActualPrice], [TotalQuantity], [UnitListPrice]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [I_FactInvoiceLine_103] ON [dbo].[FactInvoiceLine] ([LinkedInvoiceLineUniqueIdentifier]) INCLUDE ([InvoiceDateDayKey], [InvoiceLineUniqueIdentifier]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_Fil_2] ON [dbo].[FactInvoiceLine] ([TotalQuantity], [TotalActualPrice], [TotalActualCost]) INCLUDE ([AccountKey], [ClientInvoiceID], [InvoiceDateDayKey], [InvoiceLineGroup1Key], [InvoiceLineUniqueIdentifier], [ItemKey], [UDDecimal2], [UDVarchar1]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [I_FactInvoiceLine_104] ON [dbo].[FactInvoiceLine] ([UDVarchar3], [TotalQuantity]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [I_FactInvoiceLine_13] ON [dbo].[FactInvoiceLine] ([VendorKey]) ON [PRIMARY]
GO
CREATE STATISTICS [_dta_stat_52911260_1_8_25_23_24] ON [dbo].[FactInvoiceLine] ([AccountKey], [InvoiceLineGroup1Key], [TotalActualCost], [TotalQuantity], [TotalActualPrice])
GO
CREATE STATISTICS [_dta_stat_79339347_1_18_3_8] ON [dbo].[FactInvoiceLine] ([AccountKey], [InvoiceLineUniqueIdentifier], [ItemKey], [InvoiceLineGroup1Key])
GO
CREATE STATISTICS [_dta_stat_52911260_25_1_3_8_18_6_23] ON [dbo].[FactInvoiceLine] ([AccountKey], [ItemKey], [InvoiceLineGroup1Key], [InvoiceLineUniqueIdentifier], [InvoiceDateDayKey], [TotalQuantity], [TotalActualCost])
GO
CREATE STATISTICS [_dta_stat_52911260_1_3_8_25_23_24_6_21] ON [dbo].[FactInvoiceLine] ([InvoiceDateDayKey], [ClientInvoiceID], [AccountKey], [ItemKey], [InvoiceLineGroup1Key], [TotalActualCost], [TotalQuantity], [TotalActualPrice])
GO
CREATE STATISTICS [_dta_stat_79339347_6_18_1_3] ON [dbo].[FactInvoiceLine] ([InvoiceDateDayKey], [InvoiceLineUniqueIdentifier], [AccountKey], [ItemKey])
GO
CREATE STATISTICS [_dta_stat_79339347_8_1_3_6_18] ON [dbo].[FactInvoiceLine] ([InvoiceDateDayKey], [InvoiceLineUniqueIdentifier], [InvoiceLineGroup1Key], [AccountKey], [ItemKey])
GO
CREATE STATISTICS [_dta_stat_52911260_24_1_3_8_18_6] ON [dbo].[FactInvoiceLine] ([InvoiceDateDayKey], [TotalActualPrice], [AccountKey], [ItemKey], [InvoiceLineGroup1Key], [InvoiceLineUniqueIdentifier])
GO
CREATE STATISTICS [_dta_stat_52911260_8_25_23_24] ON [dbo].[FactInvoiceLine] ([InvoiceLineGroup1Key], [TotalActualCost], [TotalQuantity], [TotalActualPrice])
GO
CREATE STATISTICS [_dta_stat_79339347_18_8_1] ON [dbo].[FactInvoiceLine] ([InvoiceLineUniqueIdentifier], [InvoiceLineGroup1Key], [AccountKey])
GO
CREATE STATISTICS [_dta_stat_52911260_1_3_8_18_25_23] ON [dbo].[FactInvoiceLine] ([InvoiceLineUniqueIdentifier], [TotalActualCost], [TotalQuantity], [AccountKey], [ItemKey], [InvoiceLineGroup1Key])
GO
CREATE STATISTICS [_dta_stat_52911260_3_8_25_23_24_6] ON [dbo].[FactInvoiceLine] ([ItemKey], [InvoiceLineGroup1Key], [TotalActualCost], [TotalQuantity], [TotalActualPrice], [InvoiceDateDayKey])
GO
CREATE STATISTICS [_dta_stat_79339347_3_18] ON [dbo].[FactInvoiceLine] ([ItemKey], [InvoiceLineUniqueIdentifier])
GO
CREATE STATISTICS [_dta_stat_52911260_23_1_3_8_18] ON [dbo].[FactInvoiceLine] ([TotalQuantity], [AccountKey], [ItemKey], [InvoiceLineGroup1Key], [InvoiceLineUniqueIdentifier])
GO
CREATE STATISTICS [_dta_stat_52911260_23_24_25_8_1_3_18] ON [dbo].[FactInvoiceLine] ([TotalQuantity], [TotalActualPrice], [TotalActualCost], [InvoiceLineGroup1Key], [AccountKey], [ItemKey], [InvoiceLineUniqueIdentifier])
GO
ALTER TABLE [dbo].[FactInvoiceLine] ADD CONSTRAINT [FK_FactInvoiceLine_1] FOREIGN KEY ([AccountKey]) REFERENCES [dbo].[DimAccount] ([AccountKey])
GO
ALTER TABLE [dbo].[FactInvoiceLine] ADD CONSTRAINT [FK_FactInvoiceLine_10] FOREIGN KEY ([AccountGroup2Key]) REFERENCES [dbo].[DimAccountGroup2] ([AccountGroup2Key])
GO
ALTER TABLE [dbo].[FactInvoiceLine] ADD CONSTRAINT [FK_FactInvoiceLine_11] FOREIGN KEY ([AccountGroup3Key]) REFERENCES [dbo].[DimAccountGroup3] ([AccountGroup3Key])
GO
ALTER TABLE [dbo].[FactInvoiceLine] ADD CONSTRAINT [FK_FactInvoiceLine_12] FOREIGN KEY ([VendorKey]) REFERENCES [dbo].[DimVendor] ([VendorKey])
GO
ALTER TABLE [dbo].[FactInvoiceLine] ADD CONSTRAINT [FK_FactInvoiceLine_13] FOREIGN KEY ([ItemGroup1Key]) REFERENCES [dbo].[DimItemGroup1] ([ItemGroup1Key])
GO
ALTER TABLE [dbo].[FactInvoiceLine] ADD CONSTRAINT [FK_FactInvoiceLine_14] FOREIGN KEY ([ItemGroup2Key]) REFERENCES [dbo].[DimItemGroup2] ([ItemGroup2Key])
GO
ALTER TABLE [dbo].[FactInvoiceLine] ADD CONSTRAINT [FK_FactInvoiceLine_15] FOREIGN KEY ([ItemGroup3Key]) REFERENCES [dbo].[DimItemGroup3] ([ItemGroup3Key])
GO
ALTER TABLE [dbo].[FactInvoiceLine] ADD CONSTRAINT [FK_FactInvoiceLine_16] FOREIGN KEY ([AccountManagerKey]) REFERENCES [dbo].[DimAccountManager] ([AccountManagerKey])
GO
ALTER TABLE [dbo].[FactInvoiceLine] ADD CONSTRAINT [FK_FactInvoiceLine_19] FOREIGN KEY ([InvoiceLineJunkKey]) REFERENCES [dbo].[DimInvoiceLineJunk] ([InvoiceLineJunkKey])
GO
ALTER TABLE [dbo].[FactInvoiceLine] ADD CONSTRAINT [FK_FactInvoiceLine_2] FOREIGN KEY ([ItemKey]) REFERENCES [dbo].[DimItem] ([ItemKey])
GO
ALTER TABLE [dbo].[FactInvoiceLine] ADD CONSTRAINT [FK_FactInvoiceLine_20] FOREIGN KEY ([AlternateAccountKey]) REFERENCES [dbo].[DimAccount] ([AccountKey])
GO
ALTER TABLE [dbo].[FactInvoiceLine] ADD CONSTRAINT [FK_FactInvoiceLine_3] FOREIGN KEY ([InvoiceDateMonthKey]) REFERENCES [dbo].[DimMonth] ([MonthKey])
GO
ALTER TABLE [dbo].[FactInvoiceLine] ADD CONSTRAINT [FK_FactInvoiceLine_4] FOREIGN KEY ([InvoiceDateDayKey]) REFERENCES [dbo].[DimDay] ([DayKey])
GO
ALTER TABLE [dbo].[FactInvoiceLine] ADD CONSTRAINT [FK_FactInvoiceLine_6] FOREIGN KEY ([InvoiceLineGroup1Key]) REFERENCES [dbo].[DimInvoiceLineGroup1] ([InvoiceLineGroup1Key])
GO
ALTER TABLE [dbo].[FactInvoiceLine] ADD CONSTRAINT [FK_FactInvoiceLine_7] FOREIGN KEY ([InvoiceLineGroup2Key]) REFERENCES [dbo].[DimInvoiceLineGroup2] ([InvoiceLineGroup2Key])
GO
ALTER TABLE [dbo].[FactInvoiceLine] ADD CONSTRAINT [FK_FactInvoiceLine_8] FOREIGN KEY ([InvoiceLineGroup3Key]) REFERENCES [dbo].[DimInvoiceLineGroup3] ([InvoiceLineGroup3Key])
GO
ALTER TABLE [dbo].[FactInvoiceLine] ADD CONSTRAINT [FK_FactInvoiceLine_9] FOREIGN KEY ([AccountGroup1Key]) REFERENCES [dbo].[DimAccountGroup1] ([AccountGroup1Key])
GO
