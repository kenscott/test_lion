CREATE TABLE [dbo].[WebPermission]
(
[WebPermissionKey] [dbo].[Key_Normal_type] NOT NULL IDENTITY(10, 1),
[WebPermissionName] [dbo].[Description_Normal_type] NULL,
[WebPermissionDescription] [dbo].[Description_Normal_type] NOT NULL,
[CreationDate] [dbo].[DateTime_Type] NOT NULL CONSTRAINT [DF__WEbPermission__Creatio__6B9C4DD3] DEFAULT (getdate()),
[ModificationDate] [datetime] NOT NULL CONSTRAINT [DF_WebPermission_ModificationDate] DEFAULT (getdate())
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[WebPermission] ADD CONSTRAINT [PK_WebPermission] PRIMARY KEY CLUSTERED  ([WebPermissionKey]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [I_WebUserPermission_2] ON [dbo].[WebPermission] ([WebPermissionKey]) ON [PRIMARY]
GO
