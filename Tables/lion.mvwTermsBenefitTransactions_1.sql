CREATE TABLE [lion].[mvwTermsBenefitTransactions_1]
(
[recid] [bigint] NOT NULL IDENTITY(1, 1),
[DayKey] [dbo].[DateKey_type] NOT NULL,
[AccountKey] [dbo].[Key_Normal_type] NOT NULL,
[ItemKey] [dbo].[Key_Normal_type] NOT NULL,
[InvoiceLineGroup1Key] [dbo].[Key_Normal_type] NOT NULL,
[AccountGroup1Key] [dbo].[Key_Normal_type] NOT NULL,
[ItemGroup3Key] [dbo].[Key_Normal_type] NOT NULL,
[DeliveryDate] [dbo].[DateTime_Type] NULL,
[ChargeNote] [nvarchar] (4000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PriceDerivation] [dbo].[UDVarchar_Small_type] NULL,
[NetQuantity] [dbo].[Quantity_Normal_type] NOT NULL,
[NetSales] [dbo].[Money_Normal_Type] NOT NULL,
[NetCost] [dbo].[Money_Normal_Type] NOT NULL,
[GP] [decimal] (20, 8) NULL,
[GPP] [decimal] (38, 18) NULL,
[ActualDiscount] [decimal] (38, 6) NULL,
[TradePrice] [dbo].[Money_Normal_Type] NOT NULL,
[Contract Claims Indicator] [dbo].[UDVarchar_type] NOT NULL,
[Claim Amount] [dbo].[UDDecimal_type] NULL,
[ClientInvoiceLineUniqueID] [int] NULL,
[AccountingBrand] [dbo].[UDVarchar_Small_type] NULL,
[BranchBrand] [dbo].[UDVarchar_Small_type] NULL,
[YoY] [int] NOT NULL,
[h_QoQ] [int] NOT NULL,
[h_MoM] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [lion].[mvwTermsBenefitTransactions_1] ADD CONSTRAINT [PK_mvwTermsBenefitTransactions_1] PRIMARY KEY CLUSTERED  ([recid]) ON [PRIMARY]
GO
