CREATE TABLE [lion].[TermsBenefits]
(
[ClientInvoiceLineUniqueID] [int] NOT NULL,
[TermsTrackerID] [int] NOT NULL,
[Actual Benefit] [decimal] (38, 6) NULL,
[Potential Benefit] [decimal] (38, 6) NULL,
[RuleNumber] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [lion].[TermsBenefits] ADD CONSTRAINT [PK_TermsBenefits] PRIMARY KEY CLUSTERED  ([ClientInvoiceLineUniqueID], [TermsTrackerID]) ON [PRIMARY]
GO
