CREATE TABLE [dbo].[ProposalPrice]
(
[ProposalPriceKey] [dbo].[Key_Normal_type] NOT NULL IDENTITY(1, 1),
[PlaybookPriceProposalKey] [dbo].[Key_Normal_type] NULL,
[PlaybookMarginErosionProposalKey] [dbo].[Key_Normal_type] NULL,
[AcceptanceCodeKey] [dbo].[Key_Small_type] NOT NULL,
[UserAcceptanceCodeKey] [dbo].[Key_Small_type] NOT NULL,
[DefaultAcceptanceCodeKey] [dbo].[Key_Small_type] NULL,
[ProjectKey] [dbo].[Key_Normal_type] NOT NULL,
[AccountKey] [dbo].[Key_Normal_type] NOT NULL,
[ItemKey] [dbo].[Key_Normal_type] NOT NULL,
[InvoiceLineGroup1Key] [dbo].[Key_Normal_type] NOT NULL,
[UoM] [nvarchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NewUOM] [nvarchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[UOMConversionFactor] [decimal] (38, 16) NULL,
[ProposedPrice] [dbo].[Money_Normal_Type] NOT NULL,
[NewPrice] [dbo].[Money_Normal_Type] NOT NULL,
[TotalActual12MonthCost] [dbo].[Money_Normal_Type] NOT NULL,
[TotalActual12MonthPrice] [dbo].[Money_Normal_Type] NOT NULL,
[TotalActual12MonthQuantity] [dbo].[Quantity_Normal_type] NOT NULL,
[TotalActual12MonthFrequency] [dbo].[Quantity_Normal_type] NOT NULL,
[LastItemCost] [dbo].[Money_Normal_Type] NOT NULL,
[LastItemPrice] [dbo].[Money_Normal_Type] NOT NULL,
[CurrentItemAISCost] [dbo].[Money_Normal_Type] NULL,
[CurrentItemStandardCost] [dbo].[Money_Normal_Type] NULL,
[FutureItemCost] [dbo].[Money_Normal_Type] NULL,
[Forecasted12MonthQuantity] [dbo].[Quantity_Normal_type] NULL,
[TotalForecasted12MonthCost_LastCost] [dbo].[Money_Normal_Type] NULL,
[TotalForecasted12MonthPrice_LastPrice] [dbo].[Money_Normal_Type] NULL,
[TotalForecasted12MonthPrice_ProposedPrice] [dbo].[Money_Normal_Type] NULL,
[TotalForecasted12MonthPrice_NewPrice] [dbo].[Money_Normal_Type] NULL,
[TotalForecasted12MonthCost_RelevantCost] [dbo].[Money_Normal_Type] NULL,
[Version] [dbo].[Int_Type] NOT NULL CONSTRAINT [DF_ProposalPrice_Version] DEFAULT ((0)),
[CreationDate] [dbo].[DateTime_Type] NOT NULL CONSTRAINT [DF_ProposalPrice_CreationDate] DEFAULT (getdate()),
[ModificationDate] [dbo].[DateTime_Type] NOT NULL CONSTRAINT [DF_ProposalPrice_ModificationDate] DEFAULT (getdate()),
[CreationUser] [dbo].[Description_Normal_type] NULL,
[ModificationUser] [dbo].[Description_Normal_type] NULL,
[PlaybookDataPointGroupKey] [dbo].[Key_Normal_type] NULL,
[AccountManagerKey] [dbo].[Key_Normal_type] NULL,
[ModificationUserAddress] [dbo].[Description_Normal_type] NULL,
[CreationUserAddress] [dbo].[Description_Normal_type] NULL,
[AcceptanceStatusDate] [datetime] NULL,
[LastSaleDayKey] [dbo].[Key_Small_type] NULL,
[LastInvoiceLineGroup2Key] [dbo].[Key_Normal_type] NOT NULL,
[EffectiveDate] [dbo].[DateTime_Type] NOT NULL,
[SuppressionDate] [dbo].[DateTime_Type] NOT NULL,
[ReasonCodeKey] [dbo].[Key_Small_type] NULL,
[UDDecimal1] [dbo].[UDDecimal_type] NULL,
[UDDecimal2] [dbo].[UDDecimal_type] NULL,
[UDVarchar1] [dbo].[Description_Small_type] NULL,
[UDVarchar2] [dbo].[Description_Small_type] NULL,
[OptimalPrice] [dbo].[Money_Normal_Type] NULL,
[ExpirationDate] [datetime] NULL,
[CostEffectiveDate] [datetime] NULL,
[NewGPP] [decimal] (38, 8) NULL,
[NewDiscount] [decimal] (38, 8) NULL,
[ListPriceInBaseUoM] [dbo].[Money_Normal_Type] NULL,
[FutureEstimatedListPrice] [dbo].[Money_Normal_Type] NULL,
[ListPricingUoMConvFactor] [decimal] (19, 8) NULL,
[ListPricingUoM] [nvarchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RPBPlaybookPricingGroupKey] [dbo].[Key_Normal_type] NULL,
[ContractNo] [dbo].[Description_Small_type] NULL,
[RuleNo] [dbo].[Description_Small_type] NULL,
[ContractEndDate] [dbo].[Description_Small_type] NULL,
[MaxStatus] [nvarchar] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL CONSTRAINT [DF_ProposalPrice_MaxSubmissionStatus] DEFAULT ('P'),
[CommittedByWebUserKey] [dbo].[Key_Normal_type] NULL,
[CommittedDate] [datetime] NULL,
[MaxStatusChangeWebUserKey] [dbo].[Key_Normal_type] NULL,
[MaxStatusChangeDate] [datetime] NULL,
[MaxPrice] [dbo].[Money_Normal_Type] NULL,
[MaxGPP] [decimal] (38, 8) NULL,
[MaxDiscount] [decimal] (38, 8) NULL,
[MaxPricingRule] [nvarchar] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MaxEffectiveDate] [datetime] NULL,
[MaxExpirationDate] [datetime] NULL,
[CurrentItemAISPrice] [dbo].[Money_Normal_Type] NULL,
[UDVarchar3] [dbo].[Description_Small_type] NULL,
[UDVarchar4] [dbo].[Description_Small_type] NULL,
[RebateEndDate] [datetime] NULL,
[RebateId] [dbo].[Description_Small_type] NULL,
[DropShipIndicator] [nvarchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FTPLogKey] [dbo].[Key_Normal_type] NULL,
[UDDecimal3] [dbo].[UDDecimal_type] NULL,
[ListPriceInListUoM] [dbo].[Money_Normal_Type] NULL,
[ExcludeFromGroupIndicator] [nvarchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL CONSTRAINT [DF_ProposalPrice_ExcludeFromGroup] DEFAULT ('N'),
[ProposalPriceGroupKey] [dbo].[Key_Normal_type] NULL,
[ProposalPriceBuyGroupKey] [dbo].[Key_Normal_type] NULL,
[Comment] [dbo].[Description_Big_type] NULL,
[BandSequenceNumber] [int] NULL,
[OldItemKey] [dbo].[Key_Normal_type] NULL,
[StockStatus] [nvarchar] (5) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RelevantCost] [dbo].[Money_Normal_Type] NULL,
[RecentItemCost] [dbo].[Money_Normal_Type] NULL,
[RecentItemPrice] [dbo].[Money_Normal_Type] NULL,
[RecentDiscount] [dbo].[Percent_Type] NULL,
[RecentPriceDate] [datetime] NULL,
[ApprovalStatus] [nvarchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ApprovalStatusWebUserKey] [dbo].[Key_Normal_type] NULL,
[ApprovalStatusDate] [dbo].[DateTime_Type] NULL,
[ApprovalPrice] [dbo].[Money_Normal_Type] NULL,
[ApprovalGPP] [decimal] (38, 8) NULL,
[FloorGPP] [dbo].[Percent_Type] NULL,
[TargetGPP] [dbo].[Percent_Type] NULL,
[ProposalType] [tinyint] NULL,
[FutureRebateId] [dbo].[Description_Small_type] NULL,
[FutureRebateEndDate] [datetime] NULL,
[NotificationBatchKey] [dbo].[Key_Normal_type] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ProposalPrice] ADD CONSTRAINT [PK_ProposalPrice] PRIMARY KEY NONCLUSTERED  ([ProposalPriceKey]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [I_ProposalPrice_14] ON [dbo].[ProposalPrice] ([AcceptanceCodeKey]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [I_ProposalPrice_1] ON [dbo].[ProposalPrice] ([AccountKey]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [I_ProposalPrice_19] ON [dbo].[ProposalPrice] ([AccountKey], [AccountManagerKey], [AcceptanceCodeKey], [CommittedDate], [UDVarchar2], [LastItemPrice], [LastItemCost], [ProposedPrice], [NewPrice], [UDDecimal2], [FutureItemCost], [UOMConversionFactor]) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [I_ProposalPrice_10] ON [dbo].[ProposalPrice] ([AccountKey], [ItemKey], [InvoiceLineGroup1Key], [CostEffectiveDate], [FutureItemCost], [ProjectKey]) INCLUDE ([SuppressionDate]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [I_ProposalPrice_8] ON [dbo].[ProposalPrice] ([AccountManagerKey]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [I_ProposalPrice_18] ON [dbo].[ProposalPrice] ([AccountManagerKey], [AcceptanceCodeKey], [ProposedPrice], [TotalActual12MonthQuantity], [LastItemCost], [LastItemPrice], [FutureItemCost], [UDVarchar2], [UDDecimal2], [ExpirationDate]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [i_AISCreationData] ON [dbo].[ProposalPrice] ([CreationDate], [AccountKey], [ItemKey], [InvoiceLineGroup1Key]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [I_ProposalPrice_3] ON [dbo].[ProposalPrice] ([InvoiceLineGroup1Key]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [I_ProposalPrice_2] ON [dbo].[ProposalPrice] ([ItemKey]) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [I_ProposalPrice_22] ON [dbo].[ProposalPrice] ([MaxStatus], [AcceptanceCodeKey], [ProjectKey], [MaxStatusChangeDate], [AccountKey], [AccountManagerKey], [ItemKey], [CommittedByWebUserKey], [InvoiceLineGroup1Key], [MaxPricingRule], [RebateEndDate], [ContractNo], [ModificationUser]) INCLUDE ([BandSequenceNumber], [Comment], [CommittedDate], [ContractEndDate], [CostEffectiveDate], [CurrentItemAISCost], [DropShipIndicator], [EffectiveDate], [ExpirationDate], [FloorGPP], [ListPriceInListUoM], [MaxDiscount], [MaxEffectiveDate], [MaxExpirationDate], [MaxGPP], [MaxPrice], [MaxStatusChangeWebUserKey], [NewDiscount], [NewGPP], [NewPrice], [NewUOM], [ProposalPriceKey], [RebateId], [RelevantCost], [RPBPlaybookPricingGroupKey], [RuleNo], [TargetGPP], [UDDecimal1], [UDVarchar1], [UoM], [UOMConversionFactor]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [I_ProposalPrice_20] ON [dbo].[ProposalPrice] ([NotificationBatchKey]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [I_ProposalPrice_7] ON [dbo].[ProposalPrice] ([PlaybookDataPointGroupKey]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [I_ProposalPrice_5] ON [dbo].[ProposalPrice] ([PlaybookMarginErosionProposalKey]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [I_ProposalPrice_4] ON [dbo].[ProposalPrice] ([PlaybookPriceProposalKey]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [I_ProposalPrice_6] ON [dbo].[ProposalPrice] ([ProjectKey]) ON [PRIMARY]
GO
CREATE UNIQUE CLUSTERED INDEX [UCI_ProposalPrice_1] ON [dbo].[ProposalPrice] ([ProjectKey], [AccountKey], [ItemKey], [InvoiceLineGroup1Key]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [I_ProposalPrice_17] ON [dbo].[ProposalPrice] ([ProposalPriceBuyGroupKey]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [I_ProposalPrice_16] ON [dbo].[ProposalPrice] ([ProposalPriceGroupKey]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [I_ProposalPrice_15] ON [dbo].[ProposalPrice] ([ProposalPriceGroupKey], [AcceptanceCodeKey], [CostEffectiveDate], [EffectiveDate], [ExpirationDate]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [i_ProposalPrice_11] ON [dbo].[ProposalPrice] ([ProposalPriceKey], [ProjectKey], [AccountKey], [ItemKey], [InvoiceLineGroup1Key], [AcceptanceCodeKey], [ProposedPrice], [NewPrice], [LastItemCost], [LastItemPrice], [AcceptanceStatusDate], [CreationDate]) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [I_ProposalPrice_21] ON [dbo].[ProposalPrice] ([RebateEndDate], [ContractNo], [MaxStatus], [ModificationUser], [MaxStatusChangeDate], [AccountKey], [AccountManagerKey], [ItemKey], [InvoiceLineGroup1Key], [MaxPricingRule], [ProjectKey], [CommittedByWebUserKey], [AcceptanceCodeKey]) INCLUDE ([BandSequenceNumber], [Comment], [CommittedDate], [ContractEndDate], [CostEffectiveDate], [CurrentItemAISCost], [DropShipIndicator], [EffectiveDate], [ExpirationDate], [FloorGPP], [ListPriceInListUoM], [MaxDiscount], [MaxEffectiveDate], [MaxExpirationDate], [MaxGPP], [MaxPrice], [MaxStatusChangeWebUserKey], [NewDiscount], [NewGPP], [NewPrice], [NewUOM], [ProposalPriceKey], [RebateId], [RelevantCost], [RPBPlaybookPricingGroupKey], [RuleNo], [TargetGPP], [UDDecimal1], [UDVarchar1], [UoM], [UOMConversionFactor]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ProposalPrice] ADD CONSTRAINT [FK_ProposalPrice_1] FOREIGN KEY ([AccountKey]) REFERENCES [dbo].[DimAccount] ([AccountKey])
GO
ALTER TABLE [dbo].[ProposalPrice] ADD CONSTRAINT [FK_ProposalPrice_2] FOREIGN KEY ([ItemKey]) REFERENCES [dbo].[DimItem] ([ItemKey])
GO
ALTER TABLE [dbo].[ProposalPrice] ADD CONSTRAINT [FK_ProposalPrice_3] FOREIGN KEY ([InvoiceLineGroup1Key]) REFERENCES [dbo].[DimInvoiceLineGroup1] ([InvoiceLineGroup1Key])
GO
ALTER TABLE [dbo].[ProposalPrice] ADD CONSTRAINT [FK_ProposalPrice_4] FOREIGN KEY ([AccountManagerKey]) REFERENCES [dbo].[DimAccountManager] ([AccountManagerKey])
GO
ALTER TABLE [dbo].[ProposalPrice] ADD CONSTRAINT [FK_ProposalPrice_5] FOREIGN KEY ([ProjectKey]) REFERENCES [dbo].[PlaybookProject] ([ProjectKey])
GO
ALTER TABLE [dbo].[ProposalPrice] ADD CONSTRAINT [FK_ProposalPrice_6] FOREIGN KEY ([AcceptanceCodeKey]) REFERENCES [dbo].[ATKAcceptanceCode] ([AcceptanceCodeKey])
GO
EXEC sp_addextendedproperty N'MS_Description', N'xxx', 'SCHEMA', N'dbo', 'TABLE', N'ProposalPrice', 'COLUMN', N'ListPriceInBaseUoM'
GO
EXEC sp_addextendedproperty N'MS_Description', N'xxx', 'SCHEMA', N'dbo', 'TABLE', N'ProposalPrice', 'COLUMN', N'ListPricingUoM'
GO
EXEC sp_addextendedproperty N'MS_Description', N'xxx', 'SCHEMA', N'dbo', 'TABLE', N'ProposalPrice', 'COLUMN', N'ListPricingUoMConvFactor'
GO
