CREATE TABLE [lion].[CentralTermsArchive]
(
[ID] [int] NOT NULL IDENTITY(1, 1),
[RowType] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[OutgoingRecID] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IncomingRecID] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AccountKey] [int] NULL,
[LLSPG] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ItemKey] [int] NULL,
[InvoiceLineGroup1Key] [int] NULL,
[Prod KVI] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[KVI Override] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Default Discount] [decimal] (19, 8) NULL,
[OrigSPGDisc1] [decimal] (19, 8) NULL,
[OrigSPGDisc2] [decimal] (19, 8) NULL,
[OrigCmpDisc] [decimal] (38, 14) NULL,
[RecSPGDisc1] [decimal] (19, 8) NULL,
[RecSPGDisc2] [decimal] (19, 8) NULL,
[RecCmpDisc] [decimal] (38, 14) NULL,
[AccSPGDisc1] [decimal] (19, 8) NULL,
[AccSPGDisc2] [decimal] (19, 8) NULL,
[AccCmpDisc] [decimal] (38, 14) NULL,
[OrigExceptionDisc] [decimal] (19, 8) NULL,
[RecExceptionDisc] [decimal] (19, 8) NULL,
[AccExceptionDisc] [decimal] (19, 8) NULL,
[OrigFixedPrice] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RecExceptionFixedPrice] [decimal] (19, 8) NULL,
[AccExceptionFixedPrice] [decimal] (19, 8) NULL,
[ProjectedImpact] [decimal] (19, 8) NULL,
[EffectiveDate] [datetime] NULL,
[RecImpact] [decimal] (19, 8) NULL,
[AccImpact] [decimal] (19, 8) NULL,
[Active] [bit] NOT NULL
) ON [PRIMARY]
GO
