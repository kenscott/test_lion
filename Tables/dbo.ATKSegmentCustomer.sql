CREATE TABLE [dbo].[ATKSegmentCustomer]
(
[SegmentCustomerKey] [dbo].[Key_Normal_type] NOT NULL IDENTITY(1, 1) NOT FOR REPLICATION,
[SegmentFactorPlaybookKey] [dbo].[Key_Normal_type] NOT NULL,
[SegmentSequenceNumber] [dbo].[Int_Type] NULL,
[SegmentLabel] [dbo].[Description_Small_type] NOT NULL,
[SegmentLowerBound] [dbo].[Money_Normal_Type] NOT NULL,
[SegmentUpperBound] [dbo].[Money_Normal_Type] NOT NULL,
[SegmentType] [dbo].[Description_Small_type] NOT NULL,
[Version] [dbo].[Int_Type] NOT NULL CONSTRAINT [DF__ATKSegmen__Versi__5438FA37] DEFAULT ((0)),
[CreationDate] [dbo].[DateTime_Type] NOT NULL CONSTRAINT [DF__ATKSegmen__Creat__6EECF073] DEFAULT (getdate()),
[ModificationDate] [dbo].[DateTime_Type] NOT NULL CONSTRAINT [DF__ATKSegmen__Modif__09A0E6AF] DEFAULT (getdate()),
[CreationUser] [dbo].[Description_Normal_type] NULL,
[ModificationUser] [dbo].[Description_Normal_type] NULL,
[ModificationUserAddress] [dbo].[Description_Normal_type] NULL,
[CreationUserAddress] [dbo].[Description_Normal_type] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ATKSegmentCustomer] ADD CONSTRAINT [PK_ATKSegment] PRIMARY KEY CLUSTERED  ([SegmentCustomerKey]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ATKSegmentCustomer] ADD CONSTRAINT [FK_ATKSegmentCustomer_1] FOREIGN KEY ([SegmentFactorPlaybookKey]) REFERENCES [dbo].[ATKSegmentFactorPlaybook] ([SegmentFactorPlaybookKey])
GO
