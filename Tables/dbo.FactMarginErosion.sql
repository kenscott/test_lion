CREATE TABLE [dbo].[FactMarginErosion]
(
[AccountKey] [dbo].[Key_Normal_type] NOT NULL,
[ItemKey] [dbo].[Key_Normal_type] NOT NULL,
[InvoiceLineGroup1Key] [dbo].[Key_Normal_type] NOT NULL,
[PriceDerivation] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[LastQuantity] [dbo].[Quantity_Normal_type] NULL,
[LastUnitPrice] [dbo].[Money_Normal_Type] NULL,
[LastUnitCost] [dbo].[Money_Normal_Type] NULL,
[LastSaleDayKey] [int] NULL,
[LastSaleDate] [datetime] NULL,
[FirstSaleDayKey] [int] NULL,
[Total12MonthQuantity] [dbo].[Quantity_Normal_type] NULL,
[Total12MonthSales] [dbo].[Money_Normal_Type] NULL,
[Total12MonthCost] [dbo].[Money_Normal_Type] NULL,
[Total12MonthInvoiceLineCount] [dbo].[Quantity_Normal_type] NULL,
[SustainedGPP] [dbo].[Quantity_Normal_type] NULL,
[UnitPriceAtSustained] [dbo].[Quantity_Normal_type] NULL,
[UnitCostAtSustained] [dbo].[Quantity_Normal_type] NULL,
[MEProposedPrice] [dbo].[Quantity_Normal_type] NULL,
[MEImpact] [dbo].[Quantity_Normal_type] NULL,
[PLSIndicator] [dbo].[Description_Small_type] NULL,
[TermPyramidCode] [dbo].[UDVarchar_type] NULL,
[TermLLSPGCode] [dbo].[UDVarchar_type] NULL,
[TermExceptionProduct] [dbo].[UDVarchar_type] NULL,
[Usualdiscount1] [decimal] (38, 8) NULL,
[Usualdiscount2] [decimal] (38, 8) NULL,
[ExceptionDiscount] [decimal] (38, 8) NULL,
[ExceptionFixedPrice] [decimal] (38, 8) NULL,
[ExceptionFromDate] [dbo].[UDVarchar_type] NULL,
[ExceptionToDate] [dbo].[UDVarchar_type] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[FactMarginErosion] ADD CONSTRAINT [PK_FactMarginErosion] PRIMARY KEY CLUSTERED  ([AccountKey], [ItemKey], [InvoiceLineGroup1Key], [PriceDerivation]) ON [PRIMARY]
GO
