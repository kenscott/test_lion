CREATE TABLE [dbo].[FactAccountItemGroup3]
(
[AccountItemGroup3Key] [int] NOT NULL IDENTITY(1, 1),
[AccountKey] [dbo].[Key_Normal_type] NOT NULL,
[ItemGroup3Key] [dbo].[Key_Normal_type] NOT NULL,
[ContractClaimsPercent] [dbo].[UDDecimal_type] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[FactAccountItemGroup3] ADD CONSTRAINT [PK_FactAccountItemGroup3] PRIMARY KEY NONCLUSTERED  ([AccountItemGroup3Key]) ON [PRIMARY]
GO
CREATE UNIQUE CLUSTERED INDEX [I_FactAccountItemGroup3_1] ON [dbo].[FactAccountItemGroup3] ([AccountKey], [ItemGroup3Key]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[FactAccountItemGroup3] ADD CONSTRAINT [FK_F_AccountItemGroup3_AccountKey] FOREIGN KEY ([AccountKey]) REFERENCES [dbo].[DimAccount] ([AccountKey])
GO
ALTER TABLE [dbo].[FactAccountItemGroup3] ADD CONSTRAINT [FK_F_AccountItemGroup3_ItemGroup3Key] FOREIGN KEY ([ItemGroup3Key]) REFERENCES [dbo].[DimItemGroup3] ([ItemGroup3Key])
GO
