CREATE TABLE [lion].[StagingAccountOwner]
(
[AccountNumber] [nvarchar] (4000) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[AccountName] [nvarchar] (4000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PostCode] [nvarchar] (4000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NationalOwner] [nvarchar] (4000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NationalOwnerEmail] [nvarchar] (4000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PlumbOwner] [nvarchar] (4000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PlumbOwnerEmail] [nvarchar] (4000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PartsOwner] [nvarchar] (4000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PartsOwnerEmail] [nvarchar] (4000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DrainOwner] [nvarchar] (4000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DrainOwnerEmail] [nvarchar] (4000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PipeOwner] [nvarchar] (4000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PipeOwnerEmail] [nvarchar] (4000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ClimateOwner] [nvarchar] (4000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ClimateOwnerEmail] [nvarchar] (4000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BrandOwnerBurdens] [nvarchar] (4000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BrandOwnerBurdensEmail] [nvarchar] (4000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FusionOwner] [nvarchar] (4000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FusionOwnerEmail] [nvarchar] (4000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[UPSOwner] [nvarchar] (4000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[UPSOwnerEmail] [nvarchar] (4000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RowIndex] [int] NOT NULL,
[Message] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF_StagingAccountOwner_Message] DEFAULT ('')
) ON [PRIMARY]
GO
ALTER TABLE [lion].[StagingAccountOwner] ADD CONSTRAINT [PK_AccountOwner] PRIMARY KEY CLUSTERED  ([AccountNumber]) ON [PRIMARY]
GO
