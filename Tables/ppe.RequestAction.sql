CREATE TABLE [ppe].[RequestAction]
(
[ActionKey] [smallint] NOT NULL IDENTITY(1, 1),
[ActionAbbr] [varchar] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ActionName] [varchar] (40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ActionDescription] [varchar] (400) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [ppe].[RequestAction] ADD CONSTRAINT [PK_RequestAction] PRIMARY KEY CLUSTERED  ([ActionKey]) ON [PRIMARY]
GO
