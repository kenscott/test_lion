CREATE TABLE [dbo].[PlaybookPricingGroupPriceBand]
(
[PlaybookPricingGroupPriceBandKey] [dbo].[Key_Normal_type] NOT NULL IDENTITY(1, 1),
[PlaybookPricingGroupKey] [dbo].[Key_Normal_type] NOT NULL,
[LowerBandQuantity] [dbo].[Quantity_Normal_type] NOT NULL,
[UpperBandQuantity] [dbo].[Quantity_Normal_type] NOT NULL,
[PDPGPP] [dbo].[Percent_Type] NOT NULL,
[PDPDiscountPercent] [dbo].[Percent_Type] NULL,
[MemberRank] [int] NOT NULL,
[MemberRankDiscount] [int] NULL,
[RulingMemberCount] [int] NOT NULL,
[BandSequenceNumber] [int] NOT NULL,
[PlaybookPricingGroupQuantityBandKey] [dbo].[Key_Normal_type] NOT NULL,
[PricingRuleSequenceNumber] [tinyint] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[PlaybookPricingGroupPriceBand] ADD CONSTRAINT [PK_PlaybookPricingGroupPriceBandKey] PRIMARY KEY NONCLUSTERED  ([PlaybookPricingGroupPriceBandKey]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [I_PlaybookPricingGroupPriceBand_5] ON [dbo].[PlaybookPricingGroupPriceBand] ([PlaybookPricingGroupKey]) INCLUDE ([PDPGPP]) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [UI_PlaybookPricingGroupPriceBand_2] ON [dbo].[PlaybookPricingGroupPriceBand] ([PlaybookPricingGroupKey], [BandSequenceNumber], [MemberRank]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [UI_PlaybookPricingGroupPriceBand_4] ON [dbo].[PlaybookPricingGroupPriceBand] ([PlaybookPricingGroupKey], [BandSequenceNumber], [MemberRankDiscount]) ON [PRIMARY]
GO
CREATE UNIQUE CLUSTERED INDEX [UCI_PlaybookPricingGroupPriceBand_1] ON [dbo].[PlaybookPricingGroupPriceBand] ([PlaybookPricingGroupKey], [LowerBandQuantity], [UpperBandQuantity], [MemberRank], [RulingMemberCount]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [UI_PlaybookPricingGroupPriceBand_3] ON [dbo].[PlaybookPricingGroupPriceBand] ([PlaybookPricingGroupKey], [LowerBandQuantity], [UpperBandQuantity], [MemberRankDiscount], [RulingMemberCount]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[PlaybookPricingGroupPriceBand] ADD CONSTRAINT [FK_PlaybookPricingGroupPriceBand_2] FOREIGN KEY ([PlaybookPricingGroupKey]) REFERENCES [dbo].[PlaybookPricingGroup] ([PlaybookPricingGroupKey])
GO
