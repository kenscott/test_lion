CREATE TABLE [dbo].[ItemUOMConversion]
(
[ItemKey] [int] NOT NULL,
[BuyingConvFact] [decimal] (19, 3) NOT NULL,
[CostingConvFactor] [decimal] (19, 3) NOT NULL,
[PricingConvFact] [decimal] (19, 3) NOT NULL,
[SellingConvFactor] [decimal] (19, 3) NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ItemUOMConversion] ADD CONSTRAINT [PK_ItemUOMConversion] PRIMARY KEY CLUSTERED  ([ItemKey]) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [IU_1] ON [dbo].[ItemUOMConversion] ([ItemKey]) INCLUDE ([PricingConvFact], [SellingConvFactor]) ON [PRIMARY]
GO
