CREATE TABLE [dbo].[DimSegmentBrand]
(
[SegmentBrandKey] [int] NOT NULL IDENTITY(1, 1),
[SegmentBrand] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Segment] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[DimSegmentBrand] ADD CONSTRAINT [PK_DimSegmentBrand] PRIMARY KEY CLUSTERED  ([SegmentBrandKey]) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [I_U_DimSegmentBrand_1] ON [dbo].[DimSegmentBrand] ([SegmentBrand], [Segment]) ON [PRIMARY]
GO
