CREATE TABLE [dbo].[ProposalPriceCollision]
(
[ProposalPriceCollisionKey] [dbo].[Key_Normal_type] NOT NULL IDENTITY(1, 1),
[ProposalPriceKey_Collision] [dbo].[Key_Normal_type] NULL,
[PlaybookPriceProposalKey] [dbo].[Key_Normal_type] NULL,
[PlaybookMarginErosionProposalKey] [dbo].[Key_Normal_type] NULL,
[AcceptanceCodeKey] [dbo].[Key_Small_type] NOT NULL,
[UserAcceptanceCodeKey] [dbo].[Key_Small_type] NOT NULL,
[DefaultAcceptanceCodeKey] [dbo].[Key_Small_type] NULL,
[ProjectKey] [dbo].[Key_Normal_type] NOT NULL,
[AccountKey] [dbo].[Key_Normal_type] NOT NULL,
[ItemKey] [dbo].[Key_Normal_type] NOT NULL,
[InvoiceLineGroup1Key] [dbo].[Key_Normal_type] NOT NULL,
[ProposedPrice] [dbo].[Money_Normal_Type] NOT NULL,
[NewPrice] [dbo].[Money_Normal_Type] NOT NULL,
[TotalActual12MonthCost] [dbo].[Money_Normal_Type] NOT NULL,
[TotalActual12MonthPrice] [dbo].[Money_Normal_Type] NOT NULL,
[TotalActual12MonthQuantity] [dbo].[Quantity_Normal_type] NOT NULL,
[TotalActual12MonthFrequency] [dbo].[Quantity_Normal_type] NOT NULL,
[LastItemCost] [dbo].[Money_Normal_Type] NOT NULL,
[LastItemPrice] [dbo].[Money_Normal_Type] NOT NULL,
[Forecasted12MonthQuantity] [dbo].[Quantity_Normal_type] NOT NULL,
[TotalForecasted12MonthCost_LastCost] [dbo].[Money_Normal_Type] NOT NULL,
[TotalForecasted12MonthPrice_LastPrice] [dbo].[Money_Normal_Type] NOT NULL,
[TotalForecasted12MonthPrice_ProposedPrice] [dbo].[Money_Normal_Type] NOT NULL,
[TotalForecasted12MonthPrice_NewPrice] [dbo].[Money_Normal_Type] NOT NULL,
[Version] [dbo].[Int_Type] NOT NULL CONSTRAINT [DF_ProposalPriceCollision_Version] DEFAULT ((0)),
[CreationDate] [dbo].[DateTime_Type] NOT NULL CONSTRAINT [DF_ProposalPriceCollision_CreationDate] DEFAULT (getdate()),
[ModificationDate] [dbo].[DateTime_Type] NOT NULL CONSTRAINT [DF_ProposalPriceCollision_ModificationDate] DEFAULT (getdate()),
[CreationUser] [dbo].[Description_Normal_type] NULL,
[ModificationUser] [dbo].[Description_Normal_type] NULL,
[PlaybookDataPointGroupKey] [dbo].[Key_Normal_type] NULL,
[AccountManagerKey] [dbo].[Key_Normal_type] NULL,
[ModificationUserAddress] [dbo].[Description_Normal_type] NULL,
[CreationUserAddress] [dbo].[Description_Normal_type] NULL,
[AcceptanceStatusDate] [datetime] NULL,
[LastSaleDayKey] [dbo].[Key_Small_type] NULL,
[LastInvoiceLineGroup2Key] [dbo].[Key_Normal_type] NOT NULL,
[EffectiveDate] [dbo].[DateTime_Type] NOT NULL,
[SuppressionDate] [dbo].[DateTime_Type] NOT NULL,
[ReasonCodeKey] [dbo].[Key_Small_type] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ProposalPriceCollision] ADD CONSTRAINT [PK_ProposalPriceCollision] PRIMARY KEY NONCLUSTERED  ([ProposalPriceCollisionKey]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [I_ProposalPriceCollision_1] ON [dbo].[ProposalPriceCollision] ([ProposalPriceKey_Collision]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ProposalPriceCollision] ADD CONSTRAINT [FK_ProposalPriceCollision_1] FOREIGN KEY ([AccountKey]) REFERENCES [dbo].[DimAccount] ([AccountKey])
GO
ALTER TABLE [dbo].[ProposalPriceCollision] ADD CONSTRAINT [FK_ProposalPriceCollision_10] FOREIGN KEY ([PlaybookPriceProposalKey]) REFERENCES [dbo].[PlaybookPriceProposal] ([PlaybookPriceProposalKey])
GO
ALTER TABLE [dbo].[ProposalPriceCollision] ADD CONSTRAINT [FK_ProposalPriceCollision_11] FOREIGN KEY ([PlaybookDataPointGroupKey]) REFERENCES [dbo].[PlaybookDataPointGroup] ([PlaybookDataPointGroupKey])
GO
ALTER TABLE [dbo].[ProposalPriceCollision] ADD CONSTRAINT [FK_ProposalPriceCollision_12] FOREIGN KEY ([AccountManagerKey]) REFERENCES [dbo].[DimAccountManager] ([AccountManagerKey])
GO
ALTER TABLE [dbo].[ProposalPriceCollision] ADD CONSTRAINT [FK_ProposalPriceCollision_2] FOREIGN KEY ([ItemKey]) REFERENCES [dbo].[DimItem] ([ItemKey])
GO
ALTER TABLE [dbo].[ProposalPriceCollision] ADD CONSTRAINT [FK_ProposalPriceCollision_3] FOREIGN KEY ([InvoiceLineGroup1Key]) REFERENCES [dbo].[DimInvoiceLineGroup1] ([InvoiceLineGroup1Key])
GO
ALTER TABLE [dbo].[ProposalPriceCollision] ADD CONSTRAINT [FK_ProposalPriceCollision_4] FOREIGN KEY ([ProjectKey]) REFERENCES [dbo].[PlaybookProject] ([ProjectKey])
GO
ALTER TABLE [dbo].[ProposalPriceCollision] ADD CONSTRAINT [FK_ProposalPriceCollision_5] FOREIGN KEY ([DefaultAcceptanceCodeKey]) REFERENCES [dbo].[ATKAcceptanceCode] ([AcceptanceCodeKey])
GO
ALTER TABLE [dbo].[ProposalPriceCollision] ADD CONSTRAINT [FK_ProposalPriceCollision_6] FOREIGN KEY ([ProposalPriceKey_Collision]) REFERENCES [dbo].[ProposalPrice] ([ProposalPriceKey])
GO
ALTER TABLE [dbo].[ProposalPriceCollision] ADD CONSTRAINT [FK_ProposalPriceCollision_7] FOREIGN KEY ([AcceptanceCodeKey]) REFERENCES [dbo].[ATKAcceptanceCode] ([AcceptanceCodeKey])
GO
ALTER TABLE [dbo].[ProposalPriceCollision] ADD CONSTRAINT [FK_ProposalPriceCollision_8] FOREIGN KEY ([UserAcceptanceCodeKey]) REFERENCES [dbo].[ATKAcceptanceCode] ([AcceptanceCodeKey])
GO
ALTER TABLE [dbo].[ProposalPriceCollision] ADD CONSTRAINT [FK_ProposalPriceCollision_9] FOREIGN KEY ([ReasonCodeKey]) REFERENCES [dbo].[ProposalReasonCode] ([ReasonCodeKey])
GO
