CREATE TABLE [dbo].[DimDay]
(
[DayKey] [dbo].[Key_Small_type] NOT NULL IDENTITY(1, 1),
[MonthKey] [dbo].[Key_Small_type] NOT NULL,
[SQLDate] [dbo].[DateTime_Type] NULL,
[CalendarDay] [dbo].[Description_Small_type] NOT NULL,
[CalendarDayNumberinMonth] [dbo].[Int_255_type] NULL,
[CalendarDayNumberinQuarter] [dbo].[Int_255_type] NULL,
[CalendarDayNumberinWeek] [dbo].[Int_255_type] NULL,
[CalendarDayNumberinYear] [dbo].[Int_32767_type] NULL,
[CalendarWeek] [dbo].[Description_Small_type] NULL,
[CalendarWeekEndingDate] [dbo].[Description_Small_type] NULL,
[CalendarWeekNumber] [dbo].[Int_255_type] NULL,
[DayinWeek] [dbo].[Description_Small_type] NULL,
[DayNumber] [dbo].[Int_255_type] NULL,
[FiscalDay] [dbo].[Description_Small_type] NULL,
[FiscalDayNumberinMonth] [dbo].[Int_255_type] NULL,
[FiscalDayNumberinQuarter] [dbo].[Int_255_type] NULL,
[FiscalDayNumberinWeek] [dbo].[Int_255_type] NULL,
[FiscalDayNumberinYear] [dbo].[Int_255_type] NULL,
[FiscalWeek] [dbo].[Description_Small_type] NULL,
[FiscalWeekEndingDate] [dbo].[Description_Small_type] NULL,
[FiscalWeekNumber] [dbo].[Int_255_type] NULL,
[FiscalWeekNumberinYear] [dbo].[Int_255_type] NULL,
[Holiday] [dbo].[Description_Small_type] NULL,
[HolidayIndicator] [dbo].[Description_Small_type] NULL,
[LastDayinCalendarMonthIndicator] [dbo].[Description_Small_type] NULL,
[LastDayinCalendarQuarterIndicator] [dbo].[Description_Small_type] NULL,
[LastDayinCalendarWeekIndicator] [dbo].[Description_Small_type] NULL,
[LastDayinCalendarYearIndicator] [dbo].[Description_Small_type] NULL,
[LastDayinFiscalMonthIndicator] [dbo].[Description_Small_type] NULL,
[LastDayinFiscalQuarterIndicator] [dbo].[Description_Small_type] NULL,
[LastDayinFiscalWeekIndicator] [dbo].[Description_Small_type] NULL,
[LongCalendarDay] [dbo].[Description_Small_type] NULL,
[MajorEvent] [dbo].[Description_Small_type] NULL,
[SellingSeason] [dbo].[Description_Small_type] NULL,
[WeekdayIndicator] [dbo].[Description_Small_type] NULL,
[CognosParameterDate] [dbo].[Description_Small_type] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[DimDay] ADD CONSTRAINT [PK_DimDay] PRIMARY KEY CLUSTERED  ([DayKey]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [I_DimDay_2] ON [dbo].[DimDay] ([CalendarDay]) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [UI_DimDay_2] ON [dbo].[DimDay] ([DayKey]) INCLUDE ([SQLDate]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [I_DimDay_1] ON [dbo].[DimDay] ([MonthKey]) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [UI_DimDay_1] ON [dbo].[DimDay] ([SQLDate]) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [UI_DimDay_3] ON [dbo].[DimDay] ([SQLDate]) INCLUDE ([DayKey]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[DimDay] ADD CONSTRAINT [FK_DimDay_1] FOREIGN KEY ([MonthKey]) REFERENCES [dbo].[DimMonth] ([MonthKey])
GO
