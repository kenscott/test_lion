CREATE TABLE [dbo].[ATKPricingRulePlaybook]
(
[PricingRulePlaybookKey] [dbo].[Key_Normal_type] NOT NULL IDENTITY(1, 1),
[Version] [dbo].[Int_Type] NOT NULL CONSTRAINT [DF__ATKPricin__Versi__4E8020E1] DEFAULT ((0)),
[CreationDate] [dbo].[DateTime_Type] NOT NULL CONSTRAINT [DF__ATKPricin__Creat__6934171D] DEFAULT (getdate()),
[ModificationDate] [dbo].[DateTime_Type] NOT NULL CONSTRAINT [DF__ATKPricin__Modif__03E80D59] DEFAULT (getdate()),
[CreationUser] [dbo].[Description_Normal_type] NULL,
[ModificationUser] [dbo].[Description_Normal_type] NULL,
[ModificationUserAddress] [dbo].[Description_Normal_type] NULL,
[CreationUserAddress] [dbo].[Description_Normal_type] NULL,
[TotalSalesCDFMinPercent] [dbo].[Percent_Type] NULL,
[TotalInvoiceLinesCDFMinPercent] [dbo].[Percent_Type] NULL,
[GPPFrequencyThreshold] [dbo].[Percent_Type] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ATKPricingRulePlaybook] ADD CONSTRAINT [PK_ATKPricingRuleGroup] PRIMARY KEY CLUSTERED  ([PricingRulePlaybookKey]) ON [PRIMARY]
GO
