CREATE TABLE [cm].[ClaimJournalReasonCode]
(
[ClaimJournalReasonCodeKey] [dbo].[Key_Small_type] NOT NULL IDENTITY(1, 1),
[ClaimJournalReasonCode] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ClaimJournalReasonCodeDesc] [dbo].[Description_Normal_type] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [cm].[ClaimJournalReasonCode] ADD CONSTRAINT [PK_ATKAcceptanceCode] PRIMARY KEY CLUSTERED  ([ClaimJournalReasonCodeKey]) ON [PRIMARY]
GO
