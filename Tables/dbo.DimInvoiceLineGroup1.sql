CREATE TABLE [dbo].[DimInvoiceLineGroup1]
(
[InvoiceLineGroup1Key] [dbo].[Key_Normal_type] NOT NULL IDENTITY(1, 1),
[InvoiceLineGroup1UDVarchar1] [dbo].[UDVarchar_type] NOT NULL,
[InvoiceLineGroup1UDVarchar2] [dbo].[UDVarchar_type] NOT NULL,
[InvoiceLineGroup1UDVarchar3] [dbo].[UDVarchar_type] NOT NULL,
[InvoiceLineGroup1UDVarchar4] [dbo].[UDVarchar_type] NOT NULL,
[InvoiceLineGroup1UDVarchar5] [dbo].[UDVarchar_type] NOT NULL,
[CreationDayKey] [dbo].[DateKey_type] NOT NULL,
[ModificationDayKey] [dbo].[DateKey_type] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[DimInvoiceLineGroup1] ADD CONSTRAINT [PK_DimInvoiceLineGroup1] PRIMARY KEY CLUSTERED  ([InvoiceLineGroup1Key]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [I_DimInvoiceLineGroup1_1] ON [dbo].[DimInvoiceLineGroup1] ([InvoiceLineGroup1UDVarchar1]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [I_DimInvoiceLineGroup1_UD2] ON [dbo].[DimInvoiceLineGroup1] ([InvoiceLineGroup1UDVarchar1]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [I_DimInvoiceLineGroup1_2] ON [dbo].[DimInvoiceLineGroup1] ([InvoiceLineGroup1UDVarchar2]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [I_DimInvoiceLineGroup1_UD3] ON [dbo].[DimInvoiceLineGroup1] ([InvoiceLineGroup1UDVarchar2]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [I_DimInvoiceLineGroup1_3] ON [dbo].[DimInvoiceLineGroup1] ([InvoiceLineGroup1UDVarchar3]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [I_DimInvoiceLineGroup1_UD4] ON [dbo].[DimInvoiceLineGroup1] ([InvoiceLineGroup1UDVarchar3]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [I_DimInvoiceLineGroup1_4] ON [dbo].[DimInvoiceLineGroup1] ([InvoiceLineGroup1UDVarchar4]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [I_DimInvoiceLineGroup1_UD5] ON [dbo].[DimInvoiceLineGroup1] ([InvoiceLineGroup1UDVarchar4]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [I_DimInvoiceLineGroup1_5] ON [dbo].[DimInvoiceLineGroup1] ([InvoiceLineGroup1UDVarchar5]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [I_DimInvoiceLineGroup1_UD6] ON [dbo].[DimInvoiceLineGroup1] ([InvoiceLineGroup1UDVarchar5]) ON [PRIMARY]
GO
