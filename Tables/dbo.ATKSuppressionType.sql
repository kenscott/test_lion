CREATE TABLE [dbo].[ATKSuppressionType]
(
[SuppressionTypeKey] [dbo].[Key_Small_type] NOT NULL IDENTITY(1, 1),
[SuppressionTypeCode] [dbo].[Description_Normal_type] NOT NULL,
[SuppressionTypeDesc] [dbo].[Description_Normal_type] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ATKSuppressionType] ADD CONSTRAINT [PK_ATKSuppressionType] PRIMARY KEY CLUSTERED  ([SuppressionTypeKey]) ON [PRIMARY]
GO
