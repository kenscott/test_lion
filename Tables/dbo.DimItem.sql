CREATE TABLE [dbo].[DimItem]
(
[ItemKey] [dbo].[Key_Normal_type] NOT NULL IDENTITY(1, 1),
[ItemVendorKey] [dbo].[Key_Normal_type] NOT NULL,
[ItemGroup1Key] [dbo].[Key_Normal_type] NOT NULL,
[ItemGroup2Key] [dbo].[Key_Normal_type] NOT NULL,
[ItemGroup3Key] [dbo].[Key_Normal_type] NOT NULL,
[ItemClientUniqueIdentifier] [dbo].[Description_Small_type] NOT NULL,
[ItemNumber] [dbo].[Description_Small_type] NOT NULL,
[ItemDescription] [dbo].[Description_Normal_type] NOT NULL,
[BaseUnitOfMeasure] [dbo].[Description_Small_type] NULL,
[VendorStockNumber] [dbo].[UDVarchar_type] NULL,
[ItemUDVarchar1] [dbo].[UDVarchar_type] NULL,
[ItemUDVarchar2] [dbo].[UDVarchar_type] NULL,
[ItemUDVarchar3] [dbo].[UDVarchar_type] NULL,
[ItemUDVarchar4] [dbo].[UDVarchar_type] NULL,
[ItemUDVarchar5] [dbo].[UDVarchar_type] NULL,
[ItemUDVarchar6] [dbo].[UDVarchar_type] NULL,
[ItemUDVarchar7] [dbo].[UDVarchar_type] NULL,
[ItemUDVarchar8] [dbo].[UDVarchar_type] NULL,
[ItemUDVarchar9] [dbo].[UDVarchar_type] NULL,
[ItemUDVarchar10] [dbo].[UDVarchar_type] NULL CONSTRAINT [DF_DimItem_ItemUDVarchar10] DEFAULT (''),
[ItemUDVarchar11] [dbo].[UDVarchar_type] NULL CONSTRAINT [DF_DimItem_ItemUDVarchar11] DEFAULT (''),
[ItemUDVarchar12] [dbo].[UDVarchar_type] NULL CONSTRAINT [DF_DimItem_ItemUDVarchar12] DEFAULT (''),
[ItemUDVarchar13] [dbo].[UDVarchar_type] NULL CONSTRAINT [DF_DimItem_ItemUDVarchar13] DEFAULT (''),
[ItemUDVarchar14] [dbo].[UDVarchar_type] NULL CONSTRAINT [DF_DimItem_ItemUDVarchar14] DEFAULT (''),
[ItemUDVarchar15] [dbo].[UDVarchar_type] NULL CONSTRAINT [DF_DimItem_ItemUDVarchar15] DEFAULT (''),
[ItemUDVarchar16] [dbo].[UDVarchar_type] NULL CONSTRAINT [DF_DimItem_ItemUDVarchar16] DEFAULT (''),
[ItemUDVarchar17] [dbo].[UDVarchar_type] NULL CONSTRAINT [DF_DimItem_ItemUDVarchar17] DEFAULT (''),
[ItemUDVarchar18] [dbo].[UDVarchar_type] NULL CONSTRAINT [DF_DimItem_ItemUDVarchar18] DEFAULT (''),
[ItemUDVarchar19] [dbo].[UDVarchar_type] NULL CONSTRAINT [DF_DimItem_ItemUDVarchar19] DEFAULT (''),
[ItemUDVarchar20] [dbo].[UDVarchar_type] NULL CONSTRAINT [DF_DimItem_ItemUDVarchar20] DEFAULT (''),
[ItemUDVarchar21] [dbo].[UDVarchar_type] NULL,
[ItemUDVarchar22] [dbo].[UDVarchar_type] NULL,
[ItemUDVarchar23] [dbo].[UDVarchar_type] NULL,
[ItemUDVarchar24] [dbo].[UDVarchar_type] NULL,
[ItemUDVarchar25] [dbo].[UDVarchar_type] NULL,
[ItemUDVarChar26] [dbo].[UDVarchar_type] NULL,
[ItemUDVarChar27] [dbo].[UDVarchar_type] NULL,
[ItemUDVarChar28] [dbo].[UDVarchar_type] NULL,
[ItemUDVarChar29] [dbo].[UDVarchar_type] NULL,
[ItemUDVarChar30] [dbo].[UDVarchar_type] NULL,
[ItemUDDecimal1] [dbo].[UDDecimal_type] NULL,
[ItemUDDecimal2] [dbo].[UDDecimal_type] NULL,
[ItemUDDecimal3] [dbo].[UDDecimal_type] NULL,
[ItemUDDecimal4] [dbo].[UDDecimal_type] NULL,
[ItemUDDecimal5] [dbo].[UDDecimal_type] NULL,
[ItemUDDate1] [date] NULL,
[ItemUDDate2] [date] NULL,
[CreationDayKey] [dbo].[Key_Normal_type] NOT NULL,
[ModificationDayKey] [dbo].[Key_Normal_type] NOT NULL,
[LikeItemCode] [dbo].[Description_Normal_type] NULL,
[CurrentCost] [dbo].[Money_Normal_Type] NULL,
[InActive] [bit] NOT NULL CONSTRAINT [DF_DimItem_InActive] DEFAULT ((0)),
[CurrentInvoiceCost] [dbo].[Money_Normal_Type] NULL,
[ItemUDDate3] [date] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[DimItem] ADD CONSTRAINT [PK_ItemDimension] PRIMARY KEY CLUSTERED  ([ItemKey]) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [I_DimItem_4] ON [dbo].[DimItem] ([ItemClientUniqueIdentifier]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [I_DimItem_1] ON [dbo].[DimItem] ([ItemGroup3Key], [ItemKey]) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [I_DimItem_2] ON [dbo].[DimItem] ([ItemNumber]) INCLUDE ([ItemGroup3Key], [ItemKey], [ItemUDDecimal1]) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [I_DimItem_5] ON [dbo].[DimItem] ([ItemUDVarchar20], [ItemUDVarchar1]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [I_DimItem_3] ON [dbo].[DimItem] ([ItemVendorKey]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[DimItem] ADD CONSTRAINT [FK_DimItem_VendorKey] FOREIGN KEY ([ItemVendorKey]) REFERENCES [dbo].[DimVendor] ([VendorKey])
GO
