CREATE TABLE [dbo].[WebRolePermission]
(
[WebRolePermissionKey] [dbo].[Key_Normal_type] NOT NULL IDENTITY(22, 1),
[WebRoleKey] [dbo].[Key_Normal_type] NOT NULL,
[WebPermissionKey] [dbo].[Key_Normal_type] NOT NULL,
[CreationDate] [dbo].[DateTime_Type] NOT NULL CONSTRAINT [DF_WebUserPermission] DEFAULT (getdate())
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[WebRolePermission] ADD CONSTRAINT [PK_WebRolePermission] PRIMARY KEY CLUSTERED  ([WebRolePermissionKey]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [I_WebUserPermission_2] ON [dbo].[WebRolePermission] ([WebPermissionKey]) ON [PRIMARY]
GO
