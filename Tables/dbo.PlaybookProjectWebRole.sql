CREATE TABLE [dbo].[PlaybookProjectWebRole]
(
[ProjectWebRoleKey] [dbo].[Key_Normal_type] NOT NULL IDENTITY(1, 1),
[ProjectKey] [dbo].[Key_Normal_type] NOT NULL,
[WebRoleKey] [dbo].[Key_Normal_type] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[PlaybookProjectWebRole] ADD CONSTRAINT [PK_PlaybookProjectWebRole] PRIMARY KEY NONCLUSTERED  ([ProjectWebRoleKey]) ON [PRIMARY]
GO
CREATE UNIQUE CLUSTERED INDEX [I_PlaybookProjectWebRole_1] ON [dbo].[PlaybookProjectWebRole] ([ProjectKey], [WebRoleKey]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[PlaybookProjectWebRole] ADD CONSTRAINT [FK_PlaybookProjectWebRole_1] FOREIGN KEY ([ProjectKey]) REFERENCES [dbo].[PlaybookProject] ([ProjectKey])
GO
ALTER TABLE [dbo].[PlaybookProjectWebRole] ADD CONSTRAINT [FK_PlaybookProjectWebRole_2] FOREIGN KEY ([WebRoleKey]) REFERENCES [dbo].[WebRole] ([WebRoleKey])
GO
