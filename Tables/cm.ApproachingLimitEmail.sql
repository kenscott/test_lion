CREATE TABLE [cm].[ApproachingLimitEmail]
(
[ApproachingLimitEmailKey] [int] NOT NULL IDENTITY(1, 1),
[ContractItemKey] [int] NULL,
[ContractItemGroupXKey] [int] NULL,
[CreationDate] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [cm].[ApproachingLimitEmail] ADD CONSTRAINT [PK_ApproachingLimitEmail] PRIMARY KEY CLUSTERED  ([ApproachingLimitEmailKey]) ON [PRIMARY]
GO
