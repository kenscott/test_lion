CREATE TABLE [cg].[PlaybookDatapoint]
(
[PlaybookDataPointKey] [dbo].[Key_Normal_type] NOT NULL IDENTITY(1, 1) NOT FOR REPLICATION,
[PlaybookDataPointGroupKey] [dbo].[Key_Normal_type] NOT NULL,
[ContractKey] [dbo].[Key_Normal_type] NOT NULL,
[AccountKey] [dbo].[Key_Normal_type] NOT NULL,
[InstructionLevelType] [dbo].[Description_Small_type] NOT NULL,
[LLSPGCode] [dbo].[Description_Small_type] NOT NULL,
[MPGCode] [dbo].[Description_Small_type] NOT NULL,
[ItemNumber] [dbo].[Description_Small_type] NOT NULL,
[InstructionLevelCode] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ClaimBackPercent] [dbo].[Percent_Type] NULL,
[Sales603010Bucket] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [cg].[PlaybookDatapoint] ADD CONSTRAINT [PK_CG_PlaybookDataPoint] PRIMARY KEY CLUSTERED  ([PlaybookDataPointKey]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [CG_PlaybookDataPoint_1] ON [cg].[PlaybookDatapoint] ([PlaybookDataPointGroupKey], [ContractKey], [AccountKey]) INCLUDE ([ClaimBackPercent], [InstructionLevelType], [ItemNumber], [LLSPGCode], [MPGCode], [PlaybookDataPointKey], [Sales603010Bucket]) ON [PRIMARY]
GO
