CREATE TABLE [ppe].[RequestNote]
(
[RequestNoteKey] [int] NOT NULL IDENTITY(1, 1),
[RequestKey] [int] NOT NULL,
[Notes] [varchar] (4000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NoteType] [tinyint] NOT NULL CONSTRAINT [DF__RequestNo__NoteT__6C4EE43C] DEFAULT ((1)),
[CreatedDate] [datetime] NOT NULL CONSTRAINT [DF__RequestNo__Creat__6D430875] DEFAULT (getdate()),
[WebUserKey] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [ppe].[RequestNote] ADD CONSTRAINT [PK_RequestNote] PRIMARY KEY NONCLUSTERED  ([RequestNoteKey]) ON [PRIMARY]
GO
CREATE CLUSTERED INDEX [i_requestkey] ON [ppe].[RequestNote] ([RequestKey]) ON [PRIMARY]
GO
