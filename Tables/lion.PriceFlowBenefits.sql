CREATE TABLE [lion].[PriceFlowBenefits]
(
[ClientInvoiceLineUniqueID] [int] NOT NULL,
[RequestKey] [int] NOT NULL,
[Actual Benefit] [decimal] (38, 6) NULL,
[Potential Benefit] [decimal] (38, 6) NULL,
[RuleGroup] AS (case left([RuleCode],(1)) when 'E' then 'Excluded - Non Price Flow' when 'D' then 'Excluded - Business Decision' when 'Z' then 'No Change' when 'B' then 'Benefit' when 'L' then 'Loss' when 'U' then 'Unknown' end),
[RuleCode] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [lion].[PriceFlowBenefits] ADD CONSTRAINT [PK_PriceFlowBenefits] PRIMARY KEY CLUSTERED  ([ClientInvoiceLineUniqueID], [RequestKey]) ON [PRIMARY]
GO
