CREATE TABLE [dbo].[DimVendorContact]
(
[VendorContactKey] [int] NOT NULL IDENTITY(1, 1),
[VendorKey] [int] NOT NULL,
[ContactName] [dbo].[Description_Normal_type] NOT NULL,
[ContactEmail] [dbo].[Description_Normal_type] NOT NULL,
[ContactPhone] [dbo].[Description_Normal_type] NULL,
[ContactPrimaryIndicator] [bit] NULL,
[ContactSendClaimIndicator] [bit] NULL,
[CreationDate] [date] NOT NULL,
[CreatedByWebUserKey] [int] NOT NULL,
[ModificationDate] [date] NULL,
[ModifiedByWebUserKey] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[DimVendorContact] ADD CONSTRAINT [PK_DimVendorContact] PRIMARY KEY CLUSTERED  ([VendorContactKey]) ON [PRIMARY]
GO
