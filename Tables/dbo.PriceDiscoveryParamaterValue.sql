CREATE TABLE [dbo].[PriceDiscoveryParamaterValue]
(
[PriceDiscoveryParamaterKey] [dbo].[Key_Normal_type] NOT NULL,
[Value] [dbo].[Description_Normal_type] NOT NULL,
[SubValue1] [dbo].[Description_Normal_type] NOT NULL CONSTRAINT [DF__PriceDisc__SubVa__76D69450] DEFAULT ('')
) ON [PRIMARY]
GO
CREATE UNIQUE CLUSTERED INDEX [UI_PriceDiscoveryParamaterValue_1] ON [dbo].[PriceDiscoveryParamaterValue] ([PriceDiscoveryParamaterKey], [Value], [SubValue1]) ON [PRIMARY]
GO
