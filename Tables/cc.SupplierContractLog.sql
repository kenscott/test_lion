CREATE TABLE [cc].[SupplierContractLog]
(
[ContractLogKey] [int] NOT NULL IDENTITY(1, 1),
[WebUserKey] [int] NULL,
[ContractKey] [int] NULL,
[VendorKey] [int] NULL,
[Notes] [varchar] (4000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreationDate] [datetime] NULL CONSTRAINT [DF__ContractNo__Creat__6D430875] DEFAULT (getdate()),
[ContractAction] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ContractID] [varchar] (9) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [cc].[SupplierContractLog] ADD CONSTRAINT [PK_ContractLog] PRIMARY KEY NONCLUSTERED  ([ContractLogKey]) ON [PRIMARY]
GO
