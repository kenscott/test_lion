CREATE TABLE [lion].[MerchandisedProductData]
(
[WUK Item] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Item Description] [dbo].[Description_Normal_type] NOT NULL,
[Global SPG] [dbo].[Description_Small_type] NOT NULL,
[HL SPG] [dbo].[Description_Small_type] NOT NULL,
[LL SPG] [dbo].[Description_Small_type] NOT NULL,
[Supplier] [dbo].[Description_Small_type] NOT NULL,
[Supplier Name] [dbo].[Description_Small_type] NOT NULL,
[NDP Ind] [dbo].[UDVarchar_type] NULL,
[# of Accounts] [int] NULL,
[# of Invoices] [int] NULL,
[Sales] [decimal] (38, 8) NULL,
[Quantity] [decimal] (38, 8) NULL,
[Trade Price] [decimal] (38, 7) NULL,
[Avg Price] [decimal] (38, 6) NULL,
[Avg Disc Off Trade] [decimal] (38, 6) NULL,
[Avg GP%] [decimal] (38, 6) NULL,
[Price Index] [decimal] (38, 7) NULL,
[GPP @ Trade] [decimal] (38, 6) NULL,
[GP% @ Green] [decimal] (38, 6) NULL,
[GP% @ Amber] [decimal] (38, 6) NULL,
[GP% @ Red] [decimal] (38, 6) NULL,
[Green Price] [decimal] (38, 6) NULL,
[Amber Price] [decimal] (38, 6) NULL,
[Red Price] [decimal] (38, 6) NULL
) ON [PRIMARY]
GO
