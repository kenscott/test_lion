CREATE TABLE [dbo].[PlaybookProjectMonthPeriod]
(
[ProjectKey] [dbo].[Key_Normal_type] NOT NULL,
[InvoiceDateMonthKey] [dbo].[Key_Small_type] NOT NULL,
[PeroidNumber] [dbo].[Int_255_type] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[PlaybookProjectMonthPeriod] ADD CONSTRAINT [PK_PlaybookProjectMonthPeriod] PRIMARY KEY CLUSTERED  ([ProjectKey], [InvoiceDateMonthKey]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[PlaybookProjectMonthPeriod] ADD CONSTRAINT [FK_PlaybookProjectMonthPeriod_1] FOREIGN KEY ([ProjectKey]) REFERENCES [dbo].[PlaybookProject] ([ProjectKey])
GO
ALTER TABLE [dbo].[PlaybookProjectMonthPeriod] ADD CONSTRAINT [FK_PlaybookProjectMonthPeriod_2] FOREIGN KEY ([InvoiceDateMonthKey]) REFERENCES [dbo].[DimMonth] ([MonthKey])
GO
