CREATE TABLE [dbo].[PlaybookProjectAccountStatus]
(
[ProjectAccountKey] [dbo].[Key_Normal_type] NOT NULL IDENTITY(1, 1),
[ProjectKey] [dbo].[Key_Normal_type] NOT NULL,
[AccountKey] [dbo].[Key_Normal_type] NOT NULL,
[ProjectAccountStatus] [dbo].[Description_Normal_type] NOT NULL,
[AccountManagerKey] [dbo].[Key_Normal_type] NOT NULL,
[ProjectAccountManagerStatus] [dbo].[Description_Normal_type] NOT NULL,
[Version] [dbo].[Int_Type] NOT NULL CONSTRAINT [DF_PlaybookProjectAccountStatus_1] DEFAULT ((0)),
[CreationDate] [dbo].[DateTime_Type] NOT NULL CONSTRAINT [DF_PlaybookProjectAccountStatus_2] DEFAULT (getdate()),
[ModificationDate] [dbo].[DateTime_Type] NOT NULL CONSTRAINT [DF_PlaybookProjectAccountStatus_3] DEFAULT (getdate()),
[CreationUser] [dbo].[Description_Normal_type] NULL,
[ModificationUser] [dbo].[Description_Normal_type] NULL,
[ModificationUserAddress] [dbo].[Description_Normal_type] NULL,
[CreationUserAddress] [dbo].[Description_Normal_type] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[PlaybookProjectAccountStatus] ADD CONSTRAINT [PK_PlaybookProjectAccountStatus] PRIMARY KEY NONCLUSTERED  ([ProjectAccountKey]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [I_PlaybookProjectAccountStatus_2] ON [dbo].[PlaybookProjectAccountStatus] ([ProjectAccountStatus]) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [UI_PlaybookProjectAccountStatus_2] ON [dbo].[PlaybookProjectAccountStatus] ([ProjectKey], [AccountKey]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [I_PlaybookProjectAccountStatus_3] ON [dbo].[PlaybookProjectAccountStatus] ([ProjectKey], [AccountManagerKey]) ON [PRIMARY]
GO
CREATE CLUSTERED INDEX [I_PlaybookProjectAccountStatus_1] ON [dbo].[PlaybookProjectAccountStatus] ([ProjectKey], [ProjectAccountStatus]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[PlaybookProjectAccountStatus] ADD CONSTRAINT [FK_PlaybookProjectAccountStatus_1] FOREIGN KEY ([ProjectKey]) REFERENCES [dbo].[PlaybookProject] ([ProjectKey])
GO
ALTER TABLE [dbo].[PlaybookProjectAccountStatus] ADD CONSTRAINT [FK_PlaybookProjectAccountStatus_10] FOREIGN KEY ([ProjectAccountStatus]) REFERENCES [dbo].[ATKAccountStatus] ([AccountStatus])
GO
ALTER TABLE [dbo].[PlaybookProjectAccountStatus] ADD CONSTRAINT [FK_PlaybookProjectAccountStatus_11] FOREIGN KEY ([ProjectAccountManagerStatus]) REFERENCES [dbo].[ATKAccountManagerStatus] ([AccountManagerStatus])
GO
ALTER TABLE [dbo].[PlaybookProjectAccountStatus] ADD CONSTRAINT [FK_PlaybookProjectAccountStatus_2] FOREIGN KEY ([AccountKey]) REFERENCES [dbo].[DimAccount] ([AccountKey])
GO
