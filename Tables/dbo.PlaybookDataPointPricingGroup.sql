CREATE TABLE [dbo].[PlaybookDataPointPricingGroup]
(
[PlaybookDataPointPricingGroupKey] [dbo].[Key_Normal_type] NOT NULL IDENTITY(1, 1),
[PlaybookDataPointKey] [dbo].[Key_Normal_type] NOT NULL,
[PlaybookPricingGroupKey] [dbo].[Key_Normal_type] NOT NULL,
[PricingRuleKey] [dbo].[Key_Normal_type] NOT NULL,
[RulingMemberFlag] [dbo].[Description_Small_type] NOT NULL,
[Total12MonthQuantity] [dbo].[Quantity_Normal_type] NULL,
[LastItemPrice] [dbo].[Money_Normal_Type] NULL,
[LastItemCost] [dbo].[Money_Normal_Type] NULL,
[LastItemDiscountPercent] [dbo].[Percent_Type] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[PlaybookDataPointPricingGroup] ADD CONSTRAINT [PK_PlaybookDataPointPricingGroup] PRIMARY KEY CLUSTERED  ([PlaybookDataPointPricingGroupKey]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [I_PlaybookDataPointPricingGroup_1] ON [dbo].[PlaybookDataPointPricingGroup] ([PlaybookDataPointKey]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [I_PlaybookDataPointPricingGroup_6] ON [dbo].[PlaybookDataPointPricingGroup] ([PlaybookDataPointKey], [RulingMemberFlag], [PricingRuleKey], [PlaybookPricingGroupKey], [Total12MonthQuantity]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [I_PlaybookDataPointPricingGroup_5] ON [dbo].[PlaybookDataPointPricingGroup] ([PlaybookPricingGroupKey], [PlaybookDataPointKey]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [I_PlaybookDataPointPricingGroup_4] ON [dbo].[PlaybookDataPointPricingGroup] ([PlaybookPricingGroupKey], [RulingMemberFlag], [Total12MonthQuantity]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [I_PlaybookDataPointPricingGroup_3] ON [dbo].[PlaybookDataPointPricingGroup] ([PricingRuleKey]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [_dta_index_PlaybookDataPointPricingGroup_5_1365579903__K5_K3_2_4_6] ON [dbo].[PlaybookDataPointPricingGroup] ([RulingMemberFlag], [PlaybookPricingGroupKey]) INCLUDE ([PlaybookDataPointKey], [PricingRuleKey], [Total12MonthQuantity]) ON [PRIMARY]
GO
CREATE STATISTICS [_dta_stat_1365579903_2_5_3] ON [dbo].[PlaybookDataPointPricingGroup] ([PlaybookPricingGroupKey], [PlaybookDataPointKey], [RulingMemberFlag])
GO
CREATE STATISTICS [_dta_stat_1365579903_3_4_2] ON [dbo].[PlaybookDataPointPricingGroup] ([PlaybookPricingGroupKey], [PricingRuleKey], [PlaybookDataPointKey])
GO
ALTER TABLE [dbo].[PlaybookDataPointPricingGroup] ADD CONSTRAINT [FK_PlaybookDataPointPricingGroup_1] FOREIGN KEY ([PlaybookDataPointKey]) REFERENCES [dbo].[PlaybookDataPoint] ([PlaybookDataPointKey])
GO
ALTER TABLE [dbo].[PlaybookDataPointPricingGroup] ADD CONSTRAINT [FK_PlaybookDataPointPricingGroup_2] FOREIGN KEY ([PlaybookPricingGroupKey]) REFERENCES [dbo].[PlaybookPricingGroup] ([PlaybookPricingGroupKey])
GO
ALTER TABLE [dbo].[PlaybookDataPointPricingGroup] ADD CONSTRAINT [FK_PlaybookDataPointPricingGroup_3] FOREIGN KEY ([PricingRuleKey]) REFERENCES [dbo].[ATKPricingRule] ([PricingRuleKey])
GO
