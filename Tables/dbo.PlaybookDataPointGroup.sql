CREATE TABLE [dbo].[PlaybookDataPointGroup]
(
[PlaybookDataPointGroupKey] [dbo].[Key_Normal_type] NOT NULL IDENTITY(1, 1),
[ProjectSpecificationScenarioKey] [dbo].[Key_Normal_type] NOT NULL,
[ProjectKey] [dbo].[Key_Normal_type] NOT NULL,
[Version] [dbo].[Int_Type] NOT NULL CONSTRAINT [DF_PlaybookDataPointGroup_Version] DEFAULT ((0)),
[CreationDate] [dbo].[DateTime_Type] NOT NULL CONSTRAINT [DF_PlaybookDataPointGroup_CreationDate] DEFAULT (getdate()),
[ModificationDate] [dbo].[DateTime_Type] NOT NULL CONSTRAINT [DF_PlaybookDataPointGroup_ModificationDate] DEFAULT (getdate()),
[CreationUser] [dbo].[Description_Normal_type] NULL,
[ModificationUser] [dbo].[Description_Normal_type] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[PlaybookDataPointGroup] ADD CONSTRAINT [PK_ATKProject_1] PRIMARY KEY CLUSTERED  ([PlaybookDataPointGroupKey]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[PlaybookDataPointGroup] ADD CONSTRAINT [FK_PlaybookDataPointGroup_1] FOREIGN KEY ([ProjectSpecificationScenarioKey]) REFERENCES [dbo].[ATKProjectSpecificationScenario] ([ProjectSpecificationScenarioKey])
GO
ALTER TABLE [dbo].[PlaybookDataPointGroup] ADD CONSTRAINT [FK_PlaybookDataPointGroup_2] FOREIGN KEY ([ProjectKey]) REFERENCES [dbo].[PlaybookProject] ([ProjectKey])
GO
