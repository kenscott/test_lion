CREATE TABLE [dbo].[DimAccount]
(
[AccountKey] [dbo].[Key_Normal_type] NOT NULL IDENTITY(1, 1),
[AccountManagerKey] [dbo].[Key_Normal_type] NOT NULL,
[AccountClientUniqueIdentifier] [dbo].[Description_Small_type] NOT NULL,
[AccountNumber] [dbo].[Description_Small_type] NOT NULL,
[AccountNumberParent] [dbo].[Description_Small_type] NULL,
[AccountName] [dbo].[Description_Normal_type] NOT NULL,
[AccountGroup1Key] [dbo].[Key_Normal_type] NOT NULL,
[AccountGroup2Key] [dbo].[Key_Normal_type] NOT NULL,
[AccountGroup3Key] [dbo].[Key_Normal_type] NOT NULL,
[AccountGroup4Key] [dbo].[Key_Normal_type] NOT NULL CONSTRAINT [DF_DimAccount_AccountGroup4Key] DEFAULT ((1)),
[AccountAddress1] [dbo].[Description_Normal_type] NULL,
[AccountAddress2] [dbo].[Description_Normal_type] NULL,
[AccountAddress3] [dbo].[Description_Normal_type] NULL,
[AccountAddress4] [dbo].[Description_Normal_type] NULL,
[AccountCity] [dbo].[Description_Small_type] NULL,
[AccountState] [dbo].[Description_Small_type] NULL,
[AccountZipCode] [dbo].[Description_Small_type] NULL,
[AccountNumberBillTo] [dbo].[Description_Small_type] NULL,
[AccountMemo] [dbo].[Description_Big_type] NULL,
[CreationDayKey] [dbo].[Key_Small_type] NOT NULL,
[ModificationDayKey] [dbo].[Key_Small_type] NOT NULL,
[AccountUDVarchar1] [dbo].[UDVarchar_type] NULL,
[AccountUDVarchar2] [dbo].[UDVarchar_type] NULL,
[AccountUDVarchar3] [dbo].[UDVarchar_type] NULL,
[AccountUDVarchar4] [dbo].[UDVarchar_type] NULL,
[AccountUDVarchar5] [dbo].[UDVarchar_type] NULL,
[AccountUDVarchar6] [dbo].[UDVarchar_type] NULL,
[AccountUDDecimal1] [dbo].[UDDecimal_type] NULL,
[AccountUDDecimal2] [dbo].[UDDecimal_type] NULL,
[AccountUDVarchar7] [dbo].[UDVarchar_type] NULL CONSTRAINT [DF_DimAccount_AccountUDVarchar7] DEFAULT (''),
[AccountUDVarchar8] [dbo].[UDVarchar_type] NULL CONSTRAINT [DF_DimAccount_AccountUDVarchar8] DEFAULT (''),
[AccountUDVarchar9] [dbo].[UDVarchar_type] NULL,
[AccountUDVarchar10] [dbo].[UDVarchar_type] NULL,
[AccountUDVarchar11] [dbo].[UDVarchar_type] NULL,
[AccountUDVarchar12] [dbo].[UDVarchar_type] NULL,
[AccountUDVarChar13] [dbo].[UDVarchar_type] NULL,
[AccountUDVarChar14] [dbo].[UDVarchar_type] NULL,
[AccountUDDateTime1] [dbo].[UDDateTime_type] NULL,
[AccountUDDateTime2] [dbo].[UDDateTime_type] NULL,
[Inactive] [bit] NOT NULL CONSTRAINT [DF_DimAccount_InActive] DEFAULT ((0)),
[BrandKey] [dbo].[Key_Small_type] NOT NULL,
[CustomerSpecialism] [dbo].[UDVarchar_Small_type] NOT NULL CONSTRAINT [DF_DimAccount_CustomerSpecialism] DEFAULT (N'Heat'),
[Sales603010Bucket] [dbo].[Description_Small_type] NOT NULL CONSTRAINT [DF_DimAccount_Sales603010Bucket] DEFAULT (N'Small/Cash'),
[Sales6020155Bucket] [dbo].[Description_Small_type] NOT NULL CONSTRAINT [DF_DimAccount_Sales6020155Bucket] DEFAULT (N'D')
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[DimAccount] ADD CONSTRAINT [PK_AccountDimension] PRIMARY KEY CLUSTERED  ([AccountKey]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [I_DimAccount_10] ON [dbo].[DimAccount] ([AccountAddress1]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [I_DimAccount_8] ON [dbo].[DimAccount] ([AccountCity]) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [UI_DimAccount_1] ON [dbo].[DimAccount] ([AccountClientUniqueIdentifier]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [I_DimAccount_4] ON [dbo].[DimAccount] ([AccountGroup1Key]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [I_DimAccount_19] ON [dbo].[DimAccount] ([AccountGroup1Key]) INCLUDE ([AccountManagerKey]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [i_DimAccount_23] ON [dbo].[DimAccount] ([AccountGroup1Key], [AccountKey], [AccountManagerKey], [AccountGroup3Key], [AccountGroup2Key]) INCLUDE ([AccountName], [AccountNumber], [AccountUDDateTime1], [AccountUDVarchar1], [AccountUDVarchar10], [AccountUDVarchar6], [AccountUDVarchar7], [AccountUDVarchar8], [AccountZipCode]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [I_DimAccount_5] ON [dbo].[DimAccount] ([AccountGroup2Key]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [I_DimAccount_6] ON [dbo].[DimAccount] ([AccountGroup3Key]) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [I_DimAccount_17] ON [dbo].[DimAccount] ([AccountKey]) INCLUDE ([AccountUDVarchar7]) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [I_DimAccount_20] ON [dbo].[DimAccount] ([AccountKey], [AccountGroup1Key], [AccountUDVarchar9], [AccountUDVarchar10]) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [I_DimAccount_21] ON [dbo].[DimAccount] ([AccountKey], [AccountGroup1Key], [AccountUDVarchar9], [AccountUDVarchar10], [AccountUDVarchar11]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [I_DimAccount_1] ON [dbo].[DimAccount] ([AccountManagerKey]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [I_DimAccount_13] ON [dbo].[DimAccount] ([AccountManagerKey], [AccountGroup1Key], [AccountKey]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [I_DimAccount_3] ON [dbo].[DimAccount] ([AccountName]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [I_DimAccount_2] ON [dbo].[DimAccount] ([AccountNumber]) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [I_DimAccount_22] ON [dbo].[DimAccount] ([AccountNumber]) INCLUDE ([AccountGroup1Key], [AccountGroup2Key], [AccountKey], [AccountUDVarchar10], [AccountUDVarchar11], [AccountUDVarchar8]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [I_DimAccount_12] ON [dbo].[DimAccount] ([AccountNumberBillTo]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [i_AcctNumParent] ON [dbo].[DimAccount] ([AccountNumberParent]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [I_DimAccount_9] ON [dbo].[DimAccount] ([AccountState]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [I_DimAccount_UD25] ON [dbo].[DimAccount] ([AccountUDDateTime1]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [I_DimAccount_UD26] ON [dbo].[DimAccount] ([AccountUDDateTime2]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [I_DimAccount_UD15] ON [dbo].[DimAccount] ([AccountUDVarchar1]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [I_DimAccount_UD16] ON [dbo].[DimAccount] ([AccountUDVarchar2]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [I_DimAccount_UD17] ON [dbo].[DimAccount] ([AccountUDVarchar3]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [I_DimAccount_11] ON [dbo].[DimAccount] ([AccountZipCode]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [i_Inactive_Acct] ON [dbo].[DimAccount] ([Inactive], [AccountKey]) INCLUDE ([AccountGroup1Key], [AccountName], [AccountNumber], [AccountNumberParent]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[DimAccount] ADD CONSTRAINT [FK_DimAccount_1] FOREIGN KEY ([BrandKey]) REFERENCES [dbo].[DimBrand] ([BrandKey])
GO
ALTER TABLE [dbo].[DimAccount] ADD CONSTRAINT [FK_DimAccount_5] FOREIGN KEY ([AccountManagerKey]) REFERENCES [dbo].[DimAccountManager] ([AccountManagerKey])
GO
