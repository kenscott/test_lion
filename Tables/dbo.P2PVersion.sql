CREATE TABLE [dbo].[P2PVersion]
(
[Name] [dbo].[Description_Small_type] NOT NULL,
[Version] [dbo].[Description_Small_type] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[P2PVersion] ADD CONSTRAINT [PK_P2PVersion] PRIMARY KEY CLUSTERED  ([Name]) ON [PRIMARY]
GO
