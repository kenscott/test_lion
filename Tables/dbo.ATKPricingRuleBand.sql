CREATE TABLE [dbo].[ATKPricingRuleBand]
(
[PricingRuleBandKey] [dbo].[Key_Normal_type] NOT NULL IDENTITY(1, 1),
[PricingRuleKey] [dbo].[Key_Normal_type] NOT NULL,
[LowerBandPercent] [dbo].[Percent_Type] NULL,
[UpperBandPercent] [dbo].[Percent_Type] NULL,
[LowerBandQuantity] [dbo].[Quantity_Normal_type] NULL,
[UpperBandQuantity] [dbo].[Quantity_Normal_type] NULL,
[PercentOrQuantityIndicator] [dbo].[Description_Small_type] NOT NULL CONSTRAINT [DF_ATKPricingRuleBand_PercentOrQuantityIndicator] DEFAULT ('Percent'),
[Version] [dbo].[Int_Type] NOT NULL CONSTRAINT [DF__ATKPricin__Versi__4D8BFCA8] DEFAULT ((0)),
[CreationDate] [dbo].[DateTime_Type] NOT NULL CONSTRAINT [DF__ATKPricin__Creat__683FF2E4] DEFAULT (getdate()),
[ModificationDate] [dbo].[DateTime_Type] NOT NULL CONSTRAINT [DF__ATKPricin__Modif__02F3E920] DEFAULT (getdate()),
[CreationUser] [dbo].[Description_Normal_type] NULL,
[ModificationUser] [dbo].[Description_Normal_type] NULL,
[ModificationUserAddress] [dbo].[Description_Normal_type] NULL,
[CreationUserAddress] [dbo].[Description_Normal_type] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ATKPricingRuleBand] ADD CONSTRAINT [CK_ATKPricingRuleBand_1] CHECK (([LowerBandPercent]>=(0) AND [LowerBandPercent]<=(1)))
GO
ALTER TABLE [dbo].[ATKPricingRuleBand] ADD CONSTRAINT [Check_ATKPricingRuleBand_1] CHECK (([PercentOrQuantityIndicator]='Quantity' OR [PercentOrQuantityIndicator]='Percent'))
GO
ALTER TABLE [dbo].[ATKPricingRuleBand] ADD CONSTRAINT [CK_ATKPricingRuleBand_2] CHECK (([UpperBandPercent]>=(0) AND [UpperBandPercent]<=(1)))
GO
ALTER TABLE [dbo].[ATKPricingRuleBand] ADD CONSTRAINT [PK_ATKPricingBand] PRIMARY KEY CLUSTERED  ([PricingRuleBandKey]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [I_ATKPricingRuleBand_1] ON [dbo].[ATKPricingRuleBand] ([PricingRuleKey]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ATKPricingRuleBand] ADD CONSTRAINT [FK_ATKPricingRuleBand_1] FOREIGN KEY ([PricingRuleKey]) REFERENCES [dbo].[ATKPricingRule] ([PricingRuleKey])
GO
