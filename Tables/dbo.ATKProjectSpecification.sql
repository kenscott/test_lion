CREATE TABLE [dbo].[ATKProjectSpecification]
(
[ProjectSpecificationKey] [dbo].[Key_Normal_type] NOT NULL IDENTITY(1, 1),
[ProjectSpecificationName] [dbo].[Description_Normal_type] NOT NULL,
[ProjectSpecificationDescription] [dbo].[Description_Big_type] NULL,
[ApproachTypeKey] [dbo].[Key_Small_type] NULL,
[AccountExclusionIndicator] [dbo].[Description_Small_type] NOT NULL CONSTRAINT [DF_ATKProjectSpecification_AccountExclusionIndicator] DEFAULT ('N'),
[Version] [dbo].[Int_Type] NOT NULL CONSTRAINT [DF__ATKProjec__Versi__4F74451A] DEFAULT ((0)),
[CreationDate] [dbo].[DateTime_Type] NOT NULL CONSTRAINT [DF__ATKProjec__Creat__6A283B56] DEFAULT (getdate()),
[ModificationDate] [dbo].[DateTime_Type] NOT NULL CONSTRAINT [DF__ATKProjec__Modif__04DC3192] DEFAULT (getdate()),
[CreationUser] [dbo].[Description_Normal_type] NULL,
[ModificationUser] [dbo].[Description_Normal_type] NULL,
[ModificationUserAddress] [dbo].[Description_Normal_type] NULL,
[CreationUserAddress] [dbo].[Description_Normal_type] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ATKProjectSpecification] ADD CONSTRAINT [PK_ATKProject] PRIMARY KEY CLUSTERED  ([ProjectSpecificationKey]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ATKProjectSpecification] ADD CONSTRAINT [FK_ATKProjectSpecification_1] FOREIGN KEY ([ApproachTypeKey]) REFERENCES [dbo].[ATKApproachType] ([ApproachTypeKey])
GO
