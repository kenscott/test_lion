CREATE TABLE [cm].[ManualClaimImport]
(
[ManualClaimImportKey] [int] NOT NULL IDENTITY(1, 1),
[BatchKey] [int] NOT NULL,
[ClaimID] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Period] [int] NULL,
[ContractID] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SupplierID] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SupplierRef] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[UniqueTransactionID] [int] NULL,
[ProductQty] [decimal] (19, 3) NULL,
[ProductClaimEach] [decimal] (19, 3) NULL,
[Comment] [dbo].[Description_Huge_type] NULL,
[RowNumber] [int] NOT NULL,
[ErrorMessage] [varchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [cm].[ManualClaimImport] ADD CONSTRAINT [PK_ManualClaimImport] PRIMARY KEY CLUSTERED  ([ManualClaimImportKey]) ON [PRIMARY]
GO
