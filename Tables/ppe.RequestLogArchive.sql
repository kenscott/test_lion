CREATE TABLE [ppe].[RequestLogArchive]
(
[RequestLogKey] [int] NOT NULL,
[WebUserKey] [int] NULL,
[RequestKey] [int] NULL,
[OldStatusKey] [smallint] NULL,
[NewStatusKey] [smallint] NULL,
[RequestType] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LogTime] [datetime] NULL,
[RequestAction] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[EmailSent] [bit] NULL,
[OldOwnerWebUserKey] [int] NULL,
[NewOwnerWebUserKey] [int] NULL,
[Notes] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [ppe].[RequestLogArchive] ADD CONSTRAINT [PK_RequestLogArchive] PRIMARY KEY NONCLUSTERED  ([RequestLogKey]) ON [PRIMARY]
GO
