CREATE TABLE [dbo].[DimBrand]
(
[BrandKey] [dbo].[Key_Small_type] NOT NULL IDENTITY(1, 1),
[Brand] [dbo].[Description_Small_type] NOT NULL,
[SegmentBrand] [dbo].[UDVarchar_type] NOT NULL,
[SegmentBrandDetached] [dbo].[UDVarchar_type] NULL,
[AccountingBrand] [dbo].[UDVarchar_type] NULL,
[PlaybookBrand] [dbo].[UDVarchar_type] NULL,
[CreationDayKey] [smallint] NOT NULL,
[ModificationDayKey] [smallint] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[DimBrand] ADD CONSTRAINT [PK_DimBrand] PRIMARY KEY CLUSTERED  ([BrandKey]) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [I_U_DimBrand_1] ON [dbo].[DimBrand] ([Brand]) ON [PRIMARY]
GO
