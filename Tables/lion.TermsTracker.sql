CREATE TABLE [lion].[TermsTracker]
(
[ID] [int] NOT NULL,
[AccountKey] [int] NULL,
[ItemGroup3Key] [dbo].[Key_Normal_type] NOT NULL,
[ItemKey] [int] NULL,
[OriginalDiscount] [decimal] (19, 8) NULL,
[OriginalDate] [date] NULL,
[NewDiscount] [decimal] (19, 8) NULL,
[NewWolcenDate] [date] NULL,
[Total12MonthQuantity] [decimal] (19, 8) NULL,
[Sales @ Original] [decimal] (19, 8) NULL,
[Sales @ New] [decimal] (19, 8) NULL,
[ProjectedBenefit] AS ([Sales @ New]-[Sales @ Original]),
[NewException] [bit] NOT NULL CONSTRAINT [DF_TermsTracker_NewTerm] DEFAULT ((0)),
[NewSPG] [bit] NOT NULL CONSTRAINT [DF_TermsTracker_NewSPG] DEFAULT ((0)),
[Processed] [bit] NOT NULL CONSTRAINT [DF_TermsTracker_Processed] DEFAULT ((0)),
[CurrentIndicator] [bit] NOT NULL CONSTRAINT [DF_TermsTracker_CurrentIndicator] DEFAULT ((1)),
[LastInvoiceDateDayKey] [int] NULL
) ON [PRIMARY]
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		José A. Castaños
-- Create date: 15-May-2015
-- Description:	Turn Current Indicator on/off as needed
-- =============================================
CREATE TRIGGER [lion].[tID_TermsTracker]
   ON  [lion].[TermsTracker]
   FOR INSERT,DELETE
AS 
BEGIN
	SET NOCOUNT ON;
/*
	if exists (select * from inserted)
	begin
		UPDATE tt
		   SET tt.CurrentIndicator = 0
		  FROM lion.TermsTracker tt inner join
			   inserted i on tt.AccountKey = i.AccountKey and
							 tt.ItemGroup3Key = i.ItemGroup3Key and
							 ISNULL(tt.ItemKey, 0) = ISNULL(i.ItemKey, 0) and
							 tt.ID <> i.ID and
							 tt.CurrentIndicator = 1
		WHERE tt.id <> (
							select max(itt.ID)
							from lion.TermsTracker itt
							where itt.AccountKey = i.AccountKey and
								itt.ItemGroup3Key = i.ItemGroup3Key and
								ISNULL(itt.ItemKey,0) = ISNULL(i.ItemKey,0)
						)
	end
	else
	begin
		UPDATE tt
		   SET tt.CurrentIndicator = 1
		  FROM lion.TermsTracker tt inner join
			   deleted d on tt.AccountKey = d.AccountKey and
							tt.ItemGroup3Key = d.ItemGroup3Key and
							ISNULL(tt.ItemKey, 0) = ISNULL(d.ItemKey, 0) and 
							tt.ID <> d.ID and
							tt.CurrentIndicator = 0 and
							d.CurrentIndicator = 1
		 WHERE tt.id =  (
							select max(itt.ID)
							from lion.TermsTracker itt
							where itt.AccountKey = d.AccountKey and
								itt.ItemGroup3Key = d.ItemGroup3Key and
								ISNULL(itt.ItemKey,0) = ISNULL(d.ItemKey,0) and
								itt.ID <> d.ID
						)
	end
*/
END


GO
ALTER TABLE [lion].[TermsTracker] ADD CONSTRAINT [PK_TermsTracker] PRIMARY KEY CLUSTERED  ([ID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_22] ON [lion].[TermsTracker] ([AccountKey], [ItemKey]) ON [PRIMARY]
GO
