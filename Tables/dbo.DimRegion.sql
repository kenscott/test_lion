CREATE TABLE [dbo].[DimRegion]
(
[RegionKey] [dbo].[Key_Small_type] NOT NULL IDENTITY(1, 1),
[Region] [dbo].[Description_Small_type] NOT NULL,
[RegionName] [dbo].[UDVarchar_type] NOT NULL,
[CreationDayKey] [smallint] NOT NULL,
[ModificationDayKey] [smallint] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[DimRegion] ADD CONSTRAINT [PK_DimRegion] PRIMARY KEY CLUSTERED  ([RegionKey]) ON [PRIMARY]
GO
