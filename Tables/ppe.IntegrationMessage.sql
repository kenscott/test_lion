CREATE TABLE [ppe].[IntegrationMessage]
(
[IntegrationMessageKey] [dbo].[Key_Normal_type] NOT NULL IDENTITY(1, 1),
[Type] [int] NOT NULL,
[Status] [int] NOT NULL,
[Message] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[DateCreated] [datetime] NOT NULL CONSTRAINT [DF_IntegrationMessage_DateCreated] DEFAULT (getdate()),
[DeliveryAttempts] [int] NOT NULL CONSTRAINT [DF_IntegrationMessage_Retries] DEFAULT ((0)),
[LastErrorTimestamp] [datetime] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [ppe].[IntegrationMessage] ADD CONSTRAINT [PK_IntegrationMessage] PRIMARY KEY CLUSTERED  ([IntegrationMessageKey]) ON [PRIMARY]
GO
