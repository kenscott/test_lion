CREATE TABLE [dbo].[DimArea]
(
[AreaKey] [dbo].[Key_Small_type] NOT NULL IDENTITY(1, 1),
[Area] [dbo].[Description_Small_type] NOT NULL,
[AreaName] [dbo].[UDVarchar_type] NOT NULL,
[CreationDayKey] [smallint] NOT NULL,
[ModificationDayKey] [smallint] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[DimArea] ADD CONSTRAINT [PK_DimArea] PRIMARY KEY CLUSTERED  ([AreaKey]) ON [PRIMARY]
GO
