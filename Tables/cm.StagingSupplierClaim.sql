CREATE TABLE [cm].[StagingSupplierClaim]
(
[SupplierID] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ClaimMonth] [int] NULL,
[DeferredDate] [date] NULL
) ON [PRIMARY]
GO
