CREATE TABLE [dbo].[WebUserAppState]
(
[WebUserAppStateKey] [dbo].[Key_Normal_type] NOT NULL IDENTITY(1, 1),
[WebUserKey] [dbo].[Key_Normal_type] NOT NULL,
[Prefix] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Name] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Value] [nvarchar] (4000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreationDate] [dbo].[DateTime_Type] NOT NULL CONSTRAINT [DF_WebUserAppState_CreationDate] DEFAULT (getdate()),
[ModificationDate] [dbo].[DateTime_Type] NOT NULL CONSTRAINT [DF_WebUserAppState_ModificationDate] DEFAULT (getdate())
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[WebUserAppState] ADD CONSTRAINT [PK_WebUserAppState] PRIMARY KEY CLUSTERED  ([WebUserAppStateKey]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[WebUserAppState] ADD CONSTRAINT [FK_WebUserAppState_1] FOREIGN KEY ([WebUserKey]) REFERENCES [dbo].[WebUser] ([WebUserKey])
GO
