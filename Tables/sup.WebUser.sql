CREATE TABLE [sup].[WebUser]
(
[WebUserKey] [dbo].[Key_Normal_type] NOT NULL IDENTITY(1, 1),
[UserName] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CreationDate] [datetime] NOT NULL CONSTRAINT [DF_WebUser_Creatio_6E78BA7E] DEFAULT (getdate()),
[ModificationDate] [datetime] NOT NULL CONSTRAINT [DF_WebUser_Modific_6F6CDEB7] DEFAULT (getdate()),
[CreationUser] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModificationUser] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[VendorKey] [int] NULL,
[supplierIdHeader] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[loginIdHeader] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[rolesHeader] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[firstName] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[lastName] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[emailAddress] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [sup].[WebUser] ADD CONSTRAINT [PK_WebUser] PRIMARY KEY CLUSTERED  ([WebUserKey]) ON [PRIMARY]
GO
