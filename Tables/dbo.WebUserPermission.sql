CREATE TABLE [dbo].[WebUserPermission]
(
[WebUserPermissionKey] [dbo].[Key_Normal_type] NOT NULL IDENTITY(1, 1),
[WebUserKey] [dbo].[Key_Normal_type] NOT NULL,
[WebPermissionKey] [dbo].[Key_Normal_type] NOT NULL,
[CreationDate] [dbo].[DateTime_Type] NOT NULL CONSTRAINT [DF__WebUserPermission] DEFAULT (getdate())
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[WebUserPermission] ADD CONSTRAINT [PK_WebUserPermission] PRIMARY KEY CLUSTERED  ([WebUserPermissionKey]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [I_WebUserPermission_2] ON [dbo].[WebUserPermission] ([WebPermissionKey]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [I_WebUserPermission_1] ON [dbo].[WebUserPermission] ([WebUserKey]) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [UI_WebUserPermission_1] ON [dbo].[WebUserPermission] ([WebUserKey], [WebPermissionKey]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[WebUserPermission] ADD CONSTRAINT [FK_WebUserPermission_1] FOREIGN KEY ([WebUserKey]) REFERENCES [dbo].[WebUser] ([WebUserKey])
GO
ALTER TABLE [dbo].[WebUserPermission] ADD CONSTRAINT [FK_WebUserPermission_2] FOREIGN KEY ([WebPermissionKey]) REFERENCES [dbo].[WebPermission] ([WebPermissionKey])
GO
