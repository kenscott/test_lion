CREATE TABLE [ppe].[RecommendCriteriaCapType]
(
[RecommendCriteriaCapTypeKey] [dbo].[Key_Normal_type] NOT NULL IDENTITY(1, 1),
[CapType] [dbo].[Description_Small_type] NOT NULL,
[CapTypeDescription] [dbo].[Description_Small_type] NULL
) ON [PRIMARY]
GO
ALTER TABLE [ppe].[RecommendCriteriaCapType] ADD CONSTRAINT [PK_RecommendCriteriaCapType] PRIMARY KEY NONCLUSTERED  ([RecommendCriteriaCapTypeKey]) ON [PRIMARY]
GO
