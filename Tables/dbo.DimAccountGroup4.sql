CREATE TABLE [dbo].[DimAccountGroup4]
(
[AccountGroup4Key] [dbo].[Key_Normal_type] NOT NULL IDENTITY(1, 1),
[AG4Level1] [dbo].[Description_Small_type] NOT NULL,
[AG4Level2] [dbo].[Description_Small_type] NOT NULL CONSTRAINT [DF_DimAccountGroup4_Level2] DEFAULT ('None'),
[AG4Level3] [dbo].[Description_Small_type] NOT NULL CONSTRAINT [DF_DimAccountGroup4_Level3] DEFAULT ('None'),
[AG4Level4] [dbo].[Description_Small_type] NOT NULL CONSTRAINT [DF_DimAccountGroup4_Level4] DEFAULT ('None'),
[AG4Level5] [dbo].[Description_Small_type] NOT NULL CONSTRAINT [DF_DimAccountGroup4_Level5] DEFAULT ('None'),
[AG4Level1UDVarchar1] [dbo].[UDVarchar_type] NULL,
[AG4Level1UDVarchar2] [dbo].[UDVarchar_type] NULL,
[AG4Level1UDVarchar3] [dbo].[UDVarchar_type] NULL,
[AG4Level2UDVarchar1] [dbo].[UDVarchar_type] NULL,
[AG4Level2UDVarchar2] [dbo].[UDVarchar_type] NULL,
[AG4Level2UDVarchar3] [dbo].[UDVarchar_type] NULL,
[AG4Level3UDVarchar1] [dbo].[UDVarchar_type] NULL,
[AG4Level3UDVarchar2] [dbo].[UDVarchar_type] NULL,
[AG4Level3UDVarchar3] [dbo].[UDVarchar_type] NULL,
[AG4Level4UDVarchar1] [dbo].[UDVarchar_type] NULL,
[AG4Level4UDVarchar2] [dbo].[UDVarchar_type] NULL,
[AG4Level4UDVarchar3] [dbo].[UDVarchar_type] NULL,
[AG4Level5UDVarchar1] [dbo].[UDVarchar_type] NULL,
[AG4Level5UDVarchar2] [dbo].[UDVarchar_type] NULL,
[AG4Level5UDVarchar3] [dbo].[UDVarchar_type] NULL,
[CreationDayKey] [dbo].[DateKey_type] NOT NULL,
[ModificationDayKey] [dbo].[DateKey_type] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[DimAccountGroup4] ADD CONSTRAINT [PK_DimAccountGroup4] PRIMARY KEY CLUSTERED  ([AccountGroup4Key]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [I_DimAccountGroup4_1] ON [dbo].[DimAccountGroup4] ([AG4Level1]) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [UI_DimAccountGroup4] ON [dbo].[DimAccountGroup4] ([AG4Level1], [AG4Level2], [AG4Level3], [AG4Level4], [AG4Level5]) ON [PRIMARY]
GO
