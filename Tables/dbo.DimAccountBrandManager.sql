CREATE TABLE [dbo].[DimAccountBrandManager]
(
[AccountBrandManagerKey] [dbo].[Key_Normal_type] NOT NULL IDENTITY(1, 1),
[AccountKey] [dbo].[Key_Normal_type] NOT NULL,
[BrandKey] [dbo].[Key_Small_type] NOT NULL,
[AccountManagerKey] [dbo].[Key_Normal_type] NOT NULL,
[CreationDayKey] [dbo].[Key_Small_type] NOT NULL,
[ModificationDayKey] [dbo].[Key_Small_type] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[DimAccountBrandManager] ADD CONSTRAINT [PK_DimAccountBrandManager] PRIMARY KEY NONCLUSTERED  ([AccountBrandManagerKey]) ON [PRIMARY]
GO
CREATE UNIQUE CLUSTERED INDEX [I_DimAccountBrandManager_AccountBrand] ON [dbo].[DimAccountBrandManager] ([AccountKey], [BrandKey]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [I_DimAccountBrandManager_AccountManagerKey] ON [dbo].[DimAccountBrandManager] ([AccountManagerKey]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[DimAccountBrandManager] ADD CONSTRAINT [FK_DimAccountBrandManager_AccountKey] FOREIGN KEY ([AccountKey]) REFERENCES [dbo].[DimAccount] ([AccountKey])
GO
ALTER TABLE [dbo].[DimAccountBrandManager] ADD CONSTRAINT [FK_DimAccountBrandManager_AccountManagerKey] FOREIGN KEY ([AccountManagerKey]) REFERENCES [dbo].[DimAccountManager] ([AccountManagerKey])
GO
ALTER TABLE [dbo].[DimAccountBrandManager] ADD CONSTRAINT [FK_DimAccountBrandManager_BrandKey] FOREIGN KEY ([BrandKey]) REFERENCES [dbo].[DimBrand] ([BrandKey])
GO
ALTER TABLE [dbo].[DimAccountBrandManager] ADD CONSTRAINT [FK_DimAccountBrandManager_CreationDayKey] FOREIGN KEY ([CreationDayKey]) REFERENCES [dbo].[DimDay] ([DayKey])
GO
ALTER TABLE [dbo].[DimAccountBrandManager] ADD CONSTRAINT [FK_DimAccountBrandManager_ModificationDayKey] FOREIGN KEY ([ModificationDayKey]) REFERENCES [dbo].[DimDay] ([DayKey])
GO
