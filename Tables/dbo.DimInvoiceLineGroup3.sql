CREATE TABLE [dbo].[DimInvoiceLineGroup3]
(
[InvoiceLineGroup3Key] [dbo].[Key_Normal_type] NOT NULL IDENTITY(1, 1),
[InvoiceLineGroup3UDVarchar1] [dbo].[UDVarchar_type] NOT NULL,
[InvoiceLineGroup3UDVarchar2] [dbo].[UDVarchar_type] NOT NULL,
[InvoiceLineGroup3UDVarchar3] [dbo].[UDVarchar_type] NOT NULL,
[InvoiceLineGroup3UDVarchar4] [dbo].[UDVarchar_type] NOT NULL,
[InvoiceLineGroup3UDVarchar5] [dbo].[UDVarchar_type] NOT NULL,
[CreationDayKey] [dbo].[DateKey_type] NOT NULL,
[ModificationDayKey] [dbo].[DateKey_type] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[DimInvoiceLineGroup3] ADD CONSTRAINT [PK_DimInvoiceLineGroup3] PRIMARY KEY CLUSTERED  ([InvoiceLineGroup3Key]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [I_DimInvoiceLineGroup3_1] ON [dbo].[DimInvoiceLineGroup3] ([InvoiceLineGroup3UDVarchar1]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [I_DimInvoiceLineGroup3_UD2] ON [dbo].[DimInvoiceLineGroup3] ([InvoiceLineGroup3UDVarchar1]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [I_DimInvoiceLineGroup3_2] ON [dbo].[DimInvoiceLineGroup3] ([InvoiceLineGroup3UDVarchar2]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [I_DimInvoiceLineGroup3_UD3] ON [dbo].[DimInvoiceLineGroup3] ([InvoiceLineGroup3UDVarchar2]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [I_DimInvoiceLineGroup3_3] ON [dbo].[DimInvoiceLineGroup3] ([InvoiceLineGroup3UDVarchar3]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [I_DimInvoiceLineGroup3_UD4] ON [dbo].[DimInvoiceLineGroup3] ([InvoiceLineGroup3UDVarchar3]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [I_DimInvoiceLineGroup3_4] ON [dbo].[DimInvoiceLineGroup3] ([InvoiceLineGroup3UDVarchar4]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [I_DimInvoiceLineGroup3_UD5] ON [dbo].[DimInvoiceLineGroup3] ([InvoiceLineGroup3UDVarchar4]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [I_DimInvoiceLineGroup3_5] ON [dbo].[DimInvoiceLineGroup3] ([InvoiceLineGroup3UDVarchar5]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [I_DimInvoiceLineGroup3_UD6] ON [dbo].[DimInvoiceLineGroup3] ([InvoiceLineGroup3UDVarchar5]) ON [PRIMARY]
GO
