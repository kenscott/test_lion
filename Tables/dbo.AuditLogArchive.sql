CREATE TABLE [dbo].[AuditLogArchive]
(
[AuditLogKey] [dbo].[Int_Type] NOT NULL,
[AuditSubject] [dbo].[Description_Normal_type] NULL,
[AuditAction] [dbo].[Description_Normal_type] NULL,
[AuditActor] [dbo].[Description_Normal_type] NULL,
[AuditActorName] [dbo].[Description_Normal_type] NULL,
[AuditModule] [dbo].[Description_Normal_type] NULL,
[AuditDetails] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreationDate] [dbo].[DateTime_Type] NULL,
[WebUserKey] [dbo].[Key_Normal_type] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[AuditLogArchive] ADD CONSTRAINT [PK__AuditLogArchive_07172017] PRIMARY KEY CLUSTERED  ([AuditLogKey]) ON [PRIMARY]
GO
