CREATE TABLE [cm].[FactInvoiceLineLog]
(
[FactInvoiceLineLogKey] [int] NOT NULL IDENTITY(1, 1),
[WebUserKey] [int] NULL,
[ClientInvoiceLineUniqueID] [int] NULL,
[Notes] [varchar] (4000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreationDate] [datetime] NOT NULL,
[ClaimAction] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [cm].[FactInvoiceLineLog] ADD CONSTRAINT [PK_FactInvoiceLog] PRIMARY KEY NONCLUSTERED  ([FactInvoiceLineLogKey]) ON [PRIMARY]
GO
