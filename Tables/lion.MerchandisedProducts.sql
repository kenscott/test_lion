CREATE TABLE [lion].[MerchandisedProducts]
(
[ItemKey] [dbo].[Key_Normal_type] NOT NULL,
[ItemNumber] [dbo].[Description_Small_type] NULL
) ON [PRIMARY]
GO
ALTER TABLE [lion].[MerchandisedProducts] ADD CONSTRAINT [PK_MerchandisedProducts] PRIMARY KEY CLUSTERED  ([ItemKey]) ON [PRIMARY]
GO
