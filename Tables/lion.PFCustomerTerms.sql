CREATE TABLE [lion].[PFCustomerTerms]
(
[PFCustomerTermsKey] [dbo].[Key_Normal_type] NOT NULL IDENTITY(1, 1),
[CustomerTermsKey] [dbo].[Key_Normal_type] NOT NULL,
[AccountKey] [dbo].[Key_Normal_type] NOT NULL CONSTRAINT [DF_PFCustomerTerms_AccountKey] DEFAULT ((1)),
[ItemKey] [dbo].[Key_Normal_type] NOT NULL CONSTRAINT [DF_PFCustomerTerms_ItemKey] DEFAULT ((1)),
[ItemGroup3Key] [dbo].[Key_Normal_type] NOT NULL CONSTRAINT [DF_PFCustomerTerms_ItemGroup3Key] DEFAULT ((1)),
[AccountId] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PyramidCode] [dbo].[UDVarchar_type] NULL,
[SPGCode] [dbo].[UDVarchar_type] NULL,
[OwningBrandCode] [dbo].[UDVarchar_type] NULL,
[Usualdiscount1] [decimal] (38, 8) NULL,
[Usualdiscount2] [decimal] (38, 8) NULL,
[DeleteFlag] [dbo].[UDVarchar_type] NULL,
[LastChangeInitials] [dbo].[UDVarchar_type] NULL,
[LastChangeDate] [dbo].[UDVarchar_type] NULL,
[LastChangeTime] [dbo].[UDVarchar_type] NULL,
[LastChangePLID] [dbo].[UDVarchar_type] NULL,
[LastChangeBranch] [dbo].[UDVarchar_type] NULL,
[ExceptionProduct] [dbo].[UDVarchar_type] NULL,
[ExceptionDiscount] [decimal] (38, 8) NULL,
[ExceptionFixedPrice] [decimal] (38, 8) NULL,
[ExceptionFromDate] [dbo].[UDVarchar_type] NULL,
[ExceptionToDate] [dbo].[UDVarchar_type] NULL,
[ExceptionPCF] [dbo].[UDVarchar_type] NULL,
[FallbackMarginFlag] [bit] NULL CONSTRAINT [DF_PFCustomerTerms_FallbackMarginFlag] DEFAULT ((0))
) ON [PRIMARY]
GO
ALTER TABLE [lion].[PFCustomerTerms] ADD CONSTRAINT [PK_PFCustomerTerms] PRIMARY KEY NONCLUSTERED  ([PFCustomerTermsKey]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_PFCustomerTerms_1] ON [lion].[PFCustomerTerms] ([AccountKey], [ItemKey], [ItemGroup3Key]) INCLUDE ([ExceptionDiscount], [ExceptionFixedPrice], [Usualdiscount1], [Usualdiscount2]) ON [PRIMARY]
GO
ALTER TABLE [lion].[PFCustomerTerms] ADD CONSTRAINT [FK_PFCustomerTerms_1] FOREIGN KEY ([AccountKey]) REFERENCES [dbo].[DimAccount] ([AccountKey])
GO
ALTER TABLE [lion].[PFCustomerTerms] ADD CONSTRAINT [FK_PFCustomerTerms_2] FOREIGN KEY ([ItemKey]) REFERENCES [dbo].[DimItem] ([ItemKey])
GO
ALTER TABLE [lion].[PFCustomerTerms] ADD CONSTRAINT [FK_PFCustomerTerms_3] FOREIGN KEY ([ItemGroup3Key]) REFERENCES [dbo].[DimItemGroup3] ([ItemGroup3Key])
GO
