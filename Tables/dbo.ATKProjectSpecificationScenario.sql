CREATE TABLE [dbo].[ATKProjectSpecificationScenario]
(
[ProjectSpecificationScenarioKey] [dbo].[Key_Normal_type] NOT NULL IDENTITY(1, 1),
[ProjectSpecificationKey] [dbo].[Key_Normal_type] NOT NULL,
[ScenarioKey] [dbo].[Key_Normal_type] NOT NULL,
[Version] [dbo].[Int_Type] NOT NULL CONSTRAINT [DF__ATKProjec__Versi__50686953] DEFAULT ((0)),
[CreationDate] [dbo].[DateTime_Type] NOT NULL CONSTRAINT [DF__ATKProjec__Creat__6B1C5F8F] DEFAULT (getdate()),
[ModificationDate] [dbo].[DateTime_Type] NOT NULL CONSTRAINT [DF__ATKProjec__Modif__05D055CB] DEFAULT (getdate()),
[CreationUser] [dbo].[Description_Normal_type] NULL,
[ModificationUser] [dbo].[Description_Normal_type] NULL,
[ModificationUserAddress] [dbo].[Description_Normal_type] NULL,
[CreationUserAddress] [dbo].[Description_Normal_type] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ATKProjectSpecificationScenario] ADD CONSTRAINT [PK_ATKProjectScenario] PRIMARY KEY CLUSTERED  ([ProjectSpecificationScenarioKey]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ATKProjectSpecificationScenario] ADD CONSTRAINT [FK_ATKProjectSpecificationScenario_1] FOREIGN KEY ([ProjectSpecificationKey]) REFERENCES [dbo].[ATKProjectSpecification] ([ProjectSpecificationKey])
GO
ALTER TABLE [dbo].[ATKProjectSpecificationScenario] ADD CONSTRAINT [FK_ATKProjectSpecificationScenario_2] FOREIGN KEY ([ScenarioKey]) REFERENCES [dbo].[ATKScenario] ([ScenarioKey])
GO
