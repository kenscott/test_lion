CREATE TABLE [dbo].[DBA_IndexRebuild_Log]
(
[TableName] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Command] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Statement] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[BeginTime] [datetime] NOT NULL,
[EndTime] [datetime] NULL,
[DBAID] [int] NOT NULL IDENTITY(1, 1)
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[DBA_IndexRebuild_Log] ADD CONSTRAINT [PK_DBA_IndexRebuild_Log] PRIMARY KEY CLUSTERED  ([DBAID]) ON [PRIMARY]
GO
