CREATE TABLE [ppe].[RecommendAccountOwner]
(
[RecommendAccountOwnerKey] [int] NOT NULL IDENTITY(1, 1),
[RecommendKey] [int] NOT NULL,
[AccountManagerKey] [dbo].[Key_Normal_type] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [ppe].[RecommendAccountOwner] ADD CONSTRAINT [PK_RecommendAccountOwner] PRIMARY KEY CLUSTERED  ([RecommendAccountOwnerKey]) ON [PRIMARY]
GO
ALTER TABLE [ppe].[RecommendAccountOwner] ADD CONSTRAINT [FK_Recommend_AccountOwner] FOREIGN KEY ([RecommendKey]) REFERENCES [ppe].[Recommend] ([RecommendKey])
GO
ALTER TABLE [ppe].[RecommendAccountOwner] ADD CONSTRAINT [FK_RecommendAccount_DAM] FOREIGN KEY ([AccountManagerKey]) REFERENCES [dbo].[DimAccountManager] ([AccountManagerKey])
GO
