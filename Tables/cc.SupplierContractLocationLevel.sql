CREATE TABLE [cc].[SupplierContractLocationLevel]
(
[ContractLocationLevelKey] [tinyint] NOT NULL IDENTITY(1, 1),
[LocationLevelName] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [cc].[SupplierContractLocationLevel] ADD CONSTRAINT [PK_DimContractLocationLevel] PRIMARY KEY NONCLUSTERED  ([ContractLocationLevelKey]) ON [PRIMARY]
GO
