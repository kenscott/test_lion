CREATE TABLE [cg].[PlaybookPricingGroupPriceBand]
(
[PlaybookPricingGroupPriceBandKey] [dbo].[Key_Normal_type] NOT NULL IDENTITY(1, 1),
[PlaybookPricingGroupKey] [dbo].[Key_Normal_type] NOT NULL,
[PlaybookDatapointKey] [dbo].[Key_Normal_type] NOT NULL,
[ClaimBackPercent] [dbo].[Percent_Type] NULL,
[MemberRank] [int] NOT NULL,
[RulingMemberCount] [int] NOT NULL,
[PricingRuleSequenceNumber] [tinyint] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [cg].[PlaybookPricingGroupPriceBand] ADD CONSTRAINT [PK_PlaybookPricingGroupPriceBandKey] PRIMARY KEY NONCLUSTERED  ([PlaybookPricingGroupPriceBandKey]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [I_PlaybookPricingGroupPriceBand_5] ON [cg].[PlaybookPricingGroupPriceBand] ([PlaybookPricingGroupKey]) INCLUDE ([ClaimBackPercent]) ON [PRIMARY]
GO
CREATE UNIQUE CLUSTERED INDEX [UCI_PlaybookPricingGroupPriceBand_1] ON [cg].[PlaybookPricingGroupPriceBand] ([PlaybookPricingGroupKey], [MemberRank], [RulingMemberCount], [ClaimBackPercent]) ON [PRIMARY]
GO
ALTER TABLE [cg].[PlaybookPricingGroupPriceBand] ADD CONSTRAINT [FK_PlaybookPricingGroupPriceBand_2] FOREIGN KEY ([PlaybookPricingGroupKey]) REFERENCES [dbo].[PlaybookPricingGroup] ([PlaybookPricingGroupKey])
GO
