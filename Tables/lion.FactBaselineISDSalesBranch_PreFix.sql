CREATE TABLE [lion].[FactBaselineISDSalesBranch_PreFix]
(
[ItemKey] [dbo].[Key_Normal_type] NOT NULL,
[InvoiceLineGroup1Key] [dbo].[Key_Normal_type] NOT NULL,
[AccountGroup1Key] [dbo].[Key_Normal_type] NOT NULL,
[Sales] [decimal] (38, 8) NULL,
[Cost] [decimal] (38, 8) NULL,
[GP] [decimal] (38, 8) NULL,
[BaselineGPP] [decimal] (38, 6) NULL
) ON [PRIMARY]
GO
ALTER TABLE [lion].[FactBaselineISDSalesBranch_PreFix] ADD CONSTRAINT [PK_FactBaselineISDSalesBranch_PreFix] PRIMARY KEY CLUSTERED  ([ItemKey], [InvoiceLineGroup1Key], [AccountGroup1Key]) ON [PRIMARY]
GO
