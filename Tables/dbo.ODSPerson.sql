CREATE TABLE [dbo].[ODSPerson]
(
[ODSPersonKey] [dbo].[Key_Normal_type] NOT NULL IDENTITY(1, 1),
[FullName] [dbo].[Description_Normal_type] NOT NULL,
[Email] [dbo].[Description_Normal_type] NULL,
[ExtID] [dbo].[Description_Normal_type] NOT NULL,
[ODSPersonUDVarchar1] [dbo].[UDVarchar_type] NULL,
[ODSPersonUDVarchar2] [dbo].[UDVarchar_type] NULL,
[ODSPersonUDVarchar3] [dbo].[UDVarchar_type] NULL,
[LevelId] [dbo].[UDVarchar_type] NULL,
[EmployeeID] [dbo].[Description_Normal_type] NULL,
[ReceivesEmailIndicator] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF_ODSPerson_ReceivesEmailIndicator] DEFAULT ('Y')
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ODSPerson] ADD CONSTRAINT [PK_ODSPerson] PRIMARY KEY CLUSTERED  ([ODSPersonKey]) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [I_ODSPerson_2] ON [dbo].[ODSPerson] ([Email]) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [I_ODSPerson_1] ON [dbo].[ODSPerson] ([ExtID]) ON [PRIMARY]
GO
