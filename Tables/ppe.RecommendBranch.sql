CREATE TABLE [ppe].[RecommendBranch]
(
[RecommendBranchKey] [int] NOT NULL IDENTITY(1, 1),
[RecommendKey] [int] NOT NULL,
[AG1Level1] [dbo].[Description_Small_type] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [ppe].[RecommendBranch] ADD CONSTRAINT [PK_RecommendBranch ] PRIMARY KEY CLUSTERED  ([RecommendBranchKey]) ON [PRIMARY]
GO
ALTER TABLE [ppe].[RecommendBranch] ADD CONSTRAINT [FK_Recommend_Branch] FOREIGN KEY ([RecommendKey]) REFERENCES [ppe].[Recommend] ([RecommendKey])
GO
ALTER TABLE [ppe].[RecommendBranch] ADD CONSTRAINT [FK_RecommendBranch_AG1] FOREIGN KEY ([AG1Level1]) REFERENCES [dbo].[DimAccountGroup1] ([AG1Level1])
GO
