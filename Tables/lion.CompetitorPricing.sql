CREATE TABLE [lion].[CompetitorPricing]
(
[ItemKey] [dbo].[Key_Normal_type] NOT NULL,
[Retailer] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[SKU] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Price] [money] NULL
) ON [PRIMARY]
GO
ALTER TABLE [lion].[CompetitorPricing] ADD CONSTRAINT [PK_CompetitorPricing] PRIMARY KEY CLUSTERED  ([ItemKey], [Retailer]) ON [PRIMARY]
GO
