CREATE TABLE [dbo].[PlaybookProjectAccountExclusion]
(
[PlaybookProjectAccountExclusionKey] [dbo].[Key_Normal_type] NOT NULL IDENTITY(1, 1),
[ProjectKey] [dbo].[Key_Normal_type] NOT NULL,
[AccountKey] [dbo].[Key_Normal_type] NOT NULL,
[WebUserKey] [dbo].[Key_Normal_type] NOT NULL,
[ExclusionType] [dbo].[Description_Small_type] NULL,
[ExclusionReason] [dbo].[Description_Big_type] NULL,
[Version] [dbo].[Int_Type] NOT NULL CONSTRAINT [DF__PlaybookP__Versi__1E0A3414] DEFAULT ((0)),
[CreationDate] [dbo].[DateTime_Type] NOT NULL CONSTRAINT [DF__PlaybookP__Creat__1EFE584D] DEFAULT (getdate()),
[ModificationDate] [dbo].[DateTime_Type] NOT NULL CONSTRAINT [DF__PlaybookP__Modif__1FF27C86] DEFAULT (getdate()),
[CreationUser] [dbo].[Description_Normal_type] NULL,
[ModificationUser] [dbo].[Description_Normal_type] NULL,
[ModificationUserAddress] [dbo].[Description_Normal_type] NULL,
[CreationUserAddress] [dbo].[Description_Normal_type] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[PlaybookProjectAccountExclusion] ADD CONSTRAINT [PK_PlaybookProjectAccountExclusion] PRIMARY KEY NONCLUSTERED  ([PlaybookProjectAccountExclusionKey]) ON [PRIMARY]
GO
CREATE UNIQUE CLUSTERED INDEX [I_PlaybookProjectAccountExclusion_1] ON [dbo].[PlaybookProjectAccountExclusion] ([ProjectKey], [AccountKey]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[PlaybookProjectAccountExclusion] ADD CONSTRAINT [FK_PlaybookProjectAccountExclusion_1] FOREIGN KEY ([AccountKey]) REFERENCES [dbo].[DimAccount] ([AccountKey])
GO
ALTER TABLE [dbo].[PlaybookProjectAccountExclusion] ADD CONSTRAINT [FK_PlaybookProjectAccountExclusion_2] FOREIGN KEY ([WebUserKey]) REFERENCES [dbo].[WebUser] ([WebUserKey])
GO
ALTER TABLE [dbo].[PlaybookProjectAccountExclusion] ADD CONSTRAINT [FK_PlaybookProjectAccountExclusion_3] FOREIGN KEY ([ExclusionType]) REFERENCES [dbo].[ATKExclusionType] ([ExclusionType])
GO
ALTER TABLE [dbo].[PlaybookProjectAccountExclusion] ADD CONSTRAINT [FK_PlaybookProjectAccountExclusion_4] FOREIGN KEY ([ProjectKey]) REFERENCES [dbo].[PlaybookProject] ([ProjectKey])
GO
