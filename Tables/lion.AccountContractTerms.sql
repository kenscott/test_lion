CREATE TABLE [lion].[AccountContractTerms]
(
[ItemGroup3Key] [dbo].[Key_Normal_type] NOT NULL,
[ItemKey] [dbo].[Key_Normal_type] NOT NULL,
[ContractName] [dbo].[UDVarchar_type] NOT NULL,
[SPG] [dbo].[UDVarchar_type] NULL,
[SPGDescription] [dbo].[UDVarchar_type] NULL,
[SPGExpiryDate] [date] NULL,
[UsualDiscount1] [decimal] (38, 14) NULL,
[UsualDiscount2] [decimal] (38, 14) NULL,
[ExceptionProduct] [dbo].[UDVarchar_type] NULL,
[ExceptionProductDescription] [dbo].[UDVarchar_type] NULL,
[ExceptionDiscount] [decimal] (38, 14) NULL,
[ExceptionFixedPrice] [decimal] (19, 3) NULL,
[FixedPricePer] [decimal] (19, 3) NULL,
[ReleaseDate] [date] NULL
) ON [PRIMARY]
GO
ALTER TABLE [lion].[AccountContractTerms] ADD CONSTRAINT [PK_AccountContractTerms] PRIMARY KEY CLUSTERED  ([ItemGroup3Key], [ItemKey], [ContractName]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_2] ON [lion].[AccountContractTerms] ([ItemKey]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_12] ON [lion].[AccountContractTerms] ([SPG]) INCLUDE ([UsualDiscount1]) ON [PRIMARY]
GO
ALTER TABLE [lion].[AccountContractTerms] ADD CONSTRAINT [FK_AccountContractTerms_1] FOREIGN KEY ([ItemKey]) REFERENCES [dbo].[DimItem] ([ItemKey])
GO
ALTER TABLE [lion].[AccountContractTerms] ADD CONSTRAINT [FK_AccountContractTerms_2] FOREIGN KEY ([ItemGroup3Key]) REFERENCES [dbo].[DimItemGroup3] ([ItemGroup3Key])
GO
