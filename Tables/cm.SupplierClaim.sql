CREATE TABLE [cm].[SupplierClaim]
(
[SupplierClaimKey] [int] NOT NULL IDENTITY(1, 1),
[VendorKey] [int] NOT NULL,
[ClaimMonth] [int] NOT NULL,
[CreationDate] [datetime] NULL,
[CreatedByWebUserKey] [int] NULL,
[ModificationDate] [datetime] NULL,
[ModifiedByWebUserKey] [int] NULL,
[ClaimStatus] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BatchKey] [int] NULL,
[LastSentDate] [datetime] NULL,
[DeferredDate] [date] NULL
) ON [PRIMARY]
GO
ALTER TABLE [cm].[SupplierClaim] ADD CONSTRAINT [PK_Claim] PRIMARY KEY CLUSTERED  ([SupplierClaimKey]) ON [PRIMARY]
GO
