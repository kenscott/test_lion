CREATE TABLE [dbo].[DBA_FK_Rebuild]
(
[FKName] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ChildSchemaName] [nvarchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ChildTableName] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[PrimarySchemaName] [nvarchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[PrimaryTableName] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ChildCols] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[PrimaryCols] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[DBA_FK_Rebuild] ADD CONSTRAINT [PK_DBA_FK_Rebuild] PRIMARY KEY CLUSTERED  ([FKName], [ChildSchemaName]) ON [PRIMARY]
GO
