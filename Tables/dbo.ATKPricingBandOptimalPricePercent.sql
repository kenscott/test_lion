CREATE TABLE [dbo].[ATKPricingBandOptimalPricePercent]
(
[PricingBandOptimalPricePercentKey] [dbo].[Key_Normal_type] NOT NULL IDENTITY(1, 1),
[PricingRuleBandKey] [dbo].[Key_Normal_type] NOT NULL,
[OptimalPriceMethodDesc] [dbo].[Description_Small_type] NOT NULL,
[OptimalPricePercent] [dbo].[Percent_Type] NOT NULL,
[OptimalPriceType] [dbo].[Description_Small_type] NOT NULL,
[Version] [dbo].[Int_Type] NOT NULL CONSTRAINT [DF__ATKPricin__Versi__49BB6BC4] DEFAULT ((0)),
[CreationDate] [dbo].[DateTime_Type] NOT NULL CONSTRAINT [DF__ATKPricin__Creat__646F6200] DEFAULT (getdate()),
[ModificationDate] [dbo].[DateTime_Type] NOT NULL CONSTRAINT [DF__ATKPricin__Modif__7F23583C] DEFAULT (getdate()),
[CreationUser] [dbo].[Description_Normal_type] NULL,
[ModificationUser] [dbo].[Description_Normal_type] NULL,
[ModificationUserAddress] [dbo].[Description_Normal_type] NULL,
[CreationUserAddress] [dbo].[Description_Normal_type] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ATKPricingBandOptimalPricePercent] ADD CONSTRAINT [PK_ATKPricingBandTarget] PRIMARY KEY CLUSTERED  ([PricingBandOptimalPricePercentKey]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ATKPricingBandOptimalPricePercent] ADD CONSTRAINT [FK_ATKOptimalPriceType_5] FOREIGN KEY ([OptimalPriceType]) REFERENCES [dbo].[ATKOptimalPriceType] ([OptimalPriceType])
GO
ALTER TABLE [dbo].[ATKPricingBandOptimalPricePercent] ADD CONSTRAINT [FK_ATKPricingBandOptimalPricePercent_1] FOREIGN KEY ([PricingRuleBandKey]) REFERENCES [dbo].[ATKPricingRuleBand] ([PricingRuleBandKey])
GO
