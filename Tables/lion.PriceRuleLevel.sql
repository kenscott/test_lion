CREATE TABLE [lion].[PriceRuleLevel]
(
[Price Rule Level] [int] NOT NULL,
[Description] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [lion].[PriceRuleLevel] ADD CONSTRAINT [PK_Table_1] PRIMARY KEY CLUSTERED  ([Price Rule Level]) ON [PRIMARY]
GO
