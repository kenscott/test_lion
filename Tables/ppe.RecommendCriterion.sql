CREATE TABLE [ppe].[RecommendCriterion]
(
[RecommendCriterionKey] [dbo].[Key_Normal_type] NOT NULL IDENTITY(1, 1),
[Description] [dbo].[Description_Small_type] NOT NULL,
[InputType] [dbo].[Description_Small_type] NULL
) ON [PRIMARY]
GO
ALTER TABLE [ppe].[RecommendCriterion] ADD CONSTRAINT [PK_RecommendCriterion] PRIMARY KEY NONCLUSTERED  ([RecommendCriterionKey]) ON [PRIMARY]
GO
