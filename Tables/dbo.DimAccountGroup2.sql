CREATE TABLE [dbo].[DimAccountGroup2]
(
[AccountGroup2Key] [dbo].[Key_Normal_type] NOT NULL IDENTITY(1, 1),
[AG2Level1] [dbo].[Description_Small_type] NOT NULL,
[AG2Level2] [dbo].[Description_Small_type] NOT NULL CONSTRAINT [DF_DimAccountGroup2_Level2] DEFAULT ('None'),
[AG2Level3] [dbo].[Description_Small_type] NOT NULL CONSTRAINT [DF_DimAccountGroup2_Level3] DEFAULT ('None'),
[AG2Level4] [dbo].[Description_Small_type] NOT NULL CONSTRAINT [DF_DimAccountGroup2_Level4] DEFAULT ('None'),
[AG2Level5] [dbo].[Description_Small_type] NOT NULL CONSTRAINT [DF_DimAccountGroup2_Level5] DEFAULT ('None'),
[AG2Level1UDVarchar1] [dbo].[UDVarchar_type] NULL,
[AG2Level1UDVarchar2] [dbo].[UDVarchar_type] NULL,
[AG2Level1UDVarchar3] [dbo].[UDVarchar_type] NULL,
[AG2Level2UDVarchar1] [dbo].[UDVarchar_type] NULL,
[AG2Level2UDVarchar2] [dbo].[UDVarchar_type] NULL,
[AG2Level2UDVarchar3] [dbo].[UDVarchar_type] NULL,
[AG2Level3UDVarchar1] [dbo].[UDVarchar_type] NULL,
[AG2Level3UDVarchar2] [dbo].[UDVarchar_type] NULL,
[AG2Level3UDVarchar3] [dbo].[UDVarchar_type] NULL,
[AG2Level4UDVarchar1] [dbo].[UDVarchar_type] NULL,
[AG2Level4UDVarchar2] [dbo].[UDVarchar_type] NULL,
[AG2Level4UDVarchar3] [dbo].[UDVarchar_type] NULL,
[AG2Level5UDVarchar1] [dbo].[UDVarchar_type] NULL,
[AG2Level5UDVarchar2] [dbo].[UDVarchar_type] NULL,
[AG2Level5UDVarchar3] [dbo].[UDVarchar_type] NULL,
[CreationDayKey] [dbo].[DateKey_type] NOT NULL,
[ModificationDayKey] [dbo].[DateKey_type] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[DimAccountGroup2] ADD CONSTRAINT [PK_DimMiscGroup2] PRIMARY KEY CLUSTERED  ([AccountGroup2Key]) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [I_DimAccountGroup2_6] ON [dbo].[DimAccountGroup2] ([AccountGroup2Key]) INCLUDE ([AG2Level1]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [I_DimAccountGroup2_1] ON [dbo].[DimAccountGroup2] ([AG2Level1]) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [UI_DimMiscGroup2_1] ON [dbo].[DimAccountGroup2] ([AG2Level1], [AG2Level2], [AG2Level3], [AG2Level4], [AG2Level5]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [I_DimAccountGroup2_UD7] ON [dbo].[DimAccountGroup2] ([AG2Level1UDVarchar1]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [I_DimAccountGroup2_UD8] ON [dbo].[DimAccountGroup2] ([AG2Level1UDVarchar2]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [I_DimAccountGroup2_UD9] ON [dbo].[DimAccountGroup2] ([AG2Level1UDVarchar3]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [I_DimAccountGroup2_2] ON [dbo].[DimAccountGroup2] ([AG2Level2]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [I_DimAccountGroup2_UD12] ON [dbo].[DimAccountGroup2] ([AG2Level2UDVarchar1]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [I_DimAccountGroup2_UD13] ON [dbo].[DimAccountGroup2] ([AG2Level2UDVarchar2]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [I_DimAccountGroup2_UD14] ON [dbo].[DimAccountGroup2] ([AG2Level2UDVarchar3]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [I_DimAccountGroup2_3] ON [dbo].[DimAccountGroup2] ([AG2Level3]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [I_DimAccountGroup2_UD17] ON [dbo].[DimAccountGroup2] ([AG2Level3UDVarchar1]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [I_DimAccountGroup2_UD18] ON [dbo].[DimAccountGroup2] ([AG2Level3UDVarchar2]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [I_DimAccountGroup2_UD19] ON [dbo].[DimAccountGroup2] ([AG2Level3UDVarchar3]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [I_DimAccountGroup2_4] ON [dbo].[DimAccountGroup2] ([AG2Level4]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [I_DimAccountGroup2_UD22] ON [dbo].[DimAccountGroup2] ([AG2Level4UDVarchar1]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [I_DimAccountGroup2_UD23] ON [dbo].[DimAccountGroup2] ([AG2Level4UDVarchar2]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [I_DimAccountGroup2_UD24] ON [dbo].[DimAccountGroup2] ([AG2Level4UDVarchar3]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [I_DimAccountGroup2_5] ON [dbo].[DimAccountGroup2] ([AG2Level5]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [I_DimAccountGroup2_UD27] ON [dbo].[DimAccountGroup2] ([AG2Level5UDVarchar1]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [I_DimAccountGroup2_UD28] ON [dbo].[DimAccountGroup2] ([AG2Level5UDVarchar2]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [I_DimAccountGroup2_UD29] ON [dbo].[DimAccountGroup2] ([AG2Level5UDVarchar3]) ON [PRIMARY]
GO
