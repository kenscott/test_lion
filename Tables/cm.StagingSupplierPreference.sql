CREATE TABLE [cm].[StagingSupplierPreference]
(
[SupplierID] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AllowClaimsOnPending] [bit] NULL,
[AllowClaimReturnCredit] [bit] NULL,
[HistoricalClaimsPeriod] [int] NULL,
[CreditMatchingPeriod] [int] NULL,
[PaymentMethod] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ContactEmail] [nvarchar] (2000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
