CREATE TABLE [ppe].[RequestSource]
(
[SourceKey] [tinyint] NOT NULL IDENTITY(1, 1),
[SourceName] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
