CREATE TABLE [dbo].[DimItemGroup3]
(
[ItemGroup3Key] [dbo].[Key_Normal_type] NOT NULL IDENTITY(1, 1),
[IG3Level1] [dbo].[Description_Small_type] NOT NULL,
[IG3Level2] [dbo].[Description_Small_type] NOT NULL CONSTRAINT [DF_DimItemGroup3_Level2] DEFAULT ('None'),
[IG3Level3] [dbo].[Description_Small_type] NOT NULL CONSTRAINT [DF_DimItemGroup3_Level3] DEFAULT ('None'),
[IG3Level4] [dbo].[Description_Small_type] NOT NULL CONSTRAINT [DF_DimItemGroup3_Level4] DEFAULT ('None'),
[IG3Level5] [dbo].[Description_Small_type] NOT NULL CONSTRAINT [DF_DimItemGroup3_Level5] DEFAULT ('None'),
[IG3Level1UDVarchar1] [dbo].[UDVarchar_type] NULL,
[IG3Level1UDVarchar2] [dbo].[UDVarchar_type] NULL,
[IG3Level1UDVarchar3] [dbo].[UDVarchar_type] NULL,
[IG3Level2UDVarchar1] [dbo].[UDVarchar_type] NULL,
[IG3Level2UDVarchar2] [dbo].[UDVarchar_type] NULL,
[IG3Level2UDVarchar3] [dbo].[UDVarchar_type] NULL,
[IG3Level3UDVarchar1] [dbo].[UDVarchar_type] NULL,
[IG3Level3UDVarchar2] [dbo].[UDVarchar_type] NULL,
[IG3Level3UDVarchar3] [dbo].[UDVarchar_type] NULL,
[IG3Level4UDVarchar1] [dbo].[UDVarchar_type] NULL,
[IG3Level4UDVarchar2] [dbo].[UDVarchar_type] NULL,
[IG3Level4UDVarchar3] [dbo].[UDVarchar_type] NULL,
[IG3Level5UDVarchar1] [dbo].[UDVarchar_type] NULL,
[IG3Level5UDVarchar2] [dbo].[UDVarchar_type] NULL,
[IG3Level5UDVarchar3] [dbo].[UDVarchar_type] NULL,
[IG3Level1UDDecimal1] [dbo].[UDDecimal_type] NULL,
[IG3Level1UDDecimal2] [dbo].[UDDecimal_type] NULL,
[IG3Level2UDDecimal1] [dbo].[UDDecimal_type] NULL,
[IG3Level2UDDecimal2] [dbo].[UDDecimal_type] NULL,
[IG3Level3UDDecimal1] [dbo].[UDDecimal_type] NULL,
[IG3Level3UDDecimal2] [dbo].[UDDecimal_type] NULL,
[IG3Level4UDDecimal1] [dbo].[UDDecimal_type] NULL,
[IG3Level4UDDecimal2] [dbo].[UDDecimal_type] NULL,
[IG3Level5UDDecimal1] [dbo].[UDDecimal_type] NULL,
[IG3Level5UDDecimal2] [dbo].[UDDecimal_type] NULL,
[CreationDayKey] [dbo].[DateKey_type] NOT NULL,
[ModificationDayKey] [dbo].[DateKey_type] NOT NULL,
[BrandKey] [dbo].[Key_Small_type] NOT NULL CONSTRAINT [DF_DimItemGroup3_BrandKey] DEFAULT ((1))
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[DimItemGroup3] ADD CONSTRAINT [PK_DimItemGroup3] PRIMARY KEY CLUSTERED  ([ItemGroup3Key]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [I_DimItemGroup3_1] ON [dbo].[DimItemGroup3] ([IG3Level1]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [I_DimItemGroup3_9] ON [dbo].[DimItemGroup3] ([IG3Level1], [ItemGroup3Key]) INCLUDE ([IG3Level1UDVarchar1]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [I_DimItemGroup3_UD32] ON [dbo].[DimItemGroup3] ([IG3Level1UDDecimal1]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [I_DimItemGroup3_UD33] ON [dbo].[DimItemGroup3] ([IG3Level1UDDecimal2]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [I_DimItemGroup3_UD7] ON [dbo].[DimItemGroup3] ([IG3Level1UDVarchar1]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [I_DimItemGroup3_UD8] ON [dbo].[DimItemGroup3] ([IG3Level1UDVarchar2]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [I_DimItemGroup3_UD9] ON [dbo].[DimItemGroup3] ([IG3Level1UDVarchar3]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [I_DimItemGroup3_2] ON [dbo].[DimItemGroup3] ([IG3Level2]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [I_DimItemGroup3_UD34] ON [dbo].[DimItemGroup3] ([IG3Level2UDDecimal1]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [I_DimItemGroup3_UD35] ON [dbo].[DimItemGroup3] ([IG3Level2UDDecimal2]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [I_DimItemGroup3_UD12] ON [dbo].[DimItemGroup3] ([IG3Level2UDVarchar1]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [I_DimItemGroup3_UD13] ON [dbo].[DimItemGroup3] ([IG3Level2UDVarchar2]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [I_DimItemGroup3_UD14] ON [dbo].[DimItemGroup3] ([IG3Level2UDVarchar3]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [I_DimItemGroup3_3] ON [dbo].[DimItemGroup3] ([IG3Level3]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [I_DimItemGroup3_UD36] ON [dbo].[DimItemGroup3] ([IG3Level3UDDecimal1]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [I_DimItemGroup3_UD37] ON [dbo].[DimItemGroup3] ([IG3Level3UDDecimal2]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [I_DimItemGroup3_UD17] ON [dbo].[DimItemGroup3] ([IG3Level3UDVarchar1]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [I_DimItemGroup3_UD18] ON [dbo].[DimItemGroup3] ([IG3Level3UDVarchar2]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [I_DimItemGroup3_UD19] ON [dbo].[DimItemGroup3] ([IG3Level3UDVarchar3]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [I_DimItemGroup3_4] ON [dbo].[DimItemGroup3] ([IG3Level4]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [I_DimItemGroup3_UD38] ON [dbo].[DimItemGroup3] ([IG3Level4UDDecimal1]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [I_DimItemGroup3_UD39] ON [dbo].[DimItemGroup3] ([IG3Level4UDDecimal2]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [I_DimItemGroup3_UD22] ON [dbo].[DimItemGroup3] ([IG3Level4UDVarchar1]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [I_DimItemGroup3_UD23] ON [dbo].[DimItemGroup3] ([IG3Level4UDVarchar2]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [I_DimItemGroup3_UD24] ON [dbo].[DimItemGroup3] ([IG3Level4UDVarchar3]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [I_DimItemGroup3_5] ON [dbo].[DimItemGroup3] ([IG3Level5]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [I_DimItemGroup3_UD40] ON [dbo].[DimItemGroup3] ([IG3Level5UDDecimal1]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [I_DimItemGroup3_UD41] ON [dbo].[DimItemGroup3] ([IG3Level5UDDecimal2]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [I_DimItemGroup3_UD27] ON [dbo].[DimItemGroup3] ([IG3Level5UDVarchar1]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [I_DimItemGroup3_UD28] ON [dbo].[DimItemGroup3] ([IG3Level5UDVarchar2]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [I_DimItemGroup3_UD29] ON [dbo].[DimItemGroup3] ([IG3Level5UDVarchar3]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [I_DimItemGroup3_8] ON [dbo].[DimItemGroup3] ([ItemGroup3Key]) INCLUDE ([IG3Level1], [IG3Level1UDVarchar1], [IG3Level2], [IG3Level2UDVarchar1]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [I_DimItemGroup3_7] ON [dbo].[DimItemGroup3] ([ItemGroup3Key]) INCLUDE ([IG3Level1], [IG3Level1UDVarchar1], [IG3Level2], [IG3Level2UDVarchar1], [IG3Level3], [IG3Level3UDVarchar1]) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [I_DimItemGroup3_6] ON [dbo].[DimItemGroup3] ([ItemGroup3Key]) INCLUDE ([IG3Level1], [IG3Level4]) ON [PRIMARY]
GO
CREATE STATISTICS [_dta_stat_942626401_2_7] ON [dbo].[DimItemGroup3] ([IG3Level1], [IG3Level1UDVarchar1])
GO
CREATE STATISTICS [_dta_stat_942626401_1_7] ON [dbo].[DimItemGroup3] ([ItemGroup3Key], [IG3Level1UDVarchar1])
GO
ALTER TABLE [dbo].[DimItemGroup3] ADD CONSTRAINT [FK_DimItemGroup3_BrandKey] FOREIGN KEY ([BrandKey]) REFERENCES [dbo].[DimBrand] ([BrandKey])
GO
