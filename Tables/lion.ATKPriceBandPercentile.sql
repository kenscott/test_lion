CREATE TABLE [lion].[ATKPriceBandPercentile]
(
[ATKPriceBandPercentileKey] [dbo].[Key_Normal_type] NOT NULL IDENTITY(1, 1),
[SequenceNumber] [int] NOT NULL,
[Brand] [dbo].[UDVarchar_type] NOT NULL,
[PlaybookRegion] [dbo].[Description_Small_type] NOT NULL,
[Region] [dbo].[UDVarchar_type] NOT NULL CONSTRAINT [DF_ATKPriceBandPercentile_Region] DEFAULT (N'ALL'),
[Area] [dbo].[UDVarchar_type] NOT NULL CONSTRAINT [DF_ATKPriceBandPercentile_Area] DEFAULT (N'ALL'),
[Network] [dbo].[UDVarchar_type] NOT NULL CONSTRAINT [DF_ATKPriceBandPercentile_Network] DEFAULT (N'ALL'),
[Branch] [dbo].[UDVarchar_type] NOT NULL CONSTRAINT [DF_ATKPriceBandPercentile_Branch] DEFAULT (N'ALL'),
[Sales603010Bucket] [dbo].[UDVarchar_type] NOT NULL,
[ItemPyramidCode] [dbo].[UDVarchar_type] NOT NULL,
[LLSPGCode] [dbo].[UDVarchar_type] NOT NULL,
[FloorPercentile] [dbo].[Percent_Type] NOT NULL,
[TargetPercentile] [dbo].[Percent_Type] NOT NULL,
[StretchPercentile] [dbo].[Percent_Type] NOT NULL,
[Offset] [dbo].[Percent_Type] NOT NULL CONSTRAINT [DF_ATKPriceBandPercentile_Offset] DEFAULT ((0)),
[CreationDate] [datetime] NOT NULL,
[CreatedByWebUserKey] [dbo].[Key_Normal_type] NOT NULL,
[ModificationDate] [datetime] NOT NULL CONSTRAINT [DF_ATKPriceBandPercentile_moddate] DEFAULT (getdate()),
[ModifiedByWebUserKey] [dbo].[Key_Normal_type] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [lion].[ATKPriceBandPercentile] ADD CONSTRAINT [PK_ATKPriceBandPercentile] PRIMARY KEY CLUSTERED  ([ATKPriceBandPercentileKey]) ON [PRIMARY]
GO
