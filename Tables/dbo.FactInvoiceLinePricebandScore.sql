CREATE TABLE [dbo].[FactInvoiceLinePricebandScore]
(
[ScoreKey] [int] NOT NULL IDENTITY(1, 1),
[AccountKey] [dbo].[Key_Normal_type] NOT NULL,
[AlternateAccountKey] [dbo].[Key_Normal_type] NOT NULL,
[ItemKey] [dbo].[Key_Normal_type] NOT NULL,
[VendorKey] [dbo].[Key_Normal_type] NOT NULL,
[AccountManagerKey] [dbo].[Key_Normal_type] NOT NULL,
[InvoiceDateDayKey] [dbo].[DateKey_type] NOT NULL,
[InvoiceDateMonthKey] [dbo].[DateKey_type] NOT NULL,
[InvoiceLineGroup1Key] [dbo].[Key_Normal_type] NOT NULL,
[InvoiceLineGroup2Key] [dbo].[Key_Normal_type] NOT NULL,
[InvoiceLineGroup3Key] [dbo].[Key_Normal_type] NOT NULL,
[InvoiceLineJunkKey] [dbo].[Key_Normal_type] NOT NULL,
[AccountGroup1Key] [dbo].[Key_Normal_type] NOT NULL,
[AccountGroup2Key] [dbo].[Key_Normal_type] NOT NULL,
[AccountGroup3Key] [dbo].[Key_Normal_type] NOT NULL,
[ItemGroup1Key] [dbo].[Key_Normal_type] NOT NULL,
[ItemGroup2Key] [dbo].[Key_Normal_type] NOT NULL,
[ItemGroup3Key] [dbo].[Key_Normal_type] NOT NULL,
[InvoiceLineUniqueIdentifier] [dbo].[Key_Normal_type] NOT NULL,
[LinkedInvoiceLineUniqueIdentifier] [dbo].[Key_Normal_type] NULL,
[ClientInvoiceID] [dbo].[Description_Small_type] NOT NULL,
[ClientInvoiceLineID] [dbo].[Description_Small_type] NOT NULL,
[ClientInvoiceLineUniqueID] [int] NOT NULL,
[TotalQuantity] [dbo].[Quantity_Normal_type] NOT NULL,
[TotalActualPrice] [dbo].[Money_Normal_Type] NOT NULL,
[TotalActualCost] [dbo].[Money_Normal_Type] NOT NULL,
[UnitListPrice] [dbo].[Money_Normal_Type] NOT NULL,
[TotalNetQuantity] [dbo].[Quantity_Normal_type] NULL,
[TotalNetPrice] [dbo].[Money_Normal_Type] NULL,
[TotalNetCost] [dbo].[Money_Normal_Type] NULL,
[RedPrice] [dbo].[Money_Normal_Type] NULL,
[AmberPrice] [dbo].[Money_Normal_Type] NULL,
[GreenPrice] [dbo].[Money_Normal_Type] NULL,
[BluePrice] [dbo].[Money_Normal_Type] NULL,
[TerminalEmulator] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PriceGuidanceIndicator] [nvarchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[UDVarchar1] [dbo].[UDVarchar_Small_type] NULL,
[UDDecimal1] [dbo].[UDDecimal_type] NULL,
[UDDecimal2] [dbo].[UDDecimal_type] NULL,
[FloorGPP] [decimal] (19, 8) NULL,
[TargetGPP] [decimal] (19, 8) NULL,
[StretchGPP] [decimal] (19, 8) NULL,
[Score] [decimal] (19, 8) NULL,
[RPBPlaybookPricingGroupKey] [dbo].[Key_Normal_type] NULL,
[FloorImpact] [decimal] (19, 8) NULL,
[TargetImpact] [decimal] (19, 8) NULL,
[StretchImpact] [decimal] (19, 8) NULL,
[ClaimNo] [dbo].[UDVarchar_Small_type] NULL,
[TotalSupplierCost] [dbo].[Money_Normal_Type] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[FactInvoiceLinePricebandScore] ADD CONSTRAINT [PK_FactInvoiceLinePricebandScore] PRIMARY KEY NONCLUSTERED  ([ScoreKey]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_1] ON [dbo].[FactInvoiceLinePricebandScore] ([AccountGroup1Key]) INCLUDE ([AccountKey], [FloorGPP], [InvoiceLineGroup1Key], [InvoiceLineGroup2Key], [InvoiceLineGroup3Key], [ItemKey], [RPBPlaybookPricingGroupKey], [StretchGPP], [TargetGPP], [TotalActualCost], [TotalActualPrice], [TotalNetCost], [TotalNetPrice], [TotalNetQuantity], [TotalQuantity]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_17] ON [dbo].[FactInvoiceLinePricebandScore] ([AccountGroup1Key]) INCLUDE ([AccountKey], [FloorImpact], [InvoiceDateDayKey], [InvoiceLineGroup1Key], [InvoiceLineJunkKey], [InvoiceLineUniqueIdentifier], [ItemKey], [PriceGuidanceIndicator], [Score], [TerminalEmulator], [TotalActualCost], [TotalActualPrice], [TotalQuantity], [UDVarchar1], [VendorKey]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [I_FactInvoiceLinePricebandScore_5] ON [dbo].[FactInvoiceLinePricebandScore] ([AccountGroup1Key], [AccountKey], [ItemKey], [InvoiceLineGroup1Key]) INCLUDE ([FloorGPP], [StretchGPP], [TargetGPP], [TotalActualCost], [TotalActualPrice], [TotalQuantity], [UnitListPrice]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [I_FactInvoiceLinePricebandScore_6] ON [dbo].[FactInvoiceLinePricebandScore] ([AccountKey], [ItemKey]) INCLUDE ([FloorGPP], [StretchGPP], [TargetGPP], [TotalQuantity]) ON [PRIMARY]
GO
CREATE CLUSTERED INDEX [I_FactInvoiceLinePricebandScore_1] ON [dbo].[FactInvoiceLinePricebandScore] ([AccountKey], [ItemKey], [InvoiceLineGroup1Key]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_11] ON [dbo].[FactInvoiceLinePricebandScore] ([AccountKey], [ItemKey], [VendorKey], [InvoiceLineGroup1Key]) INCLUDE ([InvoiceLineJunkKey], [InvoiceLineUniqueIdentifier]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_15] ON [dbo].[FactInvoiceLinePricebandScore] ([InvoiceDateDayKey]) INCLUDE ([AccountKey], [InvoiceLineGroup1Key], [ItemKey], [UDVarchar1]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_18] ON [dbo].[FactInvoiceLinePricebandScore] ([InvoiceDateMonthKey], [TotalQuantity], [TotalActualPrice], [UDVarchar1]) INCLUDE ([AccountGroup1Key], [AccountGroup2Key], [AccountGroup3Key], [AccountKey], [AccountManagerKey], [AlternateAccountKey], [AmberPrice], [BluePrice], [ClientInvoiceID], [ClientInvoiceLineID], [ClientInvoiceLineUniqueID], [FloorGPP], [FloorImpact], [GreenPrice], [InvoiceDateDayKey], [InvoiceLineGroup1Key], [InvoiceLineGroup2Key], [InvoiceLineGroup3Key], [InvoiceLineJunkKey], [InvoiceLineUniqueIdentifier], [ItemGroup1Key], [ItemGroup2Key], [ItemGroup3Key], [ItemKey], [LinkedInvoiceLineUniqueIdentifier], [PriceGuidanceIndicator], [RedPrice], [RPBPlaybookPricingGroupKey], [Score], [StretchGPP], [StretchImpact], [TargetGPP], [TargetImpact], [TerminalEmulator], [TotalActualCost], [TotalNetCost], [TotalNetPrice], [TotalNetQuantity], [UnitListPrice], [VendorKey]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [I_FactInvoiceLinePricebandScore_3] ON [dbo].[FactInvoiceLinePricebandScore] ([InvoiceLineGroup1Key]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_2] ON [dbo].[FactInvoiceLinePricebandScore] ([InvoiceLineGroup3Key]) INCLUDE ([AccountGroup1Key], [AccountKey], [InvoiceLineGroup1Key], [InvoiceLineGroup2Key], [ItemKey], [RPBPlaybookPricingGroupKey]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [I_FactInvoiceLinePriceBand_Score_6] ON [dbo].[FactInvoiceLinePricebandScore] ([InvoiceLineGroup3Key], [AccountKey], [ItemKey], [InvoiceLineGroup1Key], [RPBPlaybookPricingGroupKey], [ItemGroup3Key], [InvoiceDateDayKey], [InvoiceLineGroup2Key], [AccountGroup1Key]) INCLUDE ([FloorGPP], [InvoiceLineUniqueIdentifier], [StretchGPP], [TargetGPP], [TotalActualCost], [TotalActualPrice], [TotalNetCost], [TotalNetPrice], [TotalNetQuantity], [TotalQuantity], [UDVarchar1]) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_19] ON [dbo].[FactInvoiceLinePricebandScore] ([InvoiceLineUniqueIdentifier]) INCLUDE ([FloorGPP], [RPBPlaybookPricingGroupKey], [Score], [StretchGPP], [TargetGPP]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [I_FactInvoiceLinePricebandScore_2] ON [dbo].[FactInvoiceLinePricebandScore] ([ItemKey]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [I_FactInvoiceLinePricebandScore_4] ON [dbo].[FactInvoiceLinePricebandScore] ([RPBPlaybookPricingGroupKey]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [I_FactInvoiceLinePriceBand_Score_7] ON [dbo].[FactInvoiceLinePricebandScore] ([RPBPlaybookPricingGroupKey], [ScoreKey], [InvoiceLineGroup3Key], [InvoiceLineGroup2Key], [ItemKey], [AccountKey]) INCLUDE ([FloorGPP], [FloorImpact], [StretchGPP], [StretchImpact], [TargetGPP], [TargetImpact], [TotalActualCost], [TotalActualPrice], [TotalNetCost], [TotalNetPrice], [TotalNetQuantity], [TotalQuantity]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_16] ON [dbo].[FactInvoiceLinePricebandScore] ([TotalQuantity], [TotalActualPrice], [UDVarchar1]) INCLUDE ([AccountGroup1Key], [AccountKey], [FloorImpact], [InvoiceDateDayKey], [InvoiceLineGroup1Key], [InvoiceLineJunkKey], [InvoiceLineUniqueIdentifier], [ItemKey], [PriceGuidanceIndicator], [Score], [TerminalEmulator], [TotalActualCost], [VendorKey]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_10] ON [dbo].[FactInvoiceLinePricebandScore] ([TotalQuantity], [TotalActualPrice], [UDVarchar1]) INCLUDE ([AccountKey], [ClientInvoiceID], [FloorGPP], [InvoiceDateDayKey], [InvoiceLineGroup1Key], [InvoiceLineJunkKey], [InvoiceLineUniqueIdentifier], [ItemKey], [RPBPlaybookPricingGroupKey], [Score], [StretchGPP], [TargetGPP], [TotalActualCost], [UnitListPrice]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_14] ON [dbo].[FactInvoiceLinePricebandScore] ([UDVarchar1]) INCLUDE ([AccountKey], [InvoiceDateDayKey], [InvoiceLineGroup1Key], [ItemKey]) ON [PRIMARY]
GO
CREATE STATISTICS [_dta_stat_1887345788_37_2_4_9_18_7_10_11_13] ON [dbo].[FactInvoiceLinePricebandScore] ([AccountKey], [InvoiceLineGroup1Key], [ItemGroup3Key], [InvoiceDateDayKey], [InvoiceLineGroup2Key], [InvoiceLineGroup3Key], [RPBPlaybookPricingGroupKey], [AccountGroup1Key], [ItemKey])
GO
CREATE STATISTICS [_dta_stat_1887345788_7_2_4_9_37] ON [dbo].[FactInvoiceLinePricebandScore] ([InvoiceDateDayKey], [AccountKey], [ItemKey], [InvoiceLineGroup1Key], [RPBPlaybookPricingGroupKey])
GO
CREATE STATISTICS [_dta_stat_1887345788_9_2] ON [dbo].[FactInvoiceLinePricebandScore] ([InvoiceLineGroup1Key], [AccountKey])
GO
CREATE STATISTICS [_dta_stat_1887345788_18_2_4_9] ON [dbo].[FactInvoiceLinePricebandScore] ([ItemGroup3Key], [AccountKey], [ItemKey], [InvoiceLineGroup1Key])
GO
CREATE STATISTICS [_dta_stat_1887345788_13_2_4_9_37_18_7_10] ON [dbo].[FactInvoiceLinePricebandScore] ([ItemGroup3Key], [InvoiceDateDayKey], [AccountGroup1Key], [InvoiceLineGroup2Key], [AccountKey], [ItemKey], [RPBPlaybookPricingGroupKey], [InvoiceLineGroup1Key])
GO
CREATE STATISTICS [_dta_stat_1887345788_10_2_4_9_37_18] ON [dbo].[FactInvoiceLinePricebandScore] ([ItemGroup3Key], [InvoiceLineGroup2Key], [AccountKey], [ItemKey], [InvoiceLineGroup1Key], [RPBPlaybookPricingGroupKey])
GO
