CREATE TABLE [cc].[SupplierContractNumberCounter]
(
[Originator] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ContractNumberCounter] [int] NULL
) ON [PRIMARY]
GO
CREATE UNIQUE CLUSTERED INDEX [i_originator] ON [cc].[SupplierContractNumberCounter] ([Originator]) ON [PRIMARY]
GO
