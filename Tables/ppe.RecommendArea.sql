CREATE TABLE [ppe].[RecommendArea]
(
[RecommendAreaKey] [int] NOT NULL IDENTITY(1, 1),
[RecommendKey] [int] NOT NULL,
[AG1Level3] [dbo].[Description_Small_type] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [ppe].[RecommendArea] ADD CONSTRAINT [PK_RecommendArea] PRIMARY KEY CLUSTERED  ([RecommendAreaKey]) ON [PRIMARY]
GO
ALTER TABLE [ppe].[RecommendArea] ADD CONSTRAINT [FK_Recommend_Area] FOREIGN KEY ([RecommendKey]) REFERENCES [ppe].[Recommend] ([RecommendKey])
GO
