CREATE TABLE [cm].[SupplierClaimStatus]
(
[SupplierClaimStatusKey] [int] NOT NULL IDENTITY(1, 1),
[SupplierClaimStatus] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SupplierClaimStatusDescription] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [cm].[SupplierClaimStatus] ADD CONSTRAINT [PK_SupplierClaimStatus] PRIMARY KEY CLUSTERED  ([SupplierClaimStatusKey]) ON [PRIMARY]
GO
