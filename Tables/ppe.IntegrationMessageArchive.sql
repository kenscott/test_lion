CREATE TABLE [ppe].[IntegrationMessageArchive]
(
[IntegrationMessageKey] [dbo].[Key_Normal_type] NOT NULL,
[Type] [int] NOT NULL,
[Status] [int] NOT NULL,
[Message] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[DateCreated] [datetime] NOT NULL,
[DeliveryAttempts] [int] NOT NULL,
[LastErrorTimestamp] [datetime] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [ppe].[IntegrationMessageArchive] ADD CONSTRAINT [PK_IntegrationMessageArchive] PRIMARY KEY CLUSTERED  ([IntegrationMessageKey]) ON [PRIMARY]
GO
