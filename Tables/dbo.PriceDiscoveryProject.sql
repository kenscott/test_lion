CREATE TABLE [dbo].[PriceDiscoveryProject]
(
[ProjectKey] [int] NOT NULL,
[CreationDate] [datetime] NOT NULL CONSTRAINT [DF__PriceDisc__Creat__2B7F66B9] DEFAULT (getdate()),
[ModicationDate] [datetime] NOT NULL CONSTRAINT [DF__PriceDisc__Modic__2C738AF2] DEFAULT (getdate()),
[PlaybookDAtaPointGroupKey] [int] NOT NULL
) ON [PRIMARY]
GO
