CREATE TABLE [dbo].[ATKPricingRuleAttributeType]
(
[PricingRuleAttributeTypeKey] [dbo].[Key_Normal_type] NOT NULL IDENTITY(1, 1),
[PricingRuleAttributeType] [dbo].[Description_Normal_type] NOT NULL,
[DisplayName] [dbo].[Description_Normal_type] NOT NULL,
[PricingRuleAttributeSourceSchema] [nvarchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF_ATKPricingRuleAttributeType_PricingRuleAttributeSourceSchema] DEFAULT (N'dbo'),
[PricingRuleAttributeSourceTable] [dbo].[Description_Normal_type] NULL,
[PricingRuleAttributeJoinClause1] [dbo].[Description_Normal_type] NOT NULL CONSTRAINT [DF_ATKPricingRuleAttributeType_PricingRuleAttributeJoinClause] DEFAULT (''),
[PricingRuleAttributeJoinClause2] [dbo].[Description_Normal_type] NOT NULL CONSTRAINT [DF_ATKPricingRuleAttributeType_PricingRuleAttributeJoinClause2] DEFAULT (''),
[DefaultValueForMatch] [dbo].[Description_Normal_type] NULL,
[Version] [dbo].[Int_Type] NOT NULL CONSTRAINT [DF__ATKPricin__Versi__4C97D86F] DEFAULT ((0)),
[CreationDate] [dbo].[DateTime_Type] NOT NULL CONSTRAINT [DF__ATKPricin__Creat__674BCEAB] DEFAULT ((0)),
[ModificationDate] [dbo].[DateTime_Type] NOT NULL CONSTRAINT [DF__ATKPricin__Modif__01FFC4E7] DEFAULT (getdate()),
[CreationUser] [dbo].[Description_Normal_type] NULL,
[ModificationUser] [dbo].[Description_Normal_type] NULL,
[ModificationUserAddress] [dbo].[Description_Normal_type] NULL,
[CreationUserAddress] [dbo].[Description_Normal_type] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ATKPricingRuleAttributeType] ADD CONSTRAINT [PK_ATKPricingRuleAttributeType] PRIMARY KEY CLUSTERED  ([PricingRuleAttributeTypeKey]) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [UI_2] ON [dbo].[ATKPricingRuleAttributeType] ([PricingRuleAttributeSourceTable], [PricingRuleAttributeType]) ON [PRIMARY]
GO
