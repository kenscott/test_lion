CREATE TABLE [sup].[WebRolePermission]
(
[WebRolePermissionKey] [dbo].[Key_Normal_type] NOT NULL IDENTITY(1, 1),
[WebRoleKey] [dbo].[Key_Normal_type] NOT NULL,
[WebPermissionKey] [dbo].[Key_Normal_type] NOT NULL,
[CreationDate] [dbo].[DateTime_Type] NOT NULL CONSTRAINT [DF_WebUserPermission] DEFAULT (getdate())
) ON [PRIMARY]
GO
ALTER TABLE [sup].[WebRolePermission] ADD CONSTRAINT [PK_WebRolePermission] PRIMARY KEY CLUSTERED  ([WebRolePermissionKey]) ON [PRIMARY]
GO
