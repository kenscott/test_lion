CREATE TABLE [dbo].[DimAccountBrandSegment]
(
[AccountBrandSegmentKey] [dbo].[Key_Normal_type] NOT NULL IDENTITY(1, 1),
[AccountKey] [dbo].[Key_Normal_type] NOT NULL,
[BrandKey] [dbo].[Key_Small_type] NOT NULL,
[Segment] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CreationDayKey] [smallint] NOT NULL,
[ModificationDayKey] [smallint] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[DimAccountBrandSegment] ADD CONSTRAINT [PK_DimAccountBrandSegment] PRIMARY KEY CLUSTERED  ([AccountBrandSegmentKey]) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [I_DimAccountBrandSegment_1] ON [dbo].[DimAccountBrandSegment] ([AccountKey], [BrandKey], [Segment]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[DimAccountBrandSegment] ADD CONSTRAINT [FK_DimAccountBrandSegment_1] FOREIGN KEY ([AccountKey]) REFERENCES [dbo].[DimAccount] ([AccountKey])
GO
ALTER TABLE [dbo].[DimAccountBrandSegment] ADD CONSTRAINT [FK_DimAccountBrandSegment_2] FOREIGN KEY ([BrandKey]) REFERENCES [dbo].[DimBrand] ([BrandKey])
GO
