CREATE TABLE [cc].[SupplierContractImportLocation]
(
[SupplierContractImportLocationKey] [int] NOT NULL IDENTITY(1, 1),
[BatchKey] [int] NOT NULL,
[SequenceNumber] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[LocationValue] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [cc].[SupplierContractImportLocation] ADD CONSTRAINT [PK_SupplierContractImportLocation] PRIMARY KEY CLUSTERED  ([SupplierContractImportLocationKey]) ON [PRIMARY]
GO
