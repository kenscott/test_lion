CREATE TABLE [lion].[MDXFetch_WalletShare]
(
[Playbook Area] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Customer] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AccountNumber] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[HLSPG] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LLSPG] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Strategic Item] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Contract Claims Indicator] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Sales603010 Bucket] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Sales] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Discount Off Trade] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[GP] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[GP%] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[GP% @ Green] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[GP% @ Red] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[# of Invoice Lines] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
