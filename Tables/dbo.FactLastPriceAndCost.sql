CREATE TABLE [dbo].[FactLastPriceAndCost]
(
[AccountKey] [dbo].[Key_Normal_type] NOT NULL,
[ItemKey] [dbo].[Key_Normal_type] NOT NULL,
[InvoiceLineGroup1Key] [dbo].[Key_Normal_type] NOT NULL,
[LastSaleDayKey] [dbo].[Key_Small_type] NOT NULL,
[LastItemPrice] [dbo].[Money_Normal_Type] NULL,
[LastItemCost] [dbo].[Money_Normal_Type] NULL,
[LastQuantity] [dbo].[Quantity_Normal_type] NULL,
[LastItemRebate] [dbo].[Money_Normal_Type] NULL,
[LastItemDiscountPercent] [dbo].[Percent_Type] NULL,
[LastInvoiceLineGroup2Key] [dbo].[Key_Normal_type] NULL,
[LastInvoiceLineGroup3Key] [dbo].[Key_Normal_type] NULL,
[LastInvoiceLineJunkKey] [dbo].[Key_Normal_type] NULL,
[LastClientInvoiceID] [dbo].[Description_Small_type] NULL,
[LastClientInvoiceLineID] [dbo].[Description_Small_type] NULL,
[LastInvoiceLineUniqueIdentifier] [dbo].[Key_Normal_type] NULL,
[RPBPlaybookPricingGroupKey] [dbo].[Key_Normal_type] NULL,
[AlternateRPBPlaybookPricingGroupKey] [dbo].[Key_Normal_type] NULL,
[Total12MonthInvoices] [dbo].[Int_Type] NULL,
[Total12MonthInvoiceLines] [dbo].[Int_Type] NULL,
[Total12MonthSales] [dbo].[Money_Normal_Type] NULL,
[Total12MonthCost] [dbo].[Money_Normal_Type] NULL,
[Total12MonthQuantity] [dbo].[Quantity_Normal_type] NULL,
[PercentAccountItemSalesToAccountSales] [dbo].[Percent_Type] NULL,
[AverageQuantityPerOrder] [dbo].[UDDecimal_type] NULL,
[AISToAccountSalesRowRanking] [dbo].[Percent_Type] NULL,
[LikeItemCodePercent] [dbo].[Percent_Type] NULL,
[LikeItemCodeTotalInvoiceLines] [int] NULL,
[LikeItemCodeTotalSales] [dbo].[Money_Normal_Type] NULL,
[UDVarchar1] [dbo].[UDVarchar_Small_type] NULL,
[UDVarchar2] [dbo].[UDVarchar_Small_type] NULL,
[UDVarchar3] [dbo].[UDVarchar_Small_type] NULL,
[UDVarchar4] [dbo].[UDVarchar_Small_type] NULL,
[UDVarchar5] [dbo].[UDVarchar_Small_type] NULL,
[UDVarchar6] [dbo].[UDVarchar_Small_type] NULL,
[UDVarchar7] [dbo].[UDVarchar_Small_type] NULL,
[UDVarchar8] [dbo].[UDVarchar_Small_type] NULL,
[UDVarchar9] [dbo].[UDVarchar_Small_type] NULL,
[UDVarchar10] [dbo].[UDVarchar_Small_type] NULL,
[UDDecimal1] [dbo].[UDDecimal_type] NULL,
[UDDecimal2] [dbo].[UDDecimal_type] NULL,
[UDDecimal3] [dbo].[UDDecimal_type] NULL,
[UDDecimal4] [dbo].[UDDecimal_type] NULL,
[UDDecimal5] [dbo].[UDDecimal_type] NULL,
[UDDecimal6] [dbo].[UDDecimal_type] NULL,
[UDDecimal7] [dbo].[UDDecimal_type] NULL,
[UDDecimal8] [dbo].[UDDecimal_type] NULL,
[UDDecimal9] [dbo].[UDDecimal_type] NULL,
[UDDecimal10] [dbo].[UDDecimal_type] NULL,
[UDDateTime1] [dbo].[UDDateTime_type] NULL,
[UDDateTime2] [dbo].[UDDateTime_type] NULL,
[AlternateLastInvoiceLineUniqueIdentifier] [dbo].[Key_Normal_type] NULL,
[CustomerBrandSegment] [dbo].[UDVarchar_Small_type] NULL,
[LastPriceDerivationCode] [dbo].[UDVarchar_Small_type] NULL,
[LastSupplierCost] [dbo].[Money_Normal_Type] NULL,
[CustomerSize] [dbo].[UDVarchar_Small_type] NULL CONSTRAINT [DF_FactLastPriceAndCost_CustomerSize] DEFAULT (N'4-Low')
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[FactLastPriceAndCost] ADD CONSTRAINT [PK_FactLastPriceAndCost] PRIMARY KEY CLUSTERED  ([AccountKey], [ItemKey], [InvoiceLineGroup1Key]) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [I_FactLastPriceAndCost_12] ON [dbo].[FactLastPriceAndCost] ([AccountKey], [ItemKey], [InvoiceLineGroup1Key]) INCLUDE ([AlternateRPBPlaybookPricingGroupKey], [LastInvoiceLineGroup2Key], [LastItemCost], [LastItemPrice], [LastSaleDayKey], [RPBPlaybookPricingGroupKey], [UDVarchar1], [UDVarchar10], [UDVarchar4]) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [I_FactLastPriceAndCost_6] ON [dbo].[FactLastPriceAndCost] ([AccountKey], [ItemKey], [InvoiceLineGroup1Key]) INCLUDE ([LastInvoiceLineGroup2Key]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [_dta_index_FactLastPriceAndCost_5_47339233__K1_K2_K3_K14_5_6_28_29_31] ON [dbo].[FactLastPriceAndCost] ([AccountKey], [ItemKey], [InvoiceLineGroup1Key], [LastClientInvoiceLineID]) INCLUDE ([LastItemCost], [LastItemPrice], [UDVarchar1], [UDVarchar2], [UDVarchar4]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [I_FactLastPriceAndCost_3] ON [dbo].[FactLastPriceAndCost] ([AccountKey], [ItemKey], [InvoiceLineGroup1Key], [LastInvoiceLineJunkKey]) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [I_FactLastPriceAndCost_5] ON [dbo].[FactLastPriceAndCost] ([AccountKey] DESC, [ItemKey] DESC, [InvoiceLineGroup1Key] DESC, [LastItemCost] DESC, [LastItemPrice] DESC, [LastSaleDayKey] DESC, [LastInvoiceLineGroup2Key] DESC, [LastInvoiceLineJunkKey] DESC) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [I_FactLastPriceAndCost_4] ON [dbo].[FactLastPriceAndCost] ([AccountKey], [ItemKey], [InvoiceLineGroup1Key], [UDVarchar5], [UDVarchar6]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [I_FactLastPriceAndCost_10] ON [dbo].[FactLastPriceAndCost] ([ItemKey]) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [I_FactLastPriceAndCost_11] ON [dbo].[FactLastPriceAndCost] ([ItemKey], [InvoiceLineGroup1Key], [LastInvoiceLineGroup2Key], [LastInvoiceLineGroup3Key], [AccountKey], [UDVarchar2], [UDVarchar1], [UDVarchar4]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [I_FactLastPriceAndCost_1] ON [dbo].[FactLastPriceAndCost] ([ItemKey], [LastSaleDayKey], [LastItemCost]) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [I_FactLastPriceAndCost_8] ON [dbo].[FactLastPriceAndCost] ([LastInvoiceLineGroup2Key], [LastInvoiceLineGroup3Key], [InvoiceLineGroup1Key], [AccountKey], [ItemKey]) INCLUDE ([UDVarchar1], [UDVarchar2], [UDVarchar4]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [I_FactLastPriceAndCost_9] ON [dbo].[FactLastPriceAndCost] ([LastInvoiceLineGroup3Key]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [I_FactLastPriceAndCost_2] ON [dbo].[FactLastPriceAndCost] ([LastItemPrice], [LastItemCost]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [I_FactLastPriceAndCost_7] ON [dbo].[FactLastPriceAndCost] ([RPBPlaybookPricingGroupKey]) ON [PRIMARY]
GO
CREATE STATISTICS [_dta_stat_120387498_1_3_15_2] ON [dbo].[FactLastPriceAndCost] ([AccountKey], [InvoiceLineGroup1Key], [LastInvoiceLineUniqueIdentifier], [ItemKey])
GO
CREATE STATISTICS [_dta_stat_47339233_3_1] ON [dbo].[FactLastPriceAndCost] ([InvoiceLineGroup1Key], [AccountKey])
GO
CREATE STATISTICS [_dta_stat_47339233_14_1_2_3_10] ON [dbo].[FactLastPriceAndCost] ([LastClientInvoiceLineID], [AccountKey], [ItemKey], [InvoiceLineGroup1Key], [LastInvoiceLineGroup2Key])
GO
CREATE STATISTICS [_dta_stat_47339233_10_1_2] ON [dbo].[FactLastPriceAndCost] ([LastInvoiceLineGroup2Key], [AccountKey], [ItemKey])
GO
CREATE STATISTICS [_dta_stat_120387498_15_1] ON [dbo].[FactLastPriceAndCost] ([LastInvoiceLineUniqueIdentifier], [AccountKey])
GO
CREATE STATISTICS [_dta_stat_120387498_15_2_3] ON [dbo].[FactLastPriceAndCost] ([LastInvoiceLineUniqueIdentifier], [ItemKey], [InvoiceLineGroup1Key])
GO
CREATE STATISTICS [_dta_stat_120387498_29_1_2_3] ON [dbo].[FactLastPriceAndCost] ([UDVarchar1], [AccountKey], [ItemKey], [InvoiceLineGroup1Key])
GO
ALTER TABLE [dbo].[FactLastPriceAndCost] ADD CONSTRAINT [FK_FactLastPriceAndCost_1] FOREIGN KEY ([AccountKey]) REFERENCES [dbo].[DimAccount] ([AccountKey])
GO
ALTER TABLE [dbo].[FactLastPriceAndCost] ADD CONSTRAINT [FK_FactLastPriceAndCost_2] FOREIGN KEY ([ItemKey]) REFERENCES [dbo].[DimItem] ([ItemKey])
GO
ALTER TABLE [dbo].[FactLastPriceAndCost] ADD CONSTRAINT [FK_FactLastPriceAndCost_3] FOREIGN KEY ([InvoiceLineGroup1Key]) REFERENCES [dbo].[DimInvoiceLineGroup1] ([InvoiceLineGroup1Key])
GO
ALTER TABLE [dbo].[FactLastPriceAndCost] ADD CONSTRAINT [FK_FactLastPriceAndCost_45] FOREIGN KEY ([RPBPlaybookPricingGroupKey]) REFERENCES [dbo].[PlaybookPricingGroup] ([PlaybookPricingGroupKey])
GO
