CREATE TABLE [dbo].[ATKExclusionType]
(
[ExclusionType] [dbo].[Description_Small_type] NOT NULL,
[ExclusionTypeDesc] [dbo].[Description_Normal_type] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ATKExclusionType] ADD CONSTRAINT [PK_ATKExclusionType] PRIMARY KEY CLUSTERED  ([ExclusionType]) ON [PRIMARY]
GO
