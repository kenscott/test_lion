CREATE TABLE [cc].[SupplierContractReference]
(
[ContractReferenceKey] [int] NOT NULL IDENTITY(1, 1),
[ContractKey] [int] NOT NULL,
[ContractReference] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[StartDate] [date] NULL,
[ExpirationDate] [date] NULL,
[CreatedByWebUserKey] [int] NULL,
[CreatedOnDT] [datetime] NULL CONSTRAINT [DF__SupplierC__Creat__5ED1F1FB] DEFAULT (getdate()),
[ModifiedByWebUserKey] [int] NULL,
[ModifiedOnDT] [datetime] NULL CONSTRAINT [DF__SupplierC__Modif__5FC61634] DEFAULT (getdate())
) ON [PRIMARY]
GO
ALTER TABLE [cc].[SupplierContractReference] ADD CONSTRAINT [PK_ContractReference] PRIMARY KEY NONCLUSTERED  ([ContractReferenceKey]) ON [PRIMARY]
GO
ALTER TABLE [cc].[SupplierContractReference] ADD CONSTRAINT [Contract_FK1] FOREIGN KEY ([ContractKey]) REFERENCES [cc].[SupplierContract] ([ContractKey])
GO
