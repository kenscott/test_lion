CREATE TABLE [cc].[SupplierContractItem]
(
[ContractItemKey] [int] NOT NULL IDENTITY(1, 1),
[ContractKey] [int] NOT NULL,
[ItemKey] [int] NOT NULL,
[ClaimBackAmount] [decimal] (9, 2) NULL,
[ClaimBackPerc] [decimal] (9, 4) NULL,
[StartDT] [datetime] NULL,
[ExpiryDT] [datetime] NULL,
[ClaimCost] [money] NULL,
[ClaimType] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LimitType] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LimitValue] [decimal] (36, 4) NULL,
[LimitCurrentValue] [decimal] (36, 4) NULL,
[CreatedByWebUserKey] [int] NULL,
[CreatedOnDT] [datetime] NULL CONSTRAINT [DF__DimContra__Creat__3431ACB9] DEFAULT (getdate()),
[ModifiedByWebUserKey] [int] NULL,
[ModifiedOnDT] [datetime] NULL CONSTRAINT [DF__DimContra__Modif__3525D0F2] DEFAULT (getdate()),
[GreenClaimBackPercent] [decimal] (16, 4) NULL,
[AmberClaimBackPercent] [decimal] (16, 4) NULL,
[RedClaimBackPercent] [decimal] (16, 4) NULL,
[GreenClaimBackAmount] [money] NULL,
[AmberClaimBackAmount] [money] NULL,
[RedClaimBackAmount] [money] NULL,
[PlaybookPricingGroupKey] [int] NULL,
[LimitCurrentRatio] [decimal] (16, 4) NULL,
[LimitPreviousRatio] [decimal] (16, 4) NULL,
[CurrentMonthTally] [decimal] (36, 4) NULL,
[CurrentMonthTallyOld] [decimal] (36, 4) NULL,
[PreviousMonthTally] [decimal] (36, 4) NULL
) ON [PRIMARY]
GO
ALTER TABLE [cc].[SupplierContractItem] ADD CONSTRAINT [PK_DimContractItem] PRIMARY KEY NONCLUSTERED  ([ContractItemKey]) ON [PRIMARY]
GO
CREATE CLUSTERED INDEX [i_ContractItemKey] ON [cc].[SupplierContractItem] ([ContractKey], [ItemKey]) ON [PRIMARY]
GO
ALTER TABLE [cc].[SupplierContractItem] ADD CONSTRAINT [Contract_ContractItem_FK1] FOREIGN KEY ([ContractKey]) REFERENCES [cc].[SupplierContract] ([ContractKey])
GO
ALTER TABLE [cc].[SupplierContractItem] ADD CONSTRAINT [Item_ContractItem_FK2] FOREIGN KEY ([ItemKey]) REFERENCES [dbo].[DimItem] ([ItemKey])
GO
