CREATE TABLE [ppe].[RequestType]
(
[TypeKey] [smallint] NOT NULL IDENTITY(1, 1),
[TypeName] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [ppe].[RequestType] ADD CONSTRAINT [PK_RequestType] PRIMARY KEY CLUSTERED  ([TypeKey]) ON [PRIMARY]
GO
