CREATE TABLE [dbo].[PlaybookPriceProposal]
(
[PlaybookPriceProposalKey] [dbo].[Key_Normal_type] NOT NULL IDENTITY(1, 1) NOT FOR REPLICATION,
[PlaybookDataPointOptimalPriceKey] [dbo].[Key_Normal_type] NOT NULL,
[ProposalRecommendationKey] [dbo].[Key_Normal_type] NOT NULL,
[ProposedPrice] [dbo].[Money_Normal_Type] NULL,
[ProposedCost] [dbo].[Money_Normal_Type] NULL,
[CreationDate] [dbo].[DateTime_Type] NOT NULL CONSTRAINT [DF_PlaybookPriceProposal_CreationDate] DEFAULT (getdate()),
[PlaybookDataPointGroupKey] [dbo].[Key_Normal_type] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[PlaybookPriceProposal] ADD CONSTRAINT [PK_PlaybookPriceProposal] PRIMARY KEY CLUSTERED  ([PlaybookPriceProposalKey]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [I_PlaybookPriceProposal_1] ON [dbo].[PlaybookPriceProposal] ([PlaybookDataPointOptimalPriceKey]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [I_PlaybookPriceProposal_2] ON [dbo].[PlaybookPriceProposal] ([ProposalRecommendationKey]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[PlaybookPriceProposal] ADD CONSTRAINT [FK_PlaybookPriceProposal_1] FOREIGN KEY ([PlaybookDataPointOptimalPriceKey]) REFERENCES [dbo].[PlaybookDataPointOptimalPrice] ([PlaybookDataPointOptimalPriceKey])
GO
ALTER TABLE [dbo].[PlaybookPriceProposal] ADD CONSTRAINT [FK_PlaybookPriceProposal_2] FOREIGN KEY ([ProposalRecommendationKey]) REFERENCES [dbo].[ATKProposalRecommendation] ([ProposalRecommendationKey])
GO
