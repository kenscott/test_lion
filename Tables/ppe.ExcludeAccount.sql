CREATE TABLE [ppe].[ExcludeAccount]
(
[ExcludeAccountKey] [int] NOT NULL IDENTITY(1, 1),
[AccountKey] [int] NOT NULL,
[CreatedByWebUserKey] [int] NULL,
[CreationDate] [datetime] NULL CONSTRAINT [DF__ExcludeAc__Creat__2DFD775F] DEFAULT (getdate())
) ON [PRIMARY]
GO
