CREATE TABLE [dbo].[DimVendor]
(
[VendorKey] [dbo].[Key_Normal_type] NOT NULL IDENTITY(1, 1),
[VendorClientUniqueIdentifier] [dbo].[Description_Small_type] NOT NULL,
[VendorNumber] [dbo].[Description_Small_type] NOT NULL,
[VendorDescription] [dbo].[Description_Small_type] NOT NULL,
[VendorUDVarchar1] [dbo].[UDVarchar_type] NULL,
[VendorUDVarchar2] [dbo].[UDVarchar_type] NULL,
[VendorUDVarchar3] [dbo].[UDVarchar_type] NULL,
[VendorUDDateTime1] [dbo].[UDDateTime_type] NULL,
[VendorUDDateTime2] [dbo].[UDDateTime_type] NULL,
[CreationDayKey] [dbo].[DateKey_type] NOT NULL,
[ModificationDayKey] [dbo].[DateKey_type] NOT NULL,
[VendorOracleCode] [dbo].[UDVarchar_Small_type] NULL,
[VendorAddress1] [dbo].[UDVarchar_type] NULL,
[VendorAddress2] [dbo].[UDVarchar_type] NULL,
[VendorAddress3] [dbo].[UDVarchar_type] NULL,
[VendorAddress4] [dbo].[UDVarchar_type] NULL,
[VendorZipCode] [dbo].[UDVarchar_Small_type] NULL,
[VendorComments] [dbo].[UDVarchar_type] NULL,
[Inactive] [bit] NOT NULL CONSTRAINT [DF_DimVendor_Inactive] DEFAULT ((0)),
[AllowAllLocations] [dbo].[UDVarchar_Small_type] NULL,
[AllowAllAccounts] [dbo].[UDVarchar_Small_type] NULL,
[AllowClaimPercent] [bit] NULL,
[AllowClaimAmount] [bit] NULL,
[AllowNetCost] [bit] NULL,
[AllowUnitLimits] [bit] NULL,
[AllowSalesLimits] [bit] NULL,
[AllowClaimsLimits] [bit] NULL,
[AllowClaimsOnPending] [bit] NULL,
[AllowClaimReturnCredits] [bit] NULL,
[PaymentMethod] [dbo].[UDVarchar_Small_type] NULL,
[HistoricalClaimsPeriod] [int] NULL,
[DeferredDate] [datetime] NULL,
[DeferredClaimMonth] [int] NULL,
[CreditMatchingPeriod] [int] NOT NULL CONSTRAINT [DF_DimVendor_CreditMatchingPeriod] DEFAULT ((3)),
[FixedPrice] [bit] NOT NULL CONSTRAINT [DF_DimVendor_FixedPrice] DEFAULT ((1))
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[DimVendor] ADD CONSTRAINT [PK_DimVendor] PRIMARY KEY CLUSTERED  ([VendorKey]) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [UI_DimVendor_1] ON [dbo].[DimVendor] ([VendorClientUniqueIdentifier]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [I_DimVendor_3] ON [dbo].[DimVendor] ([VendorDescription]) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [I_DimVendor_4] ON [dbo].[DimVendor] ([VendorKey]) INCLUDE ([VendorDescription], [VendorNumber]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [I_DimVendor_2] ON [dbo].[DimVendor] ([VendorNumber]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [I_DimVendor_UD8] ON [dbo].[DimVendor] ([VendorUDDateTime1]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [I_DimVendor_UD9] ON [dbo].[DimVendor] ([VendorUDDateTime2]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [I_DimVendor_UD5] ON [dbo].[DimVendor] ([VendorUDVarchar1]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [I_DimVendor_UD6] ON [dbo].[DimVendor] ([VendorUDVarchar2]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [I_DimVendor_UD7] ON [dbo].[DimVendor] ([VendorUDVarchar3]) ON [PRIMARY]
GO
