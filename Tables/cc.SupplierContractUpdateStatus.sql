CREATE TABLE [cc].[SupplierContractUpdateStatus]
(
[UpdateStatusKey] [tinyint] NOT NULL IDENTITY(1, 1),
[UpdateStatusDescription] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [cc].[SupplierContractUpdateStatus] ADD CONSTRAINT [PK_DimContractUpdateStatus] PRIMARY KEY CLUSTERED  ([UpdateStatusKey]) ON [PRIMARY]
GO
