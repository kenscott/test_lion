CREATE TABLE [cc].[SupplierContractStatus]
(
[StatusKey] [tinyint] NOT NULL IDENTITY(1, 1),
[StatusDescription] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[StatusValue] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [cc].[SupplierContractStatus] ADD CONSTRAINT [PK_DimContractStatus] PRIMARY KEY CLUSTERED  ([StatusKey]) ON [PRIMARY]
GO
