CREATE TABLE [lion].[AccountSPG]
(
[AccountKey] [dbo].[Key_Normal_type] NOT NULL,
[LLSPGCode] [dbo].[Description_Normal_type] NOT NULL,
[AccountId] [nvarchar] (25) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [lion].[AccountSPG] ADD CONSTRAINT [PK_AccountSPG_1] PRIMARY KEY CLUSTERED  ([LLSPGCode], [AccountId]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [I_2] ON [lion].[AccountSPG] ([AccountKey], [LLSPGCode]) ON [PRIMARY]
GO
CREATE STATISTICS [_dta_stat_794642074_2_1] ON [lion].[AccountSPG] ([LLSPGCode], [AccountKey])
GO
ALTER TABLE [lion].[AccountSPG] ADD CONSTRAINT [FK_AccountSPG_1] FOREIGN KEY ([AccountKey]) REFERENCES [dbo].[DimAccount] ([AccountKey])
GO
