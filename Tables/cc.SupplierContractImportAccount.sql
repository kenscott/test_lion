CREATE TABLE [cc].[SupplierContractImportAccount]
(
[SupplierContractImportAccountKey] [int] NOT NULL IDENTITY(1, 1),
[BatchKey] [int] NOT NULL,
[SequenceNumber] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[AccountNumber] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [cc].[SupplierContractImportAccount] ADD CONSTRAINT [PK_SupplierContractImportAccount] PRIMARY KEY CLUSTERED  ([SupplierContractImportAccountKey]) ON [PRIMARY]
GO
