CREATE TABLE [dbo].[FactSalesForecast]
(
[AccountKey] [dbo].[Key_Normal_type] NOT NULL,
[ItemKey] [dbo].[Key_Normal_type] NOT NULL,
[ForecastedMonthlyQty] [dbo].[Quantity_Normal_type] NOT NULL,
[InvoiceLineGroup1Key] [dbo].[Key_Normal_type] NOT NULL,
[ForecastType] [dbo].[Description_Small_type] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[FactSalesForecast] ADD CONSTRAINT [PK_FactSalesForecast] PRIMARY KEY CLUSTERED  ([AccountKey], [ItemKey], [InvoiceLineGroup1Key], [ForecastType]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [I_FactSalesForecast_1] ON [dbo].[FactSalesForecast] ([ForecastedMonthlyQty]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [I_FactSalesForecast_9] ON [dbo].[FactSalesForecast] ([InvoiceLineGroup1Key]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [I_FactSalesForecast_7] ON [dbo].[FactSalesForecast] ([ItemKey]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[FactSalesForecast] ADD CONSTRAINT [FK_FactSalesForecast_1] FOREIGN KEY ([AccountKey]) REFERENCES [dbo].[DimAccount] ([AccountKey])
GO
ALTER TABLE [dbo].[FactSalesForecast] ADD CONSTRAINT [FK_FactSalesForecast_2] FOREIGN KEY ([ItemKey]) REFERENCES [dbo].[DimItem] ([ItemKey])
GO
ALTER TABLE [dbo].[FactSalesForecast] ADD CONSTRAINT [FK_FactSalesForecast_4] FOREIGN KEY ([InvoiceLineGroup1Key]) REFERENCES [dbo].[DimInvoiceLineGroup1] ([InvoiceLineGroup1Key])
GO
