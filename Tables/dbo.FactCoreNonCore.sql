CREATE TABLE [dbo].[FactCoreNonCore]
(
[AccountKey] [dbo].[Key_Normal_type] NOT NULL,
[ItemGroup1Key] [dbo].[Key_Normal_type] NOT NULL,
[PercentItemGroup1SalesToTotalAccountSales] [dbo].[Percent_Type] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[FactCoreNonCore] ADD CONSTRAINT [PK_FactCoreNonCoree] PRIMARY KEY CLUSTERED  ([AccountKey], [ItemGroup1Key]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[FactCoreNonCore] ADD CONSTRAINT [FK_FactCoreNonCore_1] FOREIGN KEY ([ItemGroup1Key]) REFERENCES [dbo].[DimItemGroup1] ([ItemGroup1Key])
GO
ALTER TABLE [dbo].[FactCoreNonCore] ADD CONSTRAINT [FK_FactCoreNonCore_2] FOREIGN KEY ([AccountKey]) REFERENCES [dbo].[DimAccount] ([AccountKey])
GO
