CREATE TABLE [lion].[CustomerTermsPrevious]
(
[ChangeAction] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CustomerTermsKey] [dbo].[Key_Normal_type] NULL,
[AccountKey] [dbo].[Key_Normal_type] NULL CONSTRAINT [DF_CustomerTermsPrevious_AccountKey] DEFAULT ((1)),
[ItemKey] [dbo].[Key_Normal_type] NULL CONSTRAINT [DF_CustomerTermsPrevious_ItemKey] DEFAULT ((1)),
[ItemGroup3Key] [dbo].[Key_Normal_type] NULL CONSTRAINT [DF_CustomerTermsPrevious_ItemGroup3Key] DEFAULT ((1)),
[AccountId] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PyramidCode] [dbo].[UDVarchar_type] NULL,
[SPGCode] [dbo].[UDVarchar_type] NULL,
[OwningBrandCode] [dbo].[UDVarchar_type] NULL,
[Usualdiscount1] [decimal] (38, 8) NULL,
[Usualdiscount2] [decimal] (38, 8) NULL,
[DeleteFlag] [dbo].[UDVarchar_type] NULL,
[LastChangeInitials] [dbo].[UDVarchar_type] NULL,
[LastChangeDate] [dbo].[UDVarchar_type] NULL,
[LastChangeTime] [dbo].[UDVarchar_type] NULL,
[LastChangePLID] [dbo].[UDVarchar_type] NULL,
[LastChangeBranch] [dbo].[UDVarchar_type] NULL,
[ExceptionProduct] [dbo].[UDVarchar_type] NULL,
[ExceptionDiscount] [decimal] (38, 8) NULL,
[ExceptionFixedPrice] [decimal] (38, 8) NULL,
[ExceptionFromDate] [dbo].[UDVarchar_type] NULL,
[ExceptionToDate] [dbo].[UDVarchar_type] NULL,
[ExceptionPCF] [dbo].[UDVarchar_type] NULL,
[FallbackMarginFlag] [bit] NULL CONSTRAINT [DF_CustomerTermsPrevious_FallbackMarginFlag] DEFAULT ((0)),
[CreationDayKey] [dbo].[Key_Small_type] NULL CONSTRAINT [DF_CustomerTermsPrevious_CreationDayKey] DEFAULT ((1)),
[ModificationDayKey] [dbo].[Key_Small_type] NULL CONSTRAINT [DF_CustomerTermsPrevious_ModificationDayKey] DEFAULT ((1)),
[CustomerTermsKey_Previous] [dbo].[Key_Normal_type] NULL,
[AccountKey_Previous] [dbo].[Key_Normal_type] NULL CONSTRAINT [DF_CustomerTerms_Previous_AccountKey] DEFAULT ((1)),
[ItemKey_Previous] [dbo].[Key_Normal_type] NULL CONSTRAINT [DF_CustomerTerms_Previous_ItemKey] DEFAULT ((1)),
[ItemGroup3Key_Previous] [dbo].[Key_Normal_type] NULL CONSTRAINT [DF_CustomerTerms_Previous_ItemGroup3Key] DEFAULT ((1)),
[AccountId_Previous] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PyramidCode_Previous] [dbo].[UDVarchar_type] NULL,
[SPGCode_Previous] [dbo].[UDVarchar_type] NULL,
[OwningBrandCode_Previous] [dbo].[UDVarchar_type] NULL,
[Usualdiscount1_Previous] [decimal] (38, 8) NULL,
[Usualdiscount2_Previous] [decimal] (38, 8) NULL,
[DeleteFlag_Previous] [dbo].[UDVarchar_type] NULL,
[LastChangeInitials_Previous] [dbo].[UDVarchar_type] NULL,
[LastChangeDate_Previous] [dbo].[UDVarchar_type] NULL,
[LastChangeTime_Previous] [dbo].[UDVarchar_type] NULL,
[LastChangePLID_Previous] [dbo].[UDVarchar_type] NULL,
[LastChangeBranch_Previous] [dbo].[UDVarchar_type] NULL,
[ExceptionProduct_Previous] [dbo].[UDVarchar_type] NULL,
[ExceptionDiscount_Previous] [decimal] (38, 8) NULL,
[ExceptionFixedPrice_Previous] [decimal] (38, 8) NULL,
[ExceptionFromDate_Previous] [dbo].[UDVarchar_type] NULL,
[ExceptionToDate_Previous] [dbo].[UDVarchar_type] NULL,
[ExceptionPCF_Previous] [dbo].[UDVarchar_type] NULL,
[FallbackMarginFlag_Previous] [bit] NULL CONSTRAINT [DF_CustomerTermsPrevious_FallbackMarginFlag_Previous] DEFAULT ((0)),
[CreationDayKey_Previous] [dbo].[Key_Small_type] NULL CONSTRAINT [DF_CustomerTerms_Previous_CreationDayKey] DEFAULT ((1)),
[ModificationDayKey_Previous] [dbo].[Key_Small_type] NULL CONSTRAINT [DF_CustomerTerms_Previous_ModificationDayKey] DEFAULT ((1))
) ON [PRIMARY]
GO
CREATE CLUSTERED INDEX [i_2] ON [lion].[CustomerTermsPrevious] ([AccountKey], [ItemKey], [ItemGroup3Key]) ON [PRIMARY]
GO
ALTER TABLE [lion].[CustomerTermsPrevious] ADD CONSTRAINT [FK_CustomerTermsPrevious_1] FOREIGN KEY ([AccountKey]) REFERENCES [dbo].[DimAccount] ([AccountKey])
GO
ALTER TABLE [lion].[CustomerTermsPrevious] ADD CONSTRAINT [FK_CustomerTermsPrevious_2] FOREIGN KEY ([ItemKey]) REFERENCES [dbo].[DimItem] ([ItemKey])
GO
ALTER TABLE [lion].[CustomerTermsPrevious] ADD CONSTRAINT [FK_CustomerTermsPrevious_3] FOREIGN KEY ([ModificationDayKey]) REFERENCES [dbo].[DimDay] ([DayKey])
GO
