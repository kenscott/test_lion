CREATE TABLE [dbo].[ATKProposalRecommendation]
(
[ProposalRecommendationKey] [dbo].[Key_Normal_type] NOT NULL IDENTITY(1, 1) NOT FOR REPLICATION,
[ProposalRecommendationPlaybookKey] [dbo].[Key_Normal_type] NOT NULL,
[ProposalRecommendationPassSequenceNumber] [dbo].[Int_255_type] NOT NULL,
[MinCostDecreaseThresholdPercent] [dbo].[Percent_Type] NULL,
[CostDecreaseCapPercent] [dbo].[Percent_Type] NULL,
[MinPriceIncreaseThresholdPercent] [dbo].[Percent_Type] NULL,
[PriceIncreaseCapPercent] [dbo].[Percent_Type] NULL,
[Version] [dbo].[Int_Type] NOT NULL CONSTRAINT [DF__ATKPropos__Versi__515C8D8C] DEFAULT ((0)),
[CreationDate] [dbo].[DateTime_Type] NOT NULL CONSTRAINT [DF__ATKPropos__Creat__6C1083C8] DEFAULT (getdate()),
[ModificationDate] [dbo].[DateTime_Type] NOT NULL CONSTRAINT [DF__ATKPropos__Modif__06C47A04] DEFAULT (getdate()),
[CreationUser] [dbo].[Description_Normal_type] NULL,
[ModificationUser] [dbo].[Description_Normal_type] NULL,
[ModificationUserAddress] [dbo].[Description_Normal_type] NULL,
[CreationUserAddress] [dbo].[Description_Normal_type] NULL,
[WhereClause1] [dbo].[Description_Huge_type] NOT NULL CONSTRAINT [DF_ATKProposalRecommendation_WhereClause1] DEFAULT (''),
[JoinClause1] [dbo].[Description_Huge_type] NOT NULL CONSTRAINT [DF_ATKProposalRecommendation_JoinClause1] DEFAULT (''),
[WhereClause2] [dbo].[Description_Huge_type] NOT NULL CONSTRAINT [DF_ATKProposalRecommendation_WhereClause2] DEFAULT (''),
[JoinClause2] [dbo].[Description_Huge_type] NOT NULL CONSTRAINT [DF_ATKProposalRecommendation_JoinClause2] DEFAULT (''),
[WhereClause3] [dbo].[Description_Huge_type] NOT NULL CONSTRAINT [DF_ATKProposalRecommendation_WhereClause3] DEFAULT (''),
[JoinClause3] [dbo].[Description_Huge_type] NOT NULL CONSTRAINT [DF_ATKProposalRecommendation_JoinClause3] DEFAULT (''),
[AcceptRejectCode_PriceProposal] [dbo].[Description_Big_type] NOT NULL,
[AcceptRejectCode_CostProposal] [dbo].[Description_Big_type] NOT NULL,
[DynamicReasonCode] [dbo].[Description_Big_type] NULL,
[DynamicEffectiveDate] [dbo].[Description_Big_type] NULL,
[DynamicSuppressionDate] [dbo].[Description_Big_type] NULL,
[SMEMinGPP] [dbo].[Percent_Type] NULL,
[SMEMaxGPP] [dbo].[Percent_Type] NULL,
[ProposedPriceOverride] [dbo].[Description_Big_type] NOT NULL CONSTRAINT [DF_ATKProposalRecommendation_ProposedPriceOverride] DEFAULT (''),
[ProposedCostOverride] [dbo].[Description_Big_type] NOT NULL CONSTRAINT [DF_ATKProposalRecommendation_ProposedCostOverride] DEFAULT (''),
[DynamicUDVarchar1] [dbo].[Description_Small_type] NOT NULL CONSTRAINT [DF__ATKPropos__Dynam__28BD9808] DEFAULT (''),
[DynamicUDVarchar2] [dbo].[Description_Small_type] NOT NULL CONSTRAINT [DF_ATKProposalRecommendation_DynamicUDVarchar2] DEFAULT ('')
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ATKProposalRecommendation] ADD CONSTRAINT [PK_ATKAutoSage] PRIMARY KEY CLUSTERED  ([ProposalRecommendationKey]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ATKProposalRecommendation] ADD CONSTRAINT [FK_ATKProposalRecommendation_1] FOREIGN KEY ([ProposalRecommendationPlaybookKey]) REFERENCES [dbo].[ATKProposalRecommendationPlaybook] ([ProposalRecommendationPlaybookKey])
GO
