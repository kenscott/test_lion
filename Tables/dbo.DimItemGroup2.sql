CREATE TABLE [dbo].[DimItemGroup2]
(
[ItemGroup2Key] [dbo].[Key_Normal_type] NOT NULL IDENTITY(1, 1),
[IG2Level1] [dbo].[Description_Small_type] NOT NULL,
[IG2Level2] [dbo].[Description_Small_type] NOT NULL CONSTRAINT [DF_DimItemGroup2_Level2] DEFAULT ('None'),
[IG2Level3] [dbo].[Description_Small_type] NOT NULL CONSTRAINT [DF_DimItemGroup2_Level3] DEFAULT ('None'),
[IG2Level4] [dbo].[Description_Small_type] NOT NULL CONSTRAINT [DF_DimItemGroup2_Level4] DEFAULT ('None'),
[IG2Level5] [dbo].[Description_Small_type] NOT NULL CONSTRAINT [DF_DimItemGroup2_Level5] DEFAULT ('None'),
[IG2Level1UDVarchar1] [dbo].[UDVarchar_type] NULL,
[IG2Level1UDVarchar2] [dbo].[UDVarchar_type] NULL,
[IG2Level1UDVarchar3] [dbo].[UDVarchar_type] NULL,
[IG2Level2UDVarchar1] [dbo].[UDVarchar_type] NULL,
[IG2Level2UDVarchar2] [dbo].[UDVarchar_type] NULL,
[IG2Level2UDVarchar3] [dbo].[UDVarchar_type] NULL,
[IG2Level3UDVarchar1] [dbo].[UDVarchar_type] NULL,
[IG2Level3UDVarchar2] [dbo].[UDVarchar_type] NULL,
[IG2Level3UDVarchar3] [dbo].[UDVarchar_type] NULL,
[IG2Level4UDVarchar1] [dbo].[UDVarchar_type] NULL,
[IG2Level4UDVarchar2] [dbo].[UDVarchar_type] NULL,
[IG2Level4UDVarchar3] [dbo].[UDVarchar_type] NULL,
[IG2Level5UDVarchar1] [dbo].[UDVarchar_type] NULL,
[IG2Level5UDVarchar2] [dbo].[UDVarchar_type] NULL,
[IG2Level5UDVarchar3] [dbo].[UDVarchar_type] NULL,
[IG2Level1UDDecimal1] [dbo].[UDDecimal_type] NULL,
[IG2Level1UDDecimal2] [dbo].[UDDecimal_type] NULL,
[IG2Level2UDDecimal1] [dbo].[UDDecimal_type] NULL,
[IG2Level2UDDecimal2] [dbo].[UDDecimal_type] NULL,
[IG2Level3UDDecimal1] [dbo].[UDDecimal_type] NULL,
[IG2Level3UDDecimal2] [dbo].[UDDecimal_type] NULL,
[IG2Level4UDDecimal1] [dbo].[UDDecimal_type] NULL,
[IG2Level4UDDecimal2] [dbo].[UDDecimal_type] NULL,
[IG2Level5UDDecimal1] [dbo].[UDDecimal_type] NULL,
[IG2Level5UDDecimal2] [dbo].[UDDecimal_type] NULL,
[CreationDayKey] [dbo].[DateKey_type] NOT NULL,
[ModificationDayKey] [dbo].[DateKey_type] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[DimItemGroup2] ADD CONSTRAINT [PK_DimItemGroup2] PRIMARY KEY CLUSTERED  ([ItemGroup2Key]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [I_DimItemGroup2_1] ON [dbo].[DimItemGroup2] ([IG2Level1]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [I_DimItemGroup2_UD32] ON [dbo].[DimItemGroup2] ([IG2Level1UDDecimal1]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [I_DimItemGroup2_UD33] ON [dbo].[DimItemGroup2] ([IG2Level1UDDecimal2]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [I_DimItemGroup2_UD7] ON [dbo].[DimItemGroup2] ([IG2Level1UDVarchar1]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [I_DimItemGroup2_UD8] ON [dbo].[DimItemGroup2] ([IG2Level1UDVarchar2]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [I_DimItemGroup2_UD9] ON [dbo].[DimItemGroup2] ([IG2Level1UDVarchar3]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [I_DimItemGroup2_2] ON [dbo].[DimItemGroup2] ([IG2Level2]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [I_DimItemGroup2_UD34] ON [dbo].[DimItemGroup2] ([IG2Level2UDDecimal1]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [I_DimItemGroup2_UD35] ON [dbo].[DimItemGroup2] ([IG2Level2UDDecimal2]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [I_DimItemGroup2_UD12] ON [dbo].[DimItemGroup2] ([IG2Level2UDVarchar1]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [I_DimItemGroup2_UD13] ON [dbo].[DimItemGroup2] ([IG2Level2UDVarchar2]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [I_DimItemGroup2_UD14] ON [dbo].[DimItemGroup2] ([IG2Level2UDVarchar3]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [I_DimItemGroup2_3] ON [dbo].[DimItemGroup2] ([IG2Level3]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [I_DimItemGroup2_UD36] ON [dbo].[DimItemGroup2] ([IG2Level3UDDecimal1]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [I_DimItemGroup2_UD37] ON [dbo].[DimItemGroup2] ([IG2Level3UDDecimal2]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [I_DimItemGroup2_UD17] ON [dbo].[DimItemGroup2] ([IG2Level3UDVarchar1]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [I_DimItemGroup2_UD18] ON [dbo].[DimItemGroup2] ([IG2Level3UDVarchar2]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [I_DimItemGroup2_UD19] ON [dbo].[DimItemGroup2] ([IG2Level3UDVarchar3]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [I_DimItemGroup2_4] ON [dbo].[DimItemGroup2] ([IG2Level4]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [I_DimItemGroup2_UD38] ON [dbo].[DimItemGroup2] ([IG2Level4UDDecimal1]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [I_DimItemGroup2_UD39] ON [dbo].[DimItemGroup2] ([IG2Level4UDDecimal2]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [I_DimItemGroup2_UD22] ON [dbo].[DimItemGroup2] ([IG2Level4UDVarchar1]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [I_DimItemGroup2_UD23] ON [dbo].[DimItemGroup2] ([IG2Level4UDVarchar2]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [I_DimItemGroup2_UD24] ON [dbo].[DimItemGroup2] ([IG2Level4UDVarchar3]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [I_DimItemGroup2_5] ON [dbo].[DimItemGroup2] ([IG2Level5]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [I_DimItemGroup2_UD40] ON [dbo].[DimItemGroup2] ([IG2Level5UDDecimal1]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [I_DimItemGroup2_UD41] ON [dbo].[DimItemGroup2] ([IG2Level5UDDecimal2]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [I_DimItemGroup2_UD27] ON [dbo].[DimItemGroup2] ([IG2Level5UDVarchar1]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [I_DimItemGroup2_UD28] ON [dbo].[DimItemGroup2] ([IG2Level5UDVarchar2]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [I_DimItemGroup2_UD29] ON [dbo].[DimItemGroup2] ([IG2Level5UDVarchar3]) ON [PRIMARY]
GO
