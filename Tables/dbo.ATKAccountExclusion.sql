CREATE TABLE [dbo].[ATKAccountExclusion]
(
[AccountExclusionKey] [dbo].[Key_Normal_type] NOT NULL IDENTITY(1, 1),
[AccountKey] [dbo].[Key_Normal_type] NOT NULL,
[WebUserKey] [dbo].[Key_Normal_type] NULL,
[ExclusionType] [dbo].[Description_Small_type] NULL,
[ExclusionReason] [dbo].[Description_Huge_type] NULL,
[Version] [dbo].[Int_Type] NULL CONSTRAINT [DF__ATKAccoun__Versi__1B2DC769] DEFAULT ((0)),
[CreationDate] [dbo].[DateTime_Type] NULL CONSTRAINT [DF__ATKAccoun__Creat__1C21EBA2] DEFAULT (getdate()),
[ModificationDate] [dbo].[DateTime_Type] NULL CONSTRAINT [DF__ATKAccoun__Modif__1D160FDB] DEFAULT (getdate()),
[CreationUser] [dbo].[Description_Normal_type] NULL,
[ModificationUser] [dbo].[Description_Normal_type] NULL,
[ModificationUserAddress] [dbo].[Description_Normal_type] NULL,
[CreationUserAddress] [dbo].[Description_Normal_type] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ATKAccountExclusion] ADD CONSTRAINT [PK_ATKAccountExclusion] PRIMARY KEY NONCLUSTERED  ([AccountExclusionKey]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ATKAccountExclusion] ADD CONSTRAINT [FK_ATKAccountExclusion_1] FOREIGN KEY ([AccountKey]) REFERENCES [dbo].[DimAccount] ([AccountKey])
GO
ALTER TABLE [dbo].[ATKAccountExclusion] ADD CONSTRAINT [FK_ATKAccountExclusion_2] FOREIGN KEY ([WebUserKey]) REFERENCES [dbo].[WebUser] ([WebUserKey])
GO
ALTER TABLE [dbo].[ATKAccountExclusion] ADD CONSTRAINT [FK_ATKAccountExclusion_3] FOREIGN KEY ([ExclusionType]) REFERENCES [dbo].[ATKExclusionType] ([ExclusionType])
GO
