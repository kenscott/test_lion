CREATE TABLE [dbo].[DimVendorMask]
(
[VendorMaskKey] [dbo].[Key_Normal_type] NOT NULL IDENTITY(1, 1),
[VendorKey] [dbo].[Key_Normal_type] NOT NULL,
[Format] [dbo].[Description_Small_type] NOT NULL,
[CreationDate] [dbo].[DateTime_Type] NULL,
[CreatedByWebUserKey] [dbo].[Key_Normal_type] NULL,
[ModificationDate] [dbo].[DateTime_Type] NULL,
[ModifiedByWebUserKey] [dbo].[Key_Normal_type] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[DimVendorMask] ADD CONSTRAINT [PK_DimVendorMask] PRIMARY KEY CLUSTERED  ([VendorMaskKey]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [I_DimVendorMask_1] ON [dbo].[DimVendorMask] ([VendorKey]) INCLUDE ([Format]) ON [PRIMARY]
GO
