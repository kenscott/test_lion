CREATE TABLE [lion].[MarketingReportItem]
(
[ItemKey] [int] NOT NULL,
[ProductCode] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PyramidCode] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LastModifiedBy] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LastModifiedDate] [datetime] NOT NULL CONSTRAINT [DF_MarketingReportItem_LastModifiedDate] DEFAULT (getdate())
) ON [PRIMARY]
GO
ALTER TABLE [lion].[MarketingReportItem] ADD CONSTRAINT [PK_MarketingReportItem] PRIMARY KEY CLUSTERED  ([ItemKey]) ON [PRIMARY]
GO
