CREATE TABLE [dbo].[ATKLimitDataPlaybook]
(
[LimitDataPlaybookKey] [dbo].[Key_Normal_type] NOT NULL IDENTITY(1, 1),
[MaxDayThreshold] [dbo].[Int_Type] NULL,
[Version] [dbo].[Int_Type] NOT NULL CONSTRAINT [DF__ATKLimitD__Versi__44F6B6A7] DEFAULT ((0)),
[CreationDate] [dbo].[DateTime_Type] NOT NULL CONSTRAINT [DF__ATKLimitD__Creat__5FAAACE3] DEFAULT (getdate()),
[ModificationDate] [dbo].[DateTime_Type] NOT NULL CONSTRAINT [DF__ATKLimitD__Modif__7A5EA31F] DEFAULT (getdate()),
[CreationUser] [dbo].[Description_Normal_type] NULL,
[ModificationUser] [dbo].[Description_Normal_type] NULL,
[ModificationUserAddress] [dbo].[Description_Normal_type] NULL,
[CreationUserAddress] [dbo].[Description_Normal_type] NULL,
[WhereClause] [dbo].[Description_Huge_type] NULL,
[JoinClause] [dbo].[Description_Huge_type] NULL,
[HavingClause] [dbo].[Description_Huge_type] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ATKLimitDataPlaybook] ADD CONSTRAINT [PK_ATKLimitDataGroup] PRIMARY KEY CLUSTERED  ([LimitDataPlaybookKey]) ON [PRIMARY]
GO
