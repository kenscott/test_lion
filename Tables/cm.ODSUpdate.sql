CREATE TABLE [cm].[ODSUpdate]
(
[ODSUpdateKey] [int] NOT NULL IDENTITY(1, 1),
[WebUserKey] [int] NULL,
[DateTimeSubmitted] [datetime] NULL,
[DateTimeCollected] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [cm].[ODSUpdate] ADD CONSTRAINT [PK_ODSUpdate] PRIMARY KEY CLUSTERED  ([ODSUpdateKey]) ON [PRIMARY]
GO
