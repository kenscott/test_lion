CREATE TABLE [cm].[AdjustmentReason]
(
[AdjustmentReasonKey] [dbo].[Key_Small_type] NOT NULL IDENTITY(1, 1),
[AdjustmentReasonCode] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AdjustmentReasonValue] [dbo].[Description_Huge_type] NOT NULL,
[CreationDate] [datetime] NULL,
[CreatedByWebUserKey] [int] NULL,
[ModificationDate] [datetime] NULL,
[ModifiedByWebUserKey] [int] NULL,
[Inactive] [bit] NOT NULL CONSTRAINT [DF_AdjustmentReason_Inactive] DEFAULT ((0)),
[CostCenter] [varchar] (4) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [cm].[AdjustmentReason] ADD CONSTRAINT [PK_AdjustmentReason] PRIMARY KEY CLUSTERED  ([AdjustmentReasonKey]) ON [PRIMARY]
GO
