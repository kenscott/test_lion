CREATE TABLE [dbo].[WebUserView]
(
[WebUserViewKey] [int] NOT NULL IDENTITY(1, 1),
[ViewerWebUserKey] [int] NOT NULL,
[ViewedWebUserKey] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[WebUserView] ADD CONSTRAINT [PK_WebUserView] PRIMARY KEY NONCLUSTERED  ([WebUserViewKey]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[WebUserView] ADD CONSTRAINT [Idx_Viewer_Viewed] UNIQUE CLUSTERED  ([ViewerWebUserKey], [ViewedWebUserKey]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[WebUserView] ADD CONSTRAINT [fk_webuser_key1] FOREIGN KEY ([ViewerWebUserKey]) REFERENCES [dbo].[WebUser] ([WebUserKey])
GO
ALTER TABLE [dbo].[WebUserView] ADD CONSTRAINT [fk_webuser_key2] FOREIGN KEY ([ViewedWebUserKey]) REFERENCES [dbo].[WebUser] ([WebUserKey])
GO
