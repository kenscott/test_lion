CREATE TABLE [cc].[SupplierContractLogArchive]
(
[ContractLogKey] [int] NOT NULL,
[WebUserKey] [int] NULL,
[ContractKey] [int] NULL,
[VendorKey] [int] NULL,
[ContractID] [int] NULL,
[ContractAction] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Notes] [varchar] (4000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreationDate] [datetime] NULL,
[OldStatusKey] [int] NULL,
[NewStatusKey] [int] NULL,
[OldUpdateStatusKey] [int] NULL,
[NewUpdateStatusKey] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [cc].[SupplierContractLogArchive] ADD CONSTRAINT [PK_ContractLogArchive] PRIMARY KEY NONCLUSTERED  ([ContractLogKey]) ON [PRIMARY]
GO
