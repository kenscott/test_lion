CREATE TABLE [dbo].[PlaybookProjectMonthlySummary]
(
[ProjectKey] [dbo].[Key_Normal_type] NOT NULL,
[AccountKey] [dbo].[Key_Normal_type] NOT NULL,
[MonthKey] [dbo].[DateKey_type] NOT NULL,
[TotalActualPrice] [dbo].[Money_Normal_Type] NOT NULL,
[TotalActualCost] [dbo].[Money_Normal_Type] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[PlaybookProjectMonthlySummary] ADD CONSTRAINT [PK_PlaybookProjectMonthlySummary] PRIMARY KEY CLUSTERED  ([ProjectKey], [AccountKey], [MonthKey]) ON [PRIMARY]
GO
