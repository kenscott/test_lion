CREATE TABLE [dbo].[ProposalCostCollision]
(
[ProposalCostCollisionKey] [dbo].[Key_Normal_type] NOT NULL IDENTITY(1, 1),
[ProposalCostKey_Collision] [dbo].[Key_Normal_type] NULL,
[PlaybookPriceProposalKey] [dbo].[Key_Normal_type] NULL,
[AcceptanceCodeKey] [dbo].[Key_Small_type] NOT NULL,
[UserAcceptanceCodeKey] [dbo].[Key_Small_type] NOT NULL,
[DefaultAcceptanceCodeKey] [dbo].[Key_Small_type] NULL,
[ProjectKey] [dbo].[Key_Normal_type] NOT NULL,
[AccountKey] [dbo].[Key_Normal_type] NOT NULL,
[ItemKey] [dbo].[Key_Normal_type] NOT NULL,
[InvoiceLineGroup1Key] [dbo].[Key_Normal_type] NOT NULL,
[ProposedCost] [dbo].[Money_Normal_Type] NOT NULL,
[NewCost] [dbo].[Money_Normal_Type] NOT NULL,
[TotalActual12MonthCost] [dbo].[Money_Normal_Type] NOT NULL,
[TotalActual12MonthPrice] [dbo].[Money_Normal_Type] NOT NULL,
[TotalActual12MonthQuantity] [dbo].[Quantity_Normal_type] NOT NULL,
[TotalActual12MonthFrequency] [dbo].[Quantity_Normal_type] NOT NULL,
[LastItemPrice] [dbo].[Money_Normal_Type] NOT NULL,
[LastItemCost] [dbo].[Money_Normal_Type] NOT NULL,
[Forecasted12MonthQuantity] [dbo].[Quantity_Normal_type] NOT NULL,
[TotalForecasted12MonthPrice_LastPrice] [dbo].[Money_Normal_Type] NOT NULL,
[TotalForecasted12MonthCost_LastCost] [dbo].[Money_Normal_Type] NOT NULL,
[TotalForecasted12MonthCost_ProposedCost] [dbo].[Money_Normal_Type] NOT NULL,
[TotalForecasted12MonthCost_NewCost] [dbo].[Money_Normal_Type] NOT NULL,
[Version] [dbo].[Int_Type] NOT NULL CONSTRAINT [DF__ProposalC__Versi__4D4E678B] DEFAULT ((0)),
[CreationDate] [dbo].[DateTime_Type] NOT NULL CONSTRAINT [DF__ProposalC__Creat__4E428BC4] DEFAULT (getdate()),
[ModificationDate] [dbo].[DateTime_Type] NOT NULL CONSTRAINT [DF__ProposalC__Modif__4F36AFFD] DEFAULT (getdate()),
[CreationUser] [dbo].[Description_Normal_type] NULL,
[ModificationUser] [dbo].[Description_Normal_type] NULL,
[PlaybookDataPointGroupKey] [dbo].[Key_Normal_type] NULL,
[AccountManagerKey] [dbo].[Key_Normal_type] NULL,
[ModificationUserAddress] [dbo].[Description_Normal_type] NULL,
[CreationUserAddress] [dbo].[Description_Normal_type] NULL,
[AcceptanceStatusDate] [datetime] NULL,
[LastSaleDayKey] [dbo].[Key_Small_type] NULL,
[LastInvoiceLineGroup2Key] [dbo].[Key_Normal_type] NOT NULL,
[EffectiveDate] [dbo].[DateTime_Type] NOT NULL,
[SuppressionDate] [dbo].[DateTime_Type] NOT NULL,
[ReasonCodeKey] [dbo].[Key_Small_type] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ProposalCostCollision] ADD CONSTRAINT [PK_ProposalCostCollision] PRIMARY KEY NONCLUSTERED  ([ProposalCostCollisionKey]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ProposalCostCollision] ADD CONSTRAINT [FK_ProposalCostCollision_1] FOREIGN KEY ([AccountKey]) REFERENCES [dbo].[DimAccount] ([AccountKey])
GO
ALTER TABLE [dbo].[ProposalCostCollision] ADD CONSTRAINT [FK_ProposalCostCollision_10] FOREIGN KEY ([PlaybookPriceProposalKey]) REFERENCES [dbo].[PlaybookPriceProposal] ([PlaybookPriceProposalKey])
GO
ALTER TABLE [dbo].[ProposalCostCollision] ADD CONSTRAINT [FK_ProposalCostCollision_11] FOREIGN KEY ([PlaybookDataPointGroupKey]) REFERENCES [dbo].[PlaybookDataPointGroup] ([PlaybookDataPointGroupKey])
GO
ALTER TABLE [dbo].[ProposalCostCollision] ADD CONSTRAINT [FK_ProposalCostCollision_12] FOREIGN KEY ([AccountManagerKey]) REFERENCES [dbo].[DimAccountManager] ([AccountManagerKey])
GO
ALTER TABLE [dbo].[ProposalCostCollision] ADD CONSTRAINT [FK_ProposalCostCollision_2] FOREIGN KEY ([ItemKey]) REFERENCES [dbo].[DimItem] ([ItemKey])
GO
ALTER TABLE [dbo].[ProposalCostCollision] ADD CONSTRAINT [FK_ProposalCostCollision_3] FOREIGN KEY ([InvoiceLineGroup1Key]) REFERENCES [dbo].[DimInvoiceLineGroup1] ([InvoiceLineGroup1Key])
GO
ALTER TABLE [dbo].[ProposalCostCollision] ADD CONSTRAINT [FK_ProposalCostCollision_4] FOREIGN KEY ([ProjectKey]) REFERENCES [dbo].[PlaybookProject] ([ProjectKey])
GO
ALTER TABLE [dbo].[ProposalCostCollision] ADD CONSTRAINT [FK_ProposalCostCollision_5] FOREIGN KEY ([DefaultAcceptanceCodeKey]) REFERENCES [dbo].[ATKAcceptanceCode] ([AcceptanceCodeKey])
GO
ALTER TABLE [dbo].[ProposalCostCollision] ADD CONSTRAINT [FK_ProposalCostCollision_6] FOREIGN KEY ([ProposalCostKey_Collision]) REFERENCES [dbo].[ProposalCost] ([ProposalCostKey])
GO
ALTER TABLE [dbo].[ProposalCostCollision] ADD CONSTRAINT [FK_ProposalCostCollision_7] FOREIGN KEY ([AcceptanceCodeKey]) REFERENCES [dbo].[ATKAcceptanceCode] ([AcceptanceCodeKey])
GO
ALTER TABLE [dbo].[ProposalCostCollision] ADD CONSTRAINT [FK_ProposalCostCollision_8] FOREIGN KEY ([UserAcceptanceCodeKey]) REFERENCES [dbo].[ATKAcceptanceCode] ([AcceptanceCodeKey])
GO
ALTER TABLE [dbo].[ProposalCostCollision] ADD CONSTRAINT [FK_ProposalCostCollision_9] FOREIGN KEY ([ReasonCodeKey]) REFERENCES [dbo].[ProposalReasonCode] ([ReasonCodeKey])
GO
