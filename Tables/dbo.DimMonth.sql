CREATE TABLE [dbo].[DimMonth]
(
[MonthKey] [dbo].[Key_Small_type] NOT NULL IDENTITY(1, 1),
[FirstSQLDate] [dbo].[DateTime_Type] NULL,
[LastSQLDate] [dbo].[DateTime_Type] NULL,
[CalendarMonth] [dbo].[Description_Small_type] NOT NULL,
[CalendarMonthinYear] [dbo].[Description_Small_type] NOT NULL,
[CalendarMonthNumber] [dbo].[Int_255_type] NULL,
[CalendarMonthNumberinQuarter] [dbo].[Int_255_type] NULL,
[CalendarMonthNumberinYear] [dbo].[Int_255_type] NULL,
[CalendarQuarter] [dbo].[Description_Small_type] NULL,
[CalendarQuarterinYear] [dbo].[Description_Small_type] NULL,
[CalendarQuarterNumber] [dbo].[Int_255_type] NULL,
[CalendarQuarterNumberinYear] [dbo].[Int_255_type] NULL,
[CalendarYear] [dbo].[Int_32767_type] NULL,
[CalendarYearMonth] [dbo].[Description_Small_type] NULL,
[FiscalMonth] [dbo].[Description_Small_type] NULL,
[FiscalMonthinYear] [dbo].[Description_Small_type] NULL,
[FiscalMonthNumber] [dbo].[Int_255_type] NULL,
[FiscalMonthNumberinQuarter] [dbo].[Int_255_type] NULL,
[FiscalMonthNumberinYear] [dbo].[Int_255_type] NULL,
[FiscalQuarter] [dbo].[Description_Small_type] NULL,
[FiscalQuarterinYear] [dbo].[Description_Small_type] NULL,
[FiscalQuarterNumber] [dbo].[Int_255_type] NULL,
[FiscalQuarterNumberinYear] [dbo].[Int_255_type] NULL,
[FiscalYear] [dbo].[Description_Small_type] NULL,
[FiscalYearNumber] [dbo].[Int_255_type] NULL,
[FiscalYearMonth] [dbo].[Description_Small_type] NULL,
[PeriodNumber] [dbo].[Int_255_type] NULL,
[Period] [dbo].[Description_Small_type] NULL,
[MonthSequenceNumber] [dbo].[Int_Type] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[DimMonth] ADD CONSTRAINT [PK_DimMonth] PRIMARY KEY CLUSTERED  ([MonthKey]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [I_DimMonth_1] ON [dbo].[DimMonth] ([CalendarMonth]) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [UI_DimMonth_2] ON [dbo].[DimMonth] ([FirstSQLDate]) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [UI_DimMonth_1] ON [dbo].[DimMonth] ([FirstSQLDate], [LastSQLDate]) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [UI_DimMonth_3] ON [dbo].[DimMonth] ([LastSQLDate]) ON [PRIMARY]
GO
