CREATE TABLE [lion].[Batch]
(
[BatchKey] [int] NOT NULL IDENTITY(200000, 1),
[CreationDate] [date] NOT NULL CONSTRAINT [DF__RequestBa__Creat__039C5DE8] DEFAULT (getdate()),
[SourceKey] [tinyint] NULL
) ON [PRIMARY]
GO
ALTER TABLE [lion].[Batch] ADD CONSTRAINT [PK_Batch] PRIMARY KEY CLUSTERED  ([BatchKey]) ON [PRIMARY]
GO
ALTER TABLE [lion].[Batch] ADD CONSTRAINT [FK_Batch_Source] FOREIGN KEY ([SourceKey]) REFERENCES [lion].[BatchSource] ([SourceKey])
GO
