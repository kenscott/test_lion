CREATE TABLE [cm].[InvoiceClaim]
(
[InvoiceClaimKey] [int] NOT NULL IDENTITY(100000, 1),
[VendorKey] [int] NOT NULL,
[ClaimMonth] [int] NOT NULL,
[ContractId] [varchar] (9) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CreationDate] [datetime] NULL,
[CreatedByWebUserKey] [int] NULL,
[ModificationDate] [datetime] NULL,
[ModifiedByWebUserKey] [int] NULL,
[ClaimType] [varchar] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [cm].[InvoiceClaim] ADD CONSTRAINT [PK_InvoiceClaim] PRIMARY KEY CLUSTERED  ([InvoiceClaimKey]) ON [PRIMARY]
GO
