CREATE TABLE [cm].[ClaimLog]
(
[ClaimLogKey] [int] NOT NULL IDENTITY(1, 1),
[WebUserKey] [int] NULL,
[SupplierClaimKey] [int] NULL,
[Notes] [varchar] (4000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreationDate] [datetime] NOT NULL CONSTRAINT [DF__ClaimLog__Creati__09F15A2A] DEFAULT (getdate()),
[ClaimAction] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [cm].[ClaimLog] ADD CONSTRAINT [PK_ClaimLog] PRIMARY KEY NONCLUSTERED  ([ClaimLogKey]) ON [PRIMARY]
GO
