CREATE TABLE [ppe].[RecommendNetwork]
(
[RecommendNetworkKey] [int] NOT NULL IDENTITY(1, 1),
[RecommendKey] [int] NOT NULL,
[AG1Level2] [dbo].[Description_Small_type] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [ppe].[RecommendNetwork] ADD CONSTRAINT [PK_RecommendNetwork ] PRIMARY KEY CLUSTERED  ([RecommendNetworkKey]) ON [PRIMARY]
GO
ALTER TABLE [ppe].[RecommendNetwork] ADD CONSTRAINT [FK_Recommend_Network] FOREIGN KEY ([RecommendKey]) REFERENCES [ppe].[Recommend] ([RecommendKey])
GO
