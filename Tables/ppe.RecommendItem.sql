CREATE TABLE [ppe].[RecommendItem]
(
[RecommendItemKey] [int] NOT NULL IDENTITY(1, 1),
[RecommendKey] [int] NOT NULL,
[ItemKey] [dbo].[Key_Normal_type] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [ppe].[RecommendItem] ADD CONSTRAINT [PK_RecommendItem] PRIMARY KEY NONCLUSTERED  ([RecommendItemKey]) ON [PRIMARY]
GO
CREATE UNIQUE CLUSTERED INDEX [i_recitem] ON [ppe].[RecommendItem] ([RecommendKey], [ItemKey]) ON [PRIMARY]
GO
ALTER TABLE [ppe].[RecommendItem] ADD CONSTRAINT [FK_Recommend_Item] FOREIGN KEY ([RecommendKey]) REFERENCES [ppe].[Recommend] ([RecommendKey])
GO
ALTER TABLE [ppe].[RecommendItem] ADD CONSTRAINT [FK_RecommendItem_DimItem] FOREIGN KEY ([ItemKey]) REFERENCES [dbo].[DimItem] ([ItemKey])
GO
