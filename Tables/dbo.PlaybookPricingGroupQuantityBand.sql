CREATE TABLE [dbo].[PlaybookPricingGroupQuantityBand]
(
[PlaybookPricingGroupQuantityBandKey] [dbo].[Key_Normal_type] NOT NULL IDENTITY(1, 1),
[PlaybookPricingGroupKey] [dbo].[Key_Normal_type] NOT NULL,
[PricingRuleBandKey] [dbo].[Key_Normal_type] NOT NULL,
[LowerBandQuantity] [dbo].[Quantity_Normal_type] NULL,
[UpperBandQuantity] [dbo].[Quantity_Normal_type] NULL,
[BandSequenceNumber] [int] NULL,
[RulingMemberCount] [dbo].[Int_Type] NULL,
[NonRulingMemberCount] [dbo].[Int_Type] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[PlaybookPricingGroupQuantityBand] ADD CONSTRAINT [PK_ATKPricingGroupQuantityBand] PRIMARY KEY NONCLUSTERED  ([PlaybookPricingGroupQuantityBandKey]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [I_PlaybookPricingGroupQuantityBand_1] ON [dbo].[PlaybookPricingGroupQuantityBand] ([PlaybookPricingGroupKey]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [I_PlaybookPricingGroupQuantityBand_3] ON [dbo].[PlaybookPricingGroupQuantityBand] ([PlaybookPricingGroupKey], [LowerBandQuantity], [UpperBandQuantity], [BandSequenceNumber]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [I_PlaybookPricingGroupQuantityBand_2] ON [dbo].[PlaybookPricingGroupQuantityBand] ([PricingRuleBandKey]) ON [PRIMARY]
GO
CREATE STATISTICS [_dta_stat_357576312_2_6] ON [dbo].[PlaybookPricingGroupQuantityBand] ([PlaybookPricingGroupKey], [BandSequenceNumber])
GO
ALTER TABLE [dbo].[PlaybookPricingGroupQuantityBand] ADD CONSTRAINT [FK_PlaybookPricingGroupQuantityBand_1] FOREIGN KEY ([PlaybookPricingGroupKey]) REFERENCES [dbo].[PlaybookPricingGroup] ([PlaybookPricingGroupKey])
GO
ALTER TABLE [dbo].[PlaybookPricingGroupQuantityBand] ADD CONSTRAINT [FK_PlaybookPricingGroupQuantityBand_2] FOREIGN KEY ([PricingRuleBandKey]) REFERENCES [dbo].[ATKPricingRuleBand] ([PricingRuleBandKey])
GO
