CREATE TABLE [cm].[RejectionReason]
(
[RejectionReasonKey] [dbo].[Key_Small_type] NOT NULL IDENTITY(1, 1),
[RejectionReasonValue] [dbo].[Description_Huge_type] NOT NULL,
[CreationDate] [datetime] NULL,
[CreatedByWebUserKey] [int] NULL,
[ModificationDate] [datetime] NULL,
[ModifiedByWebUserKey] [int] NULL,
[Inactive] [bit] NOT NULL CONSTRAINT [DF_RejectionReasonCode_Inactive] DEFAULT ((0))
) ON [PRIMARY]
GO
ALTER TABLE [cm].[RejectionReason] ADD CONSTRAINT [PK_RejectionReasonCode] PRIMARY KEY CLUSTERED  ([RejectionReasonKey]) ON [PRIMARY]
GO
