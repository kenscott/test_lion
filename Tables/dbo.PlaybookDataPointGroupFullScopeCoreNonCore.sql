CREATE TABLE [dbo].[PlaybookDataPointGroupFullScopeCoreNonCore]
(
[PlaybookDataPointGroupKey] [dbo].[Key_Normal_type] NOT NULL,
[AccountKey] [dbo].[Key_Normal_type] NOT NULL,
[ItemGroup1Key] [dbo].[Key_Normal_type] NOT NULL,
[PercentItemGroup1SalesToTotalAccountSales] [dbo].[Percent_Type] NOT NULL,
[CoreNonCoreIndicator] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[PlaybookDataPointGroupFullScopeCoreNonCore] ADD CONSTRAINT [PK_PlaybookDataPointGroupFullScopeCoreNonCoree] PRIMARY KEY CLUSTERED  ([PlaybookDataPointGroupKey], [AccountKey], [ItemGroup1Key]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[PlaybookDataPointGroupFullScopeCoreNonCore] ADD CONSTRAINT [FK_PlaybookDataPointGroupFullScopeCoreNonCore_1] FOREIGN KEY ([ItemGroup1Key]) REFERENCES [dbo].[DimItemGroup1] ([ItemGroup1Key])
GO
ALTER TABLE [dbo].[PlaybookDataPointGroupFullScopeCoreNonCore] ADD CONSTRAINT [FK_PlaybookDataPointGroupFullScopeCoreNonCore_2] FOREIGN KEY ([AccountKey]) REFERENCES [dbo].[DimAccount] ([AccountKey])
GO
