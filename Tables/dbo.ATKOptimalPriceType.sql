CREATE TABLE [dbo].[ATKOptimalPriceType]
(
[OptimalPriceType] [dbo].[Description_Small_type] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ATKOptimalPriceType] ADD CONSTRAINT [PK_OptimalPriceType] PRIMARY KEY CLUSTERED  ([OptimalPriceType]) ON [PRIMARY]
GO
