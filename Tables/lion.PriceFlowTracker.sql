CREATE TABLE [lion].[PriceFlowTracker]
(
[RequestKey] [int] NOT NULL,
[AccountKey] [int] NULL,
[ItemGroup3Key] [dbo].[Key_Normal_type] NOT NULL,
[ItemKey] [int] NULL,
[OriginalDiscount] [decimal] (19, 8) NULL,
[OriginalDate] [date] NULL,
[NewDiscount] [decimal] (19, 8) NULL,
[NewWolcenDate] [date] NULL,
[Total12MonthQuantity] [decimal] (19, 8) NULL,
[Sales @ Original] [decimal] (19, 8) NULL,
[Sales @ New] [decimal] (19, 8) NULL,
[ProjectedBenefit] AS ([Sales @ New]-[Sales @ Original]),
[NewException] [bit] NOT NULL,
[NewSPG] [bit] NOT NULL,
[Processed] [bit] NOT NULL CONSTRAINT [DF_PriceFlowTracker_Processed] DEFAULT ((0)),
[CurrentIndicator] [bit] NOT NULL CONSTRAINT [DF_PriceFlowTracker_CurrentIndicator] DEFAULT ((1)),
[LastInvoiceDateDayKey] [int] NULL,
[TradePrice] [dbo].[UDDecimal_type] NULL,
[UnitCost] [dbo].[UDDecimal_type] NULL,
[TradeMargin] AS (([TradePrice]-[UnitCost])/nullif([TradePrice],(0)))
) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_PriceFlowTracker_1] ON [lion].[PriceFlowTracker] ([Processed], [ItemKey], [NewWolcenDate]) INCLUDE ([AccountKey]) ON [PRIMARY]
GO
