CREATE TABLE [lion].[ATKManualPriceBand]
(
[ManualPriceBandKey] [dbo].[Key_Normal_type] NOT NULL IDENTITY(1, 1),
[ItemPyramidCode] [dbo].[UDVarchar_type] NOT NULL,
[LLSPGCode] [dbo].[UDVarchar_type] NOT NULL,
[ProductCode] [dbo].[UDVarchar_type] NOT NULL CONSTRAINT [DF_ATKManualPriceBand_ProductCode] DEFAULT (N'ALL'),
[PriceApproach] [dbo].[UDVarchar_type] NOT NULL CONSTRAINT [DF_ATKManualPriceBand_PriceApproach] DEFAULT (N'ALL'),
[ContractClaimsIndicator] [dbo].[UDVarchar_type] NOT NULL CONSTRAINT [DF_ATKManualPriceBand_ContractClaimsIndicator] DEFAULT (N'ALL'),
[Branch] [dbo].[UDVarchar_type] NOT NULL CONSTRAINT [DF_ATKManualPriceBand_Branch] DEFAULT (N'ALL'),
[Network] [dbo].[UDVarchar_type] NOT NULL CONSTRAINT [DF_ATKManualPriceBand_Network] DEFAULT (N'ALL'),
[Area] [dbo].[UDVarchar_type] NOT NULL,
[Region] [dbo].[UDVarchar_type] NOT NULL CONSTRAINT [DF_ATKManualPriceBand_Region] DEFAULT (N'ALL'),
[FloorGPP] [dbo].[Percent_Type] NOT NULL,
[TargetGPP] [dbo].[Percent_Type] NOT NULL,
[StretchGPP] [dbo].[Percent_Type] NOT NULL,
[ExpirationDate] [date] NULL,
[CreationDate] [datetime] NOT NULL CONSTRAINT [DF_ATKManualPriceBand_CreationDate] DEFAULT (getdate()),
[CreatedByWebUserKey] [dbo].[Key_Normal_type] NOT NULL,
[ModificationDate] [datetime] NOT NULL CONSTRAINT [DF_ATKManualPriceBand_ModificationDate] DEFAULT (getdate()),
[ModifiedByWebUserKey] [dbo].[Key_Normal_type] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [lion].[ATKManualPriceBand] ADD CONSTRAINT [PK_ATKManualPriceBand] PRIMARY KEY NONCLUSTERED  ([ManualPriceBandKey]) ON [PRIMARY]
GO
CREATE UNIQUE CLUSTERED INDEX [I_1] ON [lion].[ATKManualPriceBand] ([ItemPyramidCode], [LLSPGCode], [ProductCode], [PriceApproach], [ContractClaimsIndicator], [Region], [Area], [Network], [Branch]) ON [PRIMARY]
GO
