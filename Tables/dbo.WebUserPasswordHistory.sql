CREATE TABLE [dbo].[WebUserPasswordHistory]
(
[WebUserPasswordHistoryKey] [dbo].[Key_Normal_type] NOT NULL IDENTITY(1, 1),
[WebUserKey] [dbo].[Key_Normal_type] NOT NULL,
[WebUserPassword] [dbo].[Description_Normal_type] NOT NULL,
[CreationDate] [dbo].[DateTime_Type] NOT NULL CONSTRAINT [DF_WebUserPasswordHistoryKey_CreationDate] DEFAULT (getdate())
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[WebUserPasswordHistory] ADD CONSTRAINT [PK_WebUserPasswordHistory] PRIMARY KEY CLUSTERED  ([WebUserPasswordHistoryKey]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[WebUserPasswordHistory] ADD CONSTRAINT [FK_WebUserPasswordHistoryKey_1] FOREIGN KEY ([WebUserKey]) REFERENCES [dbo].[WebUser] ([WebUserKey])
GO
