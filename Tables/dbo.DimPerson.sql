CREATE TABLE [dbo].[DimPerson]
(
[DimPersonKey] [dbo].[Key_Normal_type] NOT NULL IDENTITY(1, 1),
[FullName] [dbo].[Description_Normal_type] NOT NULL,
[Email] [dbo].[Description_Normal_type] NULL,
[ODSPersonKey] [dbo].[Description_Normal_type] NULL,
[CreationDayKey] [dbo].[Key_Small_type] NOT NULL,
[ModificationDayKey] [dbo].[Key_Small_type] NOT NULL,
[DimPersonUDVarchar1] [dbo].[UDVarchar_type] NULL,
[DimPersonUDVarchar2] [dbo].[UDVarchar_type] NULL,
[DimPersonUDVarchar3] [dbo].[UDVarchar_type] NULL,
[LevelId] [dbo].[UDVarchar_type] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[DimPerson] ADD CONSTRAINT [PK_DimPerson] PRIMARY KEY CLUSTERED  ([DimPersonKey]) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [I_DimPerson_2] ON [dbo].[DimPerson] ([Email]) ON [PRIMARY]
GO
SET ANSI_NULLS ON
GO
SET ANSI_PADDING ON
GO
SET ANSI_WARNINGS ON
GO
SET ARITHABORT ON
GO
SET CONCAT_NULL_YIELDS_NULL ON
GO
SET NUMERIC_ROUNDABORT OFF
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE UNIQUE NONCLUSTERED INDEX [I_DimPerson_1] ON [dbo].[DimPerson] ([ODSPersonKey]) WHERE ([ODSPersonKey] IS NOT NULL) ON [PRIMARY]
GO
CREATE STATISTICS [_dta_stat_1861581670_2_1] ON [dbo].[DimPerson] ([DimPersonKey], [FullName])
GO
CREATE STATISTICS [_dta_stat_1861581670_6_1] ON [dbo].[DimPerson] ([DimPersonKey], [ModificationDayKey])
GO
