CREATE TABLE [ppe].[Recommend]
(
[RecommendKey] [int] NOT NULL IDENTITY(1, 1),
[WebUserKey] [int] NOT NULL,
[MaxChange] [decimal] (38, 8) NULL,
[MaxSIChange] [decimal] (38, 8) NULL,
[SkipDurationMonths] [int] NULL CONSTRAINT [DF__Recommend__SkipD__33AB63BD] DEFAULT ((3)),
[CreatedOn] [datetime] NULL CONSTRAINT [DF__Recommend__Creat__626652A6] DEFAULT (getdate()),
[IncludeBranchManagers] [bit] NULL CONSTRAINT [DF__Recommend__Inclu__0E44D4E4] DEFAULT ((1)),
[IncludeSalesTeamManagers] [bit] NULL CONSTRAINT [DF__Recommend__Inclu__0F38F91D] DEFAULT ((1)),
[StatusRollup] [varchar] (40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CapOnGreen] [bit] NULL,
[TermsEffectiveDate] [date] NULL,
[LastUsedDate] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [ppe].[Recommend] ADD CONSTRAINT [PK_Recommend] PRIMARY KEY CLUSTERED  ([RecommendKey]) ON [PRIMARY]
GO
ALTER TABLE [ppe].[Recommend] ADD CONSTRAINT [FK_Recommend_WebUser] FOREIGN KEY ([WebUserKey]) REFERENCES [dbo].[WebUser] ([WebUserKey])
GO
