CREATE TABLE [ppe].[RecommendAccount]
(
[RecommendAccountKey] [int] NOT NULL IDENTITY(1, 1),
[RecommendKey] [int] NOT NULL,
[AccountKey] [dbo].[Key_Normal_type] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [ppe].[RecommendAccount] ADD CONSTRAINT [PK_RecommendAccount] PRIMARY KEY NONCLUSTERED  ([RecommendAccountKey]) ON [PRIMARY]
GO
CREATE UNIQUE CLUSTERED INDEX [i_recacct] ON [ppe].[RecommendAccount] ([RecommendKey], [AccountKey]) ON [PRIMARY]
GO
ALTER TABLE [ppe].[RecommendAccount] ADD CONSTRAINT [FK_Recommend_Account] FOREIGN KEY ([RecommendKey]) REFERENCES [ppe].[Recommend] ([RecommendKey])
GO
ALTER TABLE [ppe].[RecommendAccount] ADD CONSTRAINT [FK_RecommendAccount_DimAccount] FOREIGN KEY ([AccountKey]) REFERENCES [dbo].[DimAccount] ([AccountKey])
GO
