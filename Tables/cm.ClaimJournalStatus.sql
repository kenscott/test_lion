CREATE TABLE [cm].[ClaimJournalStatus]
(
[ClaimJournalStatusKey] [int] NOT NULL IDENTITY(1, 1),
[ClaimJournalStatus] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ClaimJournalStatusDescription] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [cm].[ClaimJournalStatus] ADD CONSTRAINT [PK_ClaimJournalStatus] PRIMARY KEY CLUSTERED  ([ClaimJournalStatusKey]) ON [PRIMARY]
GO
