CREATE TABLE [dbo].[PlaybookPricingGroup]
(
[PlaybookPricingGroupKey] [dbo].[Key_Normal_type] NOT NULL IDENTITY(1, 1),
[PlaybookDataPointGroupKey] [dbo].[Key_Normal_type] NOT NULL,
[GroupingColumns] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[GroupValues] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreationDate] [dbo].[DateTime_Type] NULL CONSTRAINT [DF_PlaybookPricingGroup_CreationDate] DEFAULT (getdate()),
[QuantityBandCreationMethod] [dbo].[Description_Normal_type] NULL,
[PreviousValuesPlaybookPricingGroupKey] [dbo].[Key_Normal_type] NULL,
[PricingRuleKey] [dbo].[Key_Normal_type] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[PlaybookPricingGroup] ADD CONSTRAINT [PK_PlaybookPricingGroup] PRIMARY KEY CLUSTERED  ([PlaybookPricingGroupKey]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [I_PlaybookPricingGroup_1] ON [dbo].[PlaybookPricingGroup] ([PlaybookDataPointGroupKey]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [I_PlaybookPricingGroup_3] ON [dbo].[PlaybookPricingGroup] ([PricingRuleKey]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[PlaybookPricingGroup] ADD CONSTRAINT [FK_PlaybookPricingGroup_1] FOREIGN KEY ([PlaybookDataPointGroupKey]) REFERENCES [dbo].[PlaybookDataPointGroup] ([PlaybookDataPointGroupKey])
GO
ALTER TABLE [dbo].[PlaybookPricingGroup] ADD CONSTRAINT [FK_PlaybookPricingGroup_2] FOREIGN KEY ([PreviousValuesPlaybookPricingGroupKey]) REFERENCES [dbo].[PlaybookPricingGroup] ([PlaybookPricingGroupKey])
GO
ALTER TABLE [dbo].[PlaybookPricingGroup] ADD CONSTRAINT [FK_PlaybookPricingGroup_3] FOREIGN KEY ([PricingRuleKey]) REFERENCES [dbo].[ATKPricingRule] ([PricingRuleKey])
GO
