CREATE TABLE [cc].[SupplierContractAccount]
(
[ContractAccountKey] [int] NOT NULL IDENTITY(1, 1),
[ContractKey] [int] NOT NULL,
[AccountKey] [int] NOT NULL,
[StartDate] [date] NULL,
[ExpirationDate] [date] NULL,
[CreatedByWebUserKey] [int] NULL,
[CreatedOnDT] [datetime] NULL CONSTRAINT [DF__DimContra__Creat__32496447] DEFAULT (getdate()),
[ModifiedByWebUserKey] [int] NULL,
[ModifiedOnDT] [datetime] NULL CONSTRAINT [DF__DimContra__Modif__333D8880] DEFAULT (getdate())
) ON [PRIMARY]
GO
ALTER TABLE [cc].[SupplierContractAccount] ADD CONSTRAINT [PK_DimContractAccount] PRIMARY KEY NONCLUSTERED  ([ContractAccountKey]) ON [PRIMARY]
GO
CREATE CLUSTERED INDEX [uc_ContactAcctID] ON [cc].[SupplierContractAccount] ([ContractKey], [AccountKey]) ON [PRIMARY]
GO
ALTER TABLE [cc].[SupplierContractAccount] ADD CONSTRAINT [Account_ContractAccount_FK2] FOREIGN KEY ([AccountKey]) REFERENCES [dbo].[DimAccount] ([AccountKey])
GO
ALTER TABLE [cc].[SupplierContractAccount] ADD CONSTRAINT [Contract_ContractAccount_FK1] FOREIGN KEY ([ContractKey]) REFERENCES [cc].[SupplierContract] ([ContractKey])
GO
