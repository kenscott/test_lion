CREATE TABLE [lion].[ATKManualPriceBandArchive]
(
[ManualPriceBandKey] [dbo].[Key_Normal_type] NOT NULL,
[ItemPyramidCode] [dbo].[UDVarchar_type] NULL,
[LLSPGCode] [dbo].[UDVarchar_type] NULL,
[ProductCode] [dbo].[UDVarchar_type] NULL,
[PriceApproach] [dbo].[UDVarchar_type] NULL,
[ContractClaimsIndicator] [dbo].[UDVarchar_type] NULL,
[Region] [dbo].[UDVarchar_type] NULL,
[Area] [dbo].[UDVarchar_type] NULL,
[Network] [dbo].[UDVarchar_type] NULL,
[Branch] [dbo].[UDVarchar_type] NULL,
[FloorGPP] [dbo].[Percent_Type] NULL,
[TargetGPP] [dbo].[Percent_Type] NULL,
[StretchGPP] [dbo].[Percent_Type] NULL,
[ExpirationDate] [date] NULL,
[CreationDate] [datetime] NULL,
[CreatedByWebUserKey] [dbo].[Key_Normal_type] NULL,
[ModificationDate] [datetime] NULL,
[ModifiedByWebUserKey] [dbo].[Key_Normal_type] NULL
) ON [PRIMARY]
GO
ALTER TABLE [lion].[ATKManualPriceBandArchive] ADD CONSTRAINT [PK_ATKManualPriceBandArchive] PRIMARY KEY NONCLUSTERED  ([ManualPriceBandKey]) ON [PRIMARY]
GO
