CREATE TABLE [dbo].[FactCurrentPriceAndCost]
(
[AccountKey] [dbo].[Key_Normal_type] NOT NULL,
[ItemKey] [dbo].[Key_Normal_type] NOT NULL,
[InvoiceLineGroup1Key] [dbo].[Key_Normal_type] NOT NULL,
[CurrentItemPrice] [dbo].[Money_Normal_Type] NULL,
[CurrentItemCost] [dbo].[Money_Normal_Type] NULL,
[CurrentDiscount] [decimal] (19, 18) NULL,
[CurrentPriceMethod] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CurrentListPrice] [decimal] (19, 12) NULL,
[RecentPriceDate] [datetime] NULL,
[RecentItemCost] [dbo].[Money_Normal_Type] NULL,
[RecentDiscount] [dbo].[Percent_Type] NULL,
[UDVarchar1] [dbo].[UDVarchar_type] NULL,
[UDVarchar2] [dbo].[UDVarchar_type] NULL,
[UDVarchar3] [dbo].[UDVarchar_type] NULL,
[UDVarchar4] [dbo].[UDVarchar_type] NULL,
[UDVarchar5] [dbo].[UDVarchar_type] NULL,
[UDInteger1] [int] NULL,
[PriceQuoteItemKey] [int] NULL,
[ProposalPriceKey] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[FactCurrentPriceAndCost] ADD CONSTRAINT [PK_FactCurrentPriceAndCost] PRIMARY KEY CLUSTERED  ([AccountKey], [ItemKey], [InvoiceLineGroup1Key]) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [IU_FactCurrentPriceAndCost_2] ON [dbo].[FactCurrentPriceAndCost] ([AccountKey], [ItemKey], [InvoiceLineGroup1Key], [CurrentItemCost]) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [IU_FactCurrentPriceAndCost_1] ON [dbo].[FactCurrentPriceAndCost] ([AccountKey], [ItemKey], [InvoiceLineGroup1Key], [CurrentItemPrice], [CurrentItemCost], [CurrentDiscount], [UDVarchar1]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[FactCurrentPriceAndCost] ADD CONSTRAINT [FK_FactCurrentPriceAndCost2_1] FOREIGN KEY ([AccountKey]) REFERENCES [dbo].[DimAccount] ([AccountKey])
GO
ALTER TABLE [dbo].[FactCurrentPriceAndCost] ADD CONSTRAINT [FK_FactCurrentPriceAndCost2_13] FOREIGN KEY ([InvoiceLineGroup1Key]) REFERENCES [dbo].[DimInvoiceLineGroup1] ([InvoiceLineGroup1Key])
GO
ALTER TABLE [dbo].[FactCurrentPriceAndCost] ADD CONSTRAINT [FK_FactCurrentPriceAndCost2_2] FOREIGN KEY ([ItemKey]) REFERENCES [dbo].[DimItem] ([ItemKey])
GO
