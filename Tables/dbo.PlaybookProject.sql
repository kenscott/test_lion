CREATE TABLE [dbo].[PlaybookProject]
(
[ProjectKey] [dbo].[Key_Normal_type] NOT NULL IDENTITY(1, 1),
[ProjectSpecificationKey] [dbo].[Key_Normal_type] NULL,
[ProjectName] [dbo].[Description_Normal_type] NULL,
[ProjectDescription] [dbo].[Description_Big_type] NULL,
[MinInvoiceDateDayKey] [dbo].[Key_Small_type] NULL,
[MaxInvoiceDateDayKey] [dbo].[Key_Small_type] NULL,
[ProductionProjectIndicator] [dbo].[Description_Small_type] NOT NULL CONSTRAINT [DF_PlaybookProject_ProductionIndicator] DEFAULT ('N'),
[Version] [dbo].[Int_Type] NOT NULL CONSTRAINT [DF__PlaybookP__Versi__5D859AAB] DEFAULT ((0)),
[CreationDate] [dbo].[DateTime_Type] NOT NULL CONSTRAINT [DF__PlaybookP__Creat__5E79BEE4] DEFAULT (getdate()),
[ModificationDate] [dbo].[DateTime_Type] NOT NULL CONSTRAINT [DF__PlaybookP__Modif__5F6DE31D] DEFAULT (getdate()),
[CreationUser] [dbo].[Description_Normal_type] NULL,
[ModificationUser] [dbo].[Description_Normal_type] NULL,
[ModificationUserAddress] [dbo].[Description_Normal_type] NULL,
[CreationUserAddress] [dbo].[Description_Normal_type] NULL,
[ApproachTypeKey] [dbo].[Key_Normal_type] NULL,
[AutoCommitDate] [datetime] NULL,
[InternalNote] [dbo].[Description_Huge_type] NULL,
[CreationUserKey] [dbo].[Key_Normal_type] NULL,
[ModificationUserKey] [dbo].[Key_Normal_type] NULL,
[StatusCodeKey] [dbo].[Key_Normal_type] NULL CONSTRAINT [DF_PlaybookProject_StatusCodeKey] DEFAULT ((1)),
[ExportIndicator] [nvarchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL CONSTRAINT [DF_PlaybookProject_ExportIndicator] DEFAULT ('N'),
[ProcessedDate] [datetime] NULL,
[GeneratedDate] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[PlaybookProject] ADD CONSTRAINT [PK_PojectKey] PRIMARY KEY NONCLUSTERED  ([ProjectKey]) ON [PRIMARY]
GO
CREATE UNIQUE CLUSTERED INDEX [IUC_PlaybookProject_ProjectKeyProductionProjectIndicator] ON [dbo].[PlaybookProject] ([ProjectKey], [ProductionProjectIndicator]) ON [PRIMARY]
GO
