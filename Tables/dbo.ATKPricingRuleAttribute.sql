CREATE TABLE [dbo].[ATKPricingRuleAttribute]
(
[PricingRuleAttributeKey] [dbo].[Key_Normal_type] NOT NULL IDENTITY(1, 1) NOT FOR REPLICATION,
[PricingRuleKey] [dbo].[Key_Normal_type] NOT NULL,
[PricingRuleAttributeSequenceNumber] [dbo].[Int_255_type] NOT NULL,
[PricingRuleAttributeTypeKey] [dbo].[Key_Normal_type] NOT NULL,
[Version] [dbo].[Int_Type] NOT NULL CONSTRAINT [DF__ATKPricin__Versi__4BA3B436] DEFAULT ((0)),
[CreationDate] [dbo].[DateTime_Type] NOT NULL CONSTRAINT [DF__ATKPricin__Creat__6657AA72] DEFAULT (getdate()),
[ModificationDate] [dbo].[DateTime_Type] NOT NULL CONSTRAINT [DF__ATKPricin__Modif__010BA0AE] DEFAULT (getdate()),
[CreationUser] [dbo].[Description_Normal_type] NULL,
[ModificationUser] [dbo].[Description_Normal_type] NULL,
[ModificationUserAddress] [dbo].[Description_Normal_type] NULL,
[CreationUserAddress] [dbo].[Description_Normal_type] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ATKPricingRuleAttribute] ADD CONSTRAINT [PK_ATKPricingRuleAttribute] PRIMARY KEY CLUSTERED  ([PricingRuleAttributeKey]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ATKPricingRuleAttribute] ADD CONSTRAINT [FK_ATKPricingRuleAttribute_1] FOREIGN KEY ([PricingRuleKey]) REFERENCES [dbo].[ATKPricingRule] ([PricingRuleKey])
GO
ALTER TABLE [dbo].[ATKPricingRuleAttribute] ADD CONSTRAINT [FK_ATKPricingRuleAttribute_2] FOREIGN KEY ([PricingRuleAttributeTypeKey]) REFERENCES [dbo].[ATKPricingRuleAttributeType] ([PricingRuleAttributeTypeKey])
GO
