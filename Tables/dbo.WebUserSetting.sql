CREATE TABLE [dbo].[WebUserSetting]
(
[WebUserSettingKey] [dbo].[Key_Normal_type] NOT NULL IDENTITY(1, 1),
[WebUserKey] [dbo].[Key_Normal_type] NOT NULL,
[SettingName] [dbo].[Description_Normal_type] NOT NULL,
[SettingValue] [dbo].[Description_Normal_type] NOT NULL,
[CreationDate] [dbo].[DateTime_Type] NOT NULL CONSTRAINT [DF_WebUserSetting_CreationDate] DEFAULT (getdate()),
[ModificationDate] [dbo].[DateTime_Type] NOT NULL CONSTRAINT [DF_WebUserSetting_ModificationDate] DEFAULT (getdate())
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[WebUserSetting] ADD CONSTRAINT [PK_WebUserSetting] PRIMARY KEY CLUSTERED  ([WebUserSettingKey]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[WebUserSetting] ADD CONSTRAINT [FK_WebUserSetting_1] FOREIGN KEY ([WebUserKey]) REFERENCES [dbo].[WebUser] ([WebUserKey])
GO
