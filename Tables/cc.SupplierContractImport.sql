CREATE TABLE [cc].[SupplierContractImport]
(
[SupplierContractImportKey] [int] NOT NULL IDENTITY(1, 1),
[BatchKey] [int] NOT NULL,
[SequenceNumber] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[PyramidCode] [varchar] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Originator] [varchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ContractNumber] [varchar] (6) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ContractOwner] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[VendorNumber] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ContractReference] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ContractType] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[StartDate] [date] NULL,
[ExpirationDate] [date] NULL,
[PurchaseType] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AuthorizedIndicator] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PricingContractType] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PricingContractID] [varchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[JobSite] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Developer] [varchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ContactName] [varchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ContactEmail] [varchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Accounts] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Locations] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LevelType] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LevelCode] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ClaimPercent] [decimal] (9, 4) NULL,
[ClaimAmount] [money] NULL,
[NetCost] [money] NULL,
[QuantityType] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[QuantityLimit] [decimal] (36, 4) NULL,
[RowNumber] [int] NOT NULL,
[ErrorMessage] [varchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [cc].[SupplierContractImport] ADD CONSTRAINT [PK_SupplierContractImport] PRIMARY KEY CLUSTERED  ([SupplierContractImportKey]) ON [PRIMARY]
GO
