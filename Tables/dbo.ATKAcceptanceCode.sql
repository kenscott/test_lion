CREATE TABLE [dbo].[ATKAcceptanceCode]
(
[AcceptanceCodeKey] [dbo].[Key_Small_type] NOT NULL IDENTITY(1, 1),
[AcceptanceCodeDesc] [dbo].[Description_Normal_type] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ATKAcceptanceCode] ADD CONSTRAINT [PK_ATKAcceptanceCode] PRIMARY KEY CLUSTERED  ([AcceptanceCodeKey]) ON [PRIMARY]
GO
