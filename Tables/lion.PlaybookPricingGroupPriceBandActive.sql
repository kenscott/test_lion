CREATE TABLE [lion].[PlaybookPricingGroupPriceBandActive]
(
[PlaybookPricingGroupKey] [dbo].[Key_Normal_type] NOT NULL,
[LowerBandQuantity] [dbo].[Quantity_Normal_type] NOT NULL,
[UpperBandQuantity] [dbo].[Quantity_Normal_type] NOT NULL,
[MemberRank] [int] NOT NULL,
[RulingMemberCount] [int] NOT NULL,
[PricingRuleSequenceNumber] [tinyint] NOT NULL,
[PDPGPP] [dbo].[Percent_Type] NOT NULL,
[PDPDiscountPercent] [dbo].[Percent_Type] NULL
) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [i1] ON [lion].[PlaybookPricingGroupPriceBandActive] ([PlaybookPricingGroupKey], [LowerBandQuantity], [UpperBandQuantity], [MemberRank]) ON [PRIMARY]
GO
CREATE UNIQUE CLUSTERED INDEX [iuc1] ON [lion].[PlaybookPricingGroupPriceBandActive] ([PlaybookPricingGroupKey], [MemberRank]) ON [PRIMARY]
GO
