CREATE TABLE [ppe].[ExcludeSPG]
(
[ExcludeSPGKey] [int] NOT NULL IDENTITY(1, 1),
[ItemGroup3Key] [int] NOT NULL,
[CreatedByWebUserKey] [int] NULL,
[CreationDate] [datetime] NULL CONSTRAINT [DF__ExcludeSP__Creat__2FE5BFD1] DEFAULT (getdate())
) ON [PRIMARY]
GO
