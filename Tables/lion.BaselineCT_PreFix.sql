CREATE TABLE [lion].[BaselineCT_PreFix]
(
[Region] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Area] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Network] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Branch] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[NTBaselineSales] [decimal] (38, 8) NULL,
[NTBaselineCost] [decimal] (38, 8) NULL,
[NTBaselineScoredSales] [decimal] (38, 15) NULL,
[NTBaselinePriceIndex] [decimal] (38, 7) NULL,
[NTBaselineAvgDailySales] [decimal] (38, 8) NULL,
[TBaselineSales] [decimal] (38, 8) NULL,
[TBaselineCost] [decimal] (38, 8) NULL,
[TBaselineScoredSales] [decimal] (38, 15) NULL,
[TBaselinePriceIndex] [decimal] (38, 7) NULL,
[TBaselineAvgDailySales] [decimal] (38, 8) NULL
) ON [PRIMARY]
GO
ALTER TABLE [lion].[BaselineCT_PreFix] ADD CONSTRAINT [PK_BaselineCT_PreFix] PRIMARY KEY CLUSTERED  ([Branch]) ON [PRIMARY]
GO
