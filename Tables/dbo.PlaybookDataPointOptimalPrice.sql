CREATE TABLE [dbo].[PlaybookDataPointOptimalPrice]
(
[PlaybookDataPointOptimalPriceKey] [dbo].[Key_Normal_type] NOT NULL IDENTITY(1, 1),
[PlaybookPricingGroupOptimalPriceKey] [dbo].[Key_Normal_type] NOT NULL,
[PlaybookDataPointPricingGroupKey] [dbo].[Key_Normal_type] NOT NULL,
[OptimalPrice] [dbo].[Money_Normal_Type] NULL,
[ThresholdFrequency] [dbo].[Quantity_Normal_type] NULL,
[ThresholdTotalPrice] [dbo].[Money_Normal_Type] NULL,
[OptimalCost] [dbo].[Money_Normal_Type] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[PlaybookDataPointOptimalPrice] ADD CONSTRAINT [PK_PlaybookDataPointOptimalPrice] PRIMARY KEY CLUSTERED  ([PlaybookDataPointOptimalPriceKey]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [I_PlaybookDataPointOptimalPrice_2] ON [dbo].[PlaybookDataPointOptimalPrice] ([PlaybookDataPointOptimalPriceKey], [PlaybookDataPointPricingGroupKey]) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [UI_PlaybookDataPointOptimalPrice_1] ON [dbo].[PlaybookDataPointOptimalPrice] ([PlaybookDataPointPricingGroupKey]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [I_PlaybookDataPointOptimalPrice_1] ON [dbo].[PlaybookDataPointOptimalPrice] ([PlaybookPricingGroupOptimalPriceKey]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[PlaybookDataPointOptimalPrice] ADD CONSTRAINT [FK_PlaybookDataPointOptimalPrice_1] FOREIGN KEY ([PlaybookPricingGroupOptimalPriceKey]) REFERENCES [dbo].[PlaybookPricingGroupOptimalPrice] ([PlaybookPricingGroupOptimalPriceKey])
GO
ALTER TABLE [dbo].[PlaybookDataPointOptimalPrice] ADD CONSTRAINT [FK_PlaybookDataPointOptimalPrice_2] FOREIGN KEY ([PlaybookDataPointPricingGroupKey]) REFERENCES [dbo].[PlaybookDataPointPricingGroup] ([PlaybookDataPointPricingGroupKey])
GO
