CREATE TABLE [dbo].[PlaybookDataPointGroupFullScopeAccountBands]
(
[PlaybookDataPointGroupKey] [dbo].[Key_Normal_type] NOT NULL,
[AccountKey] [dbo].[Key_Normal_type] NOT NULL,
[TotalActualPrice] [dbo].[Money_Normal_Type] NOT NULL,
[SalesBand] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SalesRank] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AvgGPPerOrder] [dbo].[Money_Normal_Type] NULL,
[GPBand] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[GPRank] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BlendedRank] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AvgLinesPerOrder] [dbo].[Quantity_Normal_type] NULL,
[TotalActual12MonthInvoiceLines] [dbo].[Quantity_Normal_type] NULL,
[TotalActual12MonthInvoices] [dbo].[Quantity_Normal_type] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[PlaybookDataPointGroupFullScopeAccountBands] ADD CONSTRAINT [PK_PlaybookDataPointGroupFullScopeAccountBands] PRIMARY KEY CLUSTERED  ([PlaybookDataPointGroupKey], [AccountKey]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[PlaybookDataPointGroupFullScopeAccountBands] ADD CONSTRAINT [FK_PlaybookDataPointGroupFullScopeAccountBands_1] FOREIGN KEY ([PlaybookDataPointGroupKey]) REFERENCES [dbo].[PlaybookDataPointGroup] ([PlaybookDataPointGroupKey])
GO
ALTER TABLE [dbo].[PlaybookDataPointGroupFullScopeAccountBands] ADD CONSTRAINT [FK_PlaybookDataPointGroupFullScopeAccountBands_2] FOREIGN KEY ([AccountKey]) REFERENCES [dbo].[DimAccount] ([AccountKey])
GO
