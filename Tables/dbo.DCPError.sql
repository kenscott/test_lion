CREATE TABLE [dbo].[DCPError]
(
[DCPErrorKey] [int] NOT NULL IDENTITY(1, 1),
[TimeOfError] [datetime] NOT NULL CONSTRAINT [DCPError_TimeOfError] DEFAULT (getdate()),
[ErrorProcedure] [sys].[sysname] NULL,
[ErrorNumber] [int] NOT NULL,
[ErrorLine] [int] NOT NULL,
[ErrorSeverity] [int] NOT NULL,
[ErrorState] [int] NOT NULL,
[ErrorMessage] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[DCPError] ADD CONSTRAINT [pkDCPError] PRIMARY KEY CLUSTERED  ([DCPErrorKey]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [idxDCPError_ErrorNumber] ON [dbo].[DCPError] ([ErrorNumber]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [idxDCPError_ErrorProcedure] ON [dbo].[DCPError] ([ErrorProcedure]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [idxDCPError_TimeOfError] ON [dbo].[DCPError] ([TimeOfError]) ON [PRIMARY]
GO
