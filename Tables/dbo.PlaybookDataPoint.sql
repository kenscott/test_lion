CREATE TABLE [dbo].[PlaybookDataPoint]
(
[PlaybookDataPointKey] [dbo].[Key_Normal_type] NOT NULL IDENTITY(1, 1) NOT FOR REPLICATION,
[PlaybookDataPointGroupKey] [dbo].[Key_Normal_type] NOT NULL,
[ItemKey] [dbo].[Key_Normal_type] NOT NULL,
[AccountKey] [dbo].[Key_Normal_type] NOT NULL,
[InvoiceLineGroup1Key] [dbo].[Key_Normal_type] NOT NULL,
[Total12MonthQuantity] [dbo].[Quantity_Normal_type] NOT NULL,
[LastSaleDayKey] [dbo].[Key_Small_type] NULL,
[TotalActual12MonthCost] [dbo].[Money_Normal_Type] NOT NULL,
[TotalActual12MonthPrice] [dbo].[Money_Normal_Type] NOT NULL,
[FullScopeBlendedRank] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CoreNonCoreIndicator] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AccountItemBandRank] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ItemBandRank] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FullScopeAccountTotalSalesBand] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FullScopeAccountTotalGrossProfitBand] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FullScopeAccountAvgGrossProfitPerOrderRank] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FullScopeAccountTotalSalesRank] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LastItemPrice] [dbo].[Money_Normal_Type] NULL,
[LastItemCost] [dbo].[Money_Normal_Type] NULL,
[LastPercentAccountItemSalesToAccountSales] [dbo].[Percent_Type] NULL,
[LastItemDiscountPercent] [dbo].[Percent_Type] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[PlaybookDataPoint] ADD CONSTRAINT [PK_PlaybookDataPoint] PRIMARY KEY CLUSTERED  ([PlaybookDataPointKey]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [I_PlaybookDataPoint_5] ON [dbo].[PlaybookDataPoint] ([AccountKey], [ItemKey], [InvoiceLineGroup1Key], [PlaybookDataPointGroupKey]) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [I_PlaybookDataPoint_6] ON [dbo].[PlaybookDataPoint] ([AccountKey], [ItemKey], [InvoiceLineGroup1Key], [PlaybookDataPointGroupKey]) INCLUDE ([LastItemCost], [LastItemDiscountPercent], [LastItemPrice], [LastSaleDayKey], [PlaybookDataPointKey], [Total12MonthQuantity]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [I_PlaybookDataPoint_4] ON [dbo].[PlaybookDataPoint] ([PlaybookDataPointGroupKey]) INCLUDE ([AccountKey], [InvoiceLineGroup1Key], [ItemKey], [LastItemCost], [LastItemPrice], [LastSaleDayKey], [PlaybookDataPointKey], [Total12MonthQuantity], [TotalActual12MonthPrice]) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [I_PlaybookDataPoint_1] ON [dbo].[PlaybookDataPoint] ([PlaybookDataPointGroupKey], [AccountKey], [ItemKey], [InvoiceLineGroup1Key]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [I_PlaybookDataPoint_2] ON [dbo].[PlaybookDataPoint] ([PlaybookDataPointGroupKey], [ItemKey]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [I_PlaybookDataPoint_3] ON [dbo].[PlaybookDataPoint] ([PlaybookDataPointGroupKey], [ItemKey], [InvoiceLineGroup1Key]) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [I_PlaybookDataPoint_52] ON [dbo].[PlaybookDataPoint] ([PlaybookDataPointGroupKey], [PlaybookDataPointKey], [ItemKey], [AccountKey], [InvoiceLineGroup1Key], [LastSaleDayKey], [TotalActual12MonthPrice], [Total12MonthQuantity], [LastItemPrice], [LastItemCost]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [_dta_index_PlaybookDataPoint_5_1749581271__K1_3_4_5] ON [dbo].[PlaybookDataPoint] ([PlaybookDataPointKey]) INCLUDE ([AccountKey], [InvoiceLineGroup1Key], [ItemKey]) ON [PRIMARY]
GO
CREATE STATISTICS [_dta_stat_1749581271_5_4_3_1] ON [dbo].[PlaybookDataPoint] ([AccountKey], [ItemKey], [PlaybookDataPointKey], [InvoiceLineGroup1Key])
GO
CREATE STATISTICS [_dta_stat_1749581271_1_4_3] ON [dbo].[PlaybookDataPoint] ([PlaybookDataPointKey], [AccountKey], [ItemKey])
GO
ALTER TABLE [dbo].[PlaybookDataPoint] ADD CONSTRAINT [FK_PlaybookDataPoint_1] FOREIGN KEY ([PlaybookDataPointGroupKey]) REFERENCES [dbo].[PlaybookDataPointGroup] ([PlaybookDataPointGroupKey])
GO
ALTER TABLE [dbo].[PlaybookDataPoint] ADD CONSTRAINT [FK_PlaybookDataPoint_2] FOREIGN KEY ([ItemKey]) REFERENCES [dbo].[DimItem] ([ItemKey])
GO
ALTER TABLE [dbo].[PlaybookDataPoint] ADD CONSTRAINT [FK_PlaybookDataPoint_3] FOREIGN KEY ([AccountKey]) REFERENCES [dbo].[DimAccount] ([AccountKey])
GO
ALTER TABLE [dbo].[PlaybookDataPoint] ADD CONSTRAINT [FK_PlaybookDataPoint_4] FOREIGN KEY ([InvoiceLineGroup1Key]) REFERENCES [dbo].[DimInvoiceLineGroup1] ([InvoiceLineGroup1Key])
GO
ALTER TABLE [dbo].[PlaybookDataPoint] ADD CONSTRAINT [FK_PlaybookDataPoint_5] FOREIGN KEY ([LastSaleDayKey]) REFERENCES [dbo].[DimDay] ([DayKey])
GO
