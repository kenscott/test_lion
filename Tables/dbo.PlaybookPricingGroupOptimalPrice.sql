CREATE TABLE [dbo].[PlaybookPricingGroupOptimalPrice]
(
[PlaybookPricingGroupOptimalPriceKey] [dbo].[Key_Normal_type] NOT NULL IDENTITY(1, 1),
[PricingBandOptimalPricePercentKey] [dbo].[Key_Normal_type] NOT NULL,
[PlaybookPricingGroupQuantityBandKey] [dbo].[Key_Normal_type] NOT NULL,
[OptimalPrice] [dbo].[Money_Normal_Type] NULL,
[OptimalCost] [dbo].[Money_Normal_Type] NULL,
[OptimalMargin] [dbo].[Percent_Type] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[PlaybookPricingGroupOptimalPrice] ADD CONSTRAINT [PK_PlaybookOptimalPrice] PRIMARY KEY CLUSTERED  ([PlaybookPricingGroupOptimalPriceKey]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [I_PlaybookPricingGroupOptimalPrice_1] ON [dbo].[PlaybookPricingGroupOptimalPrice] ([PlaybookPricingGroupQuantityBandKey]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [I_PlaybookPricingGroupOptimalPrice_2] ON [dbo].[PlaybookPricingGroupOptimalPrice] ([PlaybookPricingGroupQuantityBandKey]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[PlaybookPricingGroupOptimalPrice] ADD CONSTRAINT [FK_PlaybookPricingGroupOptimalPrice_1] FOREIGN KEY ([PlaybookPricingGroupQuantityBandKey]) REFERENCES [dbo].[PlaybookPricingGroupQuantityBand] ([PlaybookPricingGroupQuantityBandKey])
GO
