CREATE TABLE [dbo].[ATKAccountManagerStatus]
(
[AccountManagerStatus] [dbo].[Description_Normal_type] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ATKAccountManagerStatus] ADD CONSTRAINT [PK_ATKAccountManagerStatus] PRIMARY KEY CLUSTERED  ([AccountManagerStatus]) ON [PRIMARY]
GO
