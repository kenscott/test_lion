IF NOT EXISTS (SELECT * FROM master.dbo.syslogins WHERE loginname = N'lion')
CREATE LOGIN [lion] WITH PASSWORD = 'p@ssw0rd'
GO
CREATE USER [lion] FOR LOGIN [lion] WITH DEFAULT_SCHEMA=[lion]
GO
GRANT EXECUTE TO [lion]
