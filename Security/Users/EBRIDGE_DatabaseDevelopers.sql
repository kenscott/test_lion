IF NOT EXISTS (SELECT * FROM master.dbo.syslogins WHERE loginname = N'EBRIDGE\DatabaseDevelopers')
CREATE LOGIN [EBRIDGE\DatabaseDevelopers] FROM WINDOWS
GO
CREATE USER [EBRIDGE\DatabaseDevelopers] FOR LOGIN [EBRIDGE\DatabaseDevelopers]
GO
