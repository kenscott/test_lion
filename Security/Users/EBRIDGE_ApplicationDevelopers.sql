IF NOT EXISTS (SELECT * FROM master.dbo.syslogins WHERE loginname = N'EBRIDGE\ApplicationDevelopers')
CREATE LOGIN [EBRIDGE\ApplicationDevelopers] FROM WINDOWS
GO
CREATE USER [EBRIDGE\ApplicationDevelopers] FOR LOGIN [EBRIDGE\ApplicationDevelopers]
GO
GRANT EXECUTE TO [EBRIDGE\ApplicationDevelopers]
GRANT SELECT TO [EBRIDGE\ApplicationDevelopers]
GRANT VIEW DEFINITION TO [EBRIDGE\ApplicationDevelopers]
