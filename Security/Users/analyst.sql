IF NOT EXISTS (SELECT * FROM master.dbo.syslogins WHERE loginname = N'analyst')
CREATE LOGIN [analyst] WITH PASSWORD = 'p@ssw0rd'
GO
CREATE USER [analyst] FOR LOGIN [analyst] WITH DEFAULT_SCHEMA=[lion]
GO
GRANT EXECUTE TO [analyst]
